#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include <math.h>

#define ALFABETO "abcxV().,+*/-%^0123456789"
#define OPERADORES "+*/-%^V"

#define DELIMITADOR 1
#define VARIAVEL 2
#define NUMERO 3

char *prog; /* Cont�m a express�o a ser analisada */
char token[80];
char tok_type;

int isError = 0;

void validate_exp(double *answer);
void validate_exp2(double *answer);
void validate_exp3(double *answer);
void validate_exp4(double *answer);
void validate_exp5(double *answer);
void validate_exp6(double *answer);
void get_token(void);
void atom(double *answer);
void putback(void);
void serror(int error);
int isdelim(char c);
int validateAlphabet();
int validateOperator(char *string);
void strrep(char *str, char old, char new);
char *replace(char *instring,char *old,char *new);

int main(void) {
    /* Responsavel por permitir acento */
    setlocale (LC_ALL, "portuguese");
    
	double answer;
	char *pointer;
	
	int i;
	
	/*variavel X*/
	char *variables_name = "abc";
	int variablesLength = strlen(variables_name);
	char *variables[5];
	
	/* Aloca size bytes e retorna um ponteiro para a mem�ria alocada. */ 
	pointer = malloc(100);

	if(!pointer) {
		printf("Falha na aloca��o\n");
		exit(1);
	}

	/* Processa express�es at� que uma linha em branco seja digitada */
	do {
        prog = pointer;
        
        for(i=0; i < variablesLength; i++) {
            printf("informe o valor de %c: ", variables_name[i]);
            variables[i] = malloc(100);
            gets(variables[i]);
            fflush(stdin);
        }
        for(i=0; i < variablesLength; i++) {
            if(strlen(variables[i]) == 1) {
                strrep(variables[i], variables_name[i], variables[i][0]);
            } else {
                int j, k;
                for(j=0; j < strlen(variables[i]); j++) {
                    for(k=0; k < variablesLength; k++) {
                        if(k != i) {
                            char variablesNameStr[2];
                            variablesNameStr[0] = variables_name[k];
                            variablesNameStr[1] = '\0';
                            strcpy(variables[i], replace(variables[i], variablesNameStr, variables[k]));
                        }
                    }
                }
            }
        }
		
		printf("Digite a express�o: ");
		gets(prog);
        
		if(!*prog)
			break;
			
		if(validateAlphabet()) {
            for(i=0; i < variablesLength; i++) {
                char variablesNameStr[2];
                variablesNameStr[0] = variables_name[i];
                variablesNameStr[1] = '\0';
                strcpy(prog, replace(prog, variablesNameStr, variables[i]));
            }
            
            validate_exp(&answer);
            if(!isError) {
                printf("A resposta � %f\n", answer);
            }
        } else {
            serror(0);
        }
        isError = 0;
	} while(*pointer);

	return 0;

} /* Fim da fun��o main */

/* Ponto de entrada do analisador */
void validate_exp(double *answer) {
	get_token();
    
	if(!*token) {
		serror(2);
		return;
	}

	validate_exp2(answer);

	if(*token)
		serror(0); /* �ltimo token deve ser null */

} /* Fim da fun��o eval_exp */

/* Soma ou subtrai dois termos */
void validate_exp2(double *answer) {
	register char op;
	double temp;

	validate_exp3(answer);
    
	while((op = *token) == '+' || op == '-') {
		get_token();
		validate_exp3(&temp);

		switch(op) {
			case '-':
				*answer = *answer - temp;
				break;

			case '+':
				*answer = *answer + temp;
		}
	}

} /* Fim da fun��o eva_exp2 */

/* Multiplica ou divide dois fatores */
void validate_exp3(double *answer) {
	register char op;
	double temp;

	validate_exp4(answer);

	while((op = *token) == '*' || op == '/' || op == '%') {
		get_token();
		validate_exp4(&temp);

		switch(op) {
			case '*':
				*answer = *answer * temp;
				break;

			case '/':
				*answer = *answer / temp;
				break;

			case '%':
				*answer = (int) *answer % (int) temp;
		}
	}

} /* Fim da fun��o eva_exp3 */

/* Processa um expoente */
void validate_exp4(double *answer) {
	double temp, ex;
	register int t;
	char op;

	validate_exp5(answer);

	if((op = *token) == '^' || op == 'V') {
		get_token();
		validate_exp4(&temp);
		
		ex = *answer;
		switch(op) {
			case '^':
        		if(temp == 0.0) {
        			*answer = 1.0;
        			return;
        		}
        
        		for(t = temp - 1; t > 0; --t)
        			*answer = (*answer) * (double) ex;
				break;

			case 'V':
        	    *answer = pow(*answer, 1.0/temp);
				break;
		}
	}

} /* Fim da fun��o eva_exp4 */

/* Avalia um + ou - un�rio */
void validate_exp5(double *answer) {
	register char op;

	op = 0;

	if(((tok_type == DELIMITADOR) && (*token == '+' || *token == '-'))) {
		op = *token;
		get_token();
	}

	validate_exp6(answer);

	if(op == '-')
		*answer = -(*answer);

} /* Fim da fun��o eva_exp5 */

/* Processa uma express�o entre par�ntese */
void validate_exp6(double *answer) {
	if((*token == '(')) {
		get_token();
		validate_exp2(answer);

		if(*token != ')')
			serror(1);

		get_token();
	} else
		atom(answer);
} /* Fim da fun��o eva_exp6 */

/* Devolve o pr�ximo token */
void get_token(void) {
	register char *temp;

	tok_type = 0;
	temp = token;
	*temp = '\0';

	if(!*prog)
		return; /* Final da express�o */

	while(isspace(*prog))
		++prog; /* Ignora espa�os em branco */

	if(strchr("+-*/%^()V", *prog)) {
		tok_type = DELIMITADOR;

		/* Avan�a para o pr�ximo char */
		*temp++ = *prog++;
	} else if(isalpha(*prog)) {
        /* Verifica se � um alfanum�rico */
		while(!isdelim(*prog))
			*temp++ = *prog++;

		tok_type = VARIAVEL;
	} else if(isdigit(*prog)) {
        /* Verifica se � um numero */
		while(!isdelim(*prog))
			*temp++ = *prog++;

		tok_type = NUMERO;
	}

	*temp = '\0';
} /* Fim da fun��o get_token */

/* Obt�m o valor real de um n�mero */
void atom(double *answer) {
	if(tok_type == NUMERO) {
        strrep(token, '.', ',');
		*answer = atof(token);
		get_token();
		return;
	}

	serror(0); /* Caso contr�rio, erro de sintaxe na express�o  */

} /* Fim da fun��o atom */

/* Devolve um token � stream de entrada */
void putback(void) {
	char *t;
	
	t = token;

	for( ; *t; t++)
		prog--;

} /* Fim da fun��o putback */

/* Apresenta um erro de sintaxe */
void serror(int error) {
	static char *e[] = {
		"Erro de sintaxe",
		"Falta de par�nteses",
		"Nenhuma express�o presente"
		};

	printf("%s\n", e[error]);
    isError = 1;
} /* Fim da fun��o serror */

/* Devolve verdadeiro se c � um delimitador */
int isdelim(char c) {
	if(strchr(" +-/*%^()V", c) || c == 9 || c == '\r' || c == 0)
		return 1;

	return 0;

} /* Fim da fun��o isdelim */

/* Verifica se cada caractere esta no alfabeto */
int validateAlphabet() {
    int i, j, different = 0, isValid = 1;
    int progSize = strlen(prog), alphabetSize = strlen(ALFABETO);
    
    for(i=0; i < progSize; i++) {
        different = 0;
        for(j=0; j < alphabetSize; j++) {
            if(prog[i] != ALFABETO[j])
                different++;
        } 
        if(different == alphabetSize) {
            isValid = 0;
            break;
        } 
    }
    
    return isValid;
}

/* Verifica possui um operador */
int validateOperator(char *string) {
    int i, j;
    int stringSize = strlen(string);
    int operatorsSize = strlen(OPERADORES);
    
    for(i=0; i < stringSize; i++) {
        for(j=0; j < operatorsSize; j++) {
            if(string[i] == OPERADORES[j])
                return 0;
        } 
    }
    
    return 1;
}

/* Responsavel por fazer o replace de caratere */
void strrep(char *str, char old, char new)  {
    char *pos;
    while (1)  {
        pos = strchr(str, old);
        if (pos == NULL)  {
            break;
        }
        *pos = new;
    }
}

/* Responsavel por fazer o replace de string */
char *replace(char *instring,char *old,char *new) {
    if(!instring || !old || !new){
        return (char*)NULL;
    }

    size_t instring_size=strlen(instring);
    size_t new_size=strlen(new);
    size_t old_size=strlen(old);
    size_t diffsize=new_size-old_size;
    size_t diffsizeAll=diffsize;
    size_t outstring_size=instring_size*2 + 1;
    char *outstring;
    char *test;

    test=(char*)malloc(old_size+1);
    outstring =(char*) malloc(outstring_size);

    if(!outstring || !test){
        return (char*)NULL;
    }
    if(instring_size<old_size || old_size==0)
    {       
       strcpy(outstring, instring);
       free(test);
       return outstring;
    }   
    outstring[0]='\0';
    int i;
    for(i=0; i <= instring_size; i++)
    {       
        strncpy(test,(instring+i),old_size);
        test[old_size]='\0';
        if(strcmp(test,old)==0){
            if((instring_size+diffsizeAll) > outstring_size)
            {
                outstring_size=outstring_size*2+1;
                outstring=realloc(outstring,outstring_size);
                if(!outstring){
                    free(test);
                    return (char*)NULL;
                }
            }
            strcat(outstring,new);
            i=i+old_size-1;
            diffsizeAll=diffsizeAll+diffsize;
        }else{
            test[1]='\0';
            strcat(outstring,test);
        }
    }
    free(test);
    return outstring;
}
