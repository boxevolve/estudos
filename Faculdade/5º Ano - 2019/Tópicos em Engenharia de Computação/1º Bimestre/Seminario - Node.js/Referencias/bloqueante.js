const arquivoSistema = require('fs');
let conteudoArquivo;
const somaMatematica = 1 + 1;

try {
    conteudoArquivo = arquivoSistema.readFileSync('grande-arquivo.txt', 'utf-8');
    console.log('sucesso - arquivo lido');
} catch (erro) {
    console.log(erro);
}

const texto = `A soma é ${ somaMatematica }`;
console.log(texto);
