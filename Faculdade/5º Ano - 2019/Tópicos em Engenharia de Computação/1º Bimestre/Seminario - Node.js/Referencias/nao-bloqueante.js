const arquivoSistema = require('fs');
const somaMatematica = 1 + 1;

arquivoSistema.readFile('grande-arquivo.txt', 'utf-8', function (erro, conteudoArquivo) {
    if (erro) {
        return console.log(erro);
    }
    console.log('sucesso - arquivo lido');
});

const texto = `A soma é ${ somaMatematica }`;
console.log(texto);
