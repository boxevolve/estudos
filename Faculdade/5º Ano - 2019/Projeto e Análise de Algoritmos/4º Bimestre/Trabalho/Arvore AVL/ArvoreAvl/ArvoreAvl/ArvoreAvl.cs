﻿using System;

namespace ArvoreAvl
{
    public class ArvoreAvl
    {
        private No raiz;

        public void Inserir(int novoElemento)
        {
            var aInserir = new No(novoElemento);
            InserirAVL(raiz, aInserir);
        }

        private void InserirAVL(No aComparar, No aInserir)
        {
            if (aComparar == null)
                raiz = aInserir;
            else
            {
                if (aInserir.Chave < aComparar.Chave)
                {
                    if (aComparar.Esquerda == null)
                    {
                        aComparar.Esquerda = aInserir;
                        aInserir.Pai = aComparar;
                        VerificarBalanceamento(aComparar);
                    }
                    else
                        InserirAVL(aComparar.Esquerda, aInserir);
                }
                else if (aInserir.Chave > aComparar.Chave)
                {
                    if (aComparar.Direita == null)
                    {
                        aComparar.Direita = aInserir;
                        aInserir.Pai = aComparar;
                        VerificarBalanceamento(aComparar);
                    }
                    else
                        InserirAVL(aComparar.Direita, aInserir);
                }
            }
        }

        public void VerificarBalanceamento(No atual)
        {
            AdicionarBalanceamento(atual);

            var balanceamento = atual.Balanceamento;

            if (balanceamento == -2)
            {
                if (ObterAltura(atual.Esquerda.Esquerda) >= ObterAltura(atual.Esquerda.Direita))
                    atual = RotacaoDireita(atual);
                else
                    atual = DuplaRotacaoEsquerdaDireita(atual);
            }
            else if (balanceamento == 2)
            {

                if (ObterAltura(atual.Direita.Direita) >= ObterAltura(atual.Direita.Esquerda))
                    atual = RotacaoEsquerda(atual);
                else
                    atual = DuplaRotacaoDireitaEsquerda(atual);
            }

            if (atual.Pai != null)
                VerificarBalanceamento(atual.Pai);
            else
                raiz = atual;
        }

        private No RotacaoEsquerda(No inicial)
        {
            No pivot = inicial.Direita;

            pivot.Pai = inicial.Pai;

            inicial.Direita = pivot.Esquerda;

            if (inicial.Direita != null)
                inicial.Direita.Pai = inicial;

            pivot.Esquerda = inicial;

            inicial.Pai = pivot;

            if (pivot.Pai != null)
            {
                if (pivot.Pai.Direita == inicial)
                    pivot.Pai.Direita = pivot;
                else if (pivot.Pai.Esquerda == inicial)
                    pivot.Pai.Esquerda = pivot;
            }

            AdicionarBalanceamento(inicial);

            AdicionarBalanceamento(pivot);

            return pivot;
        }

        public No RotacaoDireita(No inicial)
        {
            No pivot = inicial.Esquerda;

            pivot.Pai = inicial.Pai;

            inicial.Esquerda = pivot.Direita;

            if (inicial.Esquerda != null)
                inicial.Esquerda.Pai = inicial;

            pivot.Direita = inicial;

            inicial.Pai = pivot;

            if (pivot.Pai != null)
            {
                if (pivot.Pai.Esquerda == inicial)
                    pivot.Pai.Esquerda = pivot;
                else if (pivot.Pai.Direita == inicial)
                    pivot.Pai.Direita = pivot;
            }

            AdicionarBalanceamento(inicial);
            AdicionarBalanceamento(pivot);

            return pivot;
        }

        public No DuplaRotacaoEsquerdaDireita(No inicial)
        {
            inicial.Esquerda = RotacaoEsquerda(inicial.Esquerda);

            return RotacaoDireita(inicial);
        }

        public No DuplaRotacaoDireitaEsquerda(No inicial)
        {
            inicial.Direita = RotacaoDireita(inicial.Direita);

            return RotacaoEsquerda(inicial);
        }

        private int ObterAltura(No atual)
        {
            if (atual == null)
                return -1;
            
            if (atual.Esquerda == null && atual.Direita == null)
                return 0;

            else if (atual.Esquerda == null)
                return 1 + ObterAltura(atual.Direita);

            else if (atual.Direita == null)
                return 1 + ObterAltura(atual.Esquerda);

            else
                return 1 + Math.Max(ObterAltura(atual.Esquerda), ObterAltura(atual.Direita));
        }

        private void AdicionarBalanceamento(No no) =>
            no.Balanceamento = (ObterAltura(no.Direita) - ObterAltura(no.Esquerda));

        public void ImprimirEmOrdem()
        {
            Console.WriteLine("Em Ordem");

            ImprimirEmOrdem(raiz);

            Console.WriteLine("\n");
        }

        protected void ImprimirEmOrdem(No no)
        {
            if (no != null)
            {
                ImprimirEmOrdem(no.Esquerda);

                Console.Write($"{ no.Chave } - ");

                ImprimirEmOrdem(no.Direita);
            }
        }

        public void ImprimirPreOrdem()
        {
            Console.WriteLine("Pre Ordem");

            ImprimirPreOrdem(raiz);

            Console.WriteLine("\n");
        }

        protected void ImprimirPreOrdem(No no)
        {
            if (no != null)
            {
                Console.Write($"{ no.Chave } - ");

                ImprimirPreOrdem(no.Esquerda);

                ImprimirPreOrdem(no.Direita);
            }
        }

        public void ImprimirPosOrdem()
        {
            Console.WriteLine("Pos Ordem");

            ImprimirPosOrdem(raiz);

            Console.WriteLine("\n");
        }

        protected void ImprimirPosOrdem(No no)
        {
            if (no != null)
            {
                ImprimirPosOrdem(no.Esquerda);

                ImprimirPosOrdem(no.Direita);

                Console.Write($"{ no.Chave } - ");
            }
        }
    }
}
