﻿using System;
using System.Collections.Generic;

namespace ArvoreAvl
{
    public static class Program
    {
        static void Main(string[] args)
        {
            ArvoreAvl arvoreParaTeste = new ArvoreAvl();

            var valores = new List<int>()
            {
                30, 20, 15, 55, 45, 60, 50
            };

            foreach (var valor in valores)
            {
                arvoreParaTeste.Inserir(valor);
            }

            arvoreParaTeste.ImprimirEmOrdem();

            arvoreParaTeste.ImprimirPreOrdem();

            arvoreParaTeste.ImprimirPosOrdem();

            Console.WriteLine("Programa finalizado\n");
        }
    }
}
