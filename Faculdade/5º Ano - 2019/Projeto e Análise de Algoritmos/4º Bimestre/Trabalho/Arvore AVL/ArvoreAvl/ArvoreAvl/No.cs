﻿namespace ArvoreAvl
{
    public class No
    {
        public No(int chave)
        {
            Pai = null;
            Direita = null;
            Esquerda = null;
            Chave = chave;
        }

        public No Esquerda { get; set; }
        public No Direita { get; set; }
        public No Pai { get; set; }
        public int Chave { get; set; }
        public int Balanceamento { get; set; }
    }
}
