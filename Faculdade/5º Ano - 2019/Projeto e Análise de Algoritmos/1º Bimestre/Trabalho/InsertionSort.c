#include <stdio.h>
#include <stdlib.h>

int menu(int *A, int length);
void insertionSort(int *A, int length, int asc);
void print(int *A, int length);

int main() {
    int A[] = {3, 2, 1, 4, 5, 7, 6, 0};
    int length = (int)(sizeof(A) / sizeof(A[0]));
    
    insertionSort(A, length, menu(A, length));
    printf("Resultado:\n");
    print(A, length);
}

int menu(int *A, int length) {
    int option = -1;
    do {
        printf("+--[ Insertion Sort ]--+\n");
        printf("| Array = [ "); print(A, length); printf(" ]\n");
        printf("| Escolha o tipo de ordena��o:\n");
        printf("| Crescente = [ 1 ] - Decrescente = [ 0 ]\n");
        scanf("%d", &option);
        if (option != 0 && option != 1) {
            system("cls");
        }
    } while(option != 0 && option != 1);
}

void insertionSort(int *A, int length, int asc) {
    int j, i, key;
    for (j=1; j < length; j++) {
        key = A[j];
        i = j-1;
        while(i >= 0 && ((asc == 1 && A[i] > key) || (asc == 0 && A[i] < key))) {
            A[i+1] = A[i];
            i = i-1;
        }
        A[i+1] = key;
    }
}

void print(int *A, int length) {
    int i;
    for(i=0; i < length; i++) {
        printf("%d", A[i]);
        if (i != length-1) {
            printf(" - ");
        }
    }
}
