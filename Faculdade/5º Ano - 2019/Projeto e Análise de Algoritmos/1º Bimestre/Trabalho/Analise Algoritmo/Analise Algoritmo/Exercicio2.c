#include <stdio.h>
#include <stdlib.h>
#include<math.h>

double log2(double data) {
    return (log10(data) / log10(2.0));
}

int main() {
    double input = 2;
    double insertion = 0;
    double inter = 0;
    do {
        insertion = 8*pow (input, 2);
        inter = 64 * input * log2(input);
        printf("Input: %f | Insercao: %f | Intercalacaoo: %f \n", input, insertion, inter);
        input++;
    } while(insertion < inter);
    return 0;
}
