#include <stdio.h>
#include <stdlib.h>
#include<math.h>

double log2(double data) {
    return (log10(data) / log10(2.0));
}

int main() {
    // Exercicio 1
    /*double input = 2;
    double insertion = 0;
    double intercalation = 0;
    do {
        insertion = 8 * pow (input, 2);
        intercalation = 64 * input * log2(input);
        printf("Input: %f | Insercao: %f | Intercalacao: %f \n", input, insertion, intercalation);
        input++;
    } while(insertion < intercalation);

    // Exercicio 2
    /*double input = 2;
    double algoritmo1 = 0;
    double algoritmo2 = 0;

    do {
        algoritmo1 = 100 * pow (input, 2);
        algoritmo2 = pow (2, input);
        printf("Input: %f | Algoritmo1: %f | Algoritmo2: %f \n", input, algoritmo1, algoritmo2);
        input++;
    } while(algoritmo2 < algoritmo1);*/

    // Exercicio 3
    /*double time[] = {
        pow(1, -6),
        pow(1.6667, -8),
        pow(2.77783333, -10),
        pow(1.1574305541667, -11),
        pow(3.805246966852749016, -13),
        pow(3.171042614155342523, -14),
        pow(3.171042614155342641e, -16)
    };*/
    double second = 1 * pow(10, -6);
    /*double minute = pow(1.6667, -8);
    double hour = pow(2.77783333, -10);
    double day = pow(1.1574305541667, -11);
    double month = pow(3.805246966852749016, -13);
    double year = pow(3.171042614155342523, -14);
    double century = pow(3.171042614155342641e, -16);

    /*for(int i=0; i < 7; i++) {
        printf("%d = %f", i+1, log());
    }*/
    /*double execution1 = log();
    double execution2 = 0;
    double execution3 = 0;
    double execution4 = 0;
    double execution5 = 0;
    double execution6 = 0;
    double execution7 = 0;
    double execution8 = 0;*/

    printf("%f", log(second));
    return 0;
}
