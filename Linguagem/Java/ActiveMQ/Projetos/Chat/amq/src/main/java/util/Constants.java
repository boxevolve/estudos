package util;

public class Constants {
	
	private Constants() {
		super();
	}
	
	/**
	 * ActiveMQ
	 */
	public static final String ACTIVEMQ_USERNAME = "admin";
	public static final String ACTIVEMQ_PASS = "admin";
	public static final String ACTIVEMQ_WIRE_LEVEL_ENDPOINT = "tcp://localhost:61616";
	public static final String ACTIVEMQ_TOPIC = "myTopic";
	
	/**
	 * Padr�es
	 */
	public static final String TITLE_PROJECT = "Chatzera";
	public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
	
	/**
	 * T�tulos
	 */
	public static final String ATTENTION = "Aten��o!";
	
	/**
	 * Mensagens
	 */
	public static final String MSG_EXIT = "Deseja realmente sair do {0}?";
	public static final String ERROR_CLOSE_JMS = "Ocorreu um erro inesperado ao fechar a comunica��o!";
	public static final String ERROR_INIT_JMS = "Ocorreu um erro inesperado ao iniciar a comunica��o!";
	public static final String ERROR_INIT = "Ocorreu um erro inesperado ao iniciar a aplicac�o!";
	
	/**
	 * Labels
	 */
	public static final String LBL_NAME = "Informe seu nome:";
	
	/**
	 * Bot�es
	 */
	public static final String BTN_EXIT = "Sair";
	public static final String BTN_SEND = "Enviar";
	
	/**
	 * M�todos
	 */
	public static String getText(String text, String... variables) {
		String result = text;
		if (variables == null) {
			return result;
		}
		for (int i = 0; i < variables.length; i++) {
			result = text.replace("{" + i + "}", variables[i]);
		}
		return result;
	}
	
}
