package ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.jms.JMSException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import config.Config;
import entity.User;
import util.Constants;
import util.Util;

public class ChatUI {
	
	/**
	 * Interface gr�fica
	 */
	private JFrame frame;
	private JTextArea display;
	private JTextArea inputText;
	private Config config;

	/**
	 * Informa��es
	 */
	private User user;

	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public ChatUI(User user, Config config) {
		setUser(user);
		setConfig(config);
		initialize();
	}

	/**
	 * Inicializa a janela.
	 */
	private void initialize() {
		JPanel middlePanel = new JPanel ();
	    middlePanel.setBorder (new TitledBorder(new EtchedBorder(), getTitle()));

	    /**
	     * Componentes dos textos de mensagens recebidas.
	     */
	    setDisplay(new JTextArea ( 16, 58 ));
	    getDisplay().setWrapStyleWord(true);
	    getDisplay().setLineWrap(true);
	    getDisplay().setEditable(false);
	    
	    JScrollPane scroll = new JScrollPane ( getDisplay() );
	    scroll.setViewportBorder(UIManager.getBorder("Button.border"));
	    scroll.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
	    middlePanel.add(scroll);

	    setFrame(new JFrame ());
	    getFrame().setTitle(getTitleChat());
	    getFrame().setResizable(false);
	    getFrame().getContentPane().add ( middlePanel );
	    Util.closeConfig(frame, getConfig());
	    
	    /**
	     * Componentes de texto de mensagem que sera enviada
	     */
	    JPanel panelBot = new JPanel();
	    getFrame().getContentPane().add(panelBot, BorderLayout.SOUTH);
	    
	    setInputText(new JTextArea());
	    getInputText().setLineWrap(true);
	    getInputText().setRows(5);
	    getInputText().setColumns(51);
	    
	    JScrollPane scrollInputText = new JScrollPane(getInputText());
	    scrollInputText.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    panelBot.add (scrollInputText);
	    
	    JButton btnSend = new JButton(Constants.BTN_SEND);
	    btnSend.addActionListener((ActionEvent e) -> send());
	    panelBot.add(btnSend);
	    getFrame().pack();
	    getFrame().setLocationRelativeTo(null);
	    setVisible(true);
	}
	
	private void send() {
		try {
			getConfig().send(this, getInputText().getText());
		} catch (JMSException e) {
			Util.showMessageError(null, Util.getException(Constants.ERROR_INIT, e.getMessage()));
		}
		getInputText().setText("");
	}
	
	public static void setTextMessage(JTextArea component, String message) {
		StringBuilder text = new StringBuilder();
		text.append(component.getText());
		text.append(message);
		text.append("\n");
		component.setText(text.toString());
	}
	
	public String getTitle() {
		return Constants.TITLE_PROJECT;
	}
	
	public String getTitleChat() {
		StringBuilder title = new StringBuilder();
		title.append(Constants.TITLE_PROJECT);
		title.append(" - ");
		title.append(getUser().getName());
		return title.toString();
	}
	
	public void setVisible(boolean visible) {
		if (this.frame != null) {
			this.frame.setVisible(visible);
		}
	}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	public JTextArea getDisplay() {
		return display;
	}
	
	public void setDisplay(String message) {
		StringBuilder text = new StringBuilder();
		text.append(display.getText());
		text.append(message);
		text.append("\n");
		display.setText(text.toString());
	}

	public void setDisplay(JTextArea display) {
		this.display = display;
	}
	
	public JTextArea getInputText() {
		return inputText;
	}

	public void setInputText(JTextArea inputText) {
		this.inputText = inputText;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}
	
}
