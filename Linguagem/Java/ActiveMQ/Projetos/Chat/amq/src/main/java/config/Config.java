package config;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;

import amq.ConsumidorJMS;
import ui.ChatUI;
import util.Constants;
import util.Util;

public class Config {
	
	private Session producerSession;
	private MessageProducer producer;
	private Connection producerConnection;
	private ConsumidorJMS consumidorJMS;
	
	public Config() {
		super();
		connect();
	}
	
	private void connect() {
		// Especificar parametros para conexao
		final String wireLevelEndpoint = Constants.ACTIVEMQ_WIRE_LEVEL_ENDPOINT;
		final String activeMqUsername = Constants.ACTIVEMQ_USERNAME;
		final String activeMqPass = Constants.ACTIVEMQ_PASS;

		try {
			// Iniciar o Consumidor em um thread separado.
			consumidorJMS = new ConsumidorJMS(wireLevelEndpoint, activeMqUsername, activeMqPass);
			Thread t = new Thread(consumidorJMS);
			t.start();

			// Criar uma fabrica de conexoes.
			final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(activeMqUsername,
					activeMqPass, wireLevelEndpoint);

			// Informar username and password.
			connectionFactory.setUserName(activeMqUsername);
			connectionFactory.setPassword(activeMqPass);

			// Criar um Fabrica de Conexoes tipo Pooled para o produtor.
			final PooledConnectionFactory pooledConnectionFactoryProducer = new PooledConnectionFactory();
			pooledConnectionFactoryProducer.setConnectionFactory(connectionFactory);
			pooledConnectionFactoryProducer.setMaxConnections(10);

			// Estabelecer uma conexao para o produtor
			producerConnection = pooledConnectionFactoryProducer.createConnection();
			producerConnection.start();

			// Criar uma session.
			this.producerSession = producerConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Criar um t�pico chamada.
			final Destination producerDestination = producerSession.createTopic(Constants.ACTIVEMQ_TOPIC);

			// Criar um produtor para o t�pico.
			this.producer = producerSession.createProducer(producerDestination);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		} catch (JMSException e) {
			Util.showMessageError(null, Util.getException(Constants.ERROR_INIT_JMS, e.getMessage()));
		}
	}
	
	public void send(ChatUI chat, String message) throws JMSException {
		StringBuilder textMessage = new StringBuilder();
		textMessage.append(Util.getDate());
		textMessage.append(" ");
		textMessage.append(chat.getUser().getName());
		textMessage.append(":\n");
		textMessage.append(message);
		final TextMessage producerMessage = getProducerSession().createTextMessage(textMessage.toString());

		// Enviar a messagem.
		getProducer().send(producerMessage);
	}
	
	public void close() {
		try {
			consumidorJMS.close();
			producer.close();
			producerSession.close();
			getProducerConnection().close();
		} catch (Exception e) {
			Util.showMessageError(null, Util.getException(Constants.ERROR_CLOSE_JMS, e.getMessage()));
		}
	}

	public Session getProducerSession() {
		return producerSession;
	}

	public void setProducerSession(Session producerSession) {
		this.producerSession = producerSession;
	}

	public MessageProducer getProducer() {
		return producer;
	}

	public void setProducer(MessageProducer producer) {
		this.producer = producer;
	}

	public Connection getProducerConnection() {
		return producerConnection;
	}

	public void setProducerConnection(Connection producerConnection) {
		this.producerConnection = producerConnection;
	}

	public ConsumidorJMS getConsumidorJMS() {
		return consumidorJMS;
	}

	public void setConsumidorJMS(ConsumidorJMS consumidorJMS) {
		this.consumidorJMS = consumidorJMS;
	}
	
}
