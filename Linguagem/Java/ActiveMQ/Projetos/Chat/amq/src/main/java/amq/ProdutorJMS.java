package amq;

import javax.swing.JOptionPane;

import config.Config;
import entity.User;
import ui.ChatUI;
import util.Constants;
import util.Util;

public class ProdutorJMS {
	
	private static ChatUI chat;

	public static void main(String[] args) {
		try {
			String nome = JOptionPane.showInputDialog(Constants.LBL_NAME);
			if (nome != null) {
				setChat(new ChatUI(new User(nome), new Config()));
			}
		} catch (Exception e) {
			Util.showMessageError(null, Util.getException(Constants.ERROR_INIT, e.getMessage()));
		}
	}

	public static ChatUI getChat() {
		return chat;
	}

	public static void setChat(ChatUI chat) {
		ProdutorJMS.chat = chat;
	}

}
