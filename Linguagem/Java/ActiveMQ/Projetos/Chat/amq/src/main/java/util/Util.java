package util;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import config.Config;

public class Util {
	
	private Util() {
		super();
	}
	
	public static void closeConfig(JFrame frame, Config config) {
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (Util.showConfirmDialog(frame, Constants.BTN_EXIT, Constants.getText(Constants.MSG_EXIT, Constants.TITLE_PROJECT))) {
					config.close();
					System.exit(0);
				}
			}
		});
	}
	
	public static String getDate() {
		SimpleDateFormat sdff = new SimpleDateFormat(Constants.DATE_FORMAT);
		return sdff.format(new Date());
	}
	
	public static String getException(String message, String e) {
		StringBuilder exception = new StringBuilder();
		exception.append(message);
		exception.append("\n");
		exception.append(e);
		return exception.toString();
	}
	
	public static boolean showConfirmDialog(Component parent, String title, String message) {
		int option = JOptionPane.showConfirmDialog(parent, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		return option == JOptionPane.YES_OPTION;
	}
	
	public static boolean showMessageWarning(Component parent, String message) {
		int option = JOptionPane.showConfirmDialog(parent, message, Constants.ATTENTION, JOptionPane.PLAIN_MESSAGE, JOptionPane.WARNING_MESSAGE);
		return option == JOptionPane.OK_OPTION;
	}
	
	public static boolean showMessageError(Component parent, String message) {
		int option = JOptionPane.showConfirmDialog(parent, message, Constants.ATTENTION, JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);
		return option == JOptionPane.OK_OPTION;
	}
	
}
