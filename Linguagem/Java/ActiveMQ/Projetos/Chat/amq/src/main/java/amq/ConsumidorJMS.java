package amq;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;

import util.Constants;
import util.Util;

public class ConsumidorJMS implements Runnable {

	// Parametros para conexao
	private String wireLevelEndpoint;
	private String activeMqUsername;
	private String activeMqPassword;
	private String historico = "";
	private boolean close = false;

	public ConsumidorJMS(String url, String userName, String password) {
		this.setWireLevelEndpoint(url);
		this.setActiveMqUsername(userName);
		this.setActiveMqPassword(password);
	}

	public void run() {
		PooledConnectionFactory pooledConnectionFactoryConsumer = null;
		Connection consumerConnection = null;
		MessageConsumer consumer = null;
		Session consumerSession = null;

		try {
			// Criar uma fabrica de conexoes
			final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(getWireLevelEndpoint());

			// Informar username and password.
			connectionFactory.setUserName(getActiveMqUsername());
			connectionFactory.setPassword(getActiveMqPassword());

			// Criar um Fabrica de Conexoes tipo Pooled para o consumidor
			pooledConnectionFactoryConsumer = new PooledConnectionFactory();
			pooledConnectionFactoryConsumer.setConnectionFactory(connectionFactory);
			pooledConnectionFactoryConsumer.setMaxConnections(10);

			// Estabelecer conexao para o consumidor
			consumerConnection = pooledConnectionFactoryConsumer.createConnection();
			consumerConnection.start();

			// Criar session.
			consumerSession = consumerConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Criar um t�pico.
			final Destination consumerDestination = consumerSession.createTopic(Constants.ACTIVEMQ_TOPIC);

			// Criar um consumidor para o t�pico.
			consumer = consumerSession.createConsumer(consumerDestination);

			while (!isClose()) {
				// Aguardar por mensagens
				final Message consumerMessage = consumer.receive();

				// Receber mensagem quando chegar
				final TextMessage consumerTextMessage = (TextMessage) consumerMessage;
				setHistorico(getHistorico() + consumerTextMessage.getText() + "\n");
				ProdutorJMS.getChat().getDisplay().setText(getHistorico());
			}
		} catch (JMSException ex) {
			Util.showMessageError(null, Util.getException(Constants.ERROR_INIT_JMS, ex.getMessage()));
		} finally {
			// "Limpeza" dos recursos do consumido
			if (consumer != null) {
				try {
					consumer.close();
					consumerSession.close();
					consumerConnection.close();
					pooledConnectionFactoryConsumer.stop();
				} catch (JMSException e) {
					Util.showMessageError(null, Util.getException(Constants.ERROR_CLOSE_JMS, e.getMessage()));
				}
			}
		}
	}
	
	public void close() {
		setClose(true);
	}

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public String getWireLevelEndpoint() {
		return wireLevelEndpoint;
	}

	public void setWireLevelEndpoint(String wireLevelEndpoint) {
		this.wireLevelEndpoint = wireLevelEndpoint;
	}

	public String getActiveMqUsername() {
		return activeMqUsername;
	}

	public void setActiveMqUsername(String activeMqUsername) {
		this.activeMqUsername = activeMqUsername;
	}

	public String getActiveMqPassword() {
		return activeMqPassword;
	}

	public void setActiveMqPassword(String activeMqPassword) {
		this.activeMqPassword = activeMqPassword;
	}

	public boolean isClose() {
		return close;
	}

	public void setClose(boolean close) {
		this.close = close;
	}
	
}
