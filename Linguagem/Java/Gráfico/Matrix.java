package matrix;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * 
 * @author bruno.dourado
 *
 */
public class Matrix extends JPanel {
	
	/**
	 * Bruno Casimiro Dourado 05215300
	 * Rafael Eduardo de Natali 05215049
	 * Thaylison Gabriel Silva 05214019
	 */

	private static final long serialVersionUID = -1426645310567102527L;
	
	/**
	 * Recuperar as dimens�es total da tela.
	 */
	private static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    private static int screenWidth = (int) screen.getWidth();
    private static int screenHeight = (int) screen.getHeight();
	
	/**
	 * Constantes
	 */
	public static final String NAME_FRAME = "Trabalho";

	/**
	 * Valores de minimo e maximo que a aplica��o suporta.
	 */
	private static Double[][] minMax = {{-2000d, +2000d},
						                {-2000d, +2000d}};
	
	/**
	 * Tamanho em x e em y da janela da aplica��o.
	 */
	private static Integer tx = screenWidth;
	private static Integer ty = screenHeight;
	
	/**
	 * Valores das coordenadas que deseja mostrar na tela com os valores reais, representa a matriz original.
	 */
	static Double[][] originalWorld = {{-1000d, -1000d, 0d, 1000d, 1000d, 0d},
							           {-1000d, -500d, 1000d, -500d, -1000d, -1500d}};

	/**
	 * Valores das coordenadas que deseja mostrar na tela com os valores reais, representa a matriz editada.
	 */
	static Double[][] world;

	/**
	 * Respons�vel por transformar as coordenadas da matriz em linhas na tela.
	 * Formas de crias as linhas:
	 * 
	 * Primeira:
	 *     Ponto x
	 *     g.setColor(Color.RED);
	 *     g.drawLine(rX(mundo[0][i]), rY(mundo[1][i]), rX(mundo[0][j]), rY(mundo[1][j]));
	 * 
	 * Segunda:
	 *     Ponto x
	 *     drawLine(g, mundo, i, j, Color.RED);
	 *     
	 * Terceira:
	 *     Ponto x
	 *     createMask(g, i, j, Color.RED);
	 */
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		
		// Borda
		createEdge(g);
		
		// Eixos
		createAxis(g);
		
		drawClose(g, Color.RED);
		/*// Ponto 1
		createMask(g, 0, 1, Color.RED);
		// Ponto 2
		createMask(g, 1, 2, Color.GREEN);
		// Ponto 3
		createMask(g, 2, 3, Color.BLUE);
		// Ponto 4
		createMask(g, 3, 4, Color.CYAN);
		// Ponto 5
		createMask(g, 4, 0, Color.ORANGE);*/
	}

	/**
	 * Respons�vel por inicializar a aplica��o.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		matrixOperation();
	}
	
	/**
	 * Respons�vel por realizar a opera��es da matriz.
	 * A��e possiveis:
	 *     translacao(mundo, 500, 1500)
	 *     
	 * Ponto zero:
	 *     escala(mundo, 0.5, 0.5)
	 *     rotacao(mundo, 90d)
	 *     
	 * Ponto arbitrario:
	 * 	   Integer[] origin = {500, 500}
	 *     escala(mundo, 0.7, 0.7, origin)
	 *	   rotacao(mundo, 90d, origin)
	 */
	public static void matrixOperation() {
		world = cloneMatrix(originalWorld);
		
		escala(world, 0.5, 0.5);
		
		Matrix panel = new Matrix();
		
		createFrame(panel);
	}
	
	/**
	 * Respons�vel por criar a borda.
	 * 
	 * @param g
	 */
	public static void createEdge(Graphics g) {
		g.drawLine(rX(minMax[0][0]), rY(minMax[1][1]), rX(minMax[0][1]), rY(minMax[1][1]));
		g.drawLine(rX(minMax[0][1]), rY(minMax[1][1]), rX(minMax[0][1]), rY(minMax[1][0]));
		g.drawLine(rX(minMax[0][1]), rY(minMax[1][0]), rX(minMax[0][0]), rY(minMax[1][0]));
		g.drawLine(rX(minMax[0][0]), rY(minMax[1][0]), rX(minMax[0][0]), rY(minMax[1][1]));
	}
	
	/**
	 * Respons�vel por criar os eixos x e y.
	 * 
	 * @param g
	 */
	public static void createAxis(Graphics g) {
		// Eixo x
		g.drawLine(rX(minMax[0][0]), rY(0d), rX(minMax[0][1]), rY(0d));
		
		// Eixo y
		g.drawLine(rX(0d), rY(minMax[1][0]), rX(0d), rY(minMax[1][1]));
	}
	
	/**
	 * Respons�vel por criar figura fechada, utilizando a sequencia de pontos na matriz.
	 * 
	 * @param g
	 * @param a
	 * @param b
	 * @param color
	 */
	public static void drawClose(Graphics g, Color color) {
		if (originalWorld == null || originalWorld.length == 0) {
			return;
		}
		int length = originalWorld[0].length;
		int next;
		for (int i = 0; i < length; i++) {
			next = (i == length - 1) ? 0 : i + 1;
			drawLine(g, originalWorld, i, next, Color.MAGENTA);
			drawLine(g, world, i, next, color);
		}
	}
	
	/**
	 * Respons�vel por criar duas figuras, uma editada e uma original.
	 * 
	 * @param g
	 * @param a
	 * @param b
	 * @param color
	 */
	public static void createMask(Graphics g, int a, int b, Color color) {
		drawLine(g, originalWorld, a, b, Color.MAGENTA);
		drawLine(g, world, a, b, color);
	}
	
	/**
	 * Respons�vel por criar linha entre dois pontos.
	 * 
	 * @param g
	 * @param matrix
	 * @param a
	 * @param b
	 * @param color
	 */
	public static void drawLine(Graphics g, Double[][] matrix, int a, int b, Color color) {
		g.setColor(color);
		g.drawLine(rX(matrix[0][a]), rY(matrix[1][a]), rX(matrix[0][b]), rY(matrix[1][b]));
	}
	
	/**
	 * Respons�vel por converter o valor em x da coordenada real para o tamanho da janela na aplica��o.
	 * 
	 * @param xMundo
	 * @return
	 */
	public static Integer rX(Double xMundo) {
		return rX(tx, xMundo, minMax[0][1], minMax[0][0]);
	}

	/**
	 * Respons�vel por converter o valor em x da coordenada real para o tamanho da janela na aplica��o, utilizando o tamanho em x da janela e minimos e m�ximos em x.
	 * 
	 * @param xMundo
	 * @return
	 */
	public static Integer rX(Integer tx, Double xMundo, Double maxX, Double minX) {
		Long result = Math.round(((xMundo - minX) / (maxX - minX)) * tx);
		return Integer.parseInt(result.toString());
	}

	/**
	 * Respons�vel por converter o valor em y da coordenada real para o tamanho da janela na aplica��o.
	 * @param yMundo
	 * @return
	 */
	public static Integer rY(Double yMundo) {
		return rY(ty, yMundo, minMax[1][1], minMax[1][0]);
	}
	
	/**
	 * Respons�vel por converter o valor em y da coordenada real para o tamanho da janela na aplica��o, utilizando o tamanho em y da janela e minimos e m�ximos em y.
	 * 
	 * @param ty
	 * @param yMundo
	 * @param maxY
	 * @param minY
	 * @return
	 */
	public static Integer rY(Integer ty, Double yMundo, Double maxY, Double minY) {
		Long result = Math.round((1 - ((yMundo - minY) / (maxY - minY))) * ty);
		return Integer.parseInt(result.toString());
	}
	
	/**
	 * Realiza a opera��o de transla��o.
	 * 
	 * @param matriz
	 * @param tx
	 * @param ty
	 */
	public static void translacao(Double[][] matriz, Integer tx, Integer ty) {
		for (int i = 0; i < matriz[0].length; i++) {
			matriz[0][i] = matriz[0][i] + tx;
			matriz[1][i] = matriz[1][i] + ty;
		}
	}
	
	/**
	 * Realiza a opera��o de escala em um ponto arbitrario.
	 * 
	 * @param matriz
	 * @param ex
	 * @param ey
	 * @param origin
	 */
	public static void escala(Double[][] matriz, Double ex, Double ey, Integer[] origin) {
		if (origin == null || origin.length != 2) {
			origin = new Integer[2];
			origin[0] = 0;
			origin[1] = 0;
		}
		translacao(matriz, origin[0] * -1, origin[1] * -1);
		escala(matriz, ex, ey);
		translacao(matriz, origin[0], origin[1]);
	}
	
	/**
	 * Realiza a opera��o de escala.
	 * 
	 * @param matriz
	 * @param ex
	 * @param ey
	 */
	public static void escala(Double[][] matriz, Double ex, Double ey) {
		for (int i = 0; i < matriz[0].length; i++) {
			matriz[0][i] = matriz[0][i] * ex;
			matriz[1][i] = matriz[1][i] * ey;
		}
	}
	
	/**
	 * Realiza a opera��o de rota��o em um ponto arbitrario.
	 * 
	 * @param matriz
	 * @param angulo
	 * @param origin
	 */
	public static void rotacao(Double[][] matriz, Double angulo, Integer[] origin) {
		if (origin == null || origin.length != 2) {
			origin = new Integer[2];
			origin[0] = 0;
			origin[1] = 0;
		}
		translacao(matriz, origin[0] * -1, origin[1] * -1);
		rotacao(matriz, angulo);
		translacao(matriz, origin[0], origin[1]);
	}
	
	/**
	 * Realiza a opera��o de rota��o.
	 * 
	 * @param matriz
	 * @param angulo
	 */
	public static void rotacao(Double[][] matriz, Double angulo) {
		Double cos = Math.cos(Math.toRadians(angulo));
		Double sin = Math.sin(Math.toRadians(angulo));
		
		Double[][] matrizRotacao= {{cos, -sin},
					               {sin, cos}};
		
		Double[][] c = multiplicarMatriz(matrizRotacao, matriz);
		for (int i = 0; i < matriz[0].length; i++) {
			matriz[0][i] = c[0][i];
			matriz[1][i] = c[1][i];
		}
	}
	
	/**
	 * Respons�vel por realizar as multiplica��o entre matrizes.
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static Double[][] multiplicarMatriz(Double[][] a, Double[][] b) {
		int linhaA = 0;
		int colunaA = 0;

		int quantLinhasA = a.length;

		int quantLinhasB = b.length;
		int quantColunasB = b[0].length;

		Double aux = 0d;
		
		Double[][] resultante = new Double[quantLinhasA][quantColunasB];

		for (linhaA = 0; linhaA < quantLinhasA; linhaA++) {
			for (colunaA = 0; colunaA < quantColunasB; colunaA++) {
				for (int k = 0; k < quantLinhasB; k++) {
					aux = aux + a[linhaA][k]* b[k][colunaA];
				}

				resultante[linhaA][colunaA] = aux;
				aux = 0d;
			}
		}
		
		return resultante;
	}
	
	/**
	 * Respons�vel por clonar a matriz.
	 * 
	 * @param matrix
	 * @return
	 */
	public static Double[][] cloneMatrix(Double[][] matrix) {
		int line = matrix.length;
		int column = matrix[0].length;
		Double[][] result = new Double[line][column];
		for(int i=0; i < line; i++) {
			for(int j=0; j < column; j++) {
				result[i][j] = matrix[i][j];
			}
		}
		return result;
	}
	
	/**
	 * Respons�vel por criar a janela da aplica��o.
	 * 
	 * @param panel
	 */
	public static void createFrame(Matrix panel) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame(NAME_FRAME);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.white);
		frame.setSize(tx.intValue(), ty.intValue());
		frame.add(panel);
		frame.setVisible(true);
	}
	
}