package grafico;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Matrix extends JPanel {
	
	/**
	 * Bruno Casimiro Dourado 05215300
	 * Rafael Eduardo de Natali 05215049
	 * Thaylison Gabriel Silva 05214019
	 */

	private static final long serialVersionUID = -1426645310567102527L;
	
	private static Dimension tela = Toolkit.getDefaultToolkit().getScreenSize();
    private static int lar = (int) tela.getWidth();
    private static int alt = (int) tela.getHeight();
	
	/**
	 * Constantes
	 */
	public static final String NAME_FRAME = "Trabalho";

	/**
	 * Valores de minimo e maximo que a aplica��o suporta.
	 */
	private static Double[][] minMax = {{-2000d, +2000d},
						                {-2000d, +2000d}};
	
	/**
	 * Tamanho em x e em y da janela da aplica��o.
	 */
	private static Integer tx = lar;
	private static Integer ty = alt;
	
	/**
	 * Valores das coordenadas que deseja mostrar na tela com os valores reais.
	 */
	static Double[][] mundoOriginal = {{-1000d, 1000d, 0d},
							           {-1000d, -1000d, 1000d}};

	/**
	 * Valores das coordenadas que deseja mostrar na tela com os valores reais.
	 */
	static Double[][] mundo = {{-1000d, 1000d, 0d},
							   {-1000d, -1000d, 1000d}};

	/**
	 * Respons�vel por transformar as coordenadas da matriz em linhas na tela.
	 */
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		
		// Borda
		g.drawLine(rX(minMax[0][0]), rY(minMax[1][1]), rX(minMax[0][1]), rY(minMax[1][1]));
		g.drawLine(rX(minMax[0][1]), rY(minMax[1][1]), rX(minMax[0][1]), rY(minMax[1][0]));
		g.drawLine(rX(minMax[0][1]), rY(minMax[1][0]), rX(minMax[0][0]), rY(minMax[1][0]));
		g.drawLine(rX(minMax[0][0]), rY(minMax[1][0]), rX(minMax[0][0]), rY(minMax[1][1]));
		
		// Eixo x
		g.drawLine(rX(minMax[0][0]), rY(0d), rX(minMax[0][1]), rY(0d));
		
		// Eixo y
		g.drawLine(rX(0d), rY(minMax[1][0]), rX(0d), rY(minMax[1][1]));
		
		
		// Ponto 1
		g.setColor(Color.RED);
		g.drawLine(rX(mundo[0][0]), rY(mundo[1][0]), rX(mundo[0][1]), rY(mundo[1][1]));

		// Ponto 2
		g.setColor(Color.GREEN);
		g.drawLine(rX(mundo[0][1]), rY(mundo[1][1]), rX(mundo[0][2]), rY(mundo[1][2]));

		// Ponto 3
		g.setColor(Color.BLUE);
		g.drawLine(rX(mundo[0][2]), rY(mundo[1][2]), rX(mundo[0][0]), rY(mundo[1][0]));
		
		// Ponto 1
		g.setColor(Color.MAGENTA);
		g.drawLine(rX(mundoOriginal[0][0]), rY(mundoOriginal[1][0]), rX(mundoOriginal[0][1]), rY(mundoOriginal[1][1]));

		// Ponto 2
		g.setColor(Color.MAGENTA);
		g.drawLine(rX(mundoOriginal[0][1]), rY(mundoOriginal[1][1]), rX(mundoOriginal[0][2]), rY(mundoOriginal[1][2]));

		// Ponto 3
		g.setColor(Color.MAGENTA);
		g.drawLine(rX(mundoOriginal[0][2]), rY(mundoOriginal[1][2]), rX(mundoOriginal[0][0]), rY(mundoOriginal[1][0]));
	}

	/**
	 * Respons�vel por inicializar a aplica��o.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		matrixOperation();
	}
	
	/**
	 * Respons�vel por realizar a opera��es da matriz.
	 */
	public static void matrixOperation() {
		//rotacao(mundo, 90d, origin);
		
		//escala(mundo, 0.5, 0.5);
		//translacao(mundo, -1000, -1000);
		/*translacao(mundo, 500, 1500);
		escala(mundo, 0.5, 0.5);
		rotacao(mundo, 90d);
		
		Integer[] origin = {500, 500};
		escala(mundo, 0.7, 0.7, origin);
		rotacao(mundo, 90d, origin);*/
		
		
		Matrix panel = new Matrix();
		
		createFrame(panel);
	}
	
	/**
	 * Respons�vel por converter o valor em x da coordenada real para o tamanho da janela na aplica��o.
	 * 
	 * @param xMundo
	 * @return
	 */
	public static Integer rX(Double xMundo) {
		return rX(tx, xMundo, minMax[0][1], minMax[0][0]);
	}

	/**
	 * Respons�vel por converter o valor em x da coordenada real para o tamanho da janela na aplica��o, utilizando o tamanho em x da janela e minimos e m�ximos em x.
	 * 
	 * @param xMundo
	 * @return
	 */
	public static Integer rX(Integer tx, Double xMundo, Double maxX, Double minX) {
		Long result = Math.round(((xMundo - minX) / (maxX - minX)) * tx);
		return Integer.parseInt(result.toString());
	}

	/**
	 * Respons�vel por converter o valor em y da coordenada real para o tamanho da janela na aplica��o.
	 * @param yMundo
	 * @return
	 */
	public static Integer rY(Double yMundo) {
		return rY(ty, yMundo, minMax[1][1], minMax[1][0]);
	}
	
	/**
	 * Respons�vel por converter o valor em y da coordenada real para o tamanho da janela na aplica��o, utilizando o tamanho em y da janela e minimos e m�ximos em y.
	 * 
	 * @param ty
	 * @param yMundo
	 * @param maxY
	 * @param minY
	 * @return
	 */
	public static Integer rY(Integer ty, Double yMundo, Double maxY, Double minY) {
		Long result = Math.round((1 - ((yMundo - minY) / (maxY - minY))) * ty);
		return Integer.parseInt(result.toString());
	}
	
	/**
	 * Realiza a opera��o de transla��o.
	 * 
	 * @param matriz
	 * @param tx
	 * @param ty
	 */
	public static void translacao(Double[][] matriz, Integer tx, Integer ty) {
		for (int i = 0; i < matriz[0].length; i++) {
			matriz[0][i] = matriz[0][i] + tx;
			matriz[1][i] = matriz[1][i] + ty;
		}
	}
	
	/**
	 * Realiza a opera��o de escala em um ponto arbitrario.
	 * 
	 * @param matriz
	 * @param ex
	 * @param ey
	 * @param origin
	 */
	public static void escala(Double[][] matriz, Double ex, Double ey, Integer[] origin) {
		if (origin == null || origin.length != 2) {
			origin = new Integer[2];
			origin[0] = 0;
			origin[1] = 0;
		}
		translacao(matriz, origin[0] * -1, origin[1] * -1);
		escala(matriz, ex, ey);
		translacao(matriz, origin[0], origin[1]);
	}
	
	/**
	 * Realiza a opera��o de escala.
	 * 
	 * @param matriz
	 * @param ex
	 * @param ey
	 */
	public static void escala(Double[][] matriz, Double ex, Double ey) {
		for (int i = 0; i < matriz[0].length; i++) {
			matriz[0][i] = matriz[0][i] * ex;
			matriz[1][i] = matriz[1][i] * ey;
		}
	}
	
	/**
	 * Realiza a opera��o de rota��o em um ponto arbitrario.
	 * 
	 * @param matriz
	 * @param angulo
	 * @param origin
	 */
	public static void rotacao(Double[][] matriz, Double angulo, Integer[] origin) {
		if (origin == null || origin.length != 2) {
			origin = new Integer[2];
			origin[0] = 0;
			origin[1] = 0;
		}
		translacao(matriz, origin[0] * -1, origin[1] * -1);
		rotacao(matriz, angulo);
		translacao(matriz, origin[0], origin[1]);
	}
	
	/**
	 * Realiza a opera��o de rota��o.
	 * 
	 * @param matriz
	 * @param angulo
	 */
	public static void rotacao(Double[][] matriz, Double angulo) {
		Double cos = Math.cos(Math.toRadians(angulo));
		Double sin = Math.sin(Math.toRadians(angulo));
		
		Double[][] matrizRotacao= {{cos, -sin},
					               {sin, cos}};
		
		Double[][] c = multiplicarMatriz(matrizRotacao, matriz);
		for (int i = 0; i < matriz[0].length; i++) {
			matriz[0][i] = c[0][i];
			matriz[1][i] = c[1][i];
		}
	}
	
	/**
	 * Respons�vel por realizar as multiplica��o entre matrizes.
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static Double[][] multiplicarMatriz(Double[][] a, Double[][] b) {
		int linhaA = 0;
		int colunaA = 0;

		int quantLinhasA = a.length;

		int quantLinhasB = b.length;
		int quantColunasB = b[0].length;

		Double aux = 0d;
		
		Double[][] resultante = new Double[quantLinhasA][quantColunasB];

		for (linhaA = 0; linhaA < quantLinhasA; linhaA++) {
			for (colunaA = 0; colunaA < quantColunasB; colunaA++) {
				for (int k = 0; k < quantLinhasB; k++) {
					aux = aux + a[linhaA][k]* b[k][colunaA];
				}

				resultante[linhaA][colunaA] = aux;
				aux = 0d;
			}
		}
		
		return resultante;
	}
	
	/**
	 * Respons�vel por criar a janela da aplica��o.
	 * 
	 * @param panel
	 */
	public static void createFrame(Matrix panel) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame(NAME_FRAME);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.white);
		frame.setSize(tx.intValue(), ty.intValue());
		frame.add(panel);
		frame.setVisible(true);
	}
	
}
