package br.edu.chat.chatzao.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import br.edu.chat.chatzao.config.Config;
import br.edu.chat.chatzao.entity.Room;
import br.edu.chat.chatzao.entity.User;
import br.edu.chat.chatzao.util.Constants;

public class ChatUI {
	
	/**
	 * Interface gráfica
	 */
	private JFrame frame;
	private JTextArea display;
	private JTextArea inputText;

	/**
	 * Informações
	 */
	private Room room;
	private User user;

	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public ChatUI(User user, Room room) {
		setRoom(room);
		setUser(user);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		JPanel middlePanel = new JPanel ();
	    middlePanel.setBorder (new TitledBorder(new EtchedBorder(), getTitle()));

	    /**
	     * Componentes dos textos de mensagens recebidas
	     */
	    setDisplay(new JTextArea ( 16, 58 ));
	    getDisplay().setWrapStyleWord(true);
	    getDisplay().setLineWrap(true);
	    getDisplay().setEditable(false);
	    
	    JScrollPane scroll = new JScrollPane ( getDisplay() );
	    scroll.setViewportBorder(UIManager.getBorder("Button.border"));
	    scroll.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
	    middlePanel.add(scroll);

	    setFrame(new JFrame ());
	    getFrame().setTitle(getTitleChat());
	    getFrame().setResizable(false);
	    getFrame().getContentPane().add ( middlePanel );
	    getFrame().setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	    getFrame().addWindowListener(new WindowAdapter() {
	    	@Override
	        public void windowClosing(WindowEvent e) {
	    		Config.close(getRoom());
	            setVisible(false);
	        }
	    });
	    
	    /**
	     * Componentes de texto de mensagem que sera enviada
	     */
	    JPanel panelBot = new JPanel();
	    getFrame().getContentPane().add(panelBot, BorderLayout.SOUTH);
	    
	    setInputText(new JTextArea());
	    getInputText().setLineWrap(true);
	    getInputText().setRows(5);
	    getInputText().setColumns(51);
	    
	    JScrollPane scrollInputText = new JScrollPane(getInputText());
	    scrollInputText.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    panelBot.add (scrollInputText);
	    
	    JButton btnSend = new JButton(Constants.BTN_SEND);
	    btnSend.addActionListener((ActionEvent e) -> send());
	    panelBot.add(btnSend);
	    getFrame().pack();
	    getFrame().setLocationRelativeTo(null);
	    setVisible(true);
	}
	
	private void send() {
		Config.send(this, getInputText().getText());
		getInputText().setText("");
	}
	
	public static void setTextMessage(JTextArea component, String message) {
		StringBuilder text = new StringBuilder();
		text.append(component.getText());
		text.append(message);
		text.append("\n");
		component.setText(text.toString());
	}
	
	public String getTitle() {
		return getRoom() != null ? getRoom().getName() : "";
	}
	
	public String getTitleChat() {
		StringBuilder title = new StringBuilder();
		title.append(Constants.TITLE_PROJECT);
		title.append(" - ");
		title.append(getTitle());
		return title.toString();
	}
	
	public void setVisible(boolean visible) {
		if (this.frame != null) {
			this.frame.setVisible(visible);
		}
	}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	public JTextArea getDisplay() {
		return display;
	}
	
	public void setDisplay(String message) {
		StringBuilder text = new StringBuilder();
		text.append(display.getText());
		text.append(message);
		text.append("\n");
		display.setText(text.toString());
	}

	public void setDisplay(JTextArea display) {
		this.display = display;
	}
	
	public JTextArea getInputText() {
		return inputText;
	}

	public void setInputText(JTextArea inputText) {
		this.inputText = inputText;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
