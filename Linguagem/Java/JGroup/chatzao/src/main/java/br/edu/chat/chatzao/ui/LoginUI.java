package br.edu.chat.chatzao.ui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import br.edu.chat.chatzao.config.Config;
import br.edu.chat.chatzao.entity.Room;
import br.edu.chat.chatzao.entity.User;
import br.edu.chat.chatzao.util.Constants;
import br.edu.chat.chatzao.util.Util;

public class LoginUI {

	/**
	 * Interface gráfica
	 */
	private JFrame frame;
	private JTextField textName;
	private JTextArea textAbout;
	
	/**
	 * Informações
	 */
	private List<Room> rooms;
	private User user;
	
	/**
	 * Create the application.
	 */
	public LoginUI(List<Room> rooms) {
		setRooms(rooms);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setTitle(Constants.TITLE_PROJECT);
		Config.closeConfig(getFrame(), getRooms());
		
		getFrame().setResizable(false);
		getFrame().setBounds(100, 100, 450, 300);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);
		
		setTextName(new JTextField());
		getTextName().setBounds(10, 74, 413, 28);
		getFrame().getContentPane().add(getTextName());
		getTextName().setColumns(10);
		
		JLabel lblName = new JLabel(Constants.LBL_NAME);
		lblName.setBounds(10, 49, 413, 14);
		getFrame().getContentPane().add(lblName);
		
		JButton btnEnter = new JButton(Constants.BTN_ENTER);
		btnEnter.setBounds(291, 237, 143, 23);
		btnEnter.addActionListener((ActionEvent e) -> login());
		getFrame().getContentPane().add(btnEnter);
		
		JLabel lblTitle = new JLabel(Constants.getText(Constants.LBL_LOGIN, Constants.TITLE_PROJECT));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(10, 11, 413, 28);
		getFrame().getContentPane().add(lblTitle);
		
		JLabel lblAbout = new JLabel(Constants.LBL_ABOUT);
		lblAbout.setBounds(10, 113, 413, 14);
		getFrame().getContentPane().add(lblAbout);
		
		JScrollPane scroll = new JScrollPane();
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setBounds(10, 138, 424, 88);
		getFrame().getContentPane().add(scroll);
		
		setTextAbout(new JTextArea());
		scroll.setViewportView(getTextAbout());
		
		setVisible(true);
	}
	
	private void login() {
		setUser(new User(getTextName().getText(), getTextAbout().getText()));
		if (validate(getUser())) {
			new RoomUI(getUser(), getRooms());
			setVisible(false);
		} else {
			Util.showMessageWarning(getFrame(), Constants.WARN_VALID_NAME);
		}
	}
	
	private boolean validate(User user) {
		return user.getName() != null
			&& user.getName().compareTo("") != 0;
	}
	
	public void setVisible(boolean visible) {
		if (this.frame != null) {
			this.frame.setVisible(visible);
		}
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTextField getTextName() {
		return textName;
	}

	public void setTextName(JTextField textName) {
		this.textName = textName;
	}
	
	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public JTextArea getTextAbout() {
		return textAbout;
	}

	public void setTextAbout(JTextArea textAbout) {
		this.textAbout = textAbout;
		textAbout.setLineWrap(true);
	}
	
}
