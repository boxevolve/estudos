package br.edu.chat.chatzao.config;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.protocols.UDP;
import org.jgroups.stack.ProtocolStack;
import org.jgroups.util.ExtendedUUID;

import br.edu.chat.chatzao.entity.Room;
import br.edu.chat.chatzao.ui.ChatUI;
import br.edu.chat.chatzao.ui.RoomUI;
import br.edu.chat.chatzao.util.Constants;
import br.edu.chat.chatzao.util.Util;

public class Config {
	
	private Config() {
		super();
	}
	
	public static void connect(ChatUI chat) throws Exception {
		connect(chat.getRoom(), chat, null);
	}
	
	public static void connect(Room room, ChatUI chat, RoomUI roomUI) throws Exception {
		if (isConnected(room)) {
			return;
		}
		if (isClosed(room)) {
			room.setChannel(new JChannel());
		}
		
		ProtocolStack stack = room.getChannel().getProtocolStack();
		final UDP udp = stack.findProtocol(UDP.class);
		room.getChannel().addAddressGenerator(() -> {
			ExtendedUUID extendedAddress=ExtendedUUID.randomUUID();
			extendedAddress.put(room.getName().replace(" ", ""), udp.getBindAddress().getAddress());
	     	return extendedAddress;
		});
		
		room.getChannel().setReceiver(new ReceiverAdapter() {
			@Override
            public void receive(Message msg) {
				String message = msg.getObject().toString();
				if (!createNewRoom(message, roomUI) && chat != null) {
					chat.setDisplay(msg.getObject().toString());
				}
            }
        });

		room.getChannel().connect(room.getName());
	}
	
	private static boolean createNewRoom(String message, RoomUI room) {
		if (room == null) {
			return false;
		}
		int sizeCommand = Constants.CREATE_NEW_ROOM.length();
		String command = message.length() >= sizeCommand ? message.substring(0, sizeCommand) : "";
		
		if (command.compareToIgnoreCase(Constants.CREATE_NEW_ROOM) == 0) {
			room.addRoom(message.substring(command.length(), message.length()));
			return true;
		}
		return false;
	}
	
	public static void close(Room room) {
		if (channelValid(room)) {
			room.getChannel().close();
		}
	}
	
	public static void send(ChatUI chat, String message) {
		if (channelValid(chat.getRoom())) {
			try {
				StringBuilder sendMessage = new StringBuilder();
				sendMessage.append(Util.getDate());
				sendMessage.append(" - ");
				sendMessage.append(chat.getUser().getName());
				sendMessage.append(":\n");
				sendMessage.append(message);
				
				chat.getRoom().getChannel().send(new Message(null, sendMessage.toString()));
			} catch (Exception e) {
				Util.showMessageError(chat.getFrame(), Util.getException(Constants.ERROR_SEND_MESSAGE, e.getMessage()));
			}
		}
	}
	
	public static void closeConfig(JFrame frame, List<Room> rooms) {
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (Util.showConfirmDialog(frame, Constants.BTN_EXIT, Constants.getText(Constants.MSG_EXIT, Constants.TITLE_PROJECT))) {
					for (Room room : rooms) {
						room.getChannel().close();
					}
					System.exit(0);
				}
			}
		});
	}
	
	public static boolean isConnected(Room room) {
		return channelValid(room) && room.getChannel().isConnected();
	}
	
	public static boolean isClosed(Room room) {
		return channelValid(room) && room.getChannel().isClosed();
	}
	
	public static boolean channelValid(Room room) {
		return room != null && room.getChannel() != null;
	}
	
}
