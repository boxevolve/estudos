package br.edu.chat.chatzao;

import java.util.ArrayList;
import java.util.List;

import org.jgroups.JChannel;

import br.edu.chat.chatzao.entity.Room;
import br.edu.chat.chatzao.ui.LoginUI;
import br.edu.chat.chatzao.util.Constants;
import br.edu.chat.chatzao.util.Util;

public class JGroupsCluster {
	
	/**
	 * Classes de interface
	 */
	private static LoginUI login;

	public static void main(String[] args) throws Exception {
		List<Room> rooms = new ArrayList<>();
		rooms.add(new Room("Exemplo 1", new JChannel()));
		rooms.add(new Room("Exemplo 2", new JChannel()));
		
		try {
			setLogin(new LoginUI(rooms));
		} catch (Exception e) {
			Util.showMessageError(null, Util.getException(Constants.ERROR_INIT, e.getMessage()));
		}
	}

	public static LoginUI getLogin() {
		return login;
	}

	public static void setLogin(LoginUI login) {
		JGroupsCluster.login = login;
	}
	
}

