package br.edu.chat.chatzao.util;

import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import br.edu.chat.chatzao.entity.Room;

public class Util {
	
	private Util() {
		super();
	}
	
	public static String getDate() {
		SimpleDateFormat sdff = new SimpleDateFormat(Constants.DATE_FORMAT);
		return sdff.format(new Date());
	}

	public static boolean isValidRoom(List<Room> rooms, String name) {
		if (name.compareTo(Constants.NAME_CONNECTION_SYSTEM) == 0) {
			return false;
		}
		List<Room> result = rooms.stream()
                			  .filter(x -> x.getName().compareTo(name) == 0)
                			  .collect(Collectors.toList());
		return result.isEmpty();
	}
	
	public static Room findRoom(List<Room> rooms, Object button) {
		Room room = new Room();
		for (int j = 0; j < rooms.size(); j++) {
			if (rooms.get(j).getButton().equals(button)) {
				room = rooms.get(j);
			}
		}
		return room;
	}
	
	public static String getException(String message, String e) {
		StringBuilder exception = new StringBuilder();
		exception.append(message);
		exception.append("\n");
		exception.append(e);
		return exception.toString();
	}
	
	public static boolean showConfirmDialog(Component parent, String title, String message) {
		int option = JOptionPane.showConfirmDialog(parent, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		return option == JOptionPane.YES_OPTION;
	}
	
	public static boolean showMessageWarning(Component parent, String message) {
		int option = JOptionPane.showConfirmDialog(parent, message, Constants.ATTENTION, JOptionPane.PLAIN_MESSAGE, JOptionPane.WARNING_MESSAGE);
		return option == JOptionPane.OK_OPTION;
	}
	
	public static boolean showMessageError(Component parent, String message) {
		int option = JOptionPane.showConfirmDialog(parent, message, Constants.ATTENTION, JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);
		return option == JOptionPane.OK_OPTION;
	}
	
}
