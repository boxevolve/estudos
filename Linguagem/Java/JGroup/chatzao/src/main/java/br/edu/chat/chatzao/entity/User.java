package br.edu.chat.chatzao.entity;

public class User {
	
	private String name;
	private String about;
	
	public User() {
		super();
	}
	
	public User(String name, String about) {
		this.name = name;
		this.about = about;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}
	
}
