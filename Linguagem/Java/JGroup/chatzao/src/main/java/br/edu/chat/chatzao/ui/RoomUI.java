package br.edu.chat.chatzao.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import org.jgroups.JChannel;
import org.jgroups.Message;

import br.edu.chat.chatzao.config.Config;
import br.edu.chat.chatzao.entity.Room;
import br.edu.chat.chatzao.entity.User;
import br.edu.chat.chatzao.util.Constants;
import br.edu.chat.chatzao.util.Util;

public class RoomUI {
	
	/**
	 * Interface gráfica
	 */
	private JFrame mainFrame;
	private JPanel panel = new JPanel();
	
	/**
	 * Informações
	 */
	private List<Room> rooms;
	private User user;
	private Room system;
	
	/**
	 * Auxiliares
	 */
	private int positionY = 0;

	/**
	 * Create the application.
	 */
	public RoomUI(User user, List<Room> rooms) {
		setRooms(rooms);
		setUser(user);
		systemConfig();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setMainFrame(new JFrame());
		getMainFrame().setTitle(getTitle());
		getMainFrame().getContentPane().setLayout(new BorderLayout());
		
		Config.closeConfig(getMainFrame(), getRooms());
		loadDefault();
	    
	    JScrollPane scrollPane = new JScrollPane(getPanel());
	    scrollPane.setVerticalScrollBarPolicy (ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    
	    JPanel panelTop = new JPanel();
	    getMainFrame().getContentPane().add(panelTop, BorderLayout.NORTH);
	    
	    JButton btnNewroom = new JButton(Constants.BTN_NEW_ROOM);
	    btnNewroom.addActionListener((ActionEvent e) -> newRoom());
	    panelTop.add(btnNewroom);
	    
	    getMainFrame().getContentPane().add(scrollPane, BorderLayout.CENTER);
	    getMainFrame().setPreferredSize(new Dimension(500, 500));
	    getMainFrame().setResizable(false);
	    update();
	    setVisible(true);
	}
	
	private void loadDefault() {
		for (int i = 0; i < roomSize(); i++) {
	    	addRoom(getRooms().get(i).getName(), i);
	    }
	}
	
	public void addRoom(String name) {
		try {
			getRooms().add(new Room(name.trim(), new JChannel()));
			addRoom(name.trim(), roomSize() - 1);
		} catch (Exception e) {
			Util.showMessageError(getMainFrame(), Util.getException(Constants.ERROR_CREATE_NEW_ROOM, e.getMessage()));
		}
	}

	private void addRoom(String name, int i) {
		JButton btn = new JButton(name);
		btn.addActionListener((ActionEvent e) -> {
			Room room = Util.findRoom(getRooms(), e.getSource());
			if (!Config.isConnected(room)) {
				try {
					ChatUI chat = new ChatUI(getUser(), room);
					Config.connect(chat);
				} catch (Exception ex) {
					Util.showMessageError(getMainFrame(), Util.getException(Constants.ERROR_CREATE_NEW_ROOM, ex.getMessage()));
				}
			}
		});
		btn.setBounds(0, getPositionY(), 465, 20);
		getPanel().add(btn);
		getRooms().get(i).setButton(btn);
		setPositionY(getPositionY() + 20);
		getPanel().setLayout(new GridLayout(roomSize() < 18 ? 18 : roomSize(), 1));
		update();
	}
	
	private void newRoom() {
		String name = null;
		while (name == null || name.equals("") || !Util.isValidRoom(getRooms(), name)) {
			name = JOptionPane.showInputDialog(Constants.MSG_NAME_ROOM);
			if (name.equals("")) {
				JOptionPane.showMessageDialog(getMainFrame(), Constants.WARN_VALID_NAME_ROOM);
			}
			if (!Util.isValidRoom(getRooms(), name)) {
				JOptionPane.showMessageDialog(getMainFrame(), Constants.WARN_DUPLICATED_NAME_ROOM);
			}
		}
		sendCommand(Constants.CREATE_NEW_ROOM, " " + name);
	}
	
	public void systemConfig() {
		try {
			setSystem(new Room(Constants.NAME_CONNECTION_SYSTEM, new JChannel()));
			Config.connect(getSystem(), null, this);
		} catch (Exception e) {
			Util.showMessageError(getMainFrame(), Util.getException(Constants.ERROR_CREATE_CONNECTION_SYSTEM, e.getMessage()));
		}
	}
	
	private void sendCommand(String command, String complement) {
		try {
			StringBuilder commandSb = new StringBuilder();
			commandSb.append(command);
			commandSb.append(" ");
			commandSb.append(complement);
			getSystem().getChannel().send(new Message(null, commandSb.toString()));
		} catch (Exception e) {
			Util.showMessageError(getMainFrame(), Util.getException(Constants.ERROR_SEND_COMMAND, e.getMessage()));
		}
	}
	
	private String getTitle() {
		StringBuilder title = new StringBuilder();
		title.append(Constants.TITLE_PROJECT);
		title.append(" - ");
		title.append(user.getName());
		return title.toString();
	}
	
	public void setVisible(boolean visible) {
		if (this.mainFrame != null) {
			this.mainFrame.setVisible(visible);
		}
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public JFrame getMainFrame() {
		return mainFrame;
	}

	public void setMainFrame(JFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}
	
	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
	
	public int roomSize() {
		return  rooms != null ? rooms.size() : 0;
	}
	
	public void update() {
		getMainFrame().pack();
	}

	public Room getSystem() {
		return system;
	}

	public void setSystem(Room system) {
		this.system = system;
	}
	
}
