package br.edu.chat.chatzao.util;

public class Constants {
	
	private Constants() {
		super();
	}
	
	/**
	 * Padrões
	 */
	public static final String TITLE_PROJECT = "Chatzão";
	public static final String NAME_CONNECTION_SYSTEM = "SystemConfig";
	public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
	
	/**
	 * Títulos
	 */
	public static final String ATTENTION = "Anteção!";
	
	/**
	 * Mensagens
	 */
	public static final String MSG_NAME_ROOM = "Informe o nome da sala:";
	public static final String MSG_EXIT = "Deseja realmente sair do {0}?";
	public static final String WARN_VALID_NAME_ROOM = "Por favor informe um nome válido.";
	public static final String WARN_DUPLICATED_NAME_ROOM = "Já existe uma sala com esse nome.";
	public static final String WARN_VALID_NAME = "Informe um nome válido!";
	public static final String ERROR_CREATE_NEW_ROOM = "Ocorreu um erro ao tentar criar uma nova sala!";
	public static final String ERROR_ADD_NEW_ROOM = "Ocorreu um erro ao tentar adicionar uma nova sala!";
	public static final String ERROR_CREATE_CONNECTION_SYSTEM = "Não foi possível criar conexão do sistema!";
	public static final String ERROR_SEND_COMMAND = "Não foi possível enviar o comando para o sistema!";
	public static final String ERROR_INIT = "Ocorreu um erro inesperado ao iniciar a aplicação!";
	public static final String ERROR_SEND_MESSAGE = "Ocorreu um erro inesperado ao enviar a mensagem!";
	
	/**
	 * Labels
	 */
	public static final String LBL_LOGIN = "Bem Vindo ao {0}";
	public static final String LBL_NAME = "Informe seu nome:";
	public static final String LBL_ABOUT = "Uma breve descrição sobre você:";
	
	/**
	 * Botões
	 */
	public static final String BTN_NEW_ROOM = "Criar Sala";
	public static final String BTN_EXIT = "Sair";
	public static final String BTN_SEND = "Enviar";
	public static final String BTN_ENTER = "Entrar";
	
	/**
	 * Comandos
	 */
	public static final String CREATE_NEW_ROOM = "create new room";
	
	/**
	 * Métodos
	 */
	public static String getText(String text, String... variables) {
		String result = text;
		if (variables == null) {
			return result;
		}
		for (int i = 0; i < variables.length; i++) {
			result = text.replace("{" + i + "}", variables[i]);
		}
		return result;
	}
	
}
