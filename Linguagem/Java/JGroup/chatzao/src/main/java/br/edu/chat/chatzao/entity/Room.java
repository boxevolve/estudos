package br.edu.chat.chatzao.entity;

import javax.swing.JButton;

import org.jgroups.JChannel;

public class Room {
	
	private String name;
	private JChannel channel;
	private JButton button;
	
	public Room() {
		super();
	}
	
	public Room(String name, JChannel channel) {
		this.name = name;
		this.channel = channel;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public JChannel getChannel() {
		return channel;
	}
	
	public void setChannel(JChannel channel) {
		this.channel = channel;
	}

	public JButton getButton() {
		return button;
	}

	public void setButton(JButton button) {
		this.button = button;
	}
	
}
