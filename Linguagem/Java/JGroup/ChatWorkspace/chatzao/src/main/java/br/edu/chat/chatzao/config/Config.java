package br.edu.chat.chatzao.config;

import org.jgroups.Address;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.protocols.UDP;
import org.jgroups.stack.AddressGenerator;
import org.jgroups.stack.ProtocolStack;
import org.jgroups.util.ExtendedUUID;
import org.jgroups.util.Util;

import br.edu.chat.chatzao.ui.ChatUI;

public class Config {
	
	private Config() {
		super();
	}
	
	public static void connect(ChatUI chat) throws Exception {
		if (chat.getRoom().getChannel().isConnected()) {
			return;
		}
		
		ProtocolStack stack = chat.getRoom().getChannel().getProtocolStack();
		final UDP udp = (UDP) stack.findProtocol(UDP.class);
		chat.getRoom().getChannel().addAddressGenerator(new AddressGenerator() {	
			@Override
			public Address generateAddress() {
				ExtendedUUID extendedAddress=ExtendedUUID.randomUUID();
				extendedAddress.put(chat.getRoom().getName(), udp.getBindAddress().getAddress());
	            return extendedAddress;
            }
		});
		
		chat.getRoom().getChannel().setReceiver(new ReceiverAdapter() {
			@Override
            public void receive(Message msg) {
				Address sourceAddress = msg.getSrc();
				byte[] ipAsByteArray =((ExtendedUUID)sourceAddress).get(chat.getRoom().getName());
				String str=Util.bytesToString(ipAsByteArray);
				
                System.out.println("received msg from "
                  + msg.getSrc() + ": " + msg.getObject());
                chat.setDisplay(msg.getSrc() + ": " + msg.getObject());
            }
        });

		chat.getRoom().getChannel().connect(chat.getRoom().getName());
	}
	
}
