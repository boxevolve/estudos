package br.edu.chat.chatzao.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;

import br.edu.chat.chatzao.config.Config;
import br.edu.chat.chatzao.entity.Room;

public class RoomUI {

	public JFrame frame;
	private JTextArea display = new JTextArea();
	
	public List<Room> rooms;

	/**
	 * Create the application.
	 */
	public RoomUI(List<Room> rooms) {
		initialize(rooms);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(List<Room> rooms) {
		frame = new JFrame();
	    JPanel panel = new JPanel();
	    for (int i = 0; i < rooms.size(); i++) {
	    	JButton btn = new JButton(rooms.get(i).getName() + i);
	    	btn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Room room = new Room();
					for (int j = 0; j < rooms.size(); j++) {
						if (rooms.get(j).getButton().equals(e.getSource())) {
							room = rooms.get(j);
						}
					}
					try {
						ChatUI chat = new ChatUI();
						chat.frame.setVisible(true);
						chat.setChannel(room.getChannel());
						chat.setRoom(room);
						Config.connect(chat);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			});
	        panel.add(btn);
	        rooms.get(i).setButton(btn);
	    }

	    JScrollPane scrollPane = new JScrollPane(panel);
	    panel.setLayout(new GridLayout(0, 1, 0, 0));
	    frame.getContentPane().setLayout(new BorderLayout());
	    scrollPane.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
	    frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
	    frame.setPreferredSize(new Dimension(500, 500));
	    frame.pack();
	    frame.setVisible(true);
	}

}
