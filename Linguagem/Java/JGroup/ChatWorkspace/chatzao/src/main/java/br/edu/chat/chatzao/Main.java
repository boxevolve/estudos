package br.edu.chat.chatzao;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Main extends JFrame {

	private static final long serialVersionUID = 7070156170526428375L;

	JTextArea txtMain;

	Main() {
		setSize(500, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		JScrollPane pane = new JScrollPane();
		txtMain = new JTextArea();
		pane.setViewportView(txtMain);
		this.add(pane, BorderLayout.CENTER);

		JButton btnAddText = new JButton("Add Text");
		btnAddText.addActionListener(e -> {
			txtMain.setText(txtMain.getText()
					+ "\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis id nibh vel rhoncus. ");
			String text = txtMain.getText();
			txtMain.setCaretPosition(text != null ? text.length() : 0);
		});
		add(btnAddText, BorderLayout.SOUTH);
		setVisible(true);
	}
}
