var router = require('./router');

var port = "8080"

var app = router(port);

var contatos = [
				{nome: "Pedro", telefone: "(16)3365-4157", data: new Date(), cor: "blue", operadora: {nome: "Oi", codigo: 14, categoria: "Celular"}, serial: "%$#¨&$%#@$"},
				{nome: "Ana", telefone: "(16)1547-4578", data: new Date(), cor: "red", operadora: {nome: "Vivo", codigo: 15, categoria: "Celular"}, serial: "*¨%$¨@%&!#"},
				{nome: "Maria", telefone: "(14)6548-2365", data: new Date(), cor: "green", operadora: {nome: "Tim", codigo: 41, categoria: "Celular"}, serial: "(*)&¨%&$#%"}
			];

var operadoras = [
				{nome: "Oi", codigo: 14, categoria: "Celular", preco: 2},
				{nome: "Vivo", codigo: 15, categoria: "Celular", preco: 1},
				{nome: "Tim", codigo: 41, categoria: "Celular", preco: 3},
				{nome: "GVT", codigo: 25, categoria: "Fixo", preco: 1},
				{nome: "Embratel", codigo: 21, categoria: "Fixo", preco: 2}
			];

var bibliotecas = [
			      {codigo: 19906, sigla: 'BLI', nome: 'Chuck Noia', tipo: 'Biblioteca central', localDeOferta: '19906'},
			      {codigo: 19805, sigla: 'BB', nome: 'Matias Sbrobows', tipo: 'Biblioteca setorial', localDeOferta: '19805'}
			    ];

app.interceptor(function (req,res,next){
	res.setHeader('Access-Control-Allow-Origin','*');
	res.setHeader('Access-Control-Allow-Headers','Content-Type');
	next();
});

app.interceptor(function (req, res, next){
	res.setHeader('Content-Type', 'application/json;charset=UTF-8');
	next();
});

app.get('/ies/biblioteca/list', function (req,res){
	res.write(JSON.stringify(bibliotecas));
	res.end();
});

app.get('/contatos', function (req,res){
	res.write(JSON.stringify(contatos));
	res.end();
});

app.post('/contatos', function (req,res){
	var contato = req.body;
	contatos.push(JSON.parse(contato));
	res.end()
});

app.options('/contatos', function(req,res){
	res.end();
});

app.post('/delete', function (req,res){
	var contato = req.body;
	contatos = JSON.parse(contato);
	res.end()
});

app.options('/delete', function(req,res){
	res.end();
});

console.log('Backend iniciado!')