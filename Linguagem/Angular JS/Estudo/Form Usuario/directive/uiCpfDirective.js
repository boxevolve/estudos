angular.module("cadUsuario").directive("uiCpf", function ($filter) {
	return {
		require: "ngModel",
		link: function (scope, element, attrs, ctrl) {
			var _formatCpf = function (cpf) {
				if (!cpf) return cpf;
				cpf = cpf.replace(/[^0-9]+/g, "");
				if(cpf.length > 3) {
					cpf = cpf.substring(0,3) + "." + cpf.substring(3);
				}
				if(cpf.length > 7) {
					cpf = cpf.substring(0,7) + "." + cpf.substring(7,9);
				}
				if(cpf.length > 10){
					cpf = cpf.substring(0,10) + "-" + cpf.substring(10,12);
				}
				return cpf;
			};

			element.bind("keyup", function () {
				ctrl.$setViewValue(_formatCpf(ctrl.$viewValue));
				ctrl.$render();
			});
		}
	};
});