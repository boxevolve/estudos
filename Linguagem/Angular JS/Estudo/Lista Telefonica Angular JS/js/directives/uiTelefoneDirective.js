angular.module("listaTelefonica").directive("uiTelefone", function($http){
	return{
		require: "ngModel",
		link: function(scope, element, attrs, ctrl){
			var _formatTelefone = function(tel){
				if(!tel) return tel;
				tel = tel.replace(/[^0-9]+/g, "");

				if(tel.length > 1){
					tel = "(" + tel;
				}
				if(tel.length > 3){
					tel = tel.substring(0,3) + ") " + tel.substring(3);
				}
				if(tel.length > 9){
					tel = tel.substring(0,9) + "-" + tel.substring(9);
				}
				if(tel.length < 14){
					tel = tel.replace(".", "");
				}
				if(tel.length > 14){
					tel = tel.replace("-", "");
					tel = tel.substring(0,10) + "-" + tel.substring(10,14);
				}
				return tel;
			};
			element.bind("keyup", function () {
				ctrl.$setViewValue(_formatTelefone(ctrl.$viewValue));
				ctrl.$render();
			});
		}
	}
});