angular.module("listaTelefonica").directive("uiAlert", function (){
	return{
		template: '<div class="ui-alert" >'
						+ '<div class="ui-alert-title">'
							+ '{{title}}'
						+ '</div>'
						+ '<div class="ui-alert-message">'
							+ '{{message}}'
						+ '</div>'
				+ '</div>',
		replace: true,
		restrict: "AE",
		scope: {
			title: "@",
			message: "="
		}
	};
});