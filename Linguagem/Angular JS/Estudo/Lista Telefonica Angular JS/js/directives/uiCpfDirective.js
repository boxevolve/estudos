angular.module("listaTelefonica").directive("uiCpf", function ($filter) {
	return {
		require: "ngModel",
		link: function (scope, element, attrs, ctrl) {
			var _formatCpf = function (cpf) {
				if (!cpf) return cpf;
				cpf = cpf.replace(/[^0-9]+/g, "");

				if(cpf.length > 3) {
					cpf = cpf.substring(0,3) + "." + cpf.substring(3);
				}
				if(cpf.length > 7) {
					cpf = cpf.substring(0,7) + "." + cpf.substring(7);
				}
				if(cpf.length > 11){
					cpf = cpf.substring(0,11) + "-" + cpf.substring(11,13);
				}

				if(cpf.length === 14){
					if(!verificaCPF(cpf)){
						swal({
							title: 'Oops ...',
							text: 'CPF Inválido!',
							type: 'error',
							showCancelButton: false,
							confirmButtonText: 'OK!',
						}).then(function() {})
						return "";
					}
				}
				return cpf;
			};

			var verificaCPF = function(cpf){
				var sum = 0;
		        var remainder;

		        // Elimina CPFs invalidos conhecidos
			    if (cpf == "00000000000" || 
			        cpf == "11111111111" || 
			        cpf == "22222222222" || 
			        cpf == "33333333333" || 
			        cpf == "44444444444" || 
			        cpf == "55555555555" || 
			        cpf == "66666666666" || 
			        cpf == "77777777777" || 
			        cpf == "88888888888" || 
			        cpf == "99999999999" ||
			        cpf == "01234567890")
			        return false;

		        cpf = cpf.replace('.', '')
		            .replace('.', '')
		            .replace('-', '')
		            .trim();

		        var allEqual = true;
		        for (var i = 0; i < cpf.length - 1; i++) {
		            if (cpf[i] != cpf[i + 1])
		                allEqual = false;
		        }
		        if (allEqual)
		            return false;

		        for (i = 1; i <= 9; i++)
		            sum = sum + parseInt(cpf.substring(i - 1, i)) * (11 - i);
		        remainder = (sum * 10) % 11;

		        if ((remainder == 10) || (remainder == 11))
		            remainder = 0;
		        if (remainder != parseInt(cpf.substring(9, 10)))
		            return false;

		        sum = 0;
		        for (i = 1; i <= 10; i++)
		            sum = sum + parseInt(cpf.substring(i - 1, i)) * (12 - i); remainder = (sum * 10) % 11;

		        if ((remainder == 10) || (remainder == 11))
		            remainder = 0;
		        if (remainder != parseInt(cpf.substring(10, 11)))
		            return false;

		        return true;
			}

			element.bind("keyup", function () {
				ctrl.$setViewValue(_formatCpf(ctrl.$viewValue));
				ctrl.$render();
			});
		}
	};
});