angular.module("listaTelefonica").directive("uiName", function ($filter) {
	return {
		require: "ngModel",
		link: function (scope, element, attrs, ctrl) {
			var _formatName = function (name) {
				if (!name) return name;

				var input = name.replace(/[^a-z,A-Z, ]+/g, "");

				var notin = ["da", "de", "do", "das", "dos"];

				for(var i = 0; i < input.length-1; i++){
					if(input[i] === " " && input[i+1] === " "){
						input = input.substring(0, input.lastIndexOf(" ")) + input.substring(input.lastIndexOf(" ")+1);
						i--;
					}
				}

				var listaDeNomes = input.split(" ");
				var listaDeNomesFormatada = listaDeNomes.map(function (nome) {
					if (notin.indexOf(nome.toLowerCase()) > -1) return nome.toLowerCase();
						return nome.charAt(0).toUpperCase() + nome.substring(1).toLowerCase();
				});
				name = listaDeNomesFormatada.join(" ");
				return name;
			};

			element.bind("keyup", function () {
				if(window.event.keyCode != 32){
					ctrl.$setViewValue(_formatName(ctrl.$viewValue));
					ctrl.$render();
				}
			});
		}
	};
});