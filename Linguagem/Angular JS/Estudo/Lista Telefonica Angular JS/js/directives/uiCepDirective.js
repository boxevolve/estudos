angular.module("listaTelefonica").directive("uiCep", function($http, $sce){
	return{
		require: "ngModel",
		link: function(scope, element, attrs, ctrl){
			var _formatCep = function(cep){
				if(!cep) return cep;
				cep = cep.replace(/[^0-9]+/g, "");

				if(cep.length > 2){
					cep = cep.substring(0,2) + "." + cep.substring(2);
				}
				if(cep.length > 6){
					cep = cep.substring(0,6) + "-" + cep.substring(6,9);
				}

				if(cep.length === 10){
					cepalt = cep.replace(".","").replace("-","");
					$http.get("http://api.postmon.com.br/v1/cep/" + cepalt).then(function(response){
						scope.contato.endereco.logradouro = response.data.logradouro;
						scope.contato.endereco.bairro = response.data.bairro;
						scope.contato.endereco.cidade = response.data.cidade;
						scope.contato.endereco.cep = response.data.cep.substring(0,2) + "." + response.data.cep.substring(2,5) + "-" + response.data.cep.substring(5);	
						//scope.contato.endereco.estado = $sce.trustAsHtml(response.data.estado_info.nome + " (" + response.data.estado + ")");
						scope.contato.endereco.estado = response.data.estado;
					}, function(){
						swal({
							title: 'Oops ...',
							text: 'CEP não existe!',
							type: 'error',
							showCancelButton: false,
							confirmButtonText: 'OK!',
						}).then(function() {})
						delete scope.contato.endereco;
					});	
				}
			return cep;
			};
			element.bind("keyup", function () {
				ctrl.$setViewValue(_formatCep(ctrl.$viewValue));
				ctrl.$render();
			});
		}
	}
});