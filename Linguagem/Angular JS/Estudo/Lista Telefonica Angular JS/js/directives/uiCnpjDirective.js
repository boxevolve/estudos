angular.module("listaTelefonica").directive("uiCnpj", function ($filter) {
	return {
		require: "ngModel",
		link: function (scope, element, attrs, ctrl) {
			var _formatCnpj = function (cnpj) {
				if (!cnpj) return cnpj;
				cnpj = cnpj.replace(/[^0-9]+/g, "");

				
				if(cnpj.length > 2) {
					cnpj = cnpj.substring(0,2) + "." + cnpj.substring(2);
				}
				if(cnpj.length > 6) {
					cnpj = cnpj.substring(0,6) + "." + cnpj.substring(6);
				}
				if(cnpj.length > 10){
					cnpj = cnpj.substring(0,10) + "/" + cnpj.substring(10);
				}
				if(cnpj.length > 15){
					cnpj = cnpj.substring(0,15) + "-" + cnpj.substring(15,17);
				}

				if(cnpj.length === 18){
					if(!verificaCNPJ(cnpj)){
						swal({
							title: 'Oops ...',
							text: 'CNPJ inválido!',
							type: 'error',
							showCancelButton: false,
							confirmButtonText: 'OK!',
						}).then(function() {})
						return "";
					}
				}
				return cnpj;
			};

			var verificaCNPJ = function(cnpj){
				cnpj = cnpj.replace(/[^\d]+/g,'');
			 
			    if(cnpj == '') return false;
			     
			    if (cnpj.length != 14)
			        return false;
			 
			    // Elimina CNPJs invalidos conhecidos
			    if (cnpj == "00000000000000" || 
			        cnpj == "11111111111111" || 
			        cnpj == "22222222222222" || 
			        cnpj == "33333333333333" || 
			        cnpj == "44444444444444" || 
			        cnpj == "55555555555555" || 
			        cnpj == "66666666666666" || 
			        cnpj == "77777777777777" || 
			        cnpj == "88888888888888" || 
			        cnpj == "99999999999999")
			        return false;
			         
			    // Valida DVs
			    tamanho = cnpj.length - 2
			    numeros = cnpj.substring(0,tamanho);
			    digitos = cnpj.substring(tamanho);
			    soma = 0;
			    pos = tamanho - 7;
			    for (i = tamanho; i >= 1; i--) {
			      soma += numeros.charAt(tamanho - i) * pos--;
			      if (pos < 2)
			            pos = 9;
			    }
			    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			    if (resultado != digitos.charAt(0))
			        return false;
			         
			    tamanho = tamanho + 1;
			    numeros = cnpj.substring(0,tamanho);
			    soma = 0;
			    pos = tamanho - 7;
			    for (i = tamanho; i >= 1; i--) {
			      soma += numeros.charAt(tamanho - i) * pos--;
			      if (pos < 2)
			            pos = 9;
			    }
			    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			    if (resultado != digitos.charAt(1))
			          return false;
			    return true;
			}

			element.bind("keyup", function () {
				ctrl.$setViewValue(_formatCnpj(ctrl.$viewValue));
				ctrl.$render();
			});
		}
	};
});