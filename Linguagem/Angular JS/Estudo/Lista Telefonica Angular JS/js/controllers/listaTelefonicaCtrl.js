angular.module("listaTelefonica").controller("listaTelefonicaCtrl",function($scope, contatosAPI, operadorasAPI, serialGenerator, $filter){
	$scope.app = "Lista Telefônica";
	$scope.contatos = [];
	$scope.operadoras = [];

	/*$http.get('http://localhost:8080/contatos').then(function (data){
		$scope.contatos = data.data;
	});*/

	var carregarContatos = function(){
		contatosAPI.getContatos().then(function (data){
			$scope.contatos = data.data;
			$scope.message = null;
		},function(error){
			$scope.message = "Aconteceu um problema ao carregar os dados! Backend não ativado!";
		});
	};

	var carregarOperadoras = function(){
		operadorasAPI.getOperadoras().then(function (data){
			$scope.operadoras = data.data;
			$scope.message = null;
		},function(error){
			$scope.message = "Aconteceu um problema ao carregar os dados! Backend não ativado!";
		});
	};

	$scope.clear = function () {
		delete $scope.contato;
		$scope.contatoForm.$setPristine();
	};

	$scope.adicionarContato = function (contato) {
		contato.serial = serialGenerator.generate();
		contatosAPI.saveContato(contato).then(function (data){
			swal({
				title: $scope.contato.nome,
				text: 'adicionado aos contatos!',
				type: 'success',
				showCancelButton: false,
				confirmButtonText: 'OK!',
			}).then(function() {})
				delete $scope.contato;
				$scope.contatoForm.$setPristine();
				carregarContatos();
		});
	};

	$scope.apagarContatos = function(contatos){
		var count = 0;

		$scope.contatos = contatos.filter(function(contato){
			if(contato.selecionado) count++;
			return contato;
		});

		var aux = "";
		if(count == 1){
			aux = "este contato";
		}else{
			aux = "estes contatos";
		}

		swal({
			title: 'Deseja realmente apagar ' + aux + '?!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}).then(function() {
			$scope.contatos = contatos.filter(function(contato){
				if(!contato.selecionado) return contato;
			});
			contatosAPI.deleteContatos($scope.contatos).then(function (data){
				var aux1 = "";
				if(count == 1){
					aux1 = "Contato removido";
				}else{
					aux1 = "Contatos removidos";
				}
				swal({
					title: aux1 + ' com sucesso!',
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'OK!',
				}).then(function() {})
			});
		}, function(dismiss) {
			if (dismiss === 'cancel') {
				swal("Cancelado!", "", "error");
			}
		})
	};

	$scope.isContatoSelecionado = function(contatos){
		return contatos.some(function (contato){
			return contato.selecionado;
		});
	};

	$scope.ordenarPor = function (campo){
		$scope.criterioDeOrdenacao = campo;
		$scope.direcaoDaOrdenacao = !$scope.direcaoDaOrdenacao;
	};

	angular.element(document).ready(function () {
        carregarContatos();
		carregarOperadoras();
    });

    $scope.carregarDados = function(){
    	$scope.pessoa = "fisica";
    };

    $scope.gerarIdade = function(date){
    	if(date.length === 10){
			var dateArray = date.split("/");
			if($filter('date')(new Date(dateArray[2], dateArray[1]-1, dateArray[0]), 'dd/MM/yyyy') != date){
				$scope.contato.idade = "";
			}else{
				$scope.contato.idade = $scope.calculaIdade(dateArray[1], dateArray[0], dateArray[2]);
			}
		}else{
			$scope.contato.idade = "";
		}
    };

    $scope.calculaIdade = function(aMes, aDia, aAno){
		hData = new Date();
		hAno = hData.getFullYear();
		hMes = hData.getMonth();
		hDia = hData.getDate();
		idade = hAno - aAno; 
		if (hMes < aMes - 1){
			idade--;
		}

		if (aMes - 1 == hMes && hDia < aDia){
			idade--;
		}
		return idade;
	}

	$scope.setDados = function(contato){
		window.sessionStorage.vercontato = JSON.stringify(contato);
		$scope.vercontato = JSON.parse(window.sessionStorage.vercontato);
		$scope.carregarDados();
	}
});