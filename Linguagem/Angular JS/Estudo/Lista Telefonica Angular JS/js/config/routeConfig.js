angular.module("listaTelefonica").config(function ($routeProvider){
	$routeProvider.when("/contatos", {
		templateUrl: "view/contatos.html",
		controller: "listaTelefonicaCtrl"
	});
	$routeProvider.when("/novoContato", {
		templateUrl: "view/novoContato.html",
		controller: " listaTelefonicaCtrl",
		resolve: {
			operadoras: function (operadorasAPI) {
				return operadorasAPI.getOperadoras();
			}
		}
	});
	$routeProvider.when("/detalhesContato", {
		templateUrl: "view/detalhesContato.html",
		controller: " listaTelefonicaCtrl"
	});
	$routeProvider.otherwise({redirectTo: "/contatos"});
});