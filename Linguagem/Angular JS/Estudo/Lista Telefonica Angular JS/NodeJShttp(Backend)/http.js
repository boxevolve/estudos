var router = require('./router');

var port = "8080"

var app = router(port);

var contatos = [
				{nome: "Bruno Casimiro", telefone: "(16) 3365-4157", data: "05/04/1996", cor: "blue", operadora: {nome: "Oi", codigo: 14, categoria: "Celular"}, serial: "%$#¨&$%#@$", idade: 20, endereco:{cep: "49042-520", logradouro: "Rua Professor João Batista", bairro: "São Conrado", cidade: "Aracaju", estado: "SE", pais: "Brasil"}, cpf: "425.167.313-10", cnpj: "86.051.875/0001-38"},
				{nome: "Ana Meneguci", telefone: "(16) 1547-4578", data: "07/08/1995", cor: "pink", operadora: {nome: "Vivo", codigo: 15, categoria: "Celular"}, serial: "*¨%$¨@%&!#", idade: 21, endereco:{cep: "35054-420", logradouro: "Rua Onze", bairro: "Jardim do Trevo", cidade: "Governador Valadares", estado: "MG", pais: "Brasil"}, cpf: "834.507.584-38", cnpj: "56.028.618/0001-14"},
				{nome: "Marcos Roberto", telefone: "(14) 6548-2365", data: "23/11/1994", cor: "green", operadora: {nome: "Tim", codigo: 41, categoria: "Celular"}, serial: "(*)&¨%&$#%", idade: 22, endereco:{cep: "59142-512", logradouro: "Rua Saturno", bairro: "Bela Parnamirim", cidade: "Parnamirim", estado: "RN", pais: "Brasil"}, cpf: "572.549.918-97", cnpj: "31.897.238/0001-95"}
			];

var operadoras = [
				{nome: "Oi", codigo: 14, categoria: "Celular", preco: 2},
				{nome: "Vivo", codigo: 15, categoria: "Celular", preco: 1},
				{nome: "Tim", codigo: 41, categoria: "Celular", preco: 3},
				{nome: "GVT", codigo: 25, categoria: "Fixo", preco: 1},
				{nome: "Embratel", codigo: 21, categoria: "Fixo", preco: 2}
			];

app.interceptor(function (req,res,next){
	res.setHeader('Access-Control-Allow-Origin','*');
	res.setHeader('Access-Control-Allow-Headers','Content-Type');
	next();
});

app.interceptor(function (req, res, next){
	res.setHeader('Content-Type', 'application/json;charset=UTF-8');
	next();
});

app.get('/operadoras', function (req,res){
	res.write(JSON.stringify(operadoras));
	res.end();
});

app.get('/contatos', function (req,res){
	res.write(JSON.stringify(contatos));
	res.end();
});

app.post('/contatos', function (req,res){
	var contato = req.body;
	contatos.push(JSON.parse(contato));
	res.end()
});

app.options('/contatos', function(req,res){
	res.end();
});

app.post('/delete', function (req,res){
	var contato = req.body;
	contatos = JSON.parse(contato);
	res.end()
});

app.options('/delete', function(req,res){
	res.end();
});

console.log('Backend iniciado!')