#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct aluno {
	char nome[81];
	char materia[10];
	char turma[10];
	char email[41];
};
typedef struct aluno Aluno;

Aluno * criarAluno() {
	Aluno * novo = (Aluno *) malloc(sizeof(Aluno));
	printf("Informe o nome: ");
	gets(novo -> nome);
	printf("Informe a materia: ");
	gets(novo -> materia);
	printf("Informe a turma: ");
	gets(novo -> turma);
	printf("Informe o e-mail: ");
	gets(novo -> email);
	
	return novo;
}

void borda(int tamanho) {
	int i;
	printf("+");
	for(i=0; i<tamanho; i++) {
		printf("-");
	}
	printf("+\n");
}

void pipe(int tamanho, int tamanhoMax) {
	int i;
	for(i=tamanho; i<tamanhoMax; i++) {
		printf(" ");
	}
	printf("|\n");
}

void imprimirAluno(Aluno** aluno, int quantidade) {
	int i, j;
	for(i=0; i<quantidade; i++) {
		borda(82);
		printf("| Nome: %s", aluno[i]->nome); pipe(8+strlen(aluno[i]->nome),83);
		printf("| Materia: %s", aluno[i]->materia); pipe(11+strlen(aluno[i]->materia),83);
		printf("| Turma: %s", aluno[i]->turma); pipe(9+strlen(aluno[i]->turma),83);
		printf("| E-Mail: %s", aluno[i]->email); pipe(10+strlen(aluno[i]->email),83);
		borda(82);
	}
}

int comparaAlunos(const void *aluno1, const void *aluno2) {
	const Aluno *alunoAux1 = aluno1;
	const Aluno *alunoAux2 = aluno2;
	
	const char **nomeAluno1 = (const char **) alunoAux1->nome;
	const char **nomeAluno2 = (const char **) alunoAux2->nome;
	
	return strcmp(*nomeAluno1, *nomeAluno2);
}

int main () {
	int quantidade, i, opcao;
	
	printf("Qual o tamanho do vetor de aluno que gostaria de trabalhar? ");
	scanf("%d", &quantidade);
	fflush(stdin);
	Aluno * vetorAluno[quantidade];
	do{
		printf("\n[Tamanho vetor utilizado [%d] ]\n",quantidade);
		borda(50);
		printf("| .:MENU:."); pipe(10,51);
		printf("| [1] Inserir alunos"); pipe(20,51);
		printf("| [2] Imprimir alunos"); pipe(21,51);
		printf("| [3] Ordenar usando QuickSort"); pipe(30,51);
		printf("| [0] Sair"); pipe(10,51);
		borda(50);
		printf("Opcao: ");
		scanf("%d", &opcao);
		fflush(stdin);
		
		switch(opcao){
			case 1:
				printf("\n[INSERIR ALUNOS]\n");
				for(i = 0; i<quantidade; i++){
					printf("\nCriando aluno [%d] \n",i+1);
					vetorAluno[i] = criarAluno();
				}
				break;
			case 2:
				printf("\n[IMPRIMIR ALUNOS]\n");
				imprimirAluno(vetorAluno,quantidade);
				break;
			case 3:
				printf("\n[ORDENAR USANDO QUICKSORT]\n");
				qsort(vetorAluno, quantidade, sizeof(Aluno *), comparaAlunos);
				printf("\nAlunos ordenados por nome.\n");
				break;
			case 0:
				break;
			default:
				printf("\nOpcao invalida!\n");
		}
	}while(opcao != 0);
	
	system("PAUSE");
	return 0;
}
