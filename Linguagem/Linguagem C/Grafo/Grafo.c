#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define LINE_SIZE 7
#define COLUMN_SIZE 7

//Menu para realizar as a��es
int menu();
void executeAction();

//Limpar console
void clearConsole();

//Inicializa a matriz com valores pre definido
void initializeMatriz();

//Inicializa a matriz com valores informado
void initializeMatrizScan();

//Inicializa a matriz com 0
void initializeNewMatriz();

//copia matriz para outra matriz
void copyMatriz();

//imprime a matriz
void printMatriz();

//Verifica se tem caminho entre dois pontos e verifica o menor caminho
bool findRoute();
bool isSameRoute();
int backRoute();
void initializeVetor();
void printRoute();
void copyVetor();

void printLessRoute();

void main() {
	int option; // Vari�vel para armazenar as escolhas do usu�rio
	int matriz[LINE_SIZE][COLUMN_SIZE]; // Cria uma matriz para poder trabalhar com ela
	initializeNewMatriz(matriz); // Preenche toda a matriz com 0
	
	do{
		option = menu(matriz); // Chama a fun��o menu() para apresentar as op��es
	}while(option != 0);
	
	system("PAUSE");
}

int menu(int matriz[LINE_SIZE][COLUMN_SIZE]) {
	int option; // Vari�vel para armazenar as escolhas do usu�rio
	printf("\n+-----[ Menu [Grafo] ]-----+\n");
	printf("Matriz atual: [%d] linhas | [%d] colunas\n", LINE_SIZE, COLUMN_SIZE);
	printf("[1] - Gerar grafo com valores pre definido.\n");
	printf("[2] - Gerar grafo com valores informados.\n");
	printf("[3] - Imprimir grafo.\n");
	printf("[4] - Verificar se existe caminho entre dois pontos.\n");
	printf("[5] - Verificar o caminho mais curto entre os pontos.\n");
	printf("[0] - Sair.\n");
	printf("Digite a opcao desejada: ");
	scanf("%d", &option);
	executeAction(option, matriz); // Chama a fun��o para executar a escolha
	return option;
}

void executeAction(int option, int matriz[LINE_SIZE][COLUMN_SIZE]) {
	int origin; // Armazena o inicio da busca busca no grafo
	int destiny; // Armazena o destino da busca no grafo
	clearConsole(); // Limpa o terminal
	switch(option){
		case 0:
			printf("Saindo...\n\n");
			break;
		case 1:
			printf("\n+--[ Gerar grafo pre definido ]--+\n\n");
			initializeMatriz(matriz); // Ele preenche a matriz com dados pre definido
			printf("Grafo gerado com sucesso!\n");
			printMatriz(matriz); // Imprimi a matriz
			break;
		case 2:
			printf("\n+--[ Gerar grafo com valores informados ]--+\n\n");
			initializeMatrizScan(matriz); // Ele preenche a matriz com os dados informados pelo usu�rio
			break;
		case 3:
			printf("\n+--Imprimir grafo--+\n\n");
			printMatriz(matriz); // Imprimi a matriz
			break;
		case 4:
			printf("\n+--[ Verificar caminho entre dois pontos ]--+\n\n");
			printf("Informe a origem: ");
			scanf("%d", &origin); // Armazena o inicio da busca busca no grafo 
			printf("Informe o destino: ");
			scanf("%d", &destiny); // Armazena o destino da busca no grafo
			if(findRoute(matriz, origin, destiny, 1)){ // Busca uma rota entre a origem e o destino no grafo, 
                                                       // se passar 1 como parametro a fun��o ira retornar apenas sim ou n�o (true ou false)
				printf("\n[Existe caminho partindo de %d para %d]\n", origin, destiny); // Se retornar sim (true)
			}else{
				printf("\n[Nao existe caminho partindo de %d para %d]\n", origin, destiny); // Se retornar n�o (false)
			}
			break;
		case 5:
			printf("\n+--[ Verificar caminho mais curto entre dois pontos ]--+\n\n");
			printf("Informe a origem: ");
			scanf("%d", &origin); // Armazena o inicio da busca busca no grafo 
			printf("Informe o destino: ");
			scanf("%d", &destiny); // Armazena o destino da busca no grafo
			if(!findRoute(matriz, origin, destiny, 0)){ // Busca uma rota entre a origem e o destino no grafo, 
                                                        // se passar 0 como parametro a fun��o ira imprimir o caminho mais curto entre os pontos
				printf("[Nao existe caminho partindo de %d para %d]\n", origin, destiny); // Se n�o encontrar o ponto de destino
			}
			break;
		default:
			printf("Opcao invalida!\n");
			break;
	}
}

void clearConsole() {
	system("cls");
}

void initializeNewMatriz(int matriz[LINE_SIZE][COLUMN_SIZE]) {
	int line = 0;
	int column = 0;
	
	for(line ; line < LINE_SIZE ; line++){
        for(column = 0 ; column < COLUMN_SIZE ; column++) {
            matriz[line][column] = 0;
    	}
	}
}

void initializeMatrizScan(int matriz[LINE_SIZE][COLUMN_SIZE]) {
	int auxMatriz[LINE_SIZE][COLUMN_SIZE];
	initializeNewMatriz(auxMatriz);
	
	int route = 0;
	int line = 0;
	int amountRoute = 0;
	
	while(route != -2) {
		do {
			printMatriz(auxMatriz);
			printf("Informe os caminhos do ponto [ %d ], (-1) para ir para o proximo ponto ou (-2) para cancelar: ", line);
			scanf("%d", &route);
			clearConsole();
			if(route == -2) {
				break;
			} else if(route == -1) {
				line++;
				amountRoute = 0;
				break;
			}
			if(route == line) {
				printf("Nao pode definir caminho para o proprio ponto!\n\n");
			} else if(route < 0 || route > COLUMN_SIZE) {
				printf("Ponto [%d] nao existente!\n\n", route);
			} else {
				auxMatriz[line][route] = 1;
				amountRoute++;
			}
			if(amountRoute+1 == COLUMN_SIZE) {
				line++;
				amountRoute = 0;
				break;
			}
		} while(route < 0 || route > COLUMN_SIZE || route == line);
		if(line+1 == LINE_SIZE) {
			break;
		}
	}
	if(route != -2) {
		copyMatriz(matriz, auxMatriz);
	}
}

void initializeMatriz(int matriz[LINE_SIZE][COLUMN_SIZE]) {
	int newMatriz[LINE_SIZE][COLUMN_SIZE] = {{0,0,1,0,0,0,0},
					   						 {0,0,0,0,0,1,1},
					   						 {0,0,0,0,1,0,0},
					   						 {0,0,0,0,0,0,0},
					   						 {1,1,0,1,0,0,0},
					   						 {0,0,0,0,0,0,0},
					   						 {0,0,0,0,0,0,0}};
	copyMatriz(matriz, newMatriz);
}

void copyMatriz(int copy[LINE_SIZE][COLUMN_SIZE], int original[LINE_SIZE][COLUMN_SIZE]) {
	int line = 0;
	int column = 0;

    for(line ; line < LINE_SIZE ; line++) {
        for(column = 0 ; column < COLUMN_SIZE ; column++) {
            copy[line][column] = original[line][column];
    	}
	}
}

void printMatriz(int matriz[LINE_SIZE][COLUMN_SIZE]) {
	int lineSize = 0;
	int columnSize = 0;
	int line = 0;
	int column = 0;
	
	printf(" |");
	for(columnSize; columnSize<COLUMN_SIZE; columnSize++) {
		printf("%d|", columnSize);
	}
	printf("\n");
	for(line; line<LINE_SIZE; line++) {
		printf("%d|", lineSize);
		for(column=0; column<COLUMN_SIZE; column++) {
			printf("%d|", matriz[line][column]);
		}
		printf("\n");
		lineSize++;
	}
}

bool findRoute(int matriz[LINE_SIZE][COLUMN_SIZE], int origin, int destiny, int onlyContains) {
    if(origin < 0 || origin > (LINE_SIZE-1)) {
        return false;
    }
    if(destiny < 0 || destiny > (COLUMN_SIZE-1)) {
        return false;
    }
	int auxMatriz[LINE_SIZE][COLUMN_SIZE];
	int route[LINE_SIZE];
	initializeVetor(route);
	int routePosition = 0;
	copyMatriz(auxMatriz, matriz);
	int line = origin;
	int column = 0;
	
	int lessRoute[LINE_SIZE];
	
	bool find = false;
	int  countRoute = 0;
	route[routePosition] = origin;
	if(route[routePosition] == destiny && onlyContains == 0) {
		find = true;
		copyVetor(route, lessRoute);
		printLessRoute(lessRoute);
		return find;
	}
	do{
		if(route[routePosition] == destiny && onlyContains == 1) {
			find = true;
			break;
		}else if(route[routePosition] == destiny && onlyContains == 0) {
			find = true;
			copyVetor(route, lessRoute);
			printLessRoute(lessRoute);
		}else{
			routePosition++;
		}
		
		countRoute = 0;
		for(column = 0; column < COLUMN_SIZE; column++) {
			if(auxMatriz[line][column] == 1) {
				if(!isSameRoute(route, column)) {
					auxMatriz[line][column] = 0;
					line = column;
					route[routePosition] = line;
					break;
				} else {
					countRoute++;
				}
			} else {
				countRoute++;
			}
		}
		if(countRoute == LINE_SIZE) {
			auxMatriz[line][column] = 0;
			line = backRoute(route, routePosition);
			if(line != -1) {
				routePosition = routePosition - 2;
			} else {
				break;
			}
		}
	} while(route[0] != -1);
	return find;
}

bool isSameRoute(int route[LINE_SIZE], int next) {
	int i=0;
	for(i; i<LINE_SIZE; i++) {
		if(route[i] == next) {
			return true;
		}
	}
	return false;
}

int backRoute(int route[LINE_SIZE], int routePosition) {
	route[routePosition-1] = -1;
	int backRoute = routePosition - 2;
	if(backRoute != -1) {
		return route[backRoute];
	}
	return -1;
}

void initializeVetor(int vetor[LINE_SIZE]) {
	int i=0;
	for(i; i<LINE_SIZE; i++) {
		vetor[i] = -1;
	}
}

void copyVetor(int original[LINE_SIZE], int copy[LINE_SIZE]) {
	int column = 0;

    for(column; column < LINE_SIZE ; column++) {
        copy[column] = original[column];
    }
}

void printLessRoute(int vetor[LINE_SIZE]) {
	printf("O caminho mais curto entre os pontos eh:\n");
	printRoute(vetor);
}

void printRoute(int vetor[LINE_SIZE]) {
	int i=0;
	printf("\n");
	for(i; i<LINE_SIZE; i++) {
		if(vetor[i] != -1) {
			printf("%d", vetor[i]);
			if(vetor[i+1] != -1) {
				printf(" -> ");
			}
		}
	}
	printf("\n");
}

