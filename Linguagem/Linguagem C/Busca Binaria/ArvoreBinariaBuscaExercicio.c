#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct arv{
    int info;
    struct arv * esq;
    struct arv * dir;
};
typedef struct arv Arv;
int count = 0;

Arv * inicializa(){
    return NULL;
}

Arv * insere(Arv * pA, int v){
    if(pA == NULL){
        Arv * p = (Arv *) malloc(sizeof(Arv)); 
        p->info = v;
        p->esq = inicializa();
        p->dir = inicializa();
        return p;
    } else if(v < pA->info)
        pA->esq = insere(pA->esq, v);
    else if(v > pA->info)
        pA->dir = insere(pA->dir, v);
    return pA;
}
void imprime_ordem_simetrica(Arv * pA){
    if(pA != NULL){
        imprime_ordem_simetrica(pA->esq);
        imprime_ordem_simetrica(pA->dir); 
        printf("[ %d ]", pA->info);
    }
}

Arv * buscaBinaria(Arv * pA, int v){
    count++;
    if(pA == NULL)
        return NULL;
    else if(v < pA->info)
        return buscaBinaria(pA->esq, v);
    else if (v > pA->info)
        return buscaBinaria(pA->dir, v);
    return pA;
}

int percoreDireita(Arv * pA){
    while (pA->dir!=NULL)
        pA=pA->dir;
    return (pA->info);
}

Arv * retirar(Arv * pA, int v){
    Arv * aux;
    if(pA == NULL)
        return pA;
    else if (v < pA->info){
        pA->esq = retirar(pA->esq, v);
    } else if (v > pA->info){
        pA->dir = retirar(pA->dir, v);
    } else { //achou o elemento
    //se for o no folha
        if(pA->esq == NULL && pA->dir == NULL){
            free(pA);
            return NULL;
        }
        if(pA->esq == NULL && pA->dir != NULL){
            aux = pA->dir;
            free(pA);
            return aux;
        }
        if(pA->esq != NULL && pA->dir == NULL){
            aux = pA->esq;
            free(pA);
            return aux;
        }
        if(pA->esq != NULL && pA->dir != NULL){
            pA->info=percoreDireita(pA->esq);
            return(retirar(pA->esq, pA->info));
        }
    }
    return pA;
}

void menu(int op, Arv * pA){
    int valor;
    switch(op){
        case 0:
            printf("Saindo...\n\n");
            break;
        case 1:
            printf("\n----+ Inserir +----\n");
            printf("Digite o numero que deseja inserir:\n");
            scanf("%d", &valor);
            pA = insere(pA, valor);
            printf("\n");
            break;
        case 2:
            printf("\n----+ Buscar +----\n");
            printf("Digite o numero que deseja buscar:\n");
            scanf("%d", &valor);
            Arv *  Q = buscaBinaria(pA, valor);
            if(Q != NULL){
                printf("\nElemento [ %d ] encontrado! com [ %d ] interacoes! \n", Q->info, count);
            } else {
                printf("\nElemento nao encontrado! \n");
            }
            printf("\n");
            break;
        case 3:
            printf("\n----+ Remover +----\n");
            printf("Digite o numero para remover: \n");
            scanf("%d", &valor);
            Arv * temp = buscaBinaria(pA, valor);
            retirar(pA, valor);
            if(temp != NULL){
                printf("\nNumero removido! \n");
            } else {
                printf("\nNumero nao encontrado na arvore! \n");
            }
            printf("\n");
            break;
        default:
            printf("Opcao invalida!");
    }
}

int main(){
    Arv * A;
    int val;
    A = inicializa();

    A = insere(A,30);
    insere(A,20);
    insere(A,15);
    insere(A,55);
    insere(A,45);
    insere(A,60);
    insere(A,50);
    
    int op;
    do{
        imprime_ordem_simetrica(A);
        printf("\n----+ Opcoes +----\n");
        printf("[1] - Inserir\n");
        printf("[2] - Buscar\n");
        printf("[3] - Remover\n");
        printf("[0] - Sair\n");
        
        scanf("%d", &op);
        menu(op, A);
    } while(op != 0);
    system("PAUSE");
    return 0;
}
