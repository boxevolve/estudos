#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct arv{
int info;       
struct arv * esq;
struct arv * dir;       
};
typedef struct arv Arv;
int count=0;

Arv * inicializa(){
    return NULL;
}

//Function receives a pointer of the tree and the value
Arv * insere(Arv * pA, int v){
    if(pA == NULL){
          Arv * p = (Arv*) malloc(sizeof(Arv));
          p->info = v;
          p->esq = inicializa(); //NULL
          p->dir = inicializa(); //NULL
          return p;      
    }
    else{
         if(v < pA-> info) //Lower numbers to the left
         pA->esq = insere(pA->esq, v);
    else{
         if(v > pA->info) //Bigger numbers to the right
         pA->dir = insere(pA->dir,v); 
      
    }}
    return pA;          
}

void imprime_ordem_simetrica(Arv * pA){
     if (pA != NULL){
        imprime_ordem_simetrica(pA->esq);
        printf("%d", pA->info);
        imprime_ordem_simetrica(pA->dir);   
     }    
}

Arv * buscaBinaria(Arv * pA, int v){
	count++;
if(pA == NULL)
	return NULL;
else
	if(v < pA->info)
		return buscaBinaria(pA->esq, v);
	else
		if (v > pA->info)
			return buscaBinaria(pA->dir, v);
		return pA;
}

Arv * findMax(Arv * pA){
	if(pA->dir == NULL)
	return pA;
	
	findMax(pA->dir);
}

Arv * retirar(Arv * pA, int v){
	if(pA == NULL)
		return pA;
	else
	if(v < pA->info)
		pA->esq = retirar(pA->esq,v);
	else
		if (v > pA->info)
		pA->dir = retirar(pA->dir,v);
		else{ //Encontrou o elemento
			//Se for o n� folha
			if(pA->esq == NULL && pA->dir == NULL)
			free(pA);
			return NULL;
			else if(pA->esq == NULL){
				Arv * aux = pA->dir;
				free(pA);
				return aux;
			}
			else if(pA->dir == NULL){
				Arv * aux = pA->esq;
				free(pA);
				return aux;
			}
			else if(pA->esq != NULL && pA->dir != NULL){
				Arv * max = findMax(pa->esq);
				pA->info = max->info
				aux = pA->esq;
				free(aux);
				return pA;
				
			}
			
		}
	return pA;
}

void main(){
    
    Arv * A;
    A = inicializa();
    
    A= insere(A,3); //Represents tree and the value
       insere(A,1);
       insere(A,2);
       insere(A,4);
       insere(A,5);
       
    imprime_ordem_simetrica(A);
    Arv * Q = buscaBinaria(A,2);
    if(Q != NULL){
    	printf("\nElemento %d encontrado na %da recursao \n", Q->info, count);
	}
	else{
		printf("Elemento nao encontrado \n");
	}
system("PAUSE");
} 


