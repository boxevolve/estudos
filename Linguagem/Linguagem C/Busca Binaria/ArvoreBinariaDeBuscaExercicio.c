#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct arv{
    int info;
    struct arv * esq;
    struct arv * dir;
};
typedef struct arv Arv;

Arv * inicializa(){
    return NULL;    
}
int count = 0;

Arv * insere (Arv * pA, int v){
    if(pA == NULL){
        Arv * p = (Arv *)malloc(sizeof(Arv));
        p->info = v;
        p->esq = inicializa();
        p->dir = inicializa();
        return p;
    }else
        if(v < pA->info)
            pA->esq = insere(pA->esq, v );
        else
            if(v > pA->info)
                pA->dir = insere(pA->dir, v);
    return pA;
}

void imprime_ordem_simetrica(Arv * pA){
    if(pA != NULL){
        imprime_ordem_simetrica(pA->esq);
        printf("[ %d ]", pA->info);
        imprime_ordem_simetrica(pA->dir);
    }
}

int buscaBinaria(Arv * pA, int v){
    int retorno = 0;
    if(pA == NULL){
        return retorno;
    }else{
        if(v == pA->info){
            count ++;
            retorno = 1;
        } else if (v < pA->info){
            count ++;
            retorno = buscaBinaria(pA->esq, v );
        } else if (v > pA->info){
            count ++;
            retorno = buscaBinaria(pA->dir, v);
        }
    }
    return retorno;
}

Arv * buscaBinariaProf(Arv * pA, int v){
    count ++;
    if(pA == NULL)
          return NULL;
    else
          if(v < pA -> info)
              return buscaBinariaProf(pA -> esq, v);
          else 
               if(v > pA -> info)
                    return buscaBinariaProf(pA -> dir, v);
    return pA;
}

Arv * retirar(Arv * pA, int v){
    if(pA == NULL)
          return pA;
    else
        if(v < pA -> info )
             pA -> esq = retirar(pA -> esq, v);
        else
             if(v > pA -> info)
                  pA -> dir = retirar(pA -> dir, v);
             else{// achou o elemento
                  //se for no folha
                  if(pA -> esq == NULL && pA -> dir == NULL){
                        Arv * t = pA;
                        free(t);
                        return NULL;
                  }
             }
    return pA;
}

Arv * getSuccessor(Arv * delNo){
        Arv * successorParent = delNo;
		Arv * successor = delNo;
	    Arv * current = delNo -> dir;
		while(current != NULL){
            successorParent = successor;
			successor = current;
			current = current -> esq;
		}
		if(successor != delNo -> dir){
			successorParent -> esq = successor -> dir;
		 	successor -> dir = delNo -> dir;
		}
		return successor;
}

Arv * remover(Arv * raiz, int v){
    if(raiz == NULL){
        return raiz;
    }
    Arv * current = raiz;
    Arv * parent = raiz;
    bool isLeftChild = true;
    
    while(current -> info != v){
        parent = current;
        if(v < current -> info){
            isLeftChild = true;
            current = current -> esq;
        } else {
            isLeftChild = false;
            current = current -> dir;
        } if(current == NULL){
            return current;
        }
    }
    if(current -> esq == NULL && current -> dir == NULL){
        if(current == raiz){
            free(current);
            return NULL;
        } else if(isLeftChild) {
            free(parent -> esq);
            return NULL;
        } else {
            free(parent -> dir);
            return NULL;
        }
    } else if(current -> dir == NULL) {
        Arv * retorno;
        if(current == raiz){
            raiz = current -> esq;
            retorno = raiz;
        } else if(isLeftChild) {
            parent -> esq = current -> esq;
            retorno = parent;
        } else {
            parent -> dir = current -> esq;
            retorno = parent;
        }
        return retorno;
    } else if(current -> esq == NULL) {
        if(current == raiz) {
            raiz = current -> dir;
        } else if(isLeftChild) {
            parent -> esq = current -> dir;
        } else {
            parent -> dir = current -> dir;
        }
    } else{
        Arv * successor = getSuccessor(current);
            if(current == raiz) {
                raiz = successor;
            } else if(isLeftChild) {
                parent -> esq = successor;
            } else {
                parent -> dir = successor;
            }
            successor -> esq = current -> esq;
            return parent;
    }
    return raiz;
}

int main(){
     
    Arv * A;
    A = inicializa();
    
    A = insere(A, 3); 
    insere(A, 1);
    insere(A, 2);
    insere(A, 3);
    insere(A, 4);
    insere(A, 5);
    insere(A, 6);
    
    imprime_ordem_simetrica(A);
    
    int x = 4;
    
    Arv * Q = buscaBinariaProf(A, x);
    
    //contem a quantidade de recursao que sera realizado
    //if(buscaBinaria(A, x) == 0){
    if(Q == NULL){
       printf("\nO numero [ %d ] nao existe na arvore!", x);                
    }else{
       printf("\nO numero [ %d ] existe na arvore!", x);
    }
    printf("\nEncontrado com [ %d ] interacoes!\n\n", count);
    
    //Arv * Z = retirar(A, x);
    Arv * Z = remover(A, x);
    
    imprime_ordem_simetrica(A);
    
    system("pause");
    return 0;
}
