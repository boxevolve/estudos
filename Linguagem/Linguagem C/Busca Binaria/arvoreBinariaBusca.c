#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct arv{
	int info;
	struct arv * esq;
	struct arv * dir;
};
typedef struct arv Arv;
int count = 0;

Arv * inicializa(){
	return NULL;
}

Arv * insere(Arv * pA, int v){
	if(pA == NULL){
		Arv * p = (Arv *) malloc(sizeof(Arv));	
		p->info = v;
		p->esq = inicializa();
		p->dir = inicializa();
		return p;
	}
	else
		if(v < pA->info)
			pA->esq = insere(pA->esq, v);
		else
			if(v > pA->info)
				pA->dir = insere(pA->dir, v);
	return pA;
}
void imprime_ordem_simetrica(Arv * pA){
	if(pA != NULL){
		imprime_ordem_simetrica(pA->esq);
		imprime_ordem_simetrica(pA->dir);
		printf("%d ", pA->info);
	}
}

Arv * buscaBinaria(Arv * pA, int v){
	count++;
	if(pA == NULL)
		return NULL;
	else
		if(v < pA->info)
			return buscaBinaria(pA->esq, v);
		else
			if (v > pA->info)
				return buscaBinaria(pA->dir, v);
			return pA;
}

Arv * retirar(Arv * pA, int v){
	if(pA == NULL)
		return pA;
	else 
		if (v < pA->info)
			pA->esq = retirar(pA->esq, v);
		else
			if (v > pA->info)
				pA->dir = retirar(pA->dir, v);
			else{ //achou o elemento
				//se for o no folha
				if(pA->esq == NULL && pA->dir == NULL)
					free(pA);
					return NULL;
			}
	return pA;
}

int main(){

	Arv * A;
	A = inicializa();

	A = insere(A,3);
		insere(A,1);
		insere(A,2);
		insere(A,4);
		insere(A,5);
		insere(A,6);

	imprime_ordem_simetrica(A);
	Arv *  Q = buscaBinaria(A, 6);
	if(Q != NULL){
		printf("Elemento %d encontrado na %d ° recursao! \n", Q->info, count);
	}
	else{
		printf("Elemento não encontrado! \n");
	}
	
	system("PAUSE");
	return 0;
}
