#include<stdio.h>
#include<string.h>
#include<stdbool.h>
#define MAX 3
#define true 1
#define false 0

typedef struct no { 
    char nome[20];
    char telefone[20];
    int ocupado;
}Elemento;

void inicializaTabHash(Elemento tabHash[], int size){
  int i;
  for(i=0;i<size;i++)
   tabHash[i].ocupado = false;
}

int h(char * chave){
   int i,soma=0;
   for(i=0;i<strlen(chave);i++) {
       soma+=abs(chave[i]);
       printf("%d ",soma);
   }
   printf("\nR: %d ", soma%MAX);
   return soma%MAX;
}

//Inserindo na Tabela
void create(Elemento tabHash[], Elemento e) {
  int pos;
  pos = h(e.nome);
  if(tabHash[pos].ocupado == false) {
     tabHash[pos] = e;
     tabHash[pos].ocupado = true;
  }
  else{
     printf("Ocorreu uma colis�o! \n");
  }
}

//Inserindo na Tabela com colis�o
int create_colisao (Elemento tabHash[], Elemento e) {
    int chave = e.nome;
    int i, pos, newPos;
    pos = h(e.nome);
    for(i=0; i < MAX; i++) {
        newPos = sondagemLinear(pos, i, MAX);
        if(tabHash[newPos].ocupado == false) {
           tabHash[newPos] = e; 
           tabHash[newPos].ocupado = true;
           return 1;
        }
    }
    return 0;
}

//Recuperando um elemento:
Elemento read (Elemento  tabHash[], char *chave) {
   int pos;
   pos = h(chave);
   if(tabHash[pos].ocupado == true) {
      if((strcmp(tabHash[pos].nome, chave)==0)){
         return tabHash[pos];
      }
   }
}

//Recuperando um elemento com colis�o:
Elemento read_colisao (Elemento  tabHash[], char *chave) {
    int i, pos, newPos;
    pos = h(chave);
    for(i=0; i < MAX; i++) {
        newPos = sondagemLinear(pos, i, MAX);
        if(tabHash[newPos].ocupado == false)
            return;
        if((strcmp(tabHash[newPos].nome, chave)==0)) {
            return tabHash[newPos];
        }
    }
    return;
}

void imprimir(Elemento tabHash[]){
	 int i;
	 for(i=0; i<MAX; i++){
	 	if(tabHash[i].ocupado == true)
	 	   printf("Posicao: %d tem: %s \n", i, tabHash[i].nome);
	 }
}

//Atualizando um elemento
Elemento update (Elemento tabHash[], Elemento e, char *chave) {
   int pos;
   pos = h(e.nome);
   if(tabHash[pos].ocupado == true) 
    if((strcmp(tabHash[pos].nome, chave)==0)) 
          strcpy(tabHash[pos].telefone, e.telefone);
	   //tabHash[pos].telefone = e.telefone;
}

//Atualizando um elemento com colis�o:
Elemento update_colisao (Elemento tabHash[], Elemento e, char *chave) {
    int i, pos, newPos;
    pos = h(e.nome);
    for(i=0; i < MAX; i++) {
        newPos = sondagemLinear(pos, i, MAX);
        if(tabHash[newPos].ocupado == true) {
            if((strcmp(tabHash[newPos].nome, chave)==0)) {
                strcpy(tabHash[pos].telefone, e.telefone);
                return e;
            }
        }
    }
}

//Excluindo um elemento
bool excluir (Elemento  tabHash[], char *chave) {
   int pos;
   pos = h(chave);
   if(tabHash[pos].ocupado == true) 
      if((strcmp(tabHash[pos].nome, chave)==0)){
   	     tabHash[pos].ocupado = false;
         return true;
      }
   return false;
}

//Excluir um elemento com colis�o:
bool excluir_colisao (Elemento  tabHash[], char *chave) {
    int i, pos, newPos;
    pos = h(chave);
    for(i=0; i < MAX; i++) {
        newPos = sondagemLinear(pos, i, MAX);
        if(tabHash[newPos].ocupado == true) {
            if((strcmp(tabHash[newPos].nome, chave)==0)) {
                tabHash[newPos].ocupado = false;
                return true;
            }
        }
    }
    return false;
}

//Sondagem Linear
int sondagemLinear (int pos, int i, int size) {
    return ((pos + i) & 0x7FFFFFFF) % size;
}
 
void main(){
	int i;	
	char n[20];
	Elemento tabHash[MAX];	//tabHash[MAX] tabela;
	inicializaTabHash(tabHash, MAX);
	for(i=0; i<MAX; i++){
        Elemento e;
        printf("Informe o nome: ");
        gets(e.nome);
        printf("Informe o telefone: ");
        gets(e.telefone);
        //create(tabHash, e);
        create_colisao(tabHash, e);
        printf("\n");
    }
    
    imprimir(tabHash);
  
    printf("\nInforme o nome para pesquisar: ");
    gets(n);
    //Elemento nome = read(tabHash, n);
    Elemento nome = read_colisao(tabHash, n);
  
    if(strcmp(nome.nome,n)==0)
        printf("\nEncontrado: %s \n", nome.nome);
    
    printf("\nInforme o nome para excluir: ");
    gets(n);
    if(excluir_colisao(tabHash, n))
       printf("Posicao desocupada!");
    else
    	printf("Posicao desocupada!");
    	
    printf("\n");
    imprimir(tabHash);
    
	system("PAUSE");
}



