/*
 * main.c
 *
 * Programa de demonstra��o do analisador.
 *
 * Andr� Luiz Faustino
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char *prog;

void eval_exp(double *answer);

int main(void)
{

	double answer;
	char *p;

	p = malloc(100);

	if(!p)
	{
		printf("Falha na aloca��o\n");
		exit(1);
	}

	/* Processa express�es at� que uma linha em branco seja digitada */
	do
	{
		prog = p;
		printf("Digite a express�o: ");
		gets(prog);

		if(!*prog)
			break;

		eval_exp(&answer);
		printf("A resposta � %.2f\n", answer);

	}while(*p);

	return 0;

} /* Fim da fun��o main */
