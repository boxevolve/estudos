//Escrever um programa em C que aceite/rejeite palavras sob o alfabeto {0,1} que terminem com "01"

#include <stdio.h>
#include <stdlib.h>

void main() {
	int n=0;
	char v[50];
	char estado = '0';
	do {
		printf("Digite a sequencia:");
		scanf("%s", v);
		for(n=0; n < strlen(v); n++) {
			switch(estado) {
				case '0': 
					if(v[n] == '0') {
						estado = '1';
					} else if(v[n] == '1') {
						estado = '0';
					} else {
						estado = '3';
					}
					break;
				case '1':
					if(v[n] == '0') {
						estado = '1';
					} else if(v[n] == '1') {
						estado = '2';
					} else {
						estado = '3';
					}
					break;
				case '2':
					if(v[n] == '0') {
						estado = '1';
					} else if(v[n] == '1') {
						estado = '0';
					} else {
						estado = '3';
					}
					break;
				default:
					break;
			}
		}
		if(estado == '2') {
			printf("Palavra aceita\n");
		} else {
			printf("Palavra rejeitada\n");
		}
		estado = '0';
	} while(n > -1);
	system("PAUSE");
}
