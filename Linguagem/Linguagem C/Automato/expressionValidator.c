#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>

#define ALFABETO "().,+*/-%^0123456789"

#define DELIMITADOR 1
#define VARIAVEL 2
#define NUMERO 3

char *prog; /* Cont�m a express�o a ser analisada */
char token[80];
char tok_type;

void validate_exp(double *answer);
void validate_exp2(double *answer);
void validate_exp3(double *answer);
void validate_exp4(double *answer);
void validate_exp5(double *answer);
void validate_exp6(double *answer);
void get_token(void);
void atom(double *answer);
void putback(void);
void serror(int error);
int isdelim(char c);
int validateAlphabet();

int main(void) {
    /* Responsavel por permitir acento */
    setlocale (LC_ALL, "portuguese");
    
	double answer;
	char *pointer;
	
	/* Aloca size bytes e retorna um ponteiro para a mem�ria alocada. */ 
	pointer = malloc(100);

	if(!pointer) {
		printf("Falha na aloca��o\n");
		exit(1);
	}

	/* Processa express�es at� que uma linha em branco seja digitada */
	do {
		prog = pointer;
		printf("Digite a express�o: ");
		gets(prog);
        
		if(!*prog)
			break;
			
		if(validateAlphabet()) {
            validate_exp(&answer);
		    printf("A resposta � %.2f\n", answer);
        } else {
            serror(0);
        }
	} while(*pointer);

	return 0;

} /* Fim da fun��o main */

/* Ponto de entrada do analisador */
void validate_exp(double *answer) {
	get_token();
    
	if(!*token) {
		serror(2);
		return;
	}

	validate_exp2(answer);

	if(*token)
		serror(0); /* �ltimo token deve ser null */

} /* Fim da fun��o eval_exp */

/* Soma ou subtrai dois termos */
void validate_exp2(double *answer) {
	register char op;
	double temp;

	validate_exp3(answer);
    
	while((op = *token) == '+' || op == '-') {
		get_token();
		validate_exp3(&temp);

		switch(op) {
			case '-':
				*answer = *answer - temp;
				break;

			case '+':
				*answer = *answer + temp;
		}
	}

} /* Fim da fun��o eva_exp2 */

/* Multiplica ou divide dois fatores */
void validate_exp3(double *answer) {
	register char op;
	double temp;

	validate_exp4(answer);

	while((op = *token) == '*' || op == '/' || op == '%') {
		get_token();
		validate_exp4(&temp);

		switch(op) {
			case '*':
				*answer = *answer * temp;
				break;

			case '/':
				*answer = *answer / temp;
				break;

			case '%':
				*answer = (int) *answer % (int) temp;
		}
	}

} /* Fim da fun��o eva_exp3 */

/* Processa um expoente */
void validate_exp4(double *answer) {
	double temp, ex;
	register int t;

	validate_exp5(answer);

	if(*token == '^') {
		get_token();
		validate_exp4(&temp);
		ex = *answer;

		if(temp == 0.0) {
			*answer = 1.0;
			return;
		}

		for(t = temp - 1; t > 0; --t)
			*answer = (*answer) * (double) ex;
	}

} /* Fim da fun��o eva_exp4 */

/* Avalia um + ou - un�rio */
void validate_exp5(double *answer) {
	register char op;

	op = 0;

	if(((tok_type == DELIMITADOR) && (*token == '+' || *token == '-'))) {
		op = *token;
		get_token();
	}

	validate_exp6(answer);

	if(op == '-')
		*answer = -(*answer);

} /* Fim da fun��o eva_exp5 */

/* Processa uma express�o entre par�ntese */
void validate_exp6(double *answer) {
	if((*token == '(')) {
		get_token();
		validate_exp2(answer);

		if(*token != ')')
			serror(1);

		get_token();
	} else
		atom(answer);
} /* Fim da fun��o eva_exp6 */

/* Devolve o pr�ximo token */
void get_token(void) {
	register char *temp;

	tok_type = 0;
	temp = token;
	*temp = '\0';

	if(!*prog)
		return; /* Final da express�o */

	while(isspace(*prog))
		++prog; /* Ignora espa�os em branco */

	if(strchr("+-*/%^()", *prog)) {
		tok_type = DELIMITADOR;

		/* Avan�a para o pr�ximo char */
		*temp++ = *prog++;
	} else if(isalpha(*prog)) {
        /* Verifica se � um alfanum�rico */
		while(!isdelim(*prog))
			*temp++ = *prog++;

		tok_type = VARIAVEL;
	} else if(isdigit(*prog)) {
        /* Verifica se � um numero */
		while(!isdelim(*prog))
			*temp++ = *prog++;

		tok_type = NUMERO;
	}

	*temp = '\0';
} /* Fim da fun��o get_token */

/* Obt�m o valor real de um n�mero */
void atom(double *answer) {
	if(tok_type == NUMERO) {
        printf("RESPOSTA = %s\n", token);
		*answer = atof(token);
		printf("RESPOSTA D = %f\n", *answer);
		get_token();
		return;
	}

	serror(0); /* Caso contr�rio, erro de sintaxe na express�o  */

} /* Fim da fun��o atom */

/* Devolve um token � stream de entrada */
void putback(void) {
	char *t;
	
	t = token;

	for( ; *t; t++)
		prog--;

} /* Fim da fun��o putback */

/* Apresenta um erro de sintaxe */
void serror(int error) {
	static char *e[] = {
		"Erro de sintaxe",
		"Falta de par�nteses",
		"Nenhuma express�o presente"
		};

	printf("%s\n", e[error]);

} /* Fim da fun��o serror */

/* Devolve verdadeiro se c � um delimitador */
int isdelim(char c) {
	if(strchr(" +-/*%^()", c) || c == 9 || c == '\r' || c == 0)
		return 1;

	return 0;

} /* Fim da fun��o isdelim */

/* Verifica se cada caractere esta no alfabeto */
int validateAlphabet() {
    int i, j, different = 0, isValid = 1;
    int progSize = strlen(prog), alphabetSize = strlen(ALFABETO);
    
    for(i=0; i < progSize; i++) {
        different = 0;
        for(j=0; j < alphabetSize; j++) {
            if(prog[i] != ALFABETO[j])
                different++;
        } 
        if(different == alphabetSize) {
            isValid = 0;
            break;
        } 
    }
    
    return isValid;
}
