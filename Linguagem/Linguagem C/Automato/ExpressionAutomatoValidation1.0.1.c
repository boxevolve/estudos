// ----- Created by Bruno Casimiro -----
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>  

// ----- EQUACAO -----
#define ALPHABET_SPECIAL "()."
#define ALPHABET_OPERATORS "+*/-"
#define ALPHABET_OPERATING "0123456789"

// ----- AUTOMATO -----
#define STATE_SIZE 3
#define MAX_STATE_NAME 2
#define ALPHABET_SIZE 2
#define ALPHABET "01"
#define MAX_TEXT_SIZE 100

char currentState[MAX_STATE_NAME];
char finalState[STATE_SIZE][MAX_STATE_NAME] = {"q2"};

// ----- AUXILIARES -----
bool abortSystem = false;
int getIntLength(int value) {
    int length = 0;
    do {
        length = length++;
        value = value / 10;
    } while (value != 0);
    return length;
}

int convertCharToInt(char character) {
    return character - '0';
}

int concatCharToInt(int value, char charNumber) {
    int length = getIntLength(value), i, newNumber;
    
    char numberStr[length];
    char newNumberStr[length+1];
    sprintf(numberStr, "%d", value);
    for(i=0; i < length+1; i++) {
        if(i == length) {
            newNumberStr[i] = charNumber;
        } else {
            newNumberStr[i] = numberStr[i];
        }
    }
    newNumber = atoi(newNumberStr);
    return newNumber;
}

int getDoubleLength(double value) {
    int length = 0;
    int valueInt = value;
    double valueDecimal = value - valueInt;
    do {
        length = length++;
        valueInt = valueInt / 10;
    } while (valueInt != 0);
    
    double valueTrunc;
    do {
        length = length++;
        valueDecimal = valueDecimal * 10;
        valueTrunc = truncf (valueDecimal);
        valueDecimal = valueDecimal - valueTrunc;
    } while (fabs(valueDecimal) > 10e-7);
    return length;
}

double convertIntToDouble(int valueInt, int decimal) {
    bool isNegative = false;
    int value = valueInt;
    if(valueInt < 0) {
        isNegative = true;
        value = abs(valueInt);
    }
    int length = getIntLength(value), lengthDecimal = getIntLength(decimal), i;
    int lengthNewNumber = length+lengthDecimal+1;
    double newNumber;
    
    char numberStr[length];
    sprintf(numberStr, "%d", value);
    char decimalStr[lengthDecimal];
    sprintf(decimalStr, "%d", decimal);
    
    char newNumberStr[lengthNewNumber];
    
    int j = 0;
    for(i=0; i < lengthNewNumber; i++) {
        if(i < length) {
           newNumberStr[i] = numberStr[i]; 
        } else if(i == length) {
            newNumberStr[i] = ALPHABET_SPECIAL[2];
        } else {
            newNumberStr[i] = decimalStr[j];
            j++;
        }
    }
    newNumber = atof(newNumberStr);
    if(isNegative) {
        newNumber *= -1;
    }
    return newNumber;
}

int* convertDoubleToInt(double valueDouble) {
    int value, valueDecimal, i, 
        length = getDoubleLength(valueDouble)+1;
    char valueStr[length];
    bool isDecimal = false, isBegin = true;
    sprintf(valueStr, "%f", valueDouble);
    
    for(i=0; i < length; i++) {
        if(valueStr[i] == ALPHABET_SPECIAL[2]) {
            isDecimal = true;
            isBegin = true;
        } else if(isDecimal) {
            if(isBegin) {
                valueDecimal = convertCharToInt(valueStr[i]);
                isBegin = false;
            } else {
                valueDecimal = concatCharToInt(valueDecimal, valueStr[i]);
            }
        } else {
            if(isBegin) {
                value = convertCharToInt(valueStr[i]);
                isBegin = false;
            } else {
                value = concatCharToInt(value, valueStr[i]);
            }
        }
    }
    
    int * valuesInt = (int *) calloc (2, sizeof (int));
    valuesInt[0] = value;
    valuesInt[1] = valueDecimal;
    return valuesInt;
}

// ----- PILHA DE OPERANDO -----
typedef struct nodeOperating {
    int value;
    int valueDecimal;
    struct nodeOperating * next;
} structOperating;
typedef structOperating * stackOperating;

void createOperating(stackOperating * stackPointer){
     *stackPointer = NULL;
}

bool pushOperating(stackOperating *stackPointer, int value, int valueDecimal) {
     stackOperating newNodeStack;
     newNodeStack = (stackOperating*) malloc(sizeof(stackOperating));
     if(newNodeStack == NULL)
       return false;
     else{
        newNodeStack -> value = value;
        newNodeStack -> valueDecimal = valueDecimal;
        newNodeStack -> next = *stackPointer;
        *stackPointer = newNodeStack;
        return true;
     }
}

void printOperating(stackOperating *stackPointer) {
     stackOperating auxiliary;
     auxiliary = *stackPointer;
     
     printf("Pilha Operando:\n");
     while(auxiliary != NULL){
        printf("--[%d.%d]--\n", auxiliary -> value, auxiliary -> valueDecimal);
        auxiliary = auxiliary -> next;
     }
     printf("\n");
}

bool popOperating(stackOperating *stackPointer, int *value, int *valueDecimal){
     stackOperating auxiliary;
     if(*stackPointer == NULL) {
        return false;
     } else {
        auxiliary = *stackPointer;
        *value = (*stackPointer) -> value;
        *valueDecimal = (*stackPointer) -> valueDecimal;
        *stackPointer = auxiliary -> next;
        free(auxiliary);
        return true;
     }
}

void stackUpOperating();
void toUnpackOperating();

// ----- PILHA DE OPERADORES -----
typedef struct nodeOperators{
    char value;
    struct nodeOperators * next;
} structOperators;
typedef structOperators * stackOperators;

void createOperators(stackOperators * stackPointer){
     *stackPointer = NULL;
}

bool pushOperators(stackOperators *stackPointer, char value){
     stackOperators newNodeStack;
     newNodeStack = (stackOperators*) malloc(sizeof(stackOperators));
     if(newNodeStack == NULL)
        return false;
     else{
        newNodeStack -> value = value;
        newNodeStack -> next = *stackPointer;
        *stackPointer = newNodeStack;
        return true;
     }
}

void printOperators(stackOperators *stackPointer){
     stackOperators auxiliary;
     auxiliary = *stackPointer;
     printf("Pilha Operadores:\n");
     while(auxiliary != NULL){
        printf("--[%c]--\n", auxiliary -> value);
        auxiliary = auxiliary -> next;
     }
     printf("\n");
}

bool popOperators(stackOperators *stackPointer, char * value){
     stackOperators auxiliary;
     if(*stackPointer == NULL)
        return false;
     else{
        auxiliary = *stackPointer;
        *value = (*stackPointer) -> value;
        *stackPointer = auxiliary -> next;
        free(auxiliary);
        return true;
     }
}

void stackUpOperators();
void toUnpackOperators();

// ----- EQUA��O -----
void equation();
void validateCharacterEquation(char character[], 
    stackOperators *stackPointerOperators,
    stackOperating *stackPointerOperating);
bool validateAlphabetEquation(char character);
bool isOperator(char);
bool isOperating(char);
bool isPoint(char);
bool validateParentheses(char character, stackOperators *stackPointerOperators, 
    stackOperating *stackPointerOperating);
double calculate(double, double, char);
bool validateGroup(char equation[], char characterOpen, char characterClose);
bool validateDivison(char operator, int divider);

// ----- AUTOMATO -----
typedef struct {
    char destinyState[MAX_STATE_NAME];
    char input[ALPHABET_SIZE];
} destiny;

typedef struct {
    char stateName[MAX_STATE_NAME];
    destiny destinations[STATE_SIZE];
} state;

void menu(state*);
void sendText();
int validateCharacter(char, state*);
int validateAlphabet(char);
int validateState(char, state);
int validateFinal();
void printAlphabet();
void printStates();
destiny getDestiny(char* destinyState, char* input);
state * getState(char* stateName, destiny* destinations, int sizeDestinations);
void printStr();
void clearVector();
void clearVectorDestinations();
void cleanConsole();

// � nessa fun��o que sera alterada caso queira validar outro automato.
state * getStates(state* stateArray) {
    int i;
    //Q0
    destiny destinationsQ0[] = {getDestiny("q1", "0"), getDestiny("q0", "1")};
    state *stateQ0 = getState("q0", destinationsQ0, 2);
    
    //Q1
    destiny destinationsQ1[] = {getDestiny("q1", "0"), getDestiny("q2", "1")};
    state *stateQ1 = getState("q1", destinationsQ1, 2);
    
    //Q2
    destiny destinationsQ2[] = {getDestiny("q1", "0"), getDestiny("q0", "1")};
    state *stateQ2 = getState("q2", destinationsQ2, 2);
    
    stateArray[0] = *stateQ0;
    stateArray[1] = *stateQ1;
    stateArray[2] = *stateQ2;
    
    // Estado inicial
    strcpy(currentState, "q0");
    
    return stateArray;
}

void main() {
    state states[STATE_SIZE];
    getStates(states);
    menu(states);
    system("PAUSE");
}

void menu(state* states) {
    int option;
    stackOperators operators;
    createOperators(&operators);
    
    stackOperating operating;
    createOperating(&operating);
    
    do {
        printf("+---| Automato finito deterministico |---+\n");
        printf("[ 1  ] Validar entrada de texto\n");
        printf("[ 2  ] Imprimir alfabeto\n");
        printf("[ 3  ] Imprimir dados do automato\n");
        printf("[ 4  ] Limpar console\n");
        printf("+---| Pilha operandos |---+\n");
        printf("[ 5  ] Empilhar operandos\n");
        printf("[ 6  ] Desempilhar operandos\n");
        printf("[ 7  ] Imprimir operandos\n");
        printf("+---| Pilha operadores |---+\n");
        printf("[ 8  ] Empilhar operadores\n");
        printf("[ 9  ] Desempilhar operadores\n");
        printf("[ 10 ] Imprimir operadores\n");
        printf("+---| Equacao |---+\n");
        printf("[ 11 ] Verificar equacao\n");
        printf("+---| Sistema |---+\n");
        printf("[ 0  ] Sair\n");
        printf("Opcao: ");
        scanf("%d", &option);
        switch(option) {
            case 0:
                cleanConsole();
                printf("Saindo...\n");
                break;
            case 1:
                cleanConsole();
                printf("+---| [ 1  ] Validar entrada de texto |---+\n");
                sendText(states);
                break;
            case 2:
                cleanConsole();
                printf("+---| [ 2  ] Imprimir alfabeto |---+\n");
                printAlphabet(ALPHABET);
                break;
            case 3:
                cleanConsole();
                printf("+---| [ 3  ] Imprimir dados do automato |---+\n");
                printStates(states);
                break;
            case 4:
                cleanConsole();
                break;
            case 5:
                cleanConsole();
                printf("+---| [ 5  ] Empilhar operandos |---+\n");
                stackUpOperating(&operating);
                break;
            case 6:
                cleanConsole();
                printf("+---| [ 6  ] Desempilhar operandos |---+\n");
                toUnpackOperating(&operating);
                break;
            case 7:
                cleanConsole();
                printf("+---| [ 7  ] Imprimir operandos |---+\n");
                printOperating(&operating);
                break;
            case 8:
                cleanConsole();
                printf("+---| [ 8  ] Empilhar operadores |---+\n");
                stackUpOperators(&operators);
                break;
            case 9:
                cleanConsole();
                printf("+---| [ 9  ] Desempilhar operadores |---+\n");
                toUnpackOperators(&operators);
                break;
            case 10:
                cleanConsole();
                printf("+---| [ 10 ] Imprimir operadores |---+\n");
                printOperators(&operators);
                break;
            case 11:
                cleanConsole();
                printf("+---| [ 11 ] Verificar equacao |---+\n");
                createOperators(&operators);
                createOperating(&operating);
                equation(&operators, &operating);
                createOperators(&operators);
    			createOperating(&operating);
                break;
            default:
                cleanConsole();
                printf("Opcao invalida!\n");
                break;
        }
    } while(option != '0'); 
}

// ----- AUTOMATO -----
// Recupera cada caracter digitado pelo usuario e envia para valida��o, 
// se o texto estiver correto e o usuario apertar "ENTER" � validado se � 
// aceito ou n�o o texto informado.
void sendText(state* states) {
    char reportedCharacter, text[MAX_TEXT_SIZE];
    int textPosition = 0, isValid = 0, isAccept = 0, reportedCharacterASCII;
    clearVector(text, MAX_TEXT_SIZE);
    
    printf("Informe o texto:\n");
    do {
        reportedCharacter = getch();
        reportedCharacterASCII = reportedCharacter;
        
        if(reportedCharacterASCII != 13) {
            text[textPosition] = reportedCharacter;
            isAccept = validateCharacter(reportedCharacter, states);
            textPosition++;
            printf("%c", reportedCharacter);
        } else {
            isValid = 1;
        }
    } while(isValid == 0 && isAccept == 0); 
    
    printf("\n--[");
    if(isAccept == 0 && validateFinal() == 0) {
		printf("Palavra aceita!");
	} else {
		printf("Palavra rejeitada!");
	}
	printf("]--\n\n");
}

// Responsavel por validar cada caractere digitado pelo usuario.
int validateCharacter(char character, state* states) {
    int isValid = validateAlphabet(character);
    if(isValid == 0) {
        int i, j, currentStateCorrect = 0;
        for(i=0; i < STATE_SIZE; i++) {
            for(j=0; j < MAX_STATE_NAME; j++) {
                if(states[i].stateName[j] == currentState[j]) {
                    currentStateCorrect++;
                }
            }
            if(currentStateCorrect == MAX_STATE_NAME) {
                validateState(character, states[i]);
                break;
            }
            currentStateCorrect = 0;
        }
    }
    return isValid;
}

// Responsavel por validar se o caractere informado pertence ao alfabeto 
// do automato.
int validateAlphabet(char character) {
    int i, isValid = 0, different = 0;
    for(i=0; i < ALPHABET_SIZE; i++) {
        if(character != ALPHABET[i]) {
            different++;
        }
    } 
    if(different == ALPHABET_SIZE) {
        isValid = 1;
    }
    return isValid;
}

// Reponsavel por verificar qual � o proximo estado que o automato deve seguir 
// de acordo com o caractere informado e o estado atual.
int validateState(char character, state state) {
    int j, k, l;
    for(j=0; j < STATE_SIZE; j++) {
        for(k=0; k < ALPHABET_SIZE; k++) {
            if(character == state.destinations[j].input[k]) {
                for(l=0; l < MAX_STATE_NAME; l++) {
                    currentState[l] = state.destinations[j].destinyState[l];
                }
            }
        }
    }
}

// Responsavel por verificar se o estado atual esta na lista de estados finais.
int validateFinal() {
    int i, j, isValid = 1, currentStateCorrect = 0;
    for(i=0; i < STATE_SIZE; i++) {
        if(finalState[i] != NULL) {
            for(j=0; j < MAX_STATE_NAME; j++) {
                if(currentState[j] == finalState[i][j]) {
                    currentStateCorrect++;
                }
            }
            if(currentStateCorrect == MAX_STATE_NAME) {
                isValid = 0;
                break;
            }
            currentStateCorrect = 0;
        }
    }
    return isValid;
}

// Responsavel por imprimir o alfabeto do automato.
void printAlphabet(char alphabet[ALPHABET_SIZE]) {
    int i;
    printf("Alfabeto: \n--[");
    printStr(alphabet, ALPHABET_SIZE);
    printf("]--\n\n");
}

// Responsavel por imprimir os estados possiveis do automato e seus respectivos 
// destinos.
void printStates(state states[]) {
    destiny destinyTemp;
    int i, j;
    for(i=0; i < STATE_SIZE; i++) {
        printf("Estado:\n+-[");
        printStr(states[i].stateName, MAX_STATE_NAME);
        printf("]--\n Destinos:\n");
        for(j=0; j < STATE_SIZE; j++) {
            destinyTemp = states[i].destinations[j];
            if(destinyTemp.destinyState[0] != NULL) {
                printf("  [");
                printStr(destinyTemp.destinyState, MAX_STATE_NAME);
                printf("]--{");
                printStr(destinyTemp.input, ALPHABET_SIZE);
                printf("}\n");
            }
        }
    }
    printf("\n");
}

// Responsavel por gerar o destino com sua respectiva lista de caractere aceito.
destiny getDestiny(char* destinyState, char* input) {
    destiny *newState = malloc(sizeof(destiny));;
    strcpy(newState -> destinyState, destinyState);
    strcpy(newState -> input, input);
    return *newState;
}

// Responsavel por gerar o estado com a lista de poss�veis destinos.
state * getState(char* stateName, destiny* destinations, int sizeDestinations) { 
    int i;
    state *newState = malloc(sizeof(state));
    strcpy(newState -> stateName, stateName);
    clearVectorDestinations(newState -> destinations);
    for(i=0; i < sizeDestinations; i++) {
        newState -> destinations[i] = destinations[i];
    }
    return newState;
}

// Responsavel por imprimir uma String, pois o "%s" estava imprimindo um 
// caractere estranho no final.
void printStr(char* string, int size) {
    int i;
    for(i=0; i < size; i++) {
        if(string[i] != NULL) {
            printf("%c", string[i]);
        }
    }
}

// Responsavel por limpar o vetor para n�o conter lixo da memoria.
void clearVector(char* vector, int sizeVector) {
    int i;
    for(i=0; i < sizeVector; i++) {
        vector[i] = NULL;
    }
}

// Responsavel por limpar o vetor de destinos para n�o conter lixo da memoria.
void clearVectorDestinations(destiny * vectorDestiny) {
    int i, j;
    for(i=0; i < STATE_SIZE; i++) {
        for(j=0; j < MAX_STATE_NAME; j++) {
            vectorDestiny[i].destinyState[j] = NULL;
        }
        for(j=0; j < ALPHABET_SIZE; j++) {
            vectorDestiny[i].input[j] = NULL;
        }
    }
} 

// Responsavel por limpar o console.
void cleanConsole() {
    system("cls");
}

// ----- EQUA��O -----
void equation(stackOperators *stackPointerOperators, 
    stackOperating *stackPointerOperating) {
    char reportedCharacter, text[MAX_TEXT_SIZE];
    int textPosition = 0, reportedCharacterASCII;
    bool isValid = true, isAccept = true;
    clearVector(text, MAX_TEXT_SIZE);
    
    printf("Informe a equacao:\n");
    do {
        reportedCharacter = getch();
        reportedCharacterASCII = reportedCharacter;
        
        if(reportedCharacterASCII != 13) {
            text[textPosition] = reportedCharacter;
            isAccept = validateAlphabetEquation(reportedCharacter); 
            textPosition++;
            printf("%c", reportedCharacter);
        } else {
            isValid = false;
        }
    } while(isValid == true && isAccept == true); 
    
    if(isAccept == false || !validateGroup(text, '(' , ')')) {
		printf("\n--[Equacao rejeitada!]--\n\n");
	} else {
        validateCharacterEquation(text, stackPointerOperators, 
            stackPointerOperating);
    }
}

void validateCharacterEquation(char *equation, 
    stackOperators *stackPointerOperators, 
    stackOperating *stackPointerOperating) {
    int i; 
    int newValue, newValueDecimal = 0;
    bool isBegin = true, isDecimal = false;
    for(i=0; i < strlen(equation); i++) {
        if(isPoint(equation[i])) {
            isDecimal = !isDecimal;
        } else if(isOperating(equation[i])) {
            if(isBegin == true) {
                newValue = convertCharToInt(equation[i]);
                newValueDecimal = 0;
                isBegin = false;
            } else {
            	if(!popOperating(stackPointerOperating, &newValue, 
                    &newValueDecimal)) {
                    newValue = convertCharToInt(equation[i]);
                    newValueDecimal = 0;
                } else {
                    if(isDecimal == true) {
                        newValueDecimal = concatCharToInt(newValueDecimal, equation[i]);
                    } else {
                        newValue = concatCharToInt(newValue, equation[i]);
                        newValueDecimal = 0;
                    }
                }
            }
            if(!pushOperating(stackPointerOperating, newValue, newValueDecimal)){
                printf("Nao foi possivel alocar memoria! \n\n");
            }
        } else if(isOperator(equation[i])) {
            isBegin = true;
            isDecimal = false;
            if(!pushOperators(stackPointerOperators, equation[i])){
                printf("Nao foi possivel alocar memoria! \n\n");
            }
        } else if(validateParentheses(equation[i], stackPointerOperators, 
            stackPointerOperating)) {
            isBegin = true;
            isDecimal = false;
        }
        
        if(abortSystem) {
            return;
        }
    }
    
    int result;
    int resultDecimal;
    if(popOperating(stackPointerOperating, &result, &resultDecimal)) {
        printf("\nO resultado da equacao eh:\n--[%f]--", 
            convertIntToDouble(result, resultDecimal));
        printf("\nO resultado inteiro da equacao eh:\n--[%d]--", result);
    } else {
        printf("\nFalha no resultado!");
    }
    printf("\n\n");
}

bool validateGroup(char *equation, char characterOpen, char characterClose) {
	int open = 0, close = 0, i;
	for(i=0; i < strlen(equation); i++) {
		if(equation[i] == characterOpen && equation[i+1] == characterClose) {
			return false;
		} else if(equation[i] == characterOpen) {
			open++;
		} else if(equation[i] == characterClose) {
			close++;
		}
	}
	if(open != close) {
		return false;
	}
	return true;
}

bool validateParentheses(char character, stackOperators *stackPointerOperators, 
    stackOperating *stackPointerOperating) {
    if(character == ')') {
        int b;
        int bDecimal;
        popOperating(stackPointerOperating, &b, &bDecimal);
        double valueB = convertIntToDouble(b, bDecimal);
        
        int a;
        int aDecimal;
        popOperating(stackPointerOperating, &a, &aDecimal);
        double valueA = convertIntToDouble(a, aDecimal);
        
        char x;
        popOperators(stackPointerOperators, &x);
        
        if(!validateDivison(x, b+bDecimal)) {
            abortSystem = true;
            return false;
        }
        
        double result = calculate(valueA, valueB, x);
        int *resultInt = convertDoubleToInt(result);
        printf("\nA I: %d A F: %d DOUBLE: %f\n\n", a, aDecimal, valueA);
        printf("B I: %d B F: %d DOUBLE: %f\n\n", b, bDecimal, valueB);
        printf("RESULTADO INT: %d.%d \n\n", resultInt[0], resultInt[1]);
        if(!pushOperating(stackPointerOperating, resultInt[0], resultInt[1])){
            printf("Nao foi possivel alocar memoria! \n\n");
        }
        return true;
    }
    return false;
}

double calculate(double a, double b, char currentOperator) {
    printf("\nA: %f B: %f\n", a, b);
    double result;
    
    switch(currentOperator) {
        case '+':
            result = a + b;
            break;
        case '-':
            result = a - b;
            break;
        case '*':
            result = a * b;
            break;
        case '/':
            result = a / b;
            break;
        default:
            break;
    }
    printf("\nRESULTADO: %f", result);
    return result;
}

bool validateAlphabetEquation(char character) {
    int i, different = 0, size = strlen(ALPHABET_SPECIAL);
    bool isValid = true;
    for(i=0; i < size; i++) {
        if(character != ALPHABET_SPECIAL[i]) {
            different++;
        }
    } 
    if(different == size) {
        isValid = false;
    } 
    
    return isValid 
        || isOperator(character) 
        || isOperating(character)
        || isPoint(character);
}

bool validateDivison(char operator, int divider) {
    if(operator == '/'
    && divider == 0) {
        printf("\n--[Nao eh possivel dividir por zero!]--\n\n");
        return false;
    }
    return true;
}

bool isOperator(char character) {
    int i, different = 0, size = strlen(ALPHABET_OPERATORS);
    bool isValid = true;
    for(i=0; i < size; i++) {
        if(character != ALPHABET_OPERATORS[i]) {
            different++;
        }
    } 
    if(different == size) {
        isValid = false;
    }
    return isValid;
}

bool isOperating(char character) {
    int i, different = 0, size = strlen(ALPHABET_OPERATING);
    bool isValid = true;
    for(i=0; i < size; i++) {
        if(character != ALPHABET_OPERATING[i]) {
            different++;
        }
    } 
    if(different == size) {
        isValid = false;
    }
    return isValid;
}

bool isPoint(char character) {
    return character == ALPHABET_SPECIAL[2];
}

// ----- PILHA DE OPERANDO -----
void stackUpOperating(stackOperating * stackPointer) {
    int value, valueDecimal;
    
    printf("Informe o valor inteiro: ");
    scanf("%d", &value);
    printf("Informe o valor decimal: ");
    scanf("%d", &valueDecimal);
    
    if(pushOperating(stackPointer, value, valueDecimal)){
        printf("\n--[%d.%d]-- \n\n", (*stackPointer) -> value, 
            (*stackPointer) -> valueDecimal);
    } else {
        printf("Nao foi possivel alocar memoria! \n\n");
    }
}

void toUnpackOperating(stackOperating *stackPointer) {
    int value, valueDecimal;
    if(popOperating(stackPointer, &value, &valueDecimal)) {
        printf("Retirado Elemento do topo da pilha:");
        printf("\n--[%d.%d]-- \n\n", value, valueDecimal);
    }
}

// ----- PILHA DE OPERADORES -----
void stackUpOperators(stackOperators *stackPointer) {
    char value;
    printf("Informe o valor: ");
    value = getch();
    if(pushOperators(stackPointer, value)){
        printf("\n--[%c]-- \n\n", **stackPointer);
    } else {
        printf("Nao foi possivel alocar memoria! \n\n");
    }
}

void toUnpackOperators(stackOperators * stackPointer) {
    char getValue;
    if(popOperators(stackPointer, &getValue)){
        printf("Retirado Elemento do topo da pilha:\n--[%c]-- \n\n", getValue);
    }
}
