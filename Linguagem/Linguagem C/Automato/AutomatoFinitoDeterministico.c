// Created by Bruno Casimiro
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STATE_SIZE 3
#define MAX_STATE_NAME 2
#define ALPHABET_SIZE 2
#define ALPHABET "01"
#define MAX_TEXT_SIZE 100

char currentState[MAX_STATE_NAME];
char finalState[STATE_SIZE][MAX_STATE_NAME] = {"q2"};

typedef struct {
    char destinyState[MAX_STATE_NAME];
    char input[ALPHABET_SIZE];
} destiny;

typedef struct {
    char stateName[MAX_STATE_NAME];
    destiny destinations[STATE_SIZE];
} state;

void menu(state*);
void sendText();
int validateCharacter(char, state*);
int validateAlphabet(char);
int validateState(char, state);
int validateFinal();
void printAlphabet();
void printStates();
destiny getDestiny(char* destinyState, char* input);
state * getState(char* stateName, destiny* destinations, int sizeDestinations);
void printStr();
void clearVector();
void clearVectorDestinations();
void cleanConsole();

// � nessa fun��o que sera alterada caso queira validar outro automato.
state * getStates(state* stateArray) {
    int i;
    //Q0
    destiny destinationsQ0[] = {getDestiny("q1", "0"), getDestiny("q0", "1")};
    state *stateQ0 = getState("q0", destinationsQ0, 2);
    
    //Q1
    destiny destinationsQ1[] = {getDestiny("q1", "0"), getDestiny("q2", "1")};
    state *stateQ1 = getState("q1", destinationsQ1, 2);
    
    //Q2
    destiny destinationsQ2[] = {getDestiny("q1", "0"), getDestiny("q0", "1")};
    state *stateQ2 = getState("q2", destinationsQ2, 2);
    
    stateArray[0] = *stateQ0;
    stateArray[1] = *stateQ1;
    stateArray[2] = *stateQ2;
    
    // Estado inicial
    strcpy(currentState, "q0");
    
    return stateArray;
}

void main() {
    state states[STATE_SIZE];
    getStates(states);
    menu(states);
    system("PAUSE");
}

void menu(state* states) {
    int option = -1;
    do {
        printf("+---| Automato finito deterministico |---+\n");
        printf("[ 1 ] Validar entrada de texto\n");
        printf("[ 2 ] Imprimir alfabeto\n");
        printf("[ 3 ] Imprimir dados do automato\n");
        printf("[ 4 ] Limpar console\n");
        printf("[ 0 ] Sair\n");
        printf("Opcao: ");
        scanf("%d", &option);
        switch(option) {
            case 0:
                cleanConsole();
                printf("Saindo...\n");
                break;
            case 1:
                cleanConsole();
                printf("+---| [ 1 ] Validar entrada de texto |---+\n");
                sendText(states);
                break;
            case 2:
                cleanConsole();
                printf("+---| [ 2 ] Imprimir alfabeto |---+\n");
                printAlphabet(ALPHABET);
                break;
            case 3:
                cleanConsole();
                printf("+---| [ 3 ] Imprimir dados do automato |---+\n");
                printStates(states);
                break;
            case 4:
                cleanConsole();
                break;
            default:
                cleanConsole();
                printf("Opcao invalida!\n");
                break;
        }
    } while(option != 0); 
}

// Recupera cada caracter digitado pelo usuario e envia para valida��o, 
// se o texto estiver correto e o usuario apertar "ENTER" � validado se � 
// aceito ou n�o o texto informado.
void sendText(state* states) {
    char reportedCharacter, text[MAX_TEXT_SIZE];
    int textPosition = 0, isValid = 0, isAccept = 0, reportedCharacterASCII;
    clearVector(text, MAX_TEXT_SIZE);
    
    printf("Informe o texto:\n");
    do {
        reportedCharacter = getch();
        reportedCharacterASCII = reportedCharacter;
        
        if(reportedCharacterASCII != 13) {
            text[textPosition] = reportedCharacter;
            isAccept = validateCharacter(reportedCharacter, states);
            textPosition++;
            printf("%c", reportedCharacter);
        } else {
            isValid = 1;
        }
    } while(isValid == 0 && isAccept == 0); 
    
    printf("\n--[");
    if(isAccept == 0 && validateFinal() == 0) {
		printf("Palavra aceita!");
	} else {
		printf("Palavra rejeitada!");
	}
	printf("]--\n\n");
}

// Responsavel por validar cada caractere digitado pelo usuario.
int validateCharacter(char character, state* states) {
    int isValid = validateAlphabet(character);
    if(isValid == 0) {
        int i, j, currentStateCorrect = 0;
        for(i=0; i < STATE_SIZE; i++) {
            for(j=0; j < MAX_STATE_NAME; j++) {
                if(states[i].stateName[j] == currentState[j]) {
                    currentStateCorrect++;
                }
            }
            if(currentStateCorrect == MAX_STATE_NAME) {
                validateState(character, states[i]);
                break;
            }
            currentStateCorrect = 0;
        }
    }
    return isValid;
}

// Responsavel por validar se o caractere informado pertence ao alfabeto 
// do automato.
int validateAlphabet(char character) {
    int i, isValid = 0, different = 0;
    for(i=0; i < ALPHABET_SIZE; i++) {
        if(character != ALPHABET[i]) {
            different++;
        }
    } 
    if(different == ALPHABET_SIZE) {
        isValid = 1;
    }
    return isValid;
}

// Reponsavel por verificar qual � o proximo estado que o automato deve seguir 
// de acordo com o caractere informado e o estado atual.
int validateState(char character, state state) {
    int j, k, l;
    for(j=0; j < STATE_SIZE; j++) {
        for(k=0; k < ALPHABET_SIZE; k++) {
            if(character == state.destinations[j].input[k]) {
                for(l=0; l < MAX_STATE_NAME; l++) {
                    currentState[l] = state.destinations[j].destinyState[l];
                }
            }
        }
    }
}

// Responsavel por verificar se o estado atual esta na lista de estados finais.
int validateFinal() {
    int i, j, isValid = 1, currentStateCorrect = 0;
    for(i=0; i < STATE_SIZE; i++) {
        if(finalState[i] != NULL) {
            for(j=0; j < MAX_STATE_NAME; j++) {
                if(currentState[j] == finalState[i][j]) {
                    currentStateCorrect++;
                }
            }
            if(currentStateCorrect == MAX_STATE_NAME) {
                isValid = 0;
                break;
            }
            currentStateCorrect = 0;
        }
    }
    return isValid;
}

// Responsavel por imprimir o alfabeto do automato.
void printAlphabet(char alphabet[ALPHABET_SIZE]) {
    int i;
    printf("Alfabeto: \n--[");
    printStr(alphabet, ALPHABET_SIZE);
    printf("]--\n\n");
}

// Responsavel por imprimir os estados possiveis do automato e seus respectivos 
// destinos.
void printStates(state states[]) {
    destiny destinyTemp;
    int i, j;
    for(i=0; i < STATE_SIZE; i++) {
        printf("Estado:\n+-[");
        printStr(states[i].stateName, MAX_STATE_NAME);
        printf("]--\n Destinos:\n");
        for(j=0; j < STATE_SIZE; j++) {
            destinyTemp = states[i].destinations[j];
            if(destinyTemp.destinyState[0] != NULL) {
                printf("  [");
                printStr(destinyTemp.destinyState, MAX_STATE_NAME);
                printf("]--{");
                printStr(destinyTemp.input, ALPHABET_SIZE);
                printf("}\n");
            }
        }
    }
}

// Responsavel por gerar o destino com sua respectiva lista de caractere aceito.
destiny getDestiny(char* destinyState, char* input) {
    destiny *newState = malloc(sizeof(destiny));;
    strcpy(newState -> destinyState, destinyState);
    strcpy(newState -> input, input);
    return *newState;
}

// Responsavel por gerar o estado com a lista de poss�veis destinos.
state * getState(char* stateName, destiny* destinations, int sizeDestinations) { 
    int i;
    state *newState = malloc(sizeof(state));
    strcpy(newState -> stateName, stateName);
    clearVectorDestinations(newState -> destinations);
    for(i=0; i < sizeDestinations; i++) {
        newState -> destinations[i] = destinations[i];
    }
    return newState;
}

// Responsavel por imprimir uma String, pois o "%s" estava imprimindo um 
// caractere estranho no final.
void printStr(char* string, int size) {
    int i;
    for(i=0; i < size; i++) {
        if(string[i] != NULL) {
            printf("%c", string[i]);
        }
    }
}

// Responsavel por limpar o vetor para n�o conter lixo da memoria.
void clearVector(char* vector, int sizeVector) {
    int i;
    for(i=0; i < sizeVector; i++) {
        vector[i] = NULL;
    }
}

// Responsavel por limpar o vetor de destinos para n�o conter lixo da memoria.
void clearVectorDestinations(destiny * vectorDestiny) {
    int i, j;
    for(i=0; i < STATE_SIZE; i++) {
        for(j=0; j < MAX_STATE_NAME; j++) {
            vectorDestiny[i].destinyState[j] = NULL;
        }
        for(j=0; j < ALPHABET_SIZE; j++) {
            vectorDestiny[i].input[j] = NULL;
        }
    }
} 

// Responsavel por limpar o console.
void cleanConsole() {
    system("cls");
}
