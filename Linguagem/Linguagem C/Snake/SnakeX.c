#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <conio.h>
#include <windows.h>

char WALL = 176;
char HEAD_SNAKE = 'O';
char BODY_SNAKE = 'o';
char FRUIT = 162;
char FREE = ' ';

const int width = 20;
const int height = 20;

bool gameOver;
int x, y, fruitX, fruitY, score, level, speed;
int tailX[100], tailY[100];
int nTail;
int levelDraw[400][400]={{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}};

enum eDirecton { 
    STOP = 0, 
    LEFT, 
    RIGHT, 
    UP, 
    DOWN
} dir;

void Setup() {
	gameOver = false;
	dir = STOP;
	x = width / 2;
	y = height / 2;
	srand((unsigned) time(NULL));
	fruitX = rand() % width;
	fruitY = rand() % height;
	score = 0;
	level = 1;
	speed = 25;
}

void level1(){
    int i, j, k;
	
	for (i = 0; i < width+2; i++)
		printf("%c", WALL);
	printf("\n");

	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			if (j == 0)
				printf("%c", WALL);
			if (levelDraw[i][j] == 1)
			    printf("%c", WALL);
			else if (i == y && j == x)
				printf("%c", HEAD_SNAKE);
			else if (i == fruitY && j == fruitX)
				printf("%c", FRUIT);
			else {
				bool print = false;
				for (k = 0; k < nTail; k++) {
					if (tailX[k] == j && tailY[k] == i) {
						printf("%c", BODY_SNAKE);
						print = true;
					}
				}
				if (!print)
					printf("%c", FREE);
			}

			if (j == width - 1)
				printf("%c", WALL);
		}
		printf("\n");
	}

	for (i = 0; i < width+2; i++)
		printf("%c", WALL);
}

void Draw() {
	system("cls"); //system("clear");
	switch(level){
        case 1:
            level1();
            break;
        default:
            break;
    }
	printf("\n");
	printf("Score: %d\n", score);
}

void Input() {
	if (_kbhit()) {
		switch (_getch()) {
		case 'a':
            if (dir != RIGHT){
		    	dir = LEFT;
            }
			break;
		case 'd':
            if (dir != LEFT){
				dir = RIGHT;
            }
			break;
		case 'w':
            if (dir != DOWN){
				dir = UP;
            }
			break;
		case 's':
            if (dir != UP){
				dir = DOWN;
            }
			break;
		case 'x':
			gameOver = true;
			break;
		}
	}
}

void Logic() {
	int prevX = tailX[0];
	int prevY = tailY[0];
	int prev2X, prev2Y;
	tailX[0] = x;
	tailY[0] = y;
	int i;
	for (i = 1; i < nTail; i++) {
		prev2X = tailX[i];
		prev2Y = tailY[i];
		tailX[i] = prevX;
		tailY[i] = prevY;
		prevX = prev2X;
		prevY = prev2Y;
	}
	switch (dir) {
    	case LEFT:
    		x--;
    		break;
    	case RIGHT:
    		x++;
    		break;
    	case UP:
    		y--;
    		break;
    	case DOWN:
    		y++;
    		break;
    	default:
    		break;
	}
	//if (x > width || x < 0 || y > height || y < 0)
	//	gameOver = true;
	if (levelDraw[y][x] == 1)
	   gameOver = true;
	if (x >= width) x = 0; else if (x < 0) x = width - 1;
	if (y >= height) y = 0; else if (y < 0) y = height - 1;

	for (i = 0; i < nTail; i++)
		if (tailX[i] == x && tailY[i] == y)
			gameOver = true;

	if (x == fruitX && y == fruitY) {
		score += 10;
		srand((unsigned) time(NULL));
		fruitX = rand() % width;
		fruitY = rand() % height;
		nTail++;
	}
}

int main() {
	Setup();
	while (!gameOver) {
		Draw();
		Input();
		Logic();
		Sleep(speed); //sleep(10);
	}
	system("PAUSE");
	return 0;
}
