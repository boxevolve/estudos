#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

//---FUNCOES PARA ARVORE CHAR---
struct arvGen{
	char info;
	struct arvGen * prim;
	struct arvGen * prox;
};
typedef struct arvGen ArvGen;

ArvGen * criar(char c){
	ArvGen * novo;
	novo = (ArvGen *) malloc(sizeof(ArvGen));
	novo->info = c;
	novo->prim = NULL;
	novo->prox = NULL;
	return novo;
}

void insereLista(ArvGen * a, ArvGen * sa){
	sa->prox = a->prim;
	a->prim = sa;	
}

void imprimeOrdemSimetrica(ArvGen * pA){
	ArvGen * p;
	printf("%c ", pA->info);
	for(p=pA->prim; p!=NULL; p=p->prox)
		imprimeOrdemSimetrica(p);
}

void ImprimirPosOrdem(ArvGen * pA){
     ArvGen * p;
     for(p=pA->prim; p!= NULL; p=p->prox)
       ImprimirPosOrdem(p);
       printf("%c ",pA->info);
}

//Exercicio 1 : Percorre a arvore criando e copiando os dados
ArvGen * copiar(ArvGen * A){
	ArvGen *b;
	if(A != NULL){
    	b = criar(A->info);
    	b->prim=copiar(A->prim);
    	b->prox=copiar(A->prox);
    	return b;
    }
}

//Exercicio 2 : Verifica cada possibilidade de diferen�a e retorna um boolean
bool verificaArvores(ArvGen * pA, ArvGen * pB){
    return pA == NULL && pB == NULL ||
    (!pA==NULL && !pB == NULL &&
    pA->info == pB->info &&
    verificaArvores(pA->prim, pB->prim) &&
    verificaArvores(pA->prox, pB->prox));
}

//Exercicio 3 : Dado uma ra�z e um valor ele procura na arvore
bool busca (ArvGen * a, char c){
     ArvGen * p;
     if(a->info == c){
       return true;
     }else{
      for(p=a->prim; p!= NULL; p=p->prox)
        if(busca(p,c)){
          return true;
        }
     }
   return false;
}

int buscav2Char (ArvGen * a, char c){
     if(a->prim == NULL && a->prox == NULL)
        if(a->info == c){
            return 1;
        }else{
            return 0;
        }
    if(a->prim == NULL && a->prox != NULL)
        if(a->info == c){
            return 1 + buscav2Char(a->prox,c);
        }else{
            return 0 + buscav2Char(a->prox,c);
        }
    if(a->prim != NULL && a->prox != NULL)
        if(a->info == c){
            return 1 + buscav2Char(a->prim,c) + buscav2Char(a->prox,c);
        }else{
            return 0 + buscav2Char(a->prim,c) + buscav2Char(a->prox,c);
        }
    if(a->info == c){
        return 1 + buscav2Char(a->prim,c);
    }else{
        return 0 + buscav2Char(a->prim,c);
    }
}

//---FUNCOES PARA ARVORE INT---
struct arvGenInt{
	int info;
	struct arvGenInt * prim;
	struct arvGenInt * prox;
};
typedef struct arvGenInt ArvGenInt;

ArvGenInt * criarInt(int i){
	ArvGenInt * novo;
	novo = (ArvGenInt *) malloc(sizeof(ArvGenInt));
	novo->info = i;
	novo->prim = NULL;
	novo->prox = NULL;
	return novo;
}

void insereListaInt(ArvGenInt * a, ArvGenInt * sa){
	sa->prox = a->prim;
	a->prim = sa;	
}

void imprimeOrdemSimetricaInt(ArvGenInt * pA){
	ArvGenInt * p;
	printf("%d ", pA->info);
	for(p=pA->prim; p!=NULL; p=p->prox)
		imprimeOrdemSimetricaInt(p);
}

void ImprimirPosOrdemInt(ArvGenInt * pA){
     ArvGenInt * p;
     for(p=pA->prim; p!= NULL; p=p->prox)
       ImprimirPosOrdemInt(p);
       printf("%d ",pA->info);
}

//Exercicio 1 : Percorre a arvore criando e copiando os dados
ArvGenInt * copiarInt(ArvGenInt * A){
	ArvGenInt *b;
	if(A != NULL){
    	b = criarInt(A->info);
    	b->prim=copiarInt(A->prim);
    	b->prox=copiarInt(A->prox);
    	return b;
    }
}

//Exercicio 2 : Verifica cada possibilidade de diferen�a e retorna um boolean
bool verificaArvoresInt(ArvGenInt * pA, ArvGenInt * pB){
    return pA == NULL && pB == NULL ||
    (!pA==NULL && !pB == NULL &&
    pA->info == pB->info &&
    verificaArvoresInt(pA->prim, pB->prim) &&
    verificaArvoresInt(pA->prox, pB->prox));
}

//Exercicio 3 : Dado uma ra�z e um valor ele procura na arvore
bool buscaInt (ArvGenInt * a, int i){
     ArvGenInt * p;
     if(a->info == i){
       return true;
     }else{
      for(p=a->prim; p!= NULL; p=p->prox)
        if(buscaInt(p,i)){
          return true;
        }
     }
   return false;
}

int buscav2Int (ArvGenInt * a, int i){
     if(a->prim == NULL && a->prox == NULL)
        if(a->info == i){
            return 1;
        }else{
            return 0;
        }
    if(a->prim == NULL && a->prox != NULL)
        if(a->info == i){
            return 1 + buscav2Int(a->prox,i);
        }else{
            return 0 + buscav2Int(a->prox,i);
        }
    if(a->prim != NULL && a->prox != NULL)
        if(a->info == i){
            return 1 + buscav2Int(a->prim,i) + buscav2Int(a->prox,i);
        }else{
            return 0 + buscav2Int(a->prim,i) + buscav2Int(a->prox,i);
        }
    if(a->info == i){
        return 1 + buscav2Int(a->prim,i);
    }else{
        return 0 + buscav2Int(a->prim,i);
    }
}

int soma(ArvGen* A){

    if(A->prim == NULL && A->prox == NULL)
       return A->info;
    if(A->prim == NULL && A->prox != NULL)
       return A->info + soma(A->prox);
    if(A->prim != NULL && A->prox != NULL)
       return A->info + soma(A->prim) + soma(A->prox);

    return A->info + soma(A->prim);
}


int main(){
    int op = 0;
    //--CHAR
    ArvGen * a = NULL;
	ArvGen * b = NULL;
	ArvGen * c = NULL;
	ArvGen * d = NULL;
	ArvGen * e = NULL;
	ArvGen * f = NULL;
	ArvGen * g = NULL;
	ArvGen * h = NULL;
	ArvGen * i = NULL;
	ArvGen * j = NULL;
	ArvGen * copia = NULL;
	char aux = 'a';
	int buscav2c = 0;
	
	//--INT
	ArvGenInt * x1 = NULL;
	ArvGenInt * x2 = NULL;
	ArvGenInt * x3 = NULL;
	ArvGenInt * x4 = NULL;
	ArvGenInt * x5 = NULL;
	ArvGenInt * x6 = NULL;
	ArvGenInt * x7 = NULL;
	ArvGenInt * x8 = NULL;
	ArvGenInt * x9 = NULL;
	ArvGenInt * x10 = NULL;
    ArvGenInt * copiaInt = NULL;
    int auxi = 0;
    int somaInt = 0;
    int buscav2i = 0;
    
    do{
        printf("\nARVORE CHAR\n");
        printf("[1]-Criar arvore char\n");
        printf("[2]-Copiar arvore\n");
        printf("[3]-Verificar se sao iguais\n");
        printf("[4]-Buscar na arvore\n");
        printf("[5]-Imprimir Ordem-Simetrica\n");
        printf("[6]-Imprimir Pos-Ordem\n");
        printf("\nARVORE INT\n");
        printf("[7]-Criar arvore int\n");
        printf("[8]-Copiar arvore\n");
        printf("[9]-Verificar se sao iguais\n");
        printf("[10]-Buscar na arvore\n");
        printf("[11]-Somar valores\n");
        printf("[12]-Imprimir Ordem-Simetrica\n");
        printf("[13]-Imprimir Pos-Ordem\n");
        printf("\nEXTRA\n");
        printf("[14]-Busca v2 Char\n");
        printf("[15]-Busca v2 Int\n");
        printf("[0]-Sair\n");
            
        scanf("%d",&op);
        fflush(stdin);
        switch(op){
            //---OPCOES ARVORE CHAR---
            case 1:
                printf("\n[ OPCAO 1 ]\n");
                //Cria todos os n�s da arvore
            	a = criar('a');
            	b = criar('b');
            	c = criar('c');
            	d = criar('d');
            	e = criar('e');
            	f = criar('f');
            	g = criar('g');
            	h = criar('h');
            	i = criar('i');
            	j = criar('j');
            	
            	//Liga os n�s
            	insereLista(c,d);
            	insereLista(b,e);
            	insereLista(b,c);
            	insereLista(i,j);
            	insereLista(g,i);
            	insereLista(g,h);
            	insereLista(a,g);
            	insereLista(a,f);
            	insereLista(a,b);
            	printf("Arvore char criada!\n");
            	break;
            case 2:
                printf("\n[ OPCAO 2 ]\n");
                if(a != NULL){
                    //Excercio 1 : Chama a fun��o copiar passando o n� raiz ( a ) da arvore
        	        copia = copiar(a);
        	        printf("Arvore char copiada!\n");
                }else{
                    printf("Arvore char ainda nao criada!\n");
                }
                break;
            case 3:
                printf("\n[ OPCAO 3 ]\n");
                if(a == NULL){
                    printf("Arvore char ainda nao criada!\n");
                }else if(copia == NULL){
                    printf("Arvore char ainda nao foi copiada!\n");
                }else{
                    //Excercio 2 : verifica se as arvores s�o realmente iguais
                	if(verificaArvores(a,copia)){
                        printf("Arvores char sao iguais!\n");  
                    }else{
                        printf("Arvores char diferentes!\n");    
                    }
                }
                break;
            case 4:
                printf("\n[ OPCAO 4 ]\n");
                if(a != NULL){
                    //Exercicio 3 : Busca um elemento na arvore
        	        printf("Digite o char que quer procurar na arvore:\n");
                    scanf("%c",&aux);
        	        if(busca(a,aux)){
                        printf("Dado [%c] encontrado na arvore char\n",aux);    
                    }else{
                        printf("Dado [%c] nao encontrado na arvore char\n",aux);    
                    }
                }else{
                    printf("Arvore char ainda nao criada!\n");
                }
                break;
            case 5:
                printf("\n[ OPCAO 5 ]\n");
                //Imprimir em Ordem Simetrica a arvore A e a c�pia
                printf("[ ORDEM SIMETRICA (CHAR) ]\n");
            	printf("Arvore Principal [ ");
            	if(a != NULL){
                    imprimeOrdemSimetrica(a);
                }else{
                    printf("VAZIA");
                }
            	printf("]\nArvore Copia [ ");
            	if(copia != NULL){
                    imprimeOrdemSimetrica(copia);
                }else{
                    printf("VAZIA");
                }
            	printf("]\n");
                break;
            case 6:
                printf("\n[ OPCAO 6 ]\n");
                //Imprimir em Pos Ordem a arvore A e a c�pia
                printf("[ POS ORDEM (CHAR) ]\n");
            	printf("Arvore Principal [ ");
            	if(a != NULL){
                    ImprimirPosOrdem(a);
                }else{
                    printf("VAZIA");
                }
            	printf(" ]\nArvore Copia [ ");
            	if(copia != NULL){
                    ImprimirPosOrdem(copia);
                }else{
                    printf("VAZIA");
                }
            	printf("]\n");
                break;
            //---OPCOES ARVORE INT---
            case 7:
                printf("\n[ OPCAO 7 ]\n");
                //Cria todos os n�s da arvore
            	x1 = criarInt(1);
            	x2 = criarInt(1);
            	x3 = criarInt(1);
            	x4 = criarInt(1);
            	x5 = criarInt(1);
            	x6 = criarInt(6);
            	x7 = criarInt(7);
            	x8 = criarInt(8);
            	x9 = criarInt(9);
            	x10 = criarInt(10);
            	
            	//Liga os n�s
            	insereListaInt(x3,x4);
            	insereListaInt(x2,x5);
            	insereListaInt(x2,x3);
            	insereListaInt(x9,x10);
            	insereListaInt(x7,x9);
            	insereListaInt(x7,x8);
            	insereListaInt(x1,x7);
            	insereListaInt(x1,x6);
            	insereListaInt(x1,x2);
            	printf("Arvore int criada!\n");
            	break;
            case 8:
                printf("\n[ OPCAO 8 ]\n");
                if(x1 != NULL){
                    //Excercio 1 : Chama a fun��o copiar passando o n� raiz ( x1 ) da arvore
        	        copiaInt = copiarInt(x1);
        	        printf("Arvore int copiada!\n");
                }else{
                    printf("Arvore int ainda nao criada!\n");
                }
                break;
            case 9:
                printf("\n[ OPCAO 9 ]\n");
                if(x1 == NULL){
                    printf("Arvore int ainda nao criada!\n");
                }else if(copiaInt == NULL){
                    printf("Arvore int ainda nao foi copiada!\n");
                }else{
                    //Excercio 2 : verifica se as arvores s�o realmente iguais
                	if(verificaArvoresInt(x1,copiaInt)){
                        printf("Arvores int sao iguais!\n");  
                    }else{
                        printf("Arvores int sao diferentes!\n");    
                    }
                }
                break;
            case 10:
                printf("\n[ OPCAO 10 ]\n");
                if(x1 != NULL){
                    //Exercicio 3 : Busca um elemento na arvore
        	        printf("Digite um numero inteiro que quer procurar na arvore:\n");
                    scanf("%d",&auxi);
        	        if(buscaInt(x1,auxi)){
                        printf("Dado [%d] encontrado na arvore int\n",auxi);    
                    }else{
                        printf("Dado [%d] nao encontrado na arvore int\n",auxi);    
                    }
                }else{
                    printf("Arvore int ainda nao criada!\n");
                }
                break;
            case 11:
                printf("\n[ OPCAO 11 ]\n");
                if(x1 != NULL){
                    //Excercio 4 : Chama a fun��o soma passando o n� raiz ( x1 ) da arvore para retornar a soma de todos os numeros da arvore
        	        somaInt = soma(x1);
        	        printf("A soma de todos os dados da arvore int eh [%d]!\n", somaInt);
                }else{
                    printf("Arvore int ainda nao criada!\n");
                }
                break;
            case 12:
                printf("\n[ OPCAO 12 ]\n");
                //Imprimir em Ordem Simetrica a arvore x1 e a c�pia
                printf("[ ORDEM SIMETRICA (INT) ]\n");
            	printf("Arvore Principal [ ");
            	if(x1 != NULL){
                    imprimeOrdemSimetricaInt(x1);
                }else{
                    printf("VAZIA");
                }
            	printf(" ]\nArvore Copia [ ");
            	if(copiaInt != NULL){
                    imprimeOrdemSimetricaInt(copiaInt);
                }else{
                    printf("VAZIA");
                }
            	printf("]\n");
                break;
            case 13:
                printf("\n[ OPCAO 13 ]\n");
                //Imprimir em Pos Ordem a arvore A e a c�pia
                printf("[ POS ORDEM (INT) ]\n");
            	printf("Arvore Principal [ ");
            	if(x1 != NULL){
                    ImprimirPosOrdemInt(x1);
                }else{
                    printf("VAZIA");
                }
            	printf(" ]\nArvore Copia [ ");
            	if(copiaInt != NULL){
                    ImprimirPosOrdemInt(copiaInt);
                }else{
                    printf("VAZIA");
                }
            	printf("]\n");
                break;
            case 14:
                printf("\n[ OPCAO 14 ]\n");
                if(a != NULL){
                    //Exercicio 3 : Busca um elemento na arvore e retorna quantas vezes esta repetido na arvore
        	        printf("Digite o char que quer procurar na arvore:\n");
                    scanf("%c",&aux);
                    buscav2c = buscav2Char(a,aux);
                    printf("Dado [%c] encontrado [%d] vez(es) na arvore char\n",aux,buscav2c);
                }else{
                    printf("Arvore char ainda nao criada!\n");
                }
                break;
            case 15:
                printf("\n[ OPCAO 15 ]\n");
                if(x1 != NULL){
                    //Exercicio 3 : Busca um elemento na arvore e retorna quantas vezes esta repetido na arvore
        	        printf("Digite um numero inteiro que quer procurar na arvore:\n");
                    scanf("%d",&auxi);
                    buscav2i = buscav2Int(x1,auxi);
                    printf("Dado [%d] encontrado [%d] vez(es) na arvore int\n",auxi,buscav2i);
                }else{
                    printf("Arvore int ainda nao criada!\n");
                }
                break;
            default:
                printf("\nOpcao incorreta!\n");
        }       
        printf("\n*************\n");
    }while(op != 0);
    system("pause");
	return 0;
}
