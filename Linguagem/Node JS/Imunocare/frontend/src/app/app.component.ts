import { Component } from '@angular/core';

@Component({
  selector: 'ic-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'ic';
}
