import { CanLoad, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { LoginService } from './login.service';

@Injectable()
export class AcessoGuard implements CanLoad, CanActivate {

    constructor(private loginService: LoginService) {}

    verificaAutenticacao(path: string): boolean {
        const usuarioLogado = this.loginService.usuarioEstaLogado()
        if (!usuarioLogado) {
            this.loginService.controlarLogin(`/${path}`)
        }
        return usuarioLogado
    }

    canLoad(route: Route): boolean {
        return this.verificaAutenticacao(route.path)
    }

    canActivate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): boolean {
        console.log(activatedRoute)
        return this.verificaAutenticacao(activatedRoute.routeConfig.path)
    }

}
