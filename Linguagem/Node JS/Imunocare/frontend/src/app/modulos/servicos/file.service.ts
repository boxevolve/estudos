
import { Injectable } from '@angular/core'
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http'
import { Observable } from 'rxjs'
import { IC_API } from '../../app.api'
import { LoginService } from './login.service';
import { FileModel } from '../features/arquivo/arquivo.model';



@Injectable()
export class FileService {
 
  constructor(private http: HttpClient, private loginService: LoginService) { }
 
  pushFileToStorage(file: FileModel): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    
    file.newname = this.loginService.getUsuario().usuario+'-'+file.name+'-'+Date.now()

    formdata.append('tipo', file.tipo)
    formdata.append('usuario', this.loginService.getUsuario()._id)
    formdata.append('file', file);
    const req = new HttpRequest('POST', `${IC_API}/api/file/upload`, formdata, {
      reportProgress: true,
      responseType: 'text'
    });
 
    return this.http.request(req);
  }
 
  getFiles(): Observable<any> {
    return this.http.get(`${IC_API}/api/file/download`);
  }
}