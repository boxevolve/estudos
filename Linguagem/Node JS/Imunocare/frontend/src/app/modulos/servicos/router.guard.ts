import { CanLoad, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { LoginService } from './login.service';

@Injectable()
export class RouterGuard implements CanLoad, CanActivate {

    constructor(private loginService: LoginService) {}

    verificaPermissao(path: string): boolean {
        return this.loginService.verificaPermissao(path);
    }

    canLoad(route: Route): boolean {
        return this.verificaPermissao(`/${route.path}`);
    }

    canActivate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): boolean {
        return this.verificaPermissao(this.createTreePath(activatedRoute));
    }

    createTreePath(activatedRoute: ActivatedRouteSnapshot): string {
        let tree = [];
        let treePath = '';
        tree.push(`/${activatedRoute.routeConfig.path}`);
        this.treePath(activatedRoute, tree);
        tree.reverse();
        tree.forEach(path => {
            treePath += path;
        });
        return treePath;
    }

    treePath(activatedRoute: ActivatedRouteSnapshot, tree: string[]): void {
        const path = activatedRoute.parent.routeConfig.path;
        if (path) {
            tree.push(`/${path}`);
            this.treePath(activatedRoute.parent, tree);
        }
    }

}
