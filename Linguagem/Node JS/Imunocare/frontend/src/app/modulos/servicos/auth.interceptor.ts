//
//
import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { LoginService } from './login.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    // Injector resolve problemas de injeção de dependencias cíclicas
    // ele substitui a dependencia específica (neste caso seria LoginService)
    constructor(private injector: Injector) {}

    intercept( request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // aqui passamos a referencia de LoginService através do objeto Injector para corrigir a injeção de dependência
        const loginService = this.injector.get(LoginService)

        console.log('Intercepting: ', request)

        if (loginService.usuarioEstaLogado()) {
            const authRequest = request.clone(
                { setHeaders: {'x-access-token': `${loginService.usuario.token}`} })
            return next.handle(authRequest)
        } else {
            return next.handle(request)
        }
    }
}

