//

import { EventEmitter } from '@angular/core'
import { MensagemModel } from '../shared/modelos/mensagem.model';

export class NotificationService {
    notifier = new EventEmitter<MensagemModel>()

    notifiy(notificacao: MensagemModel) {
        this.notifier.emit(notificacao)
    }
}
