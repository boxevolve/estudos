import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Usuario } from '../shared/modelos/profile.model';
import { IC_API } from '../../app.api';
import { Router } from '@angular/router';
import { NotificationService } from './notification.service';

@Injectable()
export class LoginService {

  usuario: Usuario;
  lastPath: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    private ns: NotificationService
  ) { }

  public reset() {
    this.usuario = undefined;
    this.lastPath = undefined;
  }

  public usuarioEstaLogado(): boolean {
    return this.usuario !== undefined;
  }

  public getUsuario(): Usuario {
    this.usuario.img = 'assets/img/IMG_5561.jpeg';
    return this.usuario;
  }

  public buscaDadosUsuario(token): Observable<Usuario> {
    return this.http.get<Usuario>(`${IC_API}/api/seguranca/usuarios/buscaDadosUsuario`).do(usuario => {
      this.usuario._id = usuario._id;
      this.usuario.campanhas = usuario.campanhas;
      this.usuario.email = usuario.email;
      this.usuario.nome = usuario.nome;
      this.usuario.perfil = usuario.perfil;
      this.usuario.usuario = usuario.usuario;
    });
  }

  public loginUsuario(email: string, senha: string): Observable<Usuario> {
    return this.http
      .post<Usuario>(`${IC_API}/api/acesso/login`, { email: email, senha: senha })
      .do(usuario => (this.usuario = usuario))
      .switchMap(usuario => this.buscaDadosUsuario(usuario.token));
  }

  public alterarSenha(email: string, senha: string, novasenha: string): Observable<any> {
    return this.http
      .post<any>(`${IC_API}/api/acesso/changePassword`, { email: email, senha: senha, novasenha: novasenha })
      .do(usuario => (this.usuario = usuario));
    // .switchMap(usuario => this.buscaDadosUsuario(usuario.token))
  }

  public forgot(email: string): Observable<any> {
    return this.http.post<any>(`${IC_API}/api/acesso/forgot`, { email: email });
  }

  public logout(): Observable<any> {
    return this.http.post<any>(`${IC_API}/api/acesso/logout`, {});
  }

  public controlarLogin(path?: string) {
    this.router.navigate(['/login', btoa(path)]);
  }

  public verificaPermissao(path?: string): boolean {
    if (path !== 'event') {
      if (this.usuario === undefined) { return false }
      switch (path) {
        case '/admin': break;
        case '/gerador': break;
        case '/usuarios': break;
        case '/cadastros': break;
        case '/produtos/': break;
        case '/produtos/add': break;
        case '/produtos/edit/:id': break;
        case '/parceiros/': break;
        case '/parceiros/add': break;
        case '/parceiros/edit/:id': break;
        case '/campanha/': break;
        case '/campanha/add': break;
        case '/campanha/edit/:id': break;
        case '/campanha/edit/:id/detalharfilial/:id': break;
        case '/calendario':
          return this.isAdmin() || this.isManager();
        case '/dash':
          return !this.isNurse();
        case '/campanhas':
          return !this.isNurse();
        case '/imunizacoes': break;
        case '/vacinacao/': break;
        case '/fora-elegibilidade/': break;
        default:
          return false;
      }
    }
    return true;
  }

  /**
   * Administrador
   */
  public isAdmin(): boolean {
    return this.validateProfile('ADMIN');
  }

  /**
   * Gestor
   */
  public isManager(): boolean {
    return this.validateProfile('GEST');
  }

  /**
   * Enfermeiro
   */
  public isNurse(): boolean {
    return this.validateProfile('ENFE');
  }

  public validateProfile(profile: string): boolean {
    return this.usuario ? this.usuario.perfil === profile : false;
  }

}
