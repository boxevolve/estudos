import { EnderecoModel } from './endereco.model';

interface EmpresaModel {
    _id?: string,
    empresa: string,
    cnpj: string,
    tipo: string,
    responsavel: string,
    email: string,
    fone: string,
    endereco?: EnderecoModel,
    numero: number,
    complemento: string,
    criado_por?: string,
    data?: Date
}

interface EmpresaTableModel {
    _id?: string,
    empresa: string,
    cnpj: string,
    responsavel: string,
    email: string,
    fone: string
}

interface ParceiroTableModel {
    _id?: string,
    empresa: string,
    cnpj: string,
    tipo: string,
    responsavel: string,
    email: string,
    fone: string
}

interface FilialTableModel {
    _id?: string,
    empresa: string,
    cnpj: string,
    tipo: string,
    responsavel: string,
    fone: string
}

export { EmpresaModel, EmpresaTableModel, ParceiroTableModel, FilialTableModel }
