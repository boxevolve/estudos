import { EmpresaModel } from '../cadastros/empresa.model';
import { ProdutoModel } from '../produtos/produto.model';
import { EnderecoModel } from '../cadastros/endereco.model';
import { AgrupamentoModel } from '../../../components/cadastro/campanha/model/agrupamento.model';

interface LoteModel {
  _id: string,
  lote: string,
  lote_qtde: number,
  lote_aplicacoes: number,
  duplicidade?: number
}

interface ProdutoTableModel {
  _id: string,
  produto?: ProdutoModel,
  produto_qtde: number,
  produto_aplicacoes: number,
  elegiveis_m: number,
  elegiveis_f: number,
  elegiveis_d: number,
  elegiveis_c: number,
  duplicidade?: number,
  elegiveis_mp: number,
  elegiveis_fp: number,
  elegiveis_dp: number,
  elegiveis_cp: number,
  duplicidade_dp?: number,
  lotes: LoteModel[]
}

interface LoteProdutoListModel {
  idCampanha: string,
  idProduto: string,
  produto: string,
  idLote: string,
  lote: string,
  lote_qtde: number
}

interface CnpjTotalModel {
  cnpj: string,
  cnpj_aplicacoes: number
}

interface StatusCampanhaModel {
  status: string
}

interface GeracaoCampanhaModel {
  step: string,
  status: string
}

interface UnidadeModel {
  _id?: string,
  cod_unid: string,
  nome_unid: string,
  endereco: EnderecoModel,
  numero: number,
  complemento: string,
  previsao_doses: number
}

interface FilialCampanhaModel {
  _id: string,
  filial: string,
  cnpj: string,
  responsavel: string,
  email: string,
  fone: string,
  unidades: UnidadeModel[],
  usuario_atendimento: string,
  atendimento_realizado: boolean,
  previsao_doses: number
}

interface CampanhaModel {
  _id?: string,
  campanha: string,
  cliente?: EmpresaModel,
  distribuidor?: EmpresaModel,
  responsavel_cliente?: string,
  email_cliente?: string,
  telefone_cliente?: string,
  gestor?: EmpresaModel,
  responsavel_gestor?: string,
  email_gestor?: string,
  telefone_gestor?: string,
  foto_gestor?: string,
  fornecedores?: any[],
  cnpj_total: [CnpjTotalModel],
  status: string,
  geracao: GeracaoCampanhaModel,
  unidade: string,
  data_inicio: Date,
  data_fim: Date,
  horario_funcionamento_ini: string,
  horario_funcionamento_fim: string,
  nf: string,
  in_company: boolean,
  cadastro_auto?: boolean,
  agrupamento?: AgrupamentoModel,
  pesquisa_satisfacao?: boolean,
  pesquisa_saude?: boolean,
  total_elegiveis: number,
  total_qtde: number,
  total_aplicacoes: number,
  valor_unitario_servico: number,
  elegiveis_m: number,
  elegiveis_f: number,
  elegiveis_d: number,
  elegiveis_mp: number,
  elegiveis_fp: number,
  elegiveis_dp: number,
  produtos?: ProdutoTableModel[],
  filiais?: FilialCampanhaModel[],
  senha_vacinacao?: string,
  observacao?: string
}

interface CampanhaTableModel {
  _id?: string,
  campanha: string,
  cliente: string,
  cnpj: string
}

interface CampanhaNewTableModel {
  _id?: string,
  cliente: string,
  campanha: string,
  status: string,
  gestor: string,
  responsavel_gestor: string
}

interface KpiBoxModel {
  nome: string,
  valor: number,
  acesso: boolean,
  redirect?: string
}

interface ImportacaoElegivelModel {
  nomecampanha: string,
  data: string,
  nome: string,
  nomeoriginal: string,
  tipo?: string,
  sucesso?: string,
  erros?: string,
  status: string,
}

interface ClinicaTableModel {
  _id?: string,
  clinica: string,
  nome: string
}

export {
  StatusCampanhaModel,
  GeracaoCampanhaModel,
  CampanhaTableModel,
  FilialCampanhaModel,
  UnidadeModel,
  LoteModel,
  ProdutoTableModel,
  CnpjTotalModel,
  CampanhaModel,
  LoteProdutoListModel,
  KpiBoxModel,
  CampanhaNewTableModel,
  ImportacaoElegivelModel,
  ClinicaTableModel
}
