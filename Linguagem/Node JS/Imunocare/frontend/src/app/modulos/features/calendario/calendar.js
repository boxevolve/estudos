/*
 *	Fun��o calend�rio:
 *
 *	Retorna o c�digo HTML para montar um calend�rio de um m�s ou ano
 *
 *  Cal.monthCal( [month , year] )
 *  Cal.yearCal( [year] )
 *  Cal.setMonth( [month [, year] )
 *  Cal.setYear( [year] )
 *
 *	month = 0...11 | 'today' | 'next' | 'prev' | 'now'
 *	year  = number | 'today' | 'next' | 'prev' | 'now'
 */

function Calendar(name,year,month){
	this.Month = Today.getMonth();
	this.Year = Today.Year;
	this.ThisDate = 0;
	this.weekHeadMonth = new Array("Dom","Seg","Ter","Qua","Qui","Sex","Sab");
	this.weekHeadYear = new Array("D","S","T","Q","Q","S","S");
	this.monthNames = new Array("Janeiro","Fevereiro","Mar&ccedil;o","Abril","Maio",
	"Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");
	this.firstDay = 0;
	this.weekColor = "black";
	this.weekendColor = "red";
	this.bgColor = "";
	this.borderMonth = 2;
	this.spacingMonth = 1;
	this.paddingMonth = 2;
	this.borderYear = 3;
	this.spacingYear = 2;
	this.paddingYear = 7;
	this.cellWidth = 0;
	this.fontFamily = "arial";
	this.fontSize = 3;
	this.fontWeight = "plain";
	this.todayFontColor = "";
	this.todayFontSize = 3;
	this.todayFontWeight = "strong";
	this.headFontColor = "black";
	this.headFontFamily = "arial";
	this.headFontSize = 3;
	this.headFontWeight = "strong";

	this.monthLen = Calendar.monthLen;
	this.monthName = Calendar.monthName;
	this.setMonthNames = Calendar.setMonthNames;
	this.setYearWeek = Calendar.setYearWeek;
	this.setMonthWeek = Calendar.setMonthWeek;
	this.monthCal = Calendar.monthCal;
	this.setMonth = Calendar.setMonth;
	this.setYear = Calendar.setYear;
	this.yearCal = Calendar.yearCal;

	if (!window.calendars) window.calendars = new Array();
	this.name = name || "Calendar"+ window.calendars.length;
	window.calendars[this.name] = this;
	window.calendars[window.calendars.length] = this;

	this.setMonth(month,year);
}

Calendar.monthLen = function() {
	var len = ((this.Month % 7) % 2 == 1)? 30 : 31;
	if (this.Month == 1) len = (this.Year % 4 == 0)? 29 : 28;
	return len;
}

Calendar.monthName = function() {
	return this.monthNames[this.Month];
}

Calendar.setMonthNames = function() {
	var args = Calendar.setMonthNames.arguments;
	for (var i = 0; i<args.length; i++)
		this.monthNames[i] = args[i];
}

Calendar.setYearWeek = function(su,mo,tu,we,th,fr,sa) {
	var args = Calendar.setYearWeek.arguments;
	for (var i = 0; i<args.length; i++)
		this.weekHeadYear[i] = args[i];
}

Calendar.setMonthWeek = function(su,mo,tu,we,th,fr,sa) {
	var args = Calendar.setMonthWeek.arguments;
	for (var i = 0; i<args.length; i++)
		this.weekHeadMonth[i] = args[i];
}

Calendar.setMonth = function(month, year) {
	if (month) {
		if(typeof(month) == 'number' && (month >= 0 && month < 12))
			this.Month = month;
		else if (month == 'today') {
			this.Year = Today.Year;
			this.Month = Today.getMonth();
		}
		else if (month == 'prev') {
			if (--this.Month == -1) {
				this.Month = 11;
				--this.Year;
			}
		}
		else if (month == 'next') {
			if (++this.Month == 12) {
				this.Month = 0;
				++this.Year;
			}
		}
		else if (month != 'now')
			alert('Unknown month: '+ month);
	}
	if (typeof(year) != 'undefined')  this.setYear(year);
	this.ThisDate = (this.Month == Today.getMonth() && this.Year == Today.Year)?  Today.getDate() : 0;
}

Calendar.setYear = function(year) {
	if(typeof(year) != 'undefined') {
		if (typeof(year) == 'number') {
			this.Year = (year < 100 && year >= 0)?  year + 1900 : year;
		}
		else if (year == 'today')
			this.Year = Today.Year;
		else if (year == 'prev')
			--this.Year;
		else if (year == 'next')
			++this.Year;
		else if (year != 'now')
			alert('Unknown year: '+ year);
	}
	this.ThisDate = 0;
}

Calendar.monthCal = function(month, year) {
	this.setMonth(month, year);
	var first = new Date(this.Year,this.Month,1);
	var day = 0;
	var lastDate = this.monthLen();
	var color;
	var str = '<TABLE WIDTH='+ (7*this.cellWidth + 8*this.spacingMonth + 14*this.paddingMonth) +' CELLSPACING='+ this.spacingMonth +' CELLPADDING='+ this.paddingMonth +' BORDER='+ this.borderMonth +' BGCOLOR="'+ this.bgColor +'">';
	str += '<TR><TH COLSPAN=7>'+ fontTag(this.headFontFamily,this.headFontSize,this.headFontColor,this.headFontWeight) +
			 this.monthName() +'  '+ this.Year +'</FONT></TH></TR>\n<TR ALIGN="center">\n';
	for (day=this.firstDay; day<this.firstDay+7; day++)
		str += ' <TD WIDTH='+ this.cellWidth +'>'+ fontTag(this.fontFamily,this.fontSize,this.headFontColor,this.headFontWeight) +
				 this.weekHeadMonth[day%7] +'</FONT></TD>';
	day = first.getDay();
	if (day != this.firstDay) {
		str += '\n</TR>\n<TR ALIGN="center">\n' +
			' <TD COLSPAN='+ ((day+7-this.firstDay)%7) +'>&nbsp;</TD>';
	}
	for (date=1; date <= lastDate; date++, day=(++day)%7) {
		if (day == this.firstDay)
			str += '\n</TR>\n<TR ALIGN="center">\n';
		str += ' <TD WIDTH='+ this.cellWidth +'>';
		color = (day==0 || day==6)? this.weekendColor : this.weekColor;
		if (date == this.ThisDate) {
			color = this.todayFontColor || color;
		  str += fontTag(this.fontFamily, this.todayFontSize, color, this.todayFontWeight);
		} else {
		  str += fontTag(this.fontFamily, this.fontSize, color, this.fontWeight);
		}
		str += date +'</FONT></TD>';
	}
	if ((this.firstDay-day)%7 != 0 )
		str += ' <TD COLSPAN='+ ((this.firstDay+7-day)%7) +'>&nbsp;</TD>';
	str += '\n</TR>\n</TABLE>\n';
	return str;
}
Calendar.yearCal = function (year) {
	this.setYear(year);
	var saveMonth = this.Month;
	var saveBorder = this.borderMonth;
	this.borderMonth = 0;
	var saveWeekHead = this.weekHeadMonth;
	this.weekHeadMonth = this.weekHeadYear;
	var str = '<TABLE BORDER="'+ this.borderYear +'" CELLPADDING='+ this.paddingYear +' CELLSPACING="'+ this.spacingYear +' BGCOLOR="'+ this.bgColor +'">\n' +
			'<CAPTION><H2>'+ fontTag(this.headFontFamily,'',this.headFontColor,this.headFontWeight) + this.Year +'</H2></CAPTION>\n';
	for (var row = 0; row < 4; row++) {
		str += '<TR VALIGN=TOP>\n';
		for (this.Month = 3*row; this.Month < 3*row + 3; this.Month++)
			str += '<TD>'+ this.monthCal() +'</TD>\n';
		str += '</TR>\n';
	}
	str += '</TABLE>\n';
	this.Month = saveMonth;
	this.borderMonth = saveBorder;
	this.weekHeadMonth = saveWeekHead;
	return str;
}

function fontTag(family,size,color,weight) {
	var font = '<FONT';
	if (family) font += ' FACE="'+ family +'"';
	if (size) font += ' SIZE="'+ size +'"';
	if (color) font += ' COLOR="'+ color +'"';
	font += '>'
	if (weight && weight != "plain") font += '<'+ weight +'>';
	return font;
}

if (!Today) var Today = new Date();
if (!Today.Year) {
	Today.Year = Today.getYear() % 100;
	Today.Year += (Today.Year < 38) ? 2000 : 1900;
}
