import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { SharedModule } from '../../shared/shared.module'
import { CalendarioComponent } from '../../../pages/calendario/calendario.component'
import * as $ from 'jquery'

import { FullCalendarModule } from 'ng-fullcalendar'
import { EventoComponent } from '../../../pages/calendario/evento/evento.component';
import { Evento2Component } from '../../../pages/calendario/evento2/evento2.component';

const ROUTES: Routes = [
    {path: '', component: CalendarioComponent},
    {path: 'eventos', component: EventoComponent}
]
 
@NgModule({
    declarations: [
        CalendarioComponent,
        EventoComponent
    ],
    imports: [ FullCalendarModule, SharedModule, 
        RouterModule.forChild(ROUTES)
    ],
    providers: [    
    ]
})


export class CalendarioModule {

    public constructor() {}

}
