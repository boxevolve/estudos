

import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { RouterGuard } from '../../servicos/router.guard'
import { SharedModule } from '../../shared/shared.module'
import { FormUpFilesComponent } from '../../../pages/admin/upload/form-up-files/form-up-files.component'
import { CargaComponent } from '../../../pages/admin/upload/carga/carga.component'


const ROUTES: Routes = [
    { path: '',         component: FormUpFilesComponent}]

@NgModule({
    declarations: [
        FormUpFilesComponent,
        CargaComponent
    ],
    imports: [ SharedModule,
        RouterModule.forChild(ROUTES)
    ]
})
 
export class GeradorModule {

    public constructor() {}

}
