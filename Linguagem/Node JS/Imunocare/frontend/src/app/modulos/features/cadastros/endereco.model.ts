interface EnderecoModel {
    _id?: string,
    endereco: string,
    bairro: string,
    cidade: string,
    estado: string,
    cep: string,
    criado_por?: string,
    data?: Date
}

export { EnderecoModel }