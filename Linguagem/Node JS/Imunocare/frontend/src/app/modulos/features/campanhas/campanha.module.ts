
import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { SharedModule } from '../../shared/shared.module'
import { CampanhasComponent } from '../../../pages/campanha/campanhas/campanhas.component'
import { CampanhaMenuComponent } from '../../../pages/campanha/campanha-menu/campanha-menu.component'

const ROUTES: Routes = [
    {path: '', component: CampanhasComponent,
        children: [
            // {path: 'campanha-detail', component: CampanhasComponent},
            // loadChildren: './modulos/features/elegiveis/elegivel.module#ElegivelModule',
            // canActivate: [AcessoGuard], canLoad: [AcessoGuard]},
        ]
    }
]

@NgModule({
    declarations: [
        CampanhasComponent,
        CampanhaMenuComponent
    ],
    imports: [ SharedModule,
        RouterModule.forChild(ROUTES)
    ]
})


export class CampanhaModule {

    public constructor() {}

}
