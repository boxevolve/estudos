import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/switchMap'
import { IC_API } from '../../../app.api';
import { UsuarioModel } from './usuario.model';
import { CampanhaModel } from '../campanhas/campanha.model';
import { NotificationService } from '../../servicos/notification.service';
import { Util } from '../../../utils/util';
import { Constants } from '../../../utils/constants';
import { Usuario } from '../../shared/modelos/profile.model';


@Injectable()
export class UsuarioService {
  url = `${IC_API}/api/seguranca/usuarios`;

  usuario: UsuarioModel
  usuarios: UsuarioModel[]
  campanhas: CampanhaModel[]

  public constructor(
    private http: HttpClient,
    private notificationService: NotificationService,
  ) { }

  getUsuarios(id?: '', usuario?: ''): Observable<UsuarioModel[]> {
    return this.http.get<any>(`${this.url}/retrieve`)
                    .do(response => this.usuarios = this.mapearUsuario(response.retorno))
                    .map(response => this.usuarios);
  }

  getById(id: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_id': id}
    ]);
    this.http.get<any>(`${this.url}/findOne`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  addCampanha(usuario: Usuario, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_id': usuario._id}
    ]);
    this.http.post<Usuario>(`${this.url}/addCampanha`, usuario, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_SAVE)}
    );
  }
  
  updateByDeleteCampanha(idCampanha: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_idCampanha': idCampanha}
    ]);
    this.http.delete<UsuarioModel>(`${IC_API}/api/seguranca/usuarios/updateByDeleteCampanha`, { params })
      .subscribe((result) => {
          this.successCallback(result, next, showCallback)
        },
        error => {
          this.errorCallback(error, Constants.MSG_ERROR_RECOVER)
        }
    );
  }

  mapearUsuario(usuarios: UsuarioModel[]) {
    const usuariosList: UsuarioModel[] = []
    for (const element of usuarios) {
      const usuario: UsuarioModel = this.retornaUsuarioNull()
      usuario._id = element._id
      usuario.usuario = element.usuario
      usuario.nome = element.nome
      usuario.email = element.email
      usuario.email_cliente = element.email_cliente
      usuario.perfil = element.perfil
      // usuario.campanhas = element.campanhas
      usuario.primeiro_acesso = element.primeiro_acesso
      usuario.email_enviado = element.email_enviado
      usuario.loginAttempts = element.loginAttempts
      usuario.lockUntil = element.lockUntil
      for ( let campanha of element.campanhas) {
        usuario.campanhas.push(campanha)
      }
      usuariosList.push(usuario)
    }
    return usuariosList
  }

  retornaUsuarioNull(): UsuarioModel {
    const us: UsuarioModel = {
      '_id': '',
      'usuario': '',
      'nome': '',
      'email': '',
      'email_cliente': '',
      'perfil': '',
      'campanhas': [],
      'primeiro_acesso': '',
      'email_enviado': '',
      'loginAttempts': '',
      'lockUntil': ''
    }
    return us
  }

  setEditableUser(usuario: UsuarioModel) {
    this.usuario = usuario
  }

  successCallback(result: any, next?: (result: any) => void, showCallback?: boolean): void {
    if (showCallback) {
      this.notificationService.notifiy({
        message: Constants.MSG_SUCCESS_OPERATION
      });
    }
    if (next) {
      next(result);
    }
  }

  errorCallback(error: any, message: string): void {
    message = `${message} o usuário.`;
    if (error
     && error.error
     && error.error.error
     && error.error.error.code === 11000) {
      message = Constants.MSG_DUPLICATED;
    }

    this.notificationService.notifiy({
      message:  message,
      tipo: 'E',
    });
  }

}
