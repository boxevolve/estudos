interface StepModel {
    seq: number,
    step: string,
    status: string
}

export { StepModel }