

export interface FileModel extends File {
    newname?: string,
    tipo?: string,
    name: string,
    progress?: { percentage?: number }
}