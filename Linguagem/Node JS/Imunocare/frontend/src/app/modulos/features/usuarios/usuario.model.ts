import { CampanhaModel } from '../campanhas/campanha.model';

export interface UsuarioModel {
    _id: string,
    usuario: string,
    nome: string,
    email: string,
    email_cliente?: string,
    perfil: string,
    campanhas?: CampanhaModel[],
    primeiro_acesso?: string,
    email_enviado?: string,
    loginAttempts?: string,
    lockUntil?: string
}

