import { Injectable } from '@angular/core';
import { InfoBox } from '../../shared/modelos/infobox.model';


@Injectable()
export class DashBoardService {

    public constructor() {}

    setBindings(indx: number): InfoBox {
        const infobox: InfoBox = {corBox: '', iconBox: ''}

        if (indx === 0) {
            infobox.corBox = 'aqua'           // 'info-box-icon bg-aqua'
            infobox.iconBox =  'ion ion-ios-gear-outline'
        } else {
            if (indx % 1  === 0) {
                infobox.corBox = 'red'           // 'info-box-icon bg-red'
                infobox.iconBox = 'ion ion-ios-pulse'
            }
            if (indx % 2  === 0) {
                infobox.corBox = 'green'           // 'info-box-icon bg-green'
                infobox.iconBox = 'ion ion-ios-people-outline'
            }
            if (indx % 3  === 0) {
                infobox.corBox = 'yellow'           // 'info-box-icon bg-yellow'
                infobox.iconBox = 'ion ion-ios-pie-outline'
            }
            if (indx % 4  === 0) {
                infobox.corBox = 'purple'           // 'info-box-icon bg-yellow'
                infobox.iconBox = 'ion ion-ios-cog-outline'
            }
        }
        return infobox
    }
}
