

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RouterGuard } from '../../servicos/router.guard';

import { SharedModule } from '../../shared/shared.module';
import { UsuarioComponent } from '../../../pages/admin/usuario/usuario.component';
import { UsuarioDetailsComponent } from '../../../pages/admin/usuario-details/usuario-details.component';

const ROUTES: Routes = [
    {path: '', component: UsuarioComponent},
    {path: 'detalhes', component: UsuarioDetailsComponent}
]


@NgModule({
    declarations: [
        UsuarioComponent,
        UsuarioDetailsComponent
    ],
    imports: [ SharedModule,
        RouterModule.forChild(ROUTES)
    ]
})


export class UsuarioModule {

    public constructor() {}

}
