import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { LoginService } from '../../servicos/login.service'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/map'

import { CampanhaModel, LoteModel, LoteProdutoListModel, KpiBoxModel, ProdutoTableModel } from './campanha.model'
import { IC_API } from '../../../app.api'
import { ProdutoModel } from '../produtos/produto.model';
import { EmpresaModel } from '../cadastros/empresa.model';
import { EnderecoModel } from '../cadastros/endereco.model';
import { AgrupamentoModel } from '../../../components/cadastro/campanha/model/agrupamento.model';
import { Util } from '../../../utils/util';
import { NotificationService } from '../../servicos/notification.service';
import { Constants } from '../../../../app/utils/constants';
import { EventoModel } from '../calendario/evento.model';

@Injectable()
export class CampanhaService {

  numCampanhas: number = 0
  totalDeAplicacoes: number = 0
  totalQtde: number = 0
  totalElegiveis: number = 0
  valor_faturado: number = 0

  private _readonly: boolean = false;

  kpis: KpiBoxModel[] = []

  campanhas: CampanhaModel[]
  produto: ProdutoTableModel
  lote: LoteModel

  detalheCampanha: CampanhaModel
  detalheLoteProdutos: LoteProdutoListModel[] = []

  public constructor(
    private notificationService: NotificationService,
    private loginService: LoginService,
    private http: HttpClient
  ) { }

  reset() {
    this.numCampanhas = 0
    this.totalDeAplicacoes = 0
    this.totalQtde = 0
    this.totalElegiveis = 0
    this.valor_faturado = 0
    this.kpis = undefined
    this.campanhas = undefined
    this.produto = undefined
    this.lote = undefined
    this.detalheCampanha = undefined
    this.detalheLoteProdutos = undefined
  }

  saveCampanha(camp: CampanhaModel): Observable<any> {
    return this.http.post<CampanhaModel>(`${IC_API}/api/dados/campanhas/create`, camp)
  }

  updateCampanha(camp: CampanhaModel): Observable<any> {
    let params: HttpParams = undefined
    if (camp._id) {
      params = new HttpParams().append('_id', camp._id)
    }
    return this.http.patch<CampanhaModel>(`${IC_API}/api/dados/campanhas/update`, camp, { params: params })
  }

  getCampanhas(): Observable<CampanhaModel[]> {
    this.kpis = []
    this.numCampanhas = 0
    this.totalDeAplicacoes = 0
    this.totalQtde = 0
    this.totalElegiveis = 0
    this.valor_faturado = 0

    return this.http.get<any>(`${IC_API}/api/seguranca/usuarios/getCampanhas`)
      .do(data => this.campanhas = data.campanhas)
      .do(data => this.totalizarKPIs())
      .map(data => data.campanhas)
  }

  consultar(sort?: string, order?: string, page?: number, size?: number, all?: boolean): Observable<any> {
    let params: any = Util.addParams([
      { 'sort': sort },
      { 'order': order },
      { 'page': page },
      { 'size': size },
      { 'all': all}
    ]);

    return this.http.get<any>(`${IC_API}/api/dados/campanhas/retrieve`, { params })
      .do(data => this.campanhas = data.retorno)
      .map(data => data);
  }

  consultarNotCadAuto(sort?: string, order?: string, page?: number, size?: number, all?: boolean): Observable<any> {
    let params: any = Util.addParams([
      { 'sort': sort },
      { 'order': order },
      { 'page': page },
      { 'size': size },
      { 'all': all}
    ]);

    return this.http.get<any>(`${IC_API}/api/dados/campanhas/getNotCadAuto`, { params })
      .do(data => this.campanhas = data.retorno)
      .map(data => data);
  }

  consultarByUsuario(sort?: string, order?: string, page?: number, size?: number): Observable<any> {
    let params: any = Util.addParams([
      { 'sort': sort },
      { 'order': order },
      { 'page': page },
      { 'size': size }
    ]);

    return this.http.get<any>(`${IC_API}/api/seguranca/usuarios/getCampanhasByUsuario`, { params })
      .do(data => this.campanhas = data.retorno)
      .map(data => data);
  }

  
  deleteEventoByCampanha(idCampanha: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_idCampanha': idCampanha}
    ]);
    this.http.delete<EventoModel>(`${IC_API}/api/dados/eventos/deleteByCampanha`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  delete(idCampanha: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_id': idCampanha}
    ]);
    this.http.delete<CampanhaModel>(`${IC_API}/api/dados/campanhas/delete`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  findCampanhaDB(idCampanha: string): Observable<any> {
    let params: HttpParams = undefined
    if (idCampanha) {
      params = new HttpParams().append('_id', idCampanha)
    }

    return this.http.get<any>(`${IC_API}/api/dados/campanhas/findOne`, { params: params })
      //.map(data => data.retorno)
  }

  getCampanhaPorId(id: String): CampanhaModel {
    return this.campanhas.find((campanha) => campanha._id === id)
  }

  totalizarKPIs() {
    if (this.kpis.length === 0) {
      this.totalDeAplicacoes = 0
      this.totalQtde = 0
      this.valor_faturado = 0
      this.totalElegiveis = 0

      for (let i = 0; i < this.campanhas.length; i++) {
        this.numCampanhas++;

        this.campanhas[i].total_qtde = this.resetValorUndefined(this.campanhas[i].total_qtde)
        this.campanhas[i].total_aplicacoes = this.resetValorUndefined(this.campanhas[i].total_aplicacoes)
        this.campanhas[i].valor_unitario_servico = this.resetValorUndefined(this.campanhas[i].valor_unitario_servico)
        this.campanhas[i].total_elegiveis = this.resetValorUndefined(this.campanhas[i].total_elegiveis)
        this.campanhas[i].elegiveis_f = this.resetValorUndefined(this.campanhas[i].elegiveis_f)
        this.campanhas[i].elegiveis_m = this.resetValorUndefined(this.campanhas[i].elegiveis_m)
        this.campanhas[i].elegiveis_d = this.resetValorUndefined(this.campanhas[i].elegiveis_d)

        this.valor_faturado = this.valor_faturado + (this.campanhas[i].total_aplicacoes * this.campanhas[i].valor_unitario_servico)
        this.totalQtde = this.totalQtde + this.campanhas[i].total_qtde
        this.totalElegiveis = this.totalElegiveis + this.campanhas[i].total_elegiveis
        this.totalDeAplicacoes = this.totalDeAplicacoes + this.campanhas[i].total_aplicacoes
      }

      this.kpis.push(this.getKPI('Campanhas', this.numCampanhas, false, false, '/campanhas'))
      this.kpis.push(this.getKPI('Vacinas', this.totalQtde, false, false))
      this.kpis.push(this.getKPI('Elegíveis', this.totalElegiveis, false, false, '/elegiveis'))
      this.kpis.push(this.getKPI('Aplicações', this.totalDeAplicacoes, false, false))
      if (this.loginService.getUsuario().perfil === 'ADMIN') {
        this.kpis.push(this.getKPI('Faturamento', this.valor_faturado, true, true))
      }
      if (this.numCampanhas > 0) {
        this.detalharCampanha(this.campanhas[0])
      }
    }
  }

  getKpisCalculados(): KpiBoxModel[] {
    return this.kpis
  }

  getKPI(nome: string, valor: number, acesso?: boolean | false, moeda?: boolean | false, redirect?: string | ''): KpiBoxModel {
    const kpi: KpiBoxModel = { nome: '', valor: 0, acesso: false, redirect: '' }
    kpi.nome = nome
    kpi.valor = this.formatarNumero(valor, moeda)
    if (acesso) {
      kpi.acesso = this.loginService.getUsuario().perfil === 'ADMIN'
    } else {
      kpi.acesso = true
    }
    kpi.redirect = redirect
    return kpi
  }

  formatarNumero(valor: number, moeda?: boolean | false): any {
    if (valor === 0 || valor === undefined) {
      valor = 0;
    }
    if (moeda) {
      return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(valor);
    } else {
      return new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(valor);
    }
  }

  detalharCampanha(campanha: CampanhaModel) {
    this.detalheCampanha = campanha
    this.detalheLoteProdutos = []

    let loteProd: LoteProdutoListModel
    if (!this.detalheCampanha.produtos) {
      return;
    }
    for (const prod of this.detalheCampanha.produtos) {
      for (const lote of prod.lotes) {
        loteProd = {
          idCampanha: '',
          idProduto: '',
          produto: '',
          idLote: '',
          lote: '',
          lote_qtde: 0
        }

        loteProd.idCampanha = this.detalheCampanha._id
        loteProd.idProduto = prod._id
        loteProd.produto = prod.produto.produto
        loteProd.idLote = lote._id
        loteProd.lote = lote.lote
        loteProd.lote_qtde = this.formatarNumero(lote.lote_qtde)
        this.detalheLoteProdutos.push(loteProd)
      }
    }
  }

  resetValorUndefined(valor: number): number {
    if (isNaN(valor) || valor === undefined) {
      return 0
    } else {
      return valor
    }
  }

  setNewEndereco(): EnderecoModel {
    return {
      _id: '',
      endereco: '',
      bairro: '',
      cidade: '',
      estado: '',
      cep: ''
    }
  }

  setNewEmpresa(): EmpresaModel {
    return {
      _id: '',
      empresa: '',
      cnpj: '',
      tipo: '',
      responsavel: '',
      email: '',
      fone: '',
      endereco: this.setNewEndereco(),
      numero: 0,
      complemento: ''
    }
  }

  setNewProduto(): ProdutoModel {
    return {
      _id: '',
      produto: '',
      fornecedores: [{
        fornecedor: this.setNewEmpresa(),
        periodos: [
          {
            _id: '',
            ordem: 0,
            periodo_meses: 0
          }
        ],
        _id: ''
      }]
    }
  }

  setNew(): CampanhaModel {
    return {
      _id: '',
      campanha: '',
      cliente: this.setNewEmpresa(),
      distribuidor: this.setNewEmpresa(),
      responsavel_cliente: '',
      email_cliente: '',
      telefone_cliente: '',
      gestor: this.setNewEmpresa(),
      responsavel_gestor: '',
      email_gestor: '',
      telefone_gestor: '',
      foto_gestor: '',
      fornecedores: [{
        fornecedor: this.setNewEmpresa()
      }],
      cnpj_total: [{
        cnpj: '',
        cnpj_aplicacoes: 0
      }],
      status: 'Nova',
      geracao: {
        step: '',
        status: ''
      },
      unidade: '',
      data_inicio: new Date(),
      data_fim: new Date(),
      horario_funcionamento_ini: '00:00',
      horario_funcionamento_fim: '00:00',
      nf: '',
      in_company: false,
      cadastro_auto: false,
      agrupamento: new AgrupamentoModel(),
      pesquisa_satisfacao: false,
      pesquisa_saude: false,
      total_elegiveis: 0,
      total_qtde: 0,
      total_aplicacoes: 0,
      valor_unitario_servico: 0,
      elegiveis_m: 0,
      elegiveis_f: 0,
      elegiveis_d: 0,
      elegiveis_mp: 0,
      elegiveis_fp: 0,
      elegiveis_dp: 0,
      produtos: [{
        _id: '',
        produto: this.setNewProduto(),
        produto_qtde: 0,
        produto_aplicacoes: 0,
        elegiveis_m: 0,
        elegiveis_f: 0,
        elegiveis_d: 0,
        elegiveis_c: 0,
        duplicidade: 0,
        elegiveis_mp: 0,
        elegiveis_fp: 0,
        elegiveis_dp: 0,
        elegiveis_cp: 0,
        duplicidade_dp: 0,
        lotes: [{
          _id: '',
          lote: '',
          lote_qtde: 0,
          lote_aplicacoes: 0,
          duplicidade: 0
        }]
      }],
      filiais: [],
      senha_vacinacao: '',
      observacao:''
    }
  }

  public get readonly(): boolean {
    console.log(`CampanhaService GET FLAG Gerador: ${this._readonly}`)
    return this._readonly;
  }

  public set readonly(value: boolean) {
    console.log(`CampanhaService SET FLAG Gerador: ${value}`)
    this._readonly = value;
  }

  
  successCallback(result: any, next?: (result: any) => void, showCallback?: boolean): void {
    if (showCallback) {
      this.notificationService.notifiy({
        message: Constants.MSG_SUCCESS_OPERATION
      });
    }
    if (next) {
      next(result);
    }
  }

  errorCallback(error: any, message: string): void {
    this.notificationService.notifiy({
      message:  `${message} o elegível.`,
      tipo: 'E',
    });
  }


}
