import { EmpresaModel } from '../cadastros/empresa.model';

interface ProdutoModel {
  _id: string,
  produto: string,
  fornecedores: [ {
    fornecedor: EmpresaModel,
    periodos: [
      {
        _id: string,
        ordem: number,
        periodo_meses: number
      }
    ],
    _id: string
  }],
  dosagem?: string,
  faixa_min?: number,
  faixa_max?: number,
  criado_por?: string,
  data?: Date
}

interface ProdutoTableModel {
  _id: string,
  descricao: string,
  fornecedor: string,
  controle_dosagem: string
}

export { ProdutoModel, ProdutoTableModel }
