import { Injectable } from "@angular/core";
import { LoginService } from "../../servicos/login.service";
import { NotificationService } from "../../servicos/notification.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { IC_API } from '../../../app.api'
import { Util } from "../../../../app/utils/util";
import { Observable } from "rxjs";
import { ImportacaoElegivelModel, CampanhaModel } from "../campanhas/campanha.model";
import { CargaModel } from "../carga/carga.model";
import { Constants } from "../../../../app/utils/constants";
import { DatePipe } from "@angular/common";

@Injectable()
export class ArquivoService {

  attachmentList: any = [];

  importacaoElegiveis: ImportacaoElegivelModel[] = []

  constructor(private loginService: LoginService,
    private notificationService: NotificationService,
    private http: HttpClient,
    private datePipe: DatePipe) {

  }

  downloadFile(file: String, folder?: String) {
    let body = { filename: file, folder: folder }
    return this.http.post(`${IC_API}/api/file/download`, body, {
      responseType: 'blob',
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }


  consultar(campanha: CampanhaModel, sort?: string, order?: string, page?: number, size?: number): Observable<any> {
    let params: any = Util.addParams([
      { 'sort': sort },
      { 'order': order },
      { 'page': page },
      { 'size': size },
      { 'tipo': 'Elegiveis' },
      { 'campanha': campanha._id }
    ]);

    return this.http.get<any>(`${IC_API}/api/dados/cargas/arquivos`, { params })
      .do(data =>this.converterParaImportacaoElegivel(data))
      .map(data => data);
  }

  converterParaImportacaoElegivel(cargas: any): void {
    let retorno: ImportacaoElegivelModel[] = [];

    for (let carga of cargas.retorno) {
      let transfer: ImportacaoElegivelModel = {
        nomecampanha: carga.nomecampanha,
        data: this.datePipe.transform(carga.data, 'dd/MM/yyyy'),
        nome: carga.filename,
        nomeoriginal: carga.originalname,
        tipo: carga.tipo,
        sucesso: (carga.docs_total-carga.docs_erros).toString(),
        erros: carga.docs_erros.toString(),
        status: carga.status
      };
      retorno.push(transfer);
    }
    cargas.retorno = retorno;
  }

  getById(id: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      { '_id': id }
    ]);
    this.http.get<any>(`${IC_API}/api/dados/cargas/findOne`, { params })
      .subscribe((result) => { this.successCallback(result, next, showCallback) },
        error => { this.errorCallback(error, Constants.MSG_ERROR_RECOVER) }
      );
  }

  successCallback(result: any, next?: (result: any) => void, showCallback?: boolean): void {
    if (showCallback) {
      this.notificationService.notifiy({
        message: Constants.MSG_SUCCESS_OPERATION
      });
    }
    if (next) {
      next(result);
    }
  }

  errorCallback(error: any, message: string): void {
    this.notificationService.notifiy({
      message: `${message} a importação de elegivel.`,
      tipo: 'E',
    });
  }
}