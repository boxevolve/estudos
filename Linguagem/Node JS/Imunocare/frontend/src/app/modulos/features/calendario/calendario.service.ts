import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs';
import { LoginService } from '../../servicos/login.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IC_API } from '../../../app.api';
import { EventoModel, EventoCalModel } from './evento.model';
import { PARAMETERS } from '@angular/core/src/util/decorators';
import { EnderecoModel } from '../cadastros/endereco.model';

@Injectable()
export class CalendarioService {
	eventos: EventoModel[] = [];
    eventosCal: EventoCalModel[] = [];
    eventoEdit: EventoModel;
    dataRet: Date
    tipoRet: number
	public constructor(private loginService: LoginService, private http: HttpClient) {}

	getEventos(data?: Date): Observable<any> {
		let params: HttpParams = undefined;
		if (data) {
			params = new HttpParams().append('dataref', data.toLocaleDateString());
		}

		return (
			this.http
				.get<any>(`${IC_API}/api/dados/eventos/retrieve`, { params: params })
				.do(data => (this.eventos = data.retorno))
				//this.print() })
				.map(data => data)
		);
    }

    setEventoEdit(evento: EventoModel) {
        this.eventoEdit = this.resetEventoModel()
        this.eventoEdit = evento
    }
    
    getEventoEdit(): EventoModel {
        if(this.eventoEdit) return this.eventoEdit
        else return this.resetEventoModel()
    }
    

	print() {
		for (let ev of this.eventos) {
			console.log(`PRINT: ${JSON.stringify(ev)}`);
		}
    } 

    setRetornoCalendario(dia: Date, tipo: number) {
        this.dataRet = dia
        this.tipoRet = tipo
    }
    
    getDataRetorno(): Date {
        return this.dataRet || new Date()
    }

    getCalendarioView(): number {
        return this.tipoRet || 0
    }

	getEventosDoDia(dia: Date, idEvento?: string): EventoModel {
		if (idEvento) {
			for (let ev of this.eventos) {
				if (ev._id.toString() === idEvento) return ev;
			}
		} else {
			for (let ev of this.eventos) {
				const inicio: Date = new Date(ev.inicio);
				const fim: Date = new Date(ev.fim);
				if (
					dia.getFullYear() >= inicio.getFullYear() &&
					dia.getMonth() >= inicio.getMonth() &&
					dia.getDate() >= inicio.getDate() &&
					dia.getFullYear() <= fim.getFullYear() &&
					dia.getMonth() <= fim.getMonth() &&
					dia.getDate() <= fim.getDate()
				) {
					return ev;
				}
			}
			return this.resetEventoModel(dia)
		}
	}

    resetEvento(): EventoCalModel {
        return {
            _id: '',
            titulo: '',
            inicio: new Date(),
            fim: new Date(),
            diatodo: false,
            agrupamento: '',
            campanha: '',
            filial: '',
            unidade: '',
            fornecedor: '',
            texto_evento: ''
        };
    }

    resetEventoModel(dia?: Date): EventoModel {
        let data: Date
        if (dia) data = new Date(data)
        else data = new Date()

        return {
            _id: '',
            // Dados do Evento
                inicio: new Date(),
                fim: new Date(),
                diatodo: false,
                titulo: '',
                texto_evento: '',
                fornecedor: {
                    _id: '',
                    empresa: ''
                },
            // Dados da Campanha
                agrupamento: '',
                campanha: {
                    _id: '',
                    campanha: ''
                },
                filial: {
                    _id: '',
                    filial: '',
                    cnpj: '',
                    responsavel: ''
                },
                unidade: {
                    _id: '',
                    cod_unid: '',
                    nome_unid: '',
                    endereco: {
                        _id: '',
                        endereco: '',
                        bairro: '',
                        cidade: '',
                        estado: '',
                        cep: '',
                        criado_por: '',
                        data: new Date()
                    },
                    numero: 0,
                    complemento: '' ,
                    previsao_doses: 0
                }
        }
    }	
}
