//import { EnderecoModel } from "../cadastros/endereco.model";
import { UnidadeModel, FilialCampanhaModel } from "../campanhas/campanha.model";

interface EventoModel {
    _id?: string,
// Dados do Evento
    inicio: Date,
    fim: Date,
    diatodo: boolean,
    titulo: string,
    texto_evento: string,
    fornecedor?: {
        _id?: string,
        empresa?: string
    },
// Dados da Campanha
    agrupamento?: string,
    campanha?: {
        _id?: string,
        campanha?: string
    },
    filial?: {
        _id?: string,
        filial: string,
        cnpj: string,
        responsavel: string
    },
    unidade?: UnidadeModel
}

interface EventoCalModel {
    _id?: string,
    titulo: string,
    inicio: Date,
    fim: Date,
    diatodo: boolean,
    agrupamento: string,
    campanha: string,
    filial: string,
    unidade: string,
    fornecedor: string,
    texto_evento: string
}

export { EventoModel, EventoCalModel }