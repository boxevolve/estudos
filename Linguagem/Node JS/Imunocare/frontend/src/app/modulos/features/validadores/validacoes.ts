// JavaScript Document
export function Validacoes()  {

// valida numero inteiro com mascara
function mascaraInteiro(event: any) {

    if (event.keyCode < 48 || event.keyCode > 57) {
        event.returnValue = false;
        return false;
    }
    return true;
}

// adiciona mascara de cnpj
function MascaraCNPJ(event: any) {
    const cnpj = event.target.value
    if (mascaraInteiro(cnpj) === false) {
        event.returnValue = false;
    }
    return formataCampo(cnpj, '00.000.000/0000-00', event);
}

// adiciona mascara de cep
function MascaraCep(event: any) {
    const cep = event.target.value
    if (mascaraInteiro(cep) === false) {
        event.returnValue = false;
    }
    return formataCampo(cep, '00.000-000', event);
}

// adiciona mascara de data
function MascaraData(event: any) {
    const data = event.target.value
    if (mascaraInteiro(data) === false) {
        event.returnValue = false;
    }
    return formataCampo(data, '00/00/0000', event);
}

// adiciona mascara ao telefone
function MascaraTelefone(event: any) {
    const tel = event.target.value
    if (mascaraInteiro(tel) === false) {
        event.returnValue = false;
    }
    return formataCampo(tel, '(00) 0000-0000', event);
}

// adiciona mascara ao CPF
function MascaraCPF(event: any) {
    const cpf = event.target.value
    if (mascaraInteiro(cpf) === false) {
        event.returnValue = false;
    }
    return formataCampo(cpf, '000.000.000-00', event);
}

// valida telefone
function ValidaTelefone(event: any) {
    const exp = /\(\d{2}\)\ \d{4}\-\d{4}/
    const tel = event.target.value
    if (!exp.test(tel.value)) {
        alert('Numero de Telefone Invalido!');
    }
}

// valida CEP
function ValidaCep(event: any) {
    const exp = /\d{2}\.\d{3}\-\d{3}/
    const cep = event.target.value
    if (!exp.test(cep.value)) {
        alert('Numero de Cep Invalido!');
    }
}

// valida data
function ValidaData(event) {
    const exp = /\d{2}\/\d{2}\/\d{4}/
    const data = event.target.value
    if (!exp.test(data.value)) {
        alert('Data Invalida!');
    }
}

// formata de forma generica os campos
function formataCampo(campo, Mascara, evento) {
    const exp = /\-|\.|\/|\(|\)|/g
    let boleanoMascara;
    const Digitato = evento.keyCode,
        campoSoNumeros = campo.value.toString().replace(exp, '');

    let posicaoCampo = 0;
    let NovoValorCampo = '';
    let TamanhoMascara = campoSoNumeros.length;

    if (Digitato !== 8) { // backspace
        for (let i = 0; i <= TamanhoMascara; i++) {
            boleanoMascara = ((Mascara.charAt(i) === '-') || (Mascara.charAt(i) === '.')
                || (Mascara.charAt(i) === '/'))
            boleanoMascara = boleanoMascara || ((Mascara.charAt(i) === '(')
                || (Mascara.charAt(i) === ')') || (Mascara.charAt(i) === ' '))
            if (boleanoMascara) {
                NovoValorCampo += Mascara.charAt(i);
                TamanhoMascara++;
            } else {
                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
                posicaoCampo++;
            }
        }
        campo.value = NovoValorCampo;
        return true;
    } else {
        return true;
    }
}

}

