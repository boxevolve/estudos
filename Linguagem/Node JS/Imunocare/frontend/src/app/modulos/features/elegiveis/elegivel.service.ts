import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/map'
import { IC_API } from '../../../app.api'
import { ElegivelModel, ImunizacaoModel, ElegivelListModel } from './elegivel.model';
import { CnpjTotalModel } from '../campanhas/campanha.model';
import { Util } from '../../../../app/utils/util';
import { NotificationService } from '../../servicos/notification.service';
import { Constants } from '../../../../app/utils/constants';

@Injectable()
export class ElegivelService {
  url = `${IC_API}/api/dados/elegiveis`;

  cnpjTotal: CnpjTotalModel
  CNPJTotais: CnpjTotalModel[]
  elegivel: ElegivelModel
  elegiveis: ElegivelModel[]
  elegivelList: ElegivelListModel
  elegiveisList: ElegivelListModel[]
  imunizacao: ImunizacaoModel
  imunizacoes: ImunizacaoModel[]
  idCampanha: string

  public constructor(
    private notificationService: NotificationService,
    private http: HttpClient
  ) { }

  save(elegivel: any, next?: (result: any) => void, showCallback?: boolean): void {
    showCallback = !Util.isDefined(showCallback) ? true : showCallback;
    this.http.post<any>(`${this.url}/create`, elegivel)
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_SAVE)}
    );
  }

  reset() {
    this.elegivel = undefined
    this.elegiveis = undefined
    this.elegivelList = undefined
    this.elegiveisList = undefined
    this.imunizacao = undefined
    this.imunizacoes = undefined
    this.idCampanha = undefined
  }

  setCampanha(idCampanha: string) {
    this.idCampanha = idCampanha
  }

  aprovarElegivel(id: string, next?: (result?: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_id': id}
    ]);
    this.http.get<any>(`${this.url}/aprovarElegivel`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  consultarByCampanha(campanha: string, pendenteAprovacao?: boolean, sort?: string, order?: string, page?: number, size?: number,
    all?: boolean): Observable<any> {
    let params: any = Util.addParams([
      { 'sort': sort },
      { 'order': order },
      { 'page': page },
      { 'size': size },
      { 'all': all },
      { 'campanha': campanha },
      { 'pendenteAprovacao': pendenteAprovacao}
    ]);

    return this.http.get<any>(`${this.url}/getByCampanha`, { params })
      .do(data => this.elegiveis = this.mapearElegiveleImunizacoes(data.retorno))
      .map(data => data)
  }

  getElegiveis(idCampanha: string, cpf?: string, idProduto?: string): Observable<ElegivelModel[]> {
    let params: HttpParams = undefined
    if (idCampanha) {
      params = new HttpParams().append('campanha', idCampanha)
    }
    if (cpf && cpf !== '') {
      if (params !== undefined) {
        params = params.append('cpfresp', this.desformatarCPF(cpf))
      } else {
        params = new HttpParams().append('cpfresp', this.desformatarCPF(cpf))
      }

      return this.http.get<any>(`${IC_API}/api/dados/elegiveis/buscaElegiveisPorCPF`, { params: params })
        .do(data => this.elegiveis = this.mapearElegiveleImunizacoes(data.retorno, idProduto))
        .map(data => this.elegiveis)

    } else {
      return this.http.get<any>(`${IC_API}/api/dados/elegiveis/retrieve`, { params: params })
        .do(data => this.elegiveis = this.mapearElegiveleImunizacoes(data.retorno, idProduto))
        .map(data => this.elegiveis)
    }
  }

  getElegiveisList(idCampanha: string, cpf?: string, pageSize?: number, page?: number, regs?: number): Observable<ElegivelListModel[]> {
    let paramsArray: any[] = [
      {'campanha': idCampanha},
      {'_pageSize': pageSize},
      {'_page': page},
      {'_regs': regs}
    ]

    if (cpf) {
      paramsArray.push({'cpfresp': this.desformatarCPF(cpf)});
      const params = Util.addParams(paramsArray);

      return this.http.get<any>(`${IC_API}/api/dados/elegiveis/buscaElegiveisPorCPF`, { params })
                              .do(data => this.elegiveis = data.retorno)
                              .do(data => this.elegiveisList = this.mapearElegivelParaLista(data.retorno))
                              .map(data => this.elegiveisList)
    } else {
      const params = Util.addParams(paramsArray);
      return this.http.get<any>(`${IC_API}/api/dados/elegiveis/retrieve`, { params })
                              .do(data => this.elegiveis = data.retorno)
                              .do(data => this.elegiveisList = this.mapearElegivelParaLista(data.retorno))
                              .map(data => this.elegiveisList);
    }
  }

  deleteByCampanha(idCampanha: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_idCampanha': idCampanha}
    ]);
    this.http.delete<ElegivelModel>(`${IC_API}/api/dados/elegiveis/deleteByCampanha`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  mapearElegiveleImunizacoes(elegiveis: ElegivelModel[], idProduto?: string): ElegivelModel[] {

    const elegiveisTemp: ElegivelModel[] = []

    // console.log(`MAPEAR ELEGIVEL - idProduto ${idProduto}`)

    if (idProduto !== undefined) {
      for (const element of elegiveis) {
        element.aplicacao = false
        for (const imu of element.imunizacoes) {
          if (imu.produto === idProduto && imu.aplicacao === true) {
            element.aplicacao = imu.aplicacao
          }
        }
        element.cpf = this.formatarCPF(element.cpf)
        element.cpfresp = this.formatarCPF(element.cpfresp)
        elegiveisTemp.push(element)
      }
      return elegiveisTemp
    } else {
      return elegiveis
    }
  }

  mapearElegivelParaLista(elegiveis: ElegivelModel[]): ElegivelListModel[] {
    const elegiveisList: ElegivelListModel[] = []

    for (const element of elegiveis) {
      let insElegOk: boolean = true
      if (element.imunizacoes !== undefined && element.imunizacoes.length > 0) {
        for (const imu of element.imunizacoes) {
          if (imu.aplicacao === true) {
            const elegivelList: ElegivelListModel = this.getElegivelListNull()

            elegivelList._id = element._id
            elegivelList.campanha = element.campanha
            elegivelList.matricula = element.matricula
            elegivelList.cpf = this.formatarCPF(element.cpf)
            if (element.cnpj) {
                elegivelList.cnpj = this.formatarCNPJ(element.cnpj)
            }
            elegivelList.nome = element.nome
            elegivelList.dependente = element.dependente
            elegivelList.cpfresp = this.formatarCPF(element.cpfresp)
            elegivelList.dtnasc = element.dtnasc
            elegivelList.sexo = element.sexo
            elegivelList.classificacao = element.classificacao
            elegivelList.email = element.email
            elegivelList._idImu = imu._id
            elegivelList.produto = imu.produto
            elegivelList.desc_produto = imu.desc_produto
            elegivelList.lote = imu.lote
            elegivelList.num_lote = imu.num_lote
            elegivelList.data = imu.data
            elegivelList.aplicacao = imu.aplicacao
            if (imu.usuario) {
                elegivelList.enfermeiro = imu.usuario.usuario
            }
            elegiveisList.push(elegivelList)
            insElegOk = false
          }
        }
      }
      if (insElegOk) {
        const elegivelList: ElegivelListModel = this.getElegivelListNull()

        elegivelList._id = element._id
        elegivelList.campanha = element.campanha
        elegivelList.matricula = element.matricula
        elegivelList.cpf = this.formatarCPF(element.cpf)
        if (element.cnpj) {
            elegivelList.cnpj = this.formatarCNPJ(element.cnpj)
        }
        elegivelList.nome = element.nome
        elegivelList.dependente = element.dependente
        elegivelList.cpfresp = this.formatarCPF(element.cpfresp)
        elegivelList.dtnasc = element.dtnasc
        elegivelList.sexo = element.sexo
        elegivelList.classificacao = element.classificacao
        elegivelList.email = element.email
        elegiveisList.push(elegivelList)
      }
    }
    // console.log(`MAPEAR: ${elegiveisList.length}`)
    return elegiveisList

  }

  atualizarStatusImunizacaoDeElegivel(eleg: ElegivelListModel) {
    const i = this.elegiveisList.findIndex((elegivel) => elegivel._id === eleg._id && elegivel._idImu === eleg._idImu)
    if (i !== undefined) {
      this.elegiveisList[i] = eleg
    }

    const ii = this.elegiveis.findIndex((elegivel) => elegivel._id === eleg._id)
    if (ii !== undefined) {
      const j = this.elegiveis[ii].imunizacoes.findIndex((imunizacao) => imunizacao._id === eleg._idImu)
      if (j !== undefined) {
        this.elegiveis[ii].imunizacoes[j].aplicacao = true
        this.elegiveis[ii].imunizacoes[j].data = new Date()
      }
    }
  }

  getElegivelListNull(): ElegivelListModel {
    const el: ElegivelListModel = {
      '_id': '',
      'campanha': '',
      'matricula': '',
      'cnpj': '',
      'cpf': '',
      'nome': '',
      'dependente': false,
      'cpfresp': '',
      'dtnasc': null,
      'sexo': '',
      'classificacao': '',
      'email': '',
      '_idImu': '',
      'produto': '',
      'desc_produto': '',
      'lote': '',
      'num_lote': '',
      'data': null,
      'aplicacao': false,
      'enfermeiro': ''
    }
    return el
  }

  getElegivelNull(): ElegivelModel {
    const el: ElegivelModel = {
      '_id': '',
      'campanha': '',
      'matricula': '',
      'cpf': '',
      'nome': '',
      'dependente': false,
      'cpfresp': '',
      'dtnasc': null,
      'sexo': '',
      'classificacao': '',
      'email': '',
      'aplicacao': false,
      'imunizacoes': [],
      'pendente_aprovacao': false
    }
    return el
  }

  formatarCPF(cpf: string | ''): string {
    if (cpf.length > 0) {
      cpf = cpf.replace(/\D/g, '')
      cpf = cpf.replace(/^(\d{3})?(\d{3})?(\d{3})?(\d{2})$/, '$1.$2.$3-$4')
    }
    return cpf || ''
  }

  desformatarCPF(cpf: string | ''): string {
    cpf = cpf.replace(/\D/g, '')
    // console.log(cpf)
    return cpf || ''
  }

  formatarCNPJ(cnpj: string | ''): string {
    if (cnpj.length > 0) {
      cnpj = cnpj.replace(/\D/g, '')
      cnpj = cnpj.replace(/^(\d{2})?(\d{3})?(\d{3})?(\d{4})?(\d{2})$/, '$1.$2.$3/$4-$5')
    }
    return cnpj || ''
  }

  desformatarCNPJ(cnpj: string | ''): string {
    cnpj = cnpj.replace(/\D/g, '')
    return cnpj || ''
  }

  successCallback(result: any, next?: (result: any) => void, showCallback?: boolean): void {
    if (showCallback) {
      this.notificationService.notifiy({
        message: Constants.MSG_SUCCESS_OPERATION
      });
    }
    if (next) {
      next(result);
    }
  }

  errorCallback(error: any, message: string): void {
    this.notificationService.notifiy({
      message:  `${message} o elegível.`,
      tipo: 'E',
    });
  }

}
