import { ClinicaTabFormComponent } from './../../../components/cadastro/campanha/form/clinica/clinica-tab-form.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { RouterGuard } from '../../servicos/router.guard';
import { CadastroComponent } from '../../../pages/cadastros/cadastro/cadastro.component';
import { ParceiroComponent } from '../../../pages/cadastros/parceiro/parceiro.component';
import { ClientesComponent } from '../../../pages/cadastros/parceiro/clientes/clientes.component';
import { FornecedoresComponent } from '../../../pages/cadastros/parceiro/fornecedores/fornecedores.component';
import { DistribuidorasComponent } from '../../../pages/cadastros/parceiro/distribuidoras/distribuidoras.component';
import { ProdutoListComponent } from '../../../components/cadastro/produto/list/produto-list.component';
import { ProdutoFormComponent } from '../../../components/cadastro/produto/form/produto-form.component';
import { ParceirosListComponent } from '../../../components/cadastro/parceiros/list/parceiros-list.component';
import { ParceirosFormComponent } from '../../../components/cadastro/parceiros/form/parceiros-form.component';
import { CampanhaListComponent } from '../../../components/cadastro/campanha/list/campanha-list.component';
import { CampanhaFormComponent } from '../../../components/cadastro/campanha/form/campanha-form.component';
import { CampanhaTabFormComponent } from '../../../components/cadastro/campanha/form/campanha/campanha-tab-form.component';
import { FilialTabFormComponent } from '../../../components/cadastro/campanha/form/filial/filial-tab-form.component';
import { ProdutoTabFormComponent } from '../../../components/cadastro/campanha/form/produto/produto-tab-form.component';
import { UnidadeTabFormComponent } from '../../../components/cadastro/campanha/form/unidade/unidade-tab-form.component';
import { ModalAgrupamentoComponent } from '../../../shared/components/modal/modal-agrupamento/modal-agrupamento.component';
import { ModalLoteProdutoComponent } from '../../../shared/components/modal/modal-lote/modal-lote-produto.component';
import { MatAutocompleteModule } from '@angular/material';

const ROUTES: Routes = [{
  path: '', component: CadastroComponent,
  children: [
    {
      path: 'parceiros',
      children: [
        {
          path: '', component: ParceirosListComponent,
          data: {
            title: 'Lista de Parceiros'
          },
          canActivate: [RouterGuard], canLoad: [RouterGuard]
        },
        {
          path: 'add', component: ParceirosFormComponent,
          data: {
            title: 'Cadastro de Parceiro'
          },
          canActivate: [RouterGuard], canLoad: [RouterGuard]
        },
          {
            path: 'edit/:id', component: ParceirosFormComponent,
            data: {
              title: 'Alteração de Parceiro'
            },
          canActivate: [RouterGuard], canLoad: [RouterGuard]
          }
      ]
    },
    {
      path: 'produtos',
      children: [
        {
          path: '', component: ProdutoListComponent,
          data: {
            title: 'Lista de Produtos'
          },
          canActivate: [RouterGuard], canLoad: [RouterGuard]
        },
        {
          path: 'add', component: ProdutoFormComponent,
          data: {
            title: 'Cadastro de Produto'
          },
          canActivate: [RouterGuard], canLoad: [RouterGuard]
        },
        {
          path: 'edit/:id', component: ProdutoFormComponent,
          data: {
            title: 'Alteração de Produto'
          },
          canActivate: [RouterGuard], canLoad: [RouterGuard]
        }
      ]
    },
    {
      path: 'campanha',
      children: [
        {
          path: '', component: CampanhaListComponent,
          data: {
            title: 'Lista de Campanhas'
          },
          canActivate: [RouterGuard], canLoad: [RouterGuard]
        },
        {
          path: 'add', component: CampanhaFormComponent,
          data: {
            title: 'Cadastro de Campanha'
          },
          canActivate: [RouterGuard], canLoad: [RouterGuard]
        },
        {
          path: 'edit/:id', component: CampanhaFormComponent,
          data: {
            title: 'Alteração de Campanha'
          },
          canActivate: [RouterGuard], canLoad: [RouterGuard],
        }
      ]
    }
  ]
}]

@NgModule({
  declarations: [
    CadastroComponent,
    ParceiroComponent,
    ClientesComponent,
    FornecedoresComponent,
    DistribuidorasComponent,
    ProdutoListComponent,
    ProdutoFormComponent,
    ParceirosListComponent,
    ParceirosFormComponent,
    CampanhaListComponent,
    CampanhaFormComponent,
    CampanhaTabFormComponent,
    FilialTabFormComponent,
    ProdutoTabFormComponent,
    UnidadeTabFormComponent,
    ClinicaTabFormComponent,
    ModalAgrupamentoComponent,
    ModalLoteProdutoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    RouterModule,
    MatAutocompleteModule
  ]
})

export class CadastroModule {
  public constructor() { }
}
