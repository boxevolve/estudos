

import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AcessoGuard } from '../../servicos/acesso.guard'
import { SharedModule } from '../../shared/shared.module'

import { CampanhaDetailsComponent } from '../../../pages/dashboard/campanha-details/campanha-details.component'
import { DashboardComponent } from '../../../pages/dashboard/dashboard.component'
import { ElegiveisComponent } from '../../../pages/dashboard/elegiveis/elegiveis.component'
import { EvolucaoComponent } from '../../../pages/dashboard/evolucao/evolucao.component'
import { GraficoComponent } from '../../../pages/dashboard/grafico/grafico.component'
import { InfoboxComponent } from '../../../pages/dashboard/infobox/infobox.component'
import { CampanhaCnpjComponent } from '../../../pages/dashboard/campanha-cnpj/campanha-cnpj.component';
import { ElegiveisTableComponent } from '../../../pages/elegiveis/elegiveis-table/elegiveis-table.component'

const ROUTES: Routes = [
    {path: '', component: DashboardComponent,
        children: [
            {path: 'campanha-detail', component: CampanhaDetailsComponent},
            {path: 'elegiveis', component: ElegiveisComponent}
        ]
    }
]

@NgModule({
    declarations: [
        CampanhaDetailsComponent,
        DashboardComponent,
        ElegiveisComponent,
        EvolucaoComponent,
        GraficoComponent,
        InfoboxComponent,
        CampanhaCnpjComponent,
        ElegiveisTableComponent
    ],
    imports: [ SharedModule,
        RouterModule.forChild(ROUTES)
    ]
})


export class DashboardModule {

    public constructor() {

    }


}
