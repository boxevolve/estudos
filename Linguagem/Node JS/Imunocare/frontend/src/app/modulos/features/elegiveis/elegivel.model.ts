interface EnfermeiroModel {
    usuario: string,
    nome: string
}

interface ImunizacaoModel {
    _id: string
    produto: string,
    desc_produto: string,
    lote: string,
    num_lote: string,
    data?: Date,
    aplicacao: boolean,
    usuario?: EnfermeiroModel
}

interface ElegivelModel {
    _id: string,
    campanha: string,
    matricula?: string,
    cpf: string,
    cnpj?: string,
    nome: string,
    dependente: boolean,
    cpfresp: string,
    dtnasc?: Date,
    sexo: string,
    classificacao?: string,
    email: string,
    aplicacao?: boolean,
    imunizacoes: ImunizacaoModel[],
    pendente_aprovacao?: boolean
}

interface ElegivelListModel {
    _id: string,
    campanha: string,
    matricula?: string,
    cnpj?: string,
    cpf: string,
    nome: string,
    dependente: boolean,
    cpfresp: string,
    dtnasc?: Date,
    sexo: string,
    classificacao?: string,
    email: string,
    _idImu: string
    produto: string,
    desc_produto: string,
    lote: string,
    num_lote: string,
    data: Date,
    aplicacao: boolean,
    enfermeiro?: string
}

export { EnfermeiroModel, ImunizacaoModel, ElegivelModel, ElegivelListModel }
