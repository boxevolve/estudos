interface CargaModel {
    _id: string,
    usuario: string,
    campanha: string,
    tipo: string,
    filename: string,
    status: string,
    data: string,
    docs_total: string,
    docs_erros: string
}

export { CargaModel }