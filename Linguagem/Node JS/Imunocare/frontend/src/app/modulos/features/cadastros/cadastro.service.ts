import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { LoginService } from '../../servicos/login.service'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/map'

import { IC_API } from '../../../app.api'
import { EmpresaModel, EmpresaTableModel } from './empresa.model';
import { EnderecoModel } from './endereco.model';
import { NotificationService } from '../../servicos/notification.service';

//'Cliente', 'Distribuidor', 'Fornecedor's

@Injectable()
export class CadastroService {

  parceiros: EmpresaModel[] = []
  parceiro: EmpresaModel = undefined

  fornecedores: EmpresaModel[] = []
  listFornecedores: EmpresaTableModel[] = []

  public constructor(
    private loginService: LoginService,
    private notificationService: NotificationService,
    private http: HttpClient
  ) {}

  getParceiros(tipo?: string, sort?: string, order?: string, page?: number, size?: number): Observable<any> {

    let params: HttpParams = undefined

    if (tipo) {
        if(params) params = params.append('tipo', tipo)
        else params = new HttpParams().append('tipo', tipo)
    }
    if (sort) {
        if(params) params = params.append('sort', sort)
        else params = new HttpParams().append('sort', sort)
    }        
    if (order) {
        if(params) params = params.append('order', order)
        else params = new HttpParams().append('order', order)
    }        
    if (page) {
        if(params) params = params.append('page', page.toString())
        else params = new HttpParams().append('page', page.toString())
    }
    if (size) {
        if(params) params = params.append('size', size.toString())
        else params = new HttpParams().append('size', size.toString())
    }

    return this.http.get<any>(`${IC_API}/api/dados/empresas/retrieve`, {params: params})
      .do(data => this.parceiros = data.retorno)
      .map(data => data)
  }

  getFornecedores(next?: (x: any) => void): Observable<any> {
    if (!next) {
      return this.http.get<any>(`${IC_API}/api/dados/empresas/listafornecedores`)
          .do(data => this.fornecedores = data.retorno)
          .map(data => data.retorno)
    } else {
      this.http.get<any[]>(`${IC_API}/api/dados/empresas/listafornecedores`).subscribe(next,
          error => {
            this.notificationService.notifiy({
              message: `Erro ao acessar dados dos fornecedores`,
              tipo: 'E',
            });
          }
      );
    }
  }

  initParceiro(): EmpresaModel {
    return {
      _id: '',
      empresa: '',
      cnpj: '',
      tipo: '',
      responsavel: '',
      email: '',
      fone: '',
      endereco: this.initEndereco(),
      numero: 0,
      complemento: '',
      criado_por: '',
      data: new Date()
    }
  }

  initEndereco(): EnderecoModel {
    return {
        _id: '',
        endereco: '',
        bairro: '',
        cidade: '',
        estado: '',
        cep: '',
        criado_por: '',
        data: new Date()
    }
  }

}
