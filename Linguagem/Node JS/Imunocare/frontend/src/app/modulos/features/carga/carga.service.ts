import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/map'
import { IC_API } from '../../../app.api'
import { Router } from '@angular/router';
import { CargaModel } from './carga.model';
import { Util } from '../../../../app/utils/util';
import { NotificationService } from '../../servicos/notification.service';
import { Constants } from '../../../../app/utils/constants';


@Injectable()
export class CargaService {

  
  cargas: CargaModel[]

  public constructor(private notificationService: NotificationService, private http: HttpClient, private router: Router) { }

  getCargas(tipo: string): Observable<CargaModel[]> {
    let params: HttpParams = undefined
    params = new HttpParams().append('tipo', tipo)

    return this.http.get<any>(`${IC_API}/api/dados/cargas/arquivos`, { params: params })
    .map(data => this.cargas)
  }

  
  deleteByCampanha(idCampanha: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_idCampanha': idCampanha}
    ]);
    this.http.delete<CargaModel>(`${IC_API}/api/dados/elegiveis/deleteByCampanha`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  successCallback(result: any, next?: (result: any) => void, showCallback?: boolean): void {
    if (showCallback) {
      this.notificationService.notifiy({
        message: Constants.MSG_SUCCESS_OPERATION
      });
    }
    if (next) {
      next(result);
    }
  }

  errorCallback(error: any, message: string): void {
    this.notificationService.notifiy({
      message:  `${message} o elegível.`,
      tipo: 'E',
    });
  }
}
