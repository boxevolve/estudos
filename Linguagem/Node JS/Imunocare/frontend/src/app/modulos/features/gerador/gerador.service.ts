import { Injectable, Input } from '@angular/core';
import { StepModel } from './gerador.model';
import { CampanhaService } from '../campanhas/campanha.service';
import { CampanhaModel } from '../campanhas/campanha.model';

const disabled = 'disabled'
const active = 'active'
const complete = 'complete'


@Injectable()
export class GeradorService {

    steps: StepModel[] = []
    currentStep: StepModel = { seq: 0, step: '', status: '' }
    _desbloqueado: boolean = true;
    campanha: CampanhaModel
    _nova: boolean = false
    _estagio: number = 0

    constructor() {

        this.currentStep = { seq: 0, step: 'Campanha', status: disabled }
        this.steps.push({ seq: 0, step: 'Campanha', status: disabled })
        this.steps.push({ seq: 1, step: 'Produtos', status: disabled })
        //this.steps.push({ seq: 2, step: 'Parceiros', status: disabled })
        this.steps.push({ seq: 2, step: 'Parceiros', status: disabled })
        this.steps.push({ seq: 3, step: 'Usuários', status: disabled })
        this.steps.push({ seq: 4, step: 'Elegíveis', status: disabled })
        this.steps.push({ seq: 5, step: 'Roteiros', status: disabled })

    }

    reset() {
        this.steps = undefined
        this.currentStep = undefined
        this._desbloqueado = true
        this.campanha = undefined
        this.estagio = 0
    }

    isDisabled(): string {
        return disabled
    }

    isActive(): string {
        return active
    }

    isComplete(): string {
        return complete
    }

    getNextStep(): StepModel {

        if (this.currentStep.seq < this.steps.length) {
            console.log(`Step atual: ${this.currentStep.seq} - ${this.currentStep.step} - ${this.currentStep.status}`)

            switch (this.currentStep.status) {
                case this.isDisabled():
                    this.currentStep.status = this.isActive()
                    this.steps[this.currentStep.seq].status = this.isActive()
                    break
                case this.isActive():
                    this.currentStep.status = this.isComplete()
                    this.steps[this.currentStep.seq].status = this.isComplete()
                    break
                case this.isComplete():
                    if (this.steps.length > (this.currentStep.seq + 1)) {
                        let step = this.steps.find((stepf) => stepf.seq === (this.currentStep.seq + 1))
                        this.steps[this.currentStep.seq].status = this.isComplete()
                        this.steps[step.seq].status = this.isActive()
                        this.currentStep = this.steps[step.seq]
                    }
                    break
            }
            console.log(`Prox step: ${this.currentStep.seq} - ${this.currentStep.step} - ${this.currentStep.status}`)
            //this.printAllStepStatus()
            return this.currentStep
        }
        else
            return null
    }

    getPreviousStep(): StepModel {
        if (this.currentStep.seq >= 0) {
            console.log(`Step atual: ${this.currentStep.seq} - ${this.currentStep.step} - ${this.currentStep.status}`)

            switch (this.currentStep.status) {
                case this.isComplete():
                    this.currentStep.status = this.isActive()
                    this.steps[this.currentStep.seq].status = this.isActive()
                    break
                case this.isActive():
                    if (this.currentStep.seq >= 0) {
                        this.currentStep.status = this.isDisabled()
                        this.steps[this.currentStep.seq].status = this.isDisabled()
                    }
                    break
                case this.isDisabled():
                    if (this.currentStep.seq >= 0) {
                        let step = this.steps.find((stepf) => stepf.seq === (this.currentStep.seq - 1))
                        this.steps[this.currentStep.seq].status = this.isDisabled()
                        this.steps[step.seq].status = this.isActive()
                        this.currentStep = this.steps[step.seq]
                    }
                    break
            }
            console.log(`Prox step: ${this.currentStep.seq} - ${this.currentStep.step} - ${this.currentStep.status}`)
            // this.printAllStepStatus()
            return this.currentStep
        }
        else
            return null
    }

    changeStepStatusForward(): string {

        switch (this.currentStep.status) {
            case this.isDisabled():
                return this.isActive()
            case this.isActive():
                return this.isComplete()
        }

    }

    changeStepStatusBackward(): string {

        switch (this.currentStep.status) {
            case this.isComplete():
                return this.isDisabled()
            case this.isActive():
                return this.isDisabled()
        }

    }

    printAllStepStatus() {
        console.log(`STATUS GERAL`)
        for (let s of this.steps) {
            console.log(`Prox step: ${s.seq} - ${s.step} - ${s.status}`)
        }
    }
    hasPrevious(): boolean {
        return this.currentStep.seq > 0 || (this.currentStep.seq === 0 && this.currentStep.status !== this.isDisabled())
    }
    hasNext(): boolean {
        return this.steps.length > (this.currentStep.seq + 1)
    }

    public set desbloqueado(value: boolean) {
        this._desbloqueado = value;
    }

    isUnlocked(): boolean {
        return this._desbloqueado
    }

    public set estagio(value: number) {
        this._estagio = value
    }
    public get estagio(): number {
        return this._estagio
    }

    public set nova(nova: boolean) {
        this._nova = nova
    }

    public get nova(): boolean {
        return this._nova
    }


}