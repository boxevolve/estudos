export interface Usuario {
    _id?: string,
    img?: string,
    usuario?: string,
    perfil?: string,
    nome?: string,
    email?: string,
    token?: string,
    expires?: Date,
    primeiro_acesso?: boolean,
    campanhas?: [
        {
            campanha?: string
        }
    ]
}
