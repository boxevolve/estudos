import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations'
import { NotificationService } from '../../../servicos/notification.service';

import { Observable, timer } from 'rxjs'
import { tap, switchMap } from 'rxjs/operators'

@Component({
  selector: 'ic-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.css'],
  animations: [
    trigger('snack-visibility', [
      state('hidden', style({
        opacity: 0,
        bottom: '0px'
      })),
      state('visible', style({
        opacity: 1,
        bottom: '80px'
      })),
      transition('hidden => visible', animate('500ms 0s ease-in')),
      transition('visible => hidden', animate('500ms 0s ease-out'))
    ])
  ]
})
export class SnackbarComponent implements OnInit {

  message = 'Hello There!'
  snackVisibility = 'hidden'
  bcolor = ''

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this.notificationService.notifier
    // exibe a mensagem no momento do click sem subscribe
      .pipe(
        tap(notificacao => {
          this.message = notificacao.message
          this.snackVisibility = 'visible'
          this.bcolor = this.retornaCorDeFundo(notificacao.tipo)
        }),
        // sobrepõe o Observable antigo (mensagem antiga)
        switchMap( message => timer(3000))
      )
      // faz o subscribe do timer quando nenhuma mensagem nova tenha sido incluida na cadeia de eventos
      .subscribe(timer => this.snackVisibility = 'hidden')
  }

  retornaCorDeFundo(erro?: string): string {
    if (erro) {
      return '#CD5542'
    } else {
      return '#3B8DBC'
    }
  }
}
