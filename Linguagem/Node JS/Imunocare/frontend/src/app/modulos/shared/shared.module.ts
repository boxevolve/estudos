import { ImunizacaoService } from './../../components/imunizacao/vacinacao/vacinacao/service/imunizacao.service';
import { NgModule, ModuleWithProviders } from '@angular/core'
import { InputComponent } from './input/input.component'
import { SnackbarComponent } from './messages/snackbar/snackbar.component'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { DatePipe } from "@angular/common";
import {NgxMaskModule} from 'ngx-mask'

import { LoginService } from '../servicos/login.service'
import { NotificationService } from '../servicos/notification.service'
import { CampanhaService } from '../features/campanhas/campanha.service';
import { DashBoardService } from '../features/dashboard/dashboard.service'
import { ElegivelService } from '../features/elegiveis/elegivel.service'
import { CargaService } from '../features/carga/carga.service'
import { UsuarioService } from '../features/usuarios/usuario.service'
import { CadastroService } from '../features/cadastros/cadastro.service';
import { FileService } from '../servicos/file.service';
import { GeradorService } from '../features/gerador/gerador.service';
import { ArquivoService } from '../features/arquivo/arquivo.service';
import { AcessoGuard } from '../servicos/acesso.guard'
import { RouterGuard } from '../servicos/router.guard';

import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { AuthInterceptor } from '../servicos/auth.interceptor'
import { OptionComponent } from './option/option.component'
import { ModalComponent } from './modal/modal.component'

import { MatInputModule,
         MatTableModule,
         MatToolbarModule,
         MatPaginatorModule,
         MatSortModule,
         MatFormFieldModule,
         MatSelectModule,
         MatRadioModule,
         MatDatepickerModule,
         MatNativeDateModule,
         MatDialogModule,
         MatIconModule} from '@angular/material'
import { MatTabsModule } from '@angular/material/tabs';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatExpansionModule } from '@angular/material/expansion'

import { FileUploadModule } from 'ng2-file-upload';
import { CampanhaDetalhesComponent } from '../../pages/campanha/campanha-detalhes/campanha-detalhes.component';
import { CalendarioService } from '../features/calendario/calendario.service';
import { ModalService } from './modal/modal.service';
import { InputEmailComponent } from '../../shared/components/input-email/input-email.component';
import { InputRadioComponent } from '../../shared/components/input-radio/input-radio.component';
import { InputTextComponent } from '../../shared/components/input-text/input-text.component';
import { InputSelectComponent } from '../../shared/components/input-select/input-select.component';
import { TableComponent } from '../../shared/components/table/table.component';
import { TableReloadService } from '../../shared/components/table/table-reload.service';
import { InputTelComponent } from '../../shared/components/input-tel/input-tel.component';
import { InputHoraComponent } from '../../shared/components/input-hora/input-hora.component';
import { InputTextAreaComponent } from '../../shared/components/input-text-area/input-text-area.component';
import { InputDateComponent } from '../../shared/components/input-date/input-date.component';
import { MatMomentDateModule } from '@angular/material-moment-adapter'
import { ButtonComponent } from '../../shared/components/button/button.component';
import { TableNoPaginationComponent } from '../../shared/components/table-no-pagination/table-no-pagination.component';
import { DialogDeleteComponent } from '../../shared/components/dialog/dialog-delete/dialog-delete.component';
import { DialogBaseComponent } from '../../shared/components/dialog/dialog-base/dialog-base.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ModalUploadImageComponent } from '../../shared/components/modal/modal-upload-image/modal-upload-image.component';
import { UploadImageComponent } from '../../shared/components/upload-image/upload-image.component';
import { UploadXlsxComponent } from '../../shared/components/upload-xlsx/upload-xlsx.component';
import { DialogInputComponent } from '../../shared/components/dialog/dialog-input/dialog-input.component';
import { InputCpfComponent } from '../../shared/components/input-cpf/input-cpf.component';
import { InputCnpjComponent } from '../../shared/components/input-cnpj/input-cnpj.component';
import { DialogTemplateDownloadComponent } from '../../shared/components/dialog/dialog-template-download/dialog-template-download.component';
import { NgbModule, NgbTimepicker } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    InputComponent,
    SnackbarComponent,
    OptionComponent,
    ModalComponent,
    CampanhaDetalhesComponent,
    InputEmailComponent,
    InputRadioComponent,
    InputTextComponent,
    InputSelectComponent,
    InputCpfComponent,
    InputCnpjComponent,
    TableComponent,
    TableNoPaginationComponent,
    InputTelComponent,
    InputHoraComponent,
    InputTextAreaComponent,
    InputDateComponent,
    ButtonComponent,
    DialogBaseComponent,
    DialogDeleteComponent,
    DialogInputComponent,
    DialogTemplateDownloadComponent,
    ModalUploadImageComponent,
    UploadImageComponent,
    UploadXlsxComponent 
  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatInputModule,
    MatTableModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatRadioModule,
    NgxMaskModule.forRoot(),
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatDialogModule,
    ImageCropperModule,
    MatIconModule,
    NgbModule
  ],

  exports: [
    InputComponent,
    OptionComponent,
    CommonModule,
    FormsModule,
    FileUploadModule,
    CampanhaDetalhesComponent,
    ReactiveFormsModule,
    SnackbarComponent,
    ModalComponent,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatInputModule,
    MatTableModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatRadioModule,
    NgxMaskModule,
    MatDatepickerModule,
    MatDialogModule,
    MatNativeDateModule,
    ImageCropperModule,
    InputEmailComponent,
    InputRadioComponent,
    InputTextComponent,
    InputSelectComponent,
    InputCpfComponent,
    InputCnpjComponent,
    TableComponent,
    TableNoPaginationComponent,
    InputTelComponent,
    InputTextAreaComponent,
    InputHoraComponent,
    InputDateComponent,
    ButtonComponent,
    DialogBaseComponent,
    ModalUploadImageComponent,
    UploadImageComponent,
    UploadXlsxComponent,
    MatIconModule,
    NgbModule, NgbTimepicker
  ],

  entryComponents: [
    DialogBaseComponent,
    DialogDeleteComponent,
    DialogInputComponent,
    DialogTemplateDownloadComponent
    
  ]

})

export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        LoginService,
        NotificationService,
        AcessoGuard,
        RouterGuard,
        CampanhaService,
        DashBoardService,
        ElegivelService,
        CargaService,
        ImunizacaoService,
        UsuarioService,
        FileService,
        GeradorService,
        ArquivoService,
        TableReloadService,
        CadastroService,
        CalendarioService,
        ModalService,
        DatePipe, 
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
      ]
    }
  }
}
