import { Routes } from '@angular/router'
import { AcessoGuard } from './modulos/servicos/acesso.guard'
import { RouterGuard } from './modulos/servicos/router.guard'
import { LoginComponent } from './pages/seguranca/login/login.component'
import { MainComponent } from './pages/main/main.component'
import { ModalComponent } from './modulos/shared/modal/modal.component';
import { Error404Component } from './pages/error404/error404.component'
import { ChangePasswordComponent } from './pages/seguranca/change-password/change-password.component';
import { ForgotComponent } from './pages/seguranca/forgot/forgot.component';

export const ROUTES: Routes = [
  {
    path: '', component: MainComponent, canActivate: [AcessoGuard], canLoad: [AcessoGuard],
    children: [
      { path: '', redirectTo: 'dash', pathMatch: 'full' },
      {
        path: 'dash', loadChildren: './modulos/features/dashboard/dashboard.module#DashboardModule',
        canActivate: [RouterGuard], canLoad: [AcessoGuard]
      },
      {
        path: 'campanhas', loadChildren: './modulos/features/campanhas/campanha.module#CampanhaModule',
        canActivate: [RouterGuard], canLoad: [RouterGuard]
      },
      {
        path: 'imunizacoes', loadChildren: './components/imunizacao/imunizacao.module#ImunizacaoModule',
        canActivate: [RouterGuard], canLoad: [RouterGuard]
      },
      {
        path: 'cadastros', loadChildren: './modulos/features/cadastros/cadastro.module#CadastroModule',
        canActivate: [RouterGuard], canLoad: [RouterGuard]
      },
      {
        path: 'gerador', loadChildren: './modulos/features/gerador/gerador.module#GeradorModule',
        canActivate: [RouterGuard], canLoad: [RouterGuard]
      },
      {
        path: 'usuarios', loadChildren: './modulos/features/usuarios/usuario.module#UsuarioModule',
        canActivate: [RouterGuard], canLoad: [RouterGuard]
      },
      {
        path: 'usuarios/detalhes', loadChildren: './modulos/features/usuarios/usuario.module#UsuarioModule',
        canActivate: [RouterGuard], canLoad: [RouterGuard]
      },
      {
        path: 'calendario', loadChildren: './modulos/features/calendario/calendario.module#CalendarioModule',
        canActivate: [RouterGuard], canLoad: [RouterGuard]
      },
      { path: 'logout', component: ModalComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'login/:to', component: LoginComponent },
  { path: 'alterarSenha', component: ChangePasswordComponent },
  { path: 'forgot', component: ForgotComponent },
  { path: '**', component: Error404Component }
]
