import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatSort, PageEvent, Sort, MatTableDataSource } from '@angular/material';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { CadastroService } from '../../../../modulos/features/cadastros/cadastro.service';
import { EmpresaModel, EmpresaTableModel } from '../../../../modulos/features/cadastros/empresa.model';
import { MatSpinner } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material';
import { NotificationService } from '../../../../modulos/servicos/notification.service';

@Component({
	selector: 'ic-distribuidoras',
	templateUrl: '../empresa.component.html',
})
export class DistribuidorasComponent implements OnInit {
	@ViewChild(MatPaginator)
	paginator: MatPaginator;
	@ViewChild(MatSort)
	sort: MatSort;

	displayedColumns: string[] = ['empresa', 'cnpj', 'responsavel', 'email', 'fone'];
	resultsLength = 0;
	isLoadingResults = true;
	//isRateLimitReached = false;
	data: EmpresaTableModel[] = [];
	distribuidoras: EmpresaModel[] = [];
	pageEvent: PageEvent = undefined;
	pageSize = 5;
	pageSizeOptions: number[] = [5, 10, 25, 100];
	tipo: string = 'Distribuidores';
	dataSource: MatTableDataSource<EmpresaTableModel> 
 
	constructor(private cadastroService: CadastroService, private notificationService: NotificationService) {}

	ngOnInit() {
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
		this.data = [];
		this.distribuidoras = [];
		merge(this.paginator.page)
		//merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				startWith({}),
				switchMap(() => {
					this.isLoadingResults = true;
					return this.cadastroService.getParceiros(
						this.tipo,
						this.sort.active,
						this.sort.direction,
						this.paginator.pageIndex,
						this.paginator.pageSize || this.pageSize
					);
				}),
				map(data => {
					// Flip flag to show that loading has finished.
					this.isLoadingResults = false;
					//this.isRateLimitReached = false;
					this.resultsLength = data.contagem + this.paginator.pageSize;
					return data;
				}),
				catchError(() => {
					this.isLoadingResults = false;
					// Catch if the GitHub API has reached its rate limit. Return empty data.
					this.notificationService.notifiy({
						message: `Erro ao acessar dados de ${this.tipo}`,
						tipo: 'E',
					});
					return observableOf([]);
				})
			)
			.subscribe(data => {
				this.distribuidoras = data.retorno;
				this.data = this.mappingDataToTable(this.distribuidoras)
				this.dataSource = new MatTableDataSource(this.data);
				this.dataSource.sort = this.sort
			});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	mappingDataToTable(data: EmpresaModel[]): EmpresaTableModel[] {
		let tabArray = []
		let table: EmpresaTableModel[] = []

		for(let linha of data) {
			let tab: EmpresaTableModel = { _id: '', empresa: '', cnpj: '', responsavel: '', email: '', fone: '' }
			tab._id = linha._id
			tab.cnpj = linha.cnpj
			tab.email = linha.email
			tab.empresa = linha.empresa
			tab.fone = linha.fone
			tab.responsavel = linha.responsavel
			table.push(tab)
		}
		//table = tabArray
		return table
	}

	rowClicked(row: any): void {
        //this.usuarioService.setEditableUser(row)
        //this.router.navigate(['/usuarios/detalhes'])
        // console.log(row);
    }

}