import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from '../../modulos/servicos/login.service';
import { Usuario } from '../../modulos/shared/modelos/profile.model';
import { CampanhaService } from '../../modulos/features/campanhas/campanha.service';
import { ElegivelService } from '../../modulos/features/elegiveis/elegivel.service';
import { ImunizacaoService } from '../../components/imunizacao/vacinacao/vacinacao/service/imunizacao.service';
import { NotificationService } from '../../modulos/servicos/notification.service';
import { GeradorService } from '../../modulos/features/gerador/gerador.service';

@Component({
  selector: 'ic-header',
  templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit {

  @Input() user: Usuario

  constructor(private loginService: LoginService,
              private campanhaService: CampanhaService,
              private elegivelService: ElegivelService,
              private notificationService: NotificationService,
              private imunizacaoService: ImunizacaoService,
              private geradorService: GeradorService ) {
  }

  ngOnInit() {
    this.user = this.loginService.getUsuario()
  }

  resetUsuario() {
    this.user = undefined
    this.campanhaService.reset()
    this.elegivelService.reset()
    this.imunizacaoService.reset()
    this.geradorService.reset()
    this.loginService.logout()
      .subscribe( response => {
        this.notificationService.notifiy({ message: response.mensagem })
      }, response => this.notificationService.notifiy({ message: 'Ocorreu um erro ao desconectar', tipo: 'E' }))
    this.loginService.reset()
  }

}
