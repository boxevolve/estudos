import { Component, OnInit } from '@angular/core'
import { CargaService } from '../../../../modulos/features/carga/carga.service'
import { FileUploader, FileUploaderOptions, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { NotificationService } from '../../../../modulos/servicos/notification.service';

@Component({
	selector: 'ic-carga',
	templateUrl: './carga.component.html',
})

export class CargaComponent implements OnInit {

  cargas:any = [];

  constructor(private cargaService: CargaService,
    private notificationService: NotificationService) {}
  
	ngOnInit() {
    
    
  }

	download(index) {

  }

  atualizar() {
    this.cargas = this.cargaService.getCargas('Elegiveis').subscribe(response => this.cargas = response,
      response => this.notificationService.notifiy({message: 'Cargas não encontradas', tipo: 'E'}));
  }
}