import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FileUploader, FileUploaderOptions, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { LoginService } from '../../../../modulos/servicos/login.service';
import { CampanhaModel } from '../../../../modulos/features/campanhas/campanha.model';
import { CampanhaService } from '../../../../modulos/features/campanhas/campanha.service';
import { NotificationService } from '../../../../modulos/servicos/notification.service';
import { IC_API } from '../../../../app.api';
import { ArquivoService } from '../../../../modulos/features/arquivo/arquivo.service';
import { MatPaginator, MatSort } from '@angular/material';
import { Observable } from 'rxjs';
import { TableReloadService } from '../../../../shared/components/table/table-reload.service';
import { saveAs } from 'file-saver';
import { Util } from '../../../../utils/util';
import { ConfigXlsxModel } from '../../../../shared/model/config-xlsx.model';

// const URL = '/api/';
const uri = `${IC_API}/api/file/upload`;

@Component({
	selector: 'ic-form-up-files',
	templateUrl: './form-up-files.component.html',
})
export class FormUpFilesComponent implements OnInit {
	@ViewChild('inputField')
	inputField: any;

	@Input() public config: ConfigXlsxModel = new ConfigXlsxModel; 
	@Input()
	tiposDeCarga = ['Elegiveis', 'Roteiros'];
	@Input()
	listCampanhas: CampanhaModel[] = [];
	campanha: CampanhaModel;
	tipo: string = '';
	carregarArquivo: boolean = true;
	maxFileSize = 10 * 1024 * 1024;

	uploader: FileUploader = new FileUploader({ url: uri , maxFileSize: this.maxFileSize});
	hasBaseDropZoneOver: boolean = false;
	hasAnotherDropZoneOver: boolean = false;
	attachmentList: any = [];
	options: FileUploaderOptions = {};

	bloquearBtnEnviar = false;

	constructor(
		private loginService: LoginService,
		private campanhaService: CampanhaService,
		private notificationService: NotificationService,
		private arquivoService: ArquivoService,
		private tableReloadService: TableReloadService
	) {}

	ngOnInit() {
		this.options = {
			url: uri,
			maxFileSize: this.maxFileSize,
			authTokenHeader: 'x-access-token',
			authToken: `${this.loginService.usuario.token}`,
			additionalParameter: [],
		};
		this.createConfigXlsx()
		this.uploader.setOptions(this.options);

		this.uploader.onAfterAddingFile = file => {
			file.withCredentials = false;
			this.carregarArquivo = false;
		};

		this.uploader.onCompleteAll = () => {
			console.log(`LOG COMPLETE ALL`);
			console.log(this.uploader.response);
		};

		this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
			if (response) {
				this.attachmentList.push(JSON.parse(response));
				this.arquivoService.attachmentList = this.attachmentList;
			}
			this.tipo = '';
			this.carregarArquivo = true;
		};

		this.uploader.onBeforeUploadItem = item => {
			this.bloquearBtnEnviar = true;
		};

		this.uploader.clearQueue = () => {
			this.uploader.queue = [];
			this.inputField.nativeElement.value = '';
			this.uploader.progress = 0;
		};

		this.uploader.onSuccessItem = (
			item: FileItem,
			response: string,
			status: number,
			headers: ParsedResponseHeaders
		) => {
			// Some of your code
			this.inputField.nativeElement.value = '';
		};

		//if (this.campanhaService.campanhas === undefined) {
			this.campanhaService.getCampanhas().subscribe(
				response => {
					this.listCampanhas = response;
					this.listCampanhas = this.listCampanhas.filter(campanha => campanha.produtos && campanha.produtos.length > 0);
				},
				response => this.notificationService.notifiy(response.mensagem)
			);
		// } else {
		// 	if (this.listCampanhas.length == 0) {
		// 		this.listCampanhas = this.campanhaService.campanhas;
		// 	}
		// }
	}

	public createConfigXlsx(): void {
		this.config = new ConfigXlsxModel(this, null, null, null, null, 1, null,
	//    'Código Unidade', false);
	null, false, true, 'Template Importação Elegiveis.xlsx', 'templates');
	}

	emitSelecionaCampanha(event: any) {
		if (event) {
			this.campanha = this.listCampanhas.find(campanha => campanha._id === event.target.value);
			if (this.campanha) {
				if (this.tipo) this.options.additionalParameter = { tipo: this.tipo, campanha: this.campanha._id };
				else this.options.additionalParameter = { tipo: undefined, camapnha: this.campanha._id };
				this.tableReloadService.reload.next(true);
			}
		}
		this.uploader.setOptions(this.options);
	}

	emitSelecionaTipo(event: any) {
		if (event) {
			this.tipo = event.target.value;
			if (this.campanha) this.options.additionalParameter = { tipo: this.tipo, campanha: this.campanha._id };
			else this.options.additionalParameter = { tipo: this.tipo, campanha: undefined };
		}
		this.uploader.setOptions(this.options);
	}

	public fileOverBase(e: any): void {
		this.hasBaseDropZoneOver = e;
	}

	public fileOverAnother(e: any): void {
		this.hasAnotherDropZoneOver = e;
	}

	public consultar(paginator: MatPaginator, sort: MatSort, pageSize: number): Observable<any> {
		return this.arquivoService.consultar(
			this.campanha,
			sort.active,
			sort.direction,
			paginator.pageIndex,
			paginator.pageSize || pageSize
		);
	}

	// INICIO - Métodos para preencher a tabela
	public consultarObservable(
		self: any,
		paginator?: MatPaginator,
		sort?: MatSort,
		pageSize?: number
	): Observable<any> {
		return self.consultar(paginator, sort, pageSize);
	}

	public mappingDataToTable(data: any[]): any[] {
		return data;
	}

	// INICIO - Variáveis para tabela
	displayedColumns: string[] = ['nomecampanha', 'data', 'nomeoriginal', 'tipo', 'sucesso', 'erros', 'status'];
	headers: string[] = [ 'Campanha', 'Data importação', 'Nome do Arquivo', 'Tipo', 'Qtd com sucesso', 'Qtd com erros', 'Status'];
	errorMessage = 'Erro ao acessar dados das importações de elegiveis';
	// FIM - Variáveis para tabela

	public rowClicked(row: any): void {
		this.baixarArquivo(row);
	}

	public atualizarCargas(): void {
		this.tableReloadService.reload.next(true);
	}

	public baixarArquivo(row: any): void {
		if (row.erros > 0)
			this.arquivoService.downloadFile(row.nome, 'naovalidados').subscribe(data => {
				saveAs(data, row.nome), error => console.error(error);
			});
	}

	public getCampanhas(): CampanhaModel[] {
		return !Util.isEmpty(this.listCampanhas) ? this.listCampanhas : [];
	}

	public downloadFile(): void {
		this.arquivoService.downloadFile(this.config.fileDownload, this.config.folderDownload).subscribe(data => {
			saveAs(data, this.config.fileDownload), error => console.error(error)
		})
	}
}
