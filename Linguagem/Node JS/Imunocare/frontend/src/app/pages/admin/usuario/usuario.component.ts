

import { Component, OnInit, AfterViewInit, ViewChild, Input, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, PageEvent, Sort } from '@angular/material';
import { NotificationService } from '../../../modulos/servicos/notification.service';
import { UsuarioModel } from '../../../modulos/features/usuarios/usuario.model';
import { UsuarioService } from '../../../modulos/features/usuarios/usuario.service';
import "rxjs/add/operator/takeWhile";
import { Router } from '@angular/router';

@Component({
    selector: 'ic-usuario',
    templateUrl: './usuario.component.html'
})
export class UsuarioComponent implements OnInit, AfterViewInit {


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    usuarios: UsuarioModel[] = []
    @Input() dataSource = new MatTableDataSource()
    @Input() pageSizeOptions = [10, 50, 100, 250, 500];
    @Input() displayedColumns = []
    @Input() sortedData = undefined
    @Input() pageEvent: PageEvent
    @Input() length = 100;
    @Input() pageSize = 10;
    @Input() i = 0;

    public constructor(private usuarioService: UsuarioService, 
                       private notificationService: NotificationService, 
                       private changeDetectorRefs: ChangeDetectorRef,
                       private router: Router) { }

    ngOnInit() {
        this.getUsuarios()
        // console.log(`INIT: ${JSON.stringify(this.usuarios)}`)
    }

    ngAfterViewInit() {

        this.changeDetectorRefs.detectChanges();

        // console.log(`VIEW_INIT: ${JSON.stringify(this.usuarios)}`)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        // reset the paginator after sorting
        if (this.sort !== undefined) {
            this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        }
    }

    setPageSizeOptions(setPageSizeOptionsInput: string) {
        this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    sortData(sort: Sort) {
        const data = this.usuarios.slice();
        if (!sort.active || sort.direction == '') {
            this.dataSource.data = data;
            return;
        }

        this.dataSource.data = data.sort((a, b) => {
            let isAsc = sort.direction == 'asc';
            switch (sort.active) {

                case 'usuario': return compare(a.usuario, b.usuario, isAsc);
                case 'nome': return compare(a.nome, b.nome, isAsc);
                case 'email': return compare(a.email, b.email, isAsc);
                // case 'email_cliente': return compare(a.email_cliente, b.email_cliente, isAsc);
                case 'perfil': return compare(a.perfil, b.perfil, isAsc);
                case 'primeiro_acesso': return compare(a.primeiro_acesso, b.primeiro_acesso, isAsc);
                case 'email_enviado': return compare(a.email_enviado, b.email_enviado, isAsc);
                case 'loginAttempts': return compare(a.loginAttempts, b.loginAttempts, isAsc);
                case 'lockUntil': return compare(a.lockUntil, b.lockUntil, isAsc);

                default: return 0;
            }
        });
    }

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
    }

    rowClicked(row: any): void {
        this.usuarioService.setEditableUser(row)
        this.router.navigate(['/usuarios/detalhes'])
        // console.log(row);
    }

    retornaZebra(i): string {
        let cor = ''
        if (i < 2) {
            if (i === 0) {
                cor = '#ECF0F6'
            } else {
                cor = 'white'
            }
        } else {
            if (i % 2 === 0) {
                cor = '#ECF0F6'
            } else {
                cor = 'white'
            }
        }
        return cor
    }

    getUsuarios() {
        this.usuarioService.getUsuarios()
            .takeWhile(response => response.length > 0)
            .subscribe(response => this.usuarios = response,
                error => this.notificationService.notifiy({ message: error.message, tipo: 'E' }))

        if (this.usuarios) {
            this.dataSource.data = this.usuarios.slice();
        }

        this.displayedColumns = [
            'usuario',
            'nome',
            'email',
            //'email_cliente',
            'perfil',
            // 'campanhas',
            'primeiro_acesso',
            'email_enviado',
            'loginAttempts',
            'lockUntil'
        ]
    }
}

function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
