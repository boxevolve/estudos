import { Component, OnInit, Input } from '@angular/core';
import { UsuarioModel } from '../../../modulos/features/usuarios/usuario.model';
import { UsuarioService } from '../../../modulos/features/usuarios/usuario.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CampanhaModel } from '../../../modulos/features/campanhas/campanha.model';

@Component({
  selector: 'ic-usuario-details',
  templateUrl: './usuario-details.component.html'
})
export class UsuarioDetailsComponent implements OnInit {


  usuarioForm: FormGroup
  @Input() usuario: UsuarioModel
  @Input() campanhas = []

  public constructor(private usuarioService: UsuarioService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.usuario = this.usuarioService.usuario
    this.campanhas = this.usuario.campanhas
    console.log(this.usuario)
    for( let camp of this.campanhas) {
      console.log(`campanha: ${camp.campanha} - cliente: ${camp.cliente}`)
    }

    this.usuarioForm = this.formBuilder.group({
      usuario: this.formBuilder.control(''),
      nome: this.formBuilder.control(''),
      email: this.formBuilder.control(''),
      campanhas: this.formBuilder.control('')
    })
  }

}
