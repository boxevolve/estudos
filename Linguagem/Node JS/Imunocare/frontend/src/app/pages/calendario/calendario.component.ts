import * as $ from 'jquery';
import { Component, OnInit, ViewChild, Input, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { CalendarioService } from '../../modulos/features/calendario/calendario.service';
import { EventoCalModel, EventoModel } from '../../modulos/features/calendario/evento.model';
import { NotificationService } from '../../modulos/servicos/notification.service';
import { ModalService } from '../../modulos/shared/modal/modal.service';
import ModalUtil from '../../utils/modal';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'ic-calendario',
    templateUrl: './calendario.component.html',
})
export class CalendarioComponent implements OnInit, AfterViewInit {
    _MENSAL = 0
    _SEMANAL = 1
    _DIARIO = 2
    _LISTA = 3

    idModal: string;


    horasDoDia: number[] = []
    idSemana: number = 0
    idDia: number = 0
    calendario: any[] = []
    calendarType: number = 0

    eventoAtual: EventoCalModel
    eventoDoDia: EventoModel
    eventos: EventoModel[] = []
    eventosCal: EventoCalModel[] = []

    @Output() updateEvent = new EventEmitter()

    frameBgColor = '#3a77ad'

    options = {
        weekday: 'long',
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
    };
    weekClass: string[] = [' fc-sun', ' fc-mon', ' fc-tue', ' fc-wed', ' fc-thu', ' fc-fri', ' fc-sat']
    textoSemana: string[] = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado']

    dataCalendario = new Date()
    hoje = new Date()
    idData = new Date()
    dia1 = new Date()
    bodyText: string

    constructor(protected calendarioService: CalendarioService, 
                private notificationService: NotificationService,
                private router: Router,
                private activateRoute: ActivatedRoute) {}
    //            private modalService: ModalService) {}

    ngOnInit() {
        this.reposicionar(this.calendarioService.getDataRetorno(), this.calendarioService.getCalendarioView())
        this.setupCalendario()
        this.getEventos()
    }

    ngAfterViewInit() {
        // this.getEventos();
        this.setupCalendario();
    }

    setupCalendario() {
        let diaSemana01 = new Date(this.dataCalendario.getFullYear(), this.dataCalendario.getMonth(), 1).getDay();
        this.dia1 = new Date(this.dataCalendario.getFullYear(), this.dataCalendario.getMonth(), 1)
        // console.log(`DIA DA SEMANA DIA 1: ${diaSemana01}`)
        let diaCalendario = new Date(
            this.dataCalendario.getFullYear(),
            this.dataCalendario.getMonth(),
            1 - (diaSemana01 + 1)
        );
        // console.log(`DIA DO PRIMEIRO DIA DO CALENDARIO: ${diaCalendario.toLocaleDateString('pt-BR', this.options)}`)

        if (diaCalendario) {
            this.calendario = [];
            let sairFor = false;
            for (let l = 0; l < 6; l++) {
                let semana: Date[] = [];

                // console.log(`SEMANA: ${l}`)
                for (let d = 0; d < 7; d++) {
                    diaCalendario = new Date(
                        diaCalendario.getFullYear(),
                        diaCalendario.getMonth(),
                        diaCalendario.getDate() + 1
                    );
                    // console.log(`Data: ${diaCalendario.toLocaleDateString('pt-BR', this.options)}`)
                    if (
                        (diaCalendario.getFullYear() === this.dataCalendario.getFullYear() &&
                            diaCalendario.getMonth() > this.dataCalendario.getMonth()) ||
                        (diaCalendario.getFullYear() > this.dataCalendario.getFullYear() &&
                            diaCalendario.getMonth() < this.dataCalendario.getMonth())
                    )
                        sairFor = true;

                    if (d === 0 && sairFor) break;
                    semana.push(diaCalendario);
                }
                if (semana.length > 0) this.calendario.push(semana);
                if (sairFor) break;
            }
        }
    }

    getDayClass(dia: Date): string {
        let monthClass = '';
        let dayClass = '';

        if (this.isMensal()) {
            if (
                dia.getFullYear() === this.hoje.getFullYear() &&
                dia.getMonth() === this.hoje.getMonth() &&
                dia.getDate() === this.hoje.getDate()
            ) {
                // console.log(`HOJE: ${this.hoje.toLocaleDateString('pt-BR', this.options)}`)
                dayClass = ' fc-today table-warning';
                monthClass = ' fc-month';
            } else {
                if (dia < this.hoje) {
                    dayClass = ' fc-past';
                } else {
                    dayClass = ' fc-future';
                }
                if (
                    dia.getFullYear() === this.dataCalendario.getFullYear() &&
                    dia.getMonth() === this.dataCalendario.getMonth()
                ) {
                    monthClass = ' fc-month';
                } else {
                    monthClass = ' fc-other-month';
                }
            }
            // console.log(`MENSAL: DIA ${dia.toLocaleDateString('pt-BR', this.options)} - CLASS: ${monthClass + dayClass + this.weekClass[dia.getDay()]}`)
        }

        if (this.isSemanal()) {
            if (
                dia.getFullYear() === this.hoje.getFullYear() &&
                dia.getMonth() === this.hoje.getMonth() &&
                dia.getDate() === this.hoje.getDate()
            ) {
                // console.log(`HOJE: ${this.hoje.toLocaleDateString('pt-BR', this.options)}`)
                dayClass = ' fc-today';
                monthClass = ' fc-month';
            } else {
                if (dia.getDay() === 0) {
                    dayClass = ' fc-past';
                    monthClass = ' fc-other-month';
                } else if (dia.getDay() === 6) {
                    dayClass = ' fc-future';
                    monthClass = ' fc-other-month';
                } else {
                    monthClass = ' fc-month';
                }
            }

            // console.log(`SEMANA: DIA ${dia.toLocaleDateString('pt-BR', this.options)} - CLASS: ${monthClass + dayClass + this.weekClass[dia.getDay()]}`)
        }

        return monthClass + dayClass + this.weekClass[dia.getDay()];
    }

    setToday() {
        this.updateToday();

        this.dataCalendario = new Date(this.hoje.getFullYear(), this.hoje.getMonth(), 1);
        // console.log(`SET HOJE: ${this.hoje.toLocaleDateString('pt-BR', this.options)}`)
        this.setupCalendario();
        //if(this.isSemanal()) {
        for (let c = 0; c < this.calendario.length; c++) {
            this.idSemana = c;
            let semana = this.calendario[c];
            let achou = false;
            for (let s = 0; s < semana.length; s++) {
                let dia = semana[s];
                if (
                    dia.getFullYear() === this.hoje.getFullYear() &&
                    dia.getMonth() === this.hoje.getMonth() &&
                    dia.getDate() === this.hoje.getDate()
                ) {
                    this.idDia = s;
                    this.idData = this.hoje;
                    achou = true;
                    break;
                }
            }
            if (achou) break;
        }
        //}
    }

    setBackCalendar() {
        this.updateToday();

        if (this.isMensal()) this.minusMonth();
        if (this.isSemanal()) this.minusWeek();
        if (this.isDiario()) this.minusDay();
    }

    setForwardCalendar() {
        this.updateToday();

        if (this.isMensal()) this.addMonth();
        if (this.isSemanal()) this.addWeek();
        if (this.isDiario()) this.addDay();
    }

    setDay(dia: Date) {
        for (let i = 0; i < this.calendario[this.idSemana].length; i++) {
            let d = this.calendario[this.idSemana][i];
            if (
                dia.getFullYear() === d.getFullYear() &&
                dia.getMonth() === d.getMonth() &&
                dia.getDate() === d.getDate()
            ) {
                this.idDia = i;
                break;
            }
        }
    }

    setCalendarView(tipo: number) {
        this.calendarType = tipo;
        //this.updateToday()

        if (this.isSemanal() || this.isDiario()) {
            this.setToday();

            this.horasDoDia = [];
            for (let i = 0; i < 24; i++) {
                this.horasDoDia.push(i);
            }
        }
    }

    isMensal(): boolean {
        return this.calendarType === this._MENSAL;
    }

    isSemanal(): boolean {
        return this.calendarType === this._SEMANAL;
    }
    isDiario(): boolean {
        return this.calendarType === this._DIARIO;
    }
    isLista(): boolean {
        return this.calendarType === this._LISTA;
    }

    getSemana(id: number): any[] {
        return this.calendario[id];
    }
    getTextoSemana(dia: number): string {
        return this.textoSemana[dia];
    }

    updateToday() {
        const dia = new Date()
        if (
            dia.getFullYear() !== this.hoje.getFullYear() ||
            dia.getMonth() !== this.hoje.getMonth() ||
            dia.getDate() !== this.hoje.getDate()
        ) {
            this.hoje.setFullYear(dia.getFullYear());
            this.hoje.setMonth(dia.getMonth());
            this.hoje.setDate(dia.getDate());
        }
    }

    addMonth() {
        this.dataCalendario.setMonth(this.dataCalendario.getMonth() + 1);
        this.setupCalendario();
        this.getEventos();
    }

    addWeek() {
        if (this.idSemana === this.calendario.length - 1) {
            if (this.calendario[this.idSemana][0].getMonth() !== this.calendario[this.idSemana][6].getMonth()) {
                this.idSemana = 1;
            } else {
                this.idSemana = 0;
            }
            this.addMonth();
        } else {
            this.idSemana = this.idSemana + 1;
        }
        this.idDia = 0;
        this.idData = this.calendario[this.idSemana][this.idDia];
    }

    addDay() {
        if (this.idDia === this.calendario[this.idSemana].length - 1) {
            this.addWeek();
        } else {
            this.idDia++;
            this.idData = this.calendario[this.idSemana][this.idDia];
        }
    }

    minusMonth() {
        this.dataCalendario.setMonth(this.dataCalendario.getMonth() - 1);
        this.setupCalendario();
        this.getEventos();
    }

    minusWeek() {
        if (this.idSemana === 0) {
            if (this.calendario[this.idSemana][0].getMonth() === this.calendario[this.idSemana][6].getMonth()) {
                this.minusMonth();
                this.idSemana = this.calendario.length - 1;
            } else {
                this.minusMonth();
                this.idSemana = this.calendario.length - 2; 
            }
        } else {
            this.idSemana = this.idSemana - 1;
        }
        this.idData = this.calendario[this.idSemana][this.idDia];
    }

    minusDay() {
        if (this.idDia === 0) {
            this.minusWeek();
            this.idDia = this.calendario[this.idSemana].length - 1;
        } else this.idDia--;
        this.idData = this.calendario[this.idSemana][this.idDia];
    }

    editarEvento(s, d, h?, m?, idEvento?: string) {
        let data = new Date(this.calendario[s][d])
        data.setHours(h, m, 0)
        if(idEvento) this.eventoDoDia = this.calendarioService.getEventosDoDia(data, idEvento)
        else {
            this.eventoDoDia = this.calendarioService.resetEventoModel()
            this.eventoDoDia.inicio = data
            this.eventoDoDia.fim = data
        }
        //this.updateEvent.emit(this.eventoDoDia)
        //this.bodyText = data.toLocaleDateString('pt-BR', this.options)
        //console.log(`EVENTO DO DIA: ${JSON.stringify(this.eventoDoDia)}`)
        //this.openModal('custom-modal-1')
        //ModalUtil.showModalBody(this.idModal);
        this.calendarioService.setEventoEdit(this.eventoDoDia)
        this.calendarioService.setRetornoCalendario(data, this.calendarType)
        this.router.navigate(['eventos'], { relativeTo: this.activateRoute }); 
 
    }

    getEventos(data?: Date) {
        //this.calendarioService.getEventos(this.dia1).subscribe(
        if (data) {
            this.calendarioService.getEventos(data).subscribe(
                data => {
                this.eventos = data.retorno;
                this.mapDataToTable();
                //console.log(JSON.stringify(this.eventos))
            },
            response => this.notificationService.notifiy({ message: response.mensagem, tipo: 'E' })
        );

        } else {
            this.calendarioService.getEventos().subscribe(
                data => {
                this.eventos = data.retorno;
                this.mapDataToTable();
                //console.log(JSON.stringify(this.eventos))
            },
            response => this.notificationService.notifiy({ message: response.mensagem, tipo: 'E' })
        );
        }
    }

    mapDataToTable() {
        let evArray = [];
        this.eventosCal = [];
        let eventos: EventoModel[] = this.eventos;
        for (let ev of eventos) {
            let evento = this.calendarioService.resetEvento();
            evento._id = ev._id;
            evento.titulo = ev.titulo;
            evento.inicio = this.setDate(ev.inicio); //this.setDate(ev.inicio)
            evento.fim = this.setDate(ev.fim);
            evento.diatodo = ev.diatodo;
            
            if (ev.agrupamento) evento.agrupamento = ev.agrupamento
            if (ev.campanha) evento.campanha = ev.campanha._id
            if (ev.fornecedor) evento.fornecedor = ev.fornecedor._id;
            evento.filial = ev.filial._id
            evento.unidade = ev.unidade.cod_unid
            evento.texto_evento = ev.texto_evento;
            evArray.push(evento);
        }

        this.eventosCal = evArray;
    }

    setDate(dia: Date): Date {
        const d: Date = new Date(dia);
        const ndia = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes());
        return new Date(ndia);
    }

    getDayEvents(dia: Date, full: boolean, hora?: number): EventoCalModel[] {
        
        let evs: EventoCalModel[] = []

        for (let evento of this.eventosCal) {
            if ( dia.getFullYear() >= evento.inicio.getFullYear() && dia.getMonth() >= evento.inicio.getMonth() && dia.getDate() >= evento.inicio.getDate()
                && dia.getFullYear() <= evento.fim.getFullYear() && dia.getMonth() <= evento.fim.getMonth() && dia.getDate() <= evento.fim.getDate() ) {
                
                if(this.isMensal()) evs.push(evento)
                else {
                    if(full) {
                        if(evento.diatodo) evs.push(evento)
                    } else {
                        if(!evento.diatodo && ( hora === undefined || hora === evento.inicio.getHours()) ) //{
                            //console.log(`sem hora > valor hora: ${hora} - ${evento.inicio.getHours()}`)
                            evs.push(evento)
                        //} else if(hora === evento.inicio.getHours()) {
                            //console.log(`com hora > valor hora: ${hora} - ${evento.inicio.getHours()}`)
                        //	evs.push(evento)
                        //}
                    }
                }
            }
        }
        return evs;
    }
    getDetalhesEvento(): EventoCalModel {
        if(this.eventoAtual) return this.eventoAtual
        else return null
    }

    setDetalhesEvento(ev:EventoCalModel) {
        this.eventoAtual = ev

    }

    reposicionar(dia: Date, tipo: number) {
        this.setCalendarView(tipo)
        this.dataCalendario = new Date(dia.getFullYear(), dia.getMonth(), 1);
    }
/*
    openModal(id: string) {
        this.modalService.open(id);
    }
 
    closeModal(id: string) {
        this.modalService.close(id);
    }
*/


  // Modal incluir elegível
  public setIdModal(event?: any): void {
    this.idModal = event;
  }

  public receiveData(dia: Date): void {
    this.getEventos(dia)
  }

}
