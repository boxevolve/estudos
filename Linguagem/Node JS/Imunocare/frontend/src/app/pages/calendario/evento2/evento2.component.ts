import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EventoModel, EventoCalModel } from '../../../modulos/features/calendario/evento.model';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ic-evento2',
  templateUrl: './evento2.component.html',
  styleUrls: ['./evento2.component.css']
})
export class Evento2Component implements OnInit, OnChanges {

  evForm: FormGroup
  @Input() evento: EventoModel;
  @Input() inicioDt: Date 
  inicioHr: String = '00:00'
  fimDt: Date
  fimHr: String
  

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {

    this.evForm = this.formBuilder.group({
      inicioDt: this.formBuilder.control(''), //Date,
    })
  }

  ngOnChanges() {
    console.log(`Data Inicio: ${this.evForm.get('inicioDt')}`)
  }

  onDateSelect(event: NgbDate) {
    console.log(`Data Inicio: ${event.day} - ${event.month} - ${event.year}`)
  }

}
