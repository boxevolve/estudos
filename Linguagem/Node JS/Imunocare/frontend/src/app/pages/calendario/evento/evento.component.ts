import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventoModel, EventoCalModel } from '../../../modulos/features/calendario/evento.model';
import { EmpresaTableModel, EmpresaModel } from '../../../modulos/features/cadastros/empresa.model';
import { CampanhaTableModel, CampanhaModel, FilialCampanhaModel, UnidadeModel } from '../../../modulos/features/campanhas/campanha.model';
import { CampanhaService } from '../../../modulos/features/campanhas/campanha.service';
import { CadastroService } from '../../../modulos/features/cadastros/cadastro.service';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { NotificationService } from '../../../modulos/servicos/notification.service';
import { CalendarioService } from '../../../modulos/features/calendario/calendario.service';
import { NgbDate, NgbDateStruct, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: 'ic-evento',
    templateUrl: './evento.component.html', 
    styleUrls: ['./evento.component.css'],
})
export class EventoComponent implements OnInit {

    @Input() idBase = 'void';
    @Input() title: string;
    @Input() titulo: string;
    @Input() texto_evento: string;
    @Input() diatodo: boolean = false;
    @Input() minWidth = 0;
    @Input() value: any;
    @Input() campanha: CampanhaModel;
    @Input() evento: EventoModel;
    @Input() cpf: string;
    @Input() iniciodt: NgbDate
    @Input() inicioHr: NgbTimeStruct = { hour: 0, minute: 0, second: 0}
    @Input() fimdt: NgbDate
    @Input() fimHr: NgbTimeStruct = { hour: 0, minute: 0, second: 0}
    @Input() numero: string = '0'
    @Input() complemento: string = ""

    @Output() submit = new EventEmitter();

    evForm: FormGroup;
    ev: EventoCalModel;
    listFornecedores: EmpresaTableModel[] = [];
    listCampanhas: CampanhaTableModel[] = [];
    
    // fornecedores: EmpresaModel[] = []
    campanhas: CampanhaModel[] = [];
    fornecedores: EmpresaModel[] = [];
    filiais: FilialCampanhaModel[] = [];
    unidades: UnidadeModel[] = [];

    constructor( private formBuilder: FormBuilder,
                private campanhaService: CampanhaService,
                private cadastroService: CadastroService,
                private calendarioService: CalendarioService,
                private notificationService: NotificationService) {

    
    }
    ngOnInit() {

      this.evento = this.calendarioService.getEventoEdit()
      if(this.evento.unidade === undefined) this.evento.unidade = this.initUnidade()
      else {
        if(this.evento.unidade.numero === undefined) this.evento.unidade.numero = 0
        if(this.evento.unidade.complemento === undefined) this.evento.unidade.complemento = ""
      }
      console.log(`ngOnInit: ${JSON.stringify(this.evento)}`);

      this.evForm = this.formBuilder.group({
          inicioDt: this.formBuilder.control(''), //Date,
          inicioHr: this.formBuilder.control(''),
          fimDt: this.formBuilder.control(''), //Date,
          fimHr: this.formBuilder.control(''), //Date,
          diatodo: this.formBuilder.control(''),
          titulo: this.formBuilder.control('', [Validators.required]),
          texto_evento: this.formBuilder.control(''),
          fornecedor: this.formBuilder.control(''),
          campanha: this.formBuilder.control(''),
          filial: this.formBuilder.control(''),
          unidade: this.formBuilder.control(''),
          endereco: this.formBuilder.control(''),
          bairro: this.formBuilder.control(''),
          cidade: this.formBuilder.control(''),
          estado: this.formBuilder.control(''),
          cep: this.formBuilder.control(''),
          numero: this.formBuilder.control(''),
          complemento: this.formBuilder.control(''),
      });
      this.getInicioDt()
      this.getFimDt()
      this.getFornecedores();
      this.getCampanhas();

    } 

    getInicioDt() {
      if (this.evento.inicio) {
        const ini = new Date(this.evento.inicio)
        const iniDt =  new NgbDate(ini.getFullYear(),
                              ini.getMonth(), 
                              ini.getDate())
        this.evForm.patchValue({"inicioDt": iniDt })

        const iniHr = { hour: ini.getHours(), minute: ini.getMinutes(), second: ini.getSeconds() }
        this.evForm.patchValue({"inicioHr": iniHr })

      }
    }
    getFimDt() {
      if (this.evento.fim === undefined) this.evento.fim = this.evento.inicio
      if (this.evento.fim) {
        const ini = new Date(this.evento.fim)
        const iniDt =  new NgbDate(ini.getFullYear(),
                              ini.getMonth(), 
                              ini.getDate())
        this.evForm.patchValue({"fimDt": iniDt })

        const iniHr = { hour: ini.getHours(), minute: ini.getMinutes(), second: ini.getSeconds() }
        this.evForm.patchValue({"fimHr": iniHr })
      }
    }
    onDateSelectInicioDt(event: NgbDate) {
      console.log(`Data Inicio: ${event.day} - ${event.month} - ${event.year}`)
    }
    onDateSelectFimDt(event: NgbDate) {
      console.log(`Data Fim: ${event.day} - ${event.month} - ${event.year}`)
    }

    onTimeSelectInicioHr(event: NgbTimeStruct) {
      console.log(`Hora Inicio: ${event.hour}:${event.minute}:${event.second}`)
    }

    isAllDayEvent(): boolean {
      return this.evento.diatodo
    }

    public getStyle(): any {
      return { 'min-width': `${this.minWidth}% !important` };
    }
    
    salvarEvento(event?: any): void {

      let ev = this.initEventoCal();
      ev._id = this.evento._id;
      ev.inicio = this.evento.inicio = new Date(event.inicioDt.year, event.inicioDt.month, event.inicioDt.day, event.inicioHr.hour, event.inicioHr.minute, event.inicioHr.second, 0)
      ev.fim = this.evento.fim       = new Date(event.fimDt.year,    event.fimDt.month,    event.fimDt.day,    event.fimHr.hour,    event.fimHr.minute,    event.fimHr.second,    0)
      ev.diatodo = event.diatodo;
      ev.titulo = this.evento.titulo = event.titulo;
      ev.texto_evento = this.evento.texto_evento = event.texto_evento;
      ev.fornecedor = this.evento.fornecedor._id;
      ev.agrupamento = this.evento.agrupamento;
      ev.campanha = this.evento.campanha._id;
      ev.filial = this.evento.filial._id;
      ev.unidade = this.evento.unidade._id;
      this.evento.unidade.numero = event.numero
      this.evento.unidade.complemento = event.complemento

      console.log(`EV: ${JSON.stringify(ev)}`);
      console.log(`EVENTO: ${JSON.stringify(this.evento)}`)

    }
    
    cancelarEdicao() {}
    
    buscarCEP() {}
    
    emitSelecionaFornecedor(event: any) {
      console.log(`FORN: ${event.target.value}`);
      let forn = this.listFornecedores.find(forn => forn._id === event.target.value)
      this.evento.fornecedor._id = forn._id;

    }
    
    emitSelecionaCampanha(event: any) {
      this.campanha = this.campanhas.find(camp => camp._id === event.target.value);
      this.campanhaService.findCampanhaDB(this.campanha._id).subscribe(
          campanha => {
              this.campanha = campanha.retorno;
              this.evento.campanha._id = this.campanha._id;
              console.log(`Campanha: ${this.evento.campanha._id} - ${JSON.stringify(this.campanha)}`);
              this.filiais = [];
              if (this.campanha.filiais) {
                  for (let filial of this.campanha.filiais) {
                      this.filiais.push(filial);
                  }
              }
          },
          response => this.notificationService.notifiy({ message: 'Erro ao buscar Campanha', tipo: 'E' })
      );
    }
    
    emitSelecionaFilial(event: any) {
      let filial = this.filiais.find(fil => fil._id === event.target.value);
      this.evento.filial._id = filial._id;
      this.unidades = [];
      if (filial.unidades) {
          for (let unid of filial.unidades) {
              console.log(`unidade: ${JSON.stringify(unid._id)}`);
              this.unidades.push(unid);
          }
      }
    }
    
    emitSelecionaUnidade(event: any) {
      this.evento.unidade = this.unidades.find(unid => unid.cod_unid === event.target.value);
    }
    
    initUnidade(): UnidadeModel {
      const unid: UnidadeModel = {
          _id: '',
          cod_unid: '',
          nome_unid: '',
          endereco: {
              _id: '',
              endereco: '',
              bairro: '',
              cidade: '',
              estado: '',
              cep: '',
              data: new Date(),
              criado_por: '',
          },
          numero: 0,
          complemento: '',
          previsao_doses: 0,
      };
      return unid;
    }
    
    initEventoCal(): EventoCalModel {
      const ev: EventoCalModel = {
          _id: '',
          titulo: '',
          inicio: new Date(),
          fim: new Date(),
          diatodo: false,
          agrupamento: '',
          campanha: '',
          filial: '',
          unidade: '',
          fornecedor: '',
          texto_evento: '',
      };
      return ev;
    }
    
    getFornecedores() {
      console.log(`GET FORNEC`);
    
      if (this.cadastroService.fornecedores === undefined || this.cadastroService.fornecedores.length === 0) {
          console.log(`GET FORNEC indo buscar`);
          this.cadastroService.getFornecedores().subscribe(
              response => {
                  this.fornecedores = response;
                  this.listFornecedores = this.mappingEmpresaDataToTable(this.fornecedores);
              },
              response => this.notificationService.notifiy(response.mensagem)
          );
      } else {
          console.log(`GET FORNEC pegando pronto`);
          this.fornecedores = this.cadastroService.fornecedores;
          this.listFornecedores = this.mappingEmpresaDataToTable(this.fornecedores);
      }
    }
    
    atualizaEvento(evento: EventoModel) {
      console.log(`chegou evento: ${JSON.stringify(evento)}`);
      this.evento = evento;
      if (this.evento.inicio === undefined) this.evento.inicio = new Date();
      if (this.evento.fim === undefined) this.evento.fim = new Date();
    }
    
    getCampanhas() {
      if (this.campanhaService.campanhas === undefined || this.campanhaService.campanhas.length === 0) {
          console.log(`getCampanhas: Campanhas undef`);
          this.campanhaService.getCampanhas().subscribe(
              response => {
                  this.campanhas = response;
                  this.listCampanhas = this.mappingCampanhaDataToTable(this.campanhas);
              },
              response => this.notificationService.notifiy(response.mensagem)
          );
      } else {
          console.log(`getCampanhas: Campanhas já carregadas`);
          this.campanhas = this.campanhaService.campanhas;
          this.listCampanhas = this.mappingCampanhaDataToTable(this.campanhas);
      }
    }
    
    mappingEmpresaDataToTable(data: EmpresaModel[]): EmpresaTableModel[] {
      let tabArray = [];
      let table: EmpresaTableModel[] = [];
    
      for (let linha of data) {
          let tab: EmpresaTableModel = { _id: '', empresa: '', cnpj: '', responsavel: '', email: '', fone: '' };
          tab._id = linha._id;
          tab.cnpj = linha.cnpj;
          tab.email = linha.email;
          tab.empresa = linha.empresa;
          tab.fone = linha.fone;
          tab.responsavel = linha.responsavel;
          table.push(tab);
          console.log(tab);
      }
      //table = tabArray
      return table;
    }
    
    mappingCampanhaDataToTable(data: CampanhaModel[]): CampanhaTableModel[] {
      let tabArray = [];
      let table: CampanhaTableModel[] = [];
    
      for (let linha of data) {
          let tab: CampanhaTableModel = { _id: '', campanha: '', cliente: '', cnpj: '' };
          tab._id = linha._id;
          tab.campanha = linha.campanha;
          if (linha.cliente !== undefined) {
              tab.cliente = linha.cliente.empresa;
              tab.cnpj = linha.cliente.cnpj;
          }
          table.push(tab);
      }
      //table = tabArray
      return table;
    }
}
    