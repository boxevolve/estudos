import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, PageEvent, Sort } from '@angular/material';
import { ElegivelListModel } from '../../../modulos/features/elegiveis/elegivel.model';
import { CampanhaModel } from '../../../modulos/features/campanhas/campanha.model';
import { CampanhaService } from '../../../modulos/features/campanhas/campanha.service';
import { ElegivelService } from '../../../modulos/features/elegiveis/elegivel.service';
import { NotificationService } from '../../../modulos/servicos/notification.service';
import "rxjs/add/operator/takeWhile";

@Component({
    selector: 'ic-elegiveis-table',
    templateUrl: './elegiveis-table.component.html'
})


export class ElegiveisTableComponent implements OnInit, AfterViewInit {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Input() campanha: CampanhaModel
    @Input() elegiveis: ElegivelListModel[]
    @Input() dataSource = new MatTableDataSource()
    @Input() pageSizeOptions = [10, 50, 100, 250, 500];
    @Input() displayedColumns = []
    @Input() sortedData = undefined
    @Input() pageEvent: PageEvent
    @Input() length = 100;
    @Input() pageSize = 10;
    @Input() i = 0;

    constructor(private elegivelService: ElegivelService,
        private campanhaService: CampanhaService,
        private notificationService: NotificationService) { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        // reset the paginator after sorting
        if (this.sort !== undefined) {
            this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        }
    }

    setPageSizeOptions(setPageSizeOptionsInput: string) {
        this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
        console.log('oi')
    }


    sortData(sort: Sort) {
        const data = this.elegiveis.slice();
        if (!sort.active || sort.direction == '') {
            this.dataSource.data = data;
            return;
        }

        this.dataSource.data = data.sort((a, b) => {
            let isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'cnpj': return compare(a.cnpj, b.cnpj, isAsc);
                case 'cpf': return compare(+a.cpf, +b.cpf, isAsc);
                case 'nome': return compare(+a.nome, +b.nome, isAsc);
                case 'matricula': return compare(+a.matricula, +b.matricula, isAsc);
                case 'sexo': return compare(+a.sexo, +b.sexo, isAsc);
                default: return 0;
            }
        });
    }

    carregarElegiveis(idCampanha: any) {

        this.campanha = this.campanhaService.getCampanhaPorId(idCampanha)

        this.elegiveis = []
        this.dataSource.data = this.elegiveis;
        //getElegiveisList(idCampanha: string, cpf?: string, pageSize?: number, page?: number, regs?: number)
        this.elegivelService.getElegiveisList(idCampanha)
            .takeWhile(response => response.length > 0 )
            .subscribe(response => {
                this.elegiveis = response;
                if (this.elegiveis) {
                    this.dataSource.data = this.elegiveis.slice();
                    this.displayedColumns = [
                        'cnpj',
                        'cpf',
                        'matricula',
                        'nome',
                        'dependente',
                        'cpfresp',
                        'dtnasc',
                        'sexo',
                        'desc_produto',
                        'num_lote',
                        'data',
                        'enfermeiro'
                    ]
                }
            },
            response => this.notificationService.notifiy({ message: `Dados não encontrados`, tipo: 'E' }));
    }

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
    }

    rowClicked(row: any): void {
        console.log(row);
    }

    retornaZebra(i): string {
        let cor = ''
        if (i < 2) {
            if (i === 0) {
                cor = '#ECF0F6'
            } else {
                cor = 'white'
            }
        } else {
            if (i % 2 === 0) {
                cor = '#ECF0F6'
            } else {
                cor = 'white'
            }
        }
        return cor
    }
}

function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
