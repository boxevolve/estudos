import { Component, OnInit, Input } from '@angular/core'
import { ElegivelService } from '../../../modulos/features/elegiveis/elegivel.service'
import { ElegivelModel, ElegivelListModel } from '../../../modulos/features/elegiveis/elegivel.model'
import { NotificationService } from '../../../modulos/servicos/notification.service'
import { CampanhaService } from '../../../modulos/features/campanhas/campanha.service'
import { CampanhaModel } from '../../../modulos/features/campanhas/campanha.model'
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms'
import { RadioOption } from '../../../modulos/shared/option/radio-option.model';

@Component({
    selector: 'ic-elegiveis',
    templateUrl: './elegiveis.component.html'
})
export class ElegiveisComponent implements OnInit {

    @Input() elegiveis: ElegivelListModel[]
    @Input() campanha: CampanhaModel
    @Input() cpfBusca: string = ''

    selectOptions: RadioOption[] = [
        {label: 'Somente Imunizados', value: 'IMU'},
        {label: 'Todos', value: 'ALL'}
      ]

    public cpfPattern =  /^([0-9]){3}\.([0-9]){3}\.([0-9]){3}\-([0-9]){2}$/
    searchForm: FormGroup
    searchControl: FormControl

    constructor(private elegivelService: ElegivelService,
        private campanhaService: CampanhaService,
        private notificationService: NotificationService,
        private formBuilder: FormBuilder) { }

    ngOnInit() {

        this.searchControl = this.formBuilder.control([Validators.pattern(this.cpfPattern),
                                                        Validators.maxLength(15),
                                                        Validators.minLength(12)])

        this.searchForm = this.formBuilder.group({
            searchControl: this.searchControl,
            selOption: this.selectOptions
        })

        

        this.searchControl.valueChanges.subscribe(cpf => this.cpfBusca = this.formatarCPF(cpf))
    }

    carregarElegiveis(idCampanha: any) {

        this.campanha = this.campanhaService.getCampanhaPorId(idCampanha)

        this.elegivelService.getElegiveisList(idCampanha)
            .subscribe(response => this.elegiveis = response,
                response => this.notificationService.notifiy({message: `Dados não encontrados`, tipo: 'E'}))
    }

    selecionarElegiveis() {

        const cpf = this.desformatarCPF(this.searchForm.value.searchControl)

        this.elegivelService.getElegiveisList(this.campanha._id, cpf)
            .subscribe(response => this.elegiveis = response,
                response => this.notificationService.notifiy({message: `Dados não encontrados`, tipo: 'E'}))
    }

    filtrarSelecao(option: string) {

    }

    ordenar() {
        console.log(`OI`)
    }

    retornaZebra(i): string {
        let cor = ''
        if (i < 2) {
            if (i === 0) {
                cor = '#ECF0F6'
            } else {
                cor = 'white'
            }
        } else {
            if (i % 2 === 0) {
                cor = '#ECF0F6'
            } else {
                cor = 'white'
            }
        }
        return cor
    }

    formatarCPF(cpf: string | ''): string {

        return this.elegivelService.formatarCPF(cpf)
        /*
        if (cpf.length > 0) {
            cpf = cpf.replace(/\D/g, '')
            cpf = cpf.replace(/^(\d{3})?(\d{3})?(\d{3})?(\d{2})$/, '$1.$2.$3-$4')
        }
        return cpf || ''
        */
    }

    desformatarCPF(cpf: string | ''): string {

        return this.elegivelService.desformatarCPF(cpf)
        /*
        cpf = cpf.replace(/\D/g, '')
        console.log(cpf)
        return cpf
        */
    }

}
