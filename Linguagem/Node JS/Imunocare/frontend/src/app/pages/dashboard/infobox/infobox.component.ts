



import { Component, OnInit, Input } from '@angular/core';
import { InfoBox } from '../../../modulos/shared/modelos/infobox.model';
import { CampanhaModel, KpiBoxModel } from '../../../modulos/features/campanhas/campanha.model';
import { DashBoardService } from '../../../modulos/features/dashboard/dashboard.service';

@Component({
    selector: 'ic-infobox',
    templateUrl: './infobox.component.html'
})
export class InfoboxComponent implements OnInit {

    infobox: InfoBox = { corBox: '', iconBox: '' }

    @Input() kpi: KpiBoxModel
    @Input() campanha: CampanhaModel
    @Input() indx: 0

    constructor(private dash: DashBoardService) { }

    ngOnInit() {
        this.infobox = this.dash.setBindings(this.indx)
    }
}
