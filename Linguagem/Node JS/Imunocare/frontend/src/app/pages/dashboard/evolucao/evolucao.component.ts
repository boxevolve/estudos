import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CampanhaModel } from '../../../modulos/features/campanhas/campanha.model';
import { DashBoardService } from '../../../modulos/features/dashboard/dashboard.service';
import { InfoBox } from '../../../modulos/shared/modelos/infobox.model';
import { CampanhaService } from '../../../modulos/features/campanhas/campanha.service';
import { ElegivelService } from '../../../modulos/features/elegiveis/elegivel.service';

@Component({
    selector: 'ic-evolucao',
    templateUrl: './evolucao.component.html'
})

export class EvolucaoComponent implements OnInit {

    @Input() campanha: CampanhaModel
    @Input() indx: 0

    @Output() seleciona = new EventEmitter()
    @Output() elegiveis = new EventEmitter()
    @Output() cnpjTotal = new EventEmitter()

    infobox: InfoBox = {corBox: '', iconBox: ''}

    constructor(private dash: DashBoardService,
                private campanhaService: CampanhaService,
                private elegivelService: ElegivelService) { }

    ngOnInit() {
        this.infobox = this.dash.setBindings(this.indx)
    }

    percentualAtingido(d: number, n: number): number {
        let res = ( d / n ) * 100
        res = Math.round(res)
        return res
    }

    emitSelecionaCampanha() {
        // console.log(`Campanha: ${JSON.stringify(this.campanha)}`)
            
        this.seleciona.emit(this.campanha)
        this.elegiveis.emit(this.campanha._id)
        this.cnpjTotal.emit(this.campanha._id)
    }

}
