import { Component, OnInit, Input } from '@angular/core';
// import { SalesChart } from './grafico.model'
import { Chart } from 'chart.js'
import { CampanhaService } from '../../../modulos/features/campanhas/campanha.service';
import { CampanhaModel } from '../../../modulos/features/campanhas/campanha.model';
import { DashBoardService } from '../../../modulos/features/dashboard/dashboard.service';
import { InfoBox } from '../../../modulos/shared/modelos/infobox.model';

@Component({
  selector: 'ic-grafico',
  templateUrl: './grafico.component.html'
})
export class GraficoComponent implements OnInit {

    @Input() chartCanvas
    @Input() campanhas: CampanhaModel[]
    @Input() campanha: CampanhaModel
    @Input() grafico = Chart;
    @Input() indx: 0
    infobox: InfoBox = {corBox: '', iconBox: ''}

    constructor(private campanhaService: CampanhaService, private dash: DashBoardService) {}

    ngOnInit() {
        this.infobox = this.dash.setBindings(this.indx)
        this.campanha = this.campanhaService.detalheCampanha
    }

    exibirDetalhesDaCampanha(camp: CampanhaModel) {
        this.campanha = camp
    }
}

