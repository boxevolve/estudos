import { Component, OnInit, Input, Output } from '@angular/core';
import { CampanhaModel, CnpjTotalModel } from '../../../modulos/features/campanhas/campanha.model';
import { EventEmitter } from 'events';
import { InfoBox } from '../../../modulos/shared/modelos/infobox.model';
import { ElegivelService } from '../../../modulos/features/elegiveis/elegivel.service';
import { DashBoardService } from '../../../modulos/features/dashboard/dashboard.service';
import { CampanhaService } from '../../../modulos/features/campanhas/campanha.service';
import { NotificationService } from '../../../modulos/servicos/notification.service';

@Component({
    selector: 'ic-campanha-cnpj',
    templateUrl: './campanha-cnpj.component.html'
})
export class CampanhaCnpjComponent implements OnInit {

    @Input() campanha: CampanhaModel
    @Input() indx: 0

    @Input() cnpjTotais: CnpjTotalModel[]

    @Output() seleciona = new EventEmitter()
    @Output() elegiveis = new EventEmitter()

    infobox: InfoBox = { corBox: '', iconBox: '' }
    infoboxAll = []

    constructor(private dash: DashBoardService,
        private campanhaService: CampanhaService,
        private notificationService: NotificationService,
        private elegivelService: ElegivelService) { }

    ngOnInit() {
        // this.infobox = this.dash.setBindings(this.indx)

    }

    carregarTotaisPorCNPJ(idCampanha) {
        this.campanha = this.campanhaService.getCampanhaPorId(idCampanha)
        this.cnpjTotais = this.campanha.cnpj_total
        if (this.cnpjTotais) {
            for (let i = 0; i < this.cnpjTotais.length; i++) {
                this.infobox = this.dash.setBindings(i)
                this.infoboxAll.push(this.infobox)

            }
        }
    }

    percentualAtingido(d: number, n: number, i: number): number {
        let res = (d / n) * 100
        res = Math.round(res)
        this.infobox = this.infoboxAll[i]
        return res
    }

    getInfoBox(i: number): string {
        return this.infoboxAll[i].corBox;
    }

    formatarCNPJ(cnpj: string | ''): string {
        if (cnpj.length > 0) {
            cnpj = cnpj.replace(/\D/g, '')
            cnpj = cnpj.replace(/^(\d{2})?(\d{3})?(\d{3})?(\d{4})?(\d{2})$/, '$1.$2.$3/$4-$5')
        }
        return cnpj || ''
    }

    desformatarCNPJ(cnpj: string | ''): string {
        cnpj = cnpj.replace(/\D/g, '')
        return cnpj || 'BRANCO'
    }
}
