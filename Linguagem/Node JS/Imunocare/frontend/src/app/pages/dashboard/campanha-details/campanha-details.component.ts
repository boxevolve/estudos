import { Component, OnInit, Input } from '@angular/core';
import { CampanhaService } from '../../../modulos/features/campanhas/campanha.service';
import { CampanhaModel } from '../../../modulos/features/campanhas/campanha.model';

@Component({
    selector: 'ic-campanha-details',
    templateUrl: './campanha-details.component.html'
})
export class CampanhaDetailsComponent implements OnInit {

    @Input() campanha: CampanhaModel

    @Input() elegiveis_cp: number = 0
    @Input() elegiveis_dp: number = 0
    @Input() elegiveis_fp: number = 0
    @Input() elegiveis_mp: number = 0

    constructor() { }

    ngOnInit() {
    }

    exibirDetalhes(campanha: any) {
        this.campanha = campanha

        if (campanha.produtos) {
            for (const produto of campanha.produtos) {
                produto.elegiveis_cp = this.percentualAtingido(produto.elegiveis_c, produto.produto_aplicacoes)
                produto.elegiveis_dp = this.percentualAtingido(produto.elegiveis_d, produto.produto_aplicacoes)
                produto.elegiveis_fp = this.percentualAtingido(produto.elegiveis_f, produto.produto_aplicacoes)
                produto.elegiveis_mp = this.percentualAtingido(produto.elegiveis_m, produto.produto_aplicacoes)
                produto.duplicidade = produto.produto_aplicacoes - (produto.elegiveis_f + produto.elegiveis_m)
                produto.duplicidade_dp = this.percentualAtingido(produto.duplicidade, produto.produto_aplicacoes)
            }
        }
    }

    percentualAtingido(d: number, n: number): number {
        if ((d === undefined || d === 0) || (n === undefined || n === 0)) {
            return 0
        }
        let res = (d / n) * 100
        res = Math.round(res)
        return res
    }

    resetValorUndefined(valor: number) {
        if (valor === undefined) {
            valor = 0
        }
    }
}
