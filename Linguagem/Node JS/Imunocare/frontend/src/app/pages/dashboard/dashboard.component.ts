import { Component, OnInit, Input } from '@angular/core';
import { NotificationService } from '../../modulos/servicos/notification.service';
import { LoginService } from '../../modulos/servicos/login.service';
import { CampanhaService } from '../../modulos/features/campanhas/campanha.service';
import { CampanhaModel, KpiBoxModel } from '../../modulos/features/campanhas/campanha.model';
import { Router } from '@angular/router';
import { getLocaleNumberFormat } from '@angular/common'

@Component({
  selector: 'ic-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

    @Input() kpis: KpiBoxModel[]
    @Input() campanhas: CampanhaModel[]
    @Input() campanhaTotal: CampanhaModel

    constructor(private campanhaService: CampanhaService,
                private loginService: LoginService,
                private notificationService: NotificationService,
                private router: Router) { }

    ngOnInit() {
        this.getCampanhas()
        this.kpis = []
        this.setKpisCalculados()
    }

    getCampanhas() {

        this.campanhaService.getCampanhas()
                .subscribe(response => this.campanhas = response,
                            response => this.notificationService.notifiy({ message: response.mensagem, tipo: 'E'}))
    }

    setKpisCalculados() {
        if ( this.campanhaService.getKpisCalculados() !== undefined ) {
            this.kpis = this.campanhaService.getKpisCalculados()
        }
    }

    getKpisCalculados(): KpiBoxModel[] {
        if (this.kpis === undefined) {
            this.setKpisCalculados()
        }
        return this.kpis
    }

    numCampanhas(): number {
        return this.campanhaService.numCampanhas
    }

    totalQtde(): number {
        return this.campanhaService.totalQtde
    }

    totalDeAplicacoes(): number {
        return this.campanhaService.totalDeAplicacoes
    }

    valor_faturado(): number {
        return this.campanhaService.valor_faturado
    }
}
