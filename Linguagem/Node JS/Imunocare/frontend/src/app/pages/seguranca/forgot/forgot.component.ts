import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../../../modulos/servicos/login.service';
import { NotificationService } from '../../../modulos/servicos/notification.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'ic-forgot',
    templateUrl: './forgot.component.html',
    styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {

    fpForm: FormGroup
    navigateTo: string

    emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i


    constructor(private formBuilder: FormBuilder,
        private loginService: LoginService,
        private notificationService: NotificationService,
        private activatedRoute: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.fpForm = this.formBuilder.group({
            email: this.formBuilder.control('', [Validators.required, Validators.pattern(this.emailPattern)])
        })
    }

    forgot() {
        this.loginService.forgot(this.fpForm.value.email)
            .subscribe(response => {
                this.notificationService.notifiy({ message: response.mensagem })
            },
            response => this.notificationService.notifiy({ message: response.error.mensagem, tipo: 'E'}))
    }
}
