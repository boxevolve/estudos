
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms'
import { LoginService } from '../../../modulos/servicos/login.service';
import { NotificationService } from '../../../modulos/servicos/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
// import { LoginService } from '../../../shared/servicos/login.service';
// import { Router } from '@angular/router';


@Component({
  selector: 'ic-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  cpForm: FormGroup
  navigateTo: string

  emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

  constructor(private formBuilder: FormBuilder,
    private loginService: LoginService,
    private notificationService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.cpForm = this.formBuilder.group({
      email: this.formBuilder.control('', [Validators.required, Validators.pattern(this.emailPattern)]),
      senha: this.formBuilder.control('', [Validators.required]),
      novasenha: this.formBuilder.control('', [Validators.required]),
      confsenha: this.formBuilder.control('', [Validators.required]),
    }, { validator: ChangePasswordComponent.equalsTo })

    this.navigateTo = this.activatedRoute.snapshot.params['to'] || btoa('/')
  }

  // tslint:disable-next-line:member-ordering
  static equalsTo(group: AbstractControl): { [key: string]: boolean } {
    const novasenha = group.get('novasenha')
    const confsenha = group.get('confsenha')
    if (!novasenha || !confsenha) {
      return undefined
    }

    if (novasenha.value !== confsenha.value) {
      return { passNotMatch: true }
    }
    return undefined
  }

  alterarSenha() {
    this.loginService.alterarSenha(this.cpForm.value.email, this.cpForm.value.senha, this.cpForm.value.novasenha)
      .subscribe(response => {
        this.notificationService.notifiy({ message: 'Senha alterada com sucesso' })
        this.router.navigate(['/login'])
      },
        response => this.notificationService.notifiy({ message: response.error.mensagem, tipo: 'E' }))
  }
}
