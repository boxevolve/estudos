
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms'
import { LoginService } from '../../../modulos/servicos/login.service';
import { NotificationService } from '../../../modulos/servicos/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
// import { LoginService } from '../../../shared/servicos/login.service';
// import { Router } from '@angular/router';


@Component({
    selector: 'ic-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    loginForm: FormGroup
    navigateTo: string

    emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

    constructor(private formBuilder: FormBuilder,
        private loginService: LoginService,
        private notificationService: NotificationService,
        private activatedRoute: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: this.formBuilder.control('', [Validators.required, Validators.pattern(this.emailPattern)]),
            senha: this.formBuilder.control('', [Validators.required])
        })
        this.navigateTo = this.activatedRoute.snapshot.params['to'] || btoa('/')
    }

    login() {
        this.loginService.loginUsuario(this.loginForm.value.email, this.loginForm.value.senha)
            .subscribe(usuario => {
                this.notificationService.notifiy({ message: `Bem vindo(a), ${this.loginService.getUsuario().nome}` })
                
                if (this.loginService.getUsuario().primeiro_acesso) {
                    this.router.navigate(['/alterarSenha'])
                } else {
                
                    if (this.loginService.getUsuario().perfil !== 'ENFE') {
                        this.router.navigate(['/dash'])
                    } else {
                        this.router.navigate(['/imunizacoes'])
                    }
                }
            },
            response => this.notificationService.notifiy({ message: 'Erro ao se autenticar', tipo: 'E' }))
    }


}
