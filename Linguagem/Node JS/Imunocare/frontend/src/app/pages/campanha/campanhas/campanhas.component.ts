import { Component, OnInit, Input } from '@angular/core'
import { CampanhaService } from '../../../modulos/features/campanhas/campanha.service'
import { CampanhaModel } from '../../../modulos/features/campanhas/campanha.model';
import { NotificationService } from '../../../modulos/servicos/notification.service';

@Component({
    selector: 'ic-campanhas',
    templateUrl: './campanhas.component.html'
})
export class CampanhasComponent implements OnInit {

    @Input() campanhas: CampanhaModel[] = []

    constructor(private campanhaService: CampanhaService,
        private notificationService: NotificationService) { }

    ngOnInit() {
        this.getCampanhas()
    }

    getCampanhas() {

        this.campanhaService.getCampanhas()
            .subscribe(response => this.campanhas = response,
                response => this.notificationService.notifiy({message: response.mensagem, tipo: 'E'}))
    }
}
