import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CampanhaModel } from '../../../modulos/features/campanhas/campanha.model';
import { CampanhaService } from '../../../modulos/features/campanhas/campanha.service';
import { GeradorService } from '../../../modulos/features/gerador/gerador.service';
import { NotificationService } from '../../../modulos/servicos/notification.service';

@Component({
    selector: 'ic-campanha-menu',
    templateUrl: './campanha-menu.component.html'
})
export class CampanhaMenuComponent implements OnInit {

    @Input() campanhas: CampanhaModel[]
    @Input() campanha: CampanhaModel
    @Input() gerador?: boolean = false
    @Output() emitCampanhaDetalhe = new EventEmitter()


    constructor(private campanhaService: CampanhaService, 
                private geradorService: GeradorService,
                private ns: NotificationService) { }

    ngOnInit() {
    }

    retornaLabel(status: string): string {

        let labelColor = ''

        switch (status) {
            case 'Nova':
                labelColor = 'primary'
                break;
            case 'Andamento':
                labelColor = 'warning'
                break;
            case 'Encerrada':
                labelColor = 'success'
                break;
        }

        return labelColor

    }

    exibirDetalhesDaCampanha(camp: CampanhaModel) {

        if(this.gerador) {
            this.buscarDadosCampanha(camp)
        } else {
            this.campanhaService.readonly = true;
            this.campanhaService.detalharCampanha(camp)
            this.campanha = camp
            this.emitCampanhaDetalhe.emit();
    
        }

    }

    buscarDadosCampanha(camp: CampanhaModel) {
        //this.campanhaService.readonly = true;
        this.campanhaService.findCampanhaDB(camp._id)

            .subscribe(response => {
                this.campanha = response.retorno;
                this.geradorService.campanha = this.campanha;
                console.log(`Campanha recuperada: ${this.geradorService.campanha}`)
                this.geradorService.estagio = 1;
                this.geradorService.desbloqueado = true;                
                this.emitCampanhaDetalhe.emit();        
            },
                response => {
                    this.ns.notifiy({message: `Erro ao recuperar dados da Campanha ${camp.campanha}`, tipo: 'E'})
                }
            )
            
    }

}
