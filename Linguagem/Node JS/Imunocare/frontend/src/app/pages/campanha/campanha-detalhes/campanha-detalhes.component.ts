import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'
import { CampanhaModel, LoteProdutoListModel, StatusCampanhaModel } from '../../../modulos/features/campanhas/campanha.model'
import { CampanhaService } from '../../../modulos/features/campanhas/campanha.service'
import { ActivatedRoute, Route, Router, ActivatedRouteSnapshot } from '@angular/router';

@Component({
  selector: 'ic-campanha-detalhes',
  templateUrl: './campanha-detalhes.component.html'
})
export class CampanhaDetalhesComponent implements OnInit {

  campForm: FormGroup
  @Input() campanha: CampanhaModel
  @Input() loteProdList: LoteProdutoListModel[] = []
  @Input() status: string = ''
  @Input() listStatusCampanhas: StatusCampanhaModel[] = [{ status: 'Nova' }, { status: 'Andamento' }, { status: 'Encerrada' }]

  readonly: boolean = false

  constructor(private campanhaService: CampanhaService,
              private formBuilder: FormBuilder) {
              }

  ngOnInit() {

    this.campanha = this.campanhaService.setNew()

    this.campForm = this.formBuilder.group({
      campanha: this.formBuilder.control(''),
      cliente: this.formBuilder.control(''),
      cep: this.formBuilder.control(''),
      cnpj: this.formBuilder.control(''),
      status: this.formBuilder.control(''),
      unidade: this.formBuilder.control(''),
      endereco: this.formBuilder.control(''),
      bairro: this.formBuilder.control(''),
      cidade: this.formBuilder.control(''),
      estado: this.formBuilder.control(''),
      produto: this.formBuilder.control(''),
      lote: this.formBuilder.control(''),
      qtde: this.formBuilder.control(''),
      responsavel_cliente: this.formBuilder.control(''),
      email_cliente: this.formBuilder.control(''),
      fone_cliente: this.formBuilder.control(''),
      data_inicio: this.formBuilder.control(''),
      data_fim: this.formBuilder.control(''),
      horario_funcionamento_ini: Date,
      horario_funcionamento_fim: Date,
      distribuidor: this.formBuilder.control(''),
      responsavel: this.formBuilder.control(''),
      email: this.formBuilder.control(''),
      fone: this.formBuilder.control(''),
      nf: this.formBuilder.control(''),
      in_company: this.formBuilder.control('')
    })

  }

  formatarCNPJ(cnpj: string | ''): string {
    if (cnpj.length > 0) {
      cnpj = cnpj.replace(/\D/g, '')
      cnpj = cnpj.replace(/^(\d{2})?(\d{3})?(\d{3})?(\d{4})?(\d{2})$/, '$1.$2.$3/$4-$5')
    }
    return cnpj || ''
  }

  setCampanha() {
    this.readonly = this.campanhaService.readonly
    this.campanha = this.campanhaService.detalheCampanha
    this.campanha.cliente.cnpj = this.formatarCNPJ(this.campanha.cliente.cnpj)
    this.loteProdList = this.campanhaService.detalheLoteProdutos
  }

  emitSelecionaStatusCampanha(event: any) {
    this.campanha.status = event.target.value
  }

  isReadOnly(): boolean {
    this.readonly = this.campanhaService.readonly
    console.log(`READONLY: ${this.readonly}`)
    return this.readonly
  }


}
