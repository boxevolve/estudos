import { Component, OnInit, Input } from '@angular/core'
import { LoginService } from '../../modulos/servicos/login.service';
import { Usuario } from '../../modulos/shared/modelos/profile.model';
import { BaseComponent } from '../../utils/base.component';


@Component({
  selector: 'ic-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent extends BaseComponent implements OnInit {

  @Input() user: Usuario;

  statusMenuAdministracao = false;

  statusMenuSeguranca = 'treeview';
  statusSubMenuSeguranca = false;

  statusMenuSegurancaCadastros = 'treeview';
  statusSubMenuSegurancaCadastros = false;

  classMenuImunizacao = 'treeview';
  statusSubMenuVacinacao = false;
  statusSubMenuForaElegi = false;

  constructor(
    private loginService: LoginService
  ) {
    super();
  }

  ngOnInit() {
    this.user = this.loginService.getUsuario();
    this.validate();
  }

  public validate() {
    this.menuAdminstracao();
  }

  public menuAdminstracao(): void {
    this.statusMenuAdministracao = this.loginService.isAdmin() || this.loginService.isManager();
  }

  public menuSeguranca(): void {
    this.setConfig('statusMenuSeguranca', 
                  [this.getVariable('statusSubMenuSeguranca', (this.loginService.isAdmin() || this.loginService.isManager()))]);
  }

  public menuSegurancaImunizacao(): void {
    this.setConfig('classMenuImunizacao',
                  [
                    this.getVariable('statusSubMenuVacinacao'),
                    this.getVariable('statusSubMenuForaElegi', (this.loginService.isAdmin() || this.loginService.isManager()))
                  ]);
  }

  public menuSegurancaCadastros(): void {
    this.setConfig('statusMenuSegurancaCadastros', ['statusSubMenuSegurancaCadastros']);
  }

  /**
   * Métodos genéricos
   */
  public setConfig(variableClass: string, variables: any[]): void {
    const isTreeView = this.isTreeView(variableClass);
    this.setClass(variableClass, isTreeView);
    this.setStatus(variables, isTreeView);
  }

  public setClass(variableClass: string, isTreeView: boolean): void {
    this[variableClass] = isTreeView ? 'treeview menu-open' : 'treeview';
  }

  public setStatus(variables: any[], isTreeView: boolean): void {
    variables.forEach((variable) => {
      if (this.typeString(variable)) {
        this[variable] = isTreeView;
      } else {
        this[variable.variable] = isTreeView
                                  ? (variable.status !== undefined && variable.status !== null ? variable.status : true)
                                  : false;
      }
    });
  }

  public isTreeView(variableClass: string): boolean {
    return this.equals(this[variableClass], 'treeview');
  }

  public getVariable(variable: string, status?: boolean): any {
    return {
      variable: variable,
      status: status
    };
  }

}
