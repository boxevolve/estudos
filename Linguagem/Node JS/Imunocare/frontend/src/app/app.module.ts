import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { registerLocaleData } from '@angular/common'
import locale from '@angular/common/locales/br';

// the second parameter 'fr' is optional
registerLocaleData(locale, 'br');

import { ROUTES } from './app.routes'
import { RouterModule, PreloadAllModules } from '@angular/router'

import { AppComponent } from './app.component'
import { HeaderComponent } from './pages/header/header.component'
import { SidebarComponent } from './pages/sidebar/sidebar.component'
import { LoginComponent } from './pages/seguranca/login/login.component'
import { MainComponent } from './pages/main/main.component'
import { SharedModule } from './modulos/shared/shared.module'
import { Error404Component } from './pages/error404/error404.component'
import { ChangePasswordComponent } from './pages/seguranca/change-password/change-password.component'
import { ForgotComponent } from './pages/seguranca/forgot/forgot.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    LoginComponent,
    MainComponent,
    Error404Component,
    ChangePasswordComponent,
    ForgotComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    SharedModule.forRoot(),
    RouterModule.forRoot(ROUTES, {preloadingStrategy: PreloadAllModules})
  ],
  providers: [], // [{provide: LOCALE_ID, useValue: 'br'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
