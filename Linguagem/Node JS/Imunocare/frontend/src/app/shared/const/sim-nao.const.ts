import { Util } from '../../utils/util';

export class SimNao {

  static SIM = new SimNao('SIM', 'Sim', 'sim', true);
  static NAO = new SimNao('NAO', 'Não', 'nao', false);

  private constructor(
    private _key?: string,
    private _text?: string,
    private _value?: any,
    private _bool?: boolean
  ) {}

  public static getByValue(value: string): SimNao {
    value = Util.removeAccentuation(value);
    return value === SimNao.SIM._value ? SimNao.SIM : (value === SimNao.NAO._value ? SimNao.NAO : null);
  }

  public static getByText(value: string): SimNao {
    return value === SimNao.SIM._text ? SimNao.SIM : (value === SimNao.NAO._text ? SimNao.NAO : null);
  }

  public static getTextByValue(value: string): string {
    const simNao = this.getByValue(value);
    return simNao ? simNao.text : '';
  }

  public static getValueByText(value: string): any {
    const simNao = this.getByText(value);
    return simNao ? simNao.value : null;
  }

  public static getTextByBool(value: boolean): string {
    const simNao = this.getByValue(value ? SimNao.SIM.value : SimNao.NAO.value);
    return simNao ? simNao.text : '';
  }

  public static list(): SimNao[] {
    return [SimNao.SIM, SimNao.NAO];
  }

  get text(): string {
    return this._text;
  }

  get value(): string {
    return this._value;
  }

  get bool(): boolean {
    return this._bool;
  }

  toString(): string {
    return this._key;
  }

}
