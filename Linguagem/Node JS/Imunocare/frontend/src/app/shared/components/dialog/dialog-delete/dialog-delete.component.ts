import { Component } from '@angular/core';
import { BaseComponent } from '../../../../utils/base.component';
import { Constants } from '../../../../utils/constants';

@Component({
  selector: 'ic-dialog-delete',
  templateUrl: 'dialog-delete.component.html',
  styleUrls: ['./dialog-delete.component.css']
})
export class DialogDeleteComponent extends BaseComponent {

  title = Constants.MSG_SPE_DIALOG_DELETE_TITLE;
  text = Constants.MSG_SPE_DIALOG_DELETE_TEXT;
  cancelLabel = Constants.MSG_SPE_DIALOG_DELETE_BTN_CANCEL;
  confirmLabel = Constants.MSG_SPE_DIALOG_DELETE_BTN_CONFIRM;

}
