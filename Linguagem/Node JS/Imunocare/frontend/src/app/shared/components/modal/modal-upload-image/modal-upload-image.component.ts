import { Component, Output, AfterViewInit, Input, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { BaseComponent } from '../../../../utils/base.component';
import ModalUtil from '../../../../utils/modal';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { Util } from '../../../../utils/util';
import { NotificationService } from '../../../../modulos/servicos/notification.service';
import { Constants } from '../../../../utils/constants';

@Component({
  selector: 'ic-modal-upload-image',
  templateUrl: './modal-upload-image.component.html',
  styleUrls: ['./modal-upload-image.component.css']
})
export class ModalUploadImageComponent extends BaseComponent implements AfterViewInit {

  modalId = 'upload_image';

  @Input() idBase = 'void';
  @Input() title: string;
  @Input() minWidth = 0;
  @Input() imageChangedEvent: any;

  @Output() upload = new EventEmitter();
  @Output() emitIdModal = new EventEmitter();
  @Output() hidden = new EventEmitter();

  croppedImage: any = '';
  image: ImageCroppedEvent;
  // 1MB = 1,048,576Bytes
  maxMB = 40;
  maxSize = 1048576 * this.maxMB;

  constructor(
    private cdr: ChangeDetectorRef,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngAfterViewInit() {
    this.emitIdModal.emit(this.getClone(this.getIdModal([])));
    ModalUtil.hiddenModal(this.getIdModal([]), () => {
      this.hidden.emit(Util.isEmpty(this.croppedImage));
      this.clear();
    });
    this.cdr.detectChanges();
  }

  public hide(event?: any): void {
    ModalUtil.hideModal(this.getIdModal([]));
  }

  public clear(): void {
    this.imageChangedEvent = null;
    this.croppedImage = null;
  }

  public save(event?: any): void {
    if (this.validateSize()) {
      this.upload.emit(this.getClone(this.croppedImage));
      this.hide();
    } else {
      this.notificationService.notifiy({
        message: Constants.MSG_SPE_FILE_MAX_SIZE + this.maxMB,
        tipo: 'E',
      });
    }
  }

  public getIdModal(fields: string[]): string {
    return ModalUtil.idModal(this.modalId, this.idBase, fields);
  }

  public getStyle(): any {
    return {'min-width' : `${ this.minWidth }% !important`};
  }

  public imageCropped(event: ImageCroppedEvent) {
    this.image = event;
    this.croppedImage = event.base64;
  }

  public imageLoaded() {
      // show cropper
  }

  public loadImageFailed() {
      // show message
  }

  public validateSize(): boolean {
    return this.image ? this.image.file.size < this.maxSize : false;
  }

}
