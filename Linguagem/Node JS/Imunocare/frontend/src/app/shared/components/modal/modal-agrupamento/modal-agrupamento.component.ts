import { Component, Output, AfterViewInit, Input, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { BaseComponent } from '../../../../utils/base.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import ModalUtil from '../../../../utils/modal';
import { AgrupamentoService } from '../../../../components/cadastro/campanha/service/agrupamento.service';

@Component({
  selector: 'ic-modal-agrupamento',
  templateUrl: './modal-agrupamento.component.html',
  styleUrls: ['./modal-agrupamento.component.css']
})
export class ModalAgrupamentoComponent extends BaseComponent implements AfterViewInit {

  modalId = 'agrupamento';

  @Input() idBase = 'void';
  @Input() title: string;
  @Input() minWidth = 0;

  @Output() hiddenModal = new EventEmitter();
  @Output() emitIdModal = new EventEmitter();

  nameDescricao = 'descricao';

  form: FormGroup;

  saveOrCancel = false;

  constructor(
    private cdr: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private agrupamentoService: AgrupamentoService
  ) {
    super();
    this.createFormGroup();
  }

  public createFormGroup(): void {
    this.form = this.formBuilder.group({});
  }

  ngAfterViewInit() {
    this.emitIdModal.emit(this.getClone(this.getIdModal([])));
    ModalUtil.hiddenModal(this.getIdModal([]), () => {
      this.hiddenModal.emit(this.getClone(this.saveOrCancel));
      this.clear();
    });
    this.cdr.detectChanges();
  }

  hide(event?: any): void {
    ModalUtil.hideModal(this.getIdModal([]));
  }

  clear(): void {
    this.form.reset();
    this.saveOrCancel = false;
  }

  save(event?: any): void {
    this.validateForm(this.form, () => {
      this.agrupamentoService.save(this.getCloneForm(this.form), (result) => {
        this.saveOrCancel = true;
        this.hide();
      }, true);
    });
  }

  getIdModal(fields: string[]): string {
    return ModalUtil.idModal(this.modalId, this.idBase, fields);
  }

  getStyle(): any {
    return {'min-width' : `${ this.minWidth }% !important`};
  }

}
