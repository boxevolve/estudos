import { Input, SimpleChanges, OnChanges, ChangeDetectorRef, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';

@Component({
  selector: 'ic-input-radio',
  templateUrl: './input-radio.component.html',
  styleUrls: ['./input-radio.component.css']
})
export class InputRadioComponent extends BaseComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() checked: any = null;
  @Input() options: any[] = [];
  @Input() value: string;
  @Input() text: string;
  @Input() required = false;
  @Input() label = '';
  @Input() clazz: string;

  @Output() eventChange = new EventEmitter();

  nameDisabled = 'disabled';
  nameChecked = 'checked';

  oldValue: any;

  constructor(
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngAfterViewInit() {
    this.setValueForm(this.form, this.name, this.checked);
    this.cdr.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameDisabled, (currentValue?: any) => {
      currentValue ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    });
    this.onChangeInput(changes, this.nameChecked, (currentValue?: any) => {
      this.setValueForm(this.form, this.name, currentValue);
    });
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.checked || null, disabled: this.disabled}, Validators.required)
      : new FormControl({value: this.checked || null, disabled: this.disabled});
  }

  public emitOnChange(event: any): void {
    this.eventChange.emit(event);
  }

  public getValue(value: any): any {
    return !this.value ? value : value[this.value];
  }

  public getText(value: any): any {
    return !this.text ? value : value[this.text];
  }

}
