import { Component, Output, AfterViewInit, Input, ChangeDetectorRef, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from '../../../../utils/base.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import ModalUtil from '../../../../utils/modal';
import { Util } from '../../../../utils/util';
import { InputType } from '../../../../shared/const/input-type.const';

@Component({
  selector: 'ic-modal-lote-produto',
  templateUrl: './modal-lote-produto.component.html',
  styleUrls: ['./modal-lote-produto.component.css']
})
export class ModalLoteProdutoComponent extends BaseComponent implements AfterViewInit, OnChanges {

  modalId = 'lote_produto';

  @Input() idBase = 'void';
  @Input() title: string;
  @Input() minWidth = 0;
  @Input() refresh  = false;
  @Input() value: any;

  @Output() submitLote = new EventEmitter();
  @Output() emitIdModal = new EventEmitter();

  nameQuantidade = 'lote_qtde';
  nameLote = 'lote';

  nameRefresh = 'refresh';
  nameValue = 'value';

  form: FormGroup;
  numberType = InputType.NUMBER;

  constructor(
    private cdr: ChangeDetectorRef,
    private formBuilder: FormBuilder
  ) {
    super();
    this.createFormGroup();
  }

  public createFormGroup(): void {
    this.form = this.formBuilder.group({
      _id: [null]
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    const keyChanges = Object.keys(changes);
    if (this.contains(keyChanges, this.nameRefresh)) {
      if (changes[this.nameRefresh].currentValue === true) {
        this.addHidden();
      }
    }
    if (this.contains(keyChanges, this.nameValue)) {
      const currentValue = changes[this.nameValue].currentValue;
      if (!Util.isEmpty(currentValue)) {
        const formValue = this.instantiateForm(this.form, currentValue);
        this.form.setValue(formValue);
      }
    }
  }

  ngAfterViewInit() {
    this.emitIdModal.emit(this.getClone(this.getIdModal([])));
    this.addHidden();
    this.cdr.detectChanges();
  }

  public hide(event?: any): void {
    ModalUtil.hideModal(this.getIdModal([]));
  }

  public addHidden(): void {
    ModalUtil.hiddenModal(this.getIdModal([]), () => {
      this.clear();
    });
  }

  public clear(): void {
    this.form.reset();
  }

  public save(event?: any): void {
    this.validateForm(this.form, () => {
      const valueForm = this.getCloneForm(this.form);
      let newValue = valueForm;
      if (this.value) {
        newValue = this.getClone(this.value);
        newValue.lote = valueForm.lote;
        newValue.valueForm = valueForm.lote_qtde;
      }
      this.submitLote.emit({ old: this.getClone(this.value), new: newValue });
      this.hide();
    });
  }

  public getIdModal(fields: string[]): string {
    return ModalUtil.idModal(this.modalId, this.idBase, fields);
  }

  public getStyle(): any {
    return {'min-width' : `${ this.minWidth }% !important`};
  }

}
