import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';
import { FileUploader, FileUploaderOptions, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { IC_API } from '../../../app.api';

const uri = `${IC_API}/api/file/upload`;

@Component({
    selector: 'ic-import-file',
    templateUrl: './import-file.component.html',
    styleUrls: ['./input-radio.component.css']
})



export class ImportFile extends BaseComponent implements OnInit{
   
    @ViewChild('inputField') inputField: any;
    uploader: FileUploader = new FileUploader({ url: uri });
    hasBaseDropZoneOver: boolean = false;
    hasAnotherDropZoneOver: boolean = false;
    attachmentList: any = [];
    options: FileUploaderOptions = {};
    carregarArquivo: boolean = true;

    ngOnInit(): void {

        
        
    }

    constructor(){
        super();
    }
}
