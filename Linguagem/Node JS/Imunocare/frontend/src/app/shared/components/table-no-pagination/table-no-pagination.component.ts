import { Input, Output, EventEmitter, ChangeDetectorRef, ViewChild, AfterContentInit, OnChanges, SimpleChanges } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';
import { PageEvent, MatTableDataSource, MatSort, MatDialog } from '@angular/material';
import DialogUtil from '../../../utils/dialog-util';
import { DialogDeleteComponent } from '../dialog/dialog-delete/dialog-delete.component';

@Component({
  selector: 'ic-table-no-pagination',
  templateUrl: './table-no-pagination.component.html',
  styleUrls: ['./table-no-pagination.component.css']
})
export class TableNoPaginationComponent extends BaseComponent implements OnInit, AfterContentInit, OnChanges {

  @ViewChild(MatSort) sort: MatSort;

  @Input() public showDelete = false;
  @Input() public showEdit = false;
  @Input() public matSortActive = 'id';
  @Input() public matSortDirection = 'asc';
  @Input() public displayedColumns: string[];
  @Input() public headers: string[];
  @Input() public list: string[] = [];
  @Input() public mappingDataToTable: (data: any[]) => any[];
  @Input() public titleDelete: string;
  @Input() public titleEdit: string;

  @Output() delete = new EventEmitter();
  @Output() edit = new EventEmitter();

  data: any[];
  columns: any[] = [];
  pageEvent: PageEvent = undefined;
  dataSource: MatTableDataSource<any>;

  action: any = {value: 'action', description: 'Ação'};

  nameList = 'list';

  constructor(
    public dialog: MatDialog,
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameList, (currentValue?: any) => {
      this.loadDataSource();
    });
  }

  ngAfterContentInit(): void {
    this.loadDataSource();
    this.cdr.detectChanges();
  }

  ngOnInit(): void {
    this.generateColumns();
  }

  public loadDataSource(): void {
    this.data = [];
    this.data = this.mappingDataToTable ? this.mappingDataToTable(this.list) : [];
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.sort = this.sort;
  }

  public generateColumns(): void {
    for (let index = 0; index < this.displayedColumns.length; index++) {
      const displayedColumn = this.displayedColumns[index];
      const column = {value: displayedColumn, description: ''};
      if (this.headers.length >= index) {
        column.description = this.headers[index];
      }
      this.columns.push(column);
    }
    if (this.showDelete) {
      this.displayedColumns.push(this.action.value);
      this.columns.push(this.action);
    }
  }

  public deleteRow(event: any, row?: any): void {
    DialogUtil.openDialog(this.dialog, DialogDeleteComponent, null, (result: any) => {
      if (result) {
        this.delete.emit(this.getClone(row));
      }
    });
  }

  public editRow(event: any, row?: any): void {
    this.edit.emit(this.getClone(row));
  }

  public getValue(col: any): string {
    return col ? col.value : '';
  }

  public getDescription(col: any): string {
    return col ? col.description : '';
  }

  public isActionDelete(col: any): boolean {
    return (this.getValue(col) === this.action.value) && this.showDelete;
  }

  public isActionEdit(col: any): boolean {
    return (this.getValue(col) === this.action.value) && this.showEdit;
  }

}
