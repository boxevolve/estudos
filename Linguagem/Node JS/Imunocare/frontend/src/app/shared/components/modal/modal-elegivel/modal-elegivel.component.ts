import { Component, Output, AfterViewInit, Input, ChangeDetectorRef, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from '../../../../utils/base.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import ModalUtil from '../../../../utils/modal';
import { Util } from '../../../../utils/util';
import { CampanhaModel, FilialCampanhaModel } from '../../../../modulos/features/campanhas/campanha.model';
import { SimNao } from '../../../../shared/const/sim-nao.const';
import { ElegivelService } from '../../../../modulos/features/elegiveis/elegivel.service';
import { ListCampanhaProdutos } from '../../../../components/imunizacao/vacinacao/vacinacao/model/listCampanhaProd.model';

@Component({
  selector: 'ic-modal-elegivel',
  templateUrl: './modal-elegivel.component.html',
  styleUrls: ['./modal-elegivel.component.css']
})
export class ModalElegivelComponent extends BaseComponent implements AfterViewInit, OnChanges {

  modalId = 'elegivel';

  @Input() idBase = 'void';
  @Input() title: string;
  @Input() minWidth = 0;
  @Input() value: any;
  @Input() campanha: CampanhaModel;
  @Input() produto: ListCampanhaProdutos;
  @Input() pendenteAprovacao: boolean;
  @Input() cpf: string;

  @Output() submit = new EventEmitter();
  @Output() emitIdModal = new EventEmitter();

  nameMatricula = 'matricula';
  nameCpf = 'cpf';
  nameNome = 'nome';
  nameDependente = 'dependente';
  nameCpfresp = 'cpfresp';
  nameDtnasc = 'dtnasc';
  nameSexo = 'sexo';
  nameClassificacao = 'classificacao';
  nameEmail = 'email';
  nameCnpj = 'cnpj';
  nameCliente = 'cliente';
  nameImunizacoes = 'imunizacoes';

  nameValue = 'value';
  nameCampanha = 'campanha';
  nameFormCampanha = 'campanha';
  namePendenteAprovacao = 'pendenteAprovacao';
  nameFormPendenteAprovacao = 'pendente_aprovacao';

  form: FormGroup;
  filiais: FilialCampanhaModel;
  currentDate: Date = new Date();

  constructor(
    private cdr: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private elegivelService: ElegivelService
  ) {
    super();
    this.createFormGroup();
  }

  public createFormGroup(): void {
    this.form = this.formBuilder.group({
      _id: [null],
      campanha: [this.campanha ? this.campanha._id : null, Validators.required],
      pendente_aprovacao: [this.pendenteAprovacao !== undefined ? this.pendenteAprovacao : null, Validators.required],
      imunizacoes: [null, Validators.required]
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameValue, (currentValue?: any) => {
      if (!Util.isEmpty(currentValue)) {
        const formValue = this.instantiateForm(this.form, currentValue);
        this.form.setValue(formValue);
      }
    });
    this.onChangeInput(changes, this.nameCampanha, (currentValue?: any) => {
      this.filiais = currentValue ? currentValue.filiais : [];
      this.setValueForm(this.form, this.nameFormCampanha, currentValue ? currentValue._id : null);
    });
    this.onChangeInput(changes, this.namePendenteAprovacao, (currentValue?: any) => {
      this.setValueForm(this.form, this.nameFormPendenteAprovacao, currentValue);
    });
  }

  ngAfterViewInit() {
    this.emitIdModal.emit(this.getClone(this.getIdModal([])));
    this.addHidden();
    this.cdr.detectChanges();
  }

  public hide(event?: any): void {
    ModalUtil.hideModal(this.getIdModal([]));
  }

  public addHidden(): void {
    ModalUtil.hiddenModal(this.getIdModal([]), () => {
      this.clear();
    });
  }

  public clear(): void {
    this.form.reset();
  }

  public save(event?: any): void {
    this.setValueForm(this.form, this.nameFormCampanha, this.campanha ? this.campanha._id : null);
    this.setValueForm(this.form, this.nameFormPendenteAprovacao, this.pendenteAprovacao);
    this.setValueForm(this.form, this.nameImunizacoes, this.createImunizacoes(this.campanha));

    this.validateForm(this.form, () => {
      const valueForm = this.getCloneForm(this.form);
      if (!valueForm[this.nameDependente]) {
        valueForm[this.nameCpfresp] = valueForm[this.nameCpf];
      }
      this.createImunizacoes(this.campanha);
      this.elegivelService.save(valueForm, (result: any) => {
        this.submit.emit(result.retorno ? (result.retorno ? result.retorno.cpfresp : null) : null);
        this.hide();
      });
    });
  }

  public createImunizacoes(campanha: CampanhaModel): any[] {
    const imunizacoes = [];
    if (campanha) {
      campanha.produtos.forEach(produto => {
        produto.lotes.forEach(lote => {
          const imunizacao = {
            produto: produto._id,
            desc_produto: produto.produto.produto,
            lote: lote._id,
            num_lote: lote.lote,
            aplicacao: false
          }
          imunizacoes.push(imunizacao);
        });
      });
    }
    return imunizacoes;
  }

  public changeDependente(event?: any): void {
    if (!event.value) {
      this.removeValidationForm(this.form, this.nameCpfresp);
    }
    this.setValueForm(this.form, this.nameCpfresp, null);
  }

  public isDependente(): boolean {
    return this.getValueForm(this.form, this.nameDependente);
  }

  public getNameCampanha(): string {
    return this.campanha ? this.campanha.campanha : '';
  }

  public getIdModal(fields: string[]): string {
    return ModalUtil.idModal(this.modalId, this.idBase, fields);
  }

  public getStyle(): any {
    return {'min-width' : `${ this.minWidth }% !important`};
  }

  public getNao(): boolean {
    return SimNao.NAO.bool;
  }

}
