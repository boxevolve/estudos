import { Component, Inject } from '@angular/core';
import { BaseComponent } from '../../../../utils/base.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { InputType } from '../../../../shared/const/input-type.const';

@Component({
  selector: 'ic-input-delete',
  templateUrl: 'dialog-input.component.html',
  styleUrls: ['./dialog-input.component.css']
})
export class DialogInputComponent extends BaseComponent {

  title: string;
  text: string;
  cancelLabel: string;
  confirmLabel: string;
  name: string;
  placeholder: string;
  required = false;
  value: any;
  disabled: boolean;
  type: InputType;

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DialogInputComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    super();
    if (data) {
      this.title = data.title;
      this.text = data.text;
      this.cancelLabel = data.cancelLabel;
      this.confirmLabel = data.confirmLabel;
      this.name = data.name;
      this.placeholder = data.placeholder;
      this.required = data.required;
      this.value = data.value;
      this.disabled = data.disabled;
      this.type = data.type;
    }
    this.createFormGroup();
  }

  public createFormGroup(): void {
    this.form = this.formBuilder.group({});
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.value || null, disabled: this.disabled}, Validators.required)
      : new FormControl({value: this.value || null, disabled: this.disabled});
  }

  getValue(): any {
    return this.getValueForm(this.form, this.name);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
