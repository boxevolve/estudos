import { Component, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'ic-dialog-base',
  templateUrl: 'dialog-base.component.html',
  styleUrls: ['./dialog-base.component.css']
})
export class DialogBaseComponent {

  @Input() title: string;
  @Input() text: string;
  @Input() cancelLabel: string;
  @Input() confirmLabel: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) data
  ) {
    if (data) {
      this.title = data.title;
      this.text = data.text;
      this.cancelLabel = data.cancelLabel;
      this.confirmLabel = data.confirmLabel;
    }
  }

}
