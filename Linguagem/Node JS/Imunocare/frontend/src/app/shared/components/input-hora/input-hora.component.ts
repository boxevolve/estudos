import { Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';
import { NotificationService } from '../../../../app/modulos/servicos/notification.service';
import { Constants } from '../../../../app/utils/constants';

@Component({
  selector: 'ic-input-hora',
  templateUrl: './input-hora.component.html',
  styleUrls: ['./input-hora.component.css']
})
export class InputHoraComponent extends BaseComponent implements OnInit, OnChanges {

  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() value: any = null;
  @Input() required = false;
  @Input() placeholder = '';
  @Input() maxlength = 5;
  @Input() minlength = 0;
  @Input() clazz: string;
  @Input() maxHoraControl: string;
  @Input() minHoraControl: string;

  horaMask = '00:00';

  nameDisabled = 'disabled';
  nameValue = 'value';

  constructor(private notificationService: NotificationService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameDisabled, (currentValue?: any) => {
      currentValue ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    });
    this.onChangeInput(changes, this.nameValue, (currentValue?: any) => {
      this.setValueForm(this.form, this.name, currentValue);
    });
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
    if (this.required) {
      this.minlength = 5;
    }
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.value || null, disabled: this.disabled}, Validators.required)
      : new FormControl({value: this.value || null, disabled: this.disabled});
  }

  public validateFieldHora(event: any, id: string): void {
    const horaFormat = /^((?:[01]\d|2[0-3]):[0-5]\d$)/;
    const value = event.currentTarget.value;
    if (value && !horaFormat.test(value)) {
      $(`#${id}`).val('');
    } else {
      if(this.maxHoraControl) {
        const valueMaxHoraControl = $(`#${this.maxHoraControl}`).val();
        const valueThis = $(`#${id}`).val();
        if(valueMaxHoraControl && valueThis > valueMaxHoraControl) {
          $(`#${id}`).val('');
          this.notificationService.notifiy({
            message: Constants.MSG_ERROR_HORARIO_MAIOR,
            tipo: 'E'
          });
        }
      } else if(this.minHoraControl) {
        const valueMinHoraControl = $(`#${this.minHoraControl}`).val();
        const valueThis = $(`#${id}`).val();
        if(valueMinHoraControl && valueThis < valueMinHoraControl) {
          $(`#${id}`).val('');
          this.notificationService.notifiy({
            message: Constants.MSG_ERROR_HORARIO_MENOR,
            tipo: 'E'
          });
        }
      }
    }
  }

}
