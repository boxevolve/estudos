import { Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';

@Component({
  selector: 'ic-input-tel',
  templateUrl: './input-tel.component.html',
  styleUrls: ['./input-tel.component.css']
})
export class InputTelComponent extends BaseComponent implements OnInit, OnChanges {

  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() value: any = null;
  @Input() required = false;
  @Input() placeholder = '';
  @Input() maxlength = 15;
  @Input() minlength = 0;
  @Input() clazz: string;

  telefoneMask = '(00) 0000-00000';

  nameDisabled = 'disabled';
  nameValue = 'value';

  constructor() {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameDisabled, (currentValue?: any) => {
      currentValue ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    });
    this.onChangeInput(changes, this.nameValue, (currentValue?: any) => {
      this.setValueForm(this.form, this.name, currentValue);
    });
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
    if (this.required) {
      this.minlength = 10;
    }
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.value || null, disabled: this.disabled}, Validators.required)
      : new FormControl({value: this.value || null, disabled: this.disabled});
  }

  onChangeTelefone(event: any) {
    if (event) {
      event.length <= 10
          ? this.telefoneMask = '(00) 0000-00000'
          : this.telefoneMask = '(00) 00000-0000';
    }
  }

}
