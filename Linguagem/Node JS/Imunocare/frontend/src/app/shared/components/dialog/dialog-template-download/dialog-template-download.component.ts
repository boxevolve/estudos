import { Component, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ArquivoService } from '../../../../modulos/features/arquivo/arquivo.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'ic-dialog-template-download',
  templateUrl: 'dialog-template-download.component.html',
  styleUrls: ['./dialog-template-download.component.css']
})
export class DialogTemplateDownloadComponent {

  @Input() title: string;
  @Input() cancelLabel: string;
  @Input() confirmLabel: string;

  fileDownload: string;
  folderDownload: string;


  constructor(
    @Inject(MAT_DIALOG_DATA) data,
    private arquivoService: ArquivoService
  ) {
    if (data) {
      this.title = data.title
      this.cancelLabel = data.cancelLabel
      this.confirmLabel = data.confirmLabel
      this.fileDownload = data.fileDownload
      this.folderDownload = data.folderDownload
    }
  }

  public downloadFile(): void {
		this.arquivoService.downloadFile(this.fileDownload, this.folderDownload).subscribe(data => {
			saveAs(data, this.fileDownload), error => console.error(error)
		})
	}


}
