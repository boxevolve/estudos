import { Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';
import ModalUtil from '../../../utils/modal';

@Component({
  selector: 'ic-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.css']
})
export class UploadImageComponent extends BaseComponent implements OnInit, OnChanges {

  defaultPhoto = '../../../../assets/img/default_photo.jpg';

  @Input() form: FormGroup;
  @Input() name: string;
  @Input() title: string;
  @Input() clazz: any;
  @Input() value: any;
  @Input() disabled = false;
  @Input() required = false;
  @Input() id = 'upload_image';
  @Input() buttonInsert: any;
  @Input() buttonRemove: any;

  imageChangedEvent: any;
  idModal: string;

  nameDisabled = 'disabled';
  nameValue = 'value';

  constructor() {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameDisabled, (currentValue?: any) => {
      currentValue ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    });
    this.onChangeInput(changes, this.nameValue, (currentValue?: any) => {
      this.setValueForm(this.form, this.name, currentValue);
    });
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.value || null, disabled: this.disabled}, Validators.required)
      : new FormControl({value: this.value || null, disabled: this.disabled});
  }

  public getImage(): any {
    const image = this.getValueForm(this.form, this.name);
    return image ? image : this.defaultPhoto;
  }

  public uploadImage(event: any): void {
    this.setValueForm(this.form, this.name, event);
    this.clean();
  }

  public fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    ModalUtil.showModalBody(this.idModal);
  }

  public fileRemoveEvent(event: any): void {
    this.setValueForm(this.form, this.name, null);
  }

  public receiveId(id: string): void {
    this.idModal = id;
  }

  public clean(event?: any): void {
    this.imageChangedEvent = null;
    this.setValue(this.id, null);
  }

  public showRemove(): boolean {
    return !(!this.getValueForm(this.form, this.name));
  }

}
