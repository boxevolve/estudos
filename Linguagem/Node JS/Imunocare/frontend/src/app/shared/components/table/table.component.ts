import {
  Input,
  Output,
  EventEmitter,
  ViewChild,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  AfterContentInit,
  OnChanges
} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';
import { PageEvent, MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { merge, Observable, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { NotificationService } from '../../../modulos/servicos/notification.service';
import DialogUtil from '../../../utils/dialog-util';
import { DialogDeleteComponent } from '../dialog/dialog-delete/dialog-delete.component';
import { TableReloadService } from './table-reload.service';


@Component({
  selector: 'ic-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent extends BaseComponent implements OnInit, AfterViewInit, AfterContentInit, OnChanges {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() public showDelete = false;
  @Input() public titleDelete = 'Remover';
  @Input() public self: any;
  @Input() public placeholderFilter = 'Filter';
  @Input() public matSortActive = 'id';
  @Input() public matSortDirection = 'asc';
  @Input() public displayedColumns: string[];
  @Input() public headers: string[];
  @Input() public list: string[];
  @Input() public errorMessage: string;
  @Input() public observable: (self: any, paginator?: MatPaginator, sort?: MatSort, pageSize?: number) => Observable<any>;
  @Input() public mappingDataToTable: (data: any[]) => any[];
  @Input() public refresh: boolean;
  @Input() public showButton = false;
  @Input() public titleButton: string;
  @Input() public clazzButton: string[] = ['btn', 'btn-primary'];
  @Input() public clazzIconButton: string[] = ['glyphicon', 'glyphicon-asterisk'];

  @Output() rowClicked = new EventEmitter();
  @Output() delete = new EventEmitter();
  @Output() buttonAction = new EventEmitter();

  data: any[];
  columns: any[] = [];
  isLoadingResults = true;
  resultsLength = 0;
  pageSize = 25;
  pageSizeOptions: number[] = [25, 50, 100];
  pageEvent: PageEvent = undefined;
  dataSource: MatTableDataSource<any>;

  action: any = {value: 'action', description: 'Ação'};

  nameList = 'list';
  nameRefresh = 'refresh';

  constructor(
    public dialog: MatDialog,
    private notificationService: NotificationService,
    private cdr: ChangeDetectorRef,
    private tableReloadService: TableReloadService
  ) {
    super();
    this.tableReloadService.reload.subscribe(value => {
      if (value) {
        this.ngAfterViewInit();
      }
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameList, (currentValue?: any) => {
      if (!this.observable) {
        this.loadDataSource();
      }
    });
    this.onChangeInput(changes, this.nameRefresh, (currentValue?: any) => {
      if (!this.equals(currentValue, undefined)) {
        this.loadObservable();
      }
    });
  }

  ngAfterContentInit(): void {
    if (!this.observable) {
      this.loadDataSource();
      this.cdr.detectChanges();
    }
  }

  ngAfterViewInit(): void {
    this.loadObservable();
  }

  public loadObservable(): void {
    this.data = [];

    if (this.observable && this.paginator) {
      this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
      merge(this.paginator.page).pipe (
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.observable(this.self, this.paginator, this.sort, this.pageSize);
        }),
        map((data: any) => {
          this.isLoadingResults = false;
          this.resultsLength = data.contagem;
          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.notificationService.notifiy({
            message: this.errorMessage || this.msgUnexpectedError(),
            tipo: 'E',
          });
          return observableOf([]);
        })
      ).subscribe(data => {
          this.data = this.mappingDataToTable ? this.mappingDataToTable(data.retorno) : [];
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.sort = this.sort
        }
      );
    }
  }

  public loadDataSource(pageEvent?: PageEvent): void {
    this.resultsLength = this.list.length;
    const list = pageEvent ? this.pagination(this.list, pageEvent.pageIndex, pageEvent.pageSize) : this.pagination(this.list);
    this.data = [];
    this.data = this.mappingDataToTable ? this.mappingDataToTable(list) : [];
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.sort = this.sort;
    this.isLoadingResults = false;
  }

  ngOnInit(): void {
    this.generateColumns();
  }

  public generateColumns(): void {
    for (let index = 0; index < this.displayedColumns.length; index++) {
      const displayedColumn = this.displayedColumns[index];
      const column = {value: displayedColumn, description: ''};
      if (this.headers.length >= index) {
        column.description = this.headers[index];
      }
      this.columns.push(column);
    }
    if (this.showDelete || this.showButton) {
      this.displayedColumns.push(this.action.value);
      this.columns.push(this.action);
    }
  }

  public pagination(data: any[], pageIndex?: number, pageSize?: number): any[] {
    pageIndex = !pageIndex ? this.paginator.pageIndex : pageIndex;
    pageSize = !pageSize ? this.pageSize : pageSize;

    const newList = this.getClone(data);
    newList.splice(0, (pageIndex * pageSize));
    newList.splice(pageSize + 1, newList.length);
    return newList;
  }

  public changePaginator(event: any): void {
    this.pageEvent = event;
    if (!this.observable) {
      this.loadDataSource(this.pageEvent);
    }
  }

  public applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public rowClickedAction(row: any): void {
    this.rowClicked.emit(this.getClone(row));
  }

  public getValue(col: any): string {
    return col ? col.value : '';
  }

  public getDescription(col: any): string {
    return col ? col.description : '';
  }

  public isActionDelete(col: any): boolean {
    return (this.getValue(col) === this.action.value) && this.showDelete;
  }

  public isActionButton(col: any): boolean {
    return (this.getValue(col) === this.action.value) && this.showButton;
  }

  public deleteRow(event: any, row?: any): void {
    DialogUtil.openDialog(this.dialog, DialogDeleteComponent, null, (result: any) => {
      if (result) {
        this.delete.emit(this.getClone(row));
      }
    });
  }

  public actionRow(event: any, row?: any): void {
    this.buttonAction.emit(this.getClone(row));
  }

}
