import { Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Component } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';
import * as XLSX from 'xlsx';
import { NotificationService } from '../../../modulos/servicos/notification.service';
import { Constants } from '../../../utils/constants';
import DialogUtil from '../../../utils/dialog-util';
import { MatDialog } from '@angular/material';
import { Util } from '../../../utils/util';
import { SimNao } from '../../const/sim-nao.const';
import { ConfigXlsxModel } from '../../model/config-xlsx.model';
import { ArquivoService } from '../../../modulos/features/arquivo/arquivo.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'ic-upload-xlsx',
  templateUrl: './upload-xlsx.component.html',
  styleUrls: ['./upload-xlsx.component.css']
})
export class UploadXlsxComponent extends BaseComponent implements OnChanges {

  @Input() public id = 'upload_xlsx';
  @Input() public config: ConfigXlsxModel = new ConfigXlsxModel;
  @Input() public clazz: any;
  @Input() public saveList: any[];

  @Output() submitData = new EventEmitter();

  file: any;
  types: any[] = [
    {value: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', description: '.xlsx'}
  ];
  alertText: string;

  constructor(
    public dialog: MatDialog,
    private notificationService: NotificationService,
    private arquivoService: ArquivoService
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  public setData(data: any): void {
    this.submitData.emit(this.getClone(this.generateData(data)));
  }

  public onFileChange(event: any) {
    if (this.validateType(event.target)) {
      this.file = event.target;
    } else {
      this.showMessageError(this.notificationService, Constants.MSG_SPE_FILE_TYPES_ALLOWED + this.getTypesDescription());
    }
  }

  public uploadFile(event?: any): void {
    if (Util.isEmpty(this.config.template)) {
      this.showMessageError(this.notificationService, Constants.MSG_SPE_FILE_TEMPLATE_NOT_FOUND);
      return;
    }
    const target: DataTransfer = <DataTransfer>(this.file);
    if (target.files.length !== 1) {
      this.showMessageError(this.notificationService, Constants.MSG_SPE_FILE_NO_MULTIPLE);
      return;
    }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      const data = this.clearMatriz(<XLSX.AOA2SheetOpts>(XLSX.utils.sheet_to_json(ws, {header: 1})));
      const errorTemplate = this.validateTemplate(data);
      if (errorTemplate) {
        this.showAlert(`${Constants.MSG_SPE_FILE_TEMPLATE_INCORRECT}\n${errorTemplate}`);
        return;
      }
      const errorJson = this.validateJson(data);
      if (errorJson) {
        this.showAlert(`${Constants.MSG_SPE_FILE_UNFILLED_FIELD}\nTemplate:\n ${this.getTemplate()}\n${errorJson}`);
        return;
      }

      if (!this.config.duplicates) {
        const errorValidateDuplicate = this.validateUpload(data, this.config.idPosition, this.config.idText, this.saveList,
          this.config.idName);
        if (errorValidateDuplicate) {
          this.showAlert(errorValidateDuplicate);
          return;
        }
      }

      if (this.config.validate) {
        const errorValidate = this.config.validate(data, this.saveList, this.config.self);
        if (errorValidate) {
          this.showAlert(errorValidate);
          return;
        }
      }

      this.setData(data.splice(1, data.length));
    };
    reader.readAsBinaryString(target.files[0]);
  }

  public removeFile(event?: any): void {
    this.file = null;
    this.setValue(this.id, null);
  }

  public getFileName(): string {
    return (this.file && this.file.files && this.file.files.length) ? this.file.files[0].name : Constants.MSG_SPE_FILE_NO_FILE_SELECTED;
  }

  public getTypes(): string {
    return this.concatStr(this.types, ',', 'value');
  }

  public getTypesDescription(): string {
    return this.concatStr(this.types, ' ,', 'description');
  }

  public getTemplate(): string {
    let message = '';
    if(this.config.template) 
      this.config.template.forEach(item => {
        message += `Nome: [${item.description}] | Tipo: [${item.value}] | Valor nulo: [${item.null}]\n `;
      });
    
    return message;
  }

  public validateType(event: any): boolean {
    return (event && event.files && event.files.length) ? this.contains(this.types, null, (x: any) => {
      return this.equals(x.value, event.files[0].type);
    }) : false ;
  }

  public disabledButton(): boolean {
    return !this.file;
  }

  public validateTemplate(data: any): string {
    if (Util.isEmpty(data) || this.getClone(data[0]).length !== this.config.template.length) {
      return 'Esta faltando ou sobrando colunas';
    }
    const header = this.getClone(data[0]);
    let error = '';
    for (let index = 0; index < header.length; index++) {
      if (header[index] !== this.config.template[index].description) {
        error += `Coluna: ${index + 1} | Valor esperado: [${this.config.template[index].description}] - Encontrado: [${header[index]}] \n`;
      }
    }
    return error;
  }

  public validateJson(data: any): string {
    let error = '';
    const rows = [];
    const templateLength = this.getClone(data[0]).length;
    for (let index = 0; index < data.length; index++) {
      const length = Object.keys(data[index]).length;
      if (length !== templateLength) {
        rows.push(index);
      }
    }
    if (rows.length) {
      error += `Falta informação na(s) linha(s):\n${rows}`;
    }
    return error;
  }

  public showAlert(text: string): void {
    DialogUtil.openBaseDialog(this.dialog, `${Constants.MSG_SPE_TITLE_ALERT} ${Constants.MSG_SPE_FILE_INCONSISTENCY}`, text, 'Ok');
  }

  public showTemplate(event?: any): void {
    DialogUtil.openBaseDialog(this.dialog, `${Constants.MSG_SPE_TITLE_TEMPLATE}`, this.getTemplate(), 'Ok');
  }

  public generateData(data: any[]): any[] {
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < data[i].length; j++) {
        const simNao = SimNao.getByValue(data[i][j]);
        if (simNao) {
          data[i][j] = simNao.bool;
        }
      }
    }

    if (this.config.model) {
      data = this.getListObject(data, this.config.model);
    }
    return data;
  }

  // Validações de upload de arquivo
  public validateUpload(data: any, idPosition: number, idText: string, saveList?: any[], idName?: string): string {
    let error = '';
    let duplicatesLine: number[] = [];
    let duplicatesId: any[] = [];

    if (saveList && saveList.length) {
      error = this.validateSave(data, saveList, idPosition, idName, idText, duplicatesLine, duplicatesId);
    }
    if (!error) {
      duplicatesLine = [];
      duplicatesId = [];
      error = this.validateFile(data, idPosition, idText, duplicatesLine, duplicatesId);
    }
    return error;
  }

  public validateSave(data: any[], saveList: any[], idPosition: number, idName: string, idText: string, duplicatesLine: number[],
    duplicatesId: any[]): string {
    let error = '';

    for (let i = 1; i < data.length; i++) {
      const row = data[i];
      if (!this.contains(duplicatesLine, i)) {
        for (let j = 0; j < saveList.length; j++) {
          const saveRow = saveList[j];
          if (!this.contains(duplicatesLine, j)) {
            const id = row[idPosition];
            const saveId = saveRow[idName];
            if (this.equals(id + '', saveId + '')) {
              duplicatesLine.push(j);
              duplicatesId.push({ line: i, id: id });
              break;
            }
          }
        }
        error = this.messageDuplicates(duplicatesId, error, 'já salvo na lista, ', idText);
        duplicatesId = [];
      }
    }
    return error;
  }

  public validateFile(data: any, idPosition: number, idText: string, duplicatesLine: number[], duplicatesId: any[]): string {
    let error = '';

    for (let i = 1; i < data.length; i++) {
      const row = data[i];
      if (!this.contains(duplicatesLine, i)) {
        for (let j = 1; j < data.length; j++) {
          const validateRow = data[j];
          if (i !== j && !this.contains(duplicatesLine, j)) {
            const id = row[idPosition];
            const validateId = validateRow[idPosition];
            if (this.equals(id + '', validateId + '')) {
              duplicatesLine.push(j);
              duplicatesId.push({ line: j, id: id });
            }
          }
        }
        error = this.messageDuplicates(duplicatesId, error, 'duplicado na(s)', idText, i);
        duplicatesId = [];
      }
    }
    return error;
  }

  public messageDuplicates(duplicatesId: any[], error: string, message: string, idText: string, start?: number): string {
    if (duplicatesId.length) {
      error += `${idText} [${duplicatesId[0].id}] ${message} linhas: \n[`
      if (start) {
        error += `${start}, `;
      }
      duplicatesId.forEach((duplicateId, index) => {
        if (start) {
          index++;
        }
        if (index !== 0 && index % 50 === 0) {
          error += '\n';
        }
        error += `${duplicateId.line}, `;
      });
      error = error.substring(0, error.length - 2);
      error += `]\n\n`
    }
    return error;
  }

  public downloadFile(): void {
		this.arquivoService.downloadFile(this.config.fileDownload, this.config.folderDownload).subscribe(data => {
			saveAs(data, this.config.fileDownload), error => console.error(error)
		})
	}
}
