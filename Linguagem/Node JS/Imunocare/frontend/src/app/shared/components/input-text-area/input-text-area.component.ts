import { Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';

@Component({
  selector: 'ic-input-text-area',
  templateUrl: './input-text-area.component.html',
  styleUrls: ['./input-text-area.component.css']
})
export class InputTextAreaComponent extends BaseComponent implements OnInit, OnChanges {

  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() value: any = null;
  @Input() placeholder = '';
  @Input() maxlength = 50;
  @Input() clazz: string;

  nameDisabled = 'disabled';
  nameValue = 'value';

  constructor() {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameDisabled, (currentValue?: any) => {
      currentValue ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    });
    this.onChangeInput(changes, this.nameValue, (currentValue?: any) => {
      this.setValueForm(this.form, this.name, currentValue);
    });
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      new FormControl({value: this.value || null, disabled: this.disabled})
    );
  }
}
