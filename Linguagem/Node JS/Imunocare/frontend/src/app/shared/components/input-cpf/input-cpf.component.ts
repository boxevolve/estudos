import { Input, SimpleChanges, OnChanges, AfterContentChecked, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';
import { ErrorStateMatcher } from '@angular/material';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'ic-input-cpf',
  templateUrl: './input-cpf.component.html',
  styleUrls: ['./input-cpf.component.css']
})
export class InputCpfComponent extends BaseComponent implements OnInit, OnChanges, AfterContentChecked {

  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() value: any = null;
  @Input() required = false;
  @Input() placeholder = '';
  @Input() maxlength = 14;
  @Input() minlength = 0;
  @Input() clazz: string;

  cpfMask = '000.000.000-00';

  nameDisabled = 'disabled';
  nameValue = 'value';
  nameRequired = 'required';

  constructor(
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameDisabled, (currentValue?: any) => {
      currentValue ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    });
    this.onChangeInput(changes, this.nameValue, (currentValue?: any) => {
      this.setValueForm(this.form, this.name, currentValue);
    });
    this.onChangeInput(changes, this.nameRequired, (currentValue?: any) => {
      this.setRequired(currentValue);
    });
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
    if (this.required) {
      this.minlength = 11;
    }
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.value || null, disabled: this.disabled}, Validators.required)
      : new FormControl({value: this.value || null, disabled: this.disabled});
  }

  public setRequired(required: boolean): void {
    const validation = required ? [Validators.required, Validators.minLength(this.minlength)] : null;
    this.setValidationForm(this.form, this.name, validation);
  }

  public validateFieldCpf(): boolean {
    const validate = this.validateCpf(this.getValueForm(this.form, this.name));
    this.minlength = validate ? 11 : 12;
    return !validate && !this.validateRequired();
  }

  public validateRequired(): boolean {
    if (!this.required) {
      return false;
    }
    const control = this.getControl(this.form, this.name);
    if (control) {
      return control.hasError('required');
    }
    return this.getFormControl().hasError('required');
  }

}
