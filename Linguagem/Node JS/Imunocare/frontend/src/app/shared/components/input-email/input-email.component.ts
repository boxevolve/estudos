import { Input, SimpleChanges, OnChanges } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';
import { ErrorStateMatcher } from '@angular/material';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'ic-input-email',
  templateUrl: './input-email.component.html',
  styleUrls: ['./input-email.component.css']
})
export class InputEmailComponent extends BaseComponent implements OnInit, OnChanges {

  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() value: any = null;
  @Input() required = false;
  @Input() placeholder = '';
  @Input() maxlength = 60;
  @Input() minlength = 0;
  @Input() clazz: string;

  nameDisabled = 'disabled';
  nameValue = 'value';

  constructor() {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameDisabled, (currentValue?: any) => {
      currentValue ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    });
    this.onChangeInput(changes, this.nameValue, (currentValue?: any) => {
      this.setValueForm(this.form, this.name, currentValue);
    });
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
    if (this.required) {
      this.minlength = 1;
    }
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.value || null, disabled: this.disabled}, [Validators.required, Validators.email])
      : new FormControl({value: this.value || null, disabled: this.disabled}, Validators.email);
  }

  public validateEmail(): boolean {
    const control = this.getControl(this.form, this.name);
    if (control) {
      return control.hasError('email') && !this.validateRequired();
    }
    return this.getFormControl().hasError('email') && !this.getFormControl().hasError('required');
  }

  public validateRequired(): boolean {
    if (!this.required) {
      return false;
    }
    const control = this.getControl(this.form, this.name);
    if (control) {
      return control.hasError('required');
    }
    return this.getFormControl().hasError('required');
  }

}
