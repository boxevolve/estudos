import { Input, OnChanges, SimpleChanges, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../utils/base.component';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

const MY_DATE_FORMATS = {
  parse: { dateInput: 'DD/MM/YYYY', },
    display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'MMMM YYYY',
      dateA11yLabel: 'MMMM YYYY',
      monthYearA11yLabel: 'MMMM YYYY'
    }
  };

@Component({
  selector: 'ic-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.css'],
  providers: [
    {
      provide: MAT_DATE_LOCALE, useValue: 'pt'
    }, {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    }, {
      provide: MAT_DATE_FORMATS,
      useValue: MY_DATE_FORMATS
    },
  ]
})
export class InputDateComponent extends BaseComponent implements OnInit, OnChanges, AfterContentChecked {

  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() value: any = null;
  @Input() required = false;
  @Input() placeholder = '';
  @Input() maxlength = 14;
  @Input() minlength = 0;
  @Input() clazz: string;
  @Input() minDate: Date;
  @Input() minDateControl: string;
  @Input() maxDate: Date;
  @Input() maxDateControl: string;

  nameDisabled = 'disabled';
  nameValue = 'value';
  namePicker = 'date';

  constructor(
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngAfterContentChecked() {
    if (this.minDateControl) {
      this.form.get(this.minDateControl).valueChanges.subscribe(value => {
        this.minDate = value;
      });
    }
    if (this.maxDateControl) {
      this.form.get(this.maxDateControl).valueChanges.subscribe(value => {
        this.maxDate = value;
      });
    }
    this.cdr.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    const keyChanges = Object.keys(changes);
    if (this.contains(keyChanges, this.nameDisabled)) {
      changes[this.nameDisabled].currentValue ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    }
    if (this.contains(keyChanges, this.nameValue)) {
      this.setValueForm(this.form, this.name, changes[this.nameValue].currentValue);
    }
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
    if (this.required) {
      this.minlength = 1;
    }
    this.namePicker += `_${this.name}`;
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.value || null, disabled: this.disabled}, Validators.required)
      : new FormControl({value: this.value || null, disabled: this.disabled});
  }

  public validadeFieldDate(event: any, id: string): void {
    const dateformat = /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/;
    const value = event.currentTarget.value;
    if (value && !dateformat.test(value)) {
      $(`#${id}`).val('');
    }
  }

}
