export class DialogTemplateDownloadModel {
    constructor (
        public title?: string,
        public confirmLabel?: string,
        public cancelLabel?: string,
        public fileDownload?: string,
        public folderDownload?: string
    ) {}
}
