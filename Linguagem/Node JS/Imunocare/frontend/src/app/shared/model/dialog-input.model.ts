import { InputType } from '../const/input-type.const';

export class DialogInputModel {
  constructor (
    public title?: string,
    public text?: string,
    public confirmLabel?: string,
    public cancelLabel?: string,
    public name?: string,
    public placeholder?: string,
    public required?: boolean,
    public value?: any,
    public disabled?: boolean,
    public type?: InputType
  ) {}
}
