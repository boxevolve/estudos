export class DialogBaseModel {
    constructor (
        public title?: string,
        public text?: string,
        public confirmLabel?: string,
        public cancelLabel?: string
    ) {}
}
