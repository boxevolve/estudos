export class ConfigXlsxModel {
  constructor (
    public self?: any,
    public validate?: (data: any, saveList?: any[], self?: any) => string,
    public template?: any[],
    public model?: string[],
    public errorText?: string,
    public idPosition?: number,
    public idName?: string,
    public idText?: string,
    public duplicates?: boolean,
    public showDownloadButton?: boolean,
    public fileDownload?: string,
    public folderDownload?: string
  ) { }
}
