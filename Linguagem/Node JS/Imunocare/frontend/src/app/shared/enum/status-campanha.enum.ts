export enum StatusCampanha {
  Nova = 'Nova',
  Andamento = 'Andamento',
  Encerrada = 'Encerrada'
}
