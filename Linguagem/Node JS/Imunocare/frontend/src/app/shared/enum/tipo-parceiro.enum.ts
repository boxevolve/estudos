export enum TipoParceiro {
  Fornecedores = 'Fornecedores',
  Clientes = 'Clientes',
  Distribuidores = 'Distribuidores',
  Clinicas = 'Clinicas'
}
