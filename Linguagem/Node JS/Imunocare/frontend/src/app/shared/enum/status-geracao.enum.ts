export enum StatusGeracao {
  Pendente = 'Pendente',
  Executando = 'Executando',
  Finalizado = 'Finalizado'
}
