import { Injectable } from '@angular/core';
import { IC_API } from '../../../../app.api';
import { HttpClient } from '@angular/common/http';
import { CargaModel } from '../../../../modulos/features/carga/carga.model';
import { ImportacaoElegivelModel } from '../model/importacao-elegivel.model';
import { Observable } from 'rxjs';
import { NotificationService } from '../../../../modulos/servicos/notification.service';
import { Constants } from '../../../../utils/constants';
import { Util } from '../../../../utils/util';

@Injectable({
  providedIn: 'root'
})
export class ImportacaoElegivelService {

  importacaoElegiveis: ImportacaoElegivelModel[] = []

  constructor(
    private http: HttpClient,
    private notificationService: NotificationService
  ) { }

  consultar(sort?: string, order?: string, page?: number, size?: number): Observable<any> {
    let params: any = Util.addParams([{tipo: 'Elegíveis'}]);

    return this.http.get<any>(`${IC_API}/api/dados/cargas/arquivos`, { params })
      .do(data => this.importacaoElegiveis = this.converterParaImportacaoElegivel(data.retorno))
      .map(data => data);
  }

  converterParaImportacaoElegivel(cargas: CargaModel[]): ImportacaoElegivelModel[] {
    let retorno: ImportacaoElegivelModel[] = [];
    
    for (let carga of cargas) {
      let transfer: ImportacaoElegivelModel = {campanha: carga.campanha, dataHoraImportacao: carga.data, nomeArquivo: carga.filename, tipo: carga.tipo, qtdSucesso: carga.docs_total, qtdErros: carga.docs_erros, status: carga.status};
      retorno.push(transfer) ;
    }
    return retorno;
  }

  getById(id: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_id': id}
    ]);
    this.http.get<any>(`${IC_API}/api/dados/cargas/findOne`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  successCallback(result: any, next?: (result: any) => void, showCallback?: boolean): void {
    if (showCallback) {
      this.notificationService.notifiy({
        message: Constants.MSG_SUCCESS_OPERATION
      });
    }
    if (next) {
      next(result);
    }
  }

  errorCallback(error: any, message: string): void {
    this.notificationService.notifiy({
      message:  `${message} a importação de elegivel.`,
      tipo: 'E',
    });
  }

}
