import { Component, OnInit } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { ImportacaoElegivelModel } from '../model/importacao-elegivel.model';
import { ImportacaoElegivelService } from '../service/importacao-elegivel.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Util } from '../../../../utils/util';
import { Observable } from 'rxjs';

@Component({
  selector: 'ic-importacao-elegivel-list',
  templateUrl: './importacao-elegivel-list.component.html',
  styleUrls: ['./importacao-elegivel-list.component.css']
})
export class ImportacaoElegivelComponent implements OnInit {

  title: string;

  // INICIO - Variáveis para tabela
  displayedColumns: string[] = ['campanha', 'dataHoraImportacao', 'nomeArquivo', 'tipo', 'qtdSucesso', 'qtdErros', 'status'];
  headers: string[] = ['Campanha', 'Data hora importação', 'Nome do Arquivo', 'Tipo', 'Qtd com sucesso', 'Qtd com erros', 'Status'];
  errorMessage = 'Erro ao acessar dados das importações de elegiveis';
  // FIM - Variáveis para tabela

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private importacaoElegivelService: ImportacaoElegivelService
  ) { }

  ngOnInit() {
    this.title = this.activateRoute.snapshot.data['title'];
  }

  public consultar(paginator: MatPaginator, sort: MatSort, pageSize: number): Observable<any> {
    return this.importacaoElegivelService.consultar(
      sort.active,
      sort.direction,
      paginator.pageIndex,
      paginator.pageSize || pageSize
    );
  }

  // INICIO - Métodos para preencher a tabela
  public consultarObservable(self: any, paginator?: MatPaginator, sort?: MatSort, pageSize?: number): Observable<any> {
    return self.consultar(paginator, sort, pageSize);
  }

  public mappingDataToTable(data: any[]): any[] { return data }

  public rowClicked(row: any): void {
   // this.router.navigate(['edit', row._id], { relativeTo: this.activateRoute });
  }

}
