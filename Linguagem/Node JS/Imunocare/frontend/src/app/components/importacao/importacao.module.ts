import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportacaoElegivelComponent } from './elegivel/list/importacao-elegivel-list.component';


@NgModule({
  declarations: [
    ImportacaoElegivelComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ImportacaoElegivelComponent
  ]
})

export class ImportacaoModule {
  public constructor() { }
}
