interface ImportacaoElegivelModel {
    campanha: string,
    dataHoraImportacao: string,
    nomeArquivo: string,
    tipo?: string,
    qtdSucesso?: string,
    qtdErros?: string,
    status: string,
}

export { ImportacaoElegivelModel }
