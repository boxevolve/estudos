import { CampanhaModel, LoteModel, ClinicaTableModel } from '../../../../../modulos/features/campanhas/campanha.model';
import { Constants } from './../../../../../utils/constants';
import { Util } from './../../../../../utils/util';
import { BaseComponent } from './../../../../../utils/base.component';
import { NotificationService } from './../../../../../modulos/servicos/notification.service';
import { ParceiroService } from './../../../parceiros/service/parceiros.service';
import { CampanhaService } from './../../../../../modulos/features/campanhas/campanha.service';
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'ic-clinica-tab-form',
  templateUrl: './clinica-tab-form.component.html',
  styleUrls: ['./clinica-tab-form.component.css']
})
export class ClinicaTabFormComponent extends BaseComponent implements OnInit {

  @Input() idCampanha: string;
  @Input() currentCampanha: any = {};

  @Output() submitCurrent = new EventEmitter();

  nameCurrentCampanha = 'currentCampanha';

  formulario: FormGroup;
  campanha: CampanhaModel;

  displayedColumns: string[] = ['nome'];
  headers: string[] = ['Nome'];

  clinicas: any[] = [];
  clinica: any;
  fornecedores: ClinicaTableModel[] = [];

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private campanhaService: CampanhaService,
    private parceiroService: ParceiroService,
    private notificationService: NotificationService) {
    super();
  }

  ngOnInit() {
    this.campanha = this.campanhaService.setNew();
    this.createFormGroup();
    this.loadCampanha();
    this.loadClinicas();
  }

  public createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      _id: [null],
      clinicas: [null]
    });
  }

  public salvar(): void {
    if (!this.clinicas.length) {
      this.notificationService.notifiy({
        message: Constants.MSG_SPE_REQUIRED_LOT,
        tipo: 'E',
      });
      return;
    }
    this.validateForm(this.formulario, () => {
      this.loadCampanha(() => {
        const form = this.getCloneForm(this.formulario);
        form.clinicas = this.clinicas;
        this.campanha.fornecedores = this.fornecedores;
        this.campanhaService.updateCampanha(this.campanha).subscribe((result) => {
          this.loadCampanha();
          this.callbackSave(result);
        });
      })
    });
  }

  public callbackSave(result: any): void {
    this.notificationService.notifiy({
      message: Constants.MSG_SUCCESS_OPERATION
    });
    this.emitCurrentCampanha();
  }

  public emitCurrentCampanha(): void {
    this.submitCurrent.emit(this.getClone(this.campanha));
  }

  public loadCampanha(callback?: () => void): void {
    if (this.idCampanha) {
      this.campanhaService.findCampanhaDB(this.idCampanha).subscribe((campanha) => {
        this.campanha = campanha.retorno;
        if (callback) {
          callback();
        } else if (this.campanha.fornecedores && this.campanha.fornecedores.length) {
          this.fornecedores = this.campanha.fornecedores;
        }
      });
    }
  }

  public loadClinicas() {
    this.parceiroService.getAllClinicas().subscribe((clinicas: any) => {
      this.clinicas = clinicas.retorno;
    });
  }

  public voltar(event?: any): void {
    this.router.navigate(['cadastros/campanha']);
  }

  public incluirClinica(): void {
    if (this.clinica) {
      if (!this.fornecedores.find(existe => existe.clinica == this.clinica._id)) {
        this.fornecedores = this.getClone(this.fornecedores);
        this.fornecedores.push({
          clinica: this.clinica._id,
          nome: this.clinica.empresa
        });
        
      } else {
        this.notificationService.notifiy({
          message: Constants.MSG_DUPLICATED
        });
      }
    }
  }

  // INICIO - Métodos para preencher a tabela
  public mappingDataToTable(data: any[]): any[] {
    if (Util.isEmpty(data)) {
      return [];
    }
    let table: any[] = []
    for (let linha of data) {
      let tab: any = this.getClone(linha);
      tab._id = linha._id;
      tab.clinica = linha.clinica;
      tab.nome = linha.nome;
      table.push(tab)
    }
    return table
  }

  public deleteClinica(clinica: any): void {
    this.fornecedores = this.getClone(this.fornecedores);
    const index = this.fornecedores.findIndex(existe => existe.clinica === clinica.clinica);
    if (index !== -1) {
      this.fornecedores.splice(index, 1);
    }
  }

  public getClinicas(): any[] {
    return !Util.isEmpty(this.clinicas) ? this.clinicas : [];
  }

  public getNameCampanha(): string {
    return this.currentCampanha ? this.currentCampanha.campanha : '';
  }

  public compareTo(objectA: any, objectB: any): boolean {
    return (objectA && objectB) ? objectA._id === objectB._id : false;
  }

  public changeClinica(event?: any): void {
    if (event.value) {
      this.clinica = event.value;
    }
  }

}
