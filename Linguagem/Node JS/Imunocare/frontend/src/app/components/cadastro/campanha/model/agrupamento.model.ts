export class AgrupamentoModel {
    constructor (
        public _id?: string,
        public descricao?: number,
        public criado_por?: string,
        public data?: Date
    ) {}
}
