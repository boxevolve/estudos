import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'ic-campanha-form',
	templateUrl: './campanha-form.component.html',
	styleUrls: ['./campanha-form.component.css'],
})
export class CampanhaFormComponent implements OnInit {
	disableTabFilial = true;
	disableTabUnidade = true;
	disableTabProduto = true;
	disableTabClinica = true;
	idCampanha: string;
	currentCampanha: any;

	constructor(private activateRoute: ActivatedRoute) {}

	ngOnInit() {
		this.edit();
	}

	public edit(): void {
		this.activateRoute.params.subscribe(parametrosUrl => {
			this.idCampanha = parametrosUrl['id'];
		});
	}

	public receiveIdCampanha(idCampanha: string): void {
		this.idCampanha = idCampanha;
	}

	public receiveCurrentCampanha(currentCampanha: string): void {
		this.currentCampanha = currentCampanha;
	}

	public setDisableAll(disable: boolean): void {
		this.setDisableTabFilial(disable);
		this.setDisableTabUnidade(disable);
		this.setDisableTabProduto(disable);
		this.setDisableTabClinica(disable);
	}

	public setDisableTabFilial(disable: boolean): void {
		this.disableTabFilial = disable;
	}

	public setDisableTabUnidade(disable: boolean): void {
		this.disableTabUnidade = disable;
	}

	public setDisableTabProduto(disable: boolean): void {
		this.disableTabProduto = disable;
	}

	public setDisableTabClinica(disable: boolean): void {
		this.disableTabClinica = disable;
	}
}
