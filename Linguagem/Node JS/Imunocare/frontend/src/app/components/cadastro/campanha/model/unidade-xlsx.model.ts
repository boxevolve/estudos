export class UnidadeXlsxModel {
  constructor (
    public cnpj?: string,
    public cod_unid?: string,
    public nome_unid?: string,
    public endereco?: string,
    public bairro?: string,
    public cidade?: string,
    public estado?: string,
    public cep?: string,
    public numero?: number,
    public complemento?: string,
    public previsao_doses?: number,
    public responsavel?: string,
    public fone?: string
  ) {}
}
