import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CampanhaNewTableModel } from '../../../../modulos/features/campanhas/campanha.model';
import { CampanhaService } from '../../../../modulos/features/campanhas/campanha.service';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { Util } from '../../../../utils/util';
import { BaseComponent } from '../../../../utils/base.component';
import DialogUtil from '../../../../utils/dialog-util';
import { UsuarioService } from '../../../../modulos/features/usuarios/usuario.service';
import { CargaService } from '../../../../modulos/features/carga/carga.service';
import { ElegivelService } from '../../../../modulos/features/elegiveis/elegivel.service';

@Component({
  selector: 'ic-campanha-list',
  templateUrl: './campanha-list.component.html',
  styleUrls: ['./campanha-list.component.css']
})
export class CampanhaListComponent extends BaseComponent implements OnInit {

  title: string;

  // INICIO - Variáveis para tabela
  displayedColumns: string[] = ['cliente', 'campanha', 'status', 'gestor', 'responsavel_gestor'];
  headers: string[] = ['Cliente', 'Nome Campanha', 'Status', 'Gestor', 'Responsável'];
  errorMessage = 'Erro ao acessar dados das campanhas';
  // FIM - Variáveis para tabela
  
  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private campanhaService: CampanhaService,
    private dialog: MatDialog,
    private elegivelService: ElegivelService,
    private cargaService: CargaService,
    private usuarioService: UsuarioService
    
  ) {
    super();
  }

  ngOnInit() {
    this.title = this.activateRoute.snapshot.data['title'];
  }

  public consultar(paginator: MatPaginator, sort: MatSort, pageSize: number): Observable<any> {
    return this.campanhaService.consultarByUsuario(
      sort.active,
      sort.direction,
      paginator.pageIndex,
      paginator.pageSize || pageSize
    );
  }

  // INICIO - Métodos para preencher a tabela
  public consultarObservable(self: any, paginator?: MatPaginator, sort?: MatSort, pageSize?: number): Observable<any> {
    return self.consultar(paginator, sort, pageSize);
  }

  public mappingDataToTable(data: any[]): any[] {
    if (Util.isEmpty(data)) {
      return [];
    }
    let table: CampanhaNewTableModel[] = []
    for (let linha of data) {
      let tab: CampanhaNewTableModel = {_id: '', cliente: '', campanha: '', status: '', gestor: '', responsavel_gestor: ''};
      tab._id = linha._id;
      tab.cliente = linha.cliente ? linha.cliente.empresa : '';
      tab.campanha = linha.campanha;
      tab.status = linha.status;
      tab.gestor = linha.gestor ? linha.gestor.empresa : '';
      tab.responsavel_gestor = linha.responsavel_gestor;
      table.push(this.instantiateString(tab));
    }
    return table
  }

  public rowClicked(row: any): void {
    this.router.navigate(['edit', row._id], { relativeTo: this.activateRoute });
  }
  // FIM - Métodos para preencher a tabela

  incluir(event?: any): void {
    this.router.navigate(['add'], { relativeTo: this.activateRoute });
  }

  public excluir(campanha): void {
    this.elegivelService.deleteByCampanha(campanha._id, (result) => {
      this.cargaService.deleteByCampanha(campanha._id, (result) => {
        this.campanhaService.deleteEventoByCampanha(campanha._id, (result) => {
          this.usuarioService.updateByDeleteCampanha(campanha._id, (result) => {
            this.campanhaService.delete(campanha._id, (result) => {
              this.refreshTable();
            });
          })
        })
      })
    });
  }

  refreshList: boolean;

  public refreshTable(): void {
    this.refreshList = !this.refreshList;
  }

}
