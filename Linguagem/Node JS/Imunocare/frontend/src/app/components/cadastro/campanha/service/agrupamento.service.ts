import { Injectable } from '@angular/core';
import { IC_API } from '../../../../app.api';
import { HttpClient } from '@angular/common/http';
import { ProdutoModel } from '../../../../modulos/features/produtos/produto.model';
import { Observable } from 'rxjs';
import { NotificationService } from '../../../../modulos/servicos/notification.service';
import { Constants } from '../../../../utils/constants';
import { Util } from '../../../../utils/util';
import { AgrupamentoModel } from '../model/agrupamento.model';

@Injectable({
  providedIn: 'root'
})
export class AgrupamentoService {
  url = `${IC_API}/api/dados/agrupamento`;

  agrupamentos: AgrupamentoModel[] = []

  constructor(
    private http: HttpClient,
    private notificationService: NotificationService,
  ) { }

  consultar(sort?: string, order?: string, page?: number, size?: number): Observable<any> {
    let params: any = Util.addParams([
      {'sort': sort},
      {'order': order},
      {'page': page},
      {'size': size}
    ]);

    return this.http.get<any>(`${this.url}/retrieve`, { params })
      .do(data => this.agrupamentos = data.retorno)
      .map(data => data);
  }

  getById(id: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_id': id}
    ]);
    this.http.get<any>(`${this.url}/findOne`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  save(agrupamento: AgrupamentoModel, next?: (result: any) => void, showCallback?: boolean): void {
    showCallback = !Util.isDefined(showCallback) ? true : showCallback;
    this.http.post<ProdutoModel>(`${this.url}/create`, agrupamento)
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_SAVE)}
    );
  }

  update(agrupamento: AgrupamentoModel, next?: (result: any) => void, showCallback?: boolean): void {
    showCallback = !Util.isDefined(showCallback) ? true : showCallback;
    let params: any = Util.addParams([
      {'_id': agrupamento._id}
    ]);
    this.http.patch<ProdutoModel>(`${this.url}/update`, agrupamento, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_EDIT)}
    );
}

  successCallback(result: any, next?: (result: any) => void, showCallback?: boolean): void {
    if (showCallback) {
      this.notificationService.notifiy({
        message: Constants.MSG_SUCCESS_OPERATION
      });
    }
    if (next) {
      next(result);
    }
  }

  errorCallback(error: any, message: string): void {
    message = `${message} o agrupamento.`;
    if (error.error.error.code === 11000) {
      message = Constants.MSG_DUPLICATED;
    }

    this.notificationService.notifiy({
      message:  message,
      tipo: 'E',
    });
  }

}
