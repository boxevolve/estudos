import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ParceiroService } from '../service/parceiros.service';
import { ParceiroTableModel } from '../../../../modulos/features/cadastros/empresa.model';
import { Router, ActivatedRoute } from '@angular/router';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import DialogUtil from '../../../../utils/dialog-util';
import { NotificationService } from '../../../../modulos/servicos/notification.service';

@Component({
  selector: 'ic-parceiros-list',
  templateUrl: './parceiros-list.component.html',
  styleUrls: ['./parceiros-list.component.css']
})
export class ParceirosListComponent implements OnInit {

  title: string;

  // INICIO - Variáveis para tabela
  displayedColumns: string[] = ['empresa', 'cnpj', 'tipo', 'responsavel', 'email', 'fone'];
  headers: string[] = ['Empresa', 'CNPJ', 'Tipo', 'Responsável', 'E-mail', 'Telefone'];
  errorMessage = 'Erro ao acessar dados dos parceiros';
  refreshList: boolean;
  // FIM - Variáveis para tabela

  enumTipoValidacao = {cliente: 'CLIE', distribuidor: 'DIST', gestor: 'GEST', clinica: 'CLIN', evento: 'EVEN', produto: 'PROD'};

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private parceiroService: ParceiroService,
    private notificationService: NotificationService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.title = this.activateRoute.snapshot.data['title'];
  }

  public consultar(paginator: MatPaginator, sort: MatSort, pageSize: number): Observable<any> {
    return this.parceiroService.consultar(
      sort.active,
      sort.direction,
      paginator.pageIndex,
      paginator.pageSize || pageSize
    );
  }

  // INICIO - Métodos para preencher a tabela
  public consultarObservable(self: any, paginator?: MatPaginator, sort?: MatSort, pageSize?: number): Observable<any> {
    return self.consultar(paginator, sort, pageSize);
  }

  public mappingDataToTable(data: any[]): any[] {
    let tabArray = []
    let table: ParceiroTableModel[] = []

    for (let linha of data) {
      let tab: ParceiroTableModel = { _id: '', empresa: '', cnpj: '', tipo: '', responsavel: '', email: '', fone: '' }
      tab._id = linha._id
      tab.cnpj = linha.cnpj
      tab.email = linha.email
      tab.empresa = linha.empresa
      tab.fone = linha.fone
      tab.responsavel = linha.responsavel
      tab.tipo = linha.tipo
      table.push(tab)
    }
    return table
  }

  public rowClicked(row: any): void {
    this.router.navigate(['edit', row._id], { relativeTo: this.activateRoute });
  }
  // FIM - Métodos para preencher a tabela

  incluir(): void {
    this.router.navigate(['add'], { relativeTo: this.activateRoute });
  }

  public excluir(parceiro): void {
    this.parceiroService.validarExclusao(parceiro._id, this.enumTipoValidacao.cliente, (countCliente) => {
      this.parceiroService.validarExclusao(parceiro._id, this.enumTipoValidacao.distribuidor, (countDistribuidor) => {
        this.parceiroService.validarExclusao(parceiro._id, this.enumTipoValidacao.gestor, (countGestor) => {
          this.parceiroService.validarExclusao(parceiro._id, this.enumTipoValidacao.clinica, (countClinica) => {
            this.parceiroService.validarExclusao(parceiro._id, this.enumTipoValidacao.evento, (countEvento) => {
              this.parceiroService.validarExclusao(parceiro._id, this.enumTipoValidacao.produto, (countProduto) => {
                
                let vinculo = this.descobrirTipoVinculo(countCliente.contagem > 0, 
                  countDistribuidor.contagem > 0,
                  countGestor.contagem > 0,
                  countClinica.contagem > 0,
                  countEvento.contagem > 0,
                  countProduto.contagem > 0)

                if(vinculo.length == 0) {
                  this.parceiroService.delete(parceiro, (result) => {
                    this.refreshTable();
                  });
                } else {
                  this.notificationService.notifiy({
                    message: vinculo.join('<br/>'),
                    tipo: 'E',
                  });
                }
              })
            })
          })
        })
      })
    });
  }

  private descobrirTipoVinculo(campanhaPorCliente, campanhaPorDistribuidor, campanhaPorGestor, campanhaPorClinica, evento, produto) {
    let vinculos = [], vinculosCampanha = [];
    campanhaPorCliente ? vinculosCampanha.push('cliente') : null;
    campanhaPorDistribuidor ? vinculosCampanha.push('distribuidor') : null;
    campanhaPorGestor ? vinculosCampanha.push('gestor') : null;
    campanhaPorClinica ? vinculosCampanha.push('clínica de fornecedor') : null;
    
    if(vinculosCampanha.length  != 0) {
      let vinculoCampanhaFinal = vinculosCampanha.join()
      vinculoCampanhaFinal = vinculosCampanha.length > 1 ? `${vinculoCampanhaFinal.substring(0, vinculoCampanhaFinal.lastIndexOf(','))} e  
          ${vinculoCampanhaFinal.substring(vinculoCampanhaFinal.lastIndexOf(',') + 1)}` : vinculosCampanha.pop()
      vinculos.push(`A Empresa apresenta vinculos como ${vinculoCampanhaFinal} em uma Campanha`)
    }
  
    evento? vinculos.push('A Empresa apresenta vinculos com Evento') : null
    produto? vinculos.push('A empresa apresenta vinculos com Produto') : null
    return vinculos;
  }
 
  
  public refreshTable(): void {
    this.refreshList = !this.refreshList;
  }


}
