import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs/Observable'
import { EmpresaModel } from '../../../../modulos/features/cadastros/empresa.model';
import { IC_API } from '../../../../app.api';
import { Util } from '../../../../utils/util';
import { NotificationService } from '../../../../modulos/servicos/notification.service';
import { Constants } from '../../../../utils/constants';

@Injectable({ providedIn: 'root' })
export class ParceiroService {

    parceiros: EmpresaModel[] = [];
    parceiro: EmpresaModel;

    public constructor(
        private http: HttpClient,
        private notificationService: NotificationService,
    ) { }

    consultar(sort?: string, order?: string, page?: number, size?: number): Observable<any> {
        let params: any = Util.addParams([
            { 'sort': sort },
            { 'order': order },
            { 'page': page },
            { 'size': size }
        ]);

        return this.http.get<any>(`${IC_API}/api/dados/empresas/retrieveAll`, { params: params })
            .do(data => this.parceiros = data.retorno)
            .map(data => data);
    }

    getAllFornecedores(): Observable<any> {
        return this.http.get<any>(`${IC_API}/api/dados/empresas/listafornecedores`)
            .do(data => this.parceiros = data.retorno)
            .map(data => data);
    }

    getAllClientes(): Observable<any> {
        return this.http.get<any>(`${IC_API}/api/dados/empresas/listaClientes`)
            .do(data => this.parceiros = data.retorno)
            .map(data => data);
    }

    getAllClinicas(): Observable<any> {
        return this.http.get<any>(`${IC_API}/api/dados/empresas/listaClinicas`)
            .do(data => this.parceiros = data.retorno)
            .map(data => data);
    }

    getById(id: string, next?: (result: any) => void, showCallback?: boolean): void {
        let params: any = Util.addParams([
            { '_id': id }
        ]);
        this.http.get<any>(`${IC_API}/api/dados/empresas/findOne`, { params })
            .subscribe((result) => { this.successCallback(result, next, showCallback) },
                error => { this.errorCallback(error, Constants.MSG_ERROR_RECOVER) }
            );
    }

    update(parceiro: EmpresaModel, next?: (result: any) => void, showCallback?: boolean): void {
        showCallback = !Util.isDefined(showCallback) ? true : showCallback;
        let params: any = Util.addParams([
          { '_id': parceiro._id }
        ]);
        this.http.patch<EmpresaModel>(`${IC_API}/api/dados/empresas/update`, parceiro, { params })
          .subscribe((result) => { this.successCallback(result, next, showCallback) },
            error => { this.errorCallback(error, error.error.mensagem) }
          );
      }

      validarExclusao(_id: string, tipoValidacao: string, next?: (result: any) => void, showCallback?: boolean): void {
        let params: any = Util.addParams([
          { '_id': _id},
          { 'tipoValidacao': tipoValidacao }
        ]);
        this.http.get<any>(`${IC_API}/api/dados/empresas/validarExclusao`, { params })
          .subscribe((result) => { this.successCallback(result, next, showCallback) },
            error => { this.errorCallback(error, error.error.mensagem) }
          );
      }

      delete(parceiro: any, next?: (result: any) => void, showCallback?: boolean): void {
        showCallback = !Util.isDefined(showCallback) ? true : showCallback;
        let params: any = Util.addParams([
          { '_id': parceiro._id }
        ]);
        this.http.delete<EmpresaModel>(`${IC_API}/api/dados/empresas/delete`, { params })
          .subscribe((result) => { this.successCallback(result, next, showCallback) },
            error => { this.errorCallback(error, error.error.mensagem) }
          );
      }

      
    successCallback(result: any, next?: (result: any) => void, showCallback?: boolean): Observable<any> {
        if (showCallback) {
            this.notificationService.notifiy({
                message: Constants.MSG_SUCCESS_OPERATION
            });
        }
        if (next) {
            next(result);
        }
        return result;
    }

    errorCallback(error: any, message: string): void {
        this.notificationService.notifiy({
            message: `${message}`,
            tipo: 'E',
        });
    }

    save(parceiro: EmpresaModel, next?: (result: any) => void, showCallback?: boolean): void {
        showCallback = !Util.isDefined(showCallback) ? true : showCallback;
        this.http.post<EmpresaModel>(`${IC_API}/api/dados/empresas/create`, parceiro)
          .subscribe((result) => {this.successCallback(result, next, showCallback)},
            error => { this.errorCallback(error, error.error.mensagem)}
        );
      }

}
