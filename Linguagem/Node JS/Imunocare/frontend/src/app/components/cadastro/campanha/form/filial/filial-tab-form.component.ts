import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FilialTableModel } from '../../../../../modulos/features/cadastros/empresa.model';
import { CampanhaModel, FilialCampanhaModel } from '../../../../../modulos/features/campanhas/campanha.model';
import { CampanhaService } from '../../../../../modulos/features/campanhas/campanha.service';
import { Util } from '../../../../../utils/util';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BaseComponent } from '../../../../../utils/base.component';
import { NotificationService } from '../../../../../modulos/servicos/notification.service';
import { Constants } from '../../../../../utils/constants';
import { Router } from '@angular/router';
import { ConfigXlsxModel } from '../../../../../shared/model/config-xlsx.model';
import { ArquivoService } from '../../../../../modulos/features/arquivo/arquivo.service';

@Component({
  selector: 'ic-filial-tab-form',
  templateUrl: './filial-tab-form.component.html',
  styleUrls: ['./filial-tab-form.component.css']
})
export class FilialTabFormComponent extends BaseComponent implements OnInit {

  @Input() idCampanha: string;
  @Input() currentCampanha: any = {};

  @Output() disableTabUnidade = new EventEmitter();
  @Output() submitCurrent = new EventEmitter();

  nameCurrentCampanha = 'currentCampanha';
  nameFiliais = 'filiais';

  formulario: FormGroup;
  campanha: CampanhaModel;

  filiais: FilialCampanhaModel[] = [];
  removeList: FilialCampanhaModel[] = [];

  // INICIO - Variáveis para tabela
  displayedColumns: string[] = ['filial', 'cnpj', 'tipo', 'responsavel', 'fone'];
  headers: string[] = ['Empresa', 'CNPJ', 'Tipo', 'Responsável', 'Telefone'];
  // FIM - Variáveis para tabela

  // INICIO - Variáveis para upload xlsx
  configXlsx: ConfigXlsxModel;

  templateXlsx: any[] = [
    {value: 'Texto', description: 'filial', null: ' - '                 },
    {value: 'Texto', description: 'cnpj', null: ' - '                   },
    {value: 'Texto', description: 'responsavel', null: ' - '            },
    {value: 'Texto', description: 'email', null: ' - '                  },
    {value: 'Texto', description: 'fone', null: ' - '                   },
    {value: 'Número', description: 'previsao_doses', null: '0'          },
    {value: 'Texto', description: 'usuario_atendimento', null: ' - '    },
    {value: 'Sim/Não', description: 'atendimento_realizado', null: 'não'}
  ];

  modelFromFileToObj: string[] = [
    'filial',
    'cnpj',
    'responsavel',
    'email',
    'fone',
    'previsao_doses',
    'usuario_atendimento',
    'atendimento_realizado'
  ];
  // INICIO - Variáveis para upload xlsx

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private notificationService: NotificationService,
    private campanhaService: CampanhaService,
    private arquivoService: ArquivoService

  ) {
    super();
  }

  ngOnInit() {
    this.createConfigXlsx();
    this.createFormGroup();
    this.setDisableTabUnidade(true);
    this.loadCampanha();
  }

  public createConfigXlsx(): void {
    this.configXlsx = new ConfigXlsxModel(this, this.validateUpload, this.templateXlsx, this.modelFromFileToObj, null, 1, 'cnpj', 
      'CNPJ', false, true, 'Template Importação Filiais.xlsx', 'templates');
  }

  public validateUpload(data: any, saveList?: any[], self?: any): string {
    let error = '';
    for (let index = 1; index < data.length; index++) {
      const cnpj = data[index][1];
      if (!self.validateCnpj(cnpj)) {
        error += `CPNJ [${cnpj}] inválido na linha [${index}]\n `
      }
    }
    return error;
  }

  public createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      filiais: [[]]
    });
  }

  public loadCampanha(callback?: () => void): void {
    if (this.idCampanha) {
      this.campanhaService.findCampanhaDB(this.idCampanha).subscribe((data) => {
        this.campanha = data.retorno;
        const lengthFilial = this.campanha.filiais && this.campanha.filiais.length;
        if (callback) {
          callback();
        } else if (lengthFilial) {
          this.setValueForm(this.formulario, this.nameFiliais, []);
          this.filiais = this.campanha.filiais;
        }
        this.setDisableTabUnidade(lengthFilial === 0);
        this.emitCurrentCampanha();
      });
    }
  }

  public salvar(): void {
    const isNewList = this.getValueForm(this.formulario, this.nameFiliais).length;
    const isRemoveList = this.removeList.length;
    if (!isNewList && !isRemoveList) {
      this.notificationService.notifiy({
        message:  Constants.MSG_SPE_REQUIRED_FILIAL,
        tipo: 'E',
      });
      return;
    }
    this.validateForm(this.formulario, () => {
      this.loadCampanha(() => {
        this.removeSaveUnidade(isNewList);
      })
    });
  }

  public removeSaveUnidade(isNewList: boolean): void {
    this.removeList.forEach(removeItem => {
      this.campanha.filiais = this.remove(this.campanha.filiais, null, null, (x: any) => {
        return this.equals(x.cnpj, removeItem.cnpj);
      });
    });

    this.removeList = [];
    if (isNewList) {
      const form = this.getCloneForm(this.formulario);
      if (!this.campanha.filiais) {
        this.campanha.filiais = [];
      }
      this.addAll(this.campanha.filiais, form.filiais);
    }
    this.finishSave();
  }

  public finishSave(): void {
    this.campanhaService.updateCampanha(this.campanha).subscribe((result) => {
      this.loadCampanha();
      this.callbackSave(result);
    });
  }

  public callbackSave(result: any): void {
    this.notificationService.notifiy({
      message: Constants.MSG_SUCCESS_OPERATION
    });
  }

  public emitCurrentCampanha(): void {
    this.submitCurrent.emit(this.getClone(this.campanha));
  }

  public voltar(event?: any): void {
    this.router.navigate(['cadastros/campanha']);
  }

  public receiveData(data: any): void {
    this.filiais = this.getClone(this.filiais);
    this.addAll(this.filiais, data);
    const formList = this.getValueForm(this.formulario, this.nameFiliais);
    this.addAll(formList, data);
    this.setValueForm(this.formulario, this.nameFiliais, formList);
  }

  public setDisableTabUnidade(disable: boolean): void {
    this.disableTabUnidade.emit(disable);
  }

  public getNameCampanha(): string {
    return this.currentCampanha ? this.currentCampanha.campanha : '';
  }

  public mappingDataToTable(data: any[]): any[] {
    if (Util.isEmpty(data)) {
      return [];
    }
    let table: FilialTableModel[] = data;
    /**
     * Mock de tipo
     */
    table.forEach(row => {
      row.tipo = 'Filial';
    });
    return table
  }

  public removeUnidade(filial?: any): void {
    this.filiais = this.remove(this.filiais, null, null, (x: any) => {
      return this.equals(x.cnpj, filial.cnpj);
    });
    this.setValueForm(this.formulario, this.nameFiliais,
      this.remove(this.getValueForm(this.formulario, this.nameFiliais), null, null, (x: any) => {
        return this.equals(x.cnpj, filial.cnpj);
      })
    );
    this.removeList.push(filial);
  }
}
