import { Component, OnInit, Output, EventEmitter, Input, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { BaseComponent } from '../../../../../utils/base.component';
import { CampanhaModel } from '../../../../../modulos/features/campanhas/campanha.model';
import { Util } from '../../../../../utils/util';
import { StatusCampanha } from '../../../../../shared/enum/status-campanha.enum';
import { SimNao } from '../../../../../shared/const/sim-nao.const';
import { EmpresaModel } from '../../../../../modulos/features/cadastros/empresa.model';
import { ParceiroService } from '../../../parceiros/service/parceiros.service';
import { Router } from '@angular/router';
import { CampanhaService } from '../../../../../modulos/features/campanhas/campanha.service';
import { StatusGeracao } from '../../../../../shared/enum/status-geracao.enum';
import { NotificationService } from '../../../../../modulos/servicos/notification.service';
import { Constants } from '../../../../../utils/constants';
import { AgrupamentoService } from '../../service/agrupamento.service';
import { AgrupamentoModel } from '../../model/agrupamento.model';
import ModalUtil from '../../../../../utils/modal';
import { LoginService } from '../../../../../modulos/servicos/login.service';
import { UsuarioService } from '../../../../../modulos/features/usuarios/usuario.service';
import { CargaService } from '../../../../../modulos/features/carga/carga.service';
import { ElegivelService } from '../../../../../modulos/features/elegiveis/elegivel.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'ic-campanha-tab-form',
  templateUrl: './campanha-tab-form.component.html',
  styleUrls: ['./campanha-tab-form.component.css']
})
export class CampanhaTabFormComponent extends BaseComponent implements OnInit, AfterViewChecked {

  @Input() idCampanha: string;
  @Input() currentCampanha: any = {};

  @Output() submitId = new EventEmitter();
  @Output() submitCurrent = new EventEmitter();
  @Output() disableTabFilial = new EventEmitter();
  @Output() disableTabUnidade = new EventEmitter();
  @Output() disableTabProduto = new EventEmitter();
  @Output() disableTabClinica = new EventEmitter();

  nameDescricao = 'campanha';
  nameStatus = 'status';
  nameInCompany = 'in_company';
  nameCliente = 'cliente';
  nameFornecedor = 'distribuidor';
  nameResponsavelCliente = 'responsavel_cliente';
  nameEmailCliente = 'email_cliente';
  nameTelefoneCliente = 'telefone_cliente';
  nameDataInicio = 'data_inicio';
  nameDataFim = 'data_fim';
  nameHorarioFuncionamentoIni = 'horario_funcionamento_ini';
  nameHorarioFuncionamentoFim = 'horario_funcionamento_fim';
  nameGestor = 'gestor';
  nameResponsavelGestor = 'responsavel_gestor';
  nameEmailGestor = 'email_gestor';
  nameTelefoneGestor = 'telefone_gestor';
  nameAgrupamento = 'agrupamento';
  nameCadastroAutomatico = 'cadastro_auto';
  namePesquisaSatisfacao = 'pesquisa_satisfacao';
  namePesquisaSaude = 'pesquisa_saude';
  nameFotoGestor = 'foto_gestor';
  nameSenhaVacinacao = 'senha_vacinacao';
  nameObservacao = '';

  formulario: FormGroup;
  campanha: CampanhaModel;

  clientes: EmpresaModel[] = [];
  fornecedores: EmpresaModel[] = [];
  gestores: EmpresaModel[] = [];
  agrupamentos: AgrupamentoModel[] = [];

  status: string;
  statusDisabled = false;
  fieldDisabled = false;
  idModalAgrupamento: string;
  
  controleFornecedores = new FormControl(null, Validators.required)
  fornecedoresOptions: Observable<EmpresaModel[]>

  constructor(
    private loginService: LoginService,
    private usuarioService: UsuarioService,
    private cdr: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private router: Router,
    private parceiroService: ParceiroService,
    private campanhaService: CampanhaService,
    private agrupamentoService: AgrupamentoService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  ngOnInit() {
    this.campanha = this.campanhaService.setNew();
    this.loadCombos();
    this.createFormGroup();
    this.edit();
    this.setStatus();
    this.validateTabs();
  }

  public createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      _id: [null],
      geracao: [{
        step: null,
        status: StatusGeracao.Pendente
      }],
      valor_unitario_servico: [2]
    });
    this.formulario.addControl('distribuidor', this.controleFornecedores)
  }

  public loadCombos(): void {
    this.loadClientes();
    this.loadFornecedores();
    this.loadGestores();
    this.loadAgrupamentos(true);
  }

  public loadClientes(): void {
    this.parceiroService.getAllClientes().subscribe((clientes: any) => {
      this.clientes = clientes.retorno;
    });

  }

  public loadFornecedores(): void {
    this.parceiroService.getAllFornecedores().subscribe((fornecedores: any) => {
      this.fornecedores = fornecedores.retorno;
    });
    this.fornecedoresOptions = this.controleFornecedores.valueChanges
    .pipe(
      startWith<string | EmpresaModel>(''),
      map(value => value ? typeof value == 'string' ? value : value.empresa : ''),
      map(value => value ? this.filtrarEmpresas(value, this.fornecedores) : this.fornecedores.slice())
    );
  }

  public loadGestores(): void {
    this.parceiroService.getAllFornecedores().subscribe((gestores: any) => {
      this.gestores = gestores.retorno;
    });
  }

  public loadAgrupamentos(event?: any): void {
    if (!event) {
      return;
    }
    this.agrupamentoService.consultar().subscribe((agrupamentos: any) => {
      this.agrupamentos = agrupamentos.retorno;
    });
  }

  public incluirAgrupamento(event?: any): void {
    ModalUtil.showModalBody(this.idModalAgrupamento);
  }

  public setIdAgrupamento(event?: any): void {
    this.idModalAgrupamento = event;
  }

  public salvar(): void {
    this.validateForm(this.formulario, () => {
      const form = this.getCloneForm(this.formulario);
      if (this.isNew()) {
        this.campanha = this.newCampanha(form);
        this.campanhaService.saveCampanha(this.campanha)
        .subscribe((result) => {
          this.campanha = result.retorno;
          this.submitId.emit(this.getClone(this.campanha._id));
          this.loadCampanha();
          this.setStatus();
          this.validateTabs();
          this.callbackSave(result);
          this.saveUser();
        });
      } else {
        if (this.currentCampanha.distribuidor.cnpj !== form.distribuidor.cnpj) {
          this.currentCampanha.produtos = [];
        }
        this.currentCampanha.senha_vacinacao = this.currentCampanha.senha_vacinacao ? this.currentCampanha.senha_vacinacao : null;
        this.campanha = this.unionObject(this.currentCampanha, form);
        this.campanhaService.updateCampanha(this.campanha)
        .subscribe((result) => {
          this.callbackSave(result);
        });
      }
    });
  }

  public callbackSave(result: any): void {
    this.notificationService.notifiy({
      message: Constants.MSG_SUCCESS_OPERATION
    });
    this.emitCurrentCampanha();
  }

  public saveUser(): void {
    if (!this.campanha._id) {
      return;
    }
    const usuario = this.loginService.getUsuario();
    const idCampanha = {campanha: this.campanha._id};
    if (!usuario.campanhas) {
      usuario.campanhas = [idCampanha]
    } else {
      usuario.campanhas.push(idCampanha);
    }
    this.usuarioService.addCampanha(usuario, (result: any) => {});
  }

  public edit(): void {
    if (this.idCampanha) {
      this.campanha._id = this.idCampanha;
      this.loadCampanha();
    }
  }

  public loadCampanha(): void {
    if (Util.isDefined(this.campanha._id)) {
      this.campanhaService.findCampanhaDB(this.campanha._id).subscribe((campanha) => {
        this.campanha = campanha.retorno;
        this.updateFormGroup();
        this.emitCurrentCampanha();
      });
    }
  }

  public updateFormGroup(): void {
    if (Util.isEmpty(this.campanha)) {
      return;
    }
    const formValue = this.instantiateForm(this.formulario, this.campanha);
    this.formulario.setValue(formValue);
    this.setDisableField();
  }

  public emitCurrentCampanha(): void {
    this.submitCurrent.emit(this.getClone(this.campanha));
  }

  public voltar(event?: any): void {
    this.router.navigate(['cadastros/campanha']);
  }

  public changeAutoRegistration(event?: any): void {
    if (event.value) {
      this.removeValidationForm(this.formulario, this.nameSenhaVacinacao);
    }
    this.setValueForm(this.formulario, this.nameSenhaVacinacao, null);
  }

  public getClientes(): EmpresaModel[] {
    return !Util.isEmpty(this.clientes) ? this.clientes : [];
  }

  public getFornecedores(): EmpresaModel[] {
    return !Util.isEmpty(this.fornecedores) ? this.fornecedores : [];
  }

  public getGestores(): EmpresaModel[] {
    return !Util.isEmpty(this.gestores) ? this.gestores : [];
  }

  public getAgrupamentos(): AgrupamentoModel[] {
    return !Util.isEmpty(this.agrupamentos) ? this.agrupamentos : [];
  }

  public isNew(): boolean {
    return !Util.isEmpty(this.campanha) ? Util.isEmpty(this.campanha._id) : true;
  }

  public setStatus(): void {
    if (this.isNew()) {
      this.status = StatusCampanha.Nova;
      this.statusDisabled = true;
    }
  }

  public validateTabs(): void {
    this.setDisableTabs(this.isNew());
    this.setDisableTabUnidade(Util.isEmpty(this.campanha.filiais));
  }

  public compareTo(objectA: any, objectB: any): boolean {
    return (objectA && objectB) ? objectA._id === objectB._id : false;
  }

  public setDisableTabs(disable: boolean): void {
    this.disableTabFilial.emit(disable);
    this.disableTabProduto.emit(disable);
    this.disableTabClinica.emit(disable);
  }

  public setDisableTabUnidade(disable: boolean): void {
    this.disableTabUnidade.emit(disable);
  }

  public getNao(): boolean {
    return SimNao.NAO.bool;
  }

  public newCampanha(formValue: any): CampanhaModel {
    const model: CampanhaModel = this.campanhaService.setNew();
    Object.keys(model).forEach(key => {
      const keysForm = Object.keys(formValue);
      for (let index = 0; index < keysForm.length; index++) {
        const keyForm = keysForm[index];
        if (key === keyForm) {
          model[key] = formValue[keyForm];
          break;
        } else {
          model[key] = null;
        }
      }
    });
    return model;
  }

  public isPassword(): boolean {
    return !this.getValueForm(this.formulario, this.nameCadastroAutomatico);
  }

  public setDisableField(event?: any): void {
    this.fieldDisabled = !this.isNew() && this.equals(this.getValueForm(this.formulario, this.nameStatus), StatusCampanha.Andamento);
  }

  public atualizaEmpresa(event: any, fornecedorId: string, control: string) {
    if (event.isUserInput) {
      this.formulario['controls'][control].setValue(event.source.value);
    }
  }

  public fechaEmpresa(event: any, control: FormControl) {
    if(this.formulario.value.fornecedor && typeof control.value == 'string') {
      this.formulario.value.fornecedor = null
      control.setValue(undefined)
    }
  }

  private filtrarEmpresas(value: string, empresas: EmpresaModel[]): EmpresaModel[] {
    return empresas.filter(empresa => empresa.empresa.toLowerCase().indexOf(value.toLowerCase()) === 0);
  }

  public mostrarEmpresa(empresa?: EmpresaModel): string | undefined {
    return empresa ? empresa.empresa : undefined;
  }

}
