import { Injectable } from '@angular/core';
import { IC_API } from '../../../../app.api';
import { HttpClient } from '@angular/common/http';
import { ProdutoModel } from '../../../../modulos/features/produtos/produto.model';
import { Observable } from 'rxjs';
import { NotificationService } from '../../../../modulos/servicos/notification.service';
import { Constants } from '../../../../utils/constants';
import { Util } from '../../../../utils/util';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  produtos: ProdutoModel[] = []

  constructor(
    private http: HttpClient,
    private notificationService: NotificationService
  ) { }

  consultar(sort?: string, order?: string, page?: number, size?: number): Observable<any> {
    let params: any = Util.addParams([
      {'sort': sort},
      {'order': order},
      {'page': page},
      {'size': size}
    ]);

    return this.http.get<any>(`${IC_API}/api/dados/produtos/retrieve`, { params })
      .do(data => this.produtos = data.retorno)
      .map(data => data);
  }

  getById(id: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_id': id}
    ]);
    this.http.get<any>(`${IC_API}/api/dados/produtos/findOne`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  getByFornecedor(id: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'fornecedor': id}
    ]);
    this.http.get<any>(`${IC_API}/api/dados/produtos/byFornecedor`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  save(produto: ProdutoModel, next?: (result: any) => void, showCallback?: boolean): void {
    showCallback = !Util.isDefined(showCallback) ? true : showCallback;
    this.http.post<ProdutoModel>(`${IC_API}/api/dados/produtos/create`, produto)
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, error.error.mensagem)}
    );
  }

  update(produto: ProdutoModel, next?: (result: any) => void, showCallback?: boolean): void {
    showCallback = !Util.isDefined(showCallback) ? true : showCallback;
    let params: any = Util.addParams([
      {'_id': produto._id}
    ]);
    this.http.patch<ProdutoModel>(`${IC_API}/api/dados/produtos/update`, produto, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, error.error.mensagem)}
    );
  }

  validarExclusao(_id: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_id': _id}
    ]);
    this.http.get<any>(`${IC_API}/api/dados/produtos/validarExclusao`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }

  delete(_id: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      {'_id': _id}
    ]);
    this.http.delete<ProdutoModel>(`${IC_API}/api/dados/produtos/delete`, { params })
      .subscribe((result) => {this.successCallback(result, next, showCallback)},
        error => {this.errorCallback(error, Constants.MSG_ERROR_RECOVER)}
    );
  }
  
  successCallback(result: any, next?: (result: any) => void, showCallback?: boolean): void {
    if (showCallback) {
      this.notificationService.notifiy({
        message: Constants.MSG_SUCCESS_OPERATION
      });
    }
    if (next) {
      next(result);
    }
  }

  errorCallback(error: any, message: string): void {
    this.notificationService.notifiy({
      message:  `${message}`,
      tipo: 'E',
    });
  }

}
