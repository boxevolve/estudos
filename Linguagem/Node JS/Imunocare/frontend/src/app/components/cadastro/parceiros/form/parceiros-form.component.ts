import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../utils/base.component';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmpresaModel } from '../../../../modulos/features/cadastros/empresa.model';
import { ParceiroService } from '../service/parceiros.service';
import { Util } from '../../../../utils/util';
import { EnderecoService } from '../../endereco/service/endereco.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'ic-parceiros-form',
  templateUrl: './parceiros-form.component.html',
  styleUrls: ['./parceiros-form.component.css']
})

export class ParceirosFormComponent extends BaseComponent implements OnInit {

  title: string;
  formulario: FormGroup;
  parceiro: EmpresaModel;

  razaoSocial = 'razao_social';
  cnpj = 'cnpj';
  tipo = 'tipo';
  responsavel = 'responsavel';
  email = 'email';
  telefone = 'telefone';
  cep = 'cep';
  numero = 'numero';
  complemento = 'complemento';
  endereco = 'endereco';
  bairro = 'bairro';
  cidade = 'cidade';
  estado = 'estado';

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private parceiroService: ParceiroService,
    private enderecoService: EnderecoService
  ) {
    super();
  }

  ngOnInit() {
    this.title = this.activatedRoute.snapshot.data['title'];
    this.parceiro = this.newParceiro({});
    this.createFormGroup();
    this.edit();
  }

  public edit() {
    this.activatedRoute.params.subscribe(params => {
      this.parceiro._id = params['id'];
      if (!Util.isEmpty(this.parceiro._id)) {
        this.parceiroService.getById(this.parceiro._id, result => {
          this.parceiro = result.retorno;
          this.updateFormGroup();
        })
      }
    });
  }

  public updateFormGroup(): void {
    this.formulario.setValue({
      id: this.parceiro._id,
      razao_social: this.parceiro.empresa,
      cnpj: this.parceiro.cnpj,
      tipo: this.parceiro.tipo,
      responsavel: this.parceiro.responsavel,
      email: this.parceiro.email,
      telefone: this.parceiro.fone,
      numero: this.parceiro.numero,
      complemento: this.parceiro.complemento,
      endereco: this.parceiro.endereco.endereco ? this.parceiro.endereco.endereco : null,
      cep: this.parceiro.endereco.cep ? this.parceiro.endereco.cep : null,
      bairro: this.parceiro.endereco.bairro ? this.parceiro.endereco.bairro : null,
      cidade: this.parceiro.endereco.cidade ? this.parceiro.endereco.cidade : null,
      estado: this.parceiro.endereco.estado ? this.parceiro.endereco.estado : null,
      enderecoId: this.parceiro.endereco._id ? this.parceiro.endereco._id : null
    });
  }

  public voltar(): void {
    this.router.navigate(['cadastros/parceiros']);
  }

  public getEnderecePorCep(): void {
    this.enderecoService.getByCep(this.formulario.value.cep, result => {
      this.mountObject();
      this.parceiro.endereco = result.retorno[0];
      if(!this.parceiro.endereco) {
        this.parceiro.endereco = { endereco: this.formulario.value.endereco,
                                   bairro: this.formulario.value.bairro,
                                   cidade: this.formulario.value.cidade,
                                   estado: this.formulario.value.estado,
                                   cep: this.formulario.value.cep }
      }
      this.updateFormGroup();
    })
  }

  public salvar(): void {
    if (this.formulario.invalid) {
      return;
    }
    this.mountObject();
    if (this.isnew()) {
      if (Util.isEmpty(this.parceiro.endereco._id)) {
        this.enderecoService.save(this.parceiro.endereco, (result) => {
          this.parceiro.endereco = result.retorno;
          this.parceiroService.save(this.parceiro, () => {
            this.voltar();
          });
        });
      } else {
        this.parceiroService.save(this.parceiro, () => {
          this.voltar();
        });
      }
    } else {
      if (Util.isEmpty(this.parceiro.endereco._id)) {
        this.enderecoService.save(this.parceiro.endereco, (result) => {
          this.parceiro.endereco = result.retorno;
          this.parceiroService.update(this.parceiro, () => {
            this.voltar();
          });
        });
      } else {
        this.parceiroService.update(this.parceiro, () => {
          this.voltar();
        });
      }
    }
  }

  public isnew(): boolean {
    return !Util.isEmpty(this.parceiro) ? Util.isEmpty(this.parceiro._id) : false;
  }

  public mountObject(): void {
    this.parceiro = this.newParceiro(this.formulario.value);
  }

  public newParceiro(formValue: any): EmpresaModel {
    return {
      _id: formValue.id,
      empresa: formValue.razao_social,
      cnpj: formValue.cnpj,
      tipo: formValue.tipo,
      responsavel: formValue.responsavel,
      email: formValue.email,
      fone: formValue.telefone,
      numero: formValue.numero,
      complemento: formValue.complemento,
      endereco: {
        _id: formValue.enderecoId,
        endereco: formValue.endereco,
        bairro: formValue.bairro,
        cidade: formValue.cidade,
        estado: formValue.estado,
        cep: formValue.cep
      }
    }
  }


  public createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      id: [null],
      razao_social: [null, Validators.required],
      cnpj: [null, Validators.required],
      tipo: [null, Validators.required],
      responsavel: [null, Validators.required],
      email: [null, Validators.required],
      telefone: [null, Validators.required],
      endereco: [null, Validators.required],
      cep: [null, Validators.required],
      numero: [null, Validators.required],
      complemento: [null],
      bairro: [null, Validators.required],
      cidade: [null, Validators.required],
      estado: [null, Validators.required],
      enderecoId: [null]
    });
  }

  public cpfcnpjmask = function (rawValue) {
    const numbers = rawValue.match(/\d/g);
    let numberLength = 0;
    if (numbers) {
      numberLength = numbers.join('').length;
    }
    if (numberLength <= 11) {
      return [/[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    } else {
      return [/[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/,
              /[0-9]/, '-', /[0-9]/, /[0-9]/];
    }
  }

}
