import { Component, OnInit, Input, SimpleChanges, OnChanges, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CampanhaModel, LoteModel } from '../../../../../modulos/features/campanhas/campanha.model';
import { CampanhaService } from '../../../../../modulos/features/campanhas/campanha.service';
import { BaseComponent } from '../../../../../utils/base.component';
import { ProdutoModel } from '../../../../../modulos/features/produtos/produto.model';
import { Util } from '../../../../../utils/util';
import { ProdutoService } from '../../../produto/service/produto.service';
import { SimNao } from '../../../../../shared/const/sim-nao.const';
import ModalUtil from '../../../../../utils/modal';
import { NotificationService } from '../../../../../modulos/servicos/notification.service';
import { Constants } from '../../../../../utils/constants';
import { Router } from '@angular/router';

@Component({
  selector: 'ic-produto-tab-form',
  templateUrl: './produto-tab-form.component.html',
  styleUrls: ['./produto-tab-form.component.css']
})
export class ProdutoTabFormComponent extends BaseComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() idCampanha: string;
  @Input() currentCampanha: any = {};

  @Output() submitCurrent = new EventEmitter();

  nameCurrentCampanha = 'currentCampanha';
  nameProduto = 'produto';

  formulario: FormGroup;
  campanha: CampanhaModel;

  produtos: ProdutoModel[] = [];
  lotes: LoteModel[] = [];

  idModalLote: string;
  refreshModalLote = false;
  loteSelected: any;

  dosagem = '';
  nuDose = '';
  meses = '';
  faixaMin = '';
  faixaMax = '';

  idProduto: any;

  // INICIO - Variáveis para tabela
  displayedColumns: string[] = ['lote_qtde', 'lote'];
  headers: string[] = ['Quantidade', 'Lote'];
  // FIM - Variáveis para tabela

  fieldProdutoDisabled: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private campanhaService: CampanhaService,
    private produtoService: ProdutoService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngAfterViewInit() {
    this.formulario.get(this.nameProduto).valueChanges.subscribe(produto => {
      this.loadReadOnlyByList(produto);
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameCurrentCampanha, (currentValue?: any) => {
      if (this.formulario) {
        this.formulario.reset();
      }
      this.lotes = [];
      this.dosagem = '';
      this.nuDose = '';
      this.meses = '';
      this.faixaMin = '';
      this.faixaMax = '';
      this.loadCampanha();
      this.loadProdutos();
    });
  }

  ngOnInit() {
    this.campanha = this.campanhaService.setNew();
    this.createFormGroup();
    this.loadCampanha();
    this.loadProdutos();
  }

  public createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      _id: [null],
      lotes: [null]
    });
  }

  public loadCampanha(callback?: () => void): void {
    if (this.idCampanha) {
      this.campanhaService.findCampanhaDB(this.idCampanha).subscribe((campanha) => {
        this.campanha = campanha.retorno;
        this.fieldProdutoDisabled = this.campanha.status == 'Andamento';
        if(this.campanha.produtos && this.campanha.produtos.length > 0){
          this.idProduto = this.campanha.produtos[0]._id;
        }
        if (callback) {
          callback();
        } else if (this.campanha.produtos && this.campanha.produtos.length) {
          this.updateFormGroup();
        }
      });
    }
  }

  public updateFormGroup(): void {
    if (Util.isEmpty(this.campanha)) {
      return;
    }
    const form = this.campanha.produtos[0];
    const formValue = this.instantiateForm(this.formulario, form);
    if (this.produtos.length) {
      this.loadReadOnlyByList(form.produto);
    }
    this.lotes = form.lotes;
    this.formulario.setValue(formValue);
  }

  public salvar(): void {
    if (!this.lotes.length) {
      this.notificationService.notifiy({
        message: Constants.MSG_SPE_REQUIRED_LOT,
        tipo: 'E',
      });
      return;
    }
    this.validateForm(this.formulario, () => {
      this.loadCampanha(() => {
        const form = this.getCloneForm(this.formulario);
        form.lotes = this.lotes;
        this.campanha.produtos = [];
        this.campanha.produtos.push(form);
        this.campanhaService.updateCampanha(this.campanha).subscribe((result) => {
          this.loadCampanha();
          this.callbackSave(result);
        });
      })
    });
  }

  public voltar(event?: any): void {
    this.router.navigate(['cadastros/campanha']);
  }

  public callbackSave(result: any): void {
    this.notificationService.notifiy({
      message: Constants.MSG_SUCCESS_OPERATION
    });
    this.emitCurrentCampanha();
  }

  public emitCurrentCampanha(): void {
    this.submitCurrent.emit(this.getClone(this.campanha));
  }

  public loadProdutos(): void {
    if (this.currentCampanha && this.currentCampanha.distribuidor) {
      this.produtoService.getByFornecedor(this.currentCampanha.distribuidor._id, (produtos) => {
        this.produtos = produtos.retorno;
        const produto = this.getValueForm(this.formulario, this.nameProduto);
        if (produto) {
          this.loadReadOnlyByList(produto);
        }
      });
    }
  }

  public incluirLote(event?: any): void {
    if (this.idProduto) {
      this.loteSelected = null;
      this.showModalLote();
    } else {
      this.notificationService.notifiy({
        message: Constants.MSG_SPE_REQUIRED_PROD,
        tipo: 'E',
      });
    }
  }

  public setIdLote(event?: any): void {
    this.idModalLote = event;
  }

  public receiveLote(lote: any): void {
    this.lotes = this.getClone(this.lotes);
    if (lote.old) {
      const index = this.find(this.lotes, lote.old);
      if (index !== -1) {
        this.lotes[index] = lote.new;
      }
    } else {
      this.lotes.push(lote.new);
    }
  }

  public showModalLote(): void {
    ModalUtil.showModalBody(this.idModalLote);
    this.refreshModalLote = true;
  }

  // INICIO - Métodos para preencher a tabela
  public mappingDataToTable(data: any[]): any[] {
    if (Util.isEmpty(data)) {
      return [];
    }
    let table: any[] = []
    for (let linha of data) {
      let tab: any = this.getClone(linha);
      tab._id = linha._id;
      tab.lote = linha.lote;
      tab.lote_qtde = linha.lote_qtde;
      table.push(tab)
    }
    return table
  }

  public editLote(lote: any): void {
    this.loteSelected = lote;
    this.showModalLote();
  }

  public deleteLote(lote: any): void {
    this.lotes = this.getClone(this.lotes);
    const index = this.find(this.lotes, lote);
    if (index !== -1) {
      this.lotes.splice(index, 1);
    }
  }
  // FIM - Métodos para preencher a tabela

  loadReadOnlyByList(produto: any): void {
    if (!produto) {
      return;
    }
    const indexProduto = this.find(this.produtos, null, (x: any) => {
      return this.equals(x._id, produto._id);
    });
    if (indexProduto !== -1) {
      this.loadReadOnly(this.produtos[indexProduto]);
    }
  }

  public loadReadOnly(produto: any): void {
    if (produto) {
      this.dosagem = produto.dosagem ? SimNao.getTextByValue(produto.dosagem) : '';
      this.faixaMin = produto.faixa_min ? produto.faixa_min : '';
      this.faixaMax = produto.faixa_max ? produto.faixa_max : '';

      if (produto.fornecedores
        && produto.fornecedores.length
        && produto.fornecedores[0].periodos
        && produto.fornecedores[0].periodos.length) {
        const periodo = produto.fornecedores[0].periodos[0];
        this.nuDose = periodo.ordem;
        this.meses = periodo.periodo_meses;
      } else {
        this.nuDose = '';
        this.meses = '';
      }
    }
  }

  public disabledButton(): boolean {
    return Util.isEmpty(this.getValueForm(this.formulario, this.nameProduto));
  }

  public getProdutos(): ProdutoModel[] {
    return !Util.isEmpty(this.produtos) ? this.produtos : [];
  }

  public getNameCampanha(): string {
    return this.currentCampanha ? this.currentCampanha.campanha : '';
  }

  public getControleDosagem(): string {
    const produto = this.getValueForm(this.formulario, this.nameProduto);
    return produto ? (produto.dosagem ? SimNao.getTextByValue(produto.dosagem) : '') : '';
  }

  public compareTo(objectA: any, objectB: any): boolean {
    return (objectA && objectB) ? objectA._id === objectB._id : false;
  }

  public clearLote(event: any) {
    this.idProduto = event.value;
    this.lotes = [];
  }

}
