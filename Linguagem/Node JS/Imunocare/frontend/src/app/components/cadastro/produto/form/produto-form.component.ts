import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { BaseComponent } from '../../../../utils/base.component';
import { EmpresaModel } from '../../../../modulos/features/cadastros/empresa.model';
import { Util } from '../../../../utils/util';
import { ProdutoModel } from '../../../../modulos/features/produtos/produto.model';
import { ProdutoService } from '../service/produto.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ParceiroService } from '../../parceiros/service/parceiros.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

export interface User {
  name: string;
}

@Component({
  selector: 'ic-produto-form',
  templateUrl: './produto-form.component.html',
  styleUrls: ['./produto-form.component.css']
})
export class ProdutoFormComponent extends BaseComponent implements OnInit {

  title: string;
  nameDescricao = 'produto';
  nameFornecedor = 'fornecedor';
  nameDosagem = 'dosagem';
  nameDose = 'ordem';
  nameMeses = 'periodo_meses';
  nameFaixaMin = 'faixa_min';
  nameFaixaMax = 'faixa_max';

  formulario: FormGroup;
  tipoEmpresa = 'Fornecedores';
  fornecedores: EmpresaModel[] = [];
  produto: ProdutoModel;
  controleFornecedores = new FormControl(null, Validators.required)
  fornecedoresOptions: Observable<EmpresaModel[]>

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private parceiroService: ParceiroService,
    private produtoService: ProdutoService,
  ) {
    super();
  }

  ngOnInit() {
    this.title = this.activateRoute.snapshot.data['title'];
    this.produto = this.newProduto({});
    this.loadFornecedores();
    this.createFormGroup();
    this.edit();
  }

  public loadFornecedores(): void {
    this.parceiroService.getAllFornecedores().subscribe((fornecedores: any) => {
      this.fornecedores = fornecedores.retorno;
    });
    this.fornecedoresOptions = this.controleFornecedores.valueChanges
    .pipe(
      startWith<string | EmpresaModel>(''),
      map(value => value ? typeof value == 'string' ? value : value.empresa : ''),
      map(value => value ? this.filtrarFornecedores(value) : this.fornecedores.slice())
    );
  }

  public createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      id: [null],
      produto: [null, Validators.required],
      dosagem: [null, Validators.required],
      ordem: [null, Validators.required],
      periodo_meses: [null, Validators.required],
      faixa_min: [null, Validators.required],
      faixa_max: [null, Validators.required]
    });
    this.formulario.addControl('fornecedor', this.controleFornecedores)
  }

  public updateFormGroup(): void {
    if (Util.isEmpty(this.produto)) {
      return;
    }
    let fornecedor;
    let periodo;
    this.produto.fornecedores.forEach(itemFornecedor => {
      fornecedor = itemFornecedor.fornecedor;
      itemFornecedor.periodos.forEach(itemPeriodo => {
        periodo = itemPeriodo;
      });
    });

    const formValue = this.instantiateObject({
      id: this.produto._id,
      produto: this.produto.produto,
      fornecedor: fornecedor,
      dosagem: this.produto.dosagem,
      ordem: periodo.ordem,
      periodo_meses: periodo.periodo_meses,
      faixa_min: this.produto.faixa_min,
      faixa_max: this.produto.faixa_max
    });

    this.formulario.setValue(formValue);
  }

  public edit(): void {
    this.activateRoute.params.subscribe(parametrosUrl => {
      this.produto._id = parametrosUrl['id'];
      if (Util.isDefined(this.produto._id)) {
        this.produtoService.getById(this.produto._id, result => {
          this.produto = result.retorno;
          this.updateFormGroup();
        })
      }
    });
  }

  public isNew(): boolean {
    return !Util.isEmpty(this.produto) ? Util.isEmpty(this.produto._id) : false;
  }

  public salvar(): void {
    this.mountObject();
    if (this.formulario.invalid) {
      return;
    }
    if (this.isNew()) {
      this.produtoService.save(this.produto, (result) => {
        this.voltar();
      });
    } else {
      this.produtoService.update(this.produto, (result) => {
        this.voltar();
      });
    }
  }

  public mountObject(): void {
    this.produto = this.newProduto(this.getFormValue());
  }

  public getFormValue(): any {
    this.fechaFornecedores(undefined);
    const formValue = this.getClone(this.formulario.value);

    if (!Util.isEmpty(this.produto._id)) {
      this.produto.fornecedores.forEach(fornecedor => {
        formValue.id_fornecedor = fornecedor._id;
        fornecedor.periodos.forEach(periodo => {
          formValue.id_periodo = periodo._id;
        });
      });
    }
    return formValue;
  }

  public newProduto(formValue: any): ProdutoModel {
    return {
      _id: formValue.id,
      produto: formValue.produto,
      fornecedores: [ {
        fornecedor: formValue.fornecedor,
        periodos: [
          {
            _id: formValue.id_periodo,
            ordem: formValue.ordem,
            periodo_meses: formValue.periodo_meses
          }
        ],
        _id: formValue.id_fornecedor
      }],
      dosagem: formValue.dosagem,
      faixa_min: formValue.faixa_min,
      faixa_max: formValue.faixa_max
    }
  }

  public getFaixaMin(): number {
    return this.getValueForm(this.formulario, this.nameFaixaMin);
  }

  public getFaixaMax(): number {
    return this.getValueForm(this.formulario, this.nameFaixaMax);
  }

  public getFornecedores(): EmpresaModel[] {
    return !Util.isEmpty(this.fornecedores) ? this.fornecedores : [];
  }

  public getFornecedoresOptions(): Observable<EmpresaModel[]> {
    return this.fornecedoresOptions;
  }

  public voltar(): void {
    this.router.navigate(['cadastros/produtos']);
  }

  public compareToFornecedor(objectA: any, objectB: any): boolean {
    return (objectA && objectB) ? objectA._id === objectB._id : false;
  }

  private filtrarFornecedores(value: string): EmpresaModel[] {
    return this.fornecedores.filter(empresa => empresa.empresa.toLowerCase().indexOf(value.toLowerCase()) === 0);
  }

  public mostrarFornecedores(empresa?: EmpresaModel): string | undefined {
    return empresa ? empresa.empresa : undefined;
  }

  public atualizaFornecedor(event: any, fornecedorId: string, control: string) {
    if (event.isUserInput && control === 'fornecedor') {
      this.formulario['controls'][control].setValue(event.source.value);
    }
  }

  public fechaFornecedores(event: any) {
    if(this.formulario.value.fornecedor && typeof this.controleFornecedores.value == 'string') {
      this.formulario.value.fornecedor = null
      this.controleFornecedores.setValue(undefined)
    }
  }

  public alterarValidacaoCamposDosagem(event: any, value: boolean) {
    if (event.isUserInput) {
      if(value){
        this.adicionarValidacaoCamposDosagem()
      }else {
        this.removerValidacaoCamposDosagem()
      }
    }
  }

  private removerValidacaoCamposDosagem() {
    this.removeValidators(this.formulario.get(this.nameDose))
    this.removeValidators(this.formulario.get(this.nameMeses))
    this.removeValidators(this.formulario.get(this.nameFaixaMin))
    this.removeValidators(this.formulario.get(this.nameFaixaMax))
  }

  private adicionarValidacaoCamposDosagem() {
    this.setValidator(this.formulario.get(this.nameDose), [Validators.required])
    this.setValidator(this.formulario.get(this.nameMeses), [Validators.required])
    this.setValidator(this.formulario.get(this.nameFaixaMin), [Validators.required])
    this.setValidator(this.formulario.get(this.nameFaixaMax), [Validators.required])
  }

  private setValidator(component: AbstractControl, validators: ValidatorFn[]) {
    component.setValidators(validators);
    component.updateValueAndValidity();
  }

  private removeValidators(component: AbstractControl) {
    component.clearValidators();
    component.updateValueAndValidity();
  }
}
