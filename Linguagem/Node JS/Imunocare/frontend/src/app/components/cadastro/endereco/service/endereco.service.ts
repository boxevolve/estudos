import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../../modulos/servicos/notification.service';
import { EnderecoModel } from '../../../../modulos/features/cadastros/endereco.model';
import { Util } from '../../../../utils/util';
import { IC_API } from '../../../../app.api';
import { Constants } from '../../../../utils/constants';
import { BaseComponent } from '../../../../utils/base.component';

@Injectable({
  providedIn: 'root'
})
export class EnderecoService extends BaseComponent {

  constructor(
    private http: HttpClient,
    private notificationService: NotificationService,
  ) {
    super();
  }

  public getByCep(cep: string, next?: (result: any) => void, showCallback?: boolean): void {
    let params: any = Util.addParams([
      { 'cep': cep }
    ]);
    this.http.get<any>(`${IC_API}/api/dados/enderecos/retrieve`, { params })
      .subscribe((result) => { this.successCallback(result, next, showCallback) },
        error => {
          this.errorCallback(error, Constants.MSG_ERROR_RECOVER)
        }
      )
  }

  public save(endereco: EnderecoModel, next?: (result: any) => void, showCallback?: boolean): void {
    showCallback = !Util.isDefined(showCallback) ? true : showCallback;
    this.http.post<EnderecoModel>(`${IC_API}/api/dados/enderecos/create`, endereco)
      .subscribe((result) => { this.successCallback(result, next, showCallback) },
        error => {
          this.errorCallback(error, Constants.MSG_ERROR_SAVE)
        }
      );
  }

  public getCep(cep: string, next?: (result: any) => void, errorNext?: (result: any) => void, showCallback?: boolean): void {
    cep = cep;
    let params: any = Util.addParams([
      { 'cep': cep }
    ]);
    if (!this.validateCep(cep)) {
      this.errorCallback(null, Constants.MSG_INCORRECT_INFORMATION, errorNext, showCallback);
    }
    this.http.get<any>(`${IC_API}/api/dados/enderecos/cep`, { params })
      .subscribe(
        (result) => {
          if (result.erro) {
            this.errorCallback(result, Constants.MSG_ERROR_RECOVER, errorNext, showCallback);
          } else {
            this.successCallback(result, next, showCallback);
          }
        },
        error => {
          this.errorCallback(error, Constants.MSG_ERROR_RECOVER, errorNext, showCallback)
        }
      );
  }

  public successCallback(result: any, next?: (result: any) => void, showCallback?: boolean): void {
    if (showCallback) {
      this.notificationService.notifiy({
        message: Constants.MSG_SUCCESS_OPERATION
      });
    }
    if (next) {
      next(result);
    }
  }

  public errorCallback(error: any, message: string, errorNext?: (result: any) => void, showCallback?: boolean): void {
    if (showCallback) {
      this.notificationService.notifiy({
        message:  `${message} o endereço.`,
        tipo: 'E',
      });
    }
    if (errorNext) {
      errorNext(error);
    }
  }

}
