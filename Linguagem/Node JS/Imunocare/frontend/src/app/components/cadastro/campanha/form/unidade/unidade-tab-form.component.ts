import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '../../../../../utils/base.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CampanhaModel, FilialCampanhaModel, UnidadeModel } from '../../../../../modulos/features/campanhas/campanha.model';
import { ConfigXlsxModel } from '../../../../../shared/model/config-xlsx.model';
import { Router } from '@angular/router';
import { NotificationService } from '../../../../../modulos/servicos/notification.service';
import { CampanhaService } from '../../../../../modulos/features/campanhas/campanha.service';
import { UnidadeXlsxModel } from '../../model/unidade-xlsx.model';
import { Constants } from '../../../../../utils/constants';
import { Util } from '../../../../../utils/util';
import { EnderecoService } from '../../../endereco/service/endereco.service';
import { EnderecoModel } from '../../../../../modulos/features/cadastros/endereco.model';
import DialogUtil from '../../../../../utils/dialog-util';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'ic-unidade-tab-form',
  templateUrl: './unidade-tab-form.component.html',
  styleUrls: ['./unidade-tab-form.component.css']
})
export class UnidadeTabFormComponent extends BaseComponent implements OnInit, OnChanges {

  @Input() idCampanha: string;
  @Input() currentCampanha: any = {};

  @Output() submitCurrent = new EventEmitter();

  nameCurrentCampanha = 'currentCampanha';
  nameUnidades = 'unidades';

  formulario: FormGroup;
  campanha: CampanhaModel;

  unidadesXlsx: UnidadeXlsxModel[] = [];
  removeList: UnidadeXlsxModel[] = [];

  // INICIO - Variáveis para tabela
  displayedColumns: string[] = ['cod_unid', 'nome_unid', 'cnpj', 'tipo', 'responsavel', 'fone'];
  headers: string[] = ['Código Unidade', 'Unidade', 'Filial Raiz', 'Tipo', 'Responsável', 'Telefone'];
  // FIM - Variáveis para tabela

  // INICIO - Variáveis para upload xlsx
  configXlsx: ConfigXlsxModel;

  templateXlsx: any[] = [
    {value: 'Texto', description: 'cnpj', null: ' - '         },
    {value: 'Texto', description: 'cod_unid', null: ' - '     },
    {value: 'Texto', description: 'nome_unid', null: ' - '    },
    {value: 'Texto', description: 'endereco', null: ' - '     },
    {value: 'Texto', description: 'bairro', null: ' - '       },
    {value: 'Texto', description: 'cidade', null: ' - '       },
    {value: 'Texto', description: 'estado', null: ' - '       },
    {value: 'Texto', description: 'cep', null: ' - '          },
    {value: 'Número', description: 'numero', null: '0'        },
    {value: 'Texto', description: 'complemento', null: ' - '  },
    {value: 'Número', description: 'previsao_doses', null: '0'}
  ];
  modelFromFileToObj: string[] = [
    'cnpj',
    'cod_unid',
    'nome_unid',
    'endereco',
    'bairro',
    'cidade',
    'estado',
    'cep',
    'numero',
    'complemento',
    'previsao_doses'
  ];
  // INICIO - Variáveis para upload xlsx

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private enderecoService: EnderecoService,
    private campanhaService: CampanhaService
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameCurrentCampanha, (currentValue?: any) => {
      this.campanha = currentValue;
      if (this.campanha) {
        const indexesRemove = [];
        this.unidadesXlsx.forEach((unidade, i) => {
          const index = this.find(this.campanha.filiais, null, (x: any) => {
            return this.equals(x.cnpj, unidade.cnpj);
          });
          if (index === -1) {
            indexesRemove.push(i);
          }
        });
        this.removeListUnidadeByIndex(this.unidadesXlsx, indexesRemove);
      }
    });
  }

  ngOnInit() {
    this.createConfigXlsx();
    this.createFormGroup();
    this.loadCampanha();
  }

  public createConfigXlsx(): void {
    this.configXlsx = new ConfigXlsxModel(this, this.validateUpload, this.templateXlsx, this.modelFromFileToObj, null, 1, 'cod_unid',
//    'Código Unidade', false);
    'Código Unidade', false, true, 'Template Importação Unidades.xlsx', 'templates');
  }

  public validateUpload(data: any, saveList?: any[], self?: any): string {
    let error = '';
    for (let index = 1; index < data.length; index++) {
      if (data[index][6].length > 2) {
        error += `A linha [${index}] da coluna [${self.templateXlsx[6].description}] deve ter no máximo 2 dígitos\n `
      }
    }
    if (!error) {
      for (let index = 1; index < data.length; index++) {
        const cnpj = data[index][0];
        if (!self.validateCnpj(cnpj)) {
          error += `CPNJ [${cnpj}] inválido na linha [${index}]\n `
        }
      }
    }
    if (!error) {
      for (let index = 1; index < data.length; index++) {
        const cnpj = data[index][0];
        if (!self.contains(self.campanha.filiais, null, (x: any) => {
              return self.equals(x.cnpj, cnpj);
            })
        ) {
          error += `A campanha não possui uma filial com o mesmo CPNJ [${cnpj}] da Unidade na linha [${index}]\n `
        }
      }
    }
    return error;
  }

  public createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      unidades: [[]]
    });
  }

  public loadCampanha(callback?: () => void): void {
    if (this.idCampanha) {
      this.campanhaService.findCampanhaDB(this.idCampanha).subscribe((data) => {
        this.campanha = data.retorno;
        if (callback) {
          callback();
        } else if (this.campanha.filiais && this.campanha.filiais.length) {
          this.setValueForm(this.formulario, this.nameUnidades, []);
          this.unidadesXlsx = [];
          this.campanha.filiais.forEach(filial => {
            filial.unidades.forEach(unidade => {
              this.unidadesXlsx.push(this.getNewUnidadeXlsx(filial, unidade));
            });
          });
        }
      });
    }
  }

  public salvar(): void {
    const isNewList = this.getValueForm(this.formulario, this.nameUnidades).length;
    const isRemoveList = this.removeList.length;
    if (!isNewList && !isRemoveList) {
      this.notificationService.notifiy({
        message:  Constants.MSG_SPE_REQUIRED_UNIT,
        tipo: 'E',
      });
      return;
    }
    this.validateForm(this.formulario, () => {
      this.loadCampanha(() => {
        this.removeSaveUnidade(isNewList);
      })
    });
  }

  public removeSaveUnidade(isNewList: boolean): void {
    const indexMatriz = [];
    this.removeList.forEach(removeItem => {
      this.campanha.filiais.forEach((filial, i) => {
        let index = -1;
        for (let j = 0; j < filial.unidades.length; j++) {
          const unidade = filial.unidades[j];
          if (this.equals(unidade.cod_unid, removeItem.cod_unid)) {
            index = j;
            break;
          }
        }
        if (index !== -1) {
          indexMatriz.push({line: i, column: index});
        }
      });
    });

    indexMatriz.forEach(indexRemove => {
      this.campanha.filiais[indexRemove.line].unidades = this.remove(this.campanha.filiais[indexRemove.line].unidades, indexRemove.column);
    });

    this.removeList = [];
    if (isNewList) {
      const form = this.getCloneForm(this.formulario);
      this.generateUnidade(form.unidades);
    } else {
      this.finishSave();
    }
  }

  public finishSave(): void {
    this.campanhaService.updateCampanha(this.campanha).subscribe((result) => {
      this.loadCampanha();
      this.callbackSave(result);
    });
  }

  public callbackSave(result: any): void {
    this.notificationService.notifiy({
      message: Constants.MSG_SUCCESS_OPERATION
    });
    this.emitCurrentCampanha();
  }

  public emitCurrentCampanha(): void {
    this.submitCurrent.emit(this.getClone(this.campanha));
  }

  public voltar(event?: any): void {
    this.router.navigate(['cadastros/campanha']);
  }

  public receiveData(data: any): void {
    const rows = data.length;
    let validateRows = 0;
    let invalidadeRows = 0;
    let errorMsg = '';

    data.forEach((item, index) => {
      item.cep = this.getCepOnly(item.cep);
      //this.enderecoService.getCep(item.cep, (result: any) => {
        validateRows++;
        if (validateRows === rows) {
          this.finishReceiveData(data);
        } else if ((validateRows + invalidadeRows) === rows) {
          this.invalidCep(errorMsg);
        }
      // }, (error?: any) => {
      //   invalidadeRows++;
      //   errorMsg += `A linha [${index}] possui CEP ${item.cep} inválido\n `;
      //   if (validateRows === rows
      //   || (validateRows + invalidadeRows) === rows) {
      //     this.invalidCep(errorMsg);
      //   }
      // }, false);
    });
  }

  public invalidCep(message: string): void {
    DialogUtil.openBaseDialog(this.dialog, `${Constants.MSG_SPE_TITLE_ALERT} ${Constants.MSG_SPE_FILE_INCONSISTENCY}`, message, 'Ok');
  }

  public finishReceiveData(data: any): void {
    data.forEach(unidadeXlsx => {
      unidadeXlsx = this.getNewUnidadeXlsxByList(unidadeXlsx);
    });

    this.unidadesXlsx = this.getClone(this.unidadesXlsx);
    this.addAll(this.unidadesXlsx, data);
    const formList = this.getValueForm(this.formulario, this.nameUnidades);
    this.addAll(formList, data);
    this.setValueForm(this.formulario, this.nameUnidades, formList);
  }

  public getNameCampanha(): string {
    return this.currentCampanha ? this.currentCampanha.campanha : '';
  }

  public mappingDataToTable(data: any[]): any[] {
    if (Util.isEmpty(data)) {
      return [];
    }
    let table: any[] = data;
    /**
     * Mock de tipo
     */
    table.forEach(row => {
      row.tipo = 'Unidade';
    });
    return table
  }

  public removeListUnidadeByIndex(listUnidade: any[], indexes: number[]): void {
    indexes.forEach(index => {
      this.removeUnidade(listUnidade[index], true);
    });
  }

  public removeUnidade(unidade?: any, realized?: boolean): void {
    this.unidadesXlsx = this.remove(this.unidadesXlsx, null, null, (x: any) => {
      return this.equals(x.cod_unid, unidade.cod_unid);
    });
    this.setValueForm(this.formulario, this.nameUnidades,
      this.remove(this.getValueForm(this.formulario, this.nameUnidades), null, null, (x: any) => {
        return this.equals(x.cod_unid, unidade.cod_unid);
      })
    );
    if (!realized) {
      this.removeList.push(unidade);
    }
  }

  public getNewUnidadeXlsx(filial: FilialCampanhaModel, unidade: UnidadeModel): UnidadeXlsxModel {
    const endereco: any = unidade.endereco ? unidade.endereco : {};
    return new UnidadeXlsxModel(
      filial.cnpj,
      unidade.cod_unid,
      unidade.nome_unid,
      endereco.endereco,
      endereco.bairro,
      endereco.cidade,
      endereco.estado,
      endereco.cep,
      unidade.numero,
      unidade.complemento,
      unidade.previsao_doses,
      filial.responsavel,
      filial.fone
    );
  }

  public getNewUnidadeXlsxByList(unidadeXlsx: any): UnidadeXlsxModel {
    const index = this.find(this.campanha.filiais, null, (x?: any) => {
      return this.equals(x.cnpj, unidadeXlsx.cnpj);
    });
    if (index === -1) {
      return null;
    }
    const filial = this.campanha.filiais[index];
    unidadeXlsx.responsavel = filial.responsavel;
    unidadeXlsx.fone = filial.fone;
    return unidadeXlsx;
  }

  public getNewEnderecoByXlsx(unidade: any): EnderecoModel {
    return {
      endereco: unidade.endereco,
      bairro: unidade.bairro,
      cidade: unidade.cidade,
      estado: unidade.estado,
      cep: unidade.cep
    };
  }

  public generateUnidade(unidadesXlsx: any[]): void {
    const saveAddress: any = {count: 0};
    const addressLength = unidadesXlsx.length;

    unidadesXlsx.forEach((unidadeXlsx) => {
      const unidade: any = {};
      unidade.cod_unid = unidadeXlsx.cod_unid;
      unidade.nome_unid = unidadeXlsx.nome_unid;
      unidade.numero = unidadeXlsx.numero;
      unidade.complemento = unidadeXlsx.complemento;
      unidade.previsao_doses = unidadeXlsx.previsao_doses;
      this.enderecoService.save(this.getNewEnderecoByXlsx(unidadeXlsx), (result: any) => {
        saveAddress.count++;
        unidade.endereco = result.retorno;
        if (saveAddress.count === addressLength) {
          this.finishSave();
        }
      });

      const index = this.find(this.campanha.filiais, null, (x?: any) => {
        return this.equals(x.cnpj, unidadeXlsx.cnpj);
      });
      if (index !== -1) {
        this.campanha.filiais[index].unidades.push(unidade);
      }
    });
  }

}
