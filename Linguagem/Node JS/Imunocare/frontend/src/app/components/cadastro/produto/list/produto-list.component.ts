import { Component, OnInit } from '@angular/core';
import { ProdutoTableModel } from '../../../../modulos/features/produtos/produto.model';
import { ProdutoService } from '../service/produto.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SimNao } from '../../../../shared/const/sim-nao.const';
import { Util } from '../../../../utils/util';
import { Observable } from 'rxjs';
import { Constants } from '../../../../../app/utils/constants';
import { NotificationService } from '../../../../modulos/servicos/notification.service';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import DialogUtil from '../../../../utils/dialog-util';

@Component({
  selector: 'ic-produto-list',
  templateUrl: './produto-list.component.html',
  styleUrls: ['./produto-list.component.css']
})
export class ProdutoListComponent implements OnInit {

  title: string;

  // INICIO - Variáveis para tabela
  displayedColumns: string[] = ['descricao', 'fornecedor', 'controle_dosagem'];
  headers: string[] = ['Descrição', 'Fornecedor', 'Controle Dosagem'];
  errorMessage = 'Erro ao acessar dados dos produtos';
  // FIM - Variáveis para tabela

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private produtoService: ProdutoService,
    private notificationService: NotificationService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.title = this.activateRoute.snapshot.data['title'];
  }

  public consultar(paginator: MatPaginator, sort: MatSort, pageSize: number): Observable<any> {
    return this.produtoService.consultar(
      sort.active,
      sort.direction,
      paginator.pageIndex,
      paginator.pageSize || pageSize
    );
  }

  // INICIO - Métodos para preencher a tabela
  public consultarObservable(self: any, paginator?: MatPaginator, sort?: MatSort, pageSize?: number): Observable<any> {
    return self.consultar(paginator, sort, pageSize);
  }

  public mappingDataToTable(data: any[]): any[] {
    if (Util.isEmpty(data)) {
      return [];
    }
    let table: ProdutoTableModel[] = []
    for (let linha of data) {
      let tab: ProdutoTableModel = {_id: '', descricao: '', fornecedor: '', controle_dosagem: '' };
      tab._id = linha._id;
      tab.descricao = linha.produto;
      tab.fornecedor = linha.fornecedores[0].fornecedor ? linha.fornecedores[0].fornecedor.empresa : '';
      tab.controle_dosagem = SimNao.getTextByValue(linha.dosagem);
      table.push(tab)
    }
    return table
  }

  public rowClicked(row: any): void {
    this.router.navigate(['edit', row._id], { relativeTo: this.activateRoute });
  }
  // FIM - Métodos para preencher a tabela

  public incluir(): void {
    this.router.navigate(['add'], { relativeTo: this.activateRoute });
  }

  public excluir(produto): void {
    this.produtoService.validarExclusao(produto._id, (result) => {
      if(!(result.contagem > 0)) {
        this.produtoService.delete(produto._id, (result) => {
          this.refreshTable()
        });
      } else {
        this.notificationService.notifiy({
          message: Constants.MSG_VALIDA_EXC_PRODUTO,
          tipo: 'E',
        });
      }
    });
  }

  refreshList: boolean;

  public refreshTable(): void {
    this.refreshList = !this.refreshList;
  }

}
