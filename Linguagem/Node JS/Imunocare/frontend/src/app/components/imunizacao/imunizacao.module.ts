import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { ModalElegivelComponent } from '../../shared/components/modal/modal-elegivel/modal-elegivel.component';
import { SharedModule } from '../../modulos/shared/shared.module';
import { VacinacaoComponent } from './vacinacao/vacinacao/form/vacinacao.component';
import { VacinacaoRegistroComponent } from './vacinacao/vacinacao-registro/list/vacinacao-registro.component';
import { VacinacaoElegiveisComponent } from './vacinacao/vacinacao-elegiveis/list/vacinacao-elegiveis.component';
import { RouterGuard } from '../../modulos/servicos/router.guard';
import { ForaElegibilidadeListComponent } from './fora-elegibilidade/list/fora-elegibilidade-list.component';
import { ImunizacaoComponent } from './imunizacao/imunizacao.component';

const ROUTES: Routes = [{
  path: '',
  component: ImunizacaoComponent,
  children: [
    {
      path: 'vacinacao',
      children: [
        {
          path: '', component: VacinacaoComponent,
          data: {
            title: 'Vacinação'
          },
          canActivate: [RouterGuard], canLoad: [RouterGuard]
        }
      ]
    },
    {
      path: 'fora-elegibilidade',
      children: [
        {
          path: '', component: ForaElegibilidadeListComponent,
          data: {
            title: 'Fora de Elegibilidade'
          },
          canActivate: [RouterGuard], canLoad: [RouterGuard]
        }
      ]
    }
  ]
}]

@NgModule({
  declarations: [
    ImunizacaoComponent,
    VacinacaoComponent,
    VacinacaoElegiveisComponent,
    VacinacaoRegistroComponent,
    ForaElegibilidadeListComponent,
    ModalElegivelComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTES)
  ]
})

export class ImunizacaoModule {
  public constructor() {}
}
