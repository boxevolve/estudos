import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/switchMap'

import { ElegivelModel } from '../../../../../modulos/features/elegiveis/elegivel.model'
import { ElegivelService } from '../../../../../modulos/features/elegiveis/elegivel.service'
import { NotificationService } from '../../../../../modulos/servicos/notification.service'
import { LoginService } from '../../../../../modulos/servicos/login.service';
import { IC_API } from '../../../../../app.api';
import { ListCampanhaProdutos } from '../model/listCampanhaProd.model';
import { ImunizacaoAPIModel } from '../model/imunizacao.model';

@Injectable()
export class ImunizacaoService {

  produto: ListCampanhaProdutos
  elegivel: ElegivelModel
  imunizacao: ImunizacaoAPIModel
  ImunizacaoLiberada: boolean = false

  public constructor(
    private elegivelService: ElegivelService,
    private notificationService: NotificationService,
    private loginService: LoginService,
    private http: HttpClient
  ) { }

  reset() {
    this.produto = undefined
    this.elegivel = undefined
    this.imunizacao = undefined
    this.ImunizacaoLiberada = false
  }

  prepararImunizacao(eleg: ElegivelModel): boolean {
    this.imunizacao = { elegivel_id: '', imunizacao_id: '', usuario: '' }
    this.ImunizacaoLiberada = false
    if (!eleg) {
      this.notificationService.notifiy({message: 'Erro ao preparar elegível para Imunização', tipo: 'E'})
    } else {
      if (eleg.aplicacao) {
        this.notificationService.notifiy({message: `O elegível ${eleg.nome} já foi imunizado contra ${this.produto.produto}`})
      } else {
        this.elegivel = eleg
        this.imunizacao.elegivel_id = eleg._id
        this.ImunizacaoLiberada = true
        this.notificationService.notifiy({message: `O elegível ${eleg.nome} pode ser imunizado contra ${this.produto.produto}`})
        return true
      }
    }
    return false
  }

  getDadosElegivel(): ElegivelModel {
    if (this.ImunizacaoLiberada === true) {
      return this.elegivel
    }
  }

  enviarImunizacaoParaRegistro(elegivel_id: string, imunizacao_id: string): Observable<any> {
    let params: HttpParams = undefined;
    params = new HttpParams().append('_id', elegivel_id)

    console.log(`NO SERVICO
              \nELegivel: ${this.imunizacao.elegivel_id}
              \nImunizacao: ${imunizacao_id}`);
    return this.http.post<any>(`${IC_API}/api/dados/elegiveis/registraAplicacao`, { _id: imunizacao_id }, { params: params} );
  }

}
