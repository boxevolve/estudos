
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CampanhaModel } from '../../../../modulos/features/campanhas/campanha.model';
import { CampanhaService } from '../../../../modulos/features/campanhas/campanha.service';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { Util } from '../../../../utils/util';
import { BaseComponent } from '../../../../utils/base.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ElegivelService } from '../../../../modulos/features/elegiveis/elegivel.service';
import { SimNao } from '../../../../shared/const/sim-nao.const';
import DialogUtil from '../../../../utils/dialog-util';

@Component({
  selector: 'ic-fora-elegibilidade-list',
  templateUrl: './fora-elegibilidade-list.component.html',
  styleUrls: ['./fora-elegibilidade-list.component.css']
})
export class ForaElegibilidadeListComponent extends BaseComponent implements OnInit {

  title: string;

  // INICIO - Variáveis para tabela
  displayedColumns: string[] = ['cpf', 'matricula', 'nome', 'dependente', 'cnpj'];
  headers: string[] = ['CPF', 'Matrícula', 'Nome', 'Dependente', 'Filial'];
  errorMessage = 'Erro ao acessar dados dos elegíveis';
  // FIM - Variáveis para tabela

  nameCampanha = 'campanha'

  formulario: FormGroup;
  campanhas: CampanhaModel[] = [];
  refreshList: boolean;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private activateRoute: ActivatedRoute,
    private campanhaService: CampanhaService,
    private elegivelService: ElegivelService
  ) {
    super();
  }

  ngOnInit() {
    this.title = this.activateRoute.snapshot.data['title'];
    this.loadCombos();
    this.createFormGroup();
  }

  public createFormGroup(): void {
    this.formulario = this.formBuilder.group({});
  }

  public loadCombos(): void {
    this.loadCampanhas();
  }

  public loadCampanhas(): void {
    this.campanhaService.consultarNotCadAuto(null, null, null, null, true).subscribe((result: any) => {
      this.campanhas = result.retorno;
    });
  }

  public consultar(paginator: MatPaginator, sort: MatSort, pageSize: number): Observable<any> {
    return this.elegivelService.consultarByCampanha(
      this.getValueForm(this.formulario, this.nameCampanha),
      true,
      sort.active,
      sort.direction,
      paginator.pageIndex,
      paginator.pageSize || pageSize
    );
  }

  // INICIO - Métodos para preencher a tabela
  public consultarObservable(self: any, paginator?: MatPaginator, sort?: MatSort, pageSize?: number): Observable<any> {
    return self.consultar(paginator, sort, pageSize);
  }

  public mappingDataToTable(data: any[]): any[] {
    if (Util.isEmpty(data)) {
      return [];
    }
    data.forEach(row => {
      row.cpf = this.formatCPF(row.cpf);
      row.dependente = SimNao.getTextByBool(row.dependente);
    });
    return data;
  }

  public buttonClicked(row: any): void {
    DialogUtil.openBaseDialog(this.dialog, 'Confirmar', 'Está ciente do elegível?',
      'Sim', 'Não', (result: any) => {
        if (result) {
          this.elegivelService.aprovarElegivel(row._id, (result?: any) => {
            this.refreshTable();
          });
        }
    });
  }
  // FIM - Métodos para preencher a tabela

  public getCampanhas(): CampanhaModel[] {
    return !Util.isEmpty(this.campanhas) ? this.campanhas : [];
  }

  public refreshTable(): void {
    this.refreshList = !this.refreshList;
  }

}
