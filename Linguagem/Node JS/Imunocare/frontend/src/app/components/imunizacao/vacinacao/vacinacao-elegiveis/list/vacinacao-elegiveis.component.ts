

import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core'
import { CampanhaService } from '../../../../../modulos/features/campanhas/campanha.service'
import { CampanhaModel } from '../../../../../modulos/features/campanhas/campanha.model'
import { ElegivelService } from '../../../../../modulos/features/elegiveis/elegivel.service'
import { ElegivelModel } from '../../../../../modulos/features/elegiveis/elegivel.model';
import { NotificationService } from '../../../../../modulos/servicos/notification.service';
import { ImunizacaoService } from '../../vacinacao/service/imunizacao.service';

@Component({
    selector: 'ic-vacinacao-elegiveis',
    templateUrl: './vacinacao-elegiveis.component.html'
})

export class VacinacaoElegiveisComponent implements OnInit {

  campanha: CampanhaModel
  @Input() elegiveis: ElegivelModel[]
  @Input() elegivel: ElegivelModel
  @Output() imunizar = new EventEmitter()

  constructor(
    private campanhaService: CampanhaService,
    private elegivelService: ElegivelService,
    private notificationService: NotificationService,
    private imunizacaoService: ImunizacaoService
  ) { }

  ngOnInit() {
  }

  selecionar(eleg: ElegivelModel) {
    this.imunizar.emit(eleg)
  }

  marcacaoParaImunizar(eleg: ElegivelModel) {
    const flagEnvio = this.imunizacaoService.prepararImunizacao(eleg);
    if (eleg.aplicacao !== true) {
      this.imunizar.emit(flagEnvio)
    }
  }

  atualizarSituacaoImunizacao() {
    this.elegiveis = []
  }

  defineCorElegivel(aplic?: boolean): string {
    if (aplic !== undefined) {
      if (aplic === true) {
        return '#ECF0F6';
      } else {
        return 'white';
      }
    }
  }

}
