import { Component, OnInit, Input, AfterViewChecked, ChangeDetectorRef } from '@angular/core'
import { CampanhaModel } from '../../../../../modulos/features/campanhas/campanha.model';
import { ListCampanhaProdutos } from '../model/listCampanhaProd.model';
import { CampanhaService } from '../../../../../modulos/features/campanhas/campanha.service';
import { ElegivelService } from '../../../../../modulos/features/elegiveis/elegivel.service';
import { ElegivelModel } from '../../../../../modulos/features/elegiveis/elegivel.model';
import { NotificationService } from '../../../../../modulos/servicos/notification.service';
import { FormGroup, FormBuilder } from '@angular/forms'
import { ImunizacaoService } from '../service/imunizacao.service';
import DialogUtil from '../../../../../utils/dialog-util';
import { MatDialog } from '@angular/material';
import { Constants } from '../../../../../utils/constants';
import { BaseComponent } from '../../../../../utils/base.component';
import ModalUtil from '../../../../../utils/modal';
import { InputType } from '../../../../../shared/const/input-type.const';

@Component({
  selector: 'ic-vacinacao',
  templateUrl: './vacinacao.component.html'
})

export class VacinacaoComponent extends BaseComponent implements OnInit, AfterViewChecked {

  @Input() campanhas: CampanhaModel[] = []
  @Input() campanha: CampanhaModel
  @Input() elegiveis: ElegivelModel[] = []
  @Input() listProdutos: ListCampanhaProdutos[] = []
  @Input() cpfBusca: string = ''

  nameCampanha = 'campanha';
  nameSearchControl = 'searchControl';

  elegBackup: ElegivelModel[]

  listCampanha: string
  produto: ListCampanhaProdutos
  cpfPattern = /ˆ\d{3}\.\d{3}\.\d{3}\-\d{2}$/i

  idModal: string;
  campanhaToModal: CampanhaModel;
  produtoToModal: ListCampanhaProdutos;
  pendenteAprovacao: boolean;

  imuForm: FormGroup;

  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private campanhaService: CampanhaService,
    private elegivelService: ElegivelService,
    private imunizacaoService: ImunizacaoService,
    private notificationService: NotificationService,
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngAfterViewChecked() {
    this.imuForm.get(this.nameSearchControl).valueChanges.subscribe(value => {
      this.cpfBusca = this.formatarCPF(value);
    });
    this.cdr.detectChanges();
  }

  ngOnInit() {
    this.imuForm = this.formBuilder.group({});

    this.campanhaService.getCampanhas().subscribe(response => {
      this.campanhas = response
      this.carregarListaDeProdutos()
    }, response => this.notificationService.notifiy(response.mensagem))
  }

  carregarListaDeProdutos() {
    this.listProdutos = []
    for (const campanha of this.campanhas) {
      const listProd: ListCampanhaProdutos[] = []
      if (campanha.produtos) {
        for (const produto of campanha.produtos) {
          if (listProd.find((prodt) => prodt.produto_id === produto._id) === undefined) {
            const prod = this.getProdutoNull()
            prod.campanha_id = campanha._id
            prod.campanha = campanha.campanha
            prod.cliente = campanha.cliente.empresa
            prod.produto_id = produto._id
            prod.produto = produto.produto.produto || ''
            listProd.push(prod)
            this.listProdutos.push(prod)
          }
        }
      }
    }
  }

  emitSelecionaCampanha(event: any) {
    this.campanha = undefined;
    this.produto = undefined;
    this.produto = this.listProdutos.find(produto => produto.produto_id === event.value)
    if (this.produto !== undefined) {
      this.campanha = this.campanhas.find(campanha => campanha._id === this.produto.campanha_id)
      if (this.campanha !== undefined) {
        this.elegBackup = undefined
        this.elegiveis = undefined
      }
    }
  }

  selecionarElegiveis() {
    const cpf = this.desformatarCPF(this.imuForm.value.searchControl)
    // if (cpf.length >= 11) {
      this.imunizacaoService.produto = this.produto
      this.elegivelService.getElegiveis(this.campanha._id, cpf, this.produto.produto_id)
        .subscribe(response => this.elegiveis = response,
          response => {
            this.showInsertNewElegivel(this.campanha, this.produto);
          });
    // }
  }

  public showInsertNewElegivel(campanha: CampanhaModel, produto: ListCampanhaProdutos) {
    DialogUtil.openBaseDialog(this.dialog, Constants.MSG_CPF_NOT_FOUND,
      Constants.getText(Constants.MSG_SPE_REGISTER_ELIGIBLE, [campanha.campanha]),
      'Sim', 'Não', (result: any) => {
        if (result) {
          this.openInsertNewElegivel(campanha, produto);
        }
    });
  }

  public openInsertNewElegivel(campanha: CampanhaModel, produto: ListCampanhaProdutos) {
    if (campanha.cadastro_auto) {
      this.openModalNewElegivel(campanha, produto, false);
    } else {
      this.showPassword(campanha, produto);
    }
  }

  public showPassword(campanha: CampanhaModel, produto: ListCampanhaProdutos) {
    DialogUtil.openInputDialog(this.dialog, Constants.MSG_SPE_TITLE_PASSWORD_MANAGER, Constants.MSG_SPE_PASSWORD_MANAGER,
      'Enviar', 'Cancelar', 'senha', 'Senha', true, null, false, InputType.PASSWORD,
      (result: any) => {
        if (this.equals(result, campanha.senha_vacinacao)) {
          this.openModalNewElegivel(campanha, produto, true);
        } else if (result !== undefined) {
          this.showMessageError(this.notificationService, Constants.MSG_PASSWORD_INVALID);
          this.showPassword(campanha, produto);
        }
    });
  }

  public openModalNewElegivel(campanha: CampanhaModel, produto: ListCampanhaProdutos, pendenteAprovacao: boolean) {
    this.campanhaToModal = campanha;
    this.produtoToModal = produto;
    this.pendenteAprovacao = pendenteAprovacao;
    ModalUtil.showModalBody(this.idModal);
  }

  reset() {
    this.elegBackup = undefined
    this.elegiveis = undefined
    this.cpfBusca = ''
  }

  getProdutoNull(): ListCampanhaProdutos {
    const prod = {
      campanha_id: '',
      cliente: '',
      campanha: '',
      produto_id: '',
      produto: ''
    }
    return prod;
  }

  formatarCPF(cpf: string | ''): string {
    return this.elegivelService.formatarCPF(cpf)
  }

  desformatarCPF(cpf: string | ''): string {
    return this.elegivelService.desformatarCPF(cpf)
  }

  // Modal incluir elegível
  public setIdModal(event?: any): void {
    this.idModal = event;
  }

  public receiveData(cpf: any): void {
    this.setValueForm(this.imuForm, 'searchControl', cpf);
    if (cpf) {
      this.selecionarElegiveis();
    }
  }

}
