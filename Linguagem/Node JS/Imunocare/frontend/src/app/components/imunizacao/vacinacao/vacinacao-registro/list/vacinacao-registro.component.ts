import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ElegivelModel, ImunizacaoModel, EnfermeiroModel } from '../../../../../modulos/features/elegiveis/elegivel.model';
import { ImunizacaoService } from '../../vacinacao/service/imunizacao.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NotificationService } from '../../../../../modulos/servicos/notification.service';
import { ElegivelService } from '../../../../../modulos/features/elegiveis/elegivel.service';

@Component({
    selector: 'ic-vacinacao-registro',
    templateUrl: './vacinacao-registro.component.html'
})

export class VacinacaoRegistroComponent implements OnInit {

  imuForm: FormGroup

  enfermeiro: EnfermeiroModel = {
    usuario: '',
    nome: ''
  }

  @Input() imunizacao: ImunizacaoModel = {
    _id: '',
    produto: '',
    desc_produto: '',
    lote: '',
    num_lote: '',
    data: null,
    aplicacao: false,
    usuario: this.enfermeiro
  }
  @Input()imunizacoes: ImunizacaoModel[] = []
  @Input() elegivel: ElegivelModel

  @Output() resetCPF = new EventEmitter()
  @Output() retornarImunizacao = new EventEmitter()

  constructor(
    private formBuilder: FormBuilder,
    private imunizacaoService: ImunizacaoService,
    private elegivelService: ElegivelService,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.imuForm = this.formBuilder.group({
      nome: this.formBuilder.control(''),
      email: this.formBuilder.control(''),
      cpf: this.formBuilder.control(''),
      produto: this.formBuilder.control(''),
      lote: this.formBuilder.control('')
    })
  }

  receberElegivelParaImunizar() {
    this.elegivel = this.imunizacaoService.getDadosElegivel()

    this.imunizacao.produto = this.imunizacaoService.produto.produto_id
    this.imunizacao.desc_produto = this.imunizacaoService.produto.produto

    this.imunizacoes = []

    for (const imu of this.imunizacaoService.getDadosElegivel().imunizacoes) {
      if (imu.produto === this.imunizacao.produto) {
        this.imunizacoes.push(imu)
      }
    }

    if (this.elegivel !== undefined) {
      this.imuForm.value.nome = this.elegivel.nome
      this.imuForm.value.cpf = this.elegivel.cpf
      this.imuForm.value.produto = this.imunizacao.desc_produto
      if (this.imunizacoes.length > 0) {
        this.imuForm.value.lote = this.imunizacoes[0]._id
      }
    }
  }

  selecionarImunizacao(event: any) {
    this.imunizacao = this.imunizacoes.find((imu) => imu._id === event.target.value)
    this.imunizacaoService.imunizacao.imunizacao_id = this.imunizacao._id
    this.imuForm.value.lote = this.imunizacao._id
    console.log(`IMUNIZAÇÃO SELECIONADA Imunizacao: ${this.imunizacaoService.imunizacao.imunizacao_id}
                                       \n Elegivel: ${this.imunizacaoService.imunizacao.elegivel_id}`)
  }

  reiniciarObjetos() {
    this.elegivel = undefined
    this.imunizacoes = []
    this.imunizacao = {
      _id: '',
      produto: '',
      desc_produto: '',
      lote: '',
      num_lote: '',
      data: null,
      aplicacao: false
    }
  }

  posicionaPrimeiroLote(i): boolean {
    if (i === 0) {
      return true
    } else {
      return false
    }
  }

  enviarImunizacao() {
    console.log(`IMUNIZAÇÃO SELECIONADA\nImunizacao: ${this.imunizacaoService.imunizacao.imunizacao_id}
    \n Elegivel: ${this.elegivel._id}
    \n imunizacao para api: ${this.imunizacao._id}`)

    this.imunizacaoService.enviarImunizacaoParaRegistro(this.elegivel._id, this.imunizacao._id)
      .subscribe(response => {
        this.notificationService.notifiy({message: response.mensagem});
        this.retornarImunizacao.emit();
        this.resetCPF.emit();
        this.reiniciarObjetos();
        console.log('ok');
      },
      response => {
        console.log(response);
        this.notificationService.notifiy({message: response.error.mensagem, tipo: 'E'})
      })
  }

}
