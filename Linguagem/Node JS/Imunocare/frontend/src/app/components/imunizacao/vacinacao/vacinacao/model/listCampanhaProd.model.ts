export interface ListCampanhaProdutos {
    campanha_id: string,
    cliente: string,
    campanha: string,
    produto_id: string,
    produto: string
}
