import { MatDialog } from '@angular/material';
import { DialogBaseComponent } from '../shared/components/dialog/dialog-base/dialog-base.component';
import { DialogInputComponent } from '../shared/components/dialog/dialog-input/dialog-input.component';
import { DialogTemplateDownloadComponent } from '../shared/components/dialog/dialog-template-download/dialog-template-download.component';
import { DialogBaseModel } from '../shared/model/dialog-base.model';
import { DialogInputModel } from '../shared/model/dialog-input.model';
import { DialogTemplateDownloadModel } from '../shared/model/dialog-template-download.model';
import { InputType } from '../shared/const/input-type.const';

export default class DialogUtil {

  static openInputDialog(dialog: MatDialog, title: string, text: string, buttonConfirm?: string, buttonCancel?: string, name?: string,
    placeholder?: string, required?: boolean, value?: any, disabled?: boolean, type?: InputType, callback?: (result: any) => void): void {
    const dataConfig = new DialogInputModel(title, text, buttonConfirm, buttonCancel, name, placeholder, required, value, disabled, type);
    DialogUtil.openDialog(dialog, DialogInputComponent, DialogUtil.newConfig(dataConfig), (result: any) => {
      if (callback) {
        callback(result);
      }
    });
  }

  static openBaseDialog(dialog: MatDialog, title: string, text: string, buttonConfirm?: string, buttonCancel?: string,
    callback?: (result: any) => void): void {
    const dataConfig = new DialogBaseModel(title, text, buttonConfirm, buttonCancel);
    DialogUtil.openDialog(dialog, DialogBaseComponent, DialogUtil.newConfig(dataConfig), (result: any) => {
      if (callback) {
        callback(result);
      }
    });
  }

  static openTemplateDownloadDialog(dialog: MatDialog, title: string, text: string, buttonConfirm?: string, buttonCancel?: string,
    fileDownload?: string, folderDownload?: string, callback?: (result: any) => void): void {
    const dataConfig = new DialogTemplateDownloadModel(title, buttonConfirm, buttonCancel, fileDownload, folderDownload);
    DialogUtil.openDialog(dialog, DialogTemplateDownloadComponent, DialogUtil.newConfig(dataConfig), (result: any) => {
      if (callback) {
        callback(result);
      }
    });
  }

  static openDialog(dialog: MatDialog, content: any, config?: any, callback?: (result: any) => void) {
    let dialogRef;
    if (config) {
      dialogRef = dialog.open(content, config);
    } else {
      dialogRef = dialog.open(content);
    }

    dialogRef.afterClosed().subscribe(result => {
      if (callback) {
        callback(result);
      }
    });
  }

  static newConfig(data?: any): any {
    return {
      data: data
    };
  }

}
