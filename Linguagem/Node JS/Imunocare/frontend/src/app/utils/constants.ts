export class Constants {

  /**
   * Sistema
   */
  // Mensagem
  static readonly MSG_REQUIRED_FIELD = 'Campo Obrigatório';
  static readonly MSG_VALID_EMAIL = 'Por favor insira um endereço de e-mail válido';
  static readonly MSG_SUCCESS_OPERATION = 'Operação realizada com sucesso';
  static readonly MSG_ERROR_SAVE = 'Ocorreu um erro ao salvar ';
  static readonly MSG_ERROR_RECOVER = 'Ocorreu um erro ao recuperar ';
  static readonly MSG_ERROR_EDIT = 'Ocorreu um erro ao editar ';
  static readonly MSG_ERROR_HORARIO_MAIOR = 'Horario inicial deve ser menor que final';
  static readonly MSG_ERROR_HORARIO_MENOR = 'Horário final deve ser maior que inicial';
  static readonly MSG_UNEXPECTED_ERROR = 'Ocorreu um erro inesperado';
  static readonly MSG_DUPLICATED = 'Já existe um registro salvo com os dados informados';
  static readonly MSG_INCORRECT_INFORMATION = 'Informação incorreta';
  static readonly MSG_CPF_NOT_FOUND = 'CPF não encontrado';
  static readonly MSG_PASSWORD_INVALID = 'Senha inválida';
  static readonly MSG_VALID_CPF = 'Por favor insira um CPF válido';
  static readonly MSG_VALID_CNPJ = 'Por favor insira um CNPJ válido';

  /**
   * Específico
   */
  static readonly MSG_SPE_REQUIRED_LOT = 'Deve ser informado pelo menos um lote';
  static readonly MSG_SPE_REQUIRED_PROD = 'Deve ser informado o produto';
  static readonly MSG_SPE_REQUIRED_FILIAL = 'Não há novas filiais';
  static readonly MSG_SPE_REQUIRED_UNIT = 'Não há novas unidades';
  static readonly MSG_SPE_REGISTER_ELIGIBLE = 'Deseja cadastrar novo elegível na campanha [{0}]?';
  static readonly MSG_SPE_PASSWORD_MANAGER = 'Informe a senha de vacinação da campanha';

  /**
   * Dialogos
   */
  // Deletar
  static readonly MSG_SPE_DIALOG_DELETE_TITLE = 'Remover';
  static readonly MSG_SPE_DIALOG_DELETE_TEXT = 'Deseja realmente remover este item?';
  static readonly MSG_SPE_DIALOG_DELETE_BTN_CONFIRM = 'Sim';
  static readonly MSG_SPE_DIALOG_DELETE_BTN_CANCEL = 'Não';

  // Titulo
  static readonly MSG_SPE_TITLE_ALERT = 'Atenção!';
  static readonly MSG_SPE_TITLE_TEMPLATE = 'Template';
  static readonly MSG_SPE_TITLE_PASSWORD_MANAGER = 'Senha de vacinação é obrigatório';

  // Arquivo
  static readonly MSG_SPE_FILE_MAX_SIZE = 'Tamanho máximo do arquivo permitido é: ';
  static readonly MSG_SPE_FILE_NO_MULTIPLE = 'Não é permitido selecionar mais de um arquivo';
  static readonly MSG_SPE_FILE_NO_FILE_SELECTED = 'Nenhum Arquivo Selecionado';
  static readonly MSG_SPE_FILE_TYPES_ALLOWED = 'Tipo de arquivo selecionado não permitido, os tipos permitidos são: ';
  static readonly MSG_SPE_FILE_INCONSISTENCY = 'Há inconsistência no arquivo';
  static readonly MSG_SPE_FILE_TEMPLATE_NOT_FOUND = 'Não há template informado';
  static readonly MSG_SPE_FILE_TEMPLATE_INCORRECT = 'Arquivo com template incorreto:';
  static readonly MSG_SPE_FILE_UNFILLED_FIELD = 'Todas as colunas das linhas devem ser preenchidas, ' +
                                                'caso não tenha informação para coluna deve ser preenchida de acordo com o tipo:';

  // Produto
  static readonly MSG_VALIDA_EXC_PRODUTO = 'O Produto apresenta vinculos com Campanha';
  // Tabela
  static readonly MSG_SPE_TABLE_NO_RESULT = 'Nenhum registro encontrado';

  static getText(text: string, variables?: any[]): string {
    variables = variables ? variables : [];
    variables.forEach((variable, index) => {
      text = text.replace(`{${index}}`, variable + '');
    });
    return text;
  }

}
