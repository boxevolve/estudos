import { AbstractControl, FormGroup } from '@angular/forms';
import { Util } from './util';
import { Constants } from './constants';
import { SimNao } from '../shared/const/sim-nao.const';
import { TipoParceiro } from '../shared/enum/tipo-parceiro.enum'
import { StatusCampanha } from '../shared/enum/status-campanha.enum';
import { SimpleChanges } from '@angular/core';
import { NotificationService } from '../modulos/servicos/notification.service';
import { Sexo } from '../shared/const/sexo.const';

export class BaseComponent {

  constructor() {}

  public showMessageError(notificationService: NotificationService, message: string): void {
    notificationService.notifiy({
      message:  message,
      tipo: 'E',
    });
  }

  public getCloneForm(form: FormGroup): any {
    return this.getClone(this.convertFormToObj(form));
  }

  public getClone(object: any): any {
    return JSON.parse(JSON.stringify(object));
  }

  public idHashtag(id: string): string {
    return id ? `#${id}` : '';
  }

  public onChangeInput(changes: SimpleChanges, name: string, next: (currentValue?: any) => void): void {
    const keyChanges = Object.keys(changes);
    if (this.contains(keyChanges, name)) {
      next(changes[name].currentValue);
    }
  }

  public concatStr(values: string[], concat?: string, attribute?: string): string {
    concat = concat ? concat : '';
    let newValue = '';
    values.forEach(value => {
      let arrayValue = value;
      if (attribute) {
        arrayValue = value[attribute];
      }
      newValue += arrayValue + concat;
    });
    return newValue.substring(0, newValue.length - concat.length);
  }

  public getControl(form: FormGroup, fieldName: string): AbstractControl {
    if (!Util.isEmpty(form)) {
      const field: AbstractControl = form.get(fieldName);
      return Util.isEmpty(field) ? null : field;
    }
    return null;
  }

  public validadePeriod(form: FormGroup, fieldNameMin: string, fieldNameMax: string): boolean {
    const valueMin = this.getValueForm(form, fieldNameMin);
    const valueMax = this.getValueForm(form, fieldNameMax);
    const isDefined = (valueMin && valueMax) || valueMin === 0 || valueMax === 0;
    if (isDefined ? valueMin > valueMax : false) {
      this.setValueForm(form, fieldNameMin, valueMax);
      return false;
    }
    return true;
  }

  public validateField(form: FormGroup, fieldName: string, type?: string): void {
    let value = this.getValueForm(form, fieldName);
    if (value) {
      value = value.trim();
      if (type && type === 'number' && Number(value) < 0) {
        value = '';
      }
      this.setValueForm(form, fieldName, value.trim());
    }
  }

  public addAll(listA: any[], listB: any[]): any[] {
    listA.push.apply(listA, listB);
    return listA;
  }

  public remove(list: any[], index?: number, value?: any, lambda?: (x: any) => boolean): any[] {
    list = this.getClone(list);
    if (lambda) {
      index = this.find(list, null, lambda);
    } else if (value) {
      index = this.find(list, value);
    }
    if (index !== -1) {
      list.splice(index, 1);
    }
    return list;
  }

  public contains(list: any[], value?: any, lambda?: (x: any) => boolean): boolean {
    let index = -1;
    if (lambda) {
      index = list.findIndex(lambda);
    } else {
      index = list.findIndex(x => this.equals(x, value));
    }
    return index !== -1;
  }

  public find(list: any[], value?: any, lambda?: (x: any) => boolean): number {
    let index = -1;
    if (lambda) {
      index = list.findIndex(lambda);
    } else {
      index = list.findIndex(x => this.equals(x, value));
    }
    return index;
  }

  public findAll(list: any[], value?: any, lambda?: (x: any) => boolean): number[] {
    let indexes = [];
    if (lambda) {
      for (let index = 0; index < list.length; index++) {
        if (lambda(list[index])) {
          indexes.push(index);
        }
      }
    } else {
      for (let index = 0; index < list.length; index++) {
        if (this.equals(list[index], value)) {
          indexes.push(index);
        }
      }
    }
    return indexes;
  }

  public equals(objectA: any, objectB: any): boolean {
    if (typeof objectA !== typeof objectB) {
      return false;
    }
    if (objectA === null && objectB === null) {
      return true;
    }
    if (objectA === undefined && objectB === undefined) {
      return true;
    }
    const aKeys = objectA !== null && typeof objectA !== 'string' ? Object.keys(objectA) : [],
          bKeys = objectB !== null && typeof objectB !== 'string' ? Object.keys(objectB) : [];
    if (aKeys.length !== bKeys.length) {
      return false;
    }
    if (aKeys.length === 0) {
      return (objectA === objectB
          || (typeof objectA === 'object' || typeof objectB === 'object'));
    }
    const areDifferent = aKeys.some((key) => {
      return !this.equals(objectA[key], objectB[key]);
    });
    return !areDifferent;
  }

  public disableField(form: FormGroup, fieldName: string): void {
    const field = this.getControl(form, fieldName);
    if (!Util.isEmpty(field)) {
      field.disable();
    }
  }

  public enableField(form: FormGroup, fieldName: string): void {
    const field = this.getControl(form, fieldName);
    if (!Util.isEmpty(field)) {
      field.enable();
    }
  }

  public setValue(id: string, value: any): void {
    (<any>$(this.idHashtag(id))).val(value);
  }

  public getValueForm(form: FormGroup, fieldName: string): any {
    const field = this.getControl(form, fieldName);
    return !Util.isEmpty(field) ? field.value : null;
  }

  public setValueForm(form: FormGroup, fieldName: string, value: any): void {
    const field = this.getControl(form, fieldName);
    if (!Util.isEmpty(field)) {
      field.setValue(value);
    }
  }

  public setValueArrayForm(form: FormGroup, fieldName: string, value: any): void {
    const field = this.getControl(form, fieldName);
    if (!Util.isEmpty(field)) {
      form.removeControl(fieldName);
      form.addControl(
        fieldName,
        value
      );
    }
  }

  public validateForm(form: FormGroup, callback?: () => void): void {
    if (form.invalid) {
      Object.keys(form.value).forEach(key => {
        form.get(key).markAsTouched();
      });
    } else if (callback) {
      callback();
    }
  }

  public removeValidationForm(form: FormGroup, fieldName: string): void {
    form.get(fieldName).clearValidators();
    form.get(fieldName).updateValueAndValidity();
  }

  public setValidationForm(form: FormGroup, fieldName: string, validations: any[]): void {
    if (!form.get(fieldName)) {
      return;
    }
    if (Util.isEmpty(!validations)) {
      form.get(fieldName).setValidators(validations);
    } else {
      form.get(fieldName).clearValidators();
    }
    form.get(fieldName).updateValueAndValidity();
  }

  public clearObject(object: any): any {
    const newObject = {};
    Object.keys(object).forEach(key => {
      if (object[key]) {
        newObject[key] = object[key];
      }
    });
    return newObject;
  }

  public clearMatriz(matriz: any): any[] {
    const newMatriz = [];
    matriz.forEach(array => {
      if (array.length) {
        newMatriz.push(array);
      }
    });
    return newMatriz;
  }

  public instantiateObject(object: any): any {
    const newObject = {};
    Object.keys(object).forEach(key => {
      newObject[key] = object[key] ? object[key] : null;
    });
    return newObject;
  }

  public instantiateForm(form: FormGroup, object: any): any {
    const newObject = {};
    const objectKeys = Object.keys(this.instantiateObject(object));
    const formValueKeys = Object.keys(this.getCloneForm(form));

    formValueKeys.forEach(formKey => {
      let find = false;
      for (let index = 0; index < objectKeys.length; index++) {
        const key = objectKeys[index];
        if (key === formKey) {
          newObject[formKey] = object[key];
          find = true;
          break;
        }
      }
      if (!find) {
        newObject[formKey] = null;
      }
    });
    return newObject;
  }

  public unionObject(objectA: any, objectB: any): any {
    const objectAKeys = Object.keys(this.instantiateObject(objectA));
    const objectBKeys = Object.keys(this.instantiateObject(objectB));

    objectAKeys.forEach(keyA => {
      for (let index = 0; index < objectBKeys.length; index++) {
        const key = objectBKeys[index];
        if (key === keyA) {
          objectA[keyA] = objectB[key];
          break;
        }
      }
    });
    return objectA;
  }

  public instantiateString(object: any): any {
    const newObject = {};
    Object.keys(object).forEach(key => {
      newObject[key] = object[key] ? object[key] : '';
    });
    return newObject;
  }

  public convertFormToObj(form: FormGroup): any {
    const newObject = {};
    Object.keys(form.controls).forEach(control => {
      newObject[control] = this.getValueForm(form, control);
    });
    return newObject;
  }

  public getListObject(data: any[], attributes?: string[]): any[] {
    if (Util.isEmpty(data) || !data.length) {
      return null;
    }
    const newList = [];
    const keys = attributes ? attributes : data[0];
    for (let i = 0; i < data.length; i++) {
      const row = data[i];
      const newObject = {};
      for (let j = 0; j < keys.length; j++) {
        const key = keys[j];
        newObject[key] = row[j];
      }
      newList.push(newObject);
    }
    return newList;
  }

  public getCepOnly(cep: string): string {
    cep = cep.toString();
    cep = cep.trim();
    cep = cep ? cep.replace(' ', '') : '';
    return cep ? cep.replace('-', '') : '';
  }

  public validateCep(cep: string): boolean {
    return this.getCepOnly(cep).length === 8;
  }

  public validateCnpj(cnpj: any) {
    if (cnpj === '' || cnpj === undefined || cnpj == null) {
      return false;
    }
    if (typeof(cnpj) !== 'string') {
      cnpj = cnpj.toString();
    }
    cnpj = cnpj.replace(/[^\d]+/g, '');
    if (cnpj.length !== 14) {
      return false;
    }
    if (cnpj === '00000000000000' ||
        cnpj === '11111111111111' ||
        cnpj === '22222222222222' ||
        cnpj === '33333333333333' ||
        cnpj === '44444444444444' ||
        cnpj === '55555555555555' ||
        cnpj === '66666666666666' ||
        cnpj === '77777777777777' ||
        cnpj === '88888888888888' ||
        cnpj === '99999999999999') {
          return false;
        }
    let tamanho = cnpj.length - 2
    let numeros = cnpj.substring(0, tamanho);
    const digitos = cnpj.substring(tamanho);
    let soma = 0;
    let pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2) {
        pos = 9;
      }
    }
    let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado + '' !== digitos.charAt(0) + '') {
      return false;
    }
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2) {
        pos = 9;
      }
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado + '' !== digitos.charAt(1) + '') {
      return false;
    }
    return true;
  };

  public validateCpf(cpf: any) {
    if (cpf === '' || cpf === undefined || cpf == null) {
      return false;
    }
    if (typeof(cpf) !== 'string') {
      cpf = cpf.toString();
    }
    cpf = cpf.replace(/[^\d]+/g, '');
    if (cpf.length !== 11) {
      return false;
    }
    if (cpf === '00000000000' ||
        cpf === '11111111111' ||
        cpf === '22222222222' ||
        cpf === '33333333333' ||
        cpf === '44444444444' ||
        cpf === '55555555555' ||
        cpf === '66666666666' ||
        cpf === '77777777777' ||
        cpf === '88888888888' ||
        cpf === '99999999999') {
      return false;
    }
    let add = 0;
    for (let i = 0; i < 9; i++) {
      add += Number(cpf.charAt(i)) * (10 - i);
    }
    let rev = 11 - (add % 11);
    if (rev === 10 || rev === 11) {
      rev = 0;
    }
    if (rev !== Number(cpf.charAt(9))) {
        return false;
    }
    add = 0;
    for (let i = 0; i < 10; i++) {
      add += Number(cpf.charAt(i)) * (11 - i);
    }
    rev = 11 - (add % 11);
    if (rev === 10 || rev === 11) {
      rev = 0;
    }
    if (rev !== Number(cpf.charAt(10))) {
      return false;
    }
    return true;
  }

  public formatCPF(cpf: string | ''): string {
    if (cpf.length > 0) {
      cpf = cpf.replace(/\D/g, '');
      cpf = cpf.replace(/^(\d{3})?(\d{3})?(\d{3})?(\d{2})$/, '$1.$2.$3-$4');
    }
    return cpf || '';
  }

  public clearCPF(cpf: string | ''): string {
    cpf = cpf.replace(/\D/g, '');
    return cpf || '';
  }

  public typeString(value: any): boolean {
    return typeof value === 'string';
  }

  public typeArray(value: any): boolean {
    return Array.isArray(value);
  }

  /**
   * Constantes
   */

  public msgRequiredField(): string {
    return Constants.MSG_REQUIRED_FIELD;
  }

  public msgValidEmail(): string {
    return Constants.MSG_VALID_EMAIL;
  }

  public msgValidCpf(): string {
    return Constants.MSG_VALID_CPF;
  }

  public msgValidCnpj(): string {
    return Constants.MSG_VALID_CNPJ;
  }

  public msgNoResult(): string {
    return Constants.MSG_SPE_TABLE_NO_RESULT;
  }

  public msgUnexpectedError(): string {
    return Constants.MSG_UNEXPECTED_ERROR;
  }

  public simNaoList(): any[] {
    return SimNao.list();
  }

  public sexoList(): any[] {
    return Sexo.list();
  }

  /**
   * Enums
   */

  public tipoParceiroList(): Array<string> {
    return Object.values(TipoParceiro);
  }

  public statusCampanhaList(): Array<string> {
    return Object.values(StatusCampanha);
  }

}
