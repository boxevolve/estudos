// Este environment corresponde ao
// ambiente Local de Desenvolvimento
export const environment = {
	server: false,
	local: true,
	envName: 'loc',
	backendUrlFromFrontend: false,
	backendHttps: true,
	backendPort: 8443
};
