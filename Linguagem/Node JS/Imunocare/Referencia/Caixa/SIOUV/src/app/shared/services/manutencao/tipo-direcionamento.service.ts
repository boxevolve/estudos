import { Injectable } from '@angular/core';
import { TipoDirecionamento } from 'app/shared/models/tipo-direcionamento';
import { HttpClient } from '@angular/common/http';
import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';

@Injectable()
export class TipoDirecionamentoService extends CrudHttpClientService<TipoDirecionamento> {

  constructor(protected http: HttpClient) {
    super('ocorrencia/tipo-direcionamento', http);
   }

}
