import { DatePipe } from '@angular/common';

export class Util {
  public static isDefined(dado: any): boolean {
    return (dado) && (dado !== 'undefined') && (dado !== 'null');
  }

  public static isBlank(dado: any): boolean {
    const text: string = dado + '';
    return !text.length || !text.trim().length;
  }

  public static isEmpty(dado: any): boolean {
    return (!Util.isDefined(dado)) || (this.isBlank(dado));
  }

  public static dateToString(d: Date): string {
    const datePipe = new DatePipe('en-US');

    return datePipe.transform(d, 'yy-MM-dd\'T\'HH:mm:ss');
  }

  public static dateToStringBr(d: Date): string {
    const datePipe = new DatePipe('pt-BR');
    return datePipe.transform(d, 'dd/MM/yyyy');
  }

  public static convertSecsToMins(time: number): number {
    return Math.floor(time / 60);
  }

  /**
	 * Converte o JSON para String
	 * @param entity
	 */
  public static objetoParaJson(entity: any): string {
    return JSON.stringify(entity);
  }

  /**
	 * Converte a String no formato JSON para Objeto
	 * @param entity
	 */
  public static jsonParaObjeto(str: string): any {
    return JSON.parse(str);
  }

  public static sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  /**
	 * dateToJson - Converte a Data para objeto Json
	 * @param date
	 */
  public static dateToJson(d: Date) {
    return {
      date: {
        year: d.getFullYear(),
        month : d.getMonth() + 1,
        day: d.getDate()
      }
    };
  }

  /**
	 * Converte a string de SIM ('S') e NAO ('N') para boolean.
	 * @param
	 */
  public static convertSNtoBool(valor: any): boolean {
    if (typeof valor === 'boolean') {
      return valor;
    }
    if (typeof valor === 'string') {
      return valor === 'S';
    }
    return false;
  }

  /**
	 * Converte o boolean para string de SIM ('S') e NAO ('N').
	 * @param
	 */
  public static convertBoolToSN(valor: any): string {
    const sim = 'S';
    const nao = 'N';
    if (typeof valor === 'boolean') {
      return valor ? sim : nao;
    }
    if (typeof valor === 'string') {
      return valor === 'true' ? sim : nao;
    }
    return nao;
  }

}
