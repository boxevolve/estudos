import { MeioComunicacao } from './meio-comunicacao';
import { EnderecoUnidade } from './endereco-unidade';
import { TipoUnidade } from './tipo-unidade';

export class Unidade {
    constructor (
        public nuUnidade: number,
        public nuNatural: number,
        public nome: string,
        public sigla: string,
        public uf: string,
        public siglaLocalizacao: string,
        public tipoUnidade: TipoUnidade,
        public meioComunicacao: MeioComunicacao,
        public endereco: EnderecoUnidade,
        public ultimaSituacao: string
    ) {}
}
