// /-------------------------------------------------\
// | artefatos da biblioteca angular                 |
// \-------------------------------------------------/
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// /-------------------------------------------------\
// | de terceiros (Bootstrap, etc.)        |
// \-------------------------------------------------/
import { ModalModule } from 'ngx-bootstrap/modal';

// /-------------------------------------------------\
// | artefatos compartilhados do projeto             |
// \-------------------------------------------------/
import { ServiceModule } from 'app/arquitetura/shared/services/service.module';
import { AppServiceModule } from 'app/shared/services/app-service.module';
import { MessageModule } from 'app/shared/components/messages/message.module';
import { MessageResourceProvider } from 'app/shared/components/messages/message-resource-provider';
import { MessageResourceImpl } from 'app/app.message';
import { ValidationModule } from 'app/shared/components/validation/validation.module';
import { ValidationResourceProvider } from 'app/shared/components/validation/validation-resource-provider';
import { KeycloakService } from 'app/arquitetura/shared/services/seguranca/keycloak.service';
import { RequestInterceptor } from 'app/arquitetura/shared/interceptors/request.interceptor';
import { LoadingModule } from 'app/shared/components/loading/loading.module';
import { PaginationModule } from 'app/shared/components/pagination/pagination.module';
import { PipeModule } from 'app/shared/pipes/pipe.module';

// /-------------------------------------------------\
// | artefatos/componentes da aplicação              |
// \-------------------------------------------------/
import { AppComponent } from './app.component';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { AppRoutingModule } from 'app/app.routing.module';
import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { TextMaskModule } from 'angular2-text-mask';
import { TransferObject } from './shared/util/trasfer-object';
import { DirectivesModule } from './arquitetura/shared/directives/directives.module';

// /--------------------------------------------------\
// | app.module.ts                                    |
// |--------------------------------------------------|
// | Módulo principal da aplicação (root module)      |
// \--------------------------------------------------/
@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    ServiceModule.forRoot(),
    AppServiceModule.forRoot(),
    MessageModule.forRoot(),
    DirectivesModule.forRoot(),
    ValidationModule,
    LoadingModule.forRoot(),
    PaginationModule.forRoot(),
    PipeModule.forRoot(),
    TemplatesModule,
    AppRoutingModule,
    TextMaskModule
  ],
  exports: [],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: MessageResourceProvider,
      useValue: MessageResourceImpl
    },
    {
      provide: ValidationResourceProvider,
      useValue: MessageResourceImpl
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    },
    KeycloakService,
    AuthGuard,
    DadosUsuarioGuard,
    TransferObject
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
