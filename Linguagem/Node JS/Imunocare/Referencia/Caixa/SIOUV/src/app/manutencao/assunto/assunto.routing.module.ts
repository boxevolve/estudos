import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { AssuntoConsultaComponent } from './assunto-consulta/assunto-consulta.component';
import { AssuntoCadastroComponent } from './assunto-cadastro/assunto-cadastro.component';
import { AssuntoHistoricoComponent } from './assunto-historico/assunto-historico.component';

const routes: Routes = [{
    path: '',
    canActivate: [AuthGuard, DadosUsuarioGuard],
    canActivateChild: [AuthGuard, DadosUsuarioGuard],
    children: [
        {
            path: '',
            component: AssuntoConsultaComponent
        },
        {
            path: 'novo',
            component: AssuntoCadastroComponent
        },
        {
            path: ':id/editar',
            component: AssuntoCadastroComponent
        },
        {
            path: 'historico',
            component: AssuntoHistoricoComponent
        }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AssuntoRoutingModule { }
