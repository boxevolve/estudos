import { Entity } from 'app/arquitetura/shared/models/entity';
import { TipoOcorrencia } from "./tipo-ocorrencia";
import {  TipoOcrnaOrigem } from "./tipo-ocrna-origem"; 
import { PrazoOrigemNatureza } from './prazo-origem-natureza';

export class Origem extends Entity {
	public nome: string = null;
	public ativo: string = null;
	public procedencia: string = null;
	public rar: string = null;
	public procon: string = null;	
	public 	acordo: string = null;	
	public 	conciliado: string = null;	
	public 	indenizacao: string = null;
	public 	unidadeConciliado: string = null;
	public tipoOcorrencia: TipoOcorrencia;
	public tiposOcorrenciaOrigem: TipoOcrnaOrigem[] = Array<TipoOcrnaOrigem>();
	public labelNomeOrigem: string;
	public labelAtivo: string;
	public labelTiposOcorrenciaNatureza: string;
	public prazoNaturezaOrigem: PrazoOrigemNatureza[] = Array<PrazoOrigemNatureza>();
}
