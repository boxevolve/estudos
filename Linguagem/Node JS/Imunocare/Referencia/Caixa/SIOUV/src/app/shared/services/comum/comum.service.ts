import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Situacao } from '../../models/situacao';
import { Entity } from '../../../arquitetura/shared/models/entity';
import { Injectable } from '@angular/core';
import { SimNao } from '../../models/sim-nao';
import { PermissaoTransferenciaTodasUnidades } from '../../models/permissao-transferencia-todas-unidades';
import { AssuntoItemMotivo } from 'app/shared/models/AssuntoItemMotivo';

@Injectable()
export class ComumService extends CrudHttpClientService<Entity> {

  constructor(protected http: HttpClient) {
    super('comum', http);
  }

    public listarSituacoes(): Observable<Situacao[]> {
      return this.http.get<Situacao[]>(this.url + '/list/situacoes', this.options());
    }

    public listarAssuntoItemMotivo(): Observable<AssuntoItemMotivo[]> {
      return this.http.get<AssuntoItemMotivo[]>(this.url + '/list/assuntoItemMotivo', this.options());
    }

    public listarSituacoesSemOpcaoTodos(): Observable<Situacao[]> {
      return this.http.get<Situacao[]>(this.url + '/list/situacoes-inclusao', this.options());
    }

    public listarSimNao(): Observable<SimNao[]> {
      return this.http.get<SimNao[]>(this.url + '/list/sim-nao', this.options());
    }

    public listarPermissaoTransferenciaTodasUnidades(): Observable<SimNao[]> {
      return this.http.get<PermissaoTransferenciaTodasUnidades[]>(this.url + '/list/permissao-transferencia-todas-unidades', this.options());
    }


}
