import { Entity } from 'app/arquitetura/shared/models/entity';

export class ConfiguracoesSeguranca extends Entity {
	public realm: string = null;
	public idCliente: string = null;
	public urlServidorAutorizacao: string = null;
	public tempoVidaToken: number = null;
	public tempoMaximoIdle: number = null;
}
