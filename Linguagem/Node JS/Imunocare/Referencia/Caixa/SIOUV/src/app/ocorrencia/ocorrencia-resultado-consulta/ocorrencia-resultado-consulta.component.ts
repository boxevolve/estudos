import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Ocorrencia } from 'app/shared/models/ocorrencia';
import { Origem } from 'app/shared/models/origem';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { SiouvTableBuilder } from 'app/shared/models/siouv-table-builder';
import { OcorrenciaService } from 'app/shared/services/ocorrencia/ocorrencia.service';
import { ParametrosConsultaOcorrencia } from '../ocorrencia-consulta/parametros-consulta-ocorrencia';
import { TransferObject } from 'app/shared/util/trasfer-object';

@Component({
  selector: 'app-ocorrencia-resultado-consulta',
  templateUrl: './ocorrencia-resultado-consulta.component.html',
  styleUrls: ['./ocorrencia-resultado-consulta.component.css']
})
export class OcorrenciaResultadoConsultaComponent extends BaseComponent implements OnInit{

    origens: Origem[] = null;
    dataList: SiouvTable;
    ocorrencias: Ocorrencia[];

    constructor(
        protected messageService: MessageService,
        private ocorrenciaService: OcorrenciaService,
        private transferObject: TransferObject,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        super(messageService);
    }

    ngOnInit(){
        var callback: Function;
        console.log('parametros: '+JSON.stringify(this.transferObject.storage));
        this.ocorrenciaService.callListarOcorrenciasPorParametros(this.transferObject.storage, callback);
    }

    atualizarDataList() {
        this.dataList = this.createSiouvTable(this.origens);
    }

    createSiouvTable(list: Origem[]): SiouvTable {
        let builder: SiouvTableBuilder<Ocorrencia> = new SiouvTableBuilder<Ocorrencia>('ocorrenciasTable', this.ocorrencias);
        builder.addColuna('nro_ocorrencia', 'Origem', (ocorrencia: Ocorrencia)=> ocorrencia.id);
        builder.addColuna('assunto', 'Origem', (ocorrencia: Ocorrencia)=> ocorrencia.itemAssunto);
        builder.addColuna('item', 'Item', (ocorrencia: Ocorrencia)=> ocorrencia.itemAssunto);
        builder.addColuna('natureza', 'Natureza', (ocorrencia: Ocorrencia)=> ocorrencia.natureza);
        builder.addColuna('origem', 'Origem', (ocorrencia: Ocorrencia)=> ocorrencia.id);
        builder.addColuna('vencimento', 'Vencimento', (ocorrencia: Ocorrencia)=> ocorrencia.vencimento);
        builder.addColuna('situacao', 'Situação', (ocorrencia: Ocorrencia)=> ocorrencia.situacaoOcorrenciaMovimentacao);
        builder.addColuna('unidade', 'Unidade', (ocorrencia: Ocorrencia)=> ocorrencia.unidadeEnvolvida);
        return this.newDefaultSiouvTableFromBuilder(builder);
      }

      
    excluir(origem: Origem): void {
    }

    finalizarExcluir(indice: number, origem: Origem): void {
    }

    alterar(origem: Origem): void {
        this.router.navigate([origem.id, 'editar'], { relativeTo: this.route.parent });
    }


}
