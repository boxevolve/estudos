import { TipoOperacao } from './tipo-operacao';
import { Natureza } from './natureza';

export class NaturezaHistoricoFiltro {
  public usuario: string = null;
  public tiposOperacao: TipoOperacao[] = Array<TipoOperacao>();
  public dataInicio: Date = null;
  public dataFim: Date = null;
  public natureza: Natureza = null;
}
