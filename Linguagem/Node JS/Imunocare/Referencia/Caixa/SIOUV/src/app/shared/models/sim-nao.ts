import { Entity } from 'app/arquitetura/shared/models/entity';

export class SimNao extends Entity {
  public value: string;
  public text: string;
}
