import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Util } from 'app/arquitetura/shared/util/util';
import { Item } from 'app/shared/models/item';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { Situacao } from 'app/shared/models/situacao';
import { AssuntoService } from 'app/shared/services/manutencao/assunto.service';
import { Assunto } from 'app/shared/models/assunto';
import { Natureza } from 'app/shared/models/natureza';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Unidade } from 'app/shared/models/unidade';
import { ItemNatureza } from 'app/shared/models/item-natureza';
import { ItemNaturezaId} from 'app/shared/models/item-natureza-id';
import { ManterItemService } from 'app/shared/services/manutencao/manter-item.service';



@Component({
    selector: 'app-manter-item-cadastro',
    templateUrl: './manter-item-cadastro.component.html',
    styleUrls: ['./manter-item-cadastro.component.css']
  })
  export class ManterItemCadastroComponent extends BaseComponent implements AfterViewInit {
  /**
  * Nomes dos formControlName dos campos
  */
  nameItem = 'noItemAssunto';
  nameUnidadeResponsavel = 'unidade';
  nameSituacao = 'ativo';
  nameAssunto = 'assunto';
  nameCarteiraTrabalho = 'carteiraTrabalho';
  nameContratoHabitacao = 'contratoHabitacao';
  nameCartaoCredito = 'cartaoCredito';
  nameCorrespondenteBancario = 'correspondenteBancario';
  nameRevendedorLoterico = 'revendedorLoterico';
  nameNis = 'nis';
  nameItemNatureza = 'itensNatureza';
  nameAgencia = 'agencia';
  nameInformacaoBancaria = 'informacaoBancaria';

  situacoes: Situacao[] = [];
  formulario: FormGroup;
  manterItem: Situacao = null;
  item: Item;
  assunto: Assunto = null;
  assuntos: Assunto[] = null;
  natureza: Natureza = null;
  naturezas: Natureza[] = null;
  titulo: string = null;
  tituloCampoCarteiraTrabalho: string = null;
  tituloContratoHabitacao: string = null;
  tituloCartaoCredito: string = null;
  tituloCorrespondenteBancario: string = null;
  tituloRevendedorLoterico: string = null;
  tituloNis: string = null;
  tituloInformacaoBancaria: string = null;
  tituloAgencia: string = null;
  unidadeSelecionada: Unidade;
  itensNaturezaTouched = false;
  itensNaturezaSelected: Natureza[] = Array<Natureza>();

  constructor(protected messageService: MessageService,
    private cdr: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private comumService: ComumService,
    private assuntoService: AssuntoService,
    private naturezaService: NaturezaService,
    private router: Router,
    private itemService: ManterItemService,
    private route: ActivatedRoute,
  ) {
    super(messageService);
    this.item = new Item();
    this.titulo = 'Incluir';
    this.carregarSituacoes();
    this.carregarAssuntos();
    this.carregarNaturezas();
    this.carregarCamposObrigatorios();
    this.editar();
    this.createFormGroup();
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  carregarCamposObrigatorios() {
    this.tituloCampoCarteiraTrabalho = 'Número e Série – Carteira de Trabalho';
    this.tituloContratoHabitacao = 'Contrato Habitação';
    this.tituloCartaoCredito = 'Cartão Crédito';
    this.tituloCorrespondenteBancario = 'Correspondente Bancário';
    this.tituloRevendedorLoterico = 'Código Rev. Lotérico';
    this.tituloNis = 'NIS/PIS';
    this.tituloInformacaoBancaria = 'Agência/Operação/Conta Corrente';
    this.tituloAgencia = 'Agência';
  }

  carregarNaturezas() {
    this.naturezaService.get().subscribe(
      (naturezas: Natureza[]) => {
        this.naturezas = naturezas;
    },
    error => {
      this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as naturezas.');
      console.log('Erro ao recuperar as naturezas:', error);
    });
  }

  carregarAssuntos() {
    this.assuntoService.consultarAssuntoAtivo((assuntos: Assunto[]) => {
      this.assuntos = assuntos;
    });
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({
        noItemAssunto: [this.item.noItemAssunto, [ Validators.required ]],
        unidade: [this.item.unidade, [Validators.required]],
        ativo: [this.item.ativo, [ Validators.required ]],
        assunto: [this.item.assunto, [ Validators.required ]],
        carteiraTrabalho: [this.isChecked(this.item.carteiraTrabalho), [ Validators.required ]],
        contratoHabitacao: [this.isChecked(this.item.contratoHabitacao)],
        cartaoCredito: [this.isChecked(this.item.cartaoCredito)],
        correspondenteBancario: [this.isChecked(this.item.correspondenteBancario)],
        revendedorLoterico: [this.isChecked(this.item.revendedorLoterico)],
        nis: [this.isChecked( this.item.nis)],
        informacaoBancaria: [this.isChecked(this.item.informacaoBancaria)],
        agencia: [this.isChecked(this.item.agencia)],
    });
  }

  carregarSituacoes(): void {
    this.comumService.listarSituacoesSemOpcaoTodos().subscribe(
        (situacoes: Situacao[]) => {
          this.situacoes = situacoes;
          //this.item.situacao = this.situacoes[0].value;
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
          console.log('Erro ao consultar situações:', error);
    });
  }

  voltar(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  limpar(): void {
    this.item = new Item();
    this.itensNaturezaTouched = false;
    this.formulario.reset();
  }

  salvar() {
    if (this.validarFormularioAll(this.formulario, [this.item.itensNatureza])) {
      this.montarObj();
      /*
      if (this.isNew()) {
        this.itemService.post(this.item).subscribe(
          () => {
            this.router.navigate(['.'], { relativeTo: this.route.parent });
            this.messageService.addMsgSuccess('Item inserido com sucesso.');
          },
          error => {
            this.messageService.addMsgDanger('Ocorreu um erro ao incluir item.');
            console.log('Erro ao incluir ao item.', error);
          }
        );
      } else {
        this.itemService.put(this.item).subscribe(
          () => {
            this.router.navigate(['.'], { relativeTo: this.route.parent });
            this.messageService.addMsgSuccess('Item alterado com sucesso.');
          },
          error => {
            this.messageService.addMsgDanger('Ocorreu um erro ao alterar o Item.');
            console.log('Ocorreu um erro ao alterar o Item.', error);
          }
        );
      }*/
      console.log(this.item);
    } else {
      this.messageService.addMsgDanger('Preencha os campos obrigatórios.');
    }
  }

  isNew(): boolean {
    return Util.isEmpty(this.item.id);
  }

  montarObj(): void {
    const itensNatureza =  this.item.itensNatureza;
    const usuario = this.item.usuario;
    this.item = this.getClone(this.formulario.value);
    this.item.itensNatureza = itensNatureza;
    this.item.usuario = usuario;
    this.item.agencia =  Util.convertBoolToSN(Util.convertSNtoBool(this.item.agencia));
    this.item.cartaoCredito = Util.convertBoolToSN(Util.convertSNtoBool(this.item.cartaoCredito));
    this.item.carteiraTrabalho = Util.convertBoolToSN(Util.convertSNtoBool(this.item.carteiraTrabalho));
    this.item.contratoHabitacao = Util.convertBoolToSN(Util.convertSNtoBool(this.item.contratoHabitacao));
    this.item.correspondenteBancario = Util.convertBoolToSN(Util.convertSNtoBool(this.item.correspondenteBancario));
    this.item.informacaoBancaria = Util.convertBoolToSN(Util.convertSNtoBool(this.item.informacaoBancaria));
    this.item.nis = Util.convertBoolToSN(Util.convertSNtoBool(this.item.nis));
    this.item.revendedorLoterico = Util.convertBoolToSN(Util.convertSNtoBool(this.item.revendedorLoterico));
    console.log(this.item);
  }

  isCheckedItensNatureza(tipo: Natureza): boolean {
    if (!this.item.itensNatureza) {
      return false;
    }
    for (let index = 0; index < this.item.itensNatureza.length; index++) {
      if (this.item.itensNatureza[index].natureza.id  === tipo.id) {
        return true;
      }
    }
    return false;
  }

  validarBloqueio(valor: boolean, tipo: number): boolean {
    return false;
  }

  changeTouched(value: any, isChecked: boolean, list: Array<any>): void {
    this.itensNaturezaTouched = true;
    this.onChangeCheckbox(value, isChecked, list);
  }

  isNewItensNatureza(item: Item, natureza: Natureza): ItemNatureza {
    const itensNaturezaId = new ItemNaturezaId(item.id, natureza.id);
    return new ItemNatureza(itensNaturezaId, natureza, null);
  }

  selectItensNatureza(selecionado: Natureza): ItemNatureza {
    return this.isNewItensNatureza(this.item, selecionado);
  }

  isChecked(value: string): boolean {
    return Util.convertSNtoBool(value);
  }

  editar(): void {
    this.route.params.subscribe(
      (params: any) => {
        const id: number = params['id'];
        if (!Util.isEmpty(id)) {
          this.itemService.get(id).subscribe(
            (item: Item) => {
              this.item = item;
              for (let index = 0; index < this.item.itensNatureza.length; index++) {
                this.item.itensNatureza[index] = this.isNewItensNatureza(this.item, this.item.itensNatureza[index].natureza);
              }
              this.updateFormGroup();
            },
            error => {
              this.messageService.addMsgDanger('Ocorreu um erro ao carregar o item.');
              console.log('Erro ao carregar Item.', error);
            });
        }
      });
  }

  updateFormGroup(): void {
    console.log(this.item);
    this.setValue(this.formulario, this.nameItem, this.item.noItemAssunto);
    this.setValue(this.formulario, this.nameUnidadeResponsavel, this.item.unidade);
    this.setValue(this.formulario, this.nameSituacao, this.item.ativo);
    this.setValue(this.formulario, this.nameAssunto, this.item.assunto);
    this.setValue(this.formulario, this.nameCarteiraTrabalho, Util.convertSNtoBool(this.item.carteiraTrabalho));
    this.setValue(this.formulario, this.nameContratoHabitacao, Util.convertSNtoBool(this.item.contratoHabitacao));
    this.setValue(this.formulario, this.nameCartaoCredito, Util.convertSNtoBool(this.item.cartaoCredito));
    this.setValue(this.formulario, this.nameCorrespondenteBancario, Util.convertSNtoBool(this.item.correspondenteBancario));
    this.setValue(this.formulario, this.nameRevendedorLoterico, Util.convertSNtoBool(this.item.revendedorLoterico));
    this.setValue(this.formulario, this.nameNis, Util.convertSNtoBool(this.item.nis));
    this.setValue(this.formulario, this.nameAgencia, Util.convertSNtoBool(this.item.agencia));
    this.setValue(this.formulario, this.nameInformacaoBancaria, Util.convertSNtoBool(this.item.informacaoBancaria));
  }

  isSelectAssunto(assunto: Assunto, posicao: number): Boolean {
    return (this.equals(this.assuntos[posicao], assunto));
  }

  compareFn(c1: Assunto, c2: Assunto): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

}
