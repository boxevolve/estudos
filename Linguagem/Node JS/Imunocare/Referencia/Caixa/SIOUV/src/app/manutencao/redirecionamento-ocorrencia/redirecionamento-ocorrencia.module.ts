import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RedirecionamentoOcorrenciaRoutingModule } from './redirecionamento-ocorrencia-routing.module';
import { RedirecionamentoOcorrenciaCadastroComponent } from './redirecionamento-ocorrencia-cadastro/redirecionamento-ocorrencia-cadastro.component';
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { UnidadeModule } from 'app/shared/components/unidade/unidade.module';
import { SiouvTableModule } from 'app/shared/components/table/table.module';

@NgModule({
  imports: [
    CommonModule,
    RedirecionamentoOcorrenciaRoutingModule,
    DirectivesModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    TemplatesModule,
    UnidadeModule.forRoot(),
    SiouvTableModule,
  ],
  declarations: [RedirecionamentoOcorrenciaCadastroComponent]
})
export class RedirecionamentoOcorrenciaModule { }
