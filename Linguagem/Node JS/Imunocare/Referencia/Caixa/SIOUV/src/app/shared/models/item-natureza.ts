import { ItemNaturezaId } from './item-natureza-id';
import { Natureza } from './natureza';
import { Item } from './item';

export class ItemNatureza {
  constructor (
    public id: ItemNaturezaId,
    public natureza: Natureza,
    public itemAssunto: Item
  ) {}
}
