import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Natureza } from 'app/shared/models/natureza';
import { TipoOcorrencia } from 'app/shared/models/tipo-ocorrencia';
import { Situacao } from 'app/shared/models/situacao';
import { SimNao } from 'app/shared/models/sim-nao';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { TipoOcorrenciaNatureza } from 'app/shared/models/tipo-ocorrencia-natureza';
import { TipoOcorrenciaService } from 'app/shared/services/manutencao/tipo-ocorrencia.service';
import { Util } from 'app/arquitetura/shared/util/util';
import { RegraDirecionamento } from 'app/shared/models/regraDirecionamento';

@Component({
  selector: 'app-natureza-cadastro',
  templateUrl: './natureza-cadastro.component.html',
  styleUrls: ['./natureza-cadastro.component.css']
})
export class NaturezaCadastroComponent extends BaseComponent implements AfterViewInit {

  /**
	* Nomes dos formControlName dos campos
	*/
  nameNatureza = 'nome';
  nameTipoOcorrencia = 'tiposOcorrenciaNatureza';
  nameSituacao = 'ativa';
  nameRestrita = 'restrito';
  nameAceite = 'aceite';
  nameOrientacao = 'orientacao';
  nameUnidadeResponsavel = 'unidadeResponsavelFinal';
  nameDesconsideraAvlco = 'desconsideraAvlco';
  nameBotaoSalvar = 'Confirmar Inclusão';

  formulario: FormGroup;
  natureza: Natureza;

  tiposOcorrencia: TipoOcorrencia[] = [];
  tiposOcorrenciaTouched = false;
  situacoes: Situacao[] = [];
  simNao: SimNao[] = [];

  bloqueiaTipoInterna: boolean;
  bloqueiaTipoSac: boolean;
  bloqueiaTipoOuvidoria: boolean;

  constructor(
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    protected messageService: MessageService,
    private naturezaService: NaturezaService,
    private tipoOcorrenciaService: TipoOcorrenciaService,
    private comumService: ComumService
  ) {
    super(messageService);
    this.natureza = new Natureza();
    this.carregarTiposOcorrencia();
    this.carregarSituacoes();
    this.carregarSimNao();
    this.editar();
    this.createFormGroup();
    this.validarFormulario(this.formulario);
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  carregarTiposOcorrencia(): void {
    this.tipoOcorrenciaService.listarTiposOcorrenciaSemAcompanhamento((tiposOcorrencia: TipoOcorrencia[]) => {
      this.tiposOcorrencia = tiposOcorrencia;
    });
  }

  carregarSituacoes(): void {
    this.comumService.listarSituacoesSemOpcaoTodos().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
        this.setValoresPadrao(this.situacoes);
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
        console.log('Erro ao consultar situações:', error);
      }
    );
  }

  carregarSimNao(): void {
    this.comumService.listarSimNao().subscribe(
      (simNao: SimNao[]) => {
        this.simNao = simNao;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
        console.log('Erro ao consultar situações:', error);
      }
    );
  }

  editar(): void {
    this.route.params.subscribe(
      (params: any) => {
        const id: number = params['id'];
        if (!Util.isEmpty(id)) {
          this.naturezaService.get(id).subscribe(
            (natureza: Natureza) => {
              this.natureza = natureza;
              for (let index = 0; index < this.natureza.tiposOcorrenciaNatureza.length; index++) {
                this.natureza.tiposOcorrenciaNatureza[index] =
                  this.newTipoOcorrenciaNatureza(this.natureza, this.natureza.tiposOcorrenciaNatureza[index].tipoOcorrencia);
              }
              this.updateFormGroup();
              this.nameBotaoSalvar = 'Confirmar Alteração';
            },
            error => {
                this.messageService.addMsgDanger('Ocorreu um erro ao carregar a natureza.');
                console.log('Erro ao carregar natureza:', error);
            }
          );
        }
      });
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      id: [this.natureza.id],
      nome: [this.natureza.nome, [ Validators.required ]],
      ativa: [this.natureza.ativa, [ Validators.required ]],
      restrito: [this.isRestrita()],
      aceite: [this.natureza.aceite, [ Validators.required ]],
      orientacao: [this.natureza.orientacao, [ Validators.required ]],
      unidadeResponsavelFinal: [this.natureza.unidadeResponsavelFinal, [ Validators.required ]],
      desconsideraAvlco: [this.natureza.desconsideraAvlco]
    });
  }

  updateFormGroup(): void {
    this.setValue(this.formulario, 'id', this.natureza.id);
    this.setValue(this.formulario, this.nameNatureza, this.natureza.nome);
    this.setValue(this.formulario, this.nameSituacao, this.natureza.ativa);
    this.setValue(this.formulario, this.nameRestrita, this.isRestrita());
    this.setValue(this.formulario, this.nameAceite, this.natureza.aceite);
    this.setValue(this.formulario, this.nameOrientacao, this.natureza.orientacao);
    this.setValue(this.formulario, this.nameUnidadeResponsavel, this.natureza.unidadeResponsavelFinal);
    this.setValue(this.formulario, this.nameDesconsideraAvlco, this.natureza.desconsideraAvlco);
  }

  selectTipoOcorrenciaNatureza(selecionado: TipoOcorrencia): TipoOcorrenciaNatureza {
    return this.newTipoOcorrenciaNatureza(this.natureza, selecionado);
  }

  isNew(): boolean {
    return Util.isEmpty(this.natureza.id);
  }

  salvar(): void {
    if (!this.validarFormularioAll(this.formulario, [this.natureza.tiposOcorrenciaNatureza])) {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
    }
    this.montarObj();
    this.naturezaService.verificarDuplicidade(this.natureza).subscribe(
      (isDuplicado: boolean) => {
        if (Util.isEmpty(isDuplicado) ? false : isDuplicado) {
          this.messageService.addMsgDanger('Registro já cadastrado.');
        } else {
          this.finalizarSalvar();
        }
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao validar a duplicidade da natureza.');
        console.log('Erro ao validar a duplicidade da natureza:', error);
      }
    );
  }

  finalizarSalvar(): void {
    if (this.isNew()) {
      this.naturezaService.post(this.natureza).subscribe(
        (natureza: Natureza) => {
          this.router.navigate(['.'], { relativeTo: this.route.parent });
          this.messageService.addMsgSuccess('Natureza inserida com sucesso.');
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao incluir a natureza.');
          console.log('Erro ao incluir natureza:', error);
        }
      );
    } else {
      this.naturezaService.put(this.natureza).subscribe(
        (natureza: Natureza) => {
          this.router.navigate(['.'], { relativeTo: this.route.parent });
          this.messageService.addMsgSuccess('Natureza alterado com sucesso.');
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao alterar a natureza.');
          console.log('Erro ao alterar motivo:', error);
        }
      );
    }
  }

  limpar(): void {
    this.natureza = new Natureza();
    this.setValoresPadrao(this.situacoes);
    this.tiposOcorrenciaTouched = false;
    this.updateFormGroup();
  }

  setValoresPadrao(situacoes: Situacao[]): void {
    if (situacoes && situacoes.length > 0
     && this.natureza && !this.natureza.ativa) {
      this.natureza.ativa = situacoes[0].value;
    }
    this.updateFormGroup();
  }

  voltar(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  irHome(): void {
    this.router.navigate(['/home']);
  }

  irManterNatureza(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  montarObj(): void {
    const tiposOcorrenciaNatureza = this.natureza.tiposOcorrenciaNatureza;
    const usuario = this.natureza.usuario;
    this.natureza = this.getClone(this.formulario.value);
    this.natureza.usuario = usuario;
    this.natureza.tiposOcorrenciaNatureza = tiposOcorrenciaNatureza;
    this.natureza.restrito = Util.convertBoolToSN(this.isRestrita());
  }

  carregarValidacoes(): void {
    for (let index = 0; index < this.tiposOcorrencia.length; index++) {
      this.verificarRegistroVinculado(this.natureza, this.tiposOcorrencia[index], index);
    }
  }

  verificarRegistroVinculado(natureza: Natureza, tipoOcorrencia: TipoOcorrencia, tipo: number): void {
    const tipoOcorrenciaNatureza = this.newTipoOcorrenciaNatureza(natureza, tipoOcorrencia);
    this.naturezaService.verificarRegistroVinculado(tipoOcorrenciaNatureza).subscribe(
      (isRegistroVinculado: any) => {
        this.validarBloqueio(Util.isEmpty(isRegistroVinculado) ? false : isRegistroVinculado, tipo);
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao verificar registro vinculado.');
        console.log('Erro ao verificar registro vinculado:', error);
      }
    );
  }

  validarBloqueio(valor: boolean, tipo: number): boolean {
    let bloqueio = false;
    const isAtribuir = !Util.isEmpty(valor);
    if (tipo === 0) {
      this.bloqueiaTipoInterna = isAtribuir ? valor : this.bloqueiaTipoInterna;
      bloqueio = this.bloqueiaTipoInterna;
    } else if (tipo === 1) {
      this.bloqueiaTipoOuvidoria = isAtribuir ? valor : this.bloqueiaTipoOuvidoria;
      bloqueio = this.bloqueiaTipoOuvidoria;
    } else if (tipo === 2) {
      this.bloqueiaTipoSac = isAtribuir ? valor : this.bloqueiaTipoSac;
      bloqueio = this.bloqueiaTipoSac;
    }
    return Util.isEmpty(bloqueio) ? false : bloqueio;
  }

  isRestrita(): boolean {
    return Util.convertSNtoBool(this.natureza.restrito);
  }

  isCheckTiposOcorrencia(tipo: TipoOcorrencia): boolean {
    if (!this.natureza.tiposOcorrenciaNatureza) {
      return false;
    }
    for (let index = 0; index < this.natureza.tiposOcorrenciaNatureza.length; index++) {
      if (this.natureza.tiposOcorrenciaNatureza[index].tipoOcorrencia.id === tipo.id) {
        return true;
      }
    }
    return false;
  }

  isBloqueiaCamposPrincipais(): boolean {
    return this.bloqueiaTipoInterna || this.bloqueiaTipoOuvidoria || this.bloqueiaTipoSac;
  }

  changeTouched(value: any, isChecked: boolean, list: Array<any>): void {
    this.tiposOcorrenciaTouched = true;
    this.onChangeCheckbox(value, isChecked, list);
  }

}
