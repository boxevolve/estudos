import { FormComponent } from './../form.component';
import { MessageService } from './../messages/message.service';
import { BaseComponent } from './../base.component';
import { FormGroup, FormControl } from '@angular/forms';
import { Input, OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.css']
})
export class InputDateComponent extends BaseComponent implements OnInit {
  @Input() label: string;
  @Input() control: string;
  @Input() form: FormGroup;
  @Input() placeholder: string;
  @Input() required = false;

  bsConfig = {
    containerClass: 'theme-dark-blue',
    showWeekNumbers: false
  };

  constructor(
    private localeService: BsLocaleService,
    protected messageService: MessageService
  ) {
    super(messageService);
    defineLocale('pt-br', ptBrLocale);
    this.localeService.use('pt-br');
  }

  ngOnInit() {
    this.form.get(this.control).valueChanges.subscribe(value => {
      if (value && value.toString().toUpperCase() === 'INVALID DATE') {
        this.form.get(this.control).setValue(null);
      }
    });
  }
}
