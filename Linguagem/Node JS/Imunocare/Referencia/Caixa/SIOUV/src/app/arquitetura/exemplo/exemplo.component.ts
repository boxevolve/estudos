import { Component } from '@angular/core';

import { MessageService } from 'app/shared/components/messages/message.service';
import { BaseComponent } from 'app/shared/components/base.component';
import { ExemploService } from 'app/arquitetura/shared/services/exemplo/exemplo.service';
import { Exemplo } from 'app/arquitetura/shared/models/exemplo';
import { Util } from 'app/arquitetura/shared/util/util';

@Component({
	selector: 'app-exemplo',
	templateUrl: './exemplo.component.html',
	styleUrls: ['./exemplo.component.scss']
})
export class ExemploComponent extends BaseComponent {
	constructor(
		protected messageService: MessageService,
		private exemploService: ExemploService
	) {
		super(messageService);
	}

	async cadastroExemplo() {
		let id1: number;
		let id2: number;
		let exemplo: Exemplo;

		exemplo = {
			id: 0,
			nome: 'João',
			idade: 30,
			ultimaModificacao: '2017-12-30T11:22:33'
		};

		console.log('Incluindo exemplo ...');
		this.exemploService.post(exemplo).subscribe(
			(pexemplo: Exemplo) => { id1 = pexemplo.id; console.log(pexemplo); },
			error => { console.log('Erro ao incluir exemplo:'); console.log(error); }
		);

		await Util.sleep(2000);

		console.log('Consultando exemplo ' + id1 + ' ...');
		this.exemploService.get(id1).subscribe(
			(pexemplo: Exemplo) => { console.log(pexemplo); },
			error => { console.log('Erro ao recuperar exemplo ' + id1 + ':'); console.log(error); }
		);

		await Util.sleep(2000);

		console.log('Consultando com erro ...');
		this.exemploService.consultarComErro().subscribe(
			(pexemplo: Exemplo) => { console.log(pexemplo); },
			error => { console.log('Erro customizado:'); console.log(error); }
		);
		
		await Util.sleep(2000);

		exemplo = {
			id: 0,
			nome: "Maria",
			idade: 29,
			ultimaModificacao: "2017-12-29T09:15:57"
		};

		console.log('Incluindo exemplo ...');
		this.exemploService.post(exemplo).subscribe(
			(pexemplo: Exemplo) => { id2 = pexemplo.id; console.log(pexemplo); },
			error => { console.log('Erro ao incluir exemplo:'); console.log(error); }
		);

		await Util.sleep(2000);

		console.log('Consultando exemplos ...');
		this.exemploService.get().subscribe(
			exemplos => { console.log(exemplos); },
			error => { console.log('Erro ao recuperar exemplos:'); console.log(error); }
		);

		exemplo.id = id2;
		exemplo.nome = "Mariana";
		exemplo.idade = 33;

		await Util.sleep(2000);

		console.log('Alterando exemplo ' + id2 + ' ...');
		this.exemploService.put(exemplo).subscribe(
			(pexemplo: Exemplo) => { console.log(pexemplo); },
			error => { console.log('Erro ao alterar exemplo ' + id2 + ':'); console.log(error); }
		);

		await Util.sleep(2000);

		console.log('Consultando exemplos ...');
		this.exemploService.get().subscribe(
			exemplos => { console.log(exemplos); },
			error => { console.log('Erro ao recuperar exemplos:'); console.log(error); }
		);

		await Util.sleep(2000);

		console.log('Excluindo exemplo ' + id1 + ' ...');
		this.exemploService.delete(id1).subscribe(
			resultado => { console.log(resultado); },
			error => { console.log('Erro ao excluir exemplo ' + id1 + ':'); console.log(error); }
		);

		await Util.sleep(2000);

		console.log('Consultando exemplos ...');
		this.exemploService.get().subscribe(
			exemplos => { console.log(exemplos); },
			error => { console.log('Erro ao recuperar exemplos:'); console.log(error); }
		);

		await Util.sleep(2000);

		console.log('Excluindo exemplo ' + id2 + ' ...');
		this.exemploService.delete(id2).subscribe(
			resultado => { console.log(resultado); },
			error => { console.log('Erro ao excluir exemplo ' + id2 + ':'); console.log(error); }
		);

		await Util.sleep(2000);

		console.log('Consultando exemplos ...');
		this.exemploService.get().subscribe(
			exemplos => { console.log(exemplos); },
			error => { console.log('Erro ao recuperar exemplos:'); console.log(error); }
		);
	}
}
