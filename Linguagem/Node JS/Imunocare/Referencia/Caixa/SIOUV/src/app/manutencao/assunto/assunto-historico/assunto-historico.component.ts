import { DATA_MASK, MATRICULA_MASK } from './../../../shared/util/masks';
import { AssuntoHistoricoService } from './../../../shared/services/manutencao/assunto-historico.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from './../../../shared/components/messages/message.service';
import { AssuntoHistorico } from './../../../shared/models/assunto-historico';
import { ComumService } from './../../../shared/services/comum/comum.service';

@Component({
  selector: 'app-assunto-historico',
  templateUrl: './assunto-historico.component.html',
  styleUrls: ['./assunto-historico.component.css']
})
export class AssuntoHistoricoComponent extends BaseComponent {
  formulario: FormGroup;
  pagina = 1;
  itens = 5;
  historicos: AssuntoHistorico[] = null;
  matriculaMask = MATRICULA_MASK;
  operacoes = [
    { id: 1, descricao: 'Inclusão' },
    { id: 2, descricao: 'Alteração' },
    { id: 3, descricao: 'Exclusão' }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    protected messageService: MessageService,
    private assuntoHistoricoService: AssuntoHistoricoService,
    private comumService: ComumService
  ) {
    super(messageService);

    this.formulario = this.formBuilder.group({
      usuario: [null],
      dataInicial: [null, [Validators.required]],
      dataFinal: [null, [Validators.required]],
      assunto: [null, [Validators.maxLength(100), Validators.minLength(3)]]
    });
  }

  consultar() {
    if (this.formulario.valid) {
      const selectedIds = this.formulario.value.operacoes
      .map((v, i) => v ? this.operacoes[i].id : null).filter(v => v !== null);
      if (this.getAssunto().value || this.getUsuario().value || selectedIds.length > 0) {
        this.assuntoHistoricoService.consultar(
            this.getUsuario().value,
            selectedIds.toString(),
            this.getDataInicial().value,
            this.getDataFinal().value,
            this.getAssunto().value
          ).subscribe((historicos: AssuntoHistorico[]) => {
              this.historicos = historicos;
              if (historicos && historicos.length === 0) {
                this.messageService.addMsgDanger('Nenhum registro encontrado.');
              }
            },
            error => {
              this.messageService.addMsgDanger(
                'Ocorreu um erro ao pesquisar os históricos de assunto.'
              );
              console.log('Erro ao consultar os históricos de assunto:', error);
            }
          );
      } else {
        this.messageService.addMsgDanger(
          'Ao menos um desses parâmetros deve ser informado: Usuário - Operação - Assunto.'
        );
      }
    } else {
      this.messageService.addMsgDanger('Preencha os campos obrigatórios.');
    }
  }

  voltar() {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
    this.formulario.reset();
  }

  limpar() {
    this.formulario.reset();
  }

  getUsuario() {
    return this.formulario.get('usuario');
  }

  getOperacao() {
    return this.formulario.get('operacoes');
  }

  getDataInicial() {
    return this.formulario.get('dataInicial');
  }

  getDataFinal() {
    return this.formulario.get('dataFinal');
  }

  getAssunto() {
    return this.formulario.get('assunto');
  }
}
