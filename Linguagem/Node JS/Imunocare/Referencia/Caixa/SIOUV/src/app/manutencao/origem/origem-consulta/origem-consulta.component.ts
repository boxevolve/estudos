import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Origem } from 'app/shared/models/origem';
import { OrigemService } from 'app/shared/services/manutencao/tipo-origem.service';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';

@Component({
  selector: 'app-origem-consulta',
  templateUrl: './origem-consulta.component.html',
  styleUrls: ['./origem-consulta.component.css']
})
export class OrigemConsultaComponent extends BaseComponent {

    origens: Origem[] = null;
    dataList: SiouvTable;

    constructor(
        protected messageService: MessageService,
        private origemService: OrigemService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        super(messageService);
        this.carregarOrigens();
    }

    carregarOrigens(): void {
        this.origemService.todos().subscribe(
          (origens: Origem[]) => {
            this.origens = origens;
            this.atualizarDataList();
          },
          error => {
            this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as origens.');
            console.log('Erro ao recuperar as origens:', error);
          }
        );
    }

    atualizarDataList() {
        this.dataList = this.createSiouvTable(this.origens);
    }

    createSiouvTable(list: Origem[]): SiouvTable {
        let idTable = 'consultaOrigem';
        const creator: SiouvTableCreator<Origem> = new SiouvTableCreator();
        // Cabeçalhos
        // Id - Texto - Class - Style
        let headers = [
          creator.addHeader('origem', 'Origem', 25, 'text-center', {'vertical-align': 'middle'}),
          creator.addHeader('tiposOcorrenciaOrigem', 'Tipo de Ocorrência', 25, 'text-center', {'vertical-align': 'middle'}),
          creator.addHeader('ativo', 'Situação', 25, 'text-center', {'vertical-align': 'middle'}),
          creator.addHeader('acao', 'Ação', 25, 'text-center', {'vertical-align': 'middle'})
        ];
        // Linhas
        // Id - Texto - Class - Style - Objeto
        let rows = [];
        for (let index = 0; index < list.length; index++) {
          let object = list[index];
          let cols = [
            creator.addCol(idTable, index, 'nome', object.nome, 'text-center', null, object),
            creator.addCol(idTable, index, 'tiposOcorrenciaNatureza', object.labelTiposOcorrenciaNatureza, 'text-center', null, object),
            creator.addCol(idTable, index, 'ativo', object.labelAtivo, 'text-center', null, object),
          ];
          rows.push(creator.addRow(idTable, index, cols, null, null, list[index]));
        }
        // Table
        // Id - Cabeçalhos - Linhas - Class - Style - Lista
        return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
      }

    incluirNovaOrigem(): void {
        this.router.navigate(['novo'], { relativeTo: this.route.parent });
    }

    excluir(origem: Origem): void {
      const indice: number = this.origens.findIndex(item => item.id == origem.id);
      this.origemService.verificarVinculo(origem).subscribe(
        (isVinculado: boolean) => {
            if (isVinculado) {
                this.messageService.addMsgDanger('Registro não poderá ser excluído, pois está vinculado a outros.');   
            } else {
                this.finalizarExcluir(indice, origem);
            }
        }
      );
      this.finalizarExcluir(indice, origem);
    }

    finalizarExcluir(indice: number, origem: Origem): void {
        this.origemService.delete(origem.id).subscribe(
            (origem: Origem) => {
                this.messageService.addMsgSuccess('Operação efetuada com sucesso.');
                if (indice >= 0) {
                    this.origens.splice(indice, 1);
                    this.atualizarDataList();
                }
            },
            error => {
                this.messageService.addMsgDanger('Ocorreu um erro ao excluir a origem.');
                console.log('Erro ao excluir a origem:', error);
            }
        );
    }

    alterar(origem: Origem): void {
        this.router.navigate([origem.id, 'editar'], { relativeTo: this.route.parent });
    }


}
