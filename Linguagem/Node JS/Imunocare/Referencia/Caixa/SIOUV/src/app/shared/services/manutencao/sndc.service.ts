import { Injectable } from '@angular/core';
import { UnidadeSndc } from 'app/shared/models/sndc';
import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class SndcService extends CrudHttpClientService<UnidadeSndc> {

  constructor(protected http: HttpClient) {
      super('manutencao/unidade-sndc', http);
   }

   consultarSndc(sndc: UnidadeSndc): Observable<UnidadeSndc[]> {
      let httpParams: HttpParams = new HttpParams();
      httpParams = httpParams.set('noUnidadeSndc', sndc.noUnidadeSndc);
      httpParams = httpParams.set('sgUf', sndc.sgUf);
      httpParams = httpParams.set('icAtivo', sndc.icAtivo);
      return this.http.get<UnidadeSndc[]>(this.url + '/consultar', this.options({ params: httpParams }));
   }
   consultarNoUnidadeSndc(sndc: UnidadeSndc): Observable<UnidadeSndc[]> {
      let httpParams: HttpParams = new HttpParams();
      httpParams = httpParams.set('noUnidadeSndc', sndc.noUnidadeSndc);
      httpParams = httpParams.set('sgUf', sndc.sgUf);
      return this.http.get<UnidadeSndc[]>(this.url + '/consultarNoUnidadeSndc', this.options({ params: httpParams }));
   }

}
