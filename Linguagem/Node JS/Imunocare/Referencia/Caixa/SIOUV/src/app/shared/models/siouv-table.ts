import { SiouvCol } from './siouv-col';
import { SiouvRow } from './siouv-row';

export class SiouvTable {
    constructor (
        public id: string,
        public title: string,
        public headers: SiouvCol[],
        public rows: SiouvRow[],
        public clazz: any,
        public style: any,
        public showPagination: boolean,
        public numberPage: number,
        public numberItems: number,
        public total: number,
        public listObject: any[]
    ) {}
}
