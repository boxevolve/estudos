import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClassificacaoOcorrenciaRoutingModule } from './classificacao-ocorrencia-routing.module';
import { ConsultarClassificacaoOcorrenciaComponent } from './consultar-classificacao-ocorrencia/consultar-classificacao-ocorrencia.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxPaginationModule } from 'ngx-pagination';
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ConsultarHistoricoClassificacaoComponent } from './consultar-historico-classificacao/consultar-historico-classificacao.component';
import { RegrasDirecionamentoModalComponent } from './regra-direcionamento-modal/regra-direcionamento-modal.component';
import { UnidadeModule } from 'app/shared/components/unidade/unidade.module';
import { SiouvTableModule } from 'app/shared/components/table/table.module';
import { CadastrarClassificacaoOcorrenciaComponent } from './cadastrar-classificacao-ocorrencia/cadastrar-classificacao-ocorrencia.component';
import { RegrasDirecionamentoBtnComponent } from './regra-direcionamento-btn/regra-direcionamento-btn.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    ComponentModule.forRoot(),
    SiouvTableModule,
    ClassificacaoOcorrenciaRoutingModule,
    BsDatepickerModule.forRoot(),
    UnidadeModule
  ],
  declarations: [
    RegrasDirecionamentoModalComponent,
    RegrasDirecionamentoBtnComponent,
    ConsultarClassificacaoOcorrenciaComponent,
    ConsultarHistoricoClassificacaoComponent,
    CadastrarClassificacaoOcorrenciaComponent,
  ],
  exports: [
    RegrasDirecionamentoModalComponent,
    RegrasDirecionamentoBtnComponent
  ]
})
export class ClassificacaoOcorrenciaModule { }
