import { Entity } from 'app/arquitetura/shared/models/entity';

export class PermissaoTransferenciaTodasUnidades extends Entity {
   public value: string;
   public text: string;
}