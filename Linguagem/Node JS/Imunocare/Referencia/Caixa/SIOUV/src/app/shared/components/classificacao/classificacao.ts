
export class Classificacao {
    public idTipoOcorrencia: number;
    public idOrigem: number;
    public idNatureza: number;
    public idAssunto: number;
    public idItem: number;
    public idMotivo: number;
}