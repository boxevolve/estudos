import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TextMaskModule } from 'angular2-text-mask';
import { NgxPaginationModule } from 'ngx-pagination';

// Aplicação
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { TextoAberturaCadastroComponent } from './cadastro/texto-abertura-cadastro.component';
import { TextoAberturaRoutingModule } from './texto-abertura.routing.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    ComponentModule.forRoot(),
    TextoAberturaRoutingModule
  ],
  declarations: [TextoAberturaCadastroComponent]
})
export class TextoAberturaModule {}
