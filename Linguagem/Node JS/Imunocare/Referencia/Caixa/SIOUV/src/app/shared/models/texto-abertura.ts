import { Entity } from 'app/arquitetura/shared/models/entity';

export class TextoAbertura extends Entity {
  public nome: string = null;
  public descricao: string = null;
  public nuTipoOcorrencia: number = null;
}
