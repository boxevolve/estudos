import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PaginationInstance } from 'ngx-pagination';

import { MessageService } from 'app/shared/components/messages/message.service';
import { ConfirmListener } from 'app/shared/components/messages/confirm-listener';
import { LoadingModalComponent as Loading } from 'app/shared/components/loading/loading-modal.component';
import { BaseComponent } from './base.component';
import { Entity } from 'app/arquitetura/shared/models/entity';

export class ListComponent<T extends Entity> extends BaseComponent implements OnInit {
	public entidade: any;
	public entidades: any[];
	public semRegistros = false;
	public errorMessage: string;
	public lblMessageConfirmOk: string = null;
	public lblMessageConfirmCancel: string = null;

	// Mensagens de ações
	public strMensagemPerguntaExclusao = 'Você deseja realmente excluir este registro?';
	public strMensagemSucessoExclusao = 'Você deseja realmente excluir este registro?';

	// Paginação
	public config: PaginationInstance = {
		currentPage: 1,
		itemsPerPage: 20,
		totalItems: 0
	};

	constructor(
		protected crudService: any,
		protected router: Router,
		protected messageService: MessageService
	) {
		super(messageService);
	}

	public ngOnInit() {
		this.getEntidades();
	}

	public getEntidades(filtros?: any) {
		const vm = this;
		// Realiza a verificação dos valores enviados
		filtros = this.limparFiltros(filtros);

		if (!filtros) {
			filtros = {
				page: this.config.currentPage
			};
		}
		if (filtros && !filtros.page) {
			filtros.page = this.config.currentPage;
		}

		if (filtros) {
			this.crudService.setFilter(filtros);
		} else {
			this.crudService.clearFilter();
		}

		Loading.start();
		this.crudService.get()
			.subscribe(
			entidades => {
				if (entidades.data.length > 0) {
					this.entidades = entidades.data;
					this.semRegistros = false;
					if (entidades.meta && entidades.meta.pagination) {
						vm.config.currentPage = entidades.meta.pagination.current_page;
						vm.config.totalItems = entidades.meta.pagination.total;
						vm.config.itemsPerPage = entidades.meta.pagination.per_page;
					}
				} else {
					this.entidades = [];
					this.semRegistros = true;

					// Zera os dados de paginação
					vm.config.currentPage = 1;
					vm.config.itemsPerPage = 20;
					vm.config.totalItems = 0;
				}
				Loading.stop();
			},
			error => {
				this.messageService.addMsgDanger('Não foi possível carregar a lista. Erro = ' + error);
				Loading.stop();
			}
			);
	}

	public delete(id: number) {
		this.messageService.addConfirmYesNo(this.strMensagemPerguntaExclusao,
			(): ConfirmListener => {
				this.crudService.delete(id).subscribe(
					res => {
						this.messageService.addMsgSuccess(this.strMensagemSucessoExclusao);
						this.excluirInicializar();
					},
					error => {
						this.messageService.addMsgDanger(this.trataMsgErroApi(error));
					}
				);
				return;
			},
			null, null,
			this.lblMessageConfirmOk,
			this.lblMessageConfirmCancel);
	}

	public excluirInicializar() {
		this.getEntidades();
	}

	/**
	 * Verifica todos os filtros e verifica
	 * @param filtros
	 */
	public limparFiltros(filtros) {
		const filtrosAjustados = {};
		if (filtros) {
			for (const item in filtros) {
				if (filtros.hasOwnProperty(item)) {
					if (filtros[item]) {
						filtrosAjustados[item] = filtros[item];
					}
				}
			}
		}
		return filtrosAjustados;
	}

	getPage(page: number) {
		const filtros = Object.assign(this.crudService.queryParams, { page: page });
		this.crudService.setFilter(filtros);
		this.getEntidades(filtros);
		return page;
	}
}
