import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TextMaskModule } from 'angular2-text-mask';
import { NgxPaginationModule } from 'ngx-pagination';

// Aplicação
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { SegurancaRoutingModule } from './seguranca.routing.module';
import { PerfilConsultaComponent } from 'app/arquitetura/seguranca/perfil/consulta/perfil-consulta.component';
import { PerfilCadastroComponent } from 'app/arquitetura/seguranca/perfil/cadastro/perfil-cadastro.component';
import { PerfilRecursoCadastroComponent } from 'app/arquitetura/seguranca/perfil-recurso/perfil-recurso-cadastro.component';
import { ComponentModule } from 'app/shared/components/component.module';

/**
 * Modulo Segurança
 **/
@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		FormsModule,
		ReactiveFormsModule,
		TextMaskModule,
		NgxPaginationModule,
		DirectivesModule.forRoot(),
		TemplatesModule,
		SegurancaRoutingModule,
		ComponentModule.forRoot()
	],
	declarations: [
		PerfilConsultaComponent,
		PerfilCadastroComponent,
		PerfilRecursoCadastroComponent
	]
})
export class SegurancaModule { }
