import { Component, Input, AfterViewInit, ChangeDetectorRef, OnInit, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { BaseComponent } from '../../base.component';
import { MessageService } from '../../messages/message.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { UnidadeService } from 'app/shared/services/comum/unidade.service';
import { UnidadeFiltro } from 'app/shared/models/unidade.filtro';
import { Unidade } from 'app/shared/models/unidade';
import { Util } from 'app/arquitetura/shared/util/util';
import { CODIGO_UNIDADE_MASK } from 'app/shared/util/masks';
import { UnidadeUtil } from 'app/shared/models/unidade-util';

@Component({
  selector: 'app-seleciona-unidade',
  templateUrl: './seleciona-unidade.component.html',
  styleUrls: ['./seleciona-unidade.component.css']
})
export class SelecionaUnidadeComponent extends BaseComponent implements AfterViewInit, OnInit, OnChanges {

  @Input() formulario: FormGroup;
  @Input() idBase = 'seleciona_unidade';
  @Input() label: string;
  @Input() required = false;
  @Input() name: string;
  @Input() disabled = false;
  @Input() showButtons = true;
  @Input() class: string;
  @Output() emitIdModal = new EventEmitter();

  @Input() modalExterna: string;
  @Input() unidadeExterna: Unidade;

  /**
  * Nomes dos formControlName dos campos
  */
  nameDescricao = this.idField(['descricao', this.idBase]);
  nameCodigo = this.idField(['codigo', this.idBase]);
  nameBtnModal = this.idField(['btnModal', this.idBase]);
  nameBtnHiddenModal = this.idField(['btnHiddenModal', this.idBase]);
  nameBtnLimpar = this.idField(['btnLimpar', this.idBase]);
  nameBtnHiddenLimpar = this.idField(['btnHiddenLimpar', this.idBase]);
  nameDisabled = 'disabled';
  nameModalExterna = 'modalExterna';
  nameUnidadeExterna = 'unidadeExterna';

  unidadeFiltro: UnidadeFiltro;
  descricao: string;
  codigo: string;
  unidadeSelecionada: Unidade;
  idModalUnidade: string;
  codigoUnidadeMask = CODIGO_UNIDADE_MASK;

  constructor(
    private cdr: ChangeDetectorRef,
    protected messageService: MessageService,
    private unidadeService: UnidadeService,
  ) {
    super(messageService);
    this.inicializar();
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  ngOnInit(): void {
    this.atualizarId();
    this.createFormGroup();
    this.disable(this.disabled);
    if (Util.isEmpty(this.class)) {
      this.class = 'col-md-4';
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const keyChanges = Object.keys(changes);
    if (this.contains(keyChanges, this.nameDisabled)) {
      this.disable(changes[this.nameDisabled].currentValue);
    }
    if (this.contains(keyChanges, this.nameModalExterna)) {
      this.receberIdModal(changes[this.nameModalExterna].currentValue);
    }
    if (this.contains(keyChanges, this.nameUnidadeExterna)) {
      this.receberUnidade(changes[this.nameUnidadeExterna].currentValue);
    }
  }

  inicializar() {
    this.unidadeFiltro = new UnidadeFiltro();
  }

  createFormGroup(): void {
    this.formulario.addControl(this.nameDescricao,
      this.required ? new FormControl(this.descricao, Validators.required) : new FormControl(this.descricao));
    this.formulario.addControl(this.nameCodigo,
      this.required ? new FormControl(this.codigo, Validators.required) : new FormControl(this.codigo));
    this.formulario.addControl(this.name,
      this.required ? new FormControl(this.unidadeSelecionada, Validators.required) : new FormControl(this.unidadeSelecionada));
  }

  updateFormGroup(): void {
    this.setValue(this.formulario, this.nameDescricao, this.descricao);
    this.setValue(this.formulario, this.nameCodigo, this.codigo);
    this.setValue(this.formulario, this.name, this.unidadeSelecionada);
  }

  receberIdModal(idModal: string): void {
    this.idModalUnidade = idModal;
    this.emitIdModal.emit(this.getClone(new UnidadeUtil(this.messageService, this.cdr).object(this.idModalUnidade,
      this.nameDescricao, this.nameBtnHiddenModal, this.nameBtnHiddenLimpar)));
  }

  atualizarId(): void {
    this.nameDescricao = this.idField(['descricao', this.idBase]);
    this.nameCodigo = this.idField(['codigo', this.idBase]);
    this.nameBtnModal = this.idField(['btnModal', this.idBase]);
    this.nameBtnHiddenModal = this.idField(['btnHiddenModal', this.idBase]);
    this.nameBtnLimpar = this.idField(['btnLimpar', this.idBase]);
    this.nameBtnHiddenLimpar = this.idField(['btnHiddenLimpar', this.idBase]);
  }

  receberUnidade(unidadeSelecionada: Unidade): void {
    this.unidadeSelecionada = unidadeSelecionada;
    this.alterarDescricao(unidadeSelecionada);
  }

  alterarDescricao(unidade: Unidade): void {
    if (unidade) {
      this.codigo = unidade.nuUnidade + '';
      this.descricao = this.getUnidadeDescricao(unidade, 1);
    } else {
      this.codigo = '';
      this.descricao = '';
    }
    this.updateFormGroup();
  }

  limpar(): void {
    this.receberUnidade(null);
  }

  disableModal(): boolean {
    return !Util.isEmpty(this.getValueForm(this.formulario, this.nameDescricao));
  }

  verificarCodigo(value: string): void {
    if (value.length === 4) {
      this.pesquisar(value);
    }
  }

  verificarCodigoBlur(value: string): void {
    if (value.length !== 0) {
      this.pesquisar(value);
    } else {
      this.limpar();
    }
  }

  pesquisar(value: any): void {
    const filtro = new UnidadeFiltro();
    filtro.nuUnidade = value;
    this.unidadeService.consultar(filtro, (unidades: Unidade[]) => {
      unidades.forEach(unidade => {
        this.receberUnidade(unidade);
      });
      if (unidades.length === 0) {
        $(this.idHashtag(this.nameBtnHiddenModal)).click();
        this.limpar();
      }
    }, this.getMessageError);
  }

  showLabel(): boolean {
    return !Util.isEmpty(this.label);
  }

  requiredLabel(): boolean {
    return this.required && this.showLabel();
  }

  disable(disabled: boolean): void {
    this.disabled = Util.isDefined(disabled) ? disabled : false;
    if (this.disabled) {
      this.disableField(this.formulario, this.nameDescricao);
      this.disableField(this.formulario, this.nameCodigo);
    } else {
      this.enableField(this.formulario, this.nameDescricao);
      this.enableField(this.formulario, this.nameCodigo);
    }
  }

  isModalExterna(): boolean {
    return Util.isEmpty(this.modalExterna);
  }

}
