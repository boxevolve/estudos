import { Component, Output, AfterViewInit, Input, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from '../../base.component';
import { MessageService } from '../../messages/message.service';
import { EventEmitter } from '@angular/core';
import { Unidade } from 'app/shared/models/unidade';
import { FormGroup, FormBuilder} from '@angular/forms';
import { Uf } from 'app/shared/models/uf';
import { SimNao } from 'app/shared/models/sim-nao';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { UnidadeService } from 'app/shared/services/comum/unidade.service';
import { UfService } from 'app/shared/services/comum/uf.service';
import { UnidadeFiltro } from 'app/shared/models/unidade.filtro';
import { Util } from 'app/arquitetura/shared/util/util';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { TipoUnidadeService } from 'app/shared/services/comum/tipo-unidade.service';
import { TipoUnidade } from 'app/shared/models/tipo-unidade';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';

@Component({
  selector: 'app-unidade-modal',
  templateUrl: './unidade-modal.component.html',
  styleUrls: ['./unidade-modal.component.css']
})
export class UnidadeModalComponent extends BaseComponent implements AfterViewInit {

  modalId = 'unidade';

  @Input() idBase = 'unidadeBase';
  @Output() selecionarUnidade = new EventEmitter();
  @Output() emitIdModal = new EventEmitter();

  /**
  * Nomes dos formControlName dos campos
  */
  nameCodigo = 'nuUnidade';
  nameNome = 'nome';
  nameSigla = 'sigla';
  nameUf = 'uf';
  nameIsAvancada = 'isAvancada';
  nameCidade = 'cidade';
  nameBairro = 'bairro';
  nameTipoUnidade = 'tipoUnidade';

  formUnidade: FormGroup;
  unidadeFiltro: UnidadeFiltro;
  isAvancada: string;
  ufs: Uf[];
  tiposUnidade: TipoUnidade[];
  simNao: SimNao[];
  unidades: Unidade[];
  dataList: SiouvTable;
  unidadeVinculadora: Unidade;

  constructor(
    protected messageService: MessageService,
    private cdr: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private comumService: ComumService,
    private unidadeService: UnidadeService,
    private ufService: UfService,
    private tipoUnidadeService: TipoUnidadeService
  ) {
    super(messageService);
    this.inicializar();
    this.carregarSimNao();
    this.carregarUfs();
    this.carregarTiposUnidade();
    this.createFormGroup();
  }

  ngAfterViewInit() {
    this.emitIdModal.emit(this.getClone(this.getIdModal([])));
    this.hiddenModal(this.getIdModal([]), () => {
      this.limpar();
    });
    this.atualizarDataList();
    this.cdr.detectChanges();
  }

  hide(): void {
    this.hideModal(this.getIdModal([]));
  }

  limpar(): void {
    this.inicializarUnidade();
    this.createFormGroup();
    this.isAvancada = this.getSN(false);
    this.atualizarDataList();
  }

  limparPesquisaAvancada(): void {
    this.setValue(this.formUnidade, this.nameCidade, null);
    this.setValue(this.formUnidade, this.nameBairro, null);
    this.setValue(this.formUnidade, this.nameTipoUnidade, null);
  }

  inicializar(): void {
    this.inicializarUnidade();
    this.ufs = [];
    this.simNao = [];
  }

  inicializarUnidade(): void {
    this.unidadeFiltro = new UnidadeFiltro();
    this.unidadeFiltro.isAvancada = this.getSN(false);
    this.unidades = [];
    this.unidadeVinculadora = null;
  }

  carregarSimNao(): void {
    this.comumService.listarSimNao().subscribe(
      (simNao: SimNao[]) => {
        this.simNao = simNao;
        this.isAvancada = this.getSN(false);
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao consultar sim e não.');
        console.log('Erro ao consultar sim e não:', error);
      }
    );
  }

  carregarUfs(): void {
    this.ufService.todos((ufs: Uf[]) => {
      this.ufs = ufs;
    });
  }

  carregarTiposUnidade(): void {
    this.tipoUnidadeService.todos((tiposUnidade: TipoUnidade[]) => {
      this.tiposUnidade = tiposUnidade;
    });
  }

  createFormGroup(): void {
    this.formUnidade = this.formBuilder.group({
      nuUnidade: [this.unidadeFiltro.nuUnidade],
      nome: [this.unidadeFiltro.nome],
      sigla: [this.unidadeFiltro.sigla],
      uf: [this.unidadeFiltro.uf],
      isAvancada: [this.unidadeFiltro.isAvancada],
      cidade: [this.unidadeFiltro.cidade],
      bairro: [this.unidadeFiltro.bairro],
      tipoUnidade: [this.unidadeFiltro.tipoUnidade]
    });
  }

  pesquisar(next?: (unidades: Unidade[]) => void): void {
    const msg = this.validar();
    if (Util.isEmpty(msg)) {
      this.montarObj();
      this.unidadeService.consultar(this.unidadeFiltro, (unidades: Unidade[]) => {
        if (next) {
          next(unidades);
        } else {
          this.setVinculadora(null);
          this.unidades = unidades;
          this.atualizarDataList();
        }
      }, this.getMessageError);
    } else {
      this.messageService.addMsgDanger(msg);
    }
  }

  pesquisarVinculadas(): void {
    this.pesquisar((unidades: Unidade[]) => {
      if (unidades.length > 1) {
        this.unidades = new Array<Unidade>();
        this.messageService.addMsgDanger('Não foi possível pesquisar as Unidades Vinculadas, mais de uma Unidade Vinculadora foi encontrada.');
        this.atualizarDataList();
      } else if (unidades.length !== 0) {
        const vinculadora = unidades[0];
        this.unidadeService.consultarUnidadesVinculadas(vinculadora, (unidadesVinculadas: Unidade[]) => {
          this.setVinculadora(vinculadora);
          this.unidades = unidadesVinculadas;
          this.atualizarDataList();
        });
      }
    });
  }

  montarObj(): void {
    this.unidadeFiltro = this.getClone(this.formUnidade.value);
  }

  validar(): string {
    let valido;
    if (this.isShowAvancada()) {
      valido = this.validarPeloMenosUm(this.formUnidade,
        [this.nameCodigo, this.nameNome, this.nameSigla, this.nameCidade, this.nameBairro]);
      if (!valido) {
        return 'Ao menos um desses parâmetros deve ser informado: Número Unidade, Nome, Sigla, Cidade, Bairro ou Tipo de Unidade.';
      }
      return this.validarPeloMenosUm(this.formUnidade, [this.nameUf]) ? null
        : 'Selecione a UF da unidade.';
    }
    valido = this.validarPeloMenosUm(this.formUnidade, [this.nameCodigo, this.nameNome, this.nameSigla]);
    return !valido ? 'Ao menos um desses parâmetros deve ser informado: Número Unidade, Nome ou Sigla.' : null;
  }

  atualizarDataList(): void {
    if (this.unidadeVinculadora) {
      this.dataList = this.createSiouvTableVinculadas(this.unidadeVinculadora, this.unidades);
    } else {
      this.dataList = this.createSiouvTable(this.unidades);
    }
  }

  createSiouvTable(list: Unidade[]): SiouvTable {
    const idTable = this.getIdModal(['table']);
    const creator: SiouvTableCreator<Unidade> = new SiouvTableCreator();
    // Cabeçalhos
    // Id - Texto - Class - Style
    const headers = [
      creator.addHeader(idTable + '_codigo', 'Código', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_sigla', 'Sigla', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_nome', 'Nome', 12.5, null, {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_uf', 'UF', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_cidade', 'Cidade', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_bairro', 'Bairro', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_endereco', 'Endereço', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_acao', 'Ação', 12.5, 'text-center', {'vertical-align': 'middle'})
    ];

    // Linhas
    // Id - Texto - Class - Style - Objeto
    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'codigo', object.nuUnidade, 'text-center', null, object),
        creator.addCol(idTable, index, 'sigla', object.sigla, 'text-center', null, object),
        creator.addCol(idTable, index, 'nome', object.nome, null, null, object),
        creator.addCol(idTable, index, 'uf', object.uf, 'text-center', null, object),
        creator.addCol(idTable, index, 'cidade', object.endereco ? object.endereco.noLocalidade : '', 'text-center', null, object),
        creator.addCol(idTable, index, 'bairro', object.endereco ? object.endereco.noBairro : '', 'text-center', null, object),
        creator.addCol(idTable, index, 'endereco', object.endereco ? object.endereco.noLogradouro : '', 'text-center', null, object),
      ];
      rows.push(creator.addRowSelect(idTable, index, cols, null, null, this.getIdModal([]), list[index]));
    }

    // Table
    // Id - Cabeçalhos - Linhas - Class - Style - Lista
    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

  createSiouvTableVinculadas(unidade: Unidade, list: Unidade[]): SiouvTable {
    const idTable = this.getIdModal(['table_vinculadas']);
    const creator: SiouvTableCreator<Unidade> = new SiouvTableCreator();
    const headers = [
      creator.addHeader(idTable + '_codigo_vinc', 'Código', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_sigla_vinc', 'Sigla', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_nome_vinc', 'Nome', 12.5, null, {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_uf_vinc', 'UF', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader(idTable + '_acao_vinc', 'Ação', 12.5, 'text-center', {'vertical-align': 'middle'})
    ];
    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'codigo', object.nuUnidade, 'text-center', null, object),
        creator.addCol(idTable, index, 'sigla', object.sigla, 'text-center', null, object),
        creator.addCol(idTable, index, 'nome', object.nome, null, null, object),
        creator.addCol(idTable, index, 'uf', object.uf, 'text-center', null, object)
      ];
      rows.push(creator.addRowSelect(idTable, index, cols, null, null, this.getIdModal([]), list[index]));
    }
    return creator.newSiouvTableTitle(idTable, this.titleTable(unidade), headers, rows, null, null, list);
  }

  titleTable(unidade: Unidade): string {
    return unidade ? 'Vinculada da ' + unidade.nuUnidade + ' - ' + unidade.sigla + ' - ' + unidade.nome  : '';
  }

  changeAvancada(simNao: string): void {
    this.isAvancada = simNao;
    if (Util.convertSNtoBool(simNao)) {
      this.limparPesquisaAvancada();
    }
  }

  isShowAvancada(): boolean {
    return this.isAvancada === this.getSN(true);
  }

  setVinculadora(unidade: Unidade): void {
    this.unidadeVinculadora = unidade;
  }

  selecionar(unidadeSelecionada: Unidade): void {
    this.selecionarUnidade.emit(this.getClone(unidadeSelecionada));
  }

  getSN(simNao: boolean): string {
    return this.simNao && this.simNao.length > 0 ? this.simNao[simNao ? 0 : 1].value : null;
  }

  getIdModal(fields: string[]): string {
    return this.idModal(this.modalId, this.idBase, fields);
  }

  getIdField(field: string): string {
    return this.idField([this.getIdModal([]), field]);
  }

}
