import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxPaginationModule } from 'ngx-pagination';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { UnidadeModule } from 'app/shared/components/unidade/unidade.module';
import { GrauSatisfacaoRoutingModule } from './grau-satisfacao.routing.module';
import { GrauSatisfacaoCadastroComponent } from './grau-satisfacao-cadastro/grau-satisfacao-cadastro.component';
import { GrauSatisfacaoConsultaComponent } from './grau-satisfacao-consulta/grau-satisfacao-consulta.component';
import { SiouvTableModule } from 'app/shared/components/table/table.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    ComponentModule.forRoot(),
    UnidadeModule,
    GrauSatisfacaoRoutingModule,
    SiouvTableModule
  ],
  declarations: [
    GrauSatisfacaoCadastroComponent,
    GrauSatisfacaoConsultaComponent
  ]
})
export class GrauSatisfacaoModule {}
