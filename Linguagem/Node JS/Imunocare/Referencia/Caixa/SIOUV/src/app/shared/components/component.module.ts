import { ManyCheckboxComponent } from './many-checkbox/many-checkbox.component';
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RequiredFieldComponent } from './required/required-field/required-field.component';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

// Componentes compartilhados para aplicação
import { DatalistComponent } from './datalist/datalist.component';
import { InputDateComponent } from './input-date/input-date.component';
import { RequiredLabelComponent } from './required/required-label/required-label.component';
import { ManyRadioComponent } from './many-radio/many-radio.component';
import { CnpjPipe } from '../pipes/cnpj.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    DirectivesModule.forRoot()
  ],
  declarations: [
    DatalistComponent,
    InputDateComponent,
    RequiredFieldComponent,
    RequiredLabelComponent,
    ManyCheckboxComponent,
    ManyRadioComponent,
    CnpjPipe
  ],
  exports: [
    DatalistComponent,
    InputDateComponent,
    RequiredFieldComponent,
    RequiredLabelComponent,
    ManyCheckboxComponent,
    ManyRadioComponent,
    CnpjPipe
  ]
})
export class ComponentModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ComponentModule
    };
  }
}
