import { UnidadeOrigemDestino } from './unidade-origem-destino';

export class RedirecionamentoOcorrencia {
    public unidades: UnidadeOrigemDestino[] = Array<UnidadeOrigemDestino>();
}