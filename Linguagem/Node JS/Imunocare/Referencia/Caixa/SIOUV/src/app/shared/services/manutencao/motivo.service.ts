import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Motivo } from 'app/shared/models/motivo';

@Injectable()
export class MotivoService extends CrudHttpClientService<Motivo> {
	
	constructor(protected http: HttpClient) {
		super('ocorrencia/motivo', http);
	}

	/**
	 * Consulta de motivos por nome
	 */
	public consultarPorNome(nome: string): Observable<Motivo[]> {
		let httpParams: HttpParams = new HttpParams().set('nome', nome);

		return this.http.get<Motivo[]>(this.url + '/consultar-por-nome',
			this.options({ params: httpParams }));
	}

	consultarPorFiltros(nome: string, situacao: string, naturezas: string): any {
		let httpParams: HttpParams = new HttpParams();
		httpParams = httpParams.set('nome', nome);
		httpParams = httpParams.set('situacao', situacao ? situacao : '');
		httpParams = httpParams.set('naturezas', naturezas);

		return this.http.get<Motivo[]>(
			this.url + '/consultar-por-filtros',
			this.options({ params: httpParams })
		);
	}

	public callListarPorTipoOcorrenciaAndItemAssunto(idTipoOcorrencia: number, idItemAssunto: number, next: (motivos: Motivo[]) => void): void {
		this.callGET<Motivo[]>('/itens/tipo-ocorrencia/' + idTipoOcorrencia + '/item-assunto/' + idItemAssunto, next);
	}
	
	public carregarMotivosPorItem(item: number): Motivo[] {
		var m = [
			this.newMotivo(1, 'Motivo 1'),
			this.newMotivo(2, 'Motivo 2'),
			this.newMotivo(3, 'Motivo 3'),
			this.newMotivo(4, 'Motivo 4'),
			this.newMotivo(5, 'Motivo 5'),
		]
		return m;
	}

	private newMotivo(id: number, nome: string): Motivo {
		var m = new Motivo();
		m.id = id;
		m.nome = nome;
		return m;
	}
}
