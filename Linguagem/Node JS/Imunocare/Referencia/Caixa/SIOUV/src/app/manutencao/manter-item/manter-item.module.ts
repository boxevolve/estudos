import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TextMaskModule } from 'angular2-text-mask';
import { NgxPaginationModule } from 'ngx-pagination';

// Aplicação
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { ManterItemRoutingModule } from './manter-item.routing.module';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { UnidadeModule } from 'app/shared/components/unidade/unidade.module';
import { SiouvTableModule } from 'app/shared/components/table/table.module';
import { ManterItemCadastroComponent } from './manter-item-cadastro/manter-item-cadastro.component';
import { ManterItemConsultaComponent } from './manter-item-consulta/manter-item-consulta.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    SiouvTableModule,
    ComponentModule.forRoot(),
    UnidadeModule,
    ManterItemRoutingModule,
    UnidadeModule.forRoot()
  ],
  declarations: [ManterItemCadastroComponent, ManterItemConsultaComponent]
})
export class ManterItemModule {}
