import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TextMaskModule } from 'angular2-text-mask';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';

import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { SiouvTableModule } from 'app/shared/components/table/table.module';
import { UnidadeModule } from 'app/shared/components/unidade/unidade.module';
import { MotivoCadastroComponent } from './motivo-cadastro/motivo-cadastro.component';
import { MotivoConsultaComponent } from './motivo-consulta/motivo-consulta.component';
import { MotivoRoutingModule } from './motivo.routing.module';
import { MotivoDetalheComponent } from './motivo-detalhe/motivo-detalhe.component';
import { MotivoHistoricoComponent } from './motivo-historico/motivo-historico.component';
import { MotivoHistoricoDetalheComponent } from './motivo-historico-detalhe/motivo-historico-detalhe.component';
import { MotivoModalComponent } from './motivo-modal/motivo-modal.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    ComponentModule.forRoot(),
    UnidadeModule,
    MotivoRoutingModule,
    SiouvTableModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [
    MotivoConsultaComponent,
    MotivoCadastroComponent,
    MotivoDetalheComponent,
    MotivoHistoricoComponent,
    MotivoHistoricoDetalheComponent,
    MotivoModalComponent
  ],
  exports: [
    MotivoModalComponent
  ]
})
export class MotivoModule {}
