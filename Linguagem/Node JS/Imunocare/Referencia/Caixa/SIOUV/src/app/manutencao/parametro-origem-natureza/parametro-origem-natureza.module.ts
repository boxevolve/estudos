import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TextMaskModule } from 'angular2-text-mask';
import { NgxPaginationModule } from 'ngx-pagination';

// Aplicação
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { ManterParametroOrigemRoutingModule } from './parametro-origem-natureza.routing.module';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { ParametroOrigemCadastroComponent } from './parametro-origem-natureza-cadastro/parametro-origem-natureza-cadastro.component';
import { SiouvTableModule } from 'app/shared/components/table/table.module';
import { UnidadeModule } from 'app/shared/components/unidade/unidade.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    ComponentModule.forRoot(),
    ManterParametroOrigemRoutingModule,
    SiouvTableModule,
    UnidadeModule.forRoot()
  ],
  declarations: [ParametroOrigemCadastroComponent]
})
export class ParametroOrigemModule {}
