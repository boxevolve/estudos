import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { SndcConsultaComponent } from './sndc-consulta/sndc-consulta.component';
import { SndcCadastroComponent } from './sndc-cadastro/sndc-cadastro.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard, DadosUsuarioGuard],
    canActivateChild: [AuthGuard, DadosUsuarioGuard],
    children: [
  {
            path: '',
            component: SndcConsultaComponent
        },
        {
            path: 'novo',
            component: SndcCadastroComponent
        },
        {
          path: 'novo/:id',
          component: SndcCadastroComponent
      }
]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SndcRoutingModule { }
