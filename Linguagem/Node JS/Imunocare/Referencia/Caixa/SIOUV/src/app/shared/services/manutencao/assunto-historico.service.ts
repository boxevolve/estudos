import { AssuntoHistorico } from './../../models/assunto-historico';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';

@Injectable()
export class AssuntoHistoricoService extends CrudHttpClientService<AssuntoHistorico> {
  constructor(protected http: HttpClient) {
    super('manutencao/assunto-historico', http);
  }

  consultar(usuario: string, operacao: any, dataInicial: Date, dataFinal: Date, assunto: string): any {
    let httpParams: HttpParams = new HttpParams();
    httpParams = httpParams.set('usuario', usuario ? usuario : '');
    httpParams = httpParams.set('operacao', operacao ? operacao : '');
    httpParams = httpParams.set('dataInicial', dataInicial.getTime().toString());
    httpParams = httpParams.set('dataFinal', dataFinal.getTime().toString());
    httpParams = httpParams.set('assunto', assunto ? assunto : '');

    return this.http.get<AssuntoHistorico[]>(
      this.url + '/consultar',
      this.options({ params: httpParams })
    );
  }
}
