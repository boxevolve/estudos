import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { CrudHttpClientService } from '../../../arquitetura/shared/services/crud-http-client.service';
import { TipoCorreioEletronico } from 'app/shared/models/tipo-correio-eletronico';

@Injectable()
export class TipoCorreioEletronicoService extends CrudHttpClientService<TipoCorreioEletronico> {

    constructor(protected http: HttpClient) {
        super('ocorrencia/tipo-correio-eltco', http);
    }

}
