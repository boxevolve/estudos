import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { ManterItemConsultaComponent } from './manter-item-consulta/manter-item-consulta.component';
import { ManterItemCadastroComponent } from './manter-item-cadastro/manter-item-cadastro.component';
const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard, DadosUsuarioGuard],
    canActivateChild: [AuthGuard, DadosUsuarioGuard],
    children: [
    {
      path: '',
      component: ManterItemConsultaComponent
    },
    {
      path: 'novo',
      component: ManterItemCadastroComponent
    },
    {
      path: ':id/editar',
      component: ManterItemCadastroComponent
    }
  ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ManterItemRoutingModule { }
