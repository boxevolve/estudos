import { Entity } from 'app/arquitetura/shared/models/entity';

export class AssuntoHistoricoId extends Entity {
  public nuAssunto: number;
  public dhAssunto: Date;
}
