import { TipoOperacao } from './tipo-operacao';
import { NaturezaMotivoHistorico } from './natureza-motivo-historico';

export class MotivoHistoricoFiltro {

  public usuario: string = null;
  public tiposOperacao: TipoOperacao[] = Array<TipoOperacao>();
  public dataInicio: Date = null;
  public dataFim: Date = null;
  public noMotivo: string = null;
  public natureza: number = null;
}
