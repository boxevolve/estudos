import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Recurso } from 'app/arquitetura/shared/models/seguranca/recurso';

@Injectable()
export class RecursoService extends CrudHttpClientService<Recurso> {
	constructor(protected http: HttpClient) {
		super('seguranca/recurso', http);
	}

	/**
	 * Insere um recurso
	 * @param entity
	 */
	public post(entity: Recurso): Observable<Recurso> {
		// ENTIDADE SOMENTE LEITURA => NÃO IMPLEMENTADO
		return null;
	}

	/**
	 * Altera um recurso
	 * @param entity
	 */
	public put(entity: Recurso): Observable<Recurso> {
		// ENTIDADE SOMENTE LEITURA => NÃO IMPLEMENTADO
		return null;
	}

	/**
	 * Exclui um recurso
	 * @param id
	 */
	public delete(id: number): Observable<Recurso> {
		// ENTIDADE SOMENTE LEITURA => NÃO IMPLEMENTADO
		return null;
	}
}
