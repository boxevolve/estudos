import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Assunto } from 'app/shared/models/assunto';
import { Item } from 'app/shared/models/item';
import { Motivo } from 'app/shared/models/motivo';
import { Natureza } from 'app/shared/models/natureza';
import { Ocorrencia } from 'app/shared/models/ocorrencia';
import { Origem } from 'app/shared/models/origem';
import { UfService } from '../comum/uf.service';
import { AssuntoService } from '../manutencao/assunto.service';
import { ManterItemService } from '../manutencao/manter-item.service';
import { MotivoService } from '../manutencao/motivo.service';
import { NaturezaService } from '../manutencao/natureza.service';
import { TipoOcorrenciaService } from '../manutencao/tipo-ocorrencia.service';
import { OrigemService } from '../manutencao/tipo-origem.service';
import { RedirecionamentoOcorrencia } from 'app/shared/models/redirecionamento-ocorrencia';

@Injectable()
export class OcorrenciaService extends CrudHttpClientService<Ocorrencia> {

  constructor(protected http: HttpClient,
    private messageService: MessageService,
    private origemService: OrigemService,
    private naturezaService: NaturezaService,
    private assuntoService: AssuntoService,
    private itemService: ManterItemService,
    private motivoService: MotivoService,
    private tipoOcorrenciaService: TipoOcorrenciaService,
    private ufService: UfService
  ) {
    super('ocorrencia', http);
  }

  callListarOcorrenciasPorParametros(storage: any, callback: Function): void {
  // this.callGET<Natureza[]>('/itens/tipoocorrencia/' + idTipoOcorrencia + '/origem/' + idTipoOrigem, next,
  // 	error => {
  // 		this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar naturezas.');
  // 		console.log('Erro ao chamar o servico. Erro: ' + error);
  // 	}
  // );
  }

  callListarMotivosPorTipoOcorrenciaItemAssunto(idTipoOcorrencia: number, idItemAssunto: number, next: (motivos: Motivo[]) => void): any {
    this.motivoService.callListarPorTipoOcorrenciaAndItemAssunto(idTipoOcorrencia, idItemAssunto, next);
  }
  public callListarOrigensPorTipoOcorrencia(idTipoOcorrencia: number, next: (origens: Origem[]) => Origem[]): void {
    this.origemService.callListarPorTipoOcorrencia(idTipoOcorrencia, next);
  }

  public callListarNaturezasPorTipoOcorrenciaAndTipoOrigem(idTipoOcorrencia: number, idTipoOrigem: number,
    next?: (naturezas: Natureza[]) => void) {
    this.naturezaService.callListarNaturezasPorTipoOcorrenciaAndTipoOrigem(idTipoOcorrencia, idTipoOrigem, next);
  }

  public carregarOrigensPorTipoOcorrencia(tipoOcorrencia: number): Origem[] {
    return this.origemService.carregarPorTipoOcorrencia(tipoOcorrencia);
  }

  public carregarNaturezasPorOrigem(origem: number, next?: (value: any) => void): Natureza[] {
    return this.naturezaService.carregarNaturezasPorOrigem(origem, next);
  }

  public carregarAssuntosPorNatureza(natureza: number): Assunto[] {
    return this.assuntoService.carregarAssuntosPorNatureza(natureza);
  }

  public carregarItensPorAssunto(assunto: number): Item[] {
    return this.itemService.carregarItensPorAssunto(assunto);
  }

  public carregarMotivosPorItem(item: number): Motivo[] {
    return this.motivoService.carregarMotivosPorItem(item);
  }

  public callListarTiposOcorrenciaSemAcompanhamento(next?: (value: any) => void): void {
    this.tipoOcorrenciaService.listarTiposOcorrenciaSemAcompanhamento(next);
  }

  public callListarAssuntosPorNatureza(idNatureza: number, next?: (value: any) => void) {
    this.assuntoService.callListarAssuntosPorNatureza(idNatureza, next);
  }

  public callListarUFs(next?: (value: any) => void): void {
    this.ufService.todos(next);
  }

  public redirecionarOcorrencia(unidades: RedirecionamentoOcorrencia, next?: (value: any) => void): any {
    this.http.post<RedirecionamentoOcorrencia>(this.url + '/redirecionamento-ocorrencia', unidades, this.options())
      .subscribe(next, error => {
          this.messageService.addMsgDanger('Ocorreu algum erro, tente novamente mais tarde.');
        }
      );
  }

  public callListarNaturezasPorTipoOrigem(idTipoOrigem: number,
     next?: (naturezas: Natureza[]) => void) {
     this.naturezaService.callListarNaturezasPorTipoOrigem(idTipoOrigem, next);
  }
}
