import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { MATRICULA_MASK } from './../../../shared/util/masks';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { NaturezaHistorico } from 'app/shared/models/natureza-historico';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TipoOperacao } from 'app/shared/models/tipo-operacao';
import { TipoOperacaoService } from 'app/shared/services/manutencao/tipo-operacao.service';
import { NaturezaHistoricoFiltro } from 'app/shared/models/natureza-historico.filtro';
import { Natureza } from 'app/shared/models/natureza';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { NaturezaHistoricoService } from 'app/shared/services/manutencao/natureza-historico.service';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { TipoOcorrenciaService } from 'app/shared/services/manutencao/tipo-ocorrencia.service';
import { Texto } from 'app/shared/models/texto';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';

@Component({
  selector: 'app-natureza-historico',
  templateUrl: './natureza-historico.component.html',
  styleUrls: ['./natureza-historico.component.css']
})
export class NaturezaHistoricoComponent extends BaseComponent {

  /**
	* Nomes dos formControlName dos campos
	*/
  nameUsuario = 'usuario';
  nameOperacao = 'tiposOperacao';
  nameDataInicio = 'dataInicio';
  nameDataFim = 'dataFim';
  nameNatureza = 'natureza';

  formulario: FormGroup;
  naturezaHistoricoFiltro: NaturezaHistoricoFiltro;
  naturezasHistorico: NaturezaHistorico[];
  dataList: SiouvTable;

  matriculaMask = MATRICULA_MASK;

  /**
   * Detalhar
   */
  idModalDetalhar: string;
  historicoSelecionado: NaturezaHistorico;

  tiposOperacao: TipoOperacao[];
  naturezas: Natureza[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    protected tipoOperacaoService: TipoOperacaoService,
    protected tipoOcorrenciaService: TipoOcorrenciaService,
    protected naturezaService: NaturezaService,
    protected naturezaHistoricoService: NaturezaHistoricoService,
    protected messageService: MessageService
  ) {
    super(messageService);
    this.naturezaHistoricoFiltro = new NaturezaHistoricoFiltro();
    this.naturezasHistorico = [];
    this.carregarTiposOperacao();
    this.carregarNaturezas();
    this.atualizarDataList();
    this.createFormGroup();
    this.validarFormulario(this.formulario);
  }

  carregarTiposOperacao(): void {
    this.tipoOperacaoService.get().subscribe(
      (tiposOperacao: TipoOperacao[]) => {
        this.tiposOperacao = tiposOperacao;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os tipos de operação.');
        console.log('Erro ao recuperar os tipos de operação:', error);
      }
    );
  }

  carregarNaturezas(): void {
    this.naturezaService.todosAtivos().subscribe(
      (naturezas: Natureza[]) => {
        this.naturezas = naturezas;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as naturezas.');
        console.log('Erro ao recuperar as naturezas:', error);
      }
    );
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      usuario: [null],
      natureza: [null],
      dataInicio: [null, [Validators.required]],
      dataFim: [null, [Validators.required]],
    });
  }

  pesquisar(): void {
    if (!this.validarFormulario(this.formulario)) {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
    }
    if (this.validarPeloMenosUmAll(this.formulario, [this.nameUsuario, this.nameNatureza], [this.naturezaHistoricoFiltro.tiposOperacao])) {
      this.montarObj();
      this.naturezaHistoricoService.consultarPorFiltro(this.naturezaHistoricoFiltro).subscribe(
        (naturezasHistorico: NaturezaHistorico[]) => {
          this.naturezasHistorico = naturezasHistorico;
          this.atualizarDataList();
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os históricos das naturezas.');
          console.log('Erro ao recuperar os históricos das naturezas:', error);
        }
      );
    } else {
      this.messageService.addMsgDanger('Ao menos um desses parâmetros deve ser informado: Usuário - Operações - Natureza.');
    }
  }

  montarObj(): void {
    const tiposOperacao = this.naturezaHistoricoFiltro.tiposOperacao;
    this.naturezaHistoricoFiltro = this.getClone(this.formulario.value);
    this.naturezaHistoricoFiltro.tiposOperacao = tiposOperacao;
  }

  limpar(): void {
    this.naturezaHistoricoFiltro = new NaturezaHistoricoFiltro();
    this.naturezasHistorico = Array<NaturezaHistorico>();
    this.createFormGroup();
    this.atualizarDataList();
  }

  voltar(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  setarIdModal(idModal: string): void {
    this.idModalDetalhar = idModal;
  }

  detalhar(naturezaHistorico: NaturezaHistorico): void {
    this.tipoOcorrenciaService.consultarLabelPorNatureza(naturezaHistorico.id.nuNatureza, (tiposOperacao: Texto) => {
      this.historicoSelecionado = naturezaHistorico;
      this.historicoSelecionado.labelTipoOcorrencia = tiposOperacao.value;
    });
  }

  isChecked(tipo: TipoOperacao): boolean {
    if (!this.naturezaHistoricoFiltro.tiposOperacao) {
      return false;
    }
    for (let index = 0; index < this.naturezaHistoricoFiltro.tiposOperacao.length; index++) {
      if (this.naturezaHistoricoFiltro.tiposOperacao[index].id === tipo.id) {
        return true;
      }
    }
    return false;
  }

  atualizarDataList() {
    this.dataList = this.createSiouvTable(this.naturezasHistorico);
  }

  createSiouvTable(list: NaturezaHistorico[]): SiouvTable {
    const idTable = 'consultaHistoricoNatureza';
    const creator: SiouvTableCreator<NaturezaHistorico> = new SiouvTableCreator();
    // Cabeçalhos
    // Id - Texto - Class - Style
    const headers = [
      creator.addHeader('usuario', 'Usuário', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('operacao', 'Operação', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('data_hora', 'Data/Hora', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('natureza', 'Natureza', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('situacao', 'Situação', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('acao', 'Ação', 16, 'text-center', {'vertical-align': 'middle'})
    ];

    // Linhas
    // Id - Texto - Class - Style - Objeto
    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'usuario', object.labelUsuario, 'text-center', null, object),
        creator.addCol(idTable, index, 'operacao', object.tipoOperacao.noTipoOperacao, 'text-center', null, object),
        creator.addCol(idTable, index, 'data_hora', object.labelDataHora, 'text-center', null, object),
        creator.addCol(idTable, index, 'natureza', object.noNatureza, 'text-center', null, object),
        creator.addCol(idTable, index, 'situacao', object.labelSituacao, 'text-center', null, object)
      ];
      rows.push(creator.addRowDetailModal(idTable, index, cols, null, null, this.idModalDetalhar, list[index]));
    }

    // Table
    // Id - Cabeçalhos - Linhas - Class - Style - Lista
    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

}
