import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { ParametroOrigemCadastroComponent } from './parametro-origem-natureza-cadastro/parametro-origem-natureza-cadastro.component';
const routes: Routes =  [
    {
        path: '',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        children: [
        {
                path: '',
                component: ParametroOrigemCadastroComponent
            },
            {
                path: 'novo',
                component: ParametroOrigemCadastroComponent
            }
            /*,{
                path: ':id/editar',
                component: OrigemCadastroComponent
            }*/
                ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ManterParametroOrigemRoutingModule {}
