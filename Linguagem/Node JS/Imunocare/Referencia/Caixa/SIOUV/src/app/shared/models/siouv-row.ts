import { SiouvCol } from './siouv-col';

export class SiouvRow {
    constructor (
        public id: string,
        public coluns: SiouvCol[],
        public clazz: any,
        public style: any,
        public showEdit: boolean,
        public showDelete: boolean,
        public showDetail: boolean,
        public showSelect: boolean,
        public showHover: boolean,
        public idModal: string,
        public object: any
    ) {}
}
