import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { CrudHttpClientService } from '../../../arquitetura/shared/services/crud-http-client.service';
import { CorreioEletronico } from 'app/shared/models/correio-eletronico';
import { TipoCorreioEletronico } from 'app/shared/models/tipo-correio-eletronico';

@Injectable()
export class CorreioEletronicoService extends CrudHttpClientService<CorreioEletronico> {

    constructor(protected http: HttpClient) {
        super('ocorrencia/correio-eletronico', http);
    }

    todos(): any {
      return this.http.get<CorreioEletronico[]>(this.url , this.options());
    }

    consultarPorFiltros(nuTipoCorreio: string, situacao: string): any {
      let httpParams: HttpParams = new HttpParams();
      httpParams = httpParams.set('nuTipoCorreioEletronico', nuTipoCorreio);
      httpParams = httpParams.set('situacao', situacao ? situacao : '');

      return this.http.get<CorreioEletronico[]>(
        this.url + '/consultar-por-filtros',
        this.options({ params: httpParams })
      );
    }

    buscarTipos(): any {
      const tipos = [
        {id: 1, nome: 'Aceite'},
        {id: 16, nome: 'Avaliação Gerada'},
        {id: 2, nome: 'Avaliação de Satisfação'},
        {id: 12, nome: 'Campanha'},
        {id: 3, nome: 'Cancelamento'},
        {id: 20, nome: 'Demanda Acompanhamento'},
        {id: 13, nome: 'Fórum'},
        {id: 15, nome: 'Informação'},
        {id: 14, nome: 'Lembrete'},
        // {id: 9, nome: 'Transferência'},
        {id: 4, nome: 'Nova Ocorrência'},
        {id: 11, nome: 'Pendência'},
        {id: 10, nome: 'Procedente'},
        {id: 5, nome: 'Prorrogação'},
        {id: 18, nome: 'Pré-Ocorrência'},
        {id: 19, nome: 'Pré-Ocorrência -> Ocorrência'},
        {id: 6, nome: 'Reabertura'},
        {id: 7, nome: 'Reclassificação'},
        {id: 17, nome: 'Redirecionamento'},
        {id: 8, nome: 'Resposta'}
      ];
      return tipos;
    }

}
