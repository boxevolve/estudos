import { MotivoHistoricoId } from './motivo-historico-id';
import { TipoOperacao } from './tipo-operacao';

export class MotivoHistorico {

  public id: MotivoHistoricoId = null;
  public coUsuario: string = null;
  public deMotivo: string = null;
  public icAtivo: string = null;
  public icDesconsideraAvlco: string = null;
  public noMotivo: string = null;
  public tipoOperacao: TipoOperacao = null;
  public labelUsuario: string = null;
  public labelDataHora: string = null;
  public labelSituacao: string = null;
}
