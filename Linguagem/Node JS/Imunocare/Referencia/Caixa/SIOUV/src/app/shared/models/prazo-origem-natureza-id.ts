export class PrazoOrigemNaturezaId {
    constructor (
        public nuNatureza: number,
        public nuOrigem: number,
        public nuTipoOcorrencia: number
    ) {}
}