import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { NaturezaMotivoHistorico } from 'app/shared/models/natureza-motivo-historico';

@Injectable()
export class NaturezaMotivoHistoricoService extends CrudHttpClientService<NaturezaMotivoHistorico> {

  constructor(protected http: HttpClient) {
    super('manutencao/natureza-motivo-historico', http);
  }

  consultarPorNuMotivo(nuMotivo: string): any {
    let httpParams: HttpParams = new HttpParams();
    httpParams = httpParams.set('nuMotivo', nuMotivo);
    return this.http.get<NaturezaMotivoHistorico[]>(this.url + '/itens/consultar-por-nuMotivo',
      this.options({ params: httpParams }));
  }
}
