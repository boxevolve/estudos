import { HttpClient } from '@angular/common/http';
import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Entity } from '../../../arquitetura/shared/models/entity';
import { Injectable } from '@angular/core';
import { Uf } from 'app/shared/models/uf';
import { MessageService } from 'app/shared/components/messages/message.service';

@Injectable()
export class UfService extends CrudHttpClientService<Entity> {
  
  constructor(protected http: HttpClient,
    private messageService: MessageService) {
    super('ico/uf', http);
  }

  todos(next?: (value: any) => void): any {
    return this.doGET('/').subscribe(next,
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar UF.');
        console.log('Erro ao consultar UF', error);
      });
  }

  private doGET(endpoint: string): any {
    return this.http.get<Uf[]>(this.url + endpoint, this.options());
  }
  

}
