import { NgModule } from '@angular/core';

import { CpfPipe } from './cpf.pipe';
import { CampoVazioPipe } from './campo-vazio.pipe';
import { FlagSimNaoPipe } from './flag-sim-nao.pipe';
import { MesPipe } from './mes.pipe';
import { OrderBy } from './orderby.pipe';

@NgModule({
	imports: [],
	declarations: [
		CpfPipe,
		CampoVazioPipe,
		FlagSimNaoPipe,
		MesPipe,
		OrderBy
	],
	exports: [
		CpfPipe,
		CampoVazioPipe,
		FlagSimNaoPipe,
		OrderBy
	],
})
export class PipeModule {
	static forRoot() {
		return {
			ngModule: PipeModule,
			providers: [],
		};
	}
}
