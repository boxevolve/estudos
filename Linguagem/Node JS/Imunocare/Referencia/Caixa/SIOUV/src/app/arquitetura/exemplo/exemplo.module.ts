import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Aplicação
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { ExemploRoutingModule } from './exemplo.routing.module';
import { ExemploComponent } from './exemplo.component';
import { ExemploAdicionalComponent } from 'app/arquitetura/exemplo/exemplo-adicional.component';

/**
 * Modulo Acesso
 **/
@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		FormsModule,
		ReactiveFormsModule,
		DirectivesModule.forRoot(),
		TemplatesModule,
		ExemploRoutingModule
	],
	declarations: [
		ExemploComponent,
		ExemploAdicionalComponent
	]
})
export class ExemploModule { }
