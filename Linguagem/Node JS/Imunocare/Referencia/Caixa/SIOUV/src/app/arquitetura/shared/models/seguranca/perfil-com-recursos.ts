import { Perfil } from './perfil';
import { Recurso } from './recurso';

export class PerfilComRecursos {
	public perfil: Perfil = null;
	public recursos: Recurso[] = null;
}
