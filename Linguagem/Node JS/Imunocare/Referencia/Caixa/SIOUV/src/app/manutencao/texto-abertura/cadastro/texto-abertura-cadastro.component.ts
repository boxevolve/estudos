import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Util } from 'app/arquitetura/shared/util/util';
import { TextoAbertura } from 'app/shared/models/texto-abertura';
import { TipoOcorrencia } from 'app/shared/models/tipo-ocorrencia';
import { TextoAberturaService } from 'app/shared/services/manutencao/texto-abertura.service';
import { TipoOcorrenciaService } from 'app/shared/services/manutencao/tipo-ocorrencia.service';

@Component({
  selector: 'app-texto-abertura-cadastro',
  templateUrl: './texto-abertura-cadastro.component.html',
  styleUrls: ['./texto-abertura-cadastro.component.css']
})
export class TextoAberturaCadastroComponent extends BaseComponent {

  /**
 * Nomes dos formControlName dos campos
 */
  nameTipoOcorrencia = 'nuTipoOcorrencia';
  nameTitulo = 'nome';
  nameDescricao = 'descricao';

  CODIGO_OCORRENCIA_INTERNA = 1;
  CODIGO_OCORRENCIA_OUVIDORIA = 2;
  CODIGO_OCORRENCIA_SAC = 3;
  CODIGO_ANTERIOR = 1;

  formulario: FormGroup;
  textoAbertura: TextoAbertura = null;
  textoAberturaInterna: TextoAbertura = null;
  textoAberturaOuvidoria: TextoAbertura = null;
  textoAberturaSAC: TextoAbertura = null;

  tiposOcorrencia: TipoOcorrencia[] = [];

  constructor(
    private formBuilder: FormBuilder,
    protected messageService: MessageService,
    private textoAberturaService: TextoAberturaService,
    private tipoOcorrenciaService: TipoOcorrenciaService
  ) {
    super(messageService);
    this.textoAbertura = new TextoAbertura();
    this.carregarTiposOcorrencia();
    this.createFormGroup();
  }

  carregarTiposOcorrencia(): void {
    this.tipoOcorrenciaService.listarTiposOcorrenciaSemAcompanhamento((tiposOcorrencia: TipoOcorrencia[]) => {
      this.tiposOcorrencia = tiposOcorrencia;
      this.textoAbertura.nuTipoOcorrencia = this.tiposOcorrencia[0].id;
      this.carregarTextosAbertura();
    });
  }

  carregarTextosAbertura(): void {
    this.carregarTextoAbertura(this.CODIGO_OCORRENCIA_INTERNA);
    this.carregarTextoAbertura(this.CODIGO_OCORRENCIA_OUVIDORIA);
    this.carregarTextoAbertura(this.CODIGO_OCORRENCIA_SAC);
  }

  carregarTextoAbertura(codigo: number): void {
    this.textoAberturaService.consultarPorOcorrencia(codigo).subscribe(
      (textoAbertura: TextoAbertura) => {
        this.atualizarTextosAbertura(codigo, textoAbertura);
        if (this.textoAbertura.nuTipoOcorrencia === codigo) {
        this.textoAbertura = textoAbertura;
          this.createFormGroup();
        }
      },
      error => { }
    );
  }

  atualizarTextosAbertura(codigo: number, textoAbertura: TextoAbertura): void {
    switch (codigo) {
      case this.CODIGO_OCORRENCIA_INTERNA:
        this.textoAberturaInterna = textoAbertura;
        break;
      case this.CODIGO_OCORRENCIA_OUVIDORIA:
        this.textoAberturaOuvidoria = textoAbertura;
        break;
      case this.CODIGO_OCORRENCIA_SAC:
        this.textoAberturaSAC = textoAbertura;
        break;
        default:
    }
  }

  changeTipoOcorrencia(codigo: number) {
    this.atualizarTextosAbertura(this.CODIGO_ANTERIOR, this.formulario.value);
    switch (codigo) {
    case this.CODIGO_OCORRENCIA_INTERNA:
        if (!this.textoAberturaInterna) {
          this.textoAberturaInterna = new TextoAbertura();
        }
        this.textoAberturaInterna.nuTipoOcorrencia = this.CODIGO_OCORRENCIA_INTERNA;
        this.textoAbertura = this.textoAberturaInterna;
        break;
      case this.CODIGO_OCORRENCIA_OUVIDORIA:
        if (!this.textoAberturaOuvidoria) {
          this.textoAberturaOuvidoria = new TextoAbertura();
        }
        this.textoAberturaOuvidoria.nuTipoOcorrencia = this.CODIGO_OCORRENCIA_OUVIDORIA;
        this.textoAbertura = this.textoAberturaOuvidoria;
        break;
      case this.CODIGO_OCORRENCIA_SAC:
        if (!this.textoAberturaSAC) {
          this.textoAberturaSAC = new TextoAbertura();
        }
        this.textoAberturaSAC.nuTipoOcorrencia = this.CODIGO_OCORRENCIA_SAC;
        this.textoAbertura = this.textoAberturaSAC;
        break;
      default:
    }
    this.createFormGroup();
    this.CODIGO_ANTERIOR = codigo;
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      id: [this.textoAbertura.id],
      nuTipoOcorrencia: [this.textoAbertura.nuTipoOcorrencia],
      nome: [
        this.textoAbertura.nome,
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(200)
        ]
      ],
      descricao: [
        this.textoAbertura.descricao,
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(10000)
        ]
      ]
    });
  }

  isNew(): boolean {
    return Util.isEmpty(this.textoAbertura.id);
  }

  salvar() {
    if (!this.validarFormulario(this.formulario)) {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
    }
    this.textoAbertura = this.getClone(this.formulario.value);
    if (!this.textoAbertura.nuTipoOcorrencia) {
      this.textoAbertura.nuTipoOcorrencia = this.tiposOcorrencia[0].id;
    }

    if (this.isNew()) {
      this.textoAberturaService.post(this.textoAbertura).subscribe(
        (textoAbertura: TextoAbertura) => {
          this.textoAbertura = textoAbertura;
          this.atualizarTextosAbertura(textoAbertura.nuTipoOcorrencia, textoAbertura);
          this.messageService.addMsgSuccess('Operação efetuada com sucesso.');
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao incluir o texto de abertura.');
          console.log('Erro ao incluir texto de abertura:', error);
        });
    } else {
      this.textoAberturaService.put(this.textoAbertura).subscribe(
        (textoAbertura: TextoAbertura) => {
          this.textoAbertura = textoAbertura;
          this.messageService.addMsgSuccess('Operação efetuada com sucesso.');
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao alterar o motivo.');
          console.log('Erro ao alterar motivo:', error);
        }
      );
    }
  }

  limpar() {
    const id = this.textoAbertura.id;
    this.textoAbertura = new TextoAbertura();
    this.textoAbertura.id = id;
    this.textoAbertura.nuTipoOcorrencia = this.CODIGO_ANTERIOR;
    this.createFormGroup();
  }

}
