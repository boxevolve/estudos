import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { TipoOcorrencia } from '../../models/tipo-ocorrencia';
import { Injectable } from '@angular/core';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Natureza } from 'app/shared/models/natureza';
import { Texto } from 'app/shared/models/texto';
import { Unidade } from 'app/shared/models/unidade';
import { RedirecionamentoOcorrencia } from 'app/shared/models/redirecionamento-ocorrencia';

@Injectable()
export class TipoOcorrenciaService extends CrudHttpClientService<TipoOcorrencia> {

  constructor(protected http: HttpClient, private messageService: MessageService) {
    super('ocorrencia/tipo-ocorrencia', http);
  }

  public listarTiposOcorrenciaSemAcompanhamento(next?: (value: any) => void): void {
    this.http.get<TipoOcorrencia[]>(this.url + '/list/tipos-ocorrencia-sem-acompanhamento', this.options())
      .subscribe(next, error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os tipos de ocorrência.');
          console.log('Erro ao recuperar os tipos de ocorrência:', error);
    });
  }

  public listarTiposOcorrenciaSemAcompanhamento2(next?: (value: any) => void): void {
    this.http.get<TipoOcorrencia[]>(this.url + '/list/tipos-ocorrencia-sem-acompanhamento', this.options())
      .subscribe(next,  error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os tipos de ocorrência.');
          console.log('Erro ao recuperar os tipos de ocorrência:', error);
    });
  }

  consultarLabelPorNatureza(idNatureza: number, next?: (value: any) => void): void {
    this.http.post<Texto>(this.url + '/consultar/label-por-natureza', idNatureza, this.options())
      .subscribe(next, error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os tipos de ocorrência.');
          console.log('Erro ao recuperar os tipos de ocorrência:', error);
    });
  }

}
