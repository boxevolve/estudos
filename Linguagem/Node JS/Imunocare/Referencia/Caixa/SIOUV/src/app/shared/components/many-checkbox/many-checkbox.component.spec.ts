import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManyCheckboxComponent } from './many-checkbox.component';

describe('ManyCheckboxComponent', () => {
  let component: ManyCheckboxComponent;
  let fixture: ComponentFixture<ManyCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManyCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManyCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
