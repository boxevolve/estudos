import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Motivo } from 'app/shared/models/motivo';
import { MotivoService } from 'app/shared/services/manutencao/motivo.service';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { Natureza } from 'app/shared/models/natureza';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { Situacao } from 'app/shared/models/situacao';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { Util } from 'app/arquitetura/shared/util/util';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';

@Component({
  selector: 'app-motivo-consulta',
  templateUrl: './motivo-consulta.component.html',
  styleUrls: ['./motivo-consulta.component.css']
})
export class MotivoConsultaComponent extends BaseComponent {

  nameMotivo = 'nome';
  nameSituacao = 'situacao';
  nameNatureza = 'natureza';

  pagina = 1;
  itens = 5;
  motivo: Motivo;
  motivos: Motivo[] = null;
  naturezas: Natureza[] = null;
  situacoes: Situacao[] = null;
  dataList: SiouvTable;
  formulario: FormGroup;
  naturezasSelecionadas: number[] = [];
  idModalDetalhar: string;
  motivoSelecionado: Motivo;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    protected messageService: MessageService,
    private motivoService: MotivoService,
    private comumService: ComumService,
    private naturezaService: NaturezaService,
    private formBuilder: FormBuilder
  ) {
    super(messageService);

    this.motivo = new Motivo();
    this.criarFormulario();
    this.carregarNaturezas();
    this.carregarSituacoes();
  }

  criarFormulario() {
    this.formulario = this.formBuilder.group({
      nome: [''],
      situacao: [null]
    });
  }

  carregarSituacoes(): void {
    this.comumService.listarSituacoes().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
        console.log('Erro ao consultar situações:', error);
      });
  }

  carregarNaturezas(): void {
    this.naturezaService.get().subscribe(
      (naturezas: Natureza[]) => {
        this.naturezas = naturezas;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as naturezas.');
        console.log('Erro ao recuperar as naturezas:', error);
      }
    );
  }

  atualizarNaturezasSelecionadas(naturezaSelecionada: Natureza) {
    let incluso = false;
    const novaLista: number[] = [];
    this.naturezasSelecionadas.forEach(naturezaId => {
      if (naturezaId === naturezaSelecionada.id) {
        incluso = true;
      } else {
        novaLista.push(naturezaId);
      }
    });
    if (!incluso) {
      novaLista.push(naturezaSelecionada.id);
    }
    this.naturezasSelecionadas = novaLista;
  }

  atualizarDataList() {
    this.dataList = this.createSiouvTable(this.motivos);
  }

  createSiouvTable(list: Motivo[]): SiouvTable {
    const idTable = 'consultaMotivo';
    const creator: SiouvTableCreator<Motivo> = new SiouvTableCreator();

    const headers = [
      creator.addHeader('motivo', 'Motivo', 33, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('situacao', 'Situação', 33, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('acao', 'Ação', 33, 'text-center', {'vertical-align': 'middle'})
    ];

    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'nome', object.nome, 'text-center', null, object),
        creator.addCol(idTable, index, 'ativa', object.labelAtiva, 'text-center', null, object),
      ];
      rows.push(creator.addRowAllDetailModal(idTable, index, cols, null, null, this.idModalDetalhar, list[index]));
    }

    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

  consultar() {

    const nomeMotivo: string = this.formulario.controls['nome'].value;
    const situacaoMotivo: string = this.formulario.controls['situacao'].value;

    if ((nomeMotivo == null && this.naturezasSelecionadas.length < 1) ||
      Util.isEmpty(nomeMotivo) && this.naturezasSelecionadas.length < 1) {
      this.messageService.addMsgDanger('Pelo menos um desses parâmetros deve ser informado: Motivo ou Natureza.');
      return;
    }
    const nuNaturezas = this.naturezasSelecionadas.toString();
    this.motivoService.consultarPorFiltros(nomeMotivo, situacaoMotivo, nuNaturezas).subscribe(
      (motivos: Motivo[]) => {
        this.motivos = motivos;
        this.atualizarDataList();
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar motivos.');
        console.log('Erro ao consultar motivos:', error);
      });
  }

  incluir() {
    this.router.navigate(['novo'], { relativeTo: this.route.parent });
  }

  alterar(motivo: Motivo) {
    this.router.navigate([motivo.id, 'editar'], { relativeTo: this.route.parent });
  }

  pesquisarHistorico() {
    this.router.navigate(['historico'], { relativeTo: this.route.parent });
  }

  excluir(motivo: Motivo) {
    const indice: number = this.motivos.findIndex(item => item.id === motivo.id);
    this.motivoService.delete(motivo.id).subscribe(_ => {
        this.messageService.addMsgSuccess('Operação efetuada com sucesso.');
        if (indice >= 0) {
          this.motivos.splice(indice, 1);
        }
        this.consultar();
      },
      error => {
        let errMsg;
        if (error.status === 405) {
          errMsg = 'Motivo não pode ser excluído pois tem vinculos.';
        } else {
          errMsg = 'Ocorreu um erro ao excluir o motivo.';
        }
        this.messageService.addMsgDanger(errMsg);
        console.log(errMsg);
      });
  }

  setarIdModal(idModal: string): void {
    this.idModalDetalhar = idModal;
  }

  detalhar(motivo: Motivo): void {
    this.motivoSelecionado = motivo;
  }

  limpar() {
    this.motivos = null;
    this.naturezasSelecionadas = [];
    this.carregarNaturezas();
    this.criarFormulario();
  }
}
