import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { PaginaNaoEncontradaComponent } from 'app/arquitetura/shared/templates/pagina-nao-encontrada.component';

const appRoutes: Routes = [
    {
        path: 'home',
        loadChildren: 'app/arquitetura/home/home.module#HomeModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'seguranca',
        loadChildren: 'app/arquitetura/seguranca/seguranca.module#SegurancaModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'texto-abertura',
        loadChildren: 'app/manutencao/texto-abertura/texto-abertura.module#TextoAberturaModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'manter-item',
        loadChildren: 'app/manutencao/manter-item/manter-item.module#ManterItemModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'origem',
        loadChildren: 'app/manutencao/origem/origem.module#OrigemModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'natureza',
        loadChildren: 'app/manutencao/natureza/natureza.module#NaturezaModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'motivo',
        loadChildren: 'app/manutencao/motivo/motivo.module#MotivoModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'assunto',
        loadChildren: 'app/manutencao/assunto/assunto.module#AssuntoModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'grau-satisfacao',
        loadChildren: 'app/manutencao/grau-satisfacao/grau-satisfacao.module#GrauSatisfacaoModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'exemplo',
        loadChildren: 'app/arquitetura/exemplo/exemplo.module#ExemploModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'ocorrencia',
        loadChildren: 'app/ocorrencia/ocorrencia.module#OcorrenciaModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'correio-eletronico',
        loadChildren: 'app/manutencao/correio-eletronico/correio-eletronico.module#CorreioEletronicoModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'parametro-origem-natureza',
        loadChildren: 'app/manutencao/parametro-origem-natureza/parametro-origem-natureza.module#ParametroOrigemModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },{
		path: 'redirecionamento-ocorrencia',
		loadChildren: 'app/manutencao/redirecionamento-ocorrencia/redirecionamento-ocorrencia.module#RedirecionamentoOcorrenciaModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    }, 
    {
		path: 'unidade-sem-ocorrencia',
		loadChildren: 'app/manutencao/unidade-sem-ocorrencia/unidade-sem-ocorrencia.module#UnidadeSemOcorrenciaModule',
		canActivate: [AuthGuard, DadosUsuarioGuard],
		canActivateChild: [AuthGuard, DadosUsuarioGuard],
		canLoad: [AuthGuard]
    },
    {
        path: 'sndc',
        loadChildren: 'app/manutencao/sndc/sndc.module#SndcModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'classificacao-ocorrencia',
        loadChildren: 'app/manutencao/classificacao-ocorrencia/classificacao-ocorrencia.module#ClassificacaoOcorrenciaModule',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        canLoad: [AuthGuard]
    },
    /*CAST_MAKER_ROTA_MODULO*/
    { path: '', pathMatch: 'full', redirectTo: '/home' },
    { path: '**', component: PaginaNaoEncontradaComponent, canActivate: [AuthGuard] }
];
@NgModule({
    imports: [RouterModule.forRoot(appRoutes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
