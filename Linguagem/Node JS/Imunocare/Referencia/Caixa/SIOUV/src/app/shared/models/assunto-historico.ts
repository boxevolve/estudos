import { Entity } from 'app/arquitetura/shared/models/entity';
import { AssuntoHistoricoId } from './assunto-historico-id';
import { TipoOperacao } from './tipo-operacao';

export class AssuntoHistorico extends Entity {
  public id: AssuntoHistoricoId;
  public coUsuario: string;
  public icAtivo: string;
  public icDesconsideraAvlco: string;
  public noAssunto: string;
  public tipoOperacao: TipoOperacao;
}
