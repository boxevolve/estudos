import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Origem } from '../../models/origem';

@Injectable()
export class OrigemService extends CrudHttpClientService<Origem> {

   public callListarPorTipoOcorrencia(idTipoOcorrencia: number, next: (origens: Origem[]) => Origem[]): void {
    this.http.get<Origem[]>(this.url + '/itens/tipoocorrencia/' + idTipoOcorrencia, this.options())
      .subscribe(next, error => {
        console.log('Não foi possível consultar origens por tipo de ocorrencia.');
        console.log(error);
      });
  }

  public listarOrigemAtivaPorTipoOcorrencia(idTipoOcorrencia: number, next: (origens: Origem[]) => Origem[]): void {
    this.http.get<Origem[]>(this.url + '/itens/tipoocorrencia/origemativa/' + idTipoOcorrencia, this.options())
      .subscribe(next, error => {
        console.log('Não foi possível consultar origens por tipo de ocorrencia.');
        console.log(error);
      });
  }

  constructor(protected http: HttpClient) {
  super('manutencao/tipo-origem', http);
    }

  todos(): any {
    return this.http.get<Origem[]>(this.url + '/', this.options());
  }

  verificarVinculo(origem: Origem): any {
    return this.http.post<boolean>(this.url + '/verificar/vinculo', origem, this.options());
  }

  carregarPorTipoOcorrencia(tipoOcorrencia: number): Origem[] {
    return [];
  }

  verificarDuplicidadeRegistro(origem: Origem): any {
    return this.http.post<boolean>(this.url + '/verificar/duplicidade', origem, this.options());
  }

}