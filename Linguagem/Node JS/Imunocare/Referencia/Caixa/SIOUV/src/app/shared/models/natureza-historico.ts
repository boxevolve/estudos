import { NaturezaHistoricoId } from './natureza-historico-id';
import { TipoOperacao } from './tipo-operacao';

export class NaturezaHistorico {
  public id: NaturezaHistoricoId = null;
  public coUsuario: string = null;
  public deOrientacao: string = null;
  public icAceite: string = null;
  public icAtiva: string = null;
  public icDesconsideraAvlco: string = null;
  public icRestrito: string = null;
  public noNatureza: string = null;
  public tipoOperacao: TipoOperacao = null;
  public labelUsuario: string = null;
  public labelDataHora: string = null;
  public labelTipoOcorrencia: string = null;
  public labelAceite: string = null;
  public labelRestrito: string = null;
  public labelSituacao: string = null;
}
