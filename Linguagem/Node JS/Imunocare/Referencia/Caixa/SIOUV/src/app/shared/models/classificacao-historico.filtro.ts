import { TipoOperacao } from "./tipo-operacao";
import { TipoOcorrencia } from "./tipo-ocorrencia";
import { AssuntoItemMotivo } from "./AssuntoItemMotivo";

export class ClassificacaoHistoricoFiltro {
    public usuario: string = null;
    public tiposOperacao: TipoOperacao[] = Array<TipoOperacao>();
    public dataInicio: Date = null;
    public dataFim: Date = null;
    public tipoOcorrencia: TipoOcorrencia;
    public tiposAssuntoItemMotivo: AssuntoItemMotivo[] = Array<AssuntoItemMotivo>();
    public palavraChave: string = null;
  }
