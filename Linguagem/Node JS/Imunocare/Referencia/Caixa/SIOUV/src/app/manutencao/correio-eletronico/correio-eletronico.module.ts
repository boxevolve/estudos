import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxPaginationModule } from 'ngx-pagination';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { UnidadeModule } from 'app/shared/components/unidade/unidade.module';
import { SiouvTableModule } from 'app/shared/components/table/table.module';
import { CorreioEletronicoRoutingModule } from './correio-eletronico.routing.module';
import { CorreioEletronicoConsultaComponent } from './correio-eletronico-consulta/correio-eletronico-consulta.component';
import { CorreioEletronicoCadastroComponent } from './correio-eletronico-cadastro/correio-eletronico-cadastro.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    ComponentModule.forRoot(),
    UnidadeModule,
    CorreioEletronicoRoutingModule,
    SiouvTableModule
  ],
  declarations: [
    CorreioEletronicoConsultaComponent,
    CorreioEletronicoCadastroComponent
  ]
})
export class CorreioEletronicoModule {}
