import { Entity } from 'app/arquitetura/shared/models/entity';
import { NaturezaMotivo } from './natureza-motivo';

export class Motivo extends Entity {
  public id: number;
  public nome: string = null;
  public descricao: string = null;
  public ativo: string = null;
  public usuario: string = null;
  public desconsideraAvlco: string = null;
  public naturezasMotivo: NaturezaMotivo[] = null;
  public labelAtiva: string = null;
}
