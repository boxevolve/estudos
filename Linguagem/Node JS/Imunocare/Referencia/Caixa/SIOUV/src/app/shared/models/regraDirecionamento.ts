import { RegraDirecionamentoId } from './regra-direcionamento-id';
import { ItemAssunto } from './item-assunto';
import { Motivo } from './motivo';
import { TipoOcorrencia } from './tipo-ocorrencia';
import { Unidade } from './unidade';

export class RegraDirecionamento {
  public id: RegraDirecionamentoId = null;
  public itemAssunto: ItemAssunto = null;
  public motivo: Motivo = null;
  public tipoOcorrencia: TipoOcorrencia = null;
  public unidadeEnvolvida: Unidade = null;
}
