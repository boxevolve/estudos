import { NaturezaMotivoId } from './natureza-motivo-id';
import { Natureza } from './natureza';

export class NaturezaMotivo {

    public id: NaturezaMotivoId;
    public pzSolucao: number;
    public coUsuario: number;
    public icAtiva: string;
    public labelAtiva: string;
    public natureza: Natureza;

    constructor (id: NaturezaMotivoId) {
        this.id = id;
        this.pzSolucao = 1;
        this.icAtiva = 'S';
        this.natureza = null;
    }
}
