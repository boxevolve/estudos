import { TipoOcorrenciaNaturezaId } from './tipo-ocorrencia-natureza-id';
import { Natureza } from './natureza';
import { TipoOcorrencia } from './tipo-ocorrencia';

export class TipoOcorrenciaNatureza {
    constructor (
        public id: TipoOcorrenciaNaturezaId,
        public natureza: Natureza,
        public tipoOcorrencia: TipoOcorrencia
    ) {}
}
