import { Component, OnInit } from '@angular/core';

import { SessaoService } from 'app/arquitetura/shared/services/seguranca/sessao.service';
import { MessageService } from 'app/shared/components/messages/message.service';

declare var jQuery: any;

@Component({
	selector: 'app-cabecalho-padrao',
	templateUrl: './cabecalho-padrao.component.html',
	styleUrls: ['./cabecalho-padrao.component.scss']
})
export class CabecalhoPadraoComponent implements OnInit {
	public mensagemContinuidadeUso: string;

	constructor(
		public sessaoService: SessaoService,
		protected messageService: MessageService
	) {
		sessaoService.onIdleStart.subscribe(() => this.onIdleStart());
		sessaoService.onIdleTimeLimit.subscribe((countdown: number) => this.onIdleTimeLimit(countdown));
		sessaoService.onIdleEnd.subscribe(() => this.onIdleStart());
		sessaoService.onTimeout.subscribe(() => this.onTimeout());
	}

	public ngOnInit() {
		jQuery('.modal-idle').on('shown.bs.modal', function () {
			jQuery('#btnIdleSim').focus();
		});
		jQuery('.modal-logout').on('shown.bs.modal', function () {
			jQuery('#btnLogoutNao').focus();
		});
	}

	public mostraEscondeBarraFavoritos() {
		jQuery('.barra-favoritos').slideToggle('slow');
	}

	/**
   * Realiza o Logout do sistema, excluindo dados armazenados por usuário
	 * Obs.: pode estar vindo da tela de Idle ou da tela de Confirmação de Logout
   */
	public logout() {
		jQuery('.modal-idle').modal('hide');
		jQuery('.modal-logout').modal('hide');
		this.sessaoService.finalizarSessao(true);
	}

	private onIdleStart() {
		this.onIdleTimeLimit();
		jQuery('.modal-idle').modal();
	}

	private onIdleTimeLimit(countdown?: number) {
		this.mensagemContinuidadeUso = 'O sistema não foi utilizado nos últimos minutos. ' +
			'Deseja continuar utilizando-o?';
		if (countdown <= 60) {
			this.mensagemContinuidadeUso += ' [' + countdown + ']';
		}
	}

	private onIdleEnd() {
		jQuery('.modal-idle').modal('hide');
	}

	private onTimeout() {
		this.onIdleEnd();
		this.sessaoService.finalizarSessao(true, true);
	}
}
