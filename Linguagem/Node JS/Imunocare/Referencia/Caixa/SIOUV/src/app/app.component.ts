// /-------------------------------------------------\
// | artefatos da biblioteca angular                 |
// \-------------------------------------------------/
import { Component, ViewEncapsulation } from '@angular/core';

import { SessaoService } from 'app/arquitetura/shared/services/seguranca/sessao.service';

// /--------------------------------------------------\
// | app.component.ts                                 |
// |--------------------------------------------------|
// | Componente root da aplicação                     |
// \--------------------------------------------------/

const closedClass = "menusidebar is-closed";
const openedClass = "menusidebar is-open";
const toggledClass = "toggled";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class AppComponent {
	public show: boolean;
	public currentClass: string;
	public wrapperClass: string;
	constructor(private sessaoService: SessaoService) {
		this.sessaoService.inicializarSessao();
	}

	ngOnInit() {
		this.show = false;
		this.currentClass = closedClass;
	}

	toggleMenu() {
		if (this.show == true) {
			this.show = false;
			this.currentClass = closedClass;
			this.wrapperClass = "";
		} else {
			this.show = true;
			this.currentClass = openedClass;
			this.wrapperClass = toggledClass;
		}
	}
}