import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Origem } from '../../models/origem';
import { ParametroOrigemNatureza } from '../../models/parametro-origem-natureza';
import { OrigemService } from '../manutencao/tipo-origem.service';
import { MessageService } from 'app/shared/components/messages/message.service';

@Injectable()
export class ParametroOrigemNaturezaService extends CrudHttpClientService<ParametroOrigemNatureza> {

   public listarOrigemAtivaPorTipoOcorrencia(idTipoOcorrencia: number, next: (origens: Origem[]) => Origem[]): void {
    this.http.get<Origem[]>(this.url + '/itens/tipoocorrencia/' + idTipoOcorrencia, this.options())
      .subscribe(next, error => {
        console.log('Não foi possível consultar origens por tipo de ocorrencia.');
        console.log(error);
      });
  }

  constructor(protected http: HttpClient,
    private origemService: OrigemService,
    private messageService: MessageService
  ) {
    super('manutencao/parametro-origem-natureza', http);
  }

  public listarOrigensAtivasPorTipoOcorrencia(idTipoOcorrencia: number, next: (origens: Origem[]) => Origem[]): void {
    this.origemService.listarOrigemAtivaPorTipoOcorrencia(idTipoOcorrencia, next);
  }

  public listarParametroOrigemPorTipoOcorrenciaEOrigem(idTipoOcorrencia: number, idTipoOrigem: number,
    next?: (listaParametroOrigemNatureza: ParametroOrigemNatureza[]) => void): void {
    this.callGET<ParametroOrigemNatureza[]>('/itens/tipoocorrencia/' + idTipoOcorrencia + '/origem/' + idTipoOrigem, next,
        error => {
            this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar os parâmetros origens.');
            console.log('Erro ao chamar o servico. Erro: ' + error);
        }
    );
}

}
