import { Component, Output, AfterViewInit, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder} from '@angular/forms';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Natureza } from 'app/shared/models/natureza';
import { Situacao } from 'app/shared/models/situacao';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { MotivoService } from 'app/shared/services/manutencao/motivo.service';
import { Motivo } from 'app/shared/models/motivo';

@Component({
  selector: 'app-motivo-modal',
  templateUrl: './motivo-modal.component.html',
  styleUrls: ['./motivo-modal.component.css']
})
export class MotivoModalComponent extends BaseComponent implements AfterViewInit {

  modalId = 'motivo';

  @Input() idBase = 'motivoBase';
  @Output() selecionarMotivo = new EventEmitter();
  @Output() emitIdModal = new EventEmitter();

  /**
  * Nomes dos formControlName dos campos
  */
  nameNome = 'nome';
  nameSituacao = 'situacao';
  nameNaturezas = 'naturezas';

  formulario: FormGroup;
  dataList: SiouvTable;
  motivos: Motivo[] = [];
  naturezas: Natureza[] = [];
  situacoes: Situacao[] = [];
  naturezasSelecionadas: number[] = [];

  /**
   * Detalhar
   */
  idModalDetalhar: string;
  motivoSelecionado: Motivo;

  constructor(
    protected messageService: MessageService,
    private formBuilder: FormBuilder,
    private comumService: ComumService,
    private naturezaService: NaturezaService,
    private motivoService: MotivoService,
  ) {
    super(messageService);
    this.carregarNaturezas();
    this.carregarSituacoes();
    this.createFormGroup();
  }

  ngAfterViewInit() {
    this.emitIdModal.emit(this.getClone(this.getIdModal([])));
    $(this.idHashtag(this.getIdModal([]))).on('hidden.bs.modal',  () => {
      this.limpar();
    });
    this.atualizarDataList();
  }

  limpar(): void {
    this.formulario.reset();
    this.motivos = [];
    this.atualizarDataList();
  }

  carregarNaturezas(): void {
    this.naturezaService.get().subscribe(
      (naturezas: Natureza[]) => {
        this.naturezas = naturezas;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as naturezas.');
        console.log('Erro ao recuperar as naturezas:', error);
      }
    );
  }

  carregarSituacoes(): void {
    this.comumService.listarSituacoes().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
        console.log('Erro ao consultar situações:', error);
      });
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      nome: [null],
      situacao: [null]
    });
  }

  pesquisar(): void {
    if (this.validarPeloMenosUmAll(this.formulario, [this.nameNome, this.nameSituacao], [this.naturezasSelecionadas])) {
      const nomeMotivo: string = this.getValueForm(this.formulario, this.nameNome);
      const situacaoMotivo: string = this.getValueForm(this.formulario, this.nameSituacao);
      const nuNaturezas = this.getValueArrayForm(this.formulario, this.nameNaturezas, this.naturezas, 'id');
      this.motivoService.consultarPorFiltros(nomeMotivo, situacaoMotivo, nuNaturezas).subscribe(
        (motivos: Motivo[]) => {
          this.motivos = motivos;
          this.atualizarDataList();
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar motivos.');
          console.log('Erro ao consultar motivos:', error);
        }
      );
    } else {
      this.messageService.addMsgDanger('Preencha o campo Motivo ou selecione ao menos uma natureza.');
    }
  }

  atualizarDataList(): void {
    this.dataList = this.createSiouvTable(this.motivos);
  }

  createSiouvTable(list: Motivo[]): SiouvTable {
    const idTable = 'consultaMotivo';
    const creator: SiouvTableCreator<Motivo> = new SiouvTableCreator();

    const headers = [
      creator.addHeader('_motivo', 'Motivo', 33, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('_situacao', 'Situação', 33, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('_acao', 'Ação', 33, 'text-center', {'vertical-align': 'middle'})
    ];

    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'nome', object.nome, 'text-center', null, object),
        creator.addCol(idTable, index, 'ativa', object.labelAtiva, 'text-center', null, object),
      ];
      rows.push(creator.addRowDetailSelectModal(idTable, index, cols, null, null, this.idModalDetalhar, list[index]));
    }

    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

  selecionar(motivoSelecionado: Motivo): void {
    this.selecionarMotivo.emit(this.getClone(motivoSelecionado));
  }

  setarIdModalDetalhar(idModal: string): void {
    this.idModalDetalhar = idModal;
  }

  detalhar(motivo: Motivo): void {
    this.motivoSelecionado = motivo;
  }

  getIdModal(fields: string[]): string {
    return this.idModal(this.modalId, this.idBase, fields);
  }

  getIdField(field: string): string {
    return this.idField([this.getIdModal([]), field]);
  }

}
