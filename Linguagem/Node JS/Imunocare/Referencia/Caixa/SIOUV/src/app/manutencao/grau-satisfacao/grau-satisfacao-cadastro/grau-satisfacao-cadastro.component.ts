import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Util } from 'app/arquitetura/shared/util/util';
import { GrauSatisfacao } from 'app/shared/models/grau-satisfacao';
import { TipoOcorrencia } from 'app/shared/models/tipo-ocorrencia';
import { TipoOcorrenciaService } from 'app/shared/services/manutencao/tipo-ocorrencia.service';
import { Situacao } from 'app/shared/models/situacao';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { TipoOcorrenciaGrauSatisfacao } from 'app/shared/models/tipo-ocorrencia-grau-satisfacao';
import { TipoOcorrenciaGrauSatisfacaoId } from 'app/shared/models/tipo-ocorrencia-grau-satisfacao-id';
import { ActivatedRoute, Router } from '@angular/router';
import { GrauSatisfacaoService } from 'app/shared/services/manutencao/grau-satisfacao.service';

@Component({
  selector: 'app-grau-satisfacao-cadastro',
  templateUrl: './grau-satisfacao-cadastro.component.html',
  styleUrls: ['./grau-satisfacao-cadastro.component.css']
})
export class GrauSatisfacaoCadastroComponent extends BaseComponent {

  nome = 'nome';
  ativo = 'ativo';
  tiposOcorrenciaGrauSatisfacao = 'tiposOcorrenciaGrauSatisfacao';
  demandaAcompanhamento = 'demandaAcompanhamento';
  ocorrencia = 'ocorrencia';
  reabre = 'reabre';
  pzPrrgo = 'pzPrrgo';

  formulario: FormGroup;
  grauSatisfacao: GrauSatisfacao;

  tiposOcorrenciaTouched: Boolean = false;
  tiposOcorrencia: TipoOcorrencia[] = [];
  situacoes: Situacao[] = [];

  titulo: String = 'Incluir';
  tituloBotao: String = 'inclusão';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    protected messageService: MessageService,
    private tipoOcorrenciaService: TipoOcorrenciaService,
    private grauSatisfacaoService: GrauSatisfacaoService,
    private comumService: ComumService
  ) {
    super(messageService);
    this.grauSatisfacao = new GrauSatisfacao();
    this.createFormGroup();
    this.carregarSituacoes();
    this.carregarTiposOcorrencia();
    this.editar();
  }

  carregarTiposOcorrencia(): void {
    this.tipoOcorrenciaService.listarTiposOcorrenciaSemAcompanhamento(
      (tiposOcorrencia: TipoOcorrencia[]) => this.tiposOcorrencia = tiposOcorrencia);
  }

  carregarSituacoes(): void {
    this.comumService.listarSituacoesSemOpcaoTodos().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
        console.log('Erro ao consultar situações:', error);
      });
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      id: [this.grauSatisfacao.id],
      ativo: [this.grauSatisfacao.ativo, [Validators.required]],
      nome: [this.grauSatisfacao.nome, [Validators.required]],
      ocorrencia: [this.isChecked(), [Validators.requiredTrue]],
      tiposOcorrenciaGrauSatisfacao: [null],
      pzPrrgo: [this.grauSatisfacao.pzPrrgo, [Validators.required]]
    });
  }

  selectTipoOcorrenciaGrauSatisfacao(selecionado: TipoOcorrencia): TipoOcorrenciaGrauSatisfacao {
    return this.novoTipoOcorrenciaGrauSatisfacao(this.grauSatisfacao, selecionado);
  }

  novoTipoOcorrenciaGrauSatisfacao(grauSatisfacao: GrauSatisfacao, tipoOcorrencia: TipoOcorrencia): TipoOcorrenciaGrauSatisfacao {
    const tipoOcorrenciaGrauSatisfacaoId = new TipoOcorrenciaGrauSatisfacaoId(grauSatisfacao.id, tipoOcorrencia.id);
    return new TipoOcorrenciaGrauSatisfacao(tipoOcorrenciaGrauSatisfacaoId, null, tipoOcorrencia);
  }

  isCheckTiposOcorrencia(tipo: TipoOcorrencia): boolean {
    if (!this.grauSatisfacao.tiposOcorrenciaGrauSatisfacao) {
      return false;
    }
    for (let index = 0; index < this.grauSatisfacao.tiposOcorrenciaGrauSatisfacao.length; index++) {
      if (this.grauSatisfacao.tiposOcorrenciaGrauSatisfacao[index].tipoOcorrencia.id === tipo.id) {
        return true;
      }
    }
    return false;
  }

  limpar(): void {
    this.grauSatisfacao = new GrauSatisfacao();
    this.tiposOcorrenciaTouched = false;
    this.formulario.reset();
  }

  irManterGrauSatisfacao(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  voltar(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  editar(): void {
    this.route.params.subscribe(
      (params: any) => {
        const id: number = params['id'];
        if (!Util.isEmpty(id)) {
          this.titulo = 'Alterar';
          this.tituloBotao = 'alteração';
          this.grauSatisfacaoService.get(id).subscribe(
            (grauSatisfacao: GrauSatisfacao) => {
              this.grauSatisfacao = grauSatisfacao;
              for (let index = 0; index < this.grauSatisfacao.tiposOcorrenciaGrauSatisfacao.length; index++) {
                this.grauSatisfacao.tiposOcorrenciaGrauSatisfacao[index] =
                  this.novoTipoOcorrenciaGrauSatisfacao(this.grauSatisfacao, this.grauSatisfacao.tiposOcorrenciaGrauSatisfacao[index].tipoOcorrencia);
              }
              this.createFormGroup();
            },
            error => {
              this.messageService.addMsgDanger('Ocorreu um erro ao carregar o grau satisfação.');
              console.log('Erro ao carregar o grau satisfação:', error);
            }
          );
        }
      });
  }

  isChecked(): boolean {
    return Util.convertSNtoBool(this.grauSatisfacao.ocorrencia);
  }

  isNew(): boolean {
    return Util.isEmpty(this.grauSatisfacao.id);
  }

  salvar(): void {
    if (!this.validarFormularioAll(this.formulario, [this.grauSatisfacao.tiposOcorrenciaGrauSatisfacao])) {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
    }
    this.montarObj();
    this.grauSatisfacaoService.verificarDuplicidadeRegistro(this.grauSatisfacao).subscribe(
      (isDuplicado: boolean) => {
        if (Util.isEmpty(isDuplicado) ? false : isDuplicado) {
          this.messageService.addMsgDanger('Registro já cadastrado.');
        } else {
          this.finalizarSalvar();
        }
      }
    );
  }

  finalizarSalvar(): void {
    if (this.isNew()) {
      this.grauSatisfacaoService.post(this.grauSatisfacao).subscribe(
        (grauSatisfacao: GrauSatisfacao) => {
          this.router.navigate(['.'], { relativeTo: this.route.parent });
          this.messageService.addMsgSuccess('Grau Satisfação inserido com sucesso.');
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao incluir o grau satisfação.');
          console.log('Erro ao incluir grau satisfação:', error);
        }
      );
    } else {
      this.grauSatisfacaoService.put(this.grauSatisfacao).subscribe(
        (grauSatisfacao: GrauSatisfacao) => {
          this.router.navigate(['.'], { relativeTo: this.route.parent });
          this.messageService.addMsgSuccess('Grau Satisfação alterado com sucesso.');
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao alterar o grau satisfação.');
          console.log('Erro ao alterar o grau satisfação, ocasionado por:', error);
        }
      );
    }
  }

  montarObj(): void {
    const tiposOcorrenciaGrauSatisfacao = this.grauSatisfacao.tiposOcorrenciaGrauSatisfacao;
    console.log("1", this.grauSatisfacao);
    this.grauSatisfacao = this.getClone(this.formulario.value);
    console.log("2", this.grauSatisfacao);
    this.grauSatisfacao.tiposOcorrenciaGrauSatisfacao = tiposOcorrenciaGrauSatisfacao;
    this.grauSatisfacao.ocorrencia = Util.convertBoolToSN(this.grauSatisfacao.ocorrencia);
    // Dados setados manualmente seguindo o sistema antigo
    this.grauSatisfacao.demandaAcompanhamento = 'N';
    this.grauSatisfacao.reabre = 'S';
  }

  changeTouched(value: any, isChecked: boolean, list: Array<any>): void {
    this.tiposOcorrenciaTouched = true;
    this.onChangeCheckbox(value, isChecked, list);
  }

}
