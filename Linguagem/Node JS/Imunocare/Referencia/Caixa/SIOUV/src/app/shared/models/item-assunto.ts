import { Entity } from 'app/arquitetura/shared/models/entity';

export class ItemAssunto extends Entity {
  public usuario: string = null;
  public agencia: string = null;
  public ativo: string = null;
  public cartaoCredito: string = null;
  public carteiraTrabalho: string = null;
  public contratoHabitacao: string = null;
  public correspondenteBancario: string = null;
  public informacaoBancaria: string = null;
  public nis: string = null;
  public revendedorLoterico: string = null;
  public itemAssunto: string = null;
  public assunto: number = null;
  public natural: number = null;
  public unidade: number = null;
}
