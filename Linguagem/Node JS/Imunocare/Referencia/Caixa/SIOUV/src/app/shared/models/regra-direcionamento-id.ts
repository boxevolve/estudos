export class RegraDirecionamentoId {
  public nuItemAssunto: number = null;
  public nuMotivo: number = null;
  public nuTipoOcorrencia: number = null;
  public sgUf: string = null;
}
