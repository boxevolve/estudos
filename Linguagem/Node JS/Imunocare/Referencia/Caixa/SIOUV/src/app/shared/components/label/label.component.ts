import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { MessageService } from '../messages/message.service';

@Component({
    selector: 'app-label',
    templateUrl: './label.component.html',
    styleUrls: ['./label.component.css']
})
export class LabelComponent extends BaseComponent implements OnInit {

    @Input("title") title: string;
    @Input("required") required: boolean;

    constructor(
        protected messageService: MessageService,
    ) {
        super(messageService);
        this.title = "";
        this.required = false;
    }

    ngOnInit() {
    }

}
