import { Entity } from 'app/arquitetura/shared/models/entity';
import { ItemAssunto } from './item-assunto';

export class Assunto extends Entity {
	public usuario: string = null;
	public ativo: string = null;
	public desconsideraAvlco: string = null;
	public noAssunto: string = null;
	public descricaoSituacao: string = null;
	public itensAssunto: ItemAssunto[] = new Array<ItemAssunto>();
}
