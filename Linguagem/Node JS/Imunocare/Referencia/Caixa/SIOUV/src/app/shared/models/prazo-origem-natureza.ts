import { PrazoOrigemNaturezaId } from "./prazo-origem-natureza-id";
import { Origem } from "./origem";
import { Natureza } from "./natureza";

export class PrazoOrigemNatureza {
    constructor(
        public id: PrazoOrigemNaturezaId,
        public tipoOrigem: Origem,
        public pzSolucao: number,
        public natureza: Natureza
    ) {}
}

