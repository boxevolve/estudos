import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { NaturezaHistorico } from 'app/shared/models/natureza-historico';
import { NaturezaHistoricoFiltro } from 'app/shared/models/natureza-historico.filtro';

@Injectable()
export class NaturezaHistoricoService extends CrudHttpClientService<NaturezaHistorico> {

	constructor(protected http: HttpClient) {
		super('manutencao/natureza-historico', http);
	}
	
	consultarPorFiltro(filtro: NaturezaHistoricoFiltro): any {
        return this.http.post<NaturezaHistorico[]>(this.url + '/consulta/filtro', filtro, this.options());
    }

}
