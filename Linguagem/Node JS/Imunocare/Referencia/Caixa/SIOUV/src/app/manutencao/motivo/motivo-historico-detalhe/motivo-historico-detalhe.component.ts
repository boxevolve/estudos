import { Component, Output, ViewChild, ElementRef, AfterViewInit, EventEmitter, Input } from '@angular/core';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';
import { MotivoHistorico } from 'app/shared/models/motivo-historico';
import { NaturezaMotivoHistorico } from 'app/shared/models/natureza-motivo-historico';
import { NaturezaMotivoHistoricoService } from 'app/shared/services/manutencao/natureza-motivo-historico.service';

@Component({
  selector: 'app-motivo-historico-detalhe',
  templateUrl: './motivo-historico-detalhe.component.html',
  styleUrls: ['./motivo-historico-detalhe.component.css']
})
export class MotivoHistoricoDetalheComponent extends BaseComponent implements AfterViewInit {

  modalId = 'modalMotivoHistoricoDetalhe';

  @ViewChild('modalMotivoHistoricoDetalhe') modal: ElementRef;
  @Input('historicoSelecionado') historicoSelecionado: MotivoHistorico;
  @Output() emitModal = new EventEmitter();

  detalhes: any[] = [];
  naturezasMotivoHistorico: NaturezaMotivoHistorico[] = [];
  carregado = false;
  datalist: SiouvTable;

  constructor(
      protected messageService: MessageService,
      protected naturezaMotivoHistoricoService: NaturezaMotivoHistoricoService
  ) {
      super(messageService);
      this.criarTabela([]);
  }

  ngAfterViewInit() {
    this.emitModal.emit(this.getClone(this.modalId));
    $(this.modal.nativeElement).on('hidden.bs.modal',  () => {
        this.limpar();
    });
  }

  limpar(): void {
      this.historicoSelecionado = null;
      this.carregado = !this.carregado;
  }

  getDetalhes(): any[] {
    if (this.historicoSelecionado && !this.carregado) {

      this.detalhes = [
        {titulo: 'Usuário:', value: this.historicoSelecionado.labelUsuario},
        {titulo: 'Operação:', value: this.historicoSelecionado.tipoOperacao.noTipoOperacao},
        {titulo: 'Data/Hora:', value: this.historicoSelecionado.labelDataHora},
        {titulo: 'Motivo:', value: this.historicoSelecionado.noMotivo},
        {titulo: 'Situação:', value: this.historicoSelecionado.labelSituacao},
        {titulo: 'Descrição:', value: this.historicoSelecionado.deMotivo}
      ];
      this.carregado = !this.carregado;

      const nuMotivo = this.historicoSelecionado.id.nuMotivo.toString();

      this.naturezaMotivoHistoricoService.consultarPorNuMotivo(nuMotivo).
        subscribe(historicos => {
          this.naturezasMotivoHistorico = historicos;
          this.criarTabela(this.naturezasMotivoHistorico);
        });
    }
    return this.detalhes;
  }

  text(text: string): string {
    return text ? text : '';
  }

  criarTabela(list: any[]): void {
    this.datalist = this.createSiouvTable(list);
  }

  createSiouvTable(list: NaturezaMotivoHistorico[]): SiouvTable {
    const idTable = 'NaturezasMotivoHistorico';
    const creator: SiouvTableCreator<NaturezaMotivoHistorico> = new SiouvTableCreator();

    const headers = [
      creator.addHeader('natureza', 'Natureza', 33, 'text-center labelAzul', {'vertical-align': 'middle'}),
      creator.addHeader('situacao', 'Situação', 33, 'text-center labelAzul', {'vertical-align': 'middle'}),
      creator.addHeader('operacao', 'Operação', 33, 'text-center labelAzul', {'vertical-align': 'middle'})
    ];

    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'nome', object.labelNatureza, 'text-center', null, object),
        creator.addCol(idTable, index, 'ativa', object.labelAtiva, 'text-center', null, object),
        creator.addCol(idTable, index, 'operacao', object.labelTipoOperacao, 'text-center', null, object)
      ];
      rows.push(creator.addRowNone(idTable, index, cols, null, null, list[index]));
    }

    return creator.newSiouvTableNoPagination(idTable, headers, rows, null, null, list);
  }

}
