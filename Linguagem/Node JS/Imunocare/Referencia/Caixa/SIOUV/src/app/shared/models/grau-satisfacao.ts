import { Entity } from 'app/arquitetura/shared/models/entity';
import { TipoOcorrenciaGrauSatisfacao } from './tipo-ocorrencia-grau-satisfacao';

export class GrauSatisfacao extends Entity {
    public nome: string = null;
    public ativo: string = null;
    public demandaAcompanhamento: string = null;
    public ocorrencia: string = null;
    public reabre: string = null;
    public pzPrrgo: number = null;
    public tiposOcorrenciaGrauSatisfacao: TipoOcorrenciaGrauSatisfacao[] = Array<TipoOcorrenciaGrauSatisfacao>();
    public labelTiposOcorrenciaGrauSatisfacao: string;
    public labelAtivo: string;
    public labelTipoDeDemanda: string;
}
