import { Entity } from 'app/arquitetura/shared/models/entity';
import { TipoCorreioEletronico } from './tipo-correio-eletronico';

export class CorreioEletronico extends Entity {
    public deCorreioEletronico: string = null;
    public deDestinatario: string = null;
    public deRemetente: string = null;
    public dtEnvio: Date = null;
    public ativo: string = null;
    public envioAnterior: string = null;
    public envioPosterior: string = null;
    public solicitanteInterno: string = null;
    public solicitanteExterno: string = null;
    public unidadeDestino: string = null;
    public unidadeOrigem: string = null;
    public unidadeSuperiorOrigem: string = null;
    public assuntoCorreioEltco: string = null;
    public tipoCorreioEletronico: number = null;
    public dias: number = null;
    public tiposCorreioEletronico: TipoCorreioEletronico[] = Array<TipoCorreioEletronico>();
    public labelAtivo: string = null;

}
