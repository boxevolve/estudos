import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Entity } from '../../../arquitetura/shared/models/entity';
import { Injectable } from '@angular/core';
import { UnidadeFiltro } from 'app/shared/models/unidade.filtro';
import { Unidade } from 'app/shared/models/unidade';
import { MessageService } from 'app/shared/components/messages/message.service';

@Injectable()
export class UnidadeService extends CrudHttpClientService<Entity> {

  constructor(
    protected http: HttpClient,
    private messageService: MessageService
  ) {
    super('ico/unidade', http);
  }

  consultar(unidadeFiltro: UnidadeFiltro, next?: (unidades: Unidade[]) => void, errorCallback?: any): any {
    this.callPOST<Unidade[]>('/consultar', unidadeFiltro, next, error => {
      const message = errorCallback(error, 'Ocorreu um erro ao pesquisar unidades.');
      this.messageService.addMsgDanger(message);
      console.log('Erro ao chamar o servico. Erro: ' + error);
    });
  }

  consultarUnidadesVinculadas(unidade: Unidade, next?: (unidades: Unidade[]) => void): any {
    this.callPOST<Unidade[]>('/consultar/vinculadas', unidade, next, error => {
      this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar unidades vinculadas.');
      console.log('Erro ao chamar o servico. Erro: ' + error);
    });
  }

}
