import { Entity } from 'app/arquitetura/shared/models/entity';

export class UnidadeSndc extends Entity {
    public noUnidadeSndc: string = null;
    public sgUf: string = null;
    public icAtivo: string = null;
}
