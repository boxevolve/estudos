import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { OrigemConsultaComponent } from './origem-consulta/origem-consulta.component';
import { OrigemCadastroComponent } from './origem-cadastro/origem-cadastro.component';
const routes: Routes =  [
    {
        path: '',
        canActivate: [AuthGuard, DadosUsuarioGuard],
        canActivateChild: [AuthGuard, DadosUsuarioGuard],
        children: [
        {
                path: '',
                component: OrigemConsultaComponent
            },
            {
                path: 'novo',
                component: OrigemCadastroComponent
            },
            {
                path: ':id/editar',
                component: OrigemCadastroComponent
            }
                ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ManterOrigemRoutingModule {}
