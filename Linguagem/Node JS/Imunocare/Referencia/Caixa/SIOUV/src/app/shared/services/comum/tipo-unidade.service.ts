import { HttpClient } from '@angular/common/http';
import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Injectable } from '@angular/core';
import { Uf } from 'app/shared/models/uf';
import { MessageService } from 'app/shared/components/messages/message.service';
import { TipoUnidade } from 'app/shared/models/tipo-unidade';

@Injectable()
export class TipoUnidadeService extends CrudHttpClientService<TipoUnidade> {
  
  constructor(protected http: HttpClient,
    private messageService: MessageService) {
    super('ico/tipo-unidade', http);
  }

  todos(next?: (value: any) => void): any {
    return this.doGET('/').subscribe(next,
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar Tipos Unidade.');
        console.log('Erro ao consultar Tipos Unidade', error);
      });
  }

  private doGET(endpoint: string): any {
    return this.http.get<Uf[]>(this.url + endpoint, this.options());
  }

}
