import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from '../../../arquitetura/shared/services/crud-http-client.service';
import { GrauSatisfacao } from '../../models/grau-satisfacao';

@Injectable()
export class GrauSatisfacaoService extends CrudHttpClientService<GrauSatisfacao> {

    constructor(protected http: HttpClient) {
        super('ocorrencia/grau-satisfacao', http);
    }

    todos(): any {
        return this.http.get<GrauSatisfacao[]>(this.url , this.options());
    }

    verificarDuplicidadeRegistro(grauSatisfacao: GrauSatisfacao): any {
        return this.http.post<boolean>(this.url + '/verificar/duplicidade', grauSatisfacao, this.options());
    }

    verificarVinculo(idGrauSatisfacao: number): any {
        return this.http.post<boolean>(this.url + '/verificar/vinculo', idGrauSatisfacao, this.options());
    }

}
