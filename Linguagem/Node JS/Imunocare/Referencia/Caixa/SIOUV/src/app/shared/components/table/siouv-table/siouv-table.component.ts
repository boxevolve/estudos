import { Component, Input, Output } from '@angular/core';
import { BaseComponent } from '../../base.component';
import { MessageService } from '../../messages/message.service';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { SiouvCol } from 'app/shared/models/siouv-col';
import { SiouvRow } from 'app/shared/models/siouv-row';
import { EventEmitter } from '@angular/core';
import { ConfirmListener } from '../../messages/confirm-listener';
import { Util } from 'app/arquitetura/shared/util/util';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';
import { PaginationInstance } from 'ngx-pagination';

@Component({
  selector: 'app-siouv-table',
  templateUrl: './siouv-table.component.html',
  styleUrls: ['./siouv-table.component.css']
})
export class SiouvTableComponent extends BaseComponent {

  @Input('dataList') dataList: SiouvTable;
  @Output() emitEdit = new EventEmitter();
  @Output() emitDelete = new EventEmitter();
  @Output() emitDetail = new EventEmitter();
  @Output() emitSelect = new EventEmitter();

  hintEdit = 'Alterar';
  hintDelete = 'Excluir';
  hintDetail = 'Detalhar';
  hintSelect = 'Selecionar';
  mgnDelete = 'Deseja realmente excluir?';
  idTableDefault = 'idTableDefault';

  constructor(
    protected messageService: MessageService,
  ) {
    super(messageService);
    this.dataList = new SiouvTableCreator().newDefaultSiouvTable('defaultTable', [], [], null, null, []);
  }

  public edit(objectRow: any): void {
    this.emitEdit.emit(this.getClone(objectRow));
  }

  public delete(objectRow: any): void {
    this.messageService.addConfirmYesNo(this.mgnDelete, (): ConfirmListener => {
      this.emitDelete.emit(this.getClone(objectRow));
      return;
    }, null, null, 'Sim', 'Não');
  }

  public detail(objectRow: any): void {
    this.emitDetail.emit(this.getClone(objectRow));
  }

  public select(objectRow: any, idModal: string): void {
    this.emitSelect.emit(this.getClone(objectRow));
    this.hideModal(idModal);
  }

  // Validações para não estourar a tela
  idTable(): string {
    return this.dataList ? (this.dataList.id ? this.dataList.id : this.idTableDefault) : this.idTableDefault;
  }

  title(): string {
    return this.dataList ? (!Util.isEmpty(this.dataList.title) ? this.dataList.title : '') : '';
  }

  headers(): SiouvCol[] {
    return this.dataList ? (this.dataList.headers ? this.dataList.headers : []) : [];
  }

  rows(): SiouvRow[] {
    return this.dataList ? (this.dataList.rows ? this.dataList.rows : []) : [];
  }

  numberItems(): number {
    if (!this.showPaginationList()) {
      return this.listObject().length;
    }
    return this.dataList ? (this.dataList.numberItems ? this.dataList.numberItems : 5) : 5;
  }

  numberPage(): number {
    return this.dataList ? (this.dataList.numberPage ? this.dataList.numberPage : 1) : 1;
  }

  fieldId(field: string): string {
    return this.idTable() + '_' + field;
  }

  rowClass(row: SiouvRow): string {
    return row ? (row.showHover ? 'bg-row ' + row.clazz : row.clazz) : row.clazz;
  }

  showTitle(): boolean {
    return this.dataList ? !Util.isEmpty(this.dataList.title) : false;
  }

  showAction(row: SiouvRow): boolean {
    return row ? (row.showEdit || row.showDelete || row.showDetail || row.showSelect) : false;
  }

  showEdit(row: SiouvRow): boolean {
    return row ? row.showEdit : false;
  }

  showDelete(row: SiouvRow): boolean {
    return row ? row.showDelete : false;
  }

  showDetail(row: SiouvRow): boolean {
    return row ? row.showDetail : false;
  }

  showSelect(row: SiouvRow): boolean {
    return row ? row.showSelect : false;
  }

  showLinkModal(row: SiouvRow): boolean {
    return row ? !Util.isEmpty(row.idModal) : false;
  }

  showPagination(): boolean {
    return this.showPaginationList() && this.showPaginacao(this.listObject());
  }

  showPaginationList(): boolean {
    return this.dataList ? this.dataList.showPagination : false;
  }

  getIdModal(row: SiouvRow): string {
    return row ? '#' + row.idModal : '';
  }

  getIdPagination(): string {
    return this.idField(['pagination', this.idTable()]);
  }

  listObject(): any[] {
    return this.dataList ? (this.dataList.listObject ? this.dataList.listObject : []) : [];
  }

  style(col: any): any {
    return col ? col.style : '';
  }

  configPagination(): PaginationInstance {
    const config: PaginationInstance = {
      id: this.getIdPagination(),
      currentPage: this.numberPage(),
      itemsPerPage: this.numberItems()
    };
    return config;
  }

}
