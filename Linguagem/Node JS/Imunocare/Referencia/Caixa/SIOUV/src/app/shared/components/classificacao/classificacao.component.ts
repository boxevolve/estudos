import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Assunto } from 'app/shared/models/assunto';
import { Item } from 'app/shared/models/item';
import { Motivo } from 'app/shared/models/motivo';
import { Natureza } from 'app/shared/models/natureza';
import { Origem } from 'app/shared/models/origem';
import { AssuntoService } from 'app/shared/services/manutencao/assunto.service';
import { ManterItemService } from 'app/shared/services/manutencao/manter-item.service';
import { MotivoService } from 'app/shared/services/manutencao/motivo.service';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { Classificacao } from './classificacao';
import { OrigemService } from 'app/shared/services/manutencao/tipo-origem.service';

@Component({
  selector: 'app-classificacao',
  templateUrl: './classificacao.component.html',
  styleUrls: ['./classificacao.component.css']
})
export class ClassificacaoComponent implements OnInit {

  @Input('tipoOcorrencia')
  tipoOcorrencia: number;

  @Input('formulario')
  formulario: FormGroup;

  @Output('configurarClassificacao')
  configurarClassificacao: EventEmitter<Classificacao>;

  @Output('configuradorOrigem')
  configOrigem: EventEmitter<Origem>;
  @Output('configuradorNatureza')
  configNatureza: EventEmitter<Natureza>;
  @Output('configuradorItem')
  configItem: EventEmitter<Item>;
  @Output('configuradorAssunto')
  configAssunto: EventEmitter<Assunto>;
  @Output('configuradorMotivo')
  configMotivo: EventEmitter<Motivo>;



  /* formGroup: FormGroup; */
  classificacao: Classificacao;

  origens: Origem[];
  naturezas: Natureza[];
  assuntos: Assunto[];
  itens: Item[];
  motivos: Motivo[];
  origemSelecionada: any;

  constructor(private formBuilder: FormBuilder,
    private origemService: OrigemService,
    private naturezaService: NaturezaService,
    private assuntoService: AssuntoService,
    private itemService: ManterItemService,
    private motivoService: MotivoService) {
    /*     this.formGroup = this.formBuilder.group({
          origem: [this.classificacao.origem, [Validators.required]],
          natureza: [this.classificacao.natureza, [Validators.required]],
          assunto: [this.classificacao.assunto, [Validators.required]],
          item: [this.classificacao.item, [Validators.required]],
          motivo: [this.classificacao.motivo, [Validators.required]]
        }); */
  }

  ngOnInit() {
    alert(this.formulario);
    console.log(this.tipoOcorrencia);
    this.formulario.addControl('origem', this.formBuilder.control('origem', Validators.required));
    this.formulario.addControl('natureza', this.formBuilder.control('natureza', Validators.required));
    this.formulario.addControl('assunto', this.formBuilder.control('assunto', Validators.required));
    this.formulario.addControl('item', this.formBuilder.control('item', Validators.required));
    this.formulario.addControl('motivo', this.formBuilder.control('motivo', Validators.required));
    this.carregarOrigensPorTipoOcorrencia();
  }

  public static newFormGroup(builder: FormBuilder): FormGroup {
    return builder.group({
      origem: ["origem", Validators.required],
      natureza: ["natureza", Validators.required],
      assunto: ["assunto", Validators.required],
      item: ["item", Validators.required],
      motivo: ["motivo", Validators.required]
    })
  }

  private carregarOrigensPorTipoOcorrencia() {
    console.log(this.tipoOcorrencia);
    this.origens = this.origemService.carregarPorTipoOcorrencia(this.tipoOcorrencia);
  }

  public carregarNaturezasPorOrigem() {
    console.log(this.classificacao.idOrigem);
    this.naturezas = this.naturezaService.carregarNaturezasPorOrigem(this.classificacao.idOrigem);
  }

  public selecionarOrigem(){
    this.configOrigem.emit(this.origemSelecionada);
  }

  public carregarAssuntosPorNatureza() {
    console.log(this.classificacao.idNatureza);
    this.assuntos = this.assuntoService.carregarAssuntosPorNatureza(this.classificacao.idNatureza);
  }

  public carregarItensPorAssunto() {
    console.log(this.classificacao.idAssunto);
    this.itens = this.itemService.carregarItensPorAssunto(this.classificacao.idAssunto);
  }

  public carregarMotivosPorItem() {
    console.log(this.classificacao.idItem);
    this.motivos = this.motivoService.carregarMotivosPorItem(this.classificacao.idItem);
  }

  public selecionarClassificacao(): void {
    this.configurarClassificacao.emit(this.classificacao);
  }
}
