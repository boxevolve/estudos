import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxPaginationModule } from 'ngx-pagination';

import { LabelComponent } from './label.component';

@NgModule({
	imports: [CommonModule, NgxPaginationModule],
	declarations: [LabelComponent],
	exports: [LabelComponent, NgxPaginationModule],
})
export class LabelModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: LabelModule
		};
	}
}
