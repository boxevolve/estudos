import { Injectable } from '@angular/core';
import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import {ClassificacaoFiltro } from 'app/shared/models/classificacao-ocorrencia-filtro';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { ClassificacaoOcorrencia } from 'app/shared/models/classificacao-ocorrencia';

@Injectable()
export class ClassificacaoService extends CrudHttpClientService<ClassificacaoFiltro> {

  constructor(protected http: HttpClient) {
      super('ocorrencia/classificacao', http);
   }

   consultarPorFiltros(classificacao: ClassificacaoFiltro) {
    return this.http.post(this.url + '/consulta/filtro', classificacao );
   }

}
