import { Component, Output, AfterViewInit, Input, ChangeDetectorRef } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { RegraDirecionamento } from 'app/shared/models/regraDirecionamento';
import { Util } from 'app/arquitetura/shared/util/util';

@Component({
  selector: 'app-regra-direcionamento-btn',
  templateUrl: './regra-direcionamento-btn.component.html',
  styleUrls: ['./regra-direcionamento-btn.component.css']
})
export class RegrasDirecionamentoBtnComponent extends BaseComponent implements AfterViewInit {

  @Input() idBase = 'regras_direcionamento_btn_base';
  @Input() position: number;
  @Input() salvos: RegraDirecionamento[] = [];
  @Output() selecionarRegras = new EventEmitter();

  /**
  * Nomes dos formControlName dos campos
  */
  nameBtn = 'btn';

  hintBtn = 'Incluir Regra';

  idModalRegra: string;

  constructor(
    private cdr: ChangeDetectorRef,
    protected messageService: MessageService
  ) {
    super(messageService);
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  receberId(idModal: any): void {
    this.idModalRegra = idModal;
  }

  getId(): string {
    return Util.isEmpty(this.idModalRegra) ? null : this.idModalRegra;
  }

  show(): void {
    this.showModal(this.getId());
  }

  receberRegras(regras: RegraDirecionamento[]): void {
    this.selecionarRegras.emit(this.getClone({regras: regras, position: this.position}));
  }

  getRegrasSalvas(): RegraDirecionamento[] {
    return !Util.isEmpty(this.salvos) ? this.salvos : [];
  }

}
