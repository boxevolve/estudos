import { Unidade } from "./unidade";

export class UnidadeOrigemDestino {
    public unidadeOrigem: Unidade;
    public unidadeDestino: Unidade;
}