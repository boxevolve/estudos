import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Natureza } from 'app/shared/models/natureza';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { TipoOcorrencia } from 'app/shared/models/tipo-ocorrencia';
import { TipoOcorrenciaService } from 'app/shared/services/manutencao/tipo-ocorrencia.service';
import { Unidade } from 'app/shared/models/unidade';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';

@Component({
  selector: 'app-natureza-consulta',
  templateUrl: './natureza-consulta.component.html',
  styleUrls: ['./natureza-consulta.component.css']
})
export class NaturezaConsultaComponent extends BaseComponent {

  naturezas: Natureza[] = null;
  dataList: SiouvTable;
  tiposOcorrencia: TipoOcorrencia[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    protected messageService: MessageService,
    private tipoOcorrenciaService: TipoOcorrenciaService,
    private naturezaService: NaturezaService,
  ) {
    super(messageService);
    this.tiposOcorrencia = [];
    this.carregarTiposOcorrencia();
    this.carregarNaturezas();
  }

  carregarTiposOcorrencia(): void {
    this.tipoOcorrenciaService.listarTiposOcorrenciaSemAcompanhamento((tiposOcorrencia: TipoOcorrencia[]) => {
      this.tiposOcorrencia = tiposOcorrencia;
    });
  }

  carregarNaturezas(): void {
    this.naturezaService.get().subscribe(
      (naturezas: Natureza[]) => {
        this.naturezas = naturezas;
        this.atualizarDataList();
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as naturezas.');
        console.log('Erro ao recuperar as naturezas:', error);
      }
    );
  }

  pesquisarHistorico(): void {
    this.router.navigate(['historico'], { relativeTo: this.route.parent });
  }

  incluir(): void {
    this.router.navigate(['novo'], { relativeTo: this.route.parent });
  }

  alterar(natureza: Natureza): void {
    this.router.navigate([natureza.id, 'editar'], { relativeTo: this.route.parent });
  }

  excluir(natureza: Natureza): void {
    const indice: number = this.naturezas.findIndex(item => item.id === natureza.id);
    this.naturezaService.delete(natureza.id).subscribe(() => {
        this.messageService.addMsgSuccess('Natureza excluída com sucesso.');
        if ( indice >= 0 ) {
          this.naturezas.splice(indice, 1);
        }
        this.atualizarDataList();
      },
      error => {
        const message = this.getMessageError(error, 'Ocorreu um erro ao excluir a natureza.');
        this.messageService.addMsgDanger(message);
        console.log('Erro ao excluir natureza:', error);
      }
    );
  }

  atualizarDataList() {
    this.dataList = this.createSiouvTable(this.naturezas);
  }

  createSiouvTable(list: Natureza[]): SiouvTable {
    const idTable = 'consultaNatureza';
    const creator: SiouvTableCreator<Natureza> = new SiouvTableCreator();
    // Cabeçalhos
    // Id - Texto - Class - Style
    const headers = [
      creator.addHeader('natureza', 'Natureza', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('tipo_ocorrencia', 'Tipo de Ocorrência', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('aceite', 'Aceite', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('restrita', 'Restrita', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('situacao', 'Situação', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('unidadeResponsavelFinal', 'Unidade Responsável Final', 12.5, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('orientacao', 'Orientação', 12.5, null, {'vertical-align': 'middle'}),
      creator.addHeader('acao', 'Ação', 12.5, 'text-center', {'vertical-align': 'middle'})
    ];

    // Linhas
    // Id - Texto - Class - Style - Objeto
    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'nome', object.nome, 'text-center', null, object),
        creator.addCol(idTable, index, 'tiposOcorrenciaNatureza', object.labelTiposOcorrenciaNatureza, 'text-center', null, object),
        creator.addCol(idTable, index, 'aceite', object.labelAceite, 'text-center', null, object),
        creator.addCol(idTable, index, 'restrito', object.labelRestrito, 'text-center', null, object),
        creator.addCol(idTable, index, 'ativa', object.labelAtiva, 'text-center', null, object),
        creator.addCol(idTable, index, 'unidadeResponsavelFinal', object.labelUnidadeResponsavelFinal, 'text-center', null, object),
        creator.addCol(idTable, index, 'orientacao', object.orientacao, null, null, object),
      ];
      rows.push(creator.addRow(idTable, index, cols, null, null, list[index]));
    }

    // Table
    // Id - Cabeçalhos - Linhas - Class - Style - Lista
    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

  receberUnidade(unidade: Unidade): void {

  }

}
