import { Component, Input } from '@angular/core';
import { BaseComponent } from '../../base.component';
import { MessageService } from '../../messages/message.service';

@Component({
  selector: 'app-required-field',
  templateUrl: './required-field.component.html',
  styleUrls: ['./required-field.component.css']
})
export class RequiredFieldComponent extends BaseComponent {
  @Input('show') show: boolean;
  @Input('showLength') showLength: boolean;
  @Input('width') width: number;

  text: string;

  constructor(protected messageService: MessageService) {
    super(messageService);
    this.width = 100;
    this.showLength = false;
  }

  getText(): string {
    return this.showLength
      ? 'Informe pelo menos três caracteres.'
      : 'Campo Obrigatório';
  }
}
