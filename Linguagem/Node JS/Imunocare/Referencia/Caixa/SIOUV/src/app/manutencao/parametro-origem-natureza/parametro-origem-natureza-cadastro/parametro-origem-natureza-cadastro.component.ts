import { Component, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Util } from 'app/arquitetura/shared/util/util';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SimNao } from 'app/shared/models/sim-nao';
import { PermissaoTransferenciaTodasUnidades } from 'app/shared/models/permissao-transferencia-todas-unidades';
import { ParametroOrigemNatureza } from 'app/shared/models/parametro-origem-natureza';
import { ParametroOrigemNaturezaId } from 'app/shared/models/parametro-origem-natureza-id';
import { Origem } from 'app/shared/models/origem';
import { Natureza } from 'app/shared/models/natureza';
import { TipoOcorrencia } from 'app/shared/models/tipo-ocorrencia';
import { TipoOcorrenciaService } from 'app/shared/services/manutencao/tipo-ocorrencia.service';
import { OcorrenciaService } from 'app/shared/services/ocorrencia/ocorrencia.service';
import { ParametroOrigemNaturezaService } from 'app/shared/services/manutencao/parametro-origem-natureza.service';
import { Unidade } from 'app/shared/models/unidade';
import { UnidadeUtil } from 'app/shared/models/unidade-util';
import { UnidadeSemOcorrenciaId } from 'app/shared/models/unidade-sem-ocorrencia-id';
import { UnidadeSemOcorrenciaService } from 'app/shared/services/manutencao/unidade-sem-ocorrencia.service';

@Component({
    selector: 'app--parametro-origem-natureza--cadastro',
    templateUrl: './parametro-origem-natureza-cadastro.component.html',
    styleUrls: ['./parametro-origem-natureza-cadastro.component.css']
  })
  export class ParametroOrigemCadastroComponent extends BaseComponent implements AfterViewInit {
/**
* Nomes dos formControlName dos campos
*/
nameTipoOcorrencia = 'nuTipoOcorrencia';
nameTipoOrigem = 'nuTipoOrigem';
nameNatureza = 'nuNatureza';
namePermitirUnidadeRespondam = 'icResposta';
namePermitirTransferenciaTodasUnidades = 'icTransferencia';
nameUnidadeTransferencia = 'nuUnidadeTransferencia';

formulario: FormGroup;
parametroOrigemNatureza: ParametroOrigemNatureza;
tituloOcorrenciaInterna: string = null;
tiposOcorrencia: TipoOcorrencia[] = [];
origens: Origem[];
naturezas: Natureza[];
simNao: SimNao[] = [];
permissaoTransferenciaTodasUnidades: PermissaoTransferenciaTodasUnidades[] = [];
showCamposDefinirParametroOrigemOcorrencia = true;
showCamposDefinirParametroOrigemDemandaAcompanhamento = true;
disabledCampoOrigem = true;
disabledCombosDefinirParametroOrigemOcorrencia = true;
naturezasSelecionadas: Natureza[] = [];
itensParametroOrigemNatureza: ParametroOrigemNatureza[] = [];
checkNatureza = false;
unidadeUtil: UnidadeUtil;

  constructor(
    protected messageService: MessageService,
    private cdr: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private comumService: ComumService,
    private tipoOcorrenciaService: TipoOcorrenciaService,
    private ocorrenciaService: OcorrenciaService,
    private parametroOrigemNaturezaService: ParametroOrigemNaturezaService,
    private unidadeSemOcorrenciaService: UnidadeSemOcorrenciaService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    super(messageService);
    this.parametroOrigemNatureza = new ParametroOrigemNatureza();
    this.unidadeUtil = new UnidadeUtil(messageService, cdr);
    this.createFormGroup();
    this.carregarTiposOcorrencia();
    this.carregarSimNao();
    this.carregarPermissaoTransferenciaTodasUnidades();
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

    carregarTiposOcorrencia(): void {
        this.tipoOcorrenciaService.listarTiposOcorrenciaSemAcompanhamento((tiposOcorrencia: TipoOcorrencia[]) => {
            this.tiposOcorrencia = tiposOcorrencia;
        });
    }

    createFormGroup(): void {
        this.formulario = this.formBuilder.group({
            id: [this.parametroOrigemNatureza.id],
            nuTipoOcorrencia: [this.parametroOrigemNatureza.nuTipoOcorrencia],
            nuTipoOrigem: [this.parametroOrigemNatureza.nuTipoOrigem, [ Validators.required ]],
            nuNatureza: [this.parametroOrigemNatureza.nuNatureza],
            icResposta: [this.parametroOrigemNatureza.icResposta],
            icTransferencia: [this.parametroOrigemNatureza.icTransferencia],
            nuUnidadeTransferencia: [this.parametroOrigemNatureza.nuUnidadeTransferencia]
        });
    }

    public recuperarTipoOrigemPorTipoOcorrencia(tipoOcorrenciaSelecionado: number) {
        if (tipoOcorrenciaSelecionado !== null) {
            this.disabledCampoOrigem = false;
        }

       if (!this.showCamposDefinirParametroOrigemOcorrencia) {
            this.showCamposDefinirParametroOrigemOcorrencia = true;
            // this.formulario.reset();
       }

        this.parametroOrigemNatureza.nuTipoOcorrencia = tipoOcorrenciaSelecionado;
        this.parametroOrigemNaturezaService.listarOrigensAtivasPorTipoOcorrencia(tipoOcorrenciaSelecionado, (origens: Origem[]) =>
        this.origens = origens);
    }

    public recuperarNaturezaPorTipoOrigemTipoOcorrencia(): void {
        this.limparCampos();
        const nuTipoOrigem = this.formulario.value.nuTipoOrigem;
        const nuTipoOcorrencia = this.formulario.value.nuTipoOcorrencia;
        this.parametroOrigemNatureza.nuTipoOrigem = nuTipoOrigem;

        if ( nuTipoOrigem !== null) {
            this.ocorrenciaService.callListarNaturezasPorTipoOrigem(nuTipoOrigem, (naturezas: Natureza[]) => {
                this.naturezas = naturezas;
                this.adicionarAtributosArrayNatureza();
                this.recuperarParametroOrigem(nuTipoOcorrencia, nuTipoOrigem, this.naturezas);
            });
            this.showCamposDefinirParametroOrigemOcorrencia = false;
        } else {
            this.showCamposDefinirParametroOrigemOcorrencia = true;
        }
    }

    recuperarParametroOrigem(nuTipoOcorrencia: number, nuTipoOrigem: number, listaNatureza: Natureza[]): void {

       this.parametroOrigemNaturezaService.listarParametroOrigemPorTipoOcorrenciaEOrigem(nuTipoOcorrencia, nuTipoOrigem, (lista: ParametroOrigemNatureza[]) => {
            this.itensParametroOrigemNatureza = lista;

            for (let i = 0; i < this.itensParametroOrigemNatureza.length; i++) {
              const campoUnidade = this.getIdUnidade(i);

              const nuNatural = this.itensParametroOrigemNatureza[i].nuNaturalTransferencia;
              const nuUnidade = this.itensParametroOrigemNatureza[i].nuUnidadeTransferencia;
              let unidadeId: UnidadeSemOcorrenciaId;
              unidadeId = new UnidadeSemOcorrenciaId(nuNatural ,  nuUnidade);

            this.unidadeSemOcorrenciaService.consultarUnidade(unidadeId).subscribe(
              () => {
                console.log('aqui: ', unidadeId);
              },
              error => {
                this.messageService.addMsgDanger('Ocorreu um erro ao excluir Unidade sem Ocorrencia.');
              });

              /*this.itensParametroOrigemNatureza[i].nuNaturalTransferencia = this.formulario.value[campoUnidade].nuNatural;
              this.itensParametroOrigemNatureza[i].nuUnidadeTransferencia = this.formulario.value[campoUnidade].nuUnidade;*/
            }

            if (this.itensParametroOrigemNatureza.length > 0) {
                this.itensParametroOrigemNatureza.forEach(item => {
                    this.parametroOrigemNatureza.id = new ParametroOrigemNaturezaId(item.id.nuNatureza, item.id.nuTipoOcorrencia, item.id.nuTipoOrigem);
                    this.parametroOrigemNatureza.icResposta = item.icResposta;
                    this.parametroOrigemNatureza.icTransferencia = item.icTransferencia;
                    this.carregarValoresEdicao(item, listaNatureza);
                });
            }
        });

    }

    carregarValoresEdicao (item: ParametroOrigemNatureza, listaNatureza: Natureza[]): void {
        listaNatureza.forEach(natureza => {
            if (item.id.nuNatureza === natureza.id) {
                natureza['habilitarCampos'] = false;
                natureza['checkNatureza'] = true;
                this.habilitarBtnPesquisarUnidade(item.icTransferencia, natureza);
                this.naturezasSelecionadas.push(natureza);
            } else {
                natureza['habilitarCampos'] = true;
                natureza['checkNatureza'] = false;
            }
        });
    }

    habilitarBtnPesquisarUnidade(item: string, itemNaturezaSelecionado: Natureza) {
      if (item === 'E') {
        itemNaturezaSelecionado['habilitarBtnPesquisarUnidade'] = false;
      } else {
        itemNaturezaSelecionado['habilitarBtnPesquisarUnidade'] = true;
      }
    }

    isSelectPermitirUnidadeRespondam(itemSelecionado: SimNao, linha: number): Boolean {
      for (let index = 0; index < this.itensParametroOrigemNatureza.length; index++) {
        const itemParametro = this.itensParametroOrigemNatureza[index];
        const natureza = this.naturezas[linha];
        if (natureza.id === itemParametro.id.nuNatureza
         && itemParametro.icResposta === itemSelecionado.value) {
          return true;
        }
      }
      return false;
    }

    isSelectPermitirTransferenciaTodasUnidades(itemSelecionado: PermissaoTransferenciaTodasUnidades, linha: number): Boolean {
      for (let index = 0; index < this.itensParametroOrigemNatureza.length; index++) {
        const itemParametro = this.itensParametroOrigemNatureza[index];
        const natureza = this.naturezas[linha];
        if (natureza.id === itemParametro.id.nuNatureza
         && itemParametro.icTransferencia === itemSelecionado.value) {
          return true;
        }
      }
      return false;
    }

    adicionarAtributosArrayNatureza() {
        this.naturezas.forEach(item => {
            item['habilitarCampos'] = true;
            item['habilitarBtnPesquisarUnidade'] = true;
        });
    }

    carregarSimNao(): void {
        this.comumService.listarSimNao().subscribe(
          (simNao: SimNao[]) => {
            this.simNao = simNao;
          },
          error => {
            this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
            console.log('Erro ao consultar sim e não:', error);
          }
        );
    }

    carregarPermissaoTransferenciaTodasUnidades(): void {
        this.comumService.listarPermissaoTransferenciaTodasUnidades().subscribe(
            (permissaoTransferenciaTodasUnidades: PermissaoTransferenciaTodasUnidades[]) => {
              this.permissaoTransferenciaTodasUnidades = permissaoTransferenciaTodasUnidades;
            },
            error => {
              this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
              console.log('Erro ao consultar Permissao Transferencia Todas Unidades:', error);
            }
        );
    }

    verificarNaturezaChecada(isChecked: boolean, itemNatureza: Natureza, linha: number): void {
        if (isChecked) {
            this.naturezasSelecionadas.push(itemNatureza);
            this.naturezasSelecionadas.forEach(item => {
                item['habilitarCampos'] = false;
            });
        } else if (!isChecked && this.naturezasSelecionadas.length > 0) {
            const index = this.naturezasSelecionadas.indexOf(itemNatureza);
            this.naturezasSelecionadas.splice(index, 1);

            const campoPermitirUnidadeRespondam = this.getIdPermitirUnidadeRespondam(linha);
            const campoPermitirTransferenciaTodasUnidades = this.getIdPermitirTransferenciaTodasUnidades(linha);
            $('#' + campoPermitirTransferenciaTodasUnidades).val('Selecione');
            $('#' + campoPermitirUnidadeRespondam).val('Selecione');

            itemNatureza['habilitarCampos'] = true;
            this.naturezasSelecionadas.forEach(item => {
                item['habilitarCampos'] = false;
            });
        }
    }

    resetarCombosParametroOrigemOcorrencia() {
        this.formulario.controls['icResposta'].setValue(null, {onlySelf: true});
        this.formulario.controls['icTransferencia'].setValue(null, {onlySelf: true});
    }

    salvar() {

        const tipoOcorrencia = this.formulario.value.nuTipoOcorrencia;
        const tipoOrigem = this.formulario.value.nuTipoOrigem;

        if (tipoOcorrencia === null || tipoOrigem === null) {
            this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
            return;
        }

        //Falta a verificação de quando seleciona 'Unidade Específica' e não preenche a unidade e clica para salvar

        this.verificarSeExisteNaturezaChecada();

        for (let i = 0; i < this.itensParametroOrigemNatureza.length; i++) {
          const campoUnidade = this.getIdUnidade(i);
          this.itensParametroOrigemNatureza[i].nuNaturalTransferencia = this.formulario.value[campoUnidade].nuNatural;
          this.itensParametroOrigemNatureza[i].nuUnidadeTransferencia = this.formulario.value[campoUnidade].nuUnidade;
        }

       // if (this.isNew()) {
            this.itensParametroOrigemNatureza.forEach(itemSalvar => {

            this.parametroOrigemNaturezaService.post(itemSalvar).subscribe(
                    (parametroOrigemNatureza: ParametroOrigemNatureza) => {
                      this.router.navigate(['.'], { relativeTo: this.route.parent });
                      this.messageService.addMsgSuccess('Parâmetro Origem inserido com sucesso.');
                    },
                    error => {
                      this.messageService.addMsgDanger('Ocorreu um erro ao incluir o parâmetro origem.');
                      console.log('Erro ao incluir parâmetro origem:', error);
                    }
                );
            });
        //} 
            //else {
            /*this.parametroOrigemNaturezaService.put(this.parametroOrigemNatureza).subscribe(
                (parametroOrigemNatureza: ParametroOrigemNatureza) => {
                this.router.navigate(['.'], { relativeTo: this.route.parent });
                this.messageService.addMsgSuccess('Parâmetro Origem alterado com sucesso.');
              },
              error => {
                this.messageService.addMsgDanger('Ocorreu um erro ao alterar o parâmetro origem.');
                console.log('Erro ao alterar o parâmetro origem, ocasionado por:', error);
              }
            );*/
          //}

    }

    adicionarPermitirUnidadeRespondam(natureza: Natureza, linha: number): void {
      for (let index = 0; index < this.itensParametroOrigemNatureza.length; index++) {
        const itemParametro = this.itensParametroOrigemNatureza[index];
        if (itemParametro.id.nuNatureza === natureza.id) {
          const icResposta = this.getValueById(this.getIdPermitirUnidadeRespondam(linha));

          this.setarSimNaoCampoIcResposta(this.parametroOrigemNatureza.icResposta);

          this.itensParametroOrigemNatureza[index].icResposta = this.parametroOrigemNatureza.icResposta;
          return;
        }
      }
      this.prepararObjetoParaSalvar(natureza.id, linha);
    }

    setarSimNaoCampoIcResposta(valorCampo: String): void {
      if (valorCampo === 'Sim') {
        this.parametroOrigemNatureza.icResposta = 'S';
      } else {
        this.parametroOrigemNatureza.icResposta = 'N';
      }
    }

    prepararObjetoParaSalvar(idNatureza: number, linha: number) {
        this.parametroOrigemNatureza = new ParametroOrigemNatureza();
        const idParametroOrigemNatureza = new ParametroOrigemNaturezaId(idNatureza, this.formulario.value.nuTipoOcorrencia, this.formulario.value.nuTipoOrigem);
        const icResposta = this.getValueById(this.getIdPermitirUnidadeRespondam(linha));

        this.setarSimNaoCampoIcResposta(icResposta);

        this.parametroOrigemNatureza.id = idParametroOrigemNatureza;
        this.itensParametroOrigemNatureza.push(this.parametroOrigemNatureza);
    }

    verificarSeExisteNaturezaChecada() {
        if (this.naturezasSelecionadas.length === 0) {
            this.messageService.addMsgDanger('Informe algum vínculo ou Ocorrência ou Demanda Acompanhamento.');
            return;
        }
    }

    receberUnidade(unidade: Unidade) {

    }

    voltar(): void {
        this.router.navigate(['.'], { relativeTo: this.route.parent });
    }

    limparCampos(): void {
        this.resetarCombosParametroOrigemOcorrencia();
        this.limparCheckbox();
    }

    limparCheckbox() {
        this.formulario.controls['nuNatureza'].setValue(null, {onlySelf: true});

        if (this.naturezas !== undefined) {
            this.naturezas.forEach(item => {
                item['habilitarCampos'] = true;
            });
        }
    }

    verificarValorSelecionado(itemNaturezaSelecionado: Natureza, linha: number): void {

        const valorIcTransferencia = this.getValueById(this.getIdPermitirTransferenciaTodasUnidades(linha));

        if (valorIcTransferencia === 'Unidade Específica') {
            itemNaturezaSelecionado['habilitarBtnPesquisarUnidade'] = false;
        } else {
            itemNaturezaSelecionado['habilitarBtnPesquisarUnidade'] = true;
        }

        for (let index = 0; index < this.itensParametroOrigemNatureza.length; index++) {
          const itemParametro = this.itensParametroOrigemNatureza[index];
          if (itemParametro.id.nuNatureza === itemNaturezaSelecionado.id) {
            const campoIcTransferencia = this.getValueById(this.getIdPermitirTransferenciaTodasUnidades(linha));
            this.setarSimNaoCampoIcTransferencia(campoIcTransferencia);
            this.itensParametroOrigemNatureza[index].icTransferencia = this.parametroOrigemNatureza.icTransferencia;
            return;
          }
        }
        //this.prepararObjetoParaSalvar(natureza.id);
    }

    setarSimNaoCampoIcTransferencia(campo: String): void {
      if (campo === 'Sim') {
        this.parametroOrigemNatureza.icTransferencia = 'S';
      } else if (campo === 'Não') {
        this.parametroOrigemNatureza.icTransferencia = 'N';
      } else {
        this.parametroOrigemNatureza.icTransferencia = 'E';
      }
    }

    montarObj(): void {
        this.itensParametroOrigemNatureza = this.getClone(this.formulario.value);
    }

    isNew(): boolean {
        return Util.isEmpty(this.itensParametroOrigemNatureza);
    }

    irHome(): void {
        this.router.navigate(['/home']);
    }

    getIdNatureza(index: number): string {
      return this.idRow([this.nameNatureza, index + '']);
    }

    getIdPermitirUnidadeRespondam(index: number): string {
      return this.idRow([this.namePermitirUnidadeRespondam, index + '']);
    }

    getIdPermitirTransferenciaTodasUnidades(index: number): string {
      return this.idRow([this.namePermitirTransferenciaTodasUnidades, index + '']);
    }

    getIdUnidade(index: number): string {
      return this.idRow([this.nameUnidadeTransferencia, index + '']);
    }
}
