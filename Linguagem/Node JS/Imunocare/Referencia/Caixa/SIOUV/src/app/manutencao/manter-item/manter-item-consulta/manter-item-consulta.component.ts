import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { Item } from 'app/shared/models/item';
import { Situacao } from 'app/shared/models/situacao';
import { AssuntoService } from 'app/shared/services/manutencao/assunto.service';
import { Assunto } from 'app/shared/models/assunto';
import { ManterItemService } from 'app/shared/services/manutencao/manter-item.service';
import { Unidade } from 'app/shared/models/unidade';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';

@Component({
  selector: 'app-manter-item-consulta',
  templateUrl: './manter-item-consulta.component.html',
  styleUrls: ['./manter-item-consulta.component.css']
})
export class ManterItemConsultaComponent extends BaseComponent {
    pagina = 1;
    itens = 5;
    nome= '';

    nameItem = 'noItemAssunto';
    nameUnidadeResponsavel = 'unidade';
    nameSituacao = 'ativo';
    nameAssunto = 'assunto';

    situacoes: Situacao[] = [];
    formulario: FormGroup;
    item: Item;
    assunto: Assunto = null;
    assuntos: Assunto[] = null;
    unidade: Unidade = null;
    unidades: Unidade[] = null;
    unidadeSelecionada: Unidade;
    dataList: SiouvTable;
    itensObj: Item[] = null;


    constructor(
        private formBuilder: FormBuilder,
        protected messageService: MessageService,
        private comumService: ComumService,
        private assuntoService: AssuntoService,
        private manterItemService: ManterItemService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        super(messageService);
        this.item = new Item();
        this.createFormGroup();
        this.carregarSituacoes();
        this.carregarAssuntos();
        this.carregarItens();
    }

    carregarItens(): void {
      this.manterItemService.get().subscribe(
          (itens: Item[]) => {
              this.itensObj = itens;
              this.atualizarDataList();
           }
      );
    }

    atualizarDataList(): void {
      this.dataList = this.createSiouvTable(this.itensObj);
    }

    createSiouvTable(list: Item[]): SiouvTable {
      const idTable = 'consultaItem';
      const creator: SiouvTableCreator<Item> = new SiouvTableCreator();
      const headers = [
        creator.addHeader('item', 'Item', 25, 'text-center', {'vertical-align' : 'middle' }),
        creator.addHeader('unidadeResponsavel', 'Unidade', 25, 'text-center', {'vertical-align' : 'middle'}),
        creator.addHeader('situacao', 'Situação', 25, 'text-center', {'vertical-align' : 'middle'}),
        creator.addHeader('assunto', 'Assunto', 25, 'text-center', {'vertical-align' : 'middle'})
      ];

      const rows = [];

      for (let index = 0; index < list.length; index++) {
        const object = list[index];
        const cols = [
          creator.addCol(idTable, index, 'nome', object.noItemAssunto, 'text-center', null, object),
          creator.addCol(idTable, index, 'unidadeResponsavel', object.unidade.nome, 'text-center',  null, object),
          creator.addCol(idTable, index, 'situacao', object.labelAtiva, 'text-center',  null, object),
          creator.addCol(idTable, index, 'assunto', object.assunto.noAssunto, 'text-center',  null, object)
        ];
        rows.push(creator.addRow(idTable, index, cols, null, null, list[index]));
      }
      return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
    }

    carregarAssuntos() {
        this.assuntoService.consultarAssuntoAtivo((assuntos: Assunto[]) => {
            this.assuntos = assuntos;
        });
    }

    carregarSituacoes(): void {
        this.comumService.listarSituacoes().subscribe(
            (situacoes: Situacao[]) => {
                this.situacoes = situacoes;
                this.item.ativo = this.situacoes[0].value;
            },
            error => {
                this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
                console.log('Erro ao consultar situações:', error);
        });
    }

    createFormGroup(): void {
        this.formulario = this.formBuilder.group({
          noItemAssunto: [this.item.noItemAssunto, [ Validators.required ]],
          unidade: [this.item.unidade],
          ativo: [this.item.ativo],
          assunto: [this.item.assunto],
        });
    }

    incluirNovoItem() {
        this.router.navigate(['novo'], { relativeTo: this.route.parent });
    }

    alterar(item: Item) {
        this.router.navigate([item.id, 'editar'], { relativeTo: this.route.parent });
    }

    excluir(item: Item) {
        const indice: number = this.itensObj.findIndex(it => it.id === item.id);
        this.manterItemService.delete(item.id).subscribe(
            () => {
                this.messageService.addMsgSuccess('Item excluído com sucesso.');
                if (indice >= 0) {
                    this.itensObj.splice(indice, 1);
                }
                this.atualizarDataList();
            },
            error => {
                const message = typeof error.error !== 'string' ? 'Ocorreu um erro ao excluir o Item.' : error.error;
                this.messageService.addMsgDanger(message);
                console.log('Erro ao excluir natureza:', error);
            }
        );
    }

    limpar() {
      this.item = new Item();
      this.createFormGroup();
      this.carregarAssuntos();
      this.carregarItens();
    }

    pesquisar(next?: (itens: Unidade[]) => void): void {
      this.montarObjPesq();
      this.manterItemService.consultar(this.item, (itens: Item[]) => {
        this.itensObj = itens;
        this.atualizarDataList();
      });
    }

    montarObjPesq(): void {
      const item = this.getClone(this.formulario.value);
      item.field_codigo_item = undefined;
      item.field_descricao_item = undefined;
      this.item = this.getClone(item);
    }
}
