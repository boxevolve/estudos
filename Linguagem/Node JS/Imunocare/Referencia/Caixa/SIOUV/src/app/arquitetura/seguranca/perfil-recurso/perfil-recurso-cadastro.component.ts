import { Component, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { DatalistComponent } from 'app/shared/components/datalist/datalist.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { RecursoService } from 'app/arquitetura/shared/services/seguranca/recurso.service';
import { PerfilService } from 'app/arquitetura/shared/services/seguranca/perfil.service';
import { PerfilRecursoService } from 'app/arquitetura/shared/services/seguranca/perfil-recurso.service';
import { Recurso } from 'app/arquitetura/shared/models/seguranca/recurso';
import { Perfil } from 'app/arquitetura/shared/models/seguranca/perfil';
import { PerfilComRecursos } from 'app/arquitetura/shared/models/seguranca/perfil-com-recursos';

@Component({
  selector: 'app-perfil-recurso-cadastro',
  templateUrl: './perfil-recurso-cadastro.component.html',
  styleUrls: ['./perfil-recurso-cadastro.component.css']
})
export class PerfilRecursoCadastroComponent extends BaseComponent {
  @ViewChildren(DatalistComponent) datalists: QueryList<DatalistComponent>;

  formulario: FormGroup;
  perfis: Perfil[] = null;
  perfil: Perfil = null;
  recursos: Recurso[] = null;
  recursosSelecionados: Recurso[] = null;

  constructor(
    protected messageService: MessageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private recursoService: RecursoService,
    private perfilService: PerfilService,
    private perfilRecursoService: PerfilRecursoService,
  ) {
    super(messageService);

    this.perfis = [];
    this.perfil = new Perfil();

    this.formulario = this.formBuilder.group({
      perfil: [{ value: this.perfil }, [Validators.required]]
    });

    this.carregarPerfis();
  }

  carregarPerfis() {
    this.perfilService.get().subscribe(
      (perfis: Perfil[]) => {
        this.perfis = perfis;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao carregar os perfis.');
        console.log('Erro ao carregar perfis:', error);
      });
  }

  listarRecursos() {
    this.recursoService.get().subscribe(
      (recursos: Recurso[]) => {
        this.recursos = recursos;

        this.perfilRecursoService.consultarRecursosPorIdPerfil(this.perfil.id).subscribe(
          (recursosSelecionados: Recurso[]) => {
            this.recursosSelecionados = recursosSelecionados;
          },
          error => {
            this.messageService.addMsgDanger('Ocorreu um erro ao carregar os recursos do perfil.');
            console.log('Erro ao carregar recursos do perfil:', error);
          });
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao carregar os recursos.');
        console.log('Erro ao carregar recursos:', error);
      });
  }

  gravar() {
    if (!this.formulario.valid) {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
    }

    const perfilComRecursos: PerfilComRecursos = new PerfilComRecursos();
    perfilComRecursos.perfil = this.perfil;
    perfilComRecursos.recursos = this.recursosSelecionados;
    this.perfilRecursoService.atualizarRecursosDoPerfil(perfilComRecursos).subscribe(
      (recursos: Recurso[]) => {
        this.recursosSelecionados = recursos;
        this.messageService.addMsgSuccess('Recursos do perfil ' + this.perfil.nome + ' gravados com sucesso.');
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao gravar os recursos do perfil.');
        console.log('Erro ao gravar os recursos do perfil:', error);
      });
  }
}
