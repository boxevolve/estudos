import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { UnidadeSemOcorrencia } from 'app/shared/models/unidade-sem-ocorrencia';
import { UnidadeSemOcorrenciaService } from 'app/shared/services/manutencao/unidade-sem-ocorrencia.service';
import { Util } from 'app/arquitetura/shared/util/util';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';
import { Unidade } from 'app/shared/models/unidade';
import { UnidadeSemOcorrenciaId } from 'app/shared/models/unidade-sem-ocorrencia-id';

@Component({
  selector: 'app-consulta-unidade-sem-ocorrencia',
  templateUrl: './consulta-unidade-sem-ocorrencia.component.html',
  styleUrls: ['./consulta-unidade-sem-ocorrencia.component.css']
})
export class ConsultaUnidadeSemOcorrenciaComponent extends BaseComponent implements AfterViewInit {

  unidades: UnidadeSemOcorrencia[] = [];
  dataList: SiouvTable;
  descricao: string;
  idModalUnidade: string;
  idUnidadeSemOcorrecia = 'semOcorrecnia';

  constructor(
    private cdr: ChangeDetectorRef,
    private unidadeService: UnidadeSemOcorrenciaService,
    protected messageService: MessageService,
  ) {
    super(messageService);
    this.carregarUnidades();
   }

   ngAfterViewInit() {
    this.cdr.detectChanges();
  }

   carregarUnidades(): void {
     this.unidadeService.get().subscribe(
       (unidades: UnidadeSemOcorrencia[]) => {
        this.unidades = unidades;
        this.atualizarDataList();
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as unidades.');
      }
    );
  }

  atualizarDataList(): void {
    this.dataList = this.createSiouvTable(this.unidades);
  }

  excluir(unidadeSemOcorrencia: UnidadeSemOcorrencia): void {
    this.unidadeService.deleteUnidadeSemOcorrencia(unidadeSemOcorrencia.id).subscribe(
      () => {
        this.messageService.addMsgSuccess('Operação efetuada com sucesso.');
        this.carregarUnidades();
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao excluir Unidade sem Ocorrencia.');
      });
  }

  createSiouvTable(list: UnidadeSemOcorrencia[]): SiouvTable {
    const idTable = 'consultaUnidade';
    const creator: SiouvTableCreator<UnidadeSemOcorrencia> = new SiouvTableCreator();

    const headers = [
      creator.addHeader('unidade', 'Unidades Sem Ocorrência', 25 , 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('acao', 'Ação', 25 , 'text-center', {'vertical-align': 'middle'})
    ];

    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const unidade = this.getUnidadeDescricao(object.unidade, 2);
      const cols = [
        creator.addCol(idTable, index, 'unidade',  unidade , 'text-center', null, object)
      ];
      rows.push(creator.addRowDelete(idTable, index, cols, null, null, list[index]));
    }
    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

  disableModal(): boolean {
    return !Util.isEmpty(this.descricao);
  }

  receberUnidade(unidade: Unidade): void {
    let unidadeId: UnidadeSemOcorrenciaId;
    unidadeId = new UnidadeSemOcorrenciaId(unidade.nuNatural , unidade.nuUnidade);

    let unidadeS: UnidadeSemOcorrencia;
    unidadeS = new UnidadeSemOcorrencia();
    unidadeS.unidade = unidade;
    unidadeS.dhRegistro = new Date();
    unidadeS.id = unidadeId;

    this.unidadeService.inserirUnidade(unidadeS).subscribe(
      () => {
        this.messageService.addMsgSuccess('Operação efetuada com sucesso.');
        this.carregarUnidades();
      }, error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao cadastrar Unidade sem Ocorrencia.');
      });
  }

  receberIdModal(idModal: string): void {
    this.idModalUnidade = idModal;
  }
}
