import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Unidade } from 'app/shared/models/unidade';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { MessageService } from 'app/shared/components/messages/message.service';
import { OcorrenciaService } from 'app/shared/services/ocorrencia/ocorrencia.service';
import { RedirecionamentoOcorrencia } from 'app/shared/models/redirecionamento-ocorrencia';
import { BaseComponent } from 'app/shared/components/base.component';

@Component({
  selector: 'app-redirecionamento-ocorrencia-cadastro',
  templateUrl: './redirecionamento-ocorrencia-cadastro.component.html',
  styleUrls: ['./redirecionamento-ocorrencia-cadastro.component.css']
})
export class RedirecionamentoOcorrenciaCadastroComponent extends BaseComponent implements OnInit {

  nameUnidadeOrigem = 'unidadeOrigem';
  nameUnidadeDestino = 'unidadeDestino';

  formulario: FormGroup;
  unidadeOrigem: any;
  unidadeDestino: any;
  dataList: SiouvTable;
  unidades: any[] = [];

  constructor(
    private fb: FormBuilder,
    private ocorrenciaService: OcorrenciaService,
    protected messageService: MessageService
   ) {
    super(messageService);
  }

  ngOnInit() {
    this.formulario = this.fb.group({});
  }

  limparFormulario(): void {
    this.formulario.reset();
    this.unidadeOrigem = null;
    this.unidadeDestino = null;
  }

  excluir(unidade: Unidade): void {
    const index: number = this.unidades.findIndex(item => item.id === unidade.nuUnidade);
    if (index >= 0) {
      this.unidades.splice(index, 1);
      this.messageService.addMsgSuccess('Unidades excluídas com sucesso.');
    } else {
      this.messageService.addMsgDanger('Erro ao excluir as unidades.');
    }
    this.atualizarDataList();
  }

  adicionarLinha(): void {
    if (!this.validarFormulario(this.formulario)) {
      this.messageService.addMsgDanger('Os campos Unidade Origem e Unidade Destino são obrigátorios.');
    } else {
      this.unidadeOrigem = this.getValueForm(this.formulario, this.nameUnidadeOrigem);
      this.unidadeDestino = this.getValueForm(this.formulario, this.nameUnidadeDestino);

      if (this.equals(this.unidadeOrigem, this.unidadeDestino)) {
        this.messageService.addMsgDanger('Unidade Destino não pode ser igual a Unidade Origem.');
      } else {
        if (this.unidades.length > 0) {
          const index = this.unidades.findIndex(x => this.equals(x.origem, this.unidadeOrigem) && this.equals(x.destino, this.unidadeDestino));
          if (index !== -1) {
            this.messageService.addMsgDanger('Registro já cadastrado');
          } else {
            this.unidades.push({'origem': this.unidadeOrigem, 'destino' : this.unidadeDestino});
            this.atualizarDataList();
          }
        } else {
          this.unidades.push({'origem': this.unidadeOrigem, 'destino' : this.unidadeDestino});
          this.atualizarDataList();
        }
      }
    }
  }

  atualizarDataList(): void {
    this.dataList = this.createSiouvTable(this.unidades);
  }

  createSiouvTable(list: any[]): SiouvTable {
    const idTable = 'consultaNatureza';
    const creator: SiouvTableCreator<Unidade> = new SiouvTableCreator();

    const headers = [
      creator.addHeader('unidadeOrigem', 'Unidade Origem', 40, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('unidadeDestino', 'Unidade Destino', 40, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('acao', 'Ação', 20, 'text-center', {'vertical-align': 'middle'})
    ];

    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'nome', this.getUnidadeDescricao(object.origem, 1), 'text-center', null, object),
        creator.addCol(idTable, index, 'tiposOcorrenciaNatureza',  this.getUnidadeDescricao(object.destino, 1), 'text-center', null, object),
      ];
      rows.push(creator.addRowDelete(idTable, index, cols, null, null, list[index]));
    }

    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

  incluir(): void {
    const object: RedirecionamentoOcorrencia = new RedirecionamentoOcorrencia();
    object.unidades = this.unidades;
    return this.ocorrenciaService.redirecionarOcorrencia(object, result => {
      result.forEach(element => {
        this.unidades = [];
        this.limparFormulario();
        this.messageService.addMsgInf(element);
      });
    });
  }
}
