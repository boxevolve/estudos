import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Situacao } from 'app/shared/models/situacao';
import { UnidadeSndc } from 'app/shared/models/sndc';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { MessageService } from 'app/shared/components/messages/message.service';
import { SndcService } from 'app/shared/services/manutencao/sndc.service';
import { Uf } from 'app/shared/models/uf';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { BaseComponent } from 'app/shared/components/base.component';
import { Router, ActivatedRoute } from '@angular/router';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';
import { UfService } from 'app/shared/services/comum/uf.service';
import { Util } from 'app/arquitetura/shared/util/util';

@Component({
  selector: 'app-sndc-consulta',
  templateUrl: './sndc-consulta.component.html',
  styleUrls: ['./sndc-consulta.component.css']
})
export class SndcConsultaComponent extends BaseComponent {

  formulario: FormGroup;
  situacoes: Situacao[];
  ufs: Uf[];
  sndc: UnidadeSndc;
  listSndc: UnidadeSndc[];
  dataList: SiouvTable;

    /**
	* Nomes dos formControlName dos campos
  */
 nameNoUnidadeSndc: 'noUnidadeSndc';
 namesgUf: 'sgUf';
 nameicAtivo: 'icAtivo';

  constructor(
    private formBuilder: FormBuilder,
    private comumService: ComumService,
    protected messageService: MessageService,
    private sndcService: SndcService,
    private router: Router,
    private route: ActivatedRoute,
    private ufService: UfService
  ) {
    super(messageService);
    this.sndc = new UnidadeSndc();
    this.listSndc = new Array;
    this.buscarSituacoes();
    this.carregarUfs();
    this.criarFormulario();
    this.setValoresPadrao();
  }

  criarFormulario() {
    this.formulario = this.formBuilder.group({
      noUnidadeSndc: [this.sndc.noUnidadeSndc, []],
      sgUf: [this.sndc.sgUf, []],
      icAtivo: [this.sndc.icAtivo]
    });
  }
  setValoresPadrao() {
    this.formulario.get('icAtivo').patchValue('S');
  }

  buscarSituacoes() {
    this.comumService.listarSituacoes().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger(
          'Ocorreu um erro ao pesquisar situações.'
        );
        console.log('Erro ao consultar situações:', error);
      }
    );
  }
  carregarUfs() {
    this.ufService.todos((ufs: Uf[]) => {
      this.ufs = ufs;
    });
  }

  consultar() {
    if (Util.isEmpty(this.formulario.get('noUnidadeSndc').value) &&
      Util.isEmpty(this.formulario.get('sgUf').value) &&
      Util.isEmpty(this.formulario.get('icAtivo').value)) {
      this.messageService.
        addMsgDanger('Ao menos um desses parâmetros deve ser informado: Unidade - Situação - UF.');
      return;
    }

    this.sndc = this.getClone(this.formulario.value);
    this.sndcService.consultarSndc(this.sndc).subscribe((result) => {
      this.listSndc = result;
      if (this.listSndc.length > 0) {
        this.atualizarListSndc();
      } else {
        this.messageService.
          addMsgDanger('Nenhuma Unidade Encontrada.');
        return;
      }
    });
  }

  incluir() {
    this.router.navigate(['novo'], { relativeTo: this.route.parent });
  }

  limpar() {
    this.listSndc = new Array;
    this.formulario.reset();
    this.setValoresPadrao();
  }

  atualizarListSndc() {
    this.dataList = this.createSiouvTable(this.listSndc);
  }

  createSiouvTable(list: UnidadeSndc[]): SiouvTable {
    const idTable = 'consultaUnidadeSndc';
    const creator: SiouvTableCreator<UnidadeSndc> = new SiouvTableCreator();
    const headers = [
      creator.addHeader('unidade', 'Unidade', 25, 'text-center', { 'vertical-align': 'middle' }),
      creator.addHeader('uf', 'UF', 25, 'text-center', { 'vertical-align': 'middle' }),
      creator.addHeader('ativo', 'Situação', 25, 'text-center', { 'vertical-align': 'middle' }),
      creator.addHeader('acao', 'Ação', 25, 'text-center', { 'vertical-align': 'middle' }),
    ];

    const rows = [];

    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'unidade', object.noUnidadeSndc, 'text-center', null, object),
        creator.addCol(idTable, index, 'uf', object.sgUf, 'text-center', null, object),
        creator.addCol(idTable, index, 'ativo', object.icAtivo, 'text-center', null, object),
      ];
      rows.push(creator.addRow(idTable, index, cols, null, null, list[index]));
    }


    // Table
    // Id - Cabeçalhos - Linhas - Class - Style - Lista
    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

  alterar(a: UnidadeSndc) {
    this.router.navigate(['novo/' + a.id], { relativeTo: this.route.parent });
  }

  excluir(a: UnidadeSndc) {
    this.sndcService.delete(a.id).subscribe(() => {
      this.messageService.addMsgSuccess('Operação efetuada com sucesso.');
      this.consultar();
    });
  }

}
