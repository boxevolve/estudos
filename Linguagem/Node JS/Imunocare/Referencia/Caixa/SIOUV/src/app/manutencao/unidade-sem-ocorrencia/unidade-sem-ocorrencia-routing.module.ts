import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { ConsultaUnidadeSemOcorrenciaComponent } from './consulta-unidade-sem-ocorrencia/consulta-unidade-sem-ocorrencia.component';
const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard, DadosUsuarioGuard],
    canActivateChild: [AuthGuard, DadosUsuarioGuard],
    children: [
        {
            path: '',
            component: ConsultaUnidadeSemOcorrenciaComponent
        }
]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnidadeSemOcorrenciaRoutingModule { }
