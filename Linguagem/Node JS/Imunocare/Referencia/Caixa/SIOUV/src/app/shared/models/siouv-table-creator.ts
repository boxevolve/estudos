import { SiouvCol } from './siouv-col';
import { SiouvRow } from './siouv-row';
import { SiouvTable } from './siouv-table';

export class SiouvTableCreator<T> {

  // INICIO - Funções para geração da tabela
  addHeader(id: string, text: string, width: number, clazz: any, style: any): SiouvCol {
    return this.newSiouvHeader(id, text, width, clazz, style);
  }

  addCol(idTable: string, index: number, idCol: string, text: any, clazz: any, style: any, object: any) {
    text = text + '';
    return this.newDefaultSiouvCol(this.getIdColTable(idTable, index, idCol), text, clazz, style, object);
  }

  addRow(idTable: string, index: number, cols: SiouvCol[], clazz: any, style: any, object: any) {
    return this.newDefaultSiouvRow(this.getIdRowTable(idTable, index), cols, clazz, style, object);
  }

  addRowNone(idTable: string, index: number, cols: SiouvCol[], clazz: any, style: any, object: any) {
    return this.newSiouvRow(this.getIdRowTable(idTable, index), cols, clazz, style, false, false, false, false, null, null, object);
  }

  addRowAll(idTable: string, index: number, cols: SiouvCol[], clazz: any, style: any, object: any) {
    return this.newDefaultAllSiouvRow(this.getIdRowTable(idTable, index), cols, clazz, style, object);
  }

  addRowDetail(idTable: string, index: number, cols: SiouvCol[], clazz: any, style: any, object: any) {
    return this.newDefaultDetailSiouvRow(this.getIdRowTable(idTable, index), cols, clazz, style, null, object);
  }

  addRowDetailModal(idTable: string, index: number, cols: SiouvCol[], clazz: any, style: any, idModal: string, object: any) {
    return this.newDefaultDetailSiouvRow(this.getIdRowTable(idTable, index), cols, clazz, style, idModal, object);
  }

  addRowAllDetailModal(idTable: string, index: number, cols: SiouvCol[], clazz: any, style: any, idModal: string, object: any) {
    return this.newDefaultAllDetailSiouvRow(this.getIdRowTable(idTable, index), cols, clazz, style, idModal, object);
  }

  addRowDetailSelectModal(idTable: string, index: number, coluns: SiouvCol[], clazz: any, style: any, idModal: string, object: any) {
    return this.newDefaultDetailSelectSiouvRow(this.getIdRowTable(idTable, index), coluns, clazz, style, idModal, object);
  }

  addRowSelect(idTable: string, index: number, cols: SiouvCol[], clazz: any, style: any, idModal: string, object: any) {
    return this.newDefaultSelectSiouvRow(this.getIdRowTable(idTable, index), cols, clazz, style, idModal, object);
  }

  addRowDelete(idTable: string, index: number, cols: SiouvCol[], clazz: any, style: any, object: any) {
    return this.newDefaultDeleteSiouvRow(this.getIdRowTable(idTable, index), cols, clazz, style, object);
  }

  newSiouvHeader(id: string, text: string, width: number, clazz: any, style: any): SiouvCol {
    return this.newSiouvCol(id, text, width, clazz, style, null);
  }

  newDefaultSiouvCol(id: string, text: string, clazz: any, style: any, object: any): SiouvCol {
    return this.newSiouvCol(id, text, null, clazz, style, object);
  }

  newSiouvCol(id: string, text: string, width: number, clazz: any, style: any, object: any): SiouvCol {
    width = !width ? 100 : width;
    clazz = clazz === null ? '' : clazz;
    style = style === null ? '' : style;
    return new SiouvCol(id, text, width, clazz, style, object);
  }

  newDefaultSiouvRow(id: string, coluns: SiouvCol[], clazz: any, style: any, object: any) {
    return this.newSiouvRow(id, coluns, clazz, style, null, null, false, false, null, null, object);
  }

  newDefaultDetailSiouvRow(id: string, coluns: SiouvCol[], clazz: any, style: any, idModal: string, object: any) {
    return this.newSiouvRow(id, coluns, clazz, style, false, false, null, false, null, idModal, object);
  }

  newDefaultAllDetailSiouvRow(id: string, coluns: SiouvCol[], clazz: any, style: any, idModal: string, object: any) {
    return this.newSiouvRow(id, coluns, clazz, style, null, null, null, false, null, idModal, object);
  }

  newDefaultDetailSelectSiouvRow(id: string, coluns: SiouvCol[], clazz: any, style: any, idModal: string, object: any) {
    return this.newSiouvRow(id, coluns, clazz, style, false, false, null, null, null, idModal, object);
  }

  newDefaultSelectSiouvRow(id: string, coluns: SiouvCol[], clazz: any, style: any, idModal: string, object: any) {
    return this.newSiouvRow(id, coluns, clazz, style, false, false, false, null, null, idModal, object);
  }

  newDefaultDeleteSiouvRow(id: string, coluns: SiouvCol[], clazz: any, style: any, object: any) {
    return this.newSiouvRow(id, coluns, clazz, style, false, null, false, false, null, null, object);
  }

  newDefaultAllSiouvRow(id: string, coluns: SiouvCol[], clazz: any, style: any, object: any) {
    return this.newSiouvRow(id, coluns, clazz, style, null, null, null, false, null, null, object);
  }

  newSiouvRow(id: string, coluns: SiouvCol[], clazz: any, style: any, showEdit: boolean,
    showDelete: boolean, showDetail: boolean, showSelect: boolean, showHover: boolean, idModal: string,
    object: any) {
    showEdit = showEdit === null ? true : showEdit;
    showDelete = showDelete === null ? true : showDelete;
    showDetail = showDetail === null ? true : showDetail;
    showSelect = showSelect === null ? true : showSelect;
    showHover = showHover === null ? true : showHover;
    clazz = clazz === null ? '' : clazz;
    style = style === null ? '' : style;
    return new SiouvRow(id, coluns, clazz, style, showEdit, showDelete, showDetail, showSelect, showHover,
      idModal, object);
  }

  newDefaultSiouvTable(id: string, headers: SiouvCol[], rows: SiouvRow[], clazz: any, style: any,
    listObject: any[]): SiouvTable {
    return this.newSiouvTable(id, null, headers, rows, clazz, style, null, null, null, null, listObject);
  }

  newSiouvTableNoPagination(id: string, headers: SiouvCol[], rows: SiouvRow[], clazz: any, style: any,
    listObject: any[]): SiouvTable {
    return this.newSiouvTable(id, null, headers, rows, clazz, style, false, null, null, null, listObject);
  }

  newSiouvTableTitle(id: string, title: string, headers: SiouvCol[], rows: SiouvRow[], clazz: any, style: any,
    listObject: any[]): SiouvTable {
    return this.newSiouvTable(id, title, headers, rows, clazz, style, null, null, null, null, listObject);
  }

  newSiouvTable(id: string, title: string, headers: SiouvCol[], rows: SiouvRow[], clazz: any, style: any, showPagination: boolean,
    numberPage: number, numberItems: number, total: number, listObject: any[]): SiouvTable {
    numberPage = !numberPage ? 1 : numberPage;
    numberItems = !numberItems ? 5 : numberItems;
    total = !total ? listObject.length : total;
    clazz = clazz === null ? '' : clazz;
    style = style === null ? '' : style;
    showPagination = showPagination === null ? true : showPagination;
    return new SiouvTable(id, title, headers, rows, clazz, style, showPagination, numberPage, numberItems, total, listObject);
  }

  getIdRowTable(idTable: string, index: number): string {
    return idTable + '_row_' + index;
  }

  getIdColTable(idTable: string, index: number, idCol: string): string {
    return this.getIdRowTable(idTable, index) + '_col_' + idCol;
  }
  // FIM - Funções para geração da tabela

}
