import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Perfil } from 'app/arquitetura/shared/models/seguranca/perfil';

@Injectable()
export class PerfilService extends CrudHttpClientService<Perfil> {
	constructor(protected http: HttpClient) {
		super('seguranca/perfil', http);
	}

	/**
	 * Recupera todos os perfis relacionados com o nome parcial informado
	 * @param nome
	 */
	public consultarPorNomeParcial(nome: string): Observable<Perfil[]> {
		let httpParams: HttpParams = new HttpParams().set('nome', nome);

		return this.http.get<Perfil[]>(this.url + '/consultar-por-nome-parcial',
			this.options({params: httpParams}));
	}
}
