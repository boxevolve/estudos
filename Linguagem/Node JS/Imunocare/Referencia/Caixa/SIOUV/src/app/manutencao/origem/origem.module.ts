import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TextMaskModule } from 'angular2-text-mask';
import { NgxPaginationModule } from 'ngx-pagination';

// Aplicação
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { ManterOrigemRoutingModule } from './origem.routing.module';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { OrigemCadastroComponent } from './origem-cadastro/origem-cadastro.component';
import { OrigemConsultaComponent } from './origem-consulta/origem-consulta.component';
import { SiouvTableModule } from 'app/shared/components/table/table.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    ComponentModule.forRoot(),
    ManterOrigemRoutingModule,
    SiouvTableModule
  ],
  declarations: [OrigemCadastroComponent, OrigemConsultaComponent]
})
export class OrigemModule {}
