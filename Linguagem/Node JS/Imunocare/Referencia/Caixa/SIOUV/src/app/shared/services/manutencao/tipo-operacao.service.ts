import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Injectable } from '@angular/core';
import { TipoOperacao } from 'app/shared/models/tipo-operacao';
import { Origem } from 'app/shared/models/origem';

@Injectable()
export class TipoOperacaoService extends CrudHttpClientService<TipoOperacao> {

	constructor(protected http: HttpClient) {
		super('manutencao/tipo-operacao', http);
	}
    
    public callListar(next?: (value: any) => void) {
        return this.http.get<Origem[]>(this.url + '/', this.options())
        .subscribe(next, error => console.log('Não foi possível consultar origens.'));
    }
    
    public carregarPorTipoOcorrencia(tipoOcorrencia: number, next?: (value: any)=> void) {

        if ( tipoOcorrencia == 1 ) {
            return [this.newOrigem( 1, 'Origem 1' ), this.newOrigem( 2, 'Origem 2' )];
        } else {
            return [this.newOrigem( 3, 'Origem 3' ), this.newOrigem( 4, 'Origem 4' )];
        }
        //return this.consultarPorParametro('consulta/tipoocorrencia', 'nuTipoOcorrencia', tipoOcorrencia);
    }

    private newOrigem( id: number, nome: string ): Origem {
        var o = new Origem();
        o.id = id;
        o.nome = nome;
        return o;
    }
}
