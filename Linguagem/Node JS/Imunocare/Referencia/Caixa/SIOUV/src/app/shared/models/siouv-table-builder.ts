import { SiouvCol } from './siouv-col';
import { SiouvRow } from './siouv-row';
import { BaseComponent } from '../components/base.component';

export class SiouvTableBuilder<T> {

    public tableId: string;
    public colunas: SiouvCol[] = [];
    public acao: SiouvCol;
    public headers: SiouvCol[];
    public rows: SiouvRow[] = [];
    public clazz: string;
    public style: string;
    public numberPage: number;
    public numberItems: number;
    public total: number;
    public tableData: T[];

    constructor(tableId: string, tableData: T[]) {
        this.tableId = tableId;
        this.tableData = tableData;
        this.acao = new SiouvCol('acao', 'Ação', null, 'text-center', {'vertical-align': 'middle'}, null);
    }

    public addColuna(id: string, label: string, callback: ((object: T) => any)): void {
        this.colunas.push(new SiouvCol(id, label, null, 'text-center', {'vertical-align': 'middle'}, null, callback));
    }

    public render(): any {
        this.renderHeaders();
        this.renderRows();
    }
    public renderRows(): any {
        this.rows = [];
        for (let index = 0; index < this.tableData.length; index++) {
            const object = this.tableData[index];
            const cols = [];
            for (let cIndex = 0; cIndex < this.colunas.length; cIndex++) {
                console.log(this.colunas[cIndex].callback(object));
                cols.push(this.addCol(this.tableId, index, this.colunas[cIndex].id, this.colunas[cIndex].callback(object), 'text-center', null, object));
            }
            this.rows.push(this.addRow(this.tableId, index, cols, null, null, this.tableData[index]));
        }
    }

    private renderHeaders(): any {
        this.headers = [];
        for (let i = 0; i < this.colunas.length; i++) {
            this.headers.push(this.addHeader(this.colunas[i].id, this.colunas[i].text, this.colunas[i].clazz, this.colunas[i].style));
        }
        this.headers.push(this.acao);
    }

    private addHeader(id: string, text: string, clazz: string, style: string): SiouvCol {
        return this.newDefaultSiouvHeader(id, text, clazz, style);
    }

    private newDefaultSiouvHeader(id: string, text: string, clazz: string, style: string): SiouvCol {
        return this.newSiouvCol(id, text, 16.5, clazz, style, null);
    }
    private addCol(idTable: string, index: number, idCol: string, text: string, clazz: string, style: string, object: any) {
        return this.newDefaultSiouvCol(this.getIdColTable(idTable, index, idCol), text, clazz, style, object);
    }

    private getIdColTable(idTable: string, index: number, idCol: string): string {
        return this.getIdRowTable(idTable, index) + '_col_' + idCol;
    }

    private newDefaultSiouvCol(id: string, text: string, clazz: string, style: string, object: any): SiouvCol {
        return this.newSiouvCol(id, text, null, clazz, style, object);
    }

    private newSiouvCol(id: string, text: string, width: number, clazz: string, style: string, object: any): SiouvCol {
        width = !width ? 100 : width;
        clazz = clazz === null ? '' : clazz;
        style = style === null ? '' : style;
        return new SiouvCol(id, text, width, clazz, style, object);
    }

    private addRow(idTable: string, index: number, cols: SiouvCol[], clazz: string, style: string, object: any) {
        return this.newDefaultSiouvRow(this.getIdRowTable(idTable, index), cols, clazz, style, object);
    }

    private getIdRowTable(idTable: string, index: number): string {
        return idTable + '_row_' + index;
    }

    private newDefaultSiouvRow(id: string, coluns: SiouvCol[], clazz: string, style: string, object: any) {
        return this.newSiouvRow(id, coluns, clazz, style, null, null, false, false, null, null, object);
    }
    private newSiouvRow(id: string, coluns: SiouvCol[], clazz: string, style: string, showEdit: boolean,
        showDelete: boolean, showDetail: boolean, showSelect: boolean, showHover: boolean, idModalDetail: string, object: any) {
        showEdit = showEdit === null ? true : showEdit;
        showDelete = showDelete === null ? true : showDelete;
        showDetail = showDetail === null ? true : showDetail;
        showSelect = showSelect === null ? true : showSelect;
        showHover = showHover === null ? true : showHover;
        clazz = clazz === null ? '' : clazz;
        style = style === null ? '' : style;
        return new SiouvRow(id, coluns, clazz, style, showEdit, showDelete, showDetail, showSelect, showHover, idModalDetail, object);
    }
}
