import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { CorreioEletronico } from 'app/shared/models/correio-eletronico';
import { Situacao } from 'app/shared/models/situacao';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { CorreioEletronicoService } from 'app/shared/services/manutencao/correio-eletronico.service';
import { TipoCorreioEletronico } from 'app/shared/models/tipo-correio-eletronico';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { Util } from 'app/arquitetura/shared/util/util';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';
import { TipoCorreioEletronicoService } from 'app/shared/services/manutencao/tipo-correio-eletronico.service';

@Component({
  selector: 'app-correio-eletronico-consulta',
  templateUrl: './correio-eletronico-consulta.component.html',
  styleUrls: ['./correio-eletronico-consulta.component.css']
})
export class CorreioEletronicoConsultaComponent extends BaseComponent {

  ativo = 'ativo';
  tipoCorreioEletronico = 'tipoCorreioEletronico';

  listaCorreioEletronico: CorreioEletronico[] = null;
  dataList: SiouvTable;

  formulario: FormGroup;
  correioEletronico: CorreioEletronico;

  situacoes: Situacao[] = [];
  tipos: TipoCorreioEletronico[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private comumService: ComumService,
    private correioEletronicoService: CorreioEletronicoService,
    private tipoCorreioEletronicoService: TipoCorreioEletronicoService,
    protected messageService: MessageService
  ) {
    super(messageService);
    this.correioEletronico = new CorreioEletronico();
    this.createFormGroup();
    this.carregarSituacoes();
    this.carregarTipos();
  }

  incluir() {
    this.router.navigate(['novo'], { relativeTo: this.route.parent });
  }

  limpar(): void {
    this.correioEletronico = new CorreioEletronico();
    this.tipos = [];
    this.listaCorreioEletronico = null;
    this.formulario.reset();
    this.carregarTipos();
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      tipoCorreioEletronico: [this.correioEletronico.tipoCorreioEletronico, [Validators.required]],
      ativo: [this.correioEletronico.ativo]
    });
  }

  carregarSituacoes(): void {
    this.comumService.listarSituacoes().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
        console.log('Erro ao consultar situações:', error);
      });
  }

  carregarTipos(): void {
    this.tipoCorreioEletronicoService.get().subscribe(
      (tipos: TipoCorreioEletronico[]) => {
        this.tipos = tipos.sort((a, b) => {
          if (a.nome > b.nome) {
            return 1;
          }
          if (a.nome < b.nome) {
            return -1;
          }
          return 0;
        });
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar os tipos.');
        console.log('Erro ao consultar os tipos:', error);
      });
  }

  carregarListaCorreioEletronico() {

    const nuTipoCorreioEletronico: string = this.formulario.controls['tipoCorreioEletronico'].value;
    const situacao: string = this.formulario.controls['ativo'].value;

    if (nuTipoCorreioEletronico == null || Util.isEmpty(nuTipoCorreioEletronico)) {
      this.messageService.addMsgDanger('Selecione o tipo de Correio Eletrônico.');
      return;
    }

    this.correioEletronicoService.consultarPorFiltros(nuTipoCorreioEletronico, situacao).subscribe(
      (listaCorreioEletronico: CorreioEletronico[]) => {
        this.listaCorreioEletronico = listaCorreioEletronico;
        this.atualizarDataList();
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os correios eletronicos.');
        console.log('Erro ao recuperar os correios eletronicos:', error);
      });
  }

  getNome(nuTipoCorreioEletronico: number): string {
    let nome = nuTipoCorreioEletronico.toString();
    this.tipos.forEach(x => {
      if (x.id === nuTipoCorreioEletronico) {
        nome = x.nome;
      }
    });
    return nome;
  }

  atualizarDataList() {
    this.dataList = this.createSiouvTable(this.listaCorreioEletronico);
  }

  createSiouvTable(list: CorreioEletronico[]): SiouvTable {
    const idTable = 'consultaCorreioEletronico';
    const creator: SiouvTableCreator<CorreioEletronico> = new SiouvTableCreator();
    // Cabeçalhos
    // Id - Texto - Class - Style
    const headers = [
      creator.addHeader('tipo', 'Tipo', 25, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('assunto', 'Assunto', 25, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('situacao', 'Situação', 25, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('acao', 'Ação', 25, 'text-center', {'vertical-align': 'middle'})
    ];

    // Linhas
    // Id - Texto - Class - Style - Objeto
    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'tipoCorreioEletronico', this.getNome(object.tipoCorreioEletronico), 'text-center', null, object),
        creator.addCol(idTable, index, 'assuntoCorreioEltco', object.assuntoCorreioEltco, 'text-center', null, object),
        creator.addCol(idTable, index, 'ativo', object.labelAtivo, 'text-center', null, object),
      ];
      rows.push(creator.addRow(idTable, index, cols, null, null, list[index]));
    }

    // Table
    // Id - Cabeçalhos - Linhas - Class - Style - Lista
    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

}
