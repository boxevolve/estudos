/**
 * Interface 'Listener' que determina o contrato da função callback referente ao 'confirm'.
 */
export interface ConfirmListener {
	method: () => void;
}
