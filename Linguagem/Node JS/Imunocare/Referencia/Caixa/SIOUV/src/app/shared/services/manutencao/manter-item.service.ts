import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Item } from 'app/shared/models/item';
import { MessageService } from 'app/shared/components/messages/message.service';

@Injectable()
export class ManterItemService extends CrudHttpClientService<Item> {

  constructor(
    protected http: HttpClient,
    protected messageService: MessageService
  ) {
    super('manutencao/manter-item', http);
  }

  public carregarItensPorAssunto(assunto: number): Item[] {
    const i = [
      this.newItem(1, 'Item 1'),
      this.newItem(2, 'Item 2'),
      this.newItem(3, 'Item 3'),
      this.newItem(4, 'Item 4'),
      this.newItem(5, 'Item 5'),
    ];
    return i;
  }

  private newItem(id: number, nome: string): Item {
    const i = new Item();
    i.id = id;
    i.noItemAssunto = nome;
    return i;
  }

  consultar(item: Item, next?: (itens: Item[]) => void, errorCallback?: any): any {
    this.callPOST<Item[]>('/consultar', item, next, error => {
      const message = errorCallback(error, 'Ocorreu um erro ao pesquisar itens.');
      this.messageService.addMsgDanger(message);
      console.log('Erro ao chamar o servico. Erro: ' + error);
    });
  }

  consultarId(next?: (itens: Item[]) => void, errorCallback?: any): any {
    this.callPOST<Item[]>('/consultarIdT', 0, next, error => {
      const message = errorCallback(error, 'Ocorreu um erro ao pesquisar itens.');
      this.messageService.addMsgDanger(message);
      console.log('Erro ao chamar o servico. Erro: ' + error);
    });
  }

}
