import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { ConfirmListener } from 'app/shared/components/messages/confirm-listener';
import { PerfilService } from 'app/arquitetura/shared/services/seguranca/perfil.service';
import { Perfil } from 'app/arquitetura/shared/models/seguranca/perfil';
import { DATAHORA_MASK } from 'app/shared/util/masks';

@Component({
  selector: 'app-perfil-consulta',
  templateUrl: './perfil-consulta.component.html',
  styleUrls: ['./perfil-consulta.component.css']
})
export class PerfilConsultaComponent extends BaseComponent {
  formulario: FormGroup;
	maskDataHora: Array<string | RegExp>;
  pagina: number = 1;
  itens: number = 5;
  nome: string = '';
  perfis: Perfil[] = null;

  constructor(
    protected messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
		private perfilService: PerfilService
  ) {
		super(messageService);

    this.formulario = this.formBuilder.group({
			nome: [{ value: this.nome }]
    });
  }

  consultar() {
		this.perfilService.consultarPorNomeParcial(this.nome).subscribe(
			(perfis: Perfil[]) => {
				this.perfis = perfis;
			},
			error => {
				this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar perfis.');
				console.log('Erro ao consultar perfis:', error);
			});
  }

  incluir() {
		this.router.navigate(['novo'], { relativeTo: this.route.parent });
	}

  alterar(perfil: Perfil) {
		this.router.navigate([perfil.id, 'editar'], { relativeTo: this.route.parent });
	}

	excluir(perfil: Perfil) {
		const indice: number = this.perfis.findIndex(item => item.id == perfil.id);

		this.messageService.addConfirmYesNo('Deseja realmente excluir o perfil de ID ' + perfil.id + '?',
			(): ConfirmListener => {
				this.perfilService.delete(perfil.id).subscribe(
					(perfil: Perfil) => {
						this.messageService.addMsgSuccess('Perfil excluído com sucesso.');

						if (indice >= 0) {
							this.perfis.splice(indice, 1);
						}
					},
					error => {
						this.messageService.addMsgDanger('Ocorreu um erro ao excluir o perfil.');
						console.log('Erro ao excluir perfil:', error);
					});
				return;
			}, null, null, 'Sim', 'Não');
	}
}
