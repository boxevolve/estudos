import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MATRICULA_MASK } from './../../../shared/util/masks';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { TipoOperacao } from 'app/shared/models/tipo-operacao';
import { TipoOperacaoService } from 'app/shared/services/manutencao/tipo-operacao.service';
import { Natureza } from 'app/shared/models/natureza';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { MotivoHistorico } from 'app/shared/models/motivo-historico';
import { MotivoHistoricoService } from 'app/shared/services/manutencao/motivo-historico.service';
import { MotivoHistoricoFiltro } from 'app/shared/models/motivo-historico.filtro';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';

@Component({
  selector: 'app-motivo-historico',
  templateUrl: './motivo-historico.component.html',
  styleUrls: ['./motivo-historico.component.css']
})
export class MotivoHistoricoComponent extends BaseComponent {

  /**
	* Nomes dos formControlName dos campos
	*/
  nameUsuario = 'usuario';
  nameOperacao = 'tiposOperacao';
  nameDataInicio = 'dataInicio';
  nameDataFim = 'dataFim';
  nameNatureza = 'natureza';
  nameMotivo = 'noMotivo';

  formulario: FormGroup;
  motivosHistorico: MotivoHistorico[] = null;
  motivoHistoricoFiltro: MotivoHistoricoFiltro;
  dataList: SiouvTable;

  matriculaMask = MATRICULA_MASK;

  /**
   * Detalhar
   */
  idModalDetalhar: string;
  historicoSelecionado: MotivoHistorico;

  tiposOperacao: TipoOperacao[] = [];
  naturezas: Natureza[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    protected tipoOperacaoService: TipoOperacaoService,
    protected naturezaService: NaturezaService,
    protected motivoHistoricoService: MotivoHistoricoService,
    protected messageService: MessageService
  ) {
    super(messageService);
    this.motivosHistorico = [];
    this.motivoHistoricoFiltro = new MotivoHistoricoFiltro();
    this.carregarTiposOperacao();
    this.carregarNaturezas();
    this.atualizarDataList();
    this.createFormGroup();
    this.validarFormulario(this.formulario);
  }

  carregarTiposOperacao(): void {
    this.tipoOperacaoService.get().subscribe(
      (tiposOperacao: TipoOperacao[]) => {
        const list: TipoOperacao[] = [];
        tiposOperacao.forEach(tipo => {
          if (tipo.id !== 4) {
            list.push(tipo);
          }
        });
        this.tiposOperacao = list;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os tipos de operação.');
        console.log('Erro ao recuperar os tipos de operação:', error);
      }
    );
  }

  carregarNaturezas(): void {
    this.naturezaService.todosAtivos().subscribe(
      (naturezas: Natureza[]) => {
        this.naturezas = naturezas;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as naturezas.');
        console.log('Erro ao recuperar as naturezas:', error);
      }
    );
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      usuario: [this.motivoHistoricoFiltro.usuario],
      natureza: [this.motivoHistoricoFiltro.natureza],
      dataInicio: [this.motivoHistoricoFiltro.dataInicio, [Validators.required]],
      dataFim: [this.motivoHistoricoFiltro.dataFim, [Validators.required]],
      noMotivo: [this.motivoHistoricoFiltro.noMotivo]
    });
  }

  pesquisar(): void {
    if (!this.formulario.valid) {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
    }
    this.montarObj();
    this.motivoHistoricoService.consultarPorFiltro(this.motivoHistoricoFiltro).subscribe(
      (motivosHistorico: MotivoHistorico[]) => {
        this.motivosHistorico = motivosHistorico;
        this.atualizarDataList();
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os históricos dos motivos.');
        console.log('Erro ao recuperar os históricos dos motivos:', error);
      }
    );
  }

  montarObj(): void {
    this.motivoHistoricoFiltro = this.getClone(this.formulario.value);
    this.motivoHistoricoFiltro.tiposOperacao = this.getValueArrayForm(this.formulario, this.nameOperacao, this.tiposOperacao);
  }

  limpar(): void {
    this.motivoHistoricoFiltro = new MotivoHistoricoFiltro();
    this.motivosHistorico = Array<MotivoHistorico>();
    this.createFormGroup();
    this.atualizarDataList();
  }

  voltar(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  setarIdModal(idModal: string): void {
    this.idModalDetalhar = idModal;
  }

  detalhar(motivoHistorico: MotivoHistorico): void {
    this.historicoSelecionado = motivoHistorico;
  }

  atualizarDataList() {
    this.dataList = this.createSiouvTable(this.motivosHistorico);
  }

  createSiouvTable(list: MotivoHistorico[]): SiouvTable {
    const idTable = 'consultaHistoricoMotivo';
    const creator: SiouvTableCreator<MotivoHistorico> = new SiouvTableCreator();
    // Cabeçalhos
    // Id - Texto - width - Class - Style
    const headers = [
      creator.addHeader('usuario', 'Usuário', 10, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('operacao', 'Operação', 10, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('data_hora', 'Data/Hora', 20, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('motivo', 'Motivo', 30, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('situacao', 'Situação', 15, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('acao', 'Ação', 15, 'text-center', {'vertical-align': 'middle'})
    ];

    // Linhas
    // Id - Texto - Class - Style - Objeto
    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'usuario', object.labelUsuario, 'text-center', null, object),
        creator.addCol(idTable, index, 'operacao', object.tipoOperacao.noTipoOperacao, 'text-center', null, object),
        creator.addCol(idTable, index, 'data_hora', object.labelDataHora, 'text-center', null, object),
        creator.addCol(idTable, index, 'motivo', object.noMotivo, 'text-center', null, object),
        creator.addCol(idTable, index, 'situacao', object.labelSituacao, 'text-center', null, object)
      ];
      rows.push(creator.addRowDetailModal(idTable, index, cols, null, null, this.idModalDetalhar, list[index]));
    }

    // Table
    // Id - Cabeçalhos - Linhas - Class - Style - Lista
    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

}
