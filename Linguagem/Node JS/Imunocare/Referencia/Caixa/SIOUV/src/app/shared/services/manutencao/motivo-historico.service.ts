import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { MotivoHistorico } from 'app/shared/models/motivo-historico';
import { MotivoHistoricoFiltro } from 'app/shared/models/motivo-historico.filtro';

@Injectable()
export class MotivoHistoricoService extends CrudHttpClientService<MotivoHistorico> {

  constructor(protected http: HttpClient) {
    super('manutencao/motivo-historico', http);
  }

  consultarPorFiltro(filtro: MotivoHistoricoFiltro): any {
    return this.http.post<MotivoHistorico[]>(this.url + '/consulta/filtro', filtro, this.options());
  }
}
