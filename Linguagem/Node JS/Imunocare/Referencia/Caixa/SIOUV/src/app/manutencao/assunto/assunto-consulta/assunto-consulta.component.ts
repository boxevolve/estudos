import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from 'app/shared/components/base.component';
import { Assunto } from 'app/shared/models/assunto';
import { Situacao } from 'app/shared/models/situacao';
import { MessageService } from 'app/shared/components/messages/message.service';
import { AssuntoService } from 'app/shared/services/manutencao/assunto.service';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { ConfirmListener } from 'app/shared/components/messages/confirm-listener';

@Component({
  selector: 'app-assunto-consulta',
  templateUrl: './assunto-consulta.component.html',
  styleUrls: ['./assunto-consulta.component.css']
})
export class AssuntoConsultaComponent extends BaseComponent implements OnInit {
  formulario: FormGroup;
  pagina = 1;
  itens = 5;
  assunto: Assunto = null;
  assuntos: Assunto[] = null;
  situacoes: Situacao[] = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    protected messageService: MessageService,
    private assuntoService: AssuntoService,
    private comumService: ComumService
  ) {
    super(messageService);
    this.assunto = new Assunto();

    this.formulario = this.formBuilder.group({
      nome: [this.assunto.noAssunto, [Validators.required]],
      situacao: [this.assunto.ativo]
    });
  }

  ngOnInit(): void {
    this.comumService.listarSituacoes().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger(
          'Ocorreu um erro ao pesquisar situações.'
        );
        console.log('Erro ao consultar situações:', error);
      }
    );
  }

  consultar() {
    if (this.formulario.valid) {
      this.assuntoService
        .consultar(this.assunto.noAssunto, this.assunto.ativo)
        .subscribe(
          (assuntos: Assunto[]) => {
            this.assuntos = assuntos;
            console.log('assunto');
            console.log(this.assuntos);
          },
          error => {
            this.messageService.addMsgDanger(
              'Ocorreu um erro ao pesquisar assuntos.'
            );
            console.log('Erro ao consultar assuntos:', error);
          }
        );
    } else {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
    }
  }

  incluir() {
    this.router.navigate(['novo'], { relativeTo: this.route.parent });
  }

  alterar(assunto: Assunto) {
    this.router.navigate([assunto.id, 'editar'], {
      relativeTo: this.route.parent
    });
  }

  excluir(assunto: Assunto) {
    const indice: number = this.assuntos.findIndex(
      item => item.id === assunto.id
    );

    this.messageService.addConfirmYesNo(
      'Deseja realmente excluir o assunto de ID ' + assunto.id + '?',
      (): ConfirmListener => {
        this.assuntoService.delete(assunto.id).subscribe(
          () => {
            this.messageService.addMsgSuccess('Assunto excluído com sucesso.');
            if (indice >= 0) {
              this.assuntos.splice(indice, 1);
            }
          },
          error => {
            this.messageService.addMsgDanger(
              'Ocorreu um erro ao excluir o assunto.'
            );
            console.log('Erro ao excluir o assunto:', error);
          }
        );
        return;
      },
      null,
      null,
      'Sim',
      'Não'
    );
  }

  historico() {
    this.router.navigate(['historico'], { relativeTo: this.route.parent });
  }

  limpar() {
    this.assunto = new Assunto();
    this.assuntos = null;
    this.formulario.reset();
  }
}
