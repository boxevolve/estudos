
export class NaturezaMotivoHistoricoId {

  public nuMotivo: string;
  public dhMotivo: Date;
  public nuNatureza: string;
  public dhNatureza: Date;
}
