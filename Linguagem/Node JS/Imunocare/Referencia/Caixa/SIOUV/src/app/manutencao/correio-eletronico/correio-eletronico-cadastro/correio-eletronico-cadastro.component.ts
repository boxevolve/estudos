import { Component, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Situacao } from 'app/shared/models/situacao';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CorreioEletronico } from 'app/shared/models/correio-eletronico';
import { TipoCorreioEletronico } from 'app/shared/models/tipo-correio-eletronico';
import { Unidade } from 'app/shared/models/unidade';
import { Util } from 'app/arquitetura/shared/util/util';
import { TipoCorreioEletronicoService } from 'app/shared/services/manutencao/tipo-correio-eletronico.service';

@Component({
  selector: 'app-correio-eletronico-cadastro',
  templateUrl: './correio-eletronico-cadastro.component.html',
  styleUrls: ['./correio-eletronico-cadastro.component.css']
})
export class CorreioEletronicoCadastroComponent extends BaseComponent implements AfterViewInit {

  ativo = 'ativo';
  tipoCorreioEletronico = 'tipoCorreioEletronico';
  deCorreioEletronico = 'deCorreioEletronico';
  deDestinatario = 'deDestinatario';
  deRemetente = 'deRemetente';
  dtEnvio = 'dtEnvio';
  envioAnterior = 'envioAnterior';
  envioPosterior = 'envioPosterior';
  solicitanteInterno = 'solicitanteInterno';
  solicitanteExterno = 'solicitanteExterno';
  unidadeDestino = 'unidadeDestino';
  unidadeOrigem = 'unidadeOrigem';
  unidadeSuperiorOrigem = 'unidadeSuperiorOrigem';
  assuntoCorreioEltco = 'assuntoCorreioEltco';
  dias = 'dias';
  unidadeCorreio = 'unidadeCorreio';

  formulario: FormGroup;
  correioEletronico: CorreioEletronico;
  email: string;
  deCompCorreioEletronico: string;
  contador = 2000;
  checkboxAtivo: boolean;

  tipos: TipoCorreioEletronico[] = [];
  situacoes: Situacao[] = [];
  listaDeEnderecosEletronicos: string[] = [];
  listaDeEnderecosEletronicosSelecionados: string[] = [];
  listaCheckboxes = [
    { id: 1, descricao: 'Unidade Destino', checkboxAtivo: true },
    { id: 2, descricao: 'Solicitante Interno', checkboxAtivo: true },
    { id: 3, descricao: 'Solicitante Externo', checkboxAtivo: true },
    { id: 4, descricao: 'Unidade Origem', checkboxAtivo: true },
    { id: 5, descricao: 'Unidade Superior Origem', checkboxAtivo: true },
    { id: 6, descricao: 'Outros', checkboxAtivo: true  }
  ];

  titulo = 'Incluir';
  tituloBotao = 'inclusão';

  idModalList = 'idModal';

  constructor(
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    protected messageService: MessageService,
    private comumService: ComumService,
    private tipoCorreioEletronicoService: TipoCorreioEletronicoService
  ) {
    super(messageService);
    this.correioEletronico = new CorreioEletronico();
    this.createFormGroup();
    this.carregarSituacoes();
    this.carregarTipos();
    this.carregarUnidades();
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  carregarTipos(): void {
    this.tipoCorreioEletronicoService.get().subscribe(
      (tipos: TipoCorreioEletronico[]) => {
        this.tipos = tipos.sort((a, b) => {
          if (a.nome > b.nome) {
            return 1;
          }
          if (a.nome < b.nome) {
            return -1;
          }
          return 0;
        });
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar os tipos.');
        console.log('Erro ao consultar os tipos:', error);
      });
  }

  carregarSituacoes(): void {
    this.comumService.listarSituacoesSemOpcaoTodos().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
        console.log('Erro ao consultar situações:', error);
      });
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({
      id: [this.correioEletronico.id],
      ativo: [this.correioEletronico.ativo, [Validators.required]],
      tipoCorreioEletronico: [this.correioEletronico.tipoCorreioEletronico],
      deCorreioEletronico: [this.correioEletronico.deCorreioEletronico],
      deDestinatario: [this.correioEletronico.deDestinatario],
      deRemetente: [this.correioEletronico.deRemetente],
      dtEnvio: [this.correioEletronico.dtEnvio],
      envioAnterior: [this.correioEletronico.envioAnterior],
      envioPosterior: [this.correioEletronico.envioPosterior],
      solicitanteInterno: [this.correioEletronico.solicitanteInterno],
      solicitanteExterno: [this.correioEletronico.solicitanteExterno],
      unidadeDestino: [this.correioEletronico.unidadeDestino],
      unidadeOrigem: [this.correioEletronico.unidadeOrigem],
      unidadeSuperiorOrigem: [this.unidadeSuperiorOrigem],
      assuntoCorreioEltco: [this.correioEletronico.assuntoCorreioEltco],
      dias: [this.correioEletronico.dias]
    });
  }

  carregarUnidades() {}

  receberUnidade(unidade: Unidade) {
    this.email = unidade.meioComunicacao.email;
  }

  receberIdModal(idModal: string): void {
    this.idModalList = idModal;
  }

  incluirEmail(email: string) {
    if (!Util.isEmpty(email)) {
      this.listaDeEnderecosEletronicos.push(email.trim());
      this.email = null;
    } else {
      this.messageService.addMsgDanger('Selecione um endereço antes de adicionar!');
    }
  }

  removerDaListaDeEnderecosEletronicos() {
    console.log(this.listaDeEnderecosEletronicosSelecionados);
    console.log(this.listaDeEnderecosEletronicos);
    for (let i = 0; i < this.listaDeEnderecosEletronicosSelecionados.length; i++) {
      const item = this.listaDeEnderecosEletronicos.find(element => element === this.listaDeEnderecosEletronicosSelecionados[i]);
      const index = this.listaDeEnderecosEletronicos.indexOf(item);
      this.listaDeEnderecosEletronicos.splice(index, 1);
    }
    this.listaDeEnderecosEletronicosSelecionados = [];
    // console.log(this.listaDeEnderecosEletronicosSelecionados);
    // lista que sobra com os valores válidos.
    console.log(this.listaDeEnderecosEletronicos);
  }

  valorTextoComplementar(nuTipoCorreioEltco: number) {
    console.log(nuTipoCorreioEltco);
    this.tipos.forEach(x => {
      if (x.id == nuTipoCorreioEltco) {
        this.deCompCorreioEletronico = x.textoComplementar;
      }
    });
  }

  verificarCaracteres(texto: string) {
    this.contador = 2000 - texto.length;
  }

  verificarEstadoDoCheckbox(nuTipoCorreioEltco: number) {
    this.listaCheckboxes.forEach(element => {
      if (nuTipoCorreioEltco === 16 || nuTipoCorreioEltco === 12 || nuTipoCorreioEltco === 13 ||
          nuTipoCorreioEltco === 14 || nuTipoCorreioEltco === 15 || nuTipoCorreioEltco === 18) {
        // campodeDestinatario também deve ser habilitado
        if (element.id === 6) {
          element.checkboxAtivo = true;
        } else {
          element.checkboxAtivo = false;
        }
      } else if (nuTipoCorreioEltco === 20) {
          // campodeDestinatario também deve ser habilitado
          element.checkboxAtivo = true;
      } else if (nuTipoCorreioEltco === 1 || nuTipoCorreioEltco === 2 || nuTipoCorreioEltco === 3 ||
                 nuTipoCorreioEltco === 4 || nuTipoCorreioEltco === 5 || nuTipoCorreioEltco === 6 ||
                 nuTipoCorreioEltco === 8 || nuTipoCorreioEltco === 10 || nuTipoCorreioEltco === 11) {
          // campodeDestinatario também deve ser habilitado
          if (element.id === 4 || element.id === 5) {
            element.checkboxAtivo = false;
          } else {
              element.checkboxAtivo = true;
          }
      } else if (nuTipoCorreioEltco === 7 || nuTipoCorreioEltco === 17 || nuTipoCorreioEltco === 9) {
          // campodeDestinatario também deve ser habilitado => campo Endereço Eletrônico
          element.checkboxAtivo = true;
      } else {
          element.checkboxAtivo = false;
      }
    });
  }
}
