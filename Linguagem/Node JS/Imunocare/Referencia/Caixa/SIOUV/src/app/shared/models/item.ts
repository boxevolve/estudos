import { Entity } from 'app/arquitetura/shared/models/entity';
import { Assunto } from './assunto';
import { Unidade } from './unidade';
import { ItemNatureza } from './item-natureza';

export class Item extends Entity {
  public id: number = null;
  public noItemAssunto: string = null;
  public usuario: string = null;
  public agencia: string = null;
  public ativo: string = null;
  public cartaoCredito: string = null;
  public carteiraTrabalho: string = null;
  public contratoHabitacao: string = null;
  public correspondenteBancario: string = null;
  public informacaoBancaria: string = null;
  public nis: string = null;
  public revendedorLoterico: string = null;
  public itemAssunto: string = null;
  public assunto: Assunto;
  public natural: number = null;
  public unidade: Unidade;
  public labelAtiva: string;
  public itensNatureza: ItemNatureza[] = Array<ItemNatureza>();

}
