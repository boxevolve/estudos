import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

declare var jQuery: any;
declare var Pace: any;

/**
 * Implementação de componente responsável por gerar a estrutura 'html' onde os alertas serão gerados.
 */
@Component({
	selector: 'app-loading-modal',
	templateUrl: './loading-modal.component.html',
	styleUrls: ['./loading-modal.component.scss'],
	exportAs: 'Loading',
	encapsulation: ViewEncapsulation.None
})
export class LoadingModalComponent implements OnInit {
	static loading(value?) {
		if (value) {
			jQuery('.loadingText').text(value);
		} else {
			return Number(jQuery('.loadingText').text());
		}
	}

	static start(loading?: any) {
		LoadingModalComponent.loading(0);
		if (loading) {
			LoadingModalComponent.showProgress(true);
		}
		Pace.restart();
	}

	static stop() {
		Pace.stop();
		LoadingModalComponent.loading(0);
	}

	static track(callback?: Function, loading?: any) {
		if (loading) {
			LoadingModalComponent.loading(0);
			LoadingModalComponent.showProgress(true);
		}

		Pace.track(callback);
	}

	static showProgress(show?: boolean) {
		/*if (show) {
				jQuery(document).ready(function () {
						const element = jQuery('.pace-progress', document)[0];
						const observer = new MutationObserver(function (mutations) {
								mutations.forEach(function (mutation) {
										LoadingModalComponent.loading = mutation.target.attributes['data-progress-text'].nodeValue;
								});
						});
						const config = {
								attributes: true,
								childList: true,
								characterData: true,
								attributeFilter: ['data-progress-text']
						};
						observer.observe(element, config);
						// observer.disconnect();
						LoadingModalComponent._loading = false;
				});
		}*/
	}

	ngOnInit() {
		const me = this;

		Pace.on('start', function () {
			jQuery('#loading-modal').modal('show');
			jQuery('body').removeClass('pace-done');
			this.visible = true;
		});
		Pace.on('done', function () {
			me.hidePace();
		});
		Pace.on('hide', function () {
			me.hidePace();
		});
		Pace.on('stop', function () {
			me.hidePace();
		});
		LoadingModalComponent.showProgress(false);
	}

	public hidePace() {
		if (jQuery('#loading-modal').is(':visible')) {
			jQuery('#loading-modal').modal('hide');
			jQuery('body').removeClass('pace-running');
		}
	}
}
