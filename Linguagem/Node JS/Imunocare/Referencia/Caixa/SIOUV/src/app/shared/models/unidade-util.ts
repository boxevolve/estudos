import { FormGroup } from '@angular/forms';
import { BaseComponent } from '../components/base.component';
import { MessageService } from '../components/messages/message.service';
import { Util } from 'app/arquitetura/shared/util/util';
import { AfterViewInit, ChangeDetectorRef } from '@angular/core';

export class UnidadeUtil extends BaseComponent implements AfterViewInit {

  idsModalUnidade: any[] = [];

  constructor(
    protected messageService: MessageService,
    private cdr: ChangeDetectorRef
  ) {
    super(messageService);
    this.idsModalUnidade = new Array();
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  object(idModal: string, idDescricao: string, idBtnModal: string, idBtnLimpar: string): any {
    return {
      modal: idModal,
      descricao: idDescricao,
      btnHiddenModal: idBtnModal,
      btnHiddenLimpar: idBtnLimpar
    };
  }

  receberId(idModal: any): void {
    this.idsModalUnidade.push(idModal);
    this.cdr.detectChanges();
  }

  limpar(position: number): void {
    const ids = this.getItemList(this.idsModalUnidade, position);
    $(this.idHashtag(Util.isEmpty(ids) ? null : ids.btnHiddenLimpar)).click();
  }

  disableBtnModal(form: FormGroup, position: number): boolean {
    const ids = this.getItemList(this.idsModalUnidade, position);
    if (!Util.isEmpty(ids)) {
      return !Util.isEmpty(this.getValueForm(form, ids.descricao));
    }
    return false;
  }

  idHashTag(position: number): string {
    const ids = this.getItemList(this.idsModalUnidade, position);
    return this.idHashtag(Util.isEmpty(ids) ? null : ids.modal);
  }

}
