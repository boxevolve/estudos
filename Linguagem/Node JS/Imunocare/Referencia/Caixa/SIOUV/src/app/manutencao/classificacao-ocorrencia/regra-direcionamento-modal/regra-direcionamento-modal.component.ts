import { Component, Output, AfterViewInit, Input, ChangeDetectorRef } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder} from '@angular/forms';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Uf } from 'app/shared/models/uf';
import { UfService } from 'app/shared/services/comum/uf.service';
import { RegraDirecionamento } from 'app/shared/models/regraDirecionamento';
import { RegraDirecionamentoId } from 'app/shared/models/regra-direcionamento-id';
import { Unidade } from 'app/shared/models/unidade';
import { Util } from 'app/arquitetura/shared/util/util';

@Component({
  selector: 'app-regra-direcionamento-modal',
  templateUrl: './regra-direcionamento-modal.component.html',
  styleUrls: ['./regra-direcionamento-modal.component.css']
})
export class RegrasDirecionamentoModalComponent extends BaseComponent implements AfterViewInit {

  modalId = 'regras_direcionamento';

  @Input() idBase = 'regras_direcionamento_base';
  @Input() salvos: RegraDirecionamento[] = [];
  @Output() selecionarRegras = new EventEmitter();
  @Output() emitIdModal = new EventEmitter();

  /**
  * Nomes dos formControlName dos campos
  */
  nameUnidade = 'unidade';
  nameUfs = 'ufs';

  formulario: FormGroup;
  dataList: SiouvTable;
  ufs: Uf[] = [];
  regrasDirecionamento: RegraDirecionamento[] = [];

  modalUnidadeExterna = 'modalExterna';
  unidadeExterna: Unidade;

  constructor(
    private cdr: ChangeDetectorRef,
    protected messageService: MessageService,
    private formBuilder: FormBuilder,
    private ufService: UfService
  ) {
    super(messageService);
    this.carregarUfs();
    this.createFormGroup();
  }

  ngAfterViewInit() {
    this.emitIdModal.emit(this.getClone(this.getIdModal([])));
    this.showBsModal(this.getIdModal([]), () => {
      this.carregarModal();
    });
    this.hiddenModal(this.getIdModal([]), () => {
      this.limpar();
    });
    this.atualizarDataList();
    this.cdr.detectChanges();
  }

  hide(): void {
    this.hideModal(this.getIdModal([]));
  }

  limpar(): void {
    this.formulario.reset();
    this.carregarUfs();
    this.regrasDirecionamento = [];
    this.atualizarDataList();
  }

  carregarUfs(): void {
    this.ufService.todos((ufs: Uf[]) => {
      this.ufs = ufs;
    });
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({});
  }

  atualizarDataList(): void {
    this.dataList = this.createSiouvTable(this.regrasDirecionamento);
  }

  createSiouvTable(list: RegraDirecionamento[]): SiouvTable {
    const idTable = 'regrasDirecionamento';
    const creator: SiouvTableCreator<RegraDirecionamento> = new SiouvTableCreator();

    const headers = [
      creator.addHeader('_unidades', 'Unidades', 33, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('_uf', 'UF', 33, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('_acao', 'Ação', 33, 'text-center', {'vertical-align': 'middle'})
    ];

    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'unidade', this.getUnidadeDescricao(object.unidadeEnvolvida, 1), 'text-center', null, object),
        creator.addCol(idTable, index, 'uf', object.id.sgUf, 'text-center', null, object),
      ];
      rows.push(creator.addRowDelete(idTable, index, cols, null, null, list[index]));
    }
    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }

  incluir(): void {
    if (!this.validarFormulario(this.formulario)) {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
    }
    const ufsDirecionamento = this.getValueArrayForm(this.formulario, this.nameUfs, this.ufs);
    const unidade = this.getValueForm(this.formulario, this.nameUnidade);
    if (!this.verificarJaExiste(ufsDirecionamento, unidade)) {
      ufsDirecionamento.forEach(uf => {
        const regraId: RegraDirecionamentoId = new RegraDirecionamentoId();
        regraId.sgUf = uf.sgUf;
        const regra: RegraDirecionamento = new RegraDirecionamento();
        regra.id = regraId;
        regra.unidadeEnvolvida = unidade;
        this.regrasDirecionamento.push(regra);
        this.ufs = this.removeList(this.ufs, uf);
      });
      this.atualizarDataList();
    }
  }

  verificarJaExiste(ufsDirecionamento: Uf[], unidade: Unidade): boolean {
    for (let index = 0; index < ufsDirecionamento.length; index++) {
      const uf = ufsDirecionamento[index];
      if (this.contains(this.regrasDirecionamento, null, (regra: any) => {
        return this.equals(regra.unidadeEnvolvida, unidade)
            && this.equals(regra.id.sgUf, uf.sgUf);
      })) {
        this.messageService.addMsgDanger(`Já existe o vínculo ${unidade.nome} para UF ${uf.sgUf}`);
        return true;
      }
    }
    return false;
  }

  incluirRegras(): void {
    if (!Util.isEmpty(this.ufs)) {
      this.messageService.addMsgDanger('Todas as UFs devem ser vinculadas á alguma Unidade.');
      return;
    }
    this.selecionarRegras.emit(this.getClone(this.regrasDirecionamento));
    this.messageService.addMsgSuccess('Regra de redirecionamento criada com sucesso.');
    this.hide();
  }

  remover(regraDirecionamento: RegraDirecionamento): void {
    this.ufs.push(this.getClone(this.getUf(regraDirecionamento.id.sgUf)));
    this.ufs.sort((a, b) => {
      return (a.sgUf > b.sgUf) ? 1 : ((a.sgUf < b.sgUf) ? -1 : 0 );
    });
    this.ufs = this.getClone(this.ufs);
    this.regrasDirecionamento = this.removeList(this.regrasDirecionamento, regraDirecionamento);
    this.atualizarDataList();
  }

  carregarModal(): void {
    if (!Util.isEmpty(this.salvos)) {
      this.salvos.forEach(data => {
        this.regrasDirecionamento.push(data);
        const uf = data.id ? data.id.sgUf : null;
        if (uf) {
          this.ufs = this.removeList(this.regrasDirecionamento, null, (value) => {
            return this.equals(value.sgUf, uf);
          });
        }
      });
      this.atualizarDataList();
    }
  }

  getUf(sigla: string): Uf {
    return new Uf(sigla, sigla);
  }

  getIdModal(fields: string[]): string {
    return this.idModal(this.modalId, this.idBase, fields);
  }

  getIdField(field: string): string {
    return this.idField([this.getIdModal([]), field]);
  }

  receberIdModalUnidade(idModal: string): void {
    this.modalUnidadeExterna = idModal;
  }

  receberUnidade(unidadeSelecionada: Unidade): void {
    this.unidadeExterna = unidadeSelecionada;
  }

}
