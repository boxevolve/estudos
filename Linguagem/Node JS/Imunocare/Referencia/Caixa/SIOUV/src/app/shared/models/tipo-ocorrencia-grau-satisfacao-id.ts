export class TipoOcorrenciaGrauSatisfacaoId {
    constructor(
        public grauSatisfacao: number,
	    public tipoOcorrencia: number
    ) {}
}