import { Component, Output, ViewChild, ElementRef, AfterViewInit, EventEmitter, Input } from '@angular/core';
import { NaturezaHistorico } from 'app/shared/models/natureza-historico';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';

@Component({
  selector: 'app-natureza-detalhe',
  templateUrl: './natureza-detalhe.component.html',
  styleUrls: ['./natureza-detalhe.component.css']
})
export class NaturezaDetalheComponent extends BaseComponent implements AfterViewInit {

  modalId = 'modalNaturezaDetalhe';

  @ViewChild('modalNaturezaDetalhe') modal: ElementRef;
  @Input('naturezaHistorico') naturezaHistorico: NaturezaHistorico;
  @Output() emitModal = new EventEmitter();

  /**
 * Nomes dos formControlName dos campos
 */
  nameUsuario = 'coUsuario';
  nameOperacao = 'tipoOperacao';
  nameDataHora = 'dhNatureza';
  nameNatureza = 'noNatureza';
  nameTipoOcorrencia = 'tipoOcorrencia';
  nameAceite = 'icAceite';
  nameRestrita = 'icRestrito';
  nameSituacao = 'icAtiva';
  nameOrientacao = 'deOrientacao';

  detalhes: any[] = [];
  carregado = false;

  constructor(
      protected messageService: MessageService,
  ) {
      super(messageService);
  }

  ngAfterViewInit() {
    this.emitModal.emit(this.getClone(this.modalId));
    $(this.modal.nativeElement).on('hidden.bs.modal',  () => {
        this.limpar();
    });
  }

  limpar(): void {
      this.naturezaHistorico = null;
      this.carregado = !this.carregado;
  }

  getDetalhes(): any[] {
    if (this.naturezaHistorico && !this.carregado) {
      this.detalhes = [
        {titulo: 'Usuário', value: this.naturezaHistorico.labelUsuario},
        {titulo: 'Operação', value: this.naturezaHistorico.tipoOperacao.noTipoOperacao},
        {titulo: 'Data/Hora', value: this.naturezaHistorico.labelDataHora},
        {titulo: 'Natureza', value: this.naturezaHistorico.noNatureza},
        {titulo: 'Tipo de Ocorrência', value: this.naturezaHistorico.labelTipoOcorrencia},
        {titulo: 'Aceite', value: this.naturezaHistorico.labelAceite},
        {titulo: 'Restrita', value: this.naturezaHistorico.labelRestrito},
        {titulo: 'Situação', value: this.naturezaHistorico.labelSituacao},
        {titulo: 'Orientação', value: this.naturezaHistorico.deOrientacao},
      ];
      this.carregado = !this.carregado;
    }
    return this.detalhes;
  }

  text(text: string): string {
    return text ? text : '';
  }

}
