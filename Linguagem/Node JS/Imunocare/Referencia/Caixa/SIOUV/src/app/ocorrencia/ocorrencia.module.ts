import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TextMaskModule } from 'angular2-text-mask';
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { LabelModule } from 'app/shared/components/label/label.module';
import { UnidadeModule } from 'app/shared/components/unidade/unidade.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { OcorrenciaCadastroComponent } from './ocorrencia-cadastro/ocorrencia-cadastro.component';
import { OcorrenciaConsultaComponent } from './ocorrencia-consulta/ocorrencia-consulta.component';
import { OcorrenciaResultadoConsultaComponent } from './ocorrencia-resultado-consulta/ocorrencia-resultado-consulta.component';
import { OcorrenciaRoutingModule } from './ocorrencia.routing.module';
import { ParametrosConsultaOcorrencia } from './ocorrencia-consulta/parametros-consulta-ocorrencia';


@NgModule({
    imports: [
        CommonModule,
		RouterModule,
		FormsModule,
		ReactiveFormsModule,
		TextMaskModule,
		NgxPaginationModule,
		DirectivesModule.forRoot(),
		TemplatesModule,
        ComponentModule.forRoot(),
        LabelModule,
        UnidadeModule,
        OcorrenciaRoutingModule
    ],
    declarations: [
        OcorrenciaConsultaComponent,
        OcorrenciaCadastroComponent,
        OcorrenciaResultadoConsultaComponent
    ],
    providers: [
        ParametrosConsultaOcorrencia
    ]
})
export class OcorrenciaModule { }
