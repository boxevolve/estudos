import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { ConfirmListener } from 'app/shared/components/messages/confirm-listener';
import { Motivo } from 'app/shared/models/motivo';
import { Util } from 'app/arquitetura/shared/util/util';
import { Situacao } from 'app/shared/models/situacao';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { Natureza } from 'app/shared/models/natureza';
import { NaturezaMotivo } from 'app/shared/models/natureza-motivo';
import { MotivoService } from 'app/shared/services/manutencao/motivo.service';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { NaturezaMotivoId } from 'app/shared/models/natureza-motivo-id';

@Component({
  selector: 'app-motivo-cadastro',
  templateUrl: './motivo-cadastro.component.html',
  styleUrls: ['./motivo-cadastro.component.css']
})
export class MotivoCadastroComponent extends BaseComponent {

  nameMotivo = 'nome';
  nameSituacao = 'situacao';
  nameDescricao = 'descricao';

  btnGravar: string;
  titulo: string;
  formulario: FormGroup;
  maskDataHora: Array<string | RegExp>;
  motivo: Motivo;
  situacoes: Situacao[];
  naturezas: Natureza[] = null;
  naturezasSelecionadas: NaturezaMotivo[] = [];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    protected messageService: MessageService,
    private motivoService: MotivoService,
    private comumService: ComumService,
    private naturezaService: NaturezaService
  ) {
    super(messageService);

    this.titulo = 'Incluir';
    this.btnGravar = 'Confirmar Inclusão';
    this.motivo = new Motivo();
    this.carregarSituacoes();
    this.carregarNaturezas();
    this.criarFormulario();

    this.route.params.subscribe(
      (params: any) => {
        const id: number = params['id'];

        if (!Util.isEmpty(id)) {
          this.titulo = 'Alterar';
          this.btnGravar = 'Confirmar Alteração';
          this.motivoService.get(id).subscribe(
            (motivo: Motivo) => {
              this.motivo = motivo;
              this.atualizarFormulario();
              this.montarListaSelecionados();
            },
            error => {
              this.messageService.addMsgDanger('Ocorreu um erro ao carregar o motivo.');
              console.log('Erro ao carregar motivo:', error);
            });
        }
      }
    );
  }

  criarFormulario() {
    this.formulario = this.formBuilder.group({
      nome: [this.motivo.nome, [Validators.required]],
      situacao: [this.motivo.ativo, [Validators.required]],
      descricao: [this.motivo.descricao, [Validators.required]]
    });
  }

  atualizarFormulario() {
    this.setValue(this.formulario, this.nameMotivo, this.motivo.nome);
    this.setValue(this.formulario, this.nameSituacao, this.motivo.ativo);
    this.setValue(this.formulario, this.nameDescricao, this.motivo.descricao);
  }

  carregarSituacoes(): void {
    this.comumService.listarSituacoesSemOpcaoTodos().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
        this.atualizarFormulario();
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
        console.log('Erro ao consultar situações:', error);
      });
  }

  carregarNaturezas(): void {
    this.naturezaService.get().subscribe(
      (naturezas: Natureza[]) => {
        this.naturezas = naturezas;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as naturezas.');
        console.log('Erro ao recuperar as naturezas:', error);
      });
  }

  isCheckNatureza(natureza: Natureza): Boolean {
    if (!this.motivo.naturezasMotivo) {
      return false;
    }

    for (let index = 0; index < this.motivo.naturezasMotivo.length; index++) {
      if (this.motivo.naturezasMotivo[index].id.nuNatureza === natureza.id) {
        return true;
      }
    }
    return false;
  }

  isSelectSituacao(situacao: Situacao): Boolean {
    return (this.motivo.ativo === situacao.value);
  }

  atualizarNaturezasSelecionadas(naturezaSelecionada: Natureza) {
    let incluso = false;
    for (let i = 0; i < this.naturezasSelecionadas.length; i++) {
      if (this.naturezasSelecionadas[i].id.nuNatureza === naturezaSelecionada.id) {
        incluso = true;
        const index = this.naturezasSelecionadas.indexOf(this.naturezasSelecionadas[i]);
        this.naturezasSelecionadas.splice(index, 1);
      }
    }
    if (!incluso) {
      const naturezaMotivoId = new NaturezaMotivoId(naturezaSelecionada.id, null);
      const nova = new NaturezaMotivo(naturezaMotivoId);
      nova.natureza = naturezaSelecionada;
      this.naturezasSelecionadas.push(nova);
    }
  }

  setarAtivoInativoNaNaturezaSelecionada(naturezaSelecionada: Natureza, valor: string) {
    for (let index = 0; index < this.naturezasSelecionadas.length; index++) {
      if (this.naturezasSelecionadas[index].id.nuNatureza === naturezaSelecionada.id) {
        this.naturezasSelecionadas[index].icAtiva = valor;
      }
    }
  }

  montarListaSelecionados(): void {
    this.motivo.naturezasMotivo.forEach(naturezaMotivo => {
      this.naturezasSelecionadas.push(naturezaMotivo);
    });
  }

  isSelectSituacaoList(natureza: Natureza, situacao: Situacao): boolean {
    for (let index = 0; index < this.naturezasSelecionadas.length; index++) {
      const selecionado = this.naturezasSelecionadas[index];
      if (selecionado.id.nuNatureza === natureza.id) {
        return selecionado.icAtiva === situacao.value;
      }
    }
    return false;
  }

  isNew(): boolean {
    return Util.isEmpty(this.motivo.id);
  }

  limpar() {
    this.motivo = new Motivo();
    this.naturezasSelecionadas = [];
    this.atualizarFormulario();
  }

  gravar() {
    this.criarObjeto();

    if (!this.validarFormulario(this.formulario)) {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
    }
    if (this.naturezasSelecionadas.length < 1) {
      this.messageService.addMsgDanger('Selecione ao menos uma Natureza.');
      return;
    }

    if (this.isNew()) {
      this.motivoService.post(this.motivo).subscribe(_ => {
        this.messageService.addMsgSuccess('Operação efetuada com sucesso.');
        this.consultar();
      },
        error => {
          let errMsg;
          if (error.status === 405) {
            errMsg = 'Motivo já cadastrado';
          } else {
            errMsg = 'Ocorreu um erro ao incluir o motivo.';
          }
          this.messageService.addMsgDanger(errMsg);
          console.log(errMsg);
        });
    } else {
      this.motivoService.put(this.motivo).subscribe(_ => {
        this.messageService.addMsgSuccess('Operação efetuada com sucesso.');
        this.consultar();
      },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao alterar o motivo.');
          console.log('Erro ao alterar motivo:', error);
        });
    }
  }

  criarObjeto(): Boolean {
    const dados = this.formulario.value;

    this.motivo.nome = dados.nome;
    this.motivo.ativo = dados.situacao;
    this.motivo.descricao = dados.descricao;
    this.motivo.naturezasMotivo = this.naturezasSelecionadas;

    return true;
  }

  excluir() {
    this.messageService.addConfirmYesNo('Deseja realmente excluir o motivo?',
      (): ConfirmListener => {
        this.motivoService.delete(this.motivo.id).subscribe(_ => {
          this.messageService.addMsgSuccess('Motivo excluído com sucesso.');
          this.consultar();
        },
          error => {
            this.messageService.addMsgDanger('Ocorreu um erro ao excluir o motivo.');
            console.log('Erro ao excluir motivo:', error);
          });
        return;
      }, null, null, 'Sim', 'Não');
  }

  voltar() {
    this.consultar();
  }

  consultar() {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }
}
