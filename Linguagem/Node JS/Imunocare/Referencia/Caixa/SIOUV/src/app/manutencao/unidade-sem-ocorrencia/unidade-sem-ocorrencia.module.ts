import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnidadeSemOcorrenciaRoutingModule } from './unidade-sem-ocorrencia-routing.module';
import { ConsultaUnidadeSemOcorrenciaComponent } from './consulta-unidade-sem-ocorrencia/consulta-unidade-sem-ocorrencia.component';
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { TextMaskModule } from 'angular2-text-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SiouvTableModule } from 'app/shared/components/table/table.module';
import { UnidadeModule } from 'app/shared/components/unidade/unidade.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    UnidadeSemOcorrenciaRoutingModule,
    SiouvTableModule,
    UnidadeModule
  ],
  declarations: [ConsultaUnidadeSemOcorrenciaComponent]
})
export class UnidadeSemOcorrenciaModule { }
