import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { TextoAbertura } from '../../models/texto-abertura';
import { Observable } from 'rxjs';

@Injectable()
export class TextoAberturaService extends CrudHttpClientService<TextoAbertura> {

	constructor(protected http: HttpClient) {
		super('manutencao/texto-abertura', http);
	}

	/**
	 * Consulta de texto de abertura por ocorrência
	 */
	public consultarPorOcorrencia(codigo: number): Observable<TextoAbertura> {
		return this.http.get<TextoAbertura>(this.url + '/consultar-por-ocorrencia/'+ codigo,
			this.options());
	}

}
