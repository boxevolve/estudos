import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from 'app/shared/components/base.component';
import { Assunto } from 'app/shared/models/assunto';
import { Situacao } from 'app/shared/models/situacao';
import { MessageService } from 'app/shared/components/messages/message.service';
import { AssuntoService } from 'app/shared/services/manutencao/assunto.service';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { Util } from 'app/arquitetura/shared/util/util';


@Component({
  selector: 'app-assunto-cadastro',
  templateUrl: './assunto-cadastro.component.html',
  styleUrls: ['./assunto-cadastro.component.css']
})
export class AssuntoCadastroComponent extends BaseComponent implements OnInit {
  formulario: FormGroup;
  assunto: Assunto = null;
  situacoes: Situacao[] = null;
  titulo: string = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    protected messageService: MessageService,
    private assuntoService: AssuntoService,
    private comumService: ComumService
  ) {
    super(messageService);
    this.assunto = new Assunto();
    this.titulo = 'Incluir';
    this.formulario = this.formBuilder.group({
      nome: [
        this.assunto.noAssunto,
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100)
        ]
      ],
      situacao: [
        this.assunto.ativo,
        [Validators.required, Validators.minLength(1)]
      ]
    });
    this.route.params.subscribe((params: any) => {
      const id: number = params['id'];

      if (!Util.isEmpty(id)) {
        this.titulo = 'Editar';
        this.assuntoService.get(id).subscribe(
          (assunto: Assunto) => {
            this.assunto = assunto;
          },
          error => {
            this.messageService.addMsgDanger(
              'Ocorreu um erro ao carregar o assunto.'
            );
            console.log('Erro ao carregar assunto:', error);
          }
        );
      }
    });
  }

  ngOnInit() {
    this.comumService.listarSituacoesSemOpcaoTodos().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger(
          'Ocorreu um erro ao pesquisar situações.'
        );
        console.log('Erro ao consultar situações:', error);
      }
    );
  }

  isNew(): boolean {
    return Util.isEmpty(this.assunto.id);
  }

  salvar() {
    if (this.formulario.valid) {
      if (this.isNew()) {
        this.assuntoService.post(this.assunto).subscribe(
          (assunto: Assunto) => {
            this.router.navigate(['.'], { relativeTo: this.route.parent });
            this.messageService.addMsgSuccess('Assunto inserido com sucesso.');
          },
          error => {
            let msg = 'Ocorreu um erro ao incluir o assunto.';
            if (error.error) {
              msg = error.error;
            }
            this.messageService.addMsgDanger(msg);
          }
        );
      } else {
        this.assuntoService.put(this.assunto).subscribe(
          (assunto: Assunto) => {
            this.router.navigate(['.'], { relativeTo: this.route.parent });
            this.messageService.addMsgSuccess('Assunto alterado com sucesso.');
          },
          error => {
            let msg = 'Ocorreu um erro ao alterar o assunto.';
            if (error.error) {
              msg = error.error;
            }
            this.messageService.addMsgDanger(msg);
          }
        );
      }
    } else {
      this.messageService.addMsgDanger('Preencha os campos obrigatórios.');
    }
  }

  limpar() {
    this.assunto = new Assunto();
    this.formulario.reset();
  }

  voltar() {
    this.limpar();
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }
}
