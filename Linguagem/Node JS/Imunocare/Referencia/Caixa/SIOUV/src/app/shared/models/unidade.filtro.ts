export class UnidadeFiltro {
    public nuUnidade: number = null;
    public nome: string = null;
    public sigla: string = null;
    public uf: string = null;
    public isAvancada: string = null;
    public cidade: string = null;
    public bairro: string = null;
    public tipoUnidade: number = null;
}
