import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { NaturezaConsultaComponent } from './natureza-consulta/natureza-consulta.component';
import { NaturezaCadastroComponent } from './natureza-cadastro/natureza-cadastro.component';
import { NaturezaHistoricoComponent } from './natureza-historico/natureza-historico.component';

const routes: Routes = [{
    path: '',
    canActivate: [AuthGuard, DadosUsuarioGuard],
    canActivateChild: [AuthGuard, DadosUsuarioGuard],
    children: [
        {
            path: '',
            component: NaturezaConsultaComponent
        },
        {
            path: 'novo',
            component: NaturezaCadastroComponent
        },
        {
            path: ':id/editar',
            component: NaturezaCadastroComponent
        },
        {
            path: 'historico',
            component: NaturezaHistoricoComponent
        }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NaturezaRoutingModule { }
