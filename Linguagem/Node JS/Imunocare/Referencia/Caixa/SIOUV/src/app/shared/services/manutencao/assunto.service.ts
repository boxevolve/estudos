import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Assunto } from '../../models/assunto';
import { MessageService } from 'app/shared/components/messages/message.service';

@Injectable()
export class AssuntoService extends CrudHttpClientService<Assunto> {
  constructor(protected http: HttpClient, private messageService: MessageService) {
    super('manutencao/assunto', http);
  }

  consultar(nome: string, situacao: string): any {
    let httpParams: HttpParams = new HttpParams();
    httpParams = httpParams.set('noAssunto', nome);
    httpParams = httpParams.set('icSituacao', situacao ? situacao : '');

    return this.http.get<Assunto[]>(
      this.url + '/consultar',
      this.options({ params: httpParams })
    );
	}
	
	public carregarAssuntos(): any {
		return this.http.get<Assunto[]>(this.url);
	}

	public consultarAssuntoAtivo(next?: (value: any) => void): void {
		this.callGET<Assunto[]>('/consultar-ativos', next, error => {
			this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar assuntos.');
			console.log('Erro ao consultar assuntos:', error);
		});
	}

	public callListarAssuntosPorNatureza(idNatureza: number, next?: (value: any) => void): void {
		this.callGET<Assunto[]>('/itens/ativos/natureza/' + idNatureza, next, error => {
			this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar assuntos.');
			console.log('Erro ao consultar assuntos:' + error)
		});
	}

	/*
	FIX-ME: (inicio) REMOVER DAQUI!!
	*/
	public carregarAssuntosPorNatureza(nuNatureza: number): Assunto[] {
		return [
			this.newAssunto(1, 'Assunto 1'),
			this.newAssunto(2, 'Assunto 2'),
			this.newAssunto(3, 'Assunto 3'),
			this.newAssunto(4, 'Assunto 4'),
			this.newAssunto(5, 'Assunto 5')
		];
	}

	newAssunto(id: number, nome: string): Assunto {
		var a = new Assunto();
		a.id = id;
		a.noAssunto = nome;
		return a;
	}

	/*
	FIX-ME: (fim) ATÉ AQUI!!
	*/
}
