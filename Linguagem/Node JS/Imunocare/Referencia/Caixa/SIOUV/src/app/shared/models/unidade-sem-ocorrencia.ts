import { Unidade } from "./unidade";
import { UnidadeSemOcorrenciaId } from "./unidade-sem-ocorrencia-id";

export class UnidadeSemOcorrencia {
  id: UnidadeSemOcorrenciaId;
  dhRegistro: Date;
  unidade: Unidade;
}