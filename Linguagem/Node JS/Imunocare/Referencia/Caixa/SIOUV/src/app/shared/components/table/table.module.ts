import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiouvTableComponent } from './siouv-table/siouv-table.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    NgxPaginationModule
  ],
  declarations: [
    SiouvTableComponent
  ],
 exports: [
    SiouvTableComponent
  ],
})
export class SiouvTableModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SiouvTableModule
    };
  }
}
