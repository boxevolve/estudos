import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxPaginationModule } from 'ngx-pagination';

import { ClassificacaoComponent } from './classificacao.component';
import { LabelModule } from '../label/label.module';
import { FormsModule, NgControl } from '@angular/forms';

@NgModule({
	imports: [CommonModule, NgxPaginationModule, FormsModule, LabelModule],
	declarations: [ClassificacaoComponent],
	exports: [ClassificacaoComponent, NgxPaginationModule],
})
export class ClassificacaoModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: ClassificacaoModule
		};
	}
}
