import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

import { MessageService } from 'app/shared/components/messages/message.service';
import { LoadingModalComponent as Loading } from 'app/shared/components/loading/loading-modal.component';
import { BaseComponent } from './base.component';
import { AcaoSistema } from './acao.sistema';
import { Entity } from 'app/arquitetura/shared/models/entity';

export class FormComponent<T extends Entity> extends BaseComponent implements OnInit {
	public acao: AcaoSistema;
	public form: FormGroup;
	public id: number;

	// mensagens de sucesso
	public mensagemCriacaoSucesso = 'Registro adicionado com sucesso';
	public mensagemAlteracaoSucesso = 'Registro alterado com sucesso';

	constructor(
		protected crudService: any,
		protected router: Router,
		protected route: ActivatedRoute,
		protected fb: FormBuilder,
		protected messageService: MessageService
	) {
		super(messageService);
	}

	public ngOnInit(): void {
		this.id = this.route.snapshot.params['id'];

		if (this.id !== undefined) {
			Loading.start();
			this.crudService.get(this.id).subscribe(
				entidade => {
					Loading.stop();
					if (entidade.data !== undefined) {
						this.form.setValue(entidade.data);
						this.inicializarEntidade(entidade.data);
					} else {
						this.form.setValue(entidade);
						this.inicializarEntidade(entidade);
					}
				},
				error => {
					Loading.stop();
					console.log('Registro não localizado');
					console.log(error);
				}
			);
		}

		// verifica se o usuário está visualizando
		this.acao = new AcaoSistema(this.route);
		if (this.acao.isAcaoVisualizar()) {
			Object.keys(this.form.controls).forEach(key => {
				this.form.get(key).disable();
			});
		}
	}

	protected inicializarEntidade(entidade: T): void {
	}

	/**
	 * Inclui um registro
	 */
	public save() {
		Loading.start();
		if (this.id === undefined) {
			this.crudService.post(this.form.value).subscribe(
				res => {
					Loading.stop();
					this.messageService.addMsgSuccess(this.mensagemCriacaoSucesso);
					this.redirectUrl();
				},
				error => {
					Loading.stop();
					this.messageService.addMsgDanger(this.trataMsgErroApi(error));
				}
			);
		} else {
			this.crudService.put(this.form.value).subscribe(
				res => {
					Loading.stop();
					this.messageService.addMsgSuccess(this.mensagemAlteracaoSucesso);
					this.redirectUrl();
				},
				error => {
					Loading.stop();
					this.messageService.addMsgDanger(this.trataMsgErroApi(error));
				}
			);
		}
	}

	/**
	 * Recupera o parametro redirect da rota atual se existir
	 */
	private getRedirect(snapshot) {
		let redirect = false;
		if (snapshot.data !== undefined) {
			if (snapshot.data.redirect !== undefined) {
				redirect = snapshot.data.redirect;
			}
		}
		return redirect;
	}

	/**
	 * Realiza o redirect para uma rota específica
	 */
	private redirectUrl() {
		const redirect = this.getRedirect(this.route.snapshot);
		if (redirect) {
			this.router.navigate([redirect]);
		}
	}
}
