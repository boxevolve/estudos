import { Component, } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { Situacao } from 'app/shared/models/situacao';
import { Natureza } from 'app/shared/models/natureza';
import { ClassificacaoFiltro } from 'app/shared/models/classificacao-ocorrencia-filtro';
import { BaseComponent } from 'app/shared/components/base.component';
import { TipoOcorrenciaService } from 'app/shared/services/manutencao/tipo-ocorrencia.service';
import { TipoOcorrencia } from 'app/shared/models/tipo-ocorrencia';
import { TipoDirecionamento } from 'app/shared/models/tipo-direcionamento';
import { TipoDirecionamentoService } from 'app/shared/services/manutencao/tipo-direcionamento.service';
import { ClassificacaoService } from 'app/shared/services/manutencao/classificacao.service';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';

@Component({
  selector: 'app-consultar-classificacao-ocorrencia',
  templateUrl: './consultar-classificacao-ocorrencia.component.html',
  styleUrls: ['./consultar-classificacao-ocorrencia.component.css']
})
export class ConsultarClassificacaoOcorrenciaComponent extends BaseComponent {
  formulario: FormGroup;
  situacoes: Situacao[];
  naturezas: Natureza[];
  classOcorrencia: ClassificacaoFiltro;
  ocorrencias: TipoOcorrencia[];
  direcionamentos: TipoDirecionamento[];

  nametipoOcorrencia: 'tipoOcorrencia';
  nameNatural: 'natural';
  nameAtiva: 'ativa';
  namePalavraChave: 'palavraChave';
  nameTipoDirecionamento: 'tipoDirecionamento';

  operacoes = [
    { id: 1, descricao: 'Assunto' },
    { id: 2, descricao: 'Item' },
    { id: 3, descricao: 'Motivo' }
  ];
  dataList: SiouvTable;
  constructor(
    private formBuilder: FormBuilder,
    private comumService: ComumService,
    protected messageService: MessageService,
    private tipoOcorrenciaService: TipoOcorrenciaService,
    private router: Router,
    private route: ActivatedRoute,
    private naturezaService: NaturezaService,
    private direcionamentoService: TipoDirecionamentoService,
    private classficacaoService: ClassificacaoService
  ) {
    super(messageService);
    this.classOcorrencia = new ClassificacaoFiltro();
    this.buscarSituacoes();
    this.buscarNaturezas();
    this.buscarOcorrencia();
    this.buscarTipoDirecionamento();
    this.criarFormulario();
    this.setValoresPadrao();
  }
  criarFormulario() {
    this.formulario = this.formBuilder.group({
      tipoOcorrencia: [this.classOcorrencia.tipoOcorrencia],
      operacoes: [this.classOcorrencia.operacoes, Validators.required],
      palavraChave: [[this.classOcorrencia.palavraChave], [Validators.required]],
      natural: [this.classOcorrencia.natural],
      tipoDirecionamento: [this.classOcorrencia.tipoDirecionamento],
      ativa: [this.classOcorrencia.ativa]
    });
  }

  buscarSituacoes() {
    this.comumService.listarSituacoes().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger(
          'Ocorreu um erro ao pesquisar situações.'
        );
        console.log('Erro ao consultar situações:', error);
      }
    );
  }

  buscarNaturezas() {
    this.naturezaService.get().subscribe(
      (naturezas: Natureza[]) => {
        this.naturezas = naturezas;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as naturezas.');
        console.log('Erro ao recuperar as naturezas:', error);
      }
    );
  }

  buscarOcorrencia() {
    this.tipoOcorrenciaService.get().subscribe(
      (ocorrencias: TipoOcorrencia[]) => {
        this.ocorrencias = ocorrencias;
      }
    );
  }

  buscarTipoDirecionamento() {
    this.direcionamentoService.get().subscribe(
      (dir: TipoDirecionamento[]) => {
        this.direcionamentos = dir;
      }
    );
  }

  setValoresPadrao() {
    this.formulario.get('ativa').patchValue('T');
  }

  consultar() {
    if (this.formulario.invalid) {
      this.messageService.addMsgDanger('Campo ' + (this.formulario.get('palavraChave').invalid ? 'Palavra chave' : 'Consulta Por') + ' é obrigatorio');
      return;
    }

    this.classOcorrencia = this.getClone(this.formulario.value);

    this.classficacaoService.consultarPorFiltros(this.formulario.value).subscribe(
      (result) => {
        console.log(result);
        this.dataList = this.createSiouvTable(result);
      });
  }
  limpar() {
    this.formulario.reset();
    this.classOcorrencia = new ClassificacaoFiltro();
    this.setValoresPadrao();
  }
  createSiouvTable(list): SiouvTable {
    const idTable = 'consultaUnidadeSndc';
    const creator: SiouvTableCreator<any> = new SiouvTableCreator();
    const headers = [
      creator.addHeader('assunto', 'Assunto', 25, 'text-center', { 'vertical-align': 'middle' }),
      creator.addHeader('item', 'Item', 25, 'text-center', { 'vertical-align': 'middle' }),
      creator.addHeader('motivo', 'Motivo', 25, 'text-center', { 'vertical-align': 'middle' }),
      creator.addHeader('tipoOcorrência', 'Tipo de Ocorrência', 25, 'text-center', { 'vertical-align': 'middle' }),
      creator.addHeader('situacao', 'Situação', 25, 'text-center', { 'vertical-align': 'middle' }),
      creator.addHeader('natureza', 'Natureza', 25, 'text-center', { 'vertical-align': 'middle' }),
      creator.addHeader('tipoDirecionamento', 'Tipo de Direcionamento', 25, 'text-center', { 'vertical-align': 'middle' }),
      creator.addHeader('acao', 'Ação', 25, 'text-center', { 'vertical-align': 'middle' }),
    ];

    const rows = [];

    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'assunto', object.itemAssunto.noAssunto, 'text-center', null, object),
        creator.addCol(idTable, index, 'item', object.itemAssunto.noItemAssunto, 'text-center', null, object),
        creator.addCol(idTable, index, 'motivo', object.motivo.nome, 'text-center', null, object),
        creator.addCol(idTable, index, 'tipoOcorrência', object.tipoOcorrencia.nome, 'text-center', null, object),
        creator.addCol(idTable, index, 'ativo', object.ativa === 'S' ? 'Ativa' : 'Inativa' , 'text-center', null, object),
        creator.addCol(idTable, index, 'natureza', object.natureza, 'text-center', null, object),
        creator.addCol(idTable, index, 'tipoDirecionamento', object.tipoDirecionamento.nome, 'text-center', null, object)
      ];
      rows.push(creator.addRow(idTable, index, cols, null, null, list[index]));
    }


    // Table
    // Id - Cabeçalhos - Linhas - Class - Style - Lista
    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }


}
