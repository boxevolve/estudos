import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Util } from 'app/arquitetura/shared/util/util';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { Situacao } from 'app/shared/models/situacao';
import { Assunto } from 'app/shared/models/assunto';
import { Natureza } from 'app/shared/models/natureza';
import { NaturezaService } from 'app/shared/services/manutencao/natureza.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Origem } from 'app/shared/models/origem';
import { SimNao } from 'app/shared/models/sim-nao';
import { TipoOcorrencia } from 'app/shared/models/tipo-ocorrencia';
import { OrigemService } from 'app/shared/services/manutencao/tipo-origem.service';
import { TipoOcorrenciaService } from 'app/shared/services/manutencao/tipo-ocorrencia.service';
import { TipoOcrnaOrigem } from 'app/shared/models/tipo-ocrna-origem';
import { TipoOcrnaOrigemId } from 'app/shared/models/tipo-ocrna-origem-id';
import { PrazoOrigemNatureza } from 'app/shared/models/prazo-origem-natureza';
import { PrazoOrigemNaturezaId } from 'app/shared/models/prazo-origem-natureza-id';

@Component({
    selector: 'app--origem-cadastro',
    templateUrl: './origem-cadastro.component.html',
    styleUrls: ['./origem-cadastro.component.css']
  })
  export class OrigemCadastroComponent extends BaseComponent {
/**
* Nomes dos formControlName dos campos
*/
nameOrigem = 'nome';
nameTipoOcorrencia = 'tiposOcorrenciaOrigem';
nameSituacao = 'ativo';
nameSolicitarProcedencia = 'procedencia';
nameSolicitarNumeroRegistro = 'rar';
nameSolicitarNumeroProcon = 'procon';
nameAcordoAudiencia = 'acordo';
nameSolicitarAvaliacaoConciliacao = 'conciliado';
nameSolicitarValorIndenizacao = 'indenizacao';
nameSolicitarUniRespConciliacao = 'unidadeConciliado';

formulario: FormGroup;
origem: Origem;
tituloOcorrenciaInterna: string = null;
assunto: Assunto = null;
assuntos: Assunto[] = null;
situacoes: Situacao[] = [];
simNao: SimNao[] = [];
manterOrigem: Situacao = null;
titulo: String = 'Incluir';
tituloBotao: String = 'inclusão';
natureza: Natureza;
prazoOrigemNatureza: PrazoOrigemNatureza;
prazosOrigemNatureza: PrazoOrigemNatureza[][] = [];
prazosOrigemNaturezaSelecionados: PrazoOrigemNatureza[][] = [];

naturezas: Natureza[] = null;
tiposOcorrencia: TipoOcorrencia[] = [];
tiposOcorrenciaTouched: Boolean = false;

  constructor(protected messageService: MessageService,
    private formBuilder: FormBuilder,
    private comumService: ComumService,
    private tipoOcorrenciaService: TipoOcorrenciaService,
    private naturezaService: NaturezaService,
    private origemService: OrigemService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    super(messageService);
    this.origem = new Origem();
    this.carregarSituacoes();
    this.carregarSimNao();
    this.carregarNaturezas();
    this.carregarTiposOcorrencia();
    this.createFormGroup();
    this.editar();
  }

  carregarTiposOcorrencia(): void {
    this.tipoOcorrenciaService.listarTiposOcorrenciaSemAcompanhamento((tiposOcorrencia: TipoOcorrencia[]) => {
      this.tiposOcorrencia = tiposOcorrencia;
    });
  }

  carregarSimNao(): void {
    this.comumService.listarSimNao().subscribe(
      (simNao: SimNao[]) => {
        this.simNao = simNao;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
        console.log('Erro ao consultar situações:', error);
      }
    );
  }

  carregarNaturezas() {
    this.naturezaService.get().subscribe(
      (naturezas: Natureza[]) => {
        this.naturezas = naturezas;
        this.carregarListaPrazo(this.naturezas);
        if (!this.isNew()) {
          this.prazosOrigemNatureza = this.carregarListaPrazoEdicao(this.prazosOrigemNatureza, this.origem.prazoNaturezaOrigem);
        }
    },
    error => {
      this.messageService.addMsgDanger('Ocorreu um erro ao recuperar as naturezas.');
      console.log('Erro ao recuperar as naturezas:', error);
    });
  }

  createFormGroup(): void {
    this.formulario = this.formBuilder.group({
        id: [this.origem.id],
        nome: [this.origem.nome, [ Validators.required ]],
        ativo: [this.origem.ativo, [ Validators.required ]],
        procedencia: [this.origem.procedencia, [ Validators.required ]],
        rar: [this.origem.rar, [ Validators.required ]],
        procon: [this.origem.procon, [ Validators.required ]],
        acordo: [this.origem.acordo, [ Validators.required ]],
        conciliado: [this.origem.conciliado, [ Validators.required ]],
        indenizacao: [this.origem.indenizacao, [ Validators.required ]],
        unidadeConciliado: [this.origem.unidadeConciliado, [ Validators.required ]],
    });
  }

  carregarSituacoes(): void {
    this.comumService.listarSituacoesSemOpcaoTodos().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar situações.');
        console.log('Erro ao consultar situações:', error);
      }
    );
  }

  voltar(): void {
     this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  limpar(): void {
    this.origem = new Origem();
    this.tiposOcorrenciaTouched = false;
    this.formulario.reset();
  }

  salvar(): void {
    this.verificarNaturezaChecada();
    if (this.formulario.valid) {
      if (this.prazosOrigemNaturezaSelecionados.length === 0) {
        this.messageService.addMsgDanger('Campo Natureza é obrigatório.');
        return;
      }

      let valid = true;

      this.prazosOrigemNaturezaSelecionados.forEach(linha => {
        let linhaValida = false;
        linha.forEach(coluna => {
          if (!Util.isEmpty(coluna.pzSolucao)) {
            linhaValida = true;
          }
        });
        if (!linhaValida) {
          valid = false;
          return;
        }
      });

      if (!valid) {
        this.messageService.addMsgDanger('Campo Prazos de Atendimento é obrigatório.');
        return;
      }

    } else {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
    }
    this.montarObj();
    JSON.parse(JSON.stringify(this.origem));
    this.origemService.verificarDuplicidadeRegistro(this.origem).subscribe(
      (isDuplicado: boolean) => {
        if (Util.isEmpty(isDuplicado) ? false : isDuplicado) {
          this.messageService.addMsgDanger('Registro já cadastrado.');
        } else {
          this.finalizarSalvar();
        }
      }
    );
  }

  validarCamposPrazoOcorrencia(): boolean {
    return true;
  }

  verificarNaturezaChecada(): boolean {
      this.prazosOrigemNaturezaSelecionados.forEach(linha => {
        this.prazosOrigemNaturezaSelecionados.forEach(coluna => {
         return true;
        });
      });
    return false;
  }

  finalizarSalvar(): void {
    if (this.isNew()) {
      this.origemService.post(this.origem).subscribe(
          (origem: Origem) => {
            this.router.navigate(['.'], { relativeTo: this.route.parent });
            this.messageService.addMsgSuccess('Origem inserida com sucesso.');
          },
          error => {
            this.messageService.addMsgDanger('Ocorreu um erro ao incluir a origem.');
            console.log('Erro ao incluir origem:', error);
          }
        );
    } else {
      this.origemService.put(this.origem).subscribe(
        (origem: Origem) => {
          this.router.navigate(['.'], { relativeTo: this.route.parent });
          this.messageService.addMsgSuccess('Origem alterada com sucesso.');
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao alterar a origem.');
          console.log('Erro ao alterar o grau satisfação, ocasionado por:', error);
        }
      );
    }
  }

  editar(): void {
    this.route.params.subscribe(
      (params: any) => {
        const id: number = params['id'];
        if (!Util.isEmpty(id)) {
          this.titulo = 'Alterar';
          this.tituloBotao = 'alteração';
          this.origemService.get(id).subscribe(
            (origem: Origem) => {
              this.origem = origem;
              for (let index = 0; index < this.origem.tiposOcorrenciaOrigem.length; index++) {
                  this.origem.tiposOcorrenciaOrigem[index] =
                  this.newTipoOcrnaOrigem(this.origem, this.origem.tiposOcorrenciaOrigem[index].tipoOcorrencia);
              }
              this.createFormGroup();
            },
            error => {
                this.messageService.addMsgDanger('Ocorreu um erro ao carregar a origem.');
                console.log('Erro ao carregar origem:', error);
            }
          );
        }
      });
  }

  montarObj(): void {
    this.salvarPrazos();
    const tiposOcorrenciaOrigem = this.origem.tiposOcorrenciaOrigem;
    const prazosNaturezaOrigem = this.origem.prazoNaturezaOrigem;
    this.origem = this.getClone(this.formulario.value);
    this.origem.tiposOcorrenciaOrigem = tiposOcorrenciaOrigem;
    this.origem.prazoNaturezaOrigem = prazosNaturezaOrigem;
  }

  isNew(): boolean {
    return Util.isEmpty(this.origem.id);
  }

  isCheckTiposOcorrencia(tipo: TipoOcorrencia): boolean {

    if(!this.origem.tiposOcorrenciaOrigem) {
      return false;
    }
    for (let index = 0; index < this.origem.tiposOcorrenciaOrigem.length; index++) {
      if(this.origem.tiposOcorrenciaOrigem[index].id.tipoOcorrencia === tipo.id) {
          return true;
      }
    }
    return false;
  }

  selectTipoOcorrenciaOrigem(selecionado: TipoOcorrencia): TipoOcrnaOrigem {
    return this.newTipoOcrnaOrigem(this.origem, selecionado);
  }

  newTipoOcrnaOrigem(origem: Origem, tipoOcorrencia: TipoOcorrencia): TipoOcrnaOrigem {
    const tipoOcrnaOrigemId = new TipoOcrnaOrigemId(tipoOcorrencia.id, null);
    return new TipoOcrnaOrigem(tipoOcrnaOrigemId, null, tipoOcorrencia);
  }


  key(id: string, prazo: PrazoOrigemNatureza, indexLinha: number, indexColuna: number): void {
    const campo = $('#' + id);
    const value: number = this.onlyNumber(campo.val());
    campo.val(value);
    this.prazosOrigemNatureza[indexLinha][indexColuna].pzSolucao = value;
  }

  onlyNumber(value: string): number {
    return +value.replace(/[^\d]+/g, '');
  }

  getNomeNatureza(listaPrazo: PrazoOrigemNatureza[]): string {
    if (!Util.isEmpty(listaPrazo) && listaPrazo.length > 0) {
      return listaPrazo[0].natureza.nome;
    }
  }

  somenteNumeros(event: any): boolean {
    const pattern = /[0-9\+\-\ ]/;

    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
      return false;
    }
    return true;
  }

  carregarListaPrazo(listaNatureza: Natureza[]): void {
    listaNatureza.forEach(natureza => {
      const prazosOrigemNaturezaItem: PrazoOrigemNatureza[] = [];
      this.tiposOcorrencia.forEach(tipo => {
        const id = new PrazoOrigemNaturezaId(natureza.id, null, tipo.id);
        const prazo = new PrazoOrigemNatureza(id, null, null, natureza);
        prazosOrigemNaturezaItem.push(prazo);
      });
      this.prazosOrigemNatureza.push(prazosOrigemNaturezaItem);
    });
  }

  carregarListaPrazoEdicao(listaPrazos: PrazoOrigemNatureza[][],
    listaPrazosSalvos: PrazoOrigemNatureza[]): PrazoOrigemNatureza[][] {
    for (let i = 0; i < listaPrazos.length; i++) {
      const prazos = listaPrazos[i];
      for (let j = 0; j < prazos.length; j++) {
        const prazo = prazos[j];
        for (let k = 0; k < listaPrazosSalvos.length; k++) {
          const salvo = listaPrazosSalvos[k];
          if (prazo.id.nuNatureza === salvo.id.nuNatureza
           && prazo.id.nuTipoOcorrencia === salvo.id.nuTipoOcorrencia) {
            listaPrazos[i][j].pzSolucao = salvo.pzSolucao;
            this.prazosOrigemNaturezaSelecionados.push(listaPrazos[i]);
            break;
          }
        }
      }
    }
    return listaPrazos;
  }

  isCheckedTeste(indexRow: number): boolean {

  for (let j = 0; j < this.prazosOrigemNatureza[indexRow].length; j++) {
    if (!Util.isEmpty(this.prazosOrigemNatureza[indexRow][j].pzSolucao)) {
      return true;
    }
  }
    return false;
  }

  isCheckNatureza(indexRow: number): boolean {
      for (let j = 0; j < this.prazosOrigemNatureza[indexRow].length; j++) {
        if (!Util.isEmpty(this.prazosOrigemNatureza[indexRow][j].pzSolucao)) {
            this.adicionarPrazosSelecionados(this.prazosOrigemNatureza[indexRow]);
            return true;
        }
      }
      return true;
  }

  adicionarPrazosSelecionados(lista: PrazoOrigemNatureza[]): boolean {
    this.prazosOrigemNaturezaSelecionados.forEach(linha => {
        if (lista !== linha) {
          this.prazosOrigemNaturezaSelecionados.push(lista);
        }
    });

    return false;
  }
  salvarPrazos(): void {
    this.origem.prazoNaturezaOrigem = [];
    this.prazosOrigemNaturezaSelecionados.forEach(matriz => {
      matriz.forEach(prazo => {
        if (!Util.isEmpty(prazo.pzSolucao)) {
            this.origem.prazoNaturezaOrigem.push(prazo);
        }
      });
    });
  }

  changeTouched(value: any, isChecked: boolean, list: Array<any>): void {
    this.tiposOcorrenciaTouched = true;
    this.onChangeCheckbox(value, isChecked, list);
  }

  changeTouchedNatureza(value: any, isChecked: boolean, list: Array<any>): void {
    this.tiposOcorrenciaTouched = true;
    if (isChecked) {
      list.push(value);
    } else {
      const index = list.findIndex(x => this.equals(x, value));
      if (index !== -1) {
        list.splice(index, 1);
      }
    }
    this.prazosOrigemNaturezaSelecionados = list;
  }

  isChecked(): boolean {
    return Util.convertSNtoBool(this.origem.tipoOcorrencia);
  }

  getValue(indexRow: number, indexCol: number): any {
    const value = this.prazosOrigemNatureza[indexRow][indexCol].pzSolucao;
    return value ? value : '';
  }

  irHome(): void {
    this.router.navigate(['/home']);
  }

  irManterOrigem(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

}
