import { Component, Input } from '@angular/core';
import { BaseComponent } from '../../base.component';
import { MessageService } from '../../messages/message.service';

@Component({
  selector: 'app-required-label',
  templateUrl: './required-label.component.html',
  styleUrls: ['./required-label.component.css']
})
export class RequiredLabelComponent extends BaseComponent {

    @Input() title: string;

    constructor(
        protected messageService: MessageService,
    ) {
        super(messageService);
        this.title = '';
    }

}
