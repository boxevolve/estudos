export class TipoOcrnaOrigemId {
    constructor (
        public tipoOcorrencia: number,
        public tipoOrigem: number,
    ) {}
}