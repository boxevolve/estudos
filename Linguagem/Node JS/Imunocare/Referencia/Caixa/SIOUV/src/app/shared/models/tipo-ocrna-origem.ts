import { TipoOcrnaOrigemId } from "./tipo-ocrna-origem-id";
import { Origem } from "./origem";
import { TipoOcorrencia } from "./tipo-ocorrencia";

export class TipoOcrnaOrigem {
    constructor(
        public id: TipoOcrnaOrigemId,
        public tipoOrigem: Origem,
        public tipoOcorrencia: TipoOcorrencia
    ) {}
}