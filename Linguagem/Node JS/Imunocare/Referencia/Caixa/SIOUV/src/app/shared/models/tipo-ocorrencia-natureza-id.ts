export class TipoOcorrenciaNaturezaId {
    constructor (
        public natureza: number,
	    public tipoOcorrencia: number
    ) {}
}
