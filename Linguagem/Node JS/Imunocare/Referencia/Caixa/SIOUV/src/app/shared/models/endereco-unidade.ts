export class EnderecoUnidade  {
    constructor (
        public noLocalidade: string,
        public noBairro: string,
        public noLogradouro: string
    ) {}
}
