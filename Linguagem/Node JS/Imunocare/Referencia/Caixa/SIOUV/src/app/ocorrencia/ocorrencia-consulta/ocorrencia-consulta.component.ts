import { Component, OnInit, Input, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Util } from 'app/arquitetura/shared/util/util';
import { BaseComponent } from 'app/shared/components/base.component';
import { Classificacao } from 'app/shared/components/classificacao/classificacao';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Ocorrencia } from 'app/shared/models/ocorrencia';
import { OcorrenciaService } from 'app/shared/services/ocorrencia/ocorrencia.service';
import { Origem } from 'app/shared/models/origem';
import { Natureza } from 'app/shared/models/natureza';
import { Assunto } from 'app/shared/models/assunto';
import { Item } from 'app/shared/models/item';
import { Motivo } from 'app/shared/models/motivo';
import { ParametrosConsultaOcorrencia } from './parametros-consulta-ocorrencia';
import { TipoOcorrencia } from 'app/shared/models/tipo-ocorrencia';
import { Uf } from 'app/shared/models/uf';
import { SimNao } from 'app/shared/models/sim-nao';
import { TransferObject } from 'app/shared/util/trasfer-object';

@Component({
    selector: 'app-ocorrencia-consulta',
    templateUrl: './ocorrencia-consulta.component.html',
    styleUrls: ['./ocorrencia-consulta.component.css']
})
export class OcorrenciaConsultaComponent extends BaseComponent implements OnInit {

    titulo: string = null;
    escondePesquisaAvancada: boolean;
    formulario: FormGroup;
    classificacao: Classificacao;
    ocorrencia: Ocorrencia;
    @Input('tipoOcorrencia')
    tipoOcorrencia: number;
    parametros: ParametrosConsultaOcorrencia
    origens: Origem[];
    naturezas: Natureza[];
    assuntos: Assunto[];
    itens: Item[];
    motivos: Motivo[];
    tiposOcorrencia: TipoOcorrencia[];
    ufs: Uf[];
    unidadeDestinoSelecionada: any;
    pesquisaAvancada: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        protected messageService: MessageService,
        private ocorrenciaService: OcorrenciaService,
        private transferObject: TransferObject
    ) {
        super(messageService);

        this.titulo = 'Consulta';
        this.route.params.subscribe(
            (params: any) => {
                const id: number = params['id'];

                if (!Util.isEmpty(id)) {
                    this.titulo = 'Editar';
                    this.ocorrenciaService.get(id).subscribe(
                        (ocorrencia: Ocorrencia) => {
                            this.ocorrencia = ocorrencia;
                        },
                        error => {
                            this.messageService.addMsgDanger('Ocorreu um erro ao carregar o assunto.');
                            console.log('Erro ao carregar assunto:', error);
                        });
                }
            });
    }

    ngOnInit() {
        this.init();
        this.configurarTiposOcorrencia();
        this.configurarUFs();
    }

    private init(): void {
        this.parametros = new ParametrosConsultaOcorrencia();
        this.parametros.classificacao = new Classificacao();
        this.escondePesquisaAvancada = true;
        this.parametros.pesquisaAvancada = false;
        this.initFormulario();
    }

    mahChange(): void {
        console.log(this.parametros.ufRDR.noUf);
    }

    initFormulario(): void {
        this.formulario = this.formBuilder.group({
            tipoOcorrencia: [this.parametros.classificacao.idTipoOcorrencia, [Validators.required]],
            tipoDemanda: [this.parametros.tipoDemanda, [Validators.required]],
            cpf: [this.parametros.cpf],
            cnpj: [this.parametros.cnpj],
            nroNIS: [this.parametros.nroNIS],
            matriculaSolicitante: [this.parametros.matriculaSolicitante],
            nroOcorrencia: [this.parametros.nroOcorrencia],
            nroPreOcorrencia: [this.parametros.nroPreOcorrencia],
            nroProtocoloSAC: [this.parametros.nroProtocoloSAC],
            nroRDR: [this.parametros.nroRDR],
            ufRDR: [this.parametros.ufRDR],
            dtInicioPeriodo: [this.parametros.dtInicioPeriodo],
            dtFimPeriodo: [this.parametros.dtFimPeriodo],
            pesquisaAvancada: [this.pesquisaAvancada],
            nomeSolicitanteExterno: [this.parametros.nomeSolicitanteExterno],
            unidadeDestinoSelecionada: [this.unidadeDestinoSelecionada],
            unidadesDestino: [this.parametros.unidadesDestino],
            origem: [this.parametros.classificacao.idOrigem],
            natureza: [this.parametros.classificacao.idNatureza],
            assunto: [this.parametros.classificacao.idAssunto],
            item: [this.parametros.classificacao.idItem],
            motivo: [this.parametros.classificacao.idMotivo],
            situacao: [this.parametros.situacao],
            grauSatisfacao: [this.parametros.grauSatisfacao],
            dtVencimento: [this.parametros.dtVencimento],
            nroProcon: [this.parametros.nroProcon],
            ufProcon: [this.parametros.ufProcon],
            localidadeProcon: [this.parametros.localidadeProcon],
            operador: [this.parametros.codOperador],
            pendente: [this.parametros.pendente],
        });
        console.log('formulario inicializado.');
    }

    isNew(): boolean {
        return Util.isEmpty(this.ocorrencia.id);
    }

    configurarTiposOcorrencia(): void {
        this.ocorrenciaService.callListarTiposOcorrenciaSemAcompanhamento(
            (tiposOcorrencia: TipoOcorrencia[]) => {
                this.tiposOcorrencia = tiposOcorrencia;
            });
    }

    configurarUFs(): void {
        this.ocorrenciaService.callListarUFs((ufs: Uf[]) => this.ufs = ufs);
    }

    public configurarClassificacaoForm() {
        console.log('configurando origens...');
        var idTipoOcorrencia = this.parametros.classificacao.idTipoOcorrencia;
        this.ocorrenciaService.callListarOrigensPorTipoOcorrencia(idTipoOcorrencia, (origens: Origem[]) =>
            this.origens = origens);
        console.log('configurando naturezas...');
        this.ocorrenciaService.callListarNaturezasPorTipoOcorrenciaAndTipoOrigem(idTipoOcorrencia, 193, (naturezas: Natureza[]) => {
            this.naturezas = naturezas;
        });
        console.log('configurando assuntos...');
        this.ocorrenciaService.callListarAssuntosPorNatureza(1, (assuntos: Assunto[]) => {
            this.assuntos = assuntos;
        });

        console.log("configurando motivos...")
        this.ocorrenciaService.callListarMotivosPorTipoOcorrenciaItemAssunto(7, 7, (motivos: Motivo[]) => {
            this.motivos = motivos;
        }
        );

        if (this.parametros.pesquisaAvancada) {
            this.ocorrenciaService.callListarOrigensPorTipoOcorrencia(idTipoOcorrencia, (origens: Origem[]) =>
                this.origens = origens);
        }
    }

    changePesquisaAvancada() {
        console.log('pesquisaAvancada: ' + this.parametros.pesquisaAvancada);
        if (!this.parametros.pesquisaAvancada) {
            this.escondePesquisaAvancada = true;
            console.log('hide true: ' + this.escondePesquisaAvancada);
        } else {
            this.escondePesquisaAvancada = false;
            console.log('hide false: ' + this.escondePesquisaAvancada);
        }
    }

    public escondeTabelaUnidadesSelecionadas(): boolean {
        return this.parametros.unidadesDestino.length == 0;
    }

    consultar() {
        this.configureParamsFromForm();
        this.transferObject.storage = this.parametros;
        this.router.navigate(['consulta/resultado'], { relativeTo: this.route.parent });
    }

    configureParamsFromForm(): void {
        //this.configurarClassificacao();
        //this.configuraParametros();

    }
    configuraParametros(): any {
        this.parametros.tipoDemanda = this.getForm().tipoDemanda;
        this.parametros.cpf = this.getForm().cpf;
        this.parametros.cnpj = this.getForm().cnpj;
        this.parametros.nroNIS = this.getForm().nroNIS;
        this.parametros.matriculaSolicitante = this.getForm().matriculaSolicitante;
        this.parametros.nroOcorrencia = this.getForm().nroOcorrencia;
        this.parametros.nroPreOcorrencia = this.getForm().nroPreOcorrencia;
        this.parametros.nroProtocoloSAC = this.getForm().nroProtocoloSAC;
        this.parametros.nroRDR = this.getForm().nroRDR;
        this.parametros.dtInicioPeriodo = this.getForm().dtInicioPeriodo;
        this.parametros.dtFimPeriodo = this.getForm().dtFimPeriodo;
        this.parametros.pesquisaAvancada = this.isPesquisaAvancada();
    }

    isPesquisaAvancada(): boolean {
        return 'S' == this.getForm().pesquisaAvancada ? true : false;
    }
    getForm(): any {
        return this.formulario.value;
    }
    configurarClassificacao(): void {
        this.parametros.classificacao.idTipoOcorrencia = this.formulario.value.tipoOcorrencia;
        this.parametros.classificacao.idOrigem = this.formulario.value.origem;
        this.parametros.classificacao.idNatureza = this.formulario.value.natureza;
        this.parametros.classificacao.idAssunto = this.formulario.value.assunto;
        this.parametros.classificacao.idItem = this.formulario.value.item;
        this.parametros.classificacao.idMotivo = this.formulario.value.motivo;

    }
}
