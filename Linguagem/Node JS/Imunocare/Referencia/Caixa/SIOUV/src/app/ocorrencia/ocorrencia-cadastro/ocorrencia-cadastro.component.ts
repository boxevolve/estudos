import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Util } from 'app/arquitetura/shared/util/util';
import { BaseComponent } from 'app/shared/components/base.component';
import { Classificacao } from 'app/shared/components/classificacao/classificacao';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Ocorrencia } from 'app/shared/models/ocorrencia';
import { OcorrenciaService } from 'app/shared/services/ocorrencia/ocorrencia.service';
import { Origem } from 'app/shared/models/origem';
import { Natureza } from 'app/shared/models/natureza';
import { Assunto } from 'app/shared/models/assunto';
import { Item } from 'app/shared/models/item';
import { Motivo } from 'app/shared/models/motivo';

@Component({
    selector: 'app-ocorrencia-cadastro',
    templateUrl: './ocorrencia-cadastro.component.html',
    styleUrls: ['./ocorrencia-cadastro.component.css']
})
export class OcorrenciaCadastroComponent extends BaseComponent implements OnInit {

    origens: Origem[];
    naturezas: Natureza[];
    assuntos: Assunto[];
    itens: Item[];
    motivos: Motivo[];

    formulario: FormGroup;
    titulo: string = null;
    classificacao: Classificacao;
    ocorrencia: Ocorrencia;
    @Input('tipoOcorrencia')
    tipoOcorrencia: number;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        protected messageService: MessageService,
        private ocorrenciaService: OcorrenciaService
    ) {
        super(messageService);
        this.ngOnInit();
        this.titulo = 'Incluir';
        this.route.params.subscribe(
            (params: any) => {
                const id: number = params['id'];

                if (!Util.isEmpty(id)) {
                    this.titulo = 'Editar';
                    this.ocorrenciaService.get(id).subscribe(
                        (ocorrencia: Ocorrencia) => {
                            this.ocorrencia = ocorrencia;
                        },
                        error => {
                            this.messageService.addMsgDanger('Ocorreu um erro ao carregar o assunto.');
                            console.log('Erro ao carregar assunto:', error);
                        });
                }
            });
    }

    ngOnInit() {
        this.initProperties();
        
        this.formulario = this.formBuilder.group({
            solicitante: [this.ocorrencia.solicitante, [Validators.required]],
            prazoSolucao: [this.ocorrencia.prazoSolicitado, [Validators.required]],
            manifesto: [this.ocorrencia.descManifesto, [Validators.required]],
            origem: [this.classificacao.idOrigem, [Validators.required]],
            natureza: [this.classificacao.idNatureza, [Validators.required]],
            assunto: [this.classificacao.idAssunto, [Validators.required]],
            item: [this.classificacao.idItem, [Validators.required]],
            motivo: [this.classificacao.idMotivo, [Validators.required]]
        });
        this.carregarOrigensPorTipoOcorrencia();
    }
    private initProperties(): void {
        this.ocorrencia = new Ocorrencia();
        this.ocorrencia.solicitante = null;
        this.ocorrencia.prazoSolicitado = null;
        this.ocorrencia.descManifesto = null;

        this.classificacao = new Classificacao();
        this.classificacao.idOrigem = null;
        this.classificacao.idNatureza = null;
        this.classificacao.idAssunto = null;
        this.classificacao.idItem = null;
        this.classificacao.idMotivo = null;
    }

    isNew(): boolean {
        return Util.isEmpty(this.ocorrencia.id);
    }

    salvar() {
        this.configurarClassificacaoOcorrencia();
        if (this.formulario.valid) {
            if (this.isNew()) {
                this.ocorrenciaService.post(this.ocorrencia).subscribe(
                    (ocorrencia: Ocorrencia) => {
                        this.router.navigate(['.'], { relativeTo: this.route.parent });
                        this.messageService.addMsgSuccess('Ocorrência inserida com sucesso.');
                    },
                    error => {
                        this.messageService.addMsgDanger('Ocorreu um erro ao incluir a ocorrência.');
                        console.log('Erro ao incluir ocorrência:', error);
                    });
            }
            else {
                this.ocorrenciaService.put(this.ocorrencia).subscribe(
                    (ocorrencia: Ocorrencia) => {
                        this.router.navigate(['.'], { relativeTo: this.route.parent });
                        this.messageService.addMsgSuccess('Ocorrência alterada com sucesso.');
                    },
                    error => {
                        this.messageService.addMsgDanger('Ocorreu um erro ao alterar a ocorrência.');
                        console.log('Erro ao alterar assunto:', error);
                    });
            }
        } else {
            this.messageService.addMsgDanger('Preencha os campos obrigatórios.');
        }
    }
    private configurarClassificacaoOcorrencia(): void {
        if (this.classificacao != null
            && this.classificacao != undefined) {
            this.ocorrencia.tipoOrigem = this.classificacao.idOrigem;
            this.ocorrencia.natureza = this.classificacao.idNatureza;
            this.ocorrencia.itemAssunto = this.classificacao.idItem;
            this.ocorrencia.motivo = this.classificacao.idMotivo;
        }
    }

    limpar() {
        this.initProperties();
        this.formulario.reset();
    }

    voltar() {
        this.limpar();
        this.router.navigate(['.'], { relativeTo: this.route.parent });
    }

    public setClassificacao(classificacao: Classificacao): void {
        this.classificacao = classificacao;
    }

    private carregarOrigensPorTipoOcorrencia() {
        console.log(this.tipoOcorrencia);
        this.origens = this.ocorrenciaService.carregarOrigensPorTipoOcorrencia(this.tipoOcorrencia);
    }

    public carregarNaturezasPorOrigem() {
        console.log(this.formulario.value.origem);
        this.naturezas = this.ocorrenciaService.carregarNaturezasPorOrigem(this.classificacao.idOrigem);
    }

    public carregarAssuntosPorNatureza() {
        console.log(this.formulario.value.natureza);
        this.assuntos = this.ocorrenciaService.carregarAssuntosPorNatureza(this.classificacao.idNatureza);
    }

    public carregarItensPorAssunto() {
        console.log(this.formulario.value.assunto);
        this.itens = this.ocorrenciaService.carregarItensPorAssunto(this.classificacao.idAssunto);
    }

    public carregarMotivosPorItem() {
        console.log(this.formulario.value.item);
        this.motivos = this.ocorrenciaService.carregarMotivosPorItem(this.classificacao.idItem);
    }

    public selecionarClassificacao(): void {
        //this.configurarClassificacao.emit(this.classificacao);
    }
}
