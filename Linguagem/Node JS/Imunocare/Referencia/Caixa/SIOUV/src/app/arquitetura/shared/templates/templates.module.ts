import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Aplicação
import { CabecalhoPadraoComponent } from './cabecalho-padrao.component';
import { RodapePadraoComponent } from './rodape-padrao.component';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada.component';

/**
 * Modulo Acesso
 **/
@NgModule({
	imports: [
		CommonModule,
		RouterModule
	],
	declarations: [
		CabecalhoPadraoComponent,
		RodapePadraoComponent,
		PaginaNaoEncontradaComponent
	],
	entryComponents: [
		CabecalhoPadraoComponent,
		RodapePadraoComponent
	]
})
export class TemplatesModule { }
