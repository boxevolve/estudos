import { Injectable } from '@angular/core';
import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { UnidadeSemOcorrencia } from 'app/shared/models/unidade-sem-ocorrencia';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { UnidadeSemOcorrenciaId } from 'app/shared/models/unidade-sem-ocorrencia-id';
import { Unidade } from 'app/shared/models/unidade';

@Injectable()
export class UnidadeSemOcorrenciaService extends CrudHttpClientService<UnidadeSemOcorrencia>{

  constructor(protected http: HttpClient) {
    super('manutencao/unidade-sem-ocorrencia' , http);
  }

  todasUnidades(): Observable<any[]> {
    return this.http.get<UnidadeSemOcorrencia[]>(this.url + '/', this.options());
  }

  inserirUnidade(unidade: UnidadeSemOcorrencia): any {
    return this.http.post<UnidadeSemOcorrencia>(this.url + '/', unidade, this.options());
  }

  deleteUnidadeSemOcorrencia(id: UnidadeSemOcorrenciaId): Observable<any> {
    return this.http.delete<UnidadeSemOcorrenciaId>(this.url + '/' + id.nuUnidade + '/' + id.nuNatural , this.options());
  }

  consultarUnidade (id: UnidadeSemOcorrenciaId): Observable<any> {
    return this.http.get<UnidadeSemOcorrenciaId>(this.url + '/consultar/nuUnidade/' + id.nuNatural + '/nuNatural/' + id.nuUnidade , this.options());
  }
}
