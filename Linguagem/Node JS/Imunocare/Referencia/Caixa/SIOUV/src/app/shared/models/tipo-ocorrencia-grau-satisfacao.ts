import { TipoOcorrenciaGrauSatisfacaoId } from "./tipo-ocorrencia-grau-satisfacao-id";
import { GrauSatisfacao } from "./grau-satisfacao";
import { TipoOcorrencia } from "./tipo-ocorrencia";

export class TipoOcorrenciaGrauSatisfacao {
    constructor(
        public id: TipoOcorrenciaGrauSatisfacaoId,
        public grauSatisfacao: GrauSatisfacao,
        public tipoOcorrencia: TipoOcorrencia
    ) {}
}