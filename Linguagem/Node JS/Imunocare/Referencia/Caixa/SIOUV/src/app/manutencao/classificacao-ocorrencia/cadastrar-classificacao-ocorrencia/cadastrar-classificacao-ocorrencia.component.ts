import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Assunto } from 'app/shared/models/assunto';
import { Item } from 'app/shared/models/item';
import { TipoOcorrencia } from 'app/shared/models/tipo-ocorrencia';
import { MessageService } from 'app/shared/components/messages/message.service';
import { TipoOcorrenciaService } from 'app/shared/services/manutencao/tipo-ocorrencia.service';
import { BaseComponent } from 'app/shared/components/base.component';
import { AssuntoService } from 'app/shared/services/manutencao/assunto.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemAssunto } from 'app/shared/models/item-assunto';
import { Classificacao } from 'app/shared/components/classificacao/classificacao';

@Component({
  selector: 'app-cadastrar-classificacao-ocorrencia',
  templateUrl: './cadastrar-classificacao-ocorrencia.component.html',
  styleUrls: ['./cadastrar-classificacao-ocorrencia.component.css']
})
export class CadastrarClassificacaoOcorrenciaComponent extends BaseComponent implements OnInit {

  formulario: FormGroup;
  assuntos: Assunto[] = [];
  itens: Item[] = [];
  tipoOcorrencia: TipoOcorrencia;
  tiposOcorrencia: TipoOcorrencia[] = [];
  itensAssunto: ItemAssunto[] = [];
  classificacao: Classificacao;

  nameTipoOcorrencia = 'nome';
  nameAssunto = 'assunto';
  nameItem = 'item';

  constructor(
              private form: FormBuilder,
              protected messageService: MessageService,
              private tipoOcorrenciaService: TipoOcorrenciaService,
              private assuntoService: AssuntoService,
              private router: Router,
              ) {
                super(messageService);
                this.classificacao = new Classificacao();
               }

  ngOnInit() {
    this.criarFormulario();
    this.carregarTiposOcorrencia();
    this.carregarAssuntos();
  }

  criarFormulario(): void {
    this.formulario = this.form.group({
      nome: [this.classificacao.idTipoOcorrencia],
      assunto: [this.classificacao.idAssunto],
      item: [this.classificacao.idItem],
    });
  }

  carregarTiposOcorrencia(): void {
    this.tipoOcorrenciaService.listarTiposOcorrenciaSemAcompanhamento((tiposOcorrencia: TipoOcorrencia[]) => {
        this.tiposOcorrencia = tiposOcorrencia;
        console.log(this.tiposOcorrencia);
    });
  }

  carregarAssuntos(): void {
    this.assuntoService.carregarAssuntos().subscribe((assuntos: Assunto[]) => {
      this.assuntos = assuntos;
      console.log(assuntos);
    });
  }

  carregaItens(): void {
      const ass: number = this.formulario.get('assunto').value;
      this.assuntos.forEach(element => {
        if (element.id === ass) {
          this.itensAssunto = element.itensAssunto;
        }
      });
  }

  carregarMotivo(): void {
    this.router.navigate(['./motivo']);
  }

  limparFormulario(): void {
    this.formulario.reset();
  }

  inserir(): void {
    this.classificacao = this.formulario.value;
    console.log(this.classificacao);
  }
}
