import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { MessageService } from 'app/shared/components/messages/message.service';
import { SndcService } from 'app/shared/services/manutencao/sndc.service';
import { BaseComponent } from 'app/shared/components/base.component';
import { UnidadeSndc } from 'app/shared/models/sndc';
import { Uf } from 'app/shared/models/uf';
import { Situacao } from 'app/shared/models/situacao';
import { Router, ActivatedRoute } from '@angular/router';
import { UfService } from 'app/shared/services/comum/uf.service';

@Component({
  selector: 'app-sndc-cadastro',
  templateUrl: './sndc-cadastro.component.html',
  styleUrls: ['./sndc-cadastro.component.css']
})
export class SndcCadastroComponent extends BaseComponent {
  sndc: UnidadeSndc;
  formulario: FormGroup;
  ufs: Uf[];
  situacoes: Situacao[];
  id: string;

  /**
	* Nomes dos formControlName dos campos
  */
  nameNoUnidadeSndc: 'noUnidadeSndc';
  namesgUf: 'sgUf';
  nameicAtivo: 'icAtivo';

  constructor(
    private formBuilder: FormBuilder,
    private comumService: ComumService,
    protected messageService: MessageService,
    private sndcService: SndcService,
    private router: Router,
    private route: ActivatedRoute,
    private ufService: UfService
  ) {
    super(messageService);
    this.sndc = new UnidadeSndc();
    this.id = this.route.snapshot.paramMap.get('id');
    this.criarFormulario();
    this.buscarSituacoes();
    this.carregarUfs();

    if (this.id != null) {
      this.sndcService.get(+this.id).subscribe((result: UnidadeSndc) => {
        this.formulario.patchValue(result);
      });
    }
  }

  criarFormulario() {
    this.formulario = this.formBuilder.group({
      id: [this.sndc.id],
      noUnidadeSndc: [this.sndc.noUnidadeSndc, [Validators.required]],
      sgUf: [this.sndc.sgUf, [Validators.required]],
      icAtivo: [this.sndc.icAtivo, [Validators.required]]
    });
  }

  buscarSituacoes() {
    this.comumService.listarSituacoesSemOpcaoTodos().subscribe(
      (situacoes: Situacao[]) => {
        this.situacoes = situacoes;
      },
      error => {
        this.messageService.addMsgDanger(
          'Ocorreu um erro ao pesquisar situações.'
        );
      }
    );
  }

  carregarUfs() {
    this.ufService.todos((ufs: Uf[]) => {
      this.ufs = ufs;
    });
  }

  limpar() {
    this.formulario.reset();
  }

  inserir() {
    if (!this.validarFormulario(this.formulario)) {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
    }
    this.sndc = new UnidadeSndc();
    this.sndc = this.getClone(this.formulario.value);
    this.sndcService.consultarNoUnidadeSndc(this.sndc).subscribe((result) => {

      if (result.length < 1) {

        if (this.id == null) {
          this.sndcService.post(this.formulario.value).subscribe(() => {
            this.messageService.addMsgSuccess(
              'Operação efetuada com sucesso.'
            );
            this.formulario.reset();
          });

        } else {
          this.sndcService.put(this.formulario.value).subscribe(() => {
            this.messageService.addMsgSuccess(
              'Operação efetuada com sucesso.'
              );
            this.voltar();
            });

          }

      } else {
        this.messageService.addMsgDanger(
        'Unidade Incluir SNDC ' + this.sndc.noUnidadeSndc +
        ' da UF' + this.sndc.sgUf +
        ' já está cadastrada no sistema.'
        );
      }
    });
  }

  voltar() {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

}
