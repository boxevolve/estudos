import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultarClassificacaoOcorrenciaComponent } from './consultar-classificacao-ocorrencia/consultar-classificacao-ocorrencia.component';
import { ConsultarHistoricoClassificacaoComponent } from './consultar-historico-classificacao/consultar-historico-classificacao.component';
import { CadastrarClassificacaoOcorrenciaComponent } from './cadastrar-classificacao-ocorrencia/cadastrar-classificacao-ocorrencia.component';

const routes: Routes = [
  {
    path: '', component: ConsultarClassificacaoOcorrenciaComponent
  },
  {
    path: 'historico', component: ConsultarHistoricoClassificacaoComponent
  },
  {
    path: 'cadastro', component: CadastrarClassificacaoOcorrenciaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassificacaoOcorrenciaRoutingModule { }
