import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { OcorrenciaCadastroComponent } from './ocorrencia-cadastro/ocorrencia-cadastro.component';
import { OcorrenciaConsultaComponent } from './ocorrencia-consulta/ocorrencia-consulta.component';
import { OcorrenciaResultadoConsultaComponent } from './ocorrencia-resultado-consulta/ocorrencia-resultado-consulta.component';


const routes: Routes = [{
    path: '',
    canActivate: [AuthGuard, DadosUsuarioGuard],
    canActivateChild: [AuthGuard, DadosUsuarioGuard],
    children: [
        {
            path: '',
            component: OcorrenciaConsultaComponent
        },
        {
            path: 'novo',
            component: OcorrenciaCadastroComponent
        },
        {
            path: ':id/editar',
            component: OcorrenciaCadastroComponent
        },
        {
            path: 'consulta/resultado',
            component: OcorrenciaResultadoConsultaComponent
        },
        {
            path: 'historico',
            component: OcorrenciaCadastroComponent
        }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OcorrenciaRoutingModule { }
