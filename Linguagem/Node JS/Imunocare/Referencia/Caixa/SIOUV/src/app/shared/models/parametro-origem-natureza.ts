import { Entity } from 'app/arquitetura/shared/models/entity';
import { ParametroOrigemNaturezaId } from './parametro-origem-natureza-id';

export class ParametroOrigemNatureza extends Entity {
    public id: ParametroOrigemNaturezaId;
    public icResposta: string;
    public icTransferencia: string;
    public nuNaturalTransferencia: number;
    public nuNatureza: number;
    public nuTipoOcorrencia: number;
    public nuTipoOrigem: number;
    public nuUnidadeTransferencia: number;
}
