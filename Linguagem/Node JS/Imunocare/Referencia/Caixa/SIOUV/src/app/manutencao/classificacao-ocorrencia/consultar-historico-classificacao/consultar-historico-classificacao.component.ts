import { Component } from '@angular/core';
import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MATRICULA_MASK } from 'app/shared/util/masks';
import { TipoOperacao } from 'app/shared/models/tipo-operacao';
import { TipoOperacaoService } from 'app/shared/services/manutencao/tipo-operacao.service';
import { TipoOcorrencia } from 'app/shared/models/tipo-ocorrencia';
import { TipoOcorrenciaService } from 'app/shared/services/manutencao/tipo-ocorrencia.service';
import { ComumService } from 'app/shared/services/comum/comum.service';
import { AssuntoItemMotivo } from 'app/shared/models/AssuntoItemMotivo';
import { ClassificacaoHistoricoFiltro } from 'app/shared/models/classificacao-historico.filtro';
import { ClassificacaoHistorico } from 'app/shared/models/classificacao-historico';
import { ClassificacaoHistoricoService } from 'app/shared/services/manutencao/classificacao-historico.service';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';

@Component({
  selector: 'app-consultar-historico-classificacao',
  templateUrl: './consultar-historico-classificacao.component.html',
  styleUrls: ['./consultar-historico-classificacao.component.css']
})
export class ConsultarHistoricoClassificacaoComponent extends BaseComponent {

  /**
	* Nomes dos formControlName dos campos
	*/
  nameUsuario = 'usuario';
  nameOperacao = 'tiposOperacao';
  nameDataInicio = 'dataInicio';
  nameDataFim = 'dataFim';
  nameTipoDeOcorrencia = 'tipoOcorrencia';
  nameAssunto = 'tiposAssuntoItemMotivo';
  namePalavra = 'palavraChave';

  matriculaMask = MATRICULA_MASK;
  formulario: FormGroup;
  dataList: SiouvTable;
  idModalDetalhar: string;

  tiposOperacao: TipoOperacao[] = [];
  tiposOcorrencia: TipoOcorrencia[] = [];
  assuntos: AssuntoItemMotivo[] = [];

  classificacaoHistorico: ClassificacaoHistorico[];
  classificacaoHistoricoFiltro: ClassificacaoHistoricoFiltro;

  constructor(
    protected messageService: MessageService,
    protected formBuilder: FormBuilder,
    protected route: ActivatedRoute,
    protected router: Router,
    protected tipoOperacaoService: TipoOperacaoService,
    protected tipoOcorrenciaService: TipoOcorrenciaService,
    protected comumService: ComumService,
    protected classificacaoService: ClassificacaoHistoricoService,

  ) {
    super(messageService);
    this.classificacaoHistorico = [];
    this.classificacaoHistoricoFiltro = new ClassificacaoHistoricoFiltro();
    this.carregarTiposOperacao();
    this.carregarOcorrencia();
    this.carregarAssuntoItemMotivo();
    this.createFormGroup();
   }

   carregarAssuntoItemMotivo() {
    this.comumService.listarAssuntoItemMotivo().subscribe(
      (assuntos: AssuntoItemMotivo[]) => {
        this.assuntos = assuntos;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os tipos de Assunto, Item e Motivo.');
      }
    );
   }

   carregarOcorrencia() {
    this.tipoOcorrenciaService.get().subscribe(
      (ocorrencia: TipoOcorrencia[]) => {
        this.tiposOcorrencia = ocorrencia;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os tipos de ocorrências.');
      }
    );
   }

   carregarTiposOperacao(): void {
    this.tipoOperacaoService.get().subscribe(
      (tiposOperacao: TipoOperacao[]) => {
        this.tiposOperacao = tiposOperacao;
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os tipos de operação.');
      }
    );
   }

   createFormGroup(): void {
     this.formulario = this.formBuilder.group({
       usuario: [this.classificacaoHistoricoFiltro.usuario],
       tiposOperacao: [this.classificacaoHistoricoFiltro.tiposOperacao],
       tiposAssuntoItemMotivo: [this.classificacaoHistoricoFiltro.tiposAssuntoItemMotivo],
       tipoOcorrencia: [this.classificacaoHistoricoFiltro.tipoOcorrencia],
       dataInicio: [this.classificacaoHistoricoFiltro.dataInicio, [Validators.required]],
       dataFim: [this.classificacaoHistoricoFiltro.dataFim, [Validators.required]],
       palavraChave: [this.classificacaoHistoricoFiltro.palavraChave],
     });
   }

   pesquisar(): void {
     if (!this.validarFormulario(this.formulario)) {
      this.messageService.addMsgDanger('Preencha os campos Obrigatórios.');
      return;
     }
      this.montarObj();
      this.classificacaoService.consultarPorFiltro(this.classificacaoHistoricoFiltro).subscribe(
        (classificacaoHistorico: ClassificacaoHistorico[]) => {
          this.classificacaoHistorico = classificacaoHistorico;
          this.atualizarDataList();
        },
        error => {
          this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os históricos da classifição.');
        }
      );
   }

   montarObj(): void {
    this.classificacaoHistoricoFiltro = this.getClone(this.formulario.value);
    this.classificacaoHistoricoFiltro.tiposAssuntoItemMotivo = this.getValueArrayForm(this.formulario, this.nameAssunto, this.assuntos, 'value');
    this.classificacaoHistoricoFiltro.tiposOperacao = this.getValueArrayForm(this.formulario, this.nameOperacao, this.tiposOperacao);
  }

   limpar(): void {
    this.formulario.reset();
  }

  voltar(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  setarIdModal(idModal: string): void {
    this.idModalDetalhar = idModal;
  }

  atualizarDataList() {
    this.dataList = this.createSiouvTable(this.classificacaoHistorico);
  }

  createSiouvTable(list: ClassificacaoHistorico[]): SiouvTable {
    const idTable = 'consultaHistoricoNatureza';
    const creator: SiouvTableCreator<ClassificacaoHistorico> = new SiouvTableCreator();
    // Cabeçalhos
    // Id - Texto - Class - Style
    const headers = [
      creator.addHeader('usuario', 'Usuário', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('operacao', 'Operação', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('data_hora', 'Data/Hora', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('tipo_de_ocorrencia', 'Tipo De Ocorrência', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('assunto', 'Assunto', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('item', 'Item', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('motivo', 'Motivo', 16, 'text-center', {'vertical-align': 'middle'}),
      creator.addHeader('acao', 'Ação', 16, 'text-center', {'vertical-align': 'middle'})
    ];

    // Linhas
    // Id - Texto - Class - Style - Objeto
    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'usuario', object.coUsuario, 'text-center', null, object),
        creator.addCol(idTable, index, 'operacao', object.tipoOperacao.noTipoOperacao, 'text-center', null, object),
        creator.addCol(idTable, index, 'data_hora', object, 'text-center', null, object),
        creator.addCol(idTable, index, 'tipo_de_ocorrencia', object, 'text-center', null, object),
        creator.addCol(idTable, index, 'assunto', object.dhItemAssunto, 'text-center', null, object),
        creator.addCol(idTable, index, 'item', object.dhItemAssunto, 'text-center', null, object),
        creator.addCol(idTable, index, 'motivo', object.motivo, 'text-center', null, object),
      ];
      rows.push(creator.addRowDetailModal(idTable, index, cols, null, null, this.idModalDetalhar, list[index]));
    }

    // Table
    // Id - Cabeçalhos - Linhas - Class - Style - Lista
    return creator.newDefaultSiouvTable(idTable, headers, rows, null, null, list);
  }
}
