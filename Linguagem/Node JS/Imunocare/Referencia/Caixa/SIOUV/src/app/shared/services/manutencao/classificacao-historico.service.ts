import { Injectable } from '@angular/core';
import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { HttpClient } from '@angular/common/http';
import { ClassificacaoHistorico } from 'app/shared/models/classificacao-historico';
import { ClassificacaoHistoricoFiltro } from 'app/shared/models/classificacao-historico.filtro';

@Injectable()
export class ClassificacaoHistoricoService extends CrudHttpClientService<ClassificacaoHistorico> {

  constructor(protected http: HttpClient) {
      super('manutencao/classificacao-historico', http);
  }

  consultarPorFiltro(filtro: ClassificacaoHistoricoFiltro): any {
    return this.http.post<ClassificacaoHistorico[]>(this.url + '/consulta/filtro', filtro, this.options());
}
}
