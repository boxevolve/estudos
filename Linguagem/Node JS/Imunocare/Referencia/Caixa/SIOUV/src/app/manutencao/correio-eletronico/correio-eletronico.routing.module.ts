import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { CorreioEletronicoConsultaComponent } from './correio-eletronico-consulta/correio-eletronico-consulta.component';
import { CorreioEletronicoCadastroComponent } from './correio-eletronico-cadastro/correio-eletronico-cadastro.component';

const routes: Routes = [{
    path: '',
    canActivate: [AuthGuard, DadosUsuarioGuard],
    canActivateChild: [AuthGuard, DadosUsuarioGuard],
    children: [
        {
            path: '',
            component: CorreioEletronicoConsultaComponent
        },
        {
            path: ':id/editar',
            component: CorreioEletronicoCadastroComponent
        },
        {
            path: 'novo',
            component: CorreioEletronicoCadastroComponent
        }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CorreioEletronicoRoutingModule { }
