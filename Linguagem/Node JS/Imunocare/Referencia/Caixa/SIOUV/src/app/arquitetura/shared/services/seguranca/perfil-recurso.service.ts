import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Perfil } from 'app/arquitetura/shared/models/seguranca/perfil';
import { Recurso } from 'app/arquitetura/shared/models/seguranca/recurso';
import { PerfilComRecursos } from 'app/arquitetura/shared/models/seguranca/perfil-com-recursos';

@Injectable()
export class PerfilRecursoService extends CrudHttpClientService<Perfil> {
	constructor(protected http: HttpClient) {
		super('seguranca/perfil-recurso', http);
	}

	/**
	 * Recupera todos os recursos relacionados com o id do perfil informado
	 * @param idPerfil
	 */
	public consultarRecursosPorIdPerfil(idPerfil: number): Observable<Recurso[]> {
		let httpParams: HttpParams = new HttpParams().set('idPerfil', String(idPerfil));

		return this.http.get<Perfil[]>(this.url + '/consultar-recursos-por-id-perfil',
			this.options({params: httpParams}));
	}

	/**
	 * Atualiza os Recursos associados a um Perfil
	 * @param entity
	 */
	public atualizarRecursosDoPerfil(perfilComRecursos: PerfilComRecursos): Observable<Recurso[]> {
		return this.http.put<Recurso[]>(this.url + '/atualizar-recursos-do-perfil',
			perfilComRecursos, this.options());
	}
}
