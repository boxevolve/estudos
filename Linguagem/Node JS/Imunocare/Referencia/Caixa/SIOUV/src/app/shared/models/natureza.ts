import { Entity } from 'app/arquitetura/shared/models/entity';
import { TipoOcorrenciaNatureza } from './tipo-ocorrencia-natureza';

export class Natureza extends Entity {
  public id: number = null;
  public nome: string = null;
  public ativa: string = null;
  public aceite: string = null;
  public usuario: string = null;
  public restrito: string = null;
  public orientacao: string = null;
  public desconsideraAvlco: string = null;
  public unidadeResponsavelFinal: string = null;
  public tiposOcorrenciaNatureza: TipoOcorrenciaNatureza[] = Array<TipoOcorrenciaNatureza>();
  public labelAtiva: string;
  public labelAceite: string;
  public labelRestrito: string;
  public labelDesconsideraAvlco: string;
  public labelUnidadeResponsavelFinal: string;
  public labelTiposOcorrenciaNatureza: string;
}
