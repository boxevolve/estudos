import { Entity } from './entity';

export class Exemplo extends Entity {
	public nome: string = null;
	public idade: number = null;
	public ultimaModificacao: string = null;
}
