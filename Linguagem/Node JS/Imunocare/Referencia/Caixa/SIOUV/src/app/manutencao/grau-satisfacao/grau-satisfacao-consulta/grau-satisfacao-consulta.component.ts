import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { GrauSatisfacaoService } from 'app/shared/services/manutencao/grau-satisfacao.service';
import { GrauSatisfacao } from 'app/shared/models/grau-satisfacao';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { SiouvTableBuilder } from 'app/shared/models/siouv-table-builder';

@Component({
  selector: 'app-grau-satisfacao-consulta',
  templateUrl: './grau-satisfacao-consulta.component.html',
  styleUrls: ['./grau-satisfacao-consulta.component.css']
})
export class GrauSatisfacaoConsultaComponent extends BaseComponent {

  listaGrauSatisfacao: GrauSatisfacao[] = null;
  dataList: SiouvTable;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    protected messageService: MessageService,
    private grauSatisfacaoService: GrauSatisfacaoService
  ) {
    super(messageService);
    this.carregarListaGrauSituacao();
  }

  incluir() {
    this.router.navigate(['novo'], { relativeTo: this.route.parent });
  }

  alterar(grauSatisfacao: GrauSatisfacao): void {
    this.router.navigate([grauSatisfacao.id, 'editar'], { relativeTo: this.route.parent });
  }

  excluir(grauSatisfacao: GrauSatisfacao): void {
    const indice: number = this.listaGrauSatisfacao.findIndex(item => item.id === grauSatisfacao.id);
    this.grauSatisfacaoService.verificarVinculo(grauSatisfacao.id).subscribe(
      (isVinculado: boolean) => {
        if (isVinculado) {
          this.messageService.addMsgDanger('Registro não poderá ser excluído, pois está vinculado a outros.');
        } else {
          this.finalizarExcluir(indice, grauSatisfacao);
        }
      }
    );
  }

  finalizarExcluir(indice: number, grauSatisfacao: GrauSatisfacao): void {
    this.grauSatisfacaoService.delete(grauSatisfacao.id).subscribe(_ => {
        this.messageService.addMsgSuccess('Grau Satisfação excluída com sucesso.');
        if (indice >= 0) {
          this.listaGrauSatisfacao.splice(indice, 1);
        }
        this.atualizarDataList();
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao excluir o grau satisfação.');
        console.log('Erro ao excluir grau satisfação:', error);
      }
    );
  }

  atualizarDataList() {
    this.dataList = this.createSiouvTable(this.listaGrauSatisfacao);
  }

  carregarListaGrauSituacao() {
    this.grauSatisfacaoService.todos().subscribe(
      (listaGrauSatisfacao: GrauSatisfacao[]) => {
        this.listaGrauSatisfacao = listaGrauSatisfacao;
        this.atualizarDataList();
      },
      error => {
        this.messageService.addMsgDanger('Ocorreu um erro ao recuperar os graus situações.');
        console.log('Erro ao recuperar os graus situações:', error);
      });
  }

  createSiouvTable(list: GrauSatisfacao[]): SiouvTable {
    const builder: SiouvTableBuilder<GrauSatisfacao> = new SiouvTableBuilder('consultaGrauSatisfacao', list);
    builder.addColuna('grauSatisfacao', 'Grau Satisfação', (grauSatisfacao: GrauSatisfacao) => grauSatisfacao.nome);
    builder.addColuna('tipo_ocorrencia', 'Tipo de Ocorrência', (grauSatisfacao: GrauSatisfacao) => grauSatisfacao.labelTiposOcorrenciaGrauSatisfacao);
    builder.addColuna('ativo', 'Situação', (grauSatisfacao: GrauSatisfacao) => grauSatisfacao.labelAtivo);
    builder.addColuna('pzPrrgo', 'Prazo de Reabertura', (grauSatisfacao: GrauSatisfacao) => grauSatisfacao.pzPrrgo.toString());
    builder.addColuna('demandaAcompanhamento', 'Tipo de Demanda', (grauSatisfacao: GrauSatisfacao) => grauSatisfacao.labelTipoDeDemanda);
    return this.newDefaultSiouvTableFromBuilder(builder);
  }

}
