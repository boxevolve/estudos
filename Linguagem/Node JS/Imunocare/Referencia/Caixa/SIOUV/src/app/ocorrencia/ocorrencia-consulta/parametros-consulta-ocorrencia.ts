import { Classificacao } from "app/shared/components/classificacao/classificacao";
import { Uf } from "app/shared/models/uf";
import { Injectable } from "@angular/core";

@Injectable()
export class ParametrosConsultaOcorrencia {

    public cpf: string;
    public cnpj: string;
    public nroNIS: number;
    public matriculaSolicitante: string;
    public nroOcorrencia: number;
    public nroPreOcorrencia: number;
    public nroProtocoloSAC: number;
    public nroRDR: number;
    public ufRDR: Uf;
    public dtInicioPeriodo: Date;
    public dtFimPeriodo: Date;
    public pesquisaAvancada: boolean;
    public nomeSolicitanteExterno: string;
    public unidadesDestino: string[]=[];
    public classificacao: Classificacao;
    public situacao: string;
    public grauSatisfacao: string;
    public dtVencimento: Date;
    public nroProcon: string;
    public ufProcon: Uf;
    public localidadeProcon: string;
    public codOperador: string;
    public pendente: string;
    public tipoDemanda: string[];

}