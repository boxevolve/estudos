import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxPaginationModule } from 'ngx-pagination';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { NaturezaConsultaComponent } from './natureza-consulta/natureza-consulta.component';
import { NaturezaCadastroComponent } from './natureza-cadastro/natureza-cadastro.component';
import { NaturezaHistoricoComponent } from './natureza-historico/natureza-historico.component';
import { NaturezaRoutingModule } from './natureza.routing.module';
import { SiouvTableModule } from 'app/shared/components/table/table.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NaturezaDetalheComponent } from './natureza-detalhe/natureza-detalhe.component';
import { UnidadeModule } from 'app/shared/components/unidade/unidade.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    ComponentModule.forRoot(),
    SiouvTableModule,
    BsDatepickerModule.forRoot(),
    NaturezaRoutingModule,
    UnidadeModule.forRoot()
  ],
  declarations: [
    NaturezaConsultaComponent,
    NaturezaCadastroComponent,
    NaturezaHistoricoComponent,
    NaturezaDetalheComponent
  ]
})
export class NaturezaModule {}
