import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { GrauSatisfacaoCadastroComponent } from './grau-satisfacao-cadastro/grau-satisfacao-cadastro.component';
import { GrauSatisfacaoConsultaComponent } from './grau-satisfacao-consulta/grau-satisfacao-consulta.component';

const routes: Routes = [{
    path: '',
    canActivate: [AuthGuard, DadosUsuarioGuard],
    canActivateChild: [AuthGuard, DadosUsuarioGuard],
    children: [
        {
            path: '',
            component: GrauSatisfacaoConsultaComponent
        },
        {
            path: ':id/editar',
            component: GrauSatisfacaoCadastroComponent
        },
        {
            path: 'novo',
            component: GrauSatisfacaoCadastroComponent
        }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GrauSatisfacaoRoutingModule { }
