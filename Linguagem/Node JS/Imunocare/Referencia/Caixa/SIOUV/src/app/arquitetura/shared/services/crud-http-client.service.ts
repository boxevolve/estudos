import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HttpClientService } from './http-client.service';
import { Entity } from 'app/arquitetura/shared/models/entity';

export class CrudHttpClientService<T extends Entity> extends HttpClientService {
  constructor(public uri: string, protected http: HttpClient) {
    super(uri, http);
  }

  /**
	 * Recupera um registro em particular, ou todos (caso não seja passado
	 * o parâmetro id)
	 * @param id
	 */
  public get(id?: number): Observable<T|T[]> {
    let url = this.url;
    if (id) {
      url += '/' + id;
    }
    return this.http.get<T>(url, this.options());
  }

  /**
	 * Insere um registro
	 * @param entity
	 */
  public post(entity: T): Observable<T> {
    return this.http.post<T>(this.url, entity, this.options());
  }

  /**
	 * Altera um registro
	 * @param entity
	 */
  public put(entity: T): Observable<T> {
    return this.http.put<T>(this.url, entity, this.options());
  }

  /**
	 * Exclui um registro
	 * @param id
	 */
  public delete(id: number): Observable<T> {
    return this.http.delete<T>(this.url + '/' + id, this.options());
  }

  /**
	 * Consulta de motivos por nome
	 */
  protected consultarPorParametro(endpoint: string, paramName: string, param: any): Observable<T[]> {
    const httpParams: HttpParams = new HttpParams().set(paramName, param);
    return this.http.get<T[]>(this.url + '/' + endpoint, this.options({ params: httpParams }));
  }

  protected callGET<O>(endpoint: string, next: (value: any) => void, error?: (value: any) => void): any {
    return this.http.get<O>(this.url + endpoint, this.options()).subscribe(next, error);
  }

  protected callPOST<O>(endpoint: string, object: any, next: (value: any) => void, error?: (value: any) => void): any {
    return this.http.post<O>(this.url + endpoint, object, this.options()).subscribe(next, error);
  }

}
