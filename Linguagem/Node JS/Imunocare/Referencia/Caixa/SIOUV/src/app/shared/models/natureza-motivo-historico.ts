import { NaturezaMotivoHistoricoId } from './natureza-motivo-historico-id';

export class NaturezaMotivoHistorico {

  public id: NaturezaMotivoHistoricoId = new NaturezaMotivoHistoricoId();
  // public pzSolucao: string = null;
  // public ativa: string = null;
  public labelNatureza: string = null;
  public labelAtiva: string = null;
  public labelTipoOperacao: string = null;
}
