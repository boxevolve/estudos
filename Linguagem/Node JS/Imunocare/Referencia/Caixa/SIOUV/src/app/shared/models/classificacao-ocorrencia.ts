import { ItemAssunto } from './item-assunto';
import { TipoOcorrencia } from './tipo-ocorrencia';
import { Motivo } from './motivo';

export class ClassificacaoOcorrencia {
    ativa: string;
    correioEletronico: string;
    itemAssunto: ItemAssunto;
    motiv: Motivo;
    natural: string;
    orientacao: string;
    respostaOrigem: string;
    tipoOcorrencia: TipoOcorrencia;
    unidade: number;
    usuario: string;
}
