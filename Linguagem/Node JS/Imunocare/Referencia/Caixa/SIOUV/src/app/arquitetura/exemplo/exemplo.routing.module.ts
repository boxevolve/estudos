import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';

import { ExemploComponent } from './exemplo.component';
import { ExemploAdicionalComponent } from 'app/arquitetura/exemplo/exemplo-adicional.component';

const exemploRoutes: Routes = [{
	path: '',
	component: ExemploComponent,
	canActivate: [AuthGuard, DadosUsuarioGuard],
	canActivateChild: [AuthGuard, DadosUsuarioGuard],
	children: [
		{
			path: 'exemplo-adicional',
			component: ExemploAdicionalComponent
		}
	]
}];

@NgModule({
	imports: [RouterModule.forChild(exemploRoutes)],
	exports: [RouterModule]
})
export class ExemploRoutingModule { }
