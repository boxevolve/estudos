import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/arquitetura/shared/guards/security/auth.guard';
import { DadosUsuarioGuard } from 'app/arquitetura/shared/guards/security/dados-usuario.guard';
import { MotivoConsultaComponent } from './motivo-consulta/motivo-consulta.component';
import { MotivoCadastroComponent } from './motivo-cadastro/motivo-cadastro.component';
import { MotivoHistoricoComponent } from './motivo-historico/motivo-historico.component';

const routes: Routes = [{
    path: '',
    canActivate: [AuthGuard, DadosUsuarioGuard],
    canActivateChild: [AuthGuard, DadosUsuarioGuard],
    children: [
        {
            path: '',
            component: MotivoConsultaComponent
        },
        {
            path: 'novo',
            component: MotivoCadastroComponent
        },
        {
            path: ':id/editar',
            component: MotivoCadastroComponent
        },
        {
            path: 'historico',
            component: MotivoHistoricoComponent
        }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MotivoRoutingModule { }
