import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { SelecionaUnidadeComponent } from './seleciona-unidade/seleciona-unidade.component';
import { ComponentModule } from '../component.module';
import { UnidadeModalComponent } from './unidade-modal/unidade-modal.component';
import { SiouvTableModule } from '../table/table.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    ComponentModule.forRoot(),
    SiouvTableModule
  ],
  declarations: [
    UnidadeModalComponent,
    SelecionaUnidadeComponent
  ],
  exports: [
    UnidadeModalComponent,
    SelecionaUnidadeComponent
  ],
})
export class UnidadeModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UnidadeModule
    };
  }
}
