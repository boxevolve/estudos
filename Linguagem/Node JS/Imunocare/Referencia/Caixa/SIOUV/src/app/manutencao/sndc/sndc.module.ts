import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SndcRoutingModule } from './sndc-routing.module';
import { SndcCadastroComponent } from './sndc-cadastro/sndc-cadastro.component';
import { SndcConsultaComponent } from './sndc-consulta/sndc-consulta.component';
import { TemplatesModule } from 'app/arquitetura/shared/templates/templates.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxPaginationModule } from 'ngx-pagination';
import { DirectivesModule } from 'app/arquitetura/shared/directives/directives.module';
import { ComponentModule } from 'app/shared/components/component.module';
import { SiouvTableModule } from 'app/shared/components/table/table.module';
import { SndcService } from 'app/shared/services/manutencao/sndc.service';

@NgModule({
  imports: [
    CommonModule,
    SndcRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    NgxPaginationModule,
    DirectivesModule.forRoot(),
    TemplatesModule,
    ComponentModule.forRoot(),
    SiouvTableModule
  ],
  declarations: [SndcCadastroComponent, SndcConsultaComponent]
})
export class SndcModule { }
