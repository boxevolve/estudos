import { Entity } from 'app/arquitetura/shared/models/entity';

export class ClassificacaoFiltro extends Entity {
    tipoOcorrencia: number = null;
    operacoes: boolean[] = new Array<boolean>();
    palavraChave: string = null;
    natural: number;
    tipoDirecionamento: number;
    ativa: string;
}
