import { HttpErrorResponse } from '@angular/common/http';
import { AbstractControl, FormGroup } from '@angular/forms';

import { MessageService } from 'app/shared/components/messages/message.service';
import { Natureza } from '../models/natureza';
import { TipoOcorrenciaNatureza } from '../models/tipo-ocorrencia-natureza';
import { TipoOcorrencia } from '../models/tipo-ocorrencia';
import { TipoOcorrenciaNaturezaId } from '../models/tipo-ocorrencia-natureza-id';
import { Util } from 'app/arquitetura/shared/util/util';
import { SiouvTableBuilder } from '../models/siouv-table-builder';
import { SiouvTableCreator } from '../models/siouv-table-creator';
import { Unidade } from '../models/unidade';

export class BaseComponent {
  constructor(
    protected messageService: MessageService
  ) {
    this.messageService = messageService;
  }

  /**
	* Trata as mensagens de erro da API
	* @param response
	*/
  trataMsgErroApi(response: HttpErrorResponse): string[] {
    const arrError = [];

    if (response instanceof HttpErrorResponse) {
      // Erros de validação da API
      if (response.status === 422) {

        if (typeof response.error === 'string') {
          const msg = JSON.parse(response.error);
          if (typeof msg.message === 'string') {
            arrError.push(msg.message);
          }
        }

        const objErrorValidation = response.error.errors;
        // Coloca todas as mensagens em um array
        for (const keyGroup in objErrorValidation) {
          if (objErrorValidation.hasOwnProperty(keyGroup)) {
            for (const key in objErrorValidation[keyGroup]) {
              if (objErrorValidation[keyGroup].hasOwnProperty(key)) {
                arrError.push(objErrorValidation[keyGroup][key]);
              }
            }
          }
        }
      } else {
        // Verifica se é uma mensagem simples
        if (typeof response.error === 'object' && typeof response.error.message === 'string') {
          arrError.push(response.error.message);
        }

        if (typeof response.error === 'string') {
          const msg = JSON.parse(response.error);
          if (typeof msg.message === 'string') {
            arrError.push(msg.message);
          }
        }
      }
    }

    return arrError;
  }

  getClone(object: any): any {
    return JSON.parse(JSON.stringify(object));
  }

  validaSeDataValida(formulario: FormGroup, nomeCampo: string): boolean {
    const campo: AbstractControl = formulario.get(nomeCampo);
    return (campo.touched && (campo.value.length === 0))
        || (campo.touched && (campo.value == null))
        || (campo.touched && (Util.isEmpty(campo.value)));
  }

  validaSeCampoValido(formulario: FormGroup, nomeCampo: string): boolean {
    const campo: AbstractControl = formulario.get(nomeCampo);
    return (campo.dirty || campo.touched)
         && campo.invalid
         && Util.isEmpty(campo.value);
  }

  validaSeCampoValidoLength(formulario: FormGroup, nomeCampo: string, minLength: number): boolean {
    const campo: AbstractControl = formulario.get(nomeCampo);
    const value: string =  campo.value + '';
    return (campo.dirty || campo.touched)
        && (value && value.trim().length < minLength || Util.isEmpty(value)) ;
  }

  aplicaClasseDeErro(formulario: FormGroup, nomeCampo: string): any {
    return { 'has-danger': this.validaSeCampoValido(formulario, nomeCampo) };
  }

  validaSeCheckboxValido(formulario: FormGroup, nomeCampo: string, lista: Array<any>): boolean {
    const campo: AbstractControl = formulario.get(nomeCampo);
    return (campo.dirty || campo.touched) && lista.length === 0;
  }

  validaSeCheckboxValidoList(touched: boolean, lista: Array<any>): boolean {
    return touched && lista.length === 0;
  }

  onChangeCheckbox(value: any, isChecked: boolean, list: Array<any>): void {
    if (isChecked) {
      list.push(value);
    } else {
      const index = list.findIndex(x => this.equals(x, value));
      if (index !== -1) {
         list.splice(index, 1);
      }
    }
  }

  showPaginacao(lista: Array<any>): boolean {
    return lista && lista.length > 0;
  }

  /**
   * Cria uma nova instancia de tipo de ocorrencia natureza, foi adicionado aqui por que existe varias
   * paginas que precisam dessa criação, para que nao fique codigo repedido.
   *
   * @param natureza
   * @param tipoOcorrencia
   */
  newTipoOcorrenciaNatureza(natureza: Natureza, tipoOcorrencia: TipoOcorrencia): TipoOcorrenciaNatureza {
    const tipoOcorrenciaNaturezaId = new TipoOcorrenciaNaturezaId(natureza.id, tipoOcorrencia.id);
    return new TipoOcorrenciaNatureza(tipoOcorrenciaNaturezaId, null, tipoOcorrencia);
  }

  contains(list: any[], value?: any, lambda?: (x: any) => boolean): boolean {
    let index = -1;
    if (lambda) {
      index = list.findIndex(lambda);
    } else {
      index = list.findIndex(x => this.equals(x, value));
    }
    return index !== -1;
  }

  equals(objectA: any, objectB: any): boolean {
    if (typeof objectA !== typeof objectB) {
      return false;
    }
    if (objectA === null && objectB === null) {
      return true;
    }
    const aKeys = objectA !== null && typeof objectA !== 'string' ? Object.keys(objectA) : [],
          bKeys = objectB !== null && typeof objectB !== 'string' ? Object.keys(objectB) : [];
    if (aKeys.length !== bKeys.length) {
      return false;
    }
    if (aKeys.length === 0) {
      return (objectA === objectB
          || (typeof objectA === 'object' || typeof objectB === 'object'));
    }

    const areDifferent = aKeys.some((key) => {
      return !this.equals(objectA[key], objectB[key]);
    });
    return !areDifferent;
  }

  disableField(formulario: FormGroup, nomeCampo: string): void {
    const field = this.getControl(formulario, nomeCampo);
    if (!Util.isEmpty(field)) {
      field.disable();
    }
  }

  enableField(formulario: FormGroup, nomeCampo: string): void {
    const field = this.getControl(formulario, nomeCampo);
    if (!Util.isEmpty(field)) {
      field.enable();
    }
  }

  getControl(formulario: FormGroup, nomeCampo: string): AbstractControl {
    if (!Util.isEmpty(formulario)) {
      const campo: AbstractControl = formulario.get(nomeCampo);
      if (!Util.isEmpty(campo)) {
        return formulario.get(nomeCampo);
      }
    }
    return null;
  }

  idRow(values: string[]): string {
    let id = 'row_';
    values.forEach(value => {
      id += value + '_';
    });
    return id.substring(0, id.length - 1);
  }

  idField(values: string[]): string {
    let idField = 'field_';
    values.forEach(value => {
      idField += value + '_';
    });
    return idField.substring(0, idField.length - 1);
  }

  idModal(idBase: string, idModal: string, values: string[]): string {
    let id = 'modal_' + idBase + '_' + idModal + '_';
    values.forEach(value => {
      id += value + '_';
    });
    return id.substring(0, id.length - 1);
  }

  idHashtag(id: string): string {
    return id ? '#' + id : '';
  }

  concatenarComTraco(values: any[]): string {
    let retorno = '';
    values.forEach(value => {
      retorno += value + ' - ';
    });
    return retorno.substring(0, retorno.length - 3);
  }

  getValueById(id: string): any {
    return $('#' + id).val();
  }

  setValueById(field: any, value: string): void {
    if (typeof field === 'string') {
      this.getValueById(field).val(value);
    }
  }

  getValueForm(form: FormGroup, fieldName: string): any {
    return form.get(fieldName).value;
  }

  getValueArrayForm(form: FormGroup, fieldName: string, list: any[], attribute?: string): any {
    const controlArray = this.getValueForm(form, fieldName);
    if (!Util.isEmpty(controlArray)) {
      return controlArray.map((v, i) => v ? (Util.isEmpty(attribute) ? list[i] : list[i][attribute]) : null).filter(v => v !== null);
    }
    return null;
  }

  setValue(form: FormGroup, fieldName: string, value: any): void {
    const field = this.getControl(form, fieldName);
    if (!Util.isEmpty(field)) {
      field.setValue(value);
    }
  }

  setValueArray(form: FormGroup, fieldName: string, value: any): void {
    const field = this.getControl(form, fieldName);
    if (!Util.isEmpty(field)) {
      form.removeControl(fieldName);
      form.addControl(
        fieldName,
        value
      );
    }
  }

  getMessageError(error: any, messageDefault: string): string {
    return typeof error.error !== 'string' ? messageDefault : error.error;
  }

  /**
   * Modelo 1 = Se possuir sigla -> 'codigo' - 'sigla' - 'uf'
   * Modelo 2 = Se possuir sigla -> 'codigo' - 'sigla' - 'uf'
   *
   * @param unidade
   * @param modelo
   */
  getUnidadeDescricao(unidade: Unidade, modelo: number): string {
    let descricao = '';
    if (!Util.isDefined(unidade)) {
      return descricao;
    }
    if (modelo === 1) {
      descricao = !Util.isEmpty(unidade.sigla) ? this.concatenarComTraco([unidade.nuUnidade, unidade.sigla, unidade.uf])
      : this.concatenarComTraco([unidade.nuUnidade, unidade.nome]);
    } else if (modelo === 2) {
      if (!Util.isEmpty(unidade.siglaLocalizacao) && !Util.isEmpty(unidade.sigla)) {
        descricao = this.concatenarComTraco([unidade.nuUnidade, unidade.sigla]) + '/' +
          this.concatenarComTraco([unidade.siglaLocalizacao, unidade.uf]);
      } else if (Util.isEmpty(unidade.siglaLocalizacao) && !Util.isEmpty(unidade.sigla)) {
        descricao = this.concatenarComTraco([unidade.nuUnidade, unidade.sigla, unidade.uf]);
      } else {
        descricao = this.concatenarComTraco([unidade.nuUnidade, unidade.nome, unidade.uf]);
      }
    }
    return descricao;
  }

  getItemList(list: any[], position: number): any {
    return list.length > 0 ? list[position] : null;
  }

  // INICIO - validação formulario
  validarFormulario(formulario: FormGroup): boolean {
    return this.validarFormularioAll(formulario, []);
  }

  validarFormularioAll(formulario: FormGroup, lists: any[]): boolean {
    const keys = Object.keys(formulario.value);
    for (let index = 0; index < keys.length; index++) {
      const field: AbstractControl = formulario.get(keys[index]);
      const value = field.value;
      if (value && typeof value === 'string') {
        field.setValue(value.trim());
      }
      if (field.invalid) {
        return false;
      }
    }
    for (let index = 0; index < lists.length; index++) {
      if (lists[index].length === 0) {
        return false;
      }
    }
    return formulario.valid;
}

  validarPeloMenosUm(formulario: FormGroup, keys: string[]): boolean {
    return this.validarPeloMenosUmAll(formulario, keys, []);
  }

  validarPeloMenosUmAll(formulario: FormGroup, keys: string[], lists: any[]): boolean {
    for (let index = 0; index < keys.length; index++) {
      const field: AbstractControl = formulario.get(keys[index]);
      const value = field.value;
      if (value && typeof value === 'string') {
        field.setValue(value.trim());
      }
      if (!Util.isEmpty(value)) {
        return true;
      }
    }
    for (let index = 0; index < lists.length; index++) {
      if (lists[index].length !== 0) {
        return true;
      }
    }
    return false;
  }
  // FIM - validação formulario

  newDefaultSiouvTableFromBuilder<T>(builder: SiouvTableBuilder<T>) {
    builder.render();
    return new SiouvTableCreator().newSiouvTable(builder.tableId, null, builder.headers, builder.rows, builder.clazz, builder.style, null, null, null, null, builder.tableData);
  }

  removeList(list: any[], item: any, lambda?: (value: any) => boolean): any[] {
    return list.filter((value) => {
      return lambda ? lambda(value) : !this.equals(value, item);
    });
  }

  // INICIO - Tratamento de modais
  showBsModal(idModal: string, showCallback?: () => void, showCallbacke?: (e) => void): void {
    this.funcaoModal(idModal, 'show.bs.modal', showCallback, showCallbacke);
  }

  shownModal(idModal: string, showCallback?: () => void, showCallbacke?: (e) => void): void {
    this.funcaoModal(idModal, 'shown.bs.modal', showCallback, showCallbacke);
  }

  hiddenModal(idModal: string, callback?: () => void, callbacke?: (e) => void): void {
    this.funcaoModal(idModal, 'hidden.bs.modal', callback, callbacke);
  }

  funcaoModal(idModal: string, functionName: string, callback?: () => void, callbacke?: (e) => void): void {
    $(this.idHashtag(idModal)).on(functionName, (e) => {
      if (e.target.id === e.currentTarget.id) {
        if (callback) {
          callback();
        }
        if (callbacke) {
          callbacke(e);
        }
      }
    });
  }

  showModal(id: string): void {
    (<any>$(this.idHashtag(id))).modal('show');
  }

  hideModal(id: string): void {
    (<any>$(this.idHashtag(id))).modal('hide');
  }
  // FIM - Tratamento de modais

}
