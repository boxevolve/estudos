import { TipoOperacao } from "./tipo-operacao";
import { Unidade } from "./unidade";
import { TipoOcorrencia } from "./tipo-ocorrencia";
import { ClassificacaoHistoricoId } from "./classificacao-historico-id";
import { Entity } from "app/arquitetura/shared/models/entity";
import { TipoDirecionamento } from "./tipo-direcionamento";
import { Motivo } from "./motivo";

export class ClassificacaoHistorico extends Entity {
    public id: ClassificacaoHistoricoId = null;
    public coUsuario: string = null;
    public deCorreioEletronico: string = null;
    public deOrientacao: string = null;
    public deRespostaOrigem: string = null;
    public icAtiva: string = null;
    public unidade: Unidade = null;
    public tipoDirecionamento: TipoDirecionamento;
    public tipoOperacao: TipoOperacao = null;
    public dhItemAssunto: Date = null;
    public motivo: Motivo = null;
    public tipoOcorrecnia: TipoOcorrencia = null;
}
