import { Component, Output, ViewChild, ElementRef, AfterViewInit, EventEmitter, Input } from '@angular/core';

import { BaseComponent } from 'app/shared/components/base.component';
import { MessageService } from 'app/shared/components/messages/message.service';
import { Motivo } from 'app/shared/models/motivo';
import { SiouvTable } from 'app/shared/models/siouv-table';
import { NaturezaMotivo } from 'app/shared/models/natureza-motivo';
import { SiouvTableCreator } from 'app/shared/models/siouv-table-creator';

@Component({
  selector: 'app-motivo-detalhe',
  templateUrl: './motivo-detalhe.component.html',
  styleUrls: ['./motivo-detalhe.component.css']
})
export class MotivoDetalheComponent extends BaseComponent implements AfterViewInit {

  modalId = 'modalMotivoDetalhe';

  @ViewChild('modalMotivoDetalhe') modal: ElementRef;
  @Input('motivoSelecionado') motivoSelecionado: Motivo;
  @Output() emitModal = new EventEmitter();

  detalhes: any[] = [];
  carregado = false;
  datalist: SiouvTable;

  constructor(
      protected messageService: MessageService,
  ) {
      super(messageService);
      this.criarTabela([]);
  }

  ngAfterViewInit() {
    this.emitModal.emit(this.getClone(this.modalId));
    $(this.modal.nativeElement).on('hidden.bs.modal',  () => {
        this.limpar();
    });
  }

  limpar(): void {
      this.motivoSelecionado = null;
      this.carregado = !this.carregado;
  }

  getDetalhes(): any[] {
    if (this.motivoSelecionado && !this.carregado) {
      this.detalhes = [
        {titulo: 'Motivo:', value: this.motivoSelecionado.nome},
        {titulo: 'Situação:', value: this.motivoSelecionado.labelAtiva},
        {titulo: 'Descrição:', value: this.motivoSelecionado.descricao}
      ];
      this.carregado = !this.carregado;
      this.criarTabela(this.motivoSelecionado.naturezasMotivo);
    }
    return this.detalhes;
  }

  text(text: string): string {
    return text ? text : '';
  }

  criarTabela(list: any[]): void {
    this.datalist = this.createSiouvTable(list);
  }

  createSiouvTable(list: NaturezaMotivo[]): SiouvTable {
    const idTable = 'NaturezasMotivo';
    const creator: SiouvTableCreator<NaturezaMotivo> = new SiouvTableCreator();

    const headers = [
      creator.addHeader('natureza', 'Natureza', 50, 'text-center labelAzul', {'vertical-align': 'middle'}),
      creator.addHeader('situacao', 'Situação', 50, 'text-center labelAzul', {'vertical-align': 'middle'})
    ];

    const rows = [];
    for (let index = 0; index < list.length; index++) {
      const object = list[index];
      const cols = [
        creator.addCol(idTable, index, 'nome', object.natureza.nome, 'text-center', null, object),
        creator.addCol(idTable, index, 'ativa', object.labelAtiva, 'text-center', null, object),
      ];
      rows.push(creator.addRowNone(idTable, index, cols, null, null, list[index]));
    }

    return creator.newSiouvTableNoPagination(idTable, headers, rows, null, null, list);
  }

}
