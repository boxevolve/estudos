import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CrudHttpClientService } from 'app/arquitetura/shared/services/crud-http-client.service';
import { Natureza } from '../../models/natureza';
import { TipoOcorrenciaNatureza } from 'app/shared/models/tipo-ocorrencia-natureza';
import { MessageService } from 'app/shared/components/messages/message.service';

@Injectable()
export class NaturezaService extends CrudHttpClientService<Natureza> {

  constructor(
    protected http: HttpClient,
    private messageService: MessageService
  ) {
    super('ocorrencia/natureza', http);
  }

  todosAtivos(): any {
      return this.http.get<Natureza[]>(this.url + '/ativos', this.options());
  }

  verificarRegistroVinculado(tipoOcorrenciaNatureza: TipoOcorrenciaNatureza): any {
      return this.http.post<boolean>(this.url + '/verificar/registro-vinculado', tipoOcorrenciaNatureza, this.options());
  }

  verificarDuplicidade(natureza: Natureza): any {
      return this.http.post<boolean>(this.url + '/verificar/duplicidade', natureza, this.options());
  }

  private callService(next?: (value: any) => void): void {
      this.http.get(this.url, this.options());
  }

  public callListarNaturezasPorTipoOcorrenciaAndTipoOrigem(idTipoOcorrencia: number, idTipoOrigem: number,
      next?: (naturezas: Natureza[]) => void): void {
      this.callGET<Natureza[]>('/itens/tipoocorrencia/' + idTipoOcorrencia + '/origem/' + idTipoOrigem, next,
          error => {
              this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar naturezas.');
              console.log('Erro ao chamar o servico. Erro: ' + error);
          }
      );
  }

  public callListarNaturezasPorTipoOrigem(idTipoOrigem: number,
      next?: (naturezas: Natureza[]) => void): void {
      this.callGET<Natureza[]>('/itens/origem/' + idTipoOrigem, next,
          error => {
              this.messageService.addMsgDanger('Ocorreu um erro ao pesquisar naturezas por origem.');
              console.log('Erro ao chamar o servico. Erro: ' + error);
          }
      );
  }

  public carregarNaturezasPorOrigem(nuOrigem: number, next?: (value: any) => void): Natureza[] {
    return [
      this.newNatureza(1, 'Natureza 1'),
      this.newNatureza(2, 'Natureza 2'),
      this.newNatureza(3, 'Natureza 3'),
      this.newNatureza(4, 'Natureza 4')
    ];
  }

  private newNatureza(id: number, nome: string) {
      const n = new Natureza();
      n.id = id;
      n.nome = nome;
      return n;
  }

}
