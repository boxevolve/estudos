import { Entity } from 'app/arquitetura/shared/models/entity';

export class TipoUnidade extends Entity {
  public sigla: string;
  public descricao: string;
}