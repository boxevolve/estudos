import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipoOcorrenciaService } from './manutencao/tipo-ocorrencia.service';
import { TextoAberturaService } from './manutencao/texto-abertura.service';
import { NaturezaService } from './manutencao/natureza.service';
import { ComumService } from './comum/comum.service';
import { MotivoService } from './manutencao/motivo.service';
import { AssuntoService } from './manutencao/assunto.service';
import { AssuntoHistoricoService } from './manutencao/assunto-historico.service';
import { ManterItemService } from './manutencao/manter-item.service';
import { UnidadeService } from './comum/unidade.service';
import { GrauSatisfacaoService } from './manutencao/grau-satisfacao.service';
import { UfService } from './comum/uf.service';
import { NaturezaHistoricoService } from './manutencao/natureza-historico.service';
import { OrigemService } from './manutencao/tipo-origem.service';
import { TipoOperacaoService } from './manutencao/tipo-operacao.service';
import { OcorrenciaService } from './ocorrencia/ocorrencia.service';
import { UnidadeSemOcorrenciaService } from './manutencao/unidade-sem-ocorrencia.service';
import { SndcService } from './manutencao/sndc.service';
import { MotivoHistoricoService } from './manutencao/motivo-historico.service';
import { TipoUnidadeService } from './comum/tipo-unidade.service';
import { ParametroOrigemNaturezaService } from './manutencao/parametro-origem-natureza.service';
import { NaturezaMotivoHistoricoService } from './manutencao/natureza-motivo-historico.service';
import { CorreioEletronicoService } from './manutencao/correio-eletronico.service';
import { TipoCorreioEletronicoService } from './manutencao/tipo-correio-eletronico.service';
import { ClassificacaoHistoricoService } from './manutencao/classificacao-historico.service';
import { TipoDirecionamentoService } from './manutencao/tipo-direcionamento.service';
import { ClassificacaoService } from './manutencao/classificacao.service';

/*CAST_MAKER_SERVICE_IMPORT*/
/**
 * Modulo responsável por prover os serviços de integração e de apoio
 * específicos da aplicação
 */
@NgModule({
  imports: [CommonModule],
  declarations: []
})
export class AppServiceModule {
  /**
   * Convenção usada para que o módulo 'app' disponibilize as instâncias 'providers'
   * como singleton para todos os modulos da aplicação.
   */
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppServiceModule,
      providers: [
        TipoOcorrenciaService,
        TextoAberturaService,
        NaturezaService,
        NaturezaHistoricoService,
        ComumService,
        MotivoService,
        MotivoHistoricoService,
        NaturezaMotivoHistoricoService,
        ManterItemService,
        UnidadeService,
        AssuntoService,
        GrauSatisfacaoService,
        UfService,
        OrigemService,
        AssuntoHistoricoService,
        TipoOperacaoService,
        OcorrenciaService,
        UnidadeSemOcorrenciaService,
        SndcService,
        TipoUnidadeService,
        ParametroOrigemNaturezaService,
        CorreioEletronicoService,
        TipoCorreioEletronicoService,
        ClassificacaoHistoricoService,
        TipoDirecionamentoService,
        ClassificacaoService
      ]
    };
  }
}
