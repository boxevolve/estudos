import { Entity } from 'app/arquitetura/shared/models/entity';

export class Situacao extends Entity {
  public value: string;
  public text: string;
}
