export class ClassificacaoHistoricoId {
    public dhClassificacao: Date;
    public dhItemAssunto: Date;
    public dhMotivo: Date;
    public nuItemAssunto: number;
    public nuMotivo: number;
    public nuTipoOcorrencia: number;
}
