import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import "reflect-metadata";

import { environment } from 'environments/environment';
import { AppModule } from './app/app.module';
import { HttpClientService } from 'app/arquitetura/shared/services/http-client.service';
import { Storage } from 'app/arquitetura/shared/storage/storage';
import { SessaoService } from 'app/arquitetura/shared/services/seguranca/sessao.service';

console.log('Ambiente ativo: \'' + environment.envName + '\' ' +
	'[relative=' + environment.backendUrlFromFrontend + ', ' +
	 'https=' + environment.backendHttps + ', ' +
	 'port=' + environment.backendPort + ']');
if (environment.server) {
	enableProdMode();
}

const NOME_SISTEMA: string = 'SIOUV';
HttpClientService.setNomeSistema(NOME_SISTEMA);
Storage.setNomeSistema(NOME_SISTEMA);

SessaoService.init()
.then((result: number) => {
	switch (result) {
		case 0: { // SUCESSO
			platformBrowserDynamic().bootstrapModule(AppModule);
			break;
		}
		case 1: { // ERRO na chamada de Consulta das Configurações de Segurança no Back-end
			alert('[ERRO01] Ocorreu um erro ao invocar o módulo servidor (back-end) do sistema.');
			console.error('Ocorreu algum erro ao Consultar as Configurações de Segurança do back-end.');
			console.error('Em ambiente de desenvolvimento, no browser Chrome, eventualmente é ' +
				'necessário confirmar a autenticidade do certificado local de desenvolvimento ' +
				'para o browser liberar o acesso à url. Faça isso numa aba a parte, invocando ' +
				'a URL que gerou o erro.');
			break;
		}
		case 2: { // ERRO na chamada de Integração com o AIM Keycloak
			window.location.reload();
			break;
		}
		case 3: { // ERRO na chamada de Consulta do Usuário Logado no Back-end
			console.error('Ocorreu algum erro ao Consultar os dados do Usuário Logado do back-end.');
			alert('[ERRO03] Ocorreu um erro ao invocar o módulo servidor (back-end) do sistema.');
			break;
		}
	}
})
.catch(() => console.error('Ocorreu algum erro ao iniciar a aplicação.'));
//platformBrowserDynamic().bootstrapModule(AppModule);
