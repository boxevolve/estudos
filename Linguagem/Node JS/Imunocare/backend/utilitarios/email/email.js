// @arquivo: email.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const nodemailer = require('nodemailer');

module.exports = function(err, usuario, req, res, resetpass, forgot) {

    let email = undefined;

    if (err) return err;
 
    if (usuario.perfil === 'ENFE') {
        email = usuario.email_cliente;
    } else {
        email = usuario.email;
    }
    try {

        const primeiro_acesso = {
            subject: 'ImunoCare: dados de usuário',
            text: 'Informações sobre seu primeiro acesso', // plaintext body
            html: '<p><br>Caro ' + usuario.nome +
                '<br>Seu usuário foi criado com sucesso no aplicativo ImunoCare.' +
                '<br>Para começar a acessar o aplicativo, será necessário alterar sua senha durante seu primeiro acesso.' +
                '<br>' +
                '<br>Seus dados para acesso são:' +
                '<br>Email:  <strong>' + email + '</strong>' +
                '<br>Senha:  <strong>' + usuario.novasenha + '</strong>' +
                '<br>' +
                '<br>Acessar o endereço web https://imunocare.care para alterar a sua senha' +
                '<br>' +
                '<br>Obrigado!' +
                '<br>' +
                '<br>&copy; 2018 ImunoCare Ltda.' +
                '</p>'
        };

        const reset_password = {
            subject: 'ImunoCare: Restrição de acesso',
            text: 'Atribuição de nova senha',
            html: '<p><br>Caro ' + usuario.nome +
                '<br>Identificamos diversas tentativas de login com seu usuário' +
                '<br>Para sua segurança, seu acesso foi bloqueado e estamos lhe enviando uma nova senha para acesso' +
                '<br>Para voltar a utilizar o aplicativo, será necessário alterar sua senha durante seu primeiro acesso.' +
                '<br>' +
                '<br>Sua nova senha é: <strong>' + resetpass + '</strong>' +
                '<br>' +
                '<br>Acessar o endereço web https://imunocare.care para alterar a sua senha' +
                '<br>' +
                '<br>Obrigado!' +
                '<br>' +
                '<br>&copy; 2018 ImunoCare Ltda.' +
                '</p>'
        };

        const forgot_password = {
            subject: 'ImunoCare: Envio de nova senha temporária',
            text: 'Atribuição de nova senha',
            html: '<p><br>Caro ' + usuario.nome +
                '<br>Segue sua nova senha de acesso.' +
                '<br>Para sua segurança, seu acesso foi bloqueado e estamos lhe enviando uma nova senha' +
                '<br>Nós garantimos que o seu cadastro está seguro.' +
                '<br>Para voltar a utilizar o aplicativo, será necessário alterar sua senha durante seu primeiro acesso.' +
                '<br>' +
                '<br>Sua nova senha é: <strong>' + resetpass + '</strong>' +
                '<br>' +
                '<br>Acessar o endereço web https://imunocare.care para alterar a sua senha' +
                '<br>' +
                '<br>Obrigado!' +
                '<br>' +
                '<br>&copy; 2018 ImunoCare Ltda.' +
                '</p>'
        };

        // create reusable transporter object using the default SMTP transport
        //const transporter = nodemailer.createTransport('smtps://yowapps%40gmail.com:AppYow@12@smtp.gmail.com');
        //const transporter = nodemailer.createTransport('smtps://no_reply%40imunocare.com.br:no_reply@smtp.imunocare.com.br');

        // Create a SMTP transporter object
        let transporter = nodemailer.createTransport(
            {
                pool: true,
                host: 'smtp.imunocare.com.br',
                port: '587',
                secure: false,
                auth: {
                    user: 'no_reply@imunocare.com.br',
                    pass: 'Imuno2018'
                },
                logger: false,
                debug: false // include SMTP traffic in the logs
            },
            {
                // default message fields
                // sender info
                from: '"ImunoCare Serviços 👥" <no_reply@imunocare.com.br>'
            }
        );


        // verify connection configuration
        transporter.verify(function(error, success) {
            if (error) {
                console.log(error);
                return res.json({ message: 'Erro ao conectar a conta de email\n', error: error });
            } else {
                console.log('Servidor de email pronto para receber chamadas');
            }
        });

        let mailOptions = {
            //from: '"YowApps 👥" <yowapps@gmail.com>',
            from: '"ImunoCare Serviços 👥" <no_reply@imunocare.com.br>',
            to: email,
            cc: 'no_reply@imunocare.com.br',
            subject: "",
            text: "",
            html: ""
        }

        if (resetpass) {
            if (forgot === true) {
                mailOptions.subject = forgot_password.subject;
                mailOptions.text = forgot_password.text;
                mailOptions.html = forgot_password.html;
            } else {
                mailOptions.subject = reset_password.subject;
                mailOptions.text = reset_password.text;
                mailOptions.html = reset_password.html;
            }

        } else {

            mailOptions.subject = primeiro_acesso.subject;
            mailOptions.text = primeiro_acesso.text;
            mailOptions.html = primeiro_acesso.html;
        }

        // send mail with defined transport object
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log('Erro ao criar email:\n', error);
                //return req.flash('error_msg', 'Erro ao criar email');
            }
            //return req.flash('success_msg', 'Email enviado com sucesso');
            return info;
        });

    } catch (err) {
        //        return req.flash('error_msg', 'Erro ao enviar email');
        console.log('Erro ao criar email');
    }
};