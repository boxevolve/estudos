"use strict";

// @arquivo: crudDinamico.js
// @autor  : Clayton C. Carvalho
// @data   : 28.11.2017
// @projeto: ImunoApp

const moment = require('moment-timezone');
const mongoose = require('mongoose');

const c_e = "E",
      c_s = "S";

const querystring = require("querystring"),
      url = require("url");

const montaParams = (_url, param) => {
  let url_parts = url.parse(_url);
  if (param === "query") return querystring.parse(url_parts.query);
  //if (param === "options") return querystring.parse(url_parts.options);
};

function get_headerOrigem(req) {
  return req.headers["x-access-origem"];
}

function callback(req, res, data) {
  return res.json({
    contagem: data.length,
    retorno: data
  });
}

function returnError(req, res, err) {
  console.log(err.message);
  return res.status(400).json({
    mensagem: err.message, 
    error: err
  });
}

module.exports = function(modelDinamico, schema) {
  let modelArray = [modelDinamico];
  const crudDinamico = {
    carga: (req, res) => {
      let objArray = [schema],
          promise = [],
          contador = 0,
          total = 0,
          dataAll = [],
          errAll = [];

      objArray = req.body;
      modelArray = objArray;
      total = objArray.length;

      modelArray.forEach(item => {
        let modelo = new modelDinamico(item);
        promise.push(modelo.save());
      });
      
      Promise.all(promise)
        .then((data) => {
          contador = data.length;
          dataAll = data;
          return res.json({
            mensagem: `Registros criados ${contador} de um total de ${total}`,
            contagem: `${contador}/${total}`,
            retorno: dataAll,
            erros: errAll                    
          });
        })
        .catch((err) => {
          errAll = err;
          return res.json({
            mensagem: `Registros criados ${contador} de um total de ${total}`,
            contagem: `${contador}/${total}`,
            retorno: dataAll,
            erros: errAll                    
          });
        }); 
    },

    create: (req, res) => {
      if (!req.body._id) {
        req.body._id = new mongoose.Types.ObjectId();
      }
      
      let modelo = new modelDinamico(req.body);

      modelo.data = moment.tz(new Date(), "America/Sao_Paulo");
      modelo.criado_por = res.usuario.id

      modelo.save()
        .then(callback.bind(null, req, res))
        .catch(returnError.bind(null, req, res));
    },

    retrieve: (req, res, sort, fields, deepArray) => {
      let query = montaParams(req.url, "query");

      if (deepArray === undefined || deepArray.length === 0) {
        modelDinamico.find(query, fields, sort)
                     .exec()
                     .then( callback.bind(null, req, res) )
                     .catch( returnError.bind(null, req, res) );

      } else {
        modelDinamico.find(query, fields, sort)
                     .deepPopulate(deepArray)
                     .exec()
                     .then( callback.bind(null, req, res) )
                     .catch( returnError.bind(null, req, res) );
      }
    },

    findOne: (req, res, fields, deepArray) => {
      let query = montaParams(req.url, "query");

      if (deepArray.length === 0) {
        modelDinamico.findOne(query, fields)
                     .exec()
                     .then( callback.bind(null, req, res) )
                     .catch( returnError.bind(null, req, res) );
      } else {
        modelDinamico.findOne(query, fields)
                     .deepPopulate(deepArray)
                     .exec()
                     .then( callback.bind(null, req, res) )
                     .catch( returnError.bind(null, req, res) );
      }
    },

    update: (req, res, deepArray, model) => {
      let modelo = req.body,
          query = montaParams(req.url, "query"),
          options = {
            runValidators: 1,
            multi: true
          };

      let mod = { $set: model ? model : modelo };

      modelDinamico.update(query, mod, options)
          .exec()
          .then( callback.bind(null, req, res) )
          .catch( returnError.bind(null, req, res) );
    },

    delete: (req, res) => {
      let query = montaParams(req.url, "query");
      let tam = Object.keys(query).length;
      if (tam === 0) {
        returnError(req, res);
      } else {
        modelDinamico.remove(query)
          .then( callback.bind(null, req, res) )
          .catch( returnError.bind(null, req, res) );
      }
    }
  };

  return crudDinamico;
};