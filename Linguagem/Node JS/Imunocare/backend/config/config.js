// @arquivo: config.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const parametros = require('../ambiente/parametros')

const path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

const config = {
    development: {
        root: rootPath,
        app: {
            name: parametros.nomeApp
        },
        port: process.env.PORT || 3001,
        ports: 50441
    },

    test: {
        root: rootPath,
        app: {
            name: parametros.nomeApp
        },
        port: process.env.PORT || 3001,
        ports: 50441
    },

    production: {
        root: rootPath,
        app: {
            name: parametros.nomeApp
        },
        port: process.env.PORT || 3001,
        ports: 50441
    }

};

module.exports = config[env];