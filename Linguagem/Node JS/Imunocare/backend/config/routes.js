'use strict';

// @arquivo: routes.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp


module.exports = function(app) {

    const config = require('./config'),
        path = require('path'),

        colecoes_routes = path.join(config.root, '/colecoes'),

        LoginAPI = require(path.join(colecoes_routes, '/auth/login/login-route')),
        validarToken = require(path.join(colecoes_routes, '/auth/validarToken')),

        CampanhasAPI = require(path.join(colecoes_routes, '/dados/campanhas/campanha-route')),
        ElegiveisAPI = require(path.join(colecoes_routes, '/dados/elegiveis/elegivel-route')),
        EmpresaAPI  = require(path.join(colecoes_routes, '/dados/empresas/empresa-route')),
        EnderecoAPI  = require(path.join(colecoes_routes, '/dados/enderecos/endereco-route')),
        EventoAPI  = require(path.join(colecoes_routes, '/dados/eventos/evento-route')),
        GcmAPI      = require(path.join(colecoes_routes, '/shared/notificacoes/gcm-route')),
        CargaAPI    = require(path.join(config.root, '/cargas/arquivos/file')),
        ProdutosAPI = require(path.join(colecoes_routes, '/dados/produtos/produto-route')),
        UsuariosAPI = require(path.join(colecoes_routes, '/seguranca/usuario/usuario-route')),
        AgrupamentoAPI  = require(path.join(colecoes_routes, '/dados/agrupamento/agrupamento-route')),
        CargasAPI = require(path.join(colecoes_routes, '/dados/cargas/carga-route'));

//       app.use('**', validarToken);        

    app.use('/api/acesso', LoginAPI);

    app.use('/api/dados/campanhas', validarToken, CampanhasAPI);
    app.use('/api/dados/elegiveis', validarToken, ElegiveisAPI);
    app.use('/api/dados/empresas', validarToken, EmpresaAPI);    
    app.use('/api/dados/enderecos', validarToken, EnderecoAPI);    
    app.use('/api/dados/eventos', validarToken, EventoAPI);    
    app.use('/api/dados/produtos', validarToken, ProdutosAPI);
    app.use('/api/dados/cargas', validarToken, CargasAPI) ;
    app.use('/api/dados/agrupamento', validarToken, AgrupamentoAPI);
    app.use('/api/file', validarToken, CargaAPI);
    app.use('/api/seguranca/usuarios', validarToken, UsuariosAPI);
    app.use('/api/shared/notificacoes', validarToken, GcmAPI);
};