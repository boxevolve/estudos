const express = require('express');
const _router = express.Router();
const multer = require('multer');
const path = require('path');
const fs  = require('fs');

const convertExcel = require('excel-as-json').processFile
const convertJson = require('json2xls');
const moment = require('moment-timezone');     

const options = {
    sheet:'1',
    isColOriented: false,
    omitEmtpyFields: false
}

const cargaSchema = require("../../colecoes/dados/cargas/carga-schema"),
      modelCarga = require("../../colecoes/dados/cargas/carga-model")(cargaSchema, "cargas"),
      campanhaSchema = require("../../colecoes/dados/campanhas/campanha-schema"),
      modelCampanha = require("../../colecoes/dados/campanhas/campanha-model")(campanhaSchema, "campanhas");

const crudCargaCustom = require('../../colecoes/dados/cargas/carga-crud-custom');

const config = require('../../config/config');



const formatarData = function(data) {
    let d   = new Date(data),
        mes = '' + (d.getMonth() + 1),
        dia = '' + d.getDate(),
        ano = d.getFullYear()

    if (mes.length < 2) mes = '0' + mes;
    if (dia.length < 2) dia = '0' + dia;

    return [dia, mes, ano].join('-'); // "join" é o caracter para separar a formatação da data, neste caso, a barra (/)
}

const store = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null, path.join(config.root, './FILES/upload'));
    },
    filename:function(req, file, cb){
        cb(null, (formatarData(Date.now())+'-'+file.originalname));
    }
});


const upload = multer({ storage: store }).single('file');


_router.post('/importarFilial', function(req, res){
    
    upload(req, res, function(err, newFile){
        if(err){
            return res.status(501).json({error:err});
        }

        req.file.filename;

        const fileJSON = 'a.json'                        
        const fileOrigem = path.join(config.root, './FILES/upload/')+req.file.filename
        const fileDest = path.join(config.root, './FILES/convertidos/')+fileJSON

        convertExcel (fileOrigem, undefined, undefined, (err, data) => {
            data;
        })

        convertExcel(fileOrigem, fileDest, options, (err, result) => {
            if(err) {
                return res.json({error: err});
            }
            result;
          
            })

    })
});



_router.post('/upload', (req, res, next) => {

    upload(req, res, function(err, newFile){

        modelCampanha.findById(req.body.campanha).then(campanhaBanco => {

            if (err) {
                return res.status(501).json({ error: err });
            }

            const filename = req.file.filename.split('.')

            let carga = new modelCarga({
                campanha: req.body.campanha,
                nomecampanha: campanhaBanco.campanha,
                tipo: req.body.tipo,
                status: 'Carregado',
                usuario: res.usuario._id.toString()
            })

            const fileJSON = 'U' + carga.usuario + '-C' + carga.campanha + '-' + filename[0] + '.json'
            carga.filename = fileJSON

            carga.save()
                .then(data => {

                    let nomeArquivo = moment().format('DD-MM-YYYY-T-HH:mm') + req.file.filename.substring(10).replace('.xlsx', '-erros.xlsx')

                    let cargaArquivoConvertido = new modelCarga({
                        usuario: res.usuario._id.toString(),
                        campanha: carga.campanha,
                        nomecampanha: carga.nomecampanha,
                        tipo: carga.tipo,
                        filename: nomeArquivo,
                        originalname: req.file.filename,
                        status: 'Em processamento',
                        data: moment.tz(new Date(), "America/Sao_Paulo").toDate().toISOString(),
                        docs_total: 0,
                        docs_erros: 0
                    })

                    cargaArquivoConvertido.save().then(dado => {

                        console.log(`Campanha "${campanhaBanco.campanha}": Inicio importaçao ${req.body.tipo}`)

                        const fileOrigem = path.join(config.root, './FILES/upload/') + req.file.filename
                        const fileDest = path.join(config.root, './FILES/convertidos/') + fileJSON

                        try {
                            convertExcel(fileOrigem, fileDest, options, (err, result) => {
                                if (err) {
                                    return res.json({ error: err });
                                }
                                /* */
                                crudCargaCustom.validacao(req, res, campanhaBanco, req.body.tipo, result, (inv) => {
                                    let elegiveis = []

                                    inv.erros.forEach(element => {
                                        let elegivel = element.data;
                                        elegivel.erro = '';
                                        element.errors.forEach(element => elegivel.erro += element + '; ')
                                        elegiveis.push(elegivel);
                                    });

                                    if (elegiveis.length > 0) {
                                        let xls = convertJson(elegiveis);
                                        fs.writeFile((path.join(config.root, './FILES/naovalidados/') + cargaArquivoConvertido.filename), xls, 'binary', (err) => {
                                            if (err) throw err;
                                          });
                                    }

                                    let resposta = {
                                        sucesso: elegiveis.length == 0,
                                        nomeArquivo: cargaArquivoConvertido.filename
                                    };

                                    cargaArquivoConvertido.status = resposta.sucesso ? 'Processado com sucesso' : 'Processado com erros';
                                    cargaArquivoConvertido.docs_erros = elegiveis.length;
                                    cargaArquivoConvertido.docs_total = inv.total;

                                    cargaArquivoConvertido.save().then(
                                        (cargaFinal) => {
                                            [
                                                `Fim importação ${req.body.tipo}`, 
                                                `Total Registros ${cargaFinal.docs_total}`, 
                                                `N° Válidos ${(cargaFinal.docs_total - cargaFinal.docs_erros)}`, 
                                                `N° Inválidos ${cargaFinal.docs_erros}`,
                                            ].forEach(log => console.log(log))
                                        }
                                    );

                                    return res.json(resposta);
                                })
                            })


                        } catch (err) {
                            return res.json({ error: err });
                        }
                    })
                })
                .catch(err => {
                    return res.status(412).json({ error: err })
                })
        })
    });
});


_router.post('/download', async function(req,res,next){
    let folder = req.body.folder ? req.body.folder : 'upload'
    let filepath = path.join(config.root, `./FILES/${folder}`) +`/${req.body.filename}`;
    res.sendFile(filepath);
});






module.exports = _router;