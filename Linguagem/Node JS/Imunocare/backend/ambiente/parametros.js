'use strict';

const parametros = {
    nomeApp: 'imunoCare',
    segredo: 'CamaDeGatoTambemQuebra',
    SALT_WORK_FACTOR: 9,
    MAX_LOGIN_ATTEMPTS: 5,
    LOCK_TIME: 2 * 60 * 60 * 1000
}

module.exports = parametros; 