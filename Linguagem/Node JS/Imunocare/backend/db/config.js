// @arquivo: config.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

module.exports = function(app) {

  const dbserver = require("../environments/environment")(app);

  const mongoose = require('mongoose');
  // mongoose.set('debug', true);
  mongoose.Promise = global.Promise // Não exibe a mensagem de advertência sobre o suporte à Promises do mongoose

  const usuarioSchema = require("../colecoes/seguranca/usuario/usuario-schema"),
    modelUsuario = require("../colecoes/seguranca/usuario/usuario-model")(usuarioSchema, "usuarios");

  //	const strconn  = 'mongodb://yowappdevw.southcentralus.cloudapp.azure.com/botox';
  // const strconn = `mongodb://icmongoproxy/imunocare`;
  const strconn = `mongodb://imunocare:imunocare2019@ds163354.mlab.com:63354/mongo-imunocare`;
  //const strconn = `mongodb://127.0.0.1/imunocare-local`;
  //const strconn = `mongodb://127.0.0.1/imunocare`;

  //const strconn = `mongodb://admin:OVIkrn78195@node176765-app-imunocare.jelasticlw.com.br/imunocare`;

  //console.log(strconn);

  const options = {
    
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 50, // Maintain up to 50 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    useNewUrlParser: true
  };


  var promise = mongoose.connect(strconn, options);

  promise.then(function (db) {

    /* Use `db`, for instance `db.model()` */
  });

  // mongoose.connection.on('connected', function() {
  mongoose.connection.on('connected', function () {
    console.log('Mongoose: Conexão Default aberta para ', strconn);
  });
  mongoose.connection.on('error', function (err) {
    console.log('Mongoose: Erro na Conexão Default: ', err);
  });
  mongoose.connection.on('disconnected', function () {
    console.log('Mongoose: Conexão Default desconectada');
  });
  mongoose.connection.on('open', function () {
    console.log('Mongoose: Conexão Default iniciada ');
  });

  process.on('SIGINT', function () {
    mongoose.connection.close(function () {
      console.log('Mongoose: Conexão Default perdida pelo App');
      process.exit(0);
    });
  });

  modelUsuario.count((err, count) => {
    if (count === 0) {
      const usuario = new modelUsuario({
        "usuario": "ImunoCare",
        "nome": "ImunoCare",
        "email": "desenvolvimento@imunocare.com.br",
        "senha": "Imuno2018",
        "perfil": "ADMIN",
        "primeiro_acesso": false,
        "email_enviado": false,
      });

      usuario.save((err, data) => {
        if (err) return console.log('Erro ao criar usuário admin')
        return console.log('Usuário: ', usuario);
      });
    }
  });

}