// @arquivo: messages.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const data_messages = {

    campo_obrigatorio: () => {
        return 'Campo Obrigatório';
    },

    campo_unico: () => {
        return 'Valor do campo duplicado!';
    },

    indice_unico: () => {
        return 'Indice duplicado!';
    },

    email_invalido: () => {
        return 'Email não informado ou inválido';
    },

    data_invalida: () => {
        return 'Data inválida';
    },

    nome: () => {
        return 'Nome obrigatório';
    },

    fone: () => {
        return 'Fone obrigatório';
    },
    sysop: () => {
        return 'Mobile SO obrigatório';
    },
    token_gcm: () => {
        return 'Mobile Token obrigatório';
    },
    cpf_invalido: () => {
        return 'CPF não informado ou inválido';
    },
    cnpj_invalido: () => {
        return 'CNPJ não informado ou inválido';
    },
    cep_invalido: () => {
        return 'CPF não informado ou inválido';
    },
    campo_fone_invalido: () => {
        return 'Formato campo Fone inválido';
    },
    tipo_parceiro_invalido: () => {
        return 'Tipo de Parceiro deve ser Cliente(C) ou Fornecedor(F)';
    }

};

module.exports = data_messages;