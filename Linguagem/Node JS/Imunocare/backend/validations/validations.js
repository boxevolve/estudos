// @arquivo: validations.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

function validarCalculoCPF(cpf) {
    cpf = cpf.replace(/[^\d]+/g, '');
    if (cpf == '') return false;
    // Elimina CPFs invalidos conhecidos	
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999")
        return false;
    // Valida 1o digito	
    let add = 0, rev = 0;
    for (let i = 0; i < 9; i++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(9)))
        return false;
    // Valida 2o digito	
    add = 0;
    for (let i = 0; i < 10; i++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return false;
    return true;   
}

function validarCalculoCNPJ(cnpj) {

    cnpj = cnpj.replace(/[^\d]+/g, '');

    if (cnpj == '') return false;

    if (cnpj.length != 14)
        return false;

    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
        return false;

    // Valida DVs
    let tamanho = cnpj.length - 2
    let numeros = cnpj.substring(0, tamanho);
    let digitos = cnpj.substring(tamanho);
    let soma = 0;
    let pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
        return false;

    return true;
}

function normalizarSequencia(sequencia, valoresParaNormalizacao, tamanhoMaximo, subTamanhoMaximo) {
    valoresParaNormalizacao.forEach(valor => sequencia = sequencia.split(valor).join(''))
    while(sequencia.length < tamanhoMaximo) sequencia = `${subTamanhoMaximo}` + sequencia
    if(sequencia.length > tamanhoMaximo) sequencia = sequencia.substr(0, tamanhoMaximo);
    return sequencia
}

const data_validation = {

    validarEmail: email => {
        const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(email);
    },

    validarFone: fone => {
        const re = /^\+([0-9]{2})\([0-9]{2}\)([0-9]{5})-([0-9]{4})+$/;
        if(fone)
        return re.test(fone);
        else return true;
    },

    validarCEP: cep => {
        //        const re = /^([0-9]{4})([1-9]{1})([0-9]{3})+$/;
        // const re = /^([0-9]{5})-([0-9]{3})*$/ ;
        const re = /^([0-9]{8})*$/ ;
        return re.test(cep);
    },

    validarCPF: cpf => {
        return validarCalculoCPF(cpf);
    },

    validarCNPJ: cnpj => {
        if(cnpj) return validarCalculoCNPJ(cnpj);
        else return true;
    },

    setCep: cep => {
        if (cep) {
            return cep.replace("-", "");
        } else {
            return '';
        }
    },

    getCep: cep => {
        if (cep) {
            return (cep.substr(0, 5) + "-" + cep.substr(-3)); 
        } else {
            return '';
        }
    },

    getCPF: cpf => {
        cpf = normalizarSequencia(cpf, ['-', '.'], 11, '0')
        return (cpf.substr(0, 3) + '.' + cpf.substr(3, 3) + '.' + cpf.substr(6, 3) + '-' + cpf.substr(-2))
    },

    setCPF: cpf => {
        return normalizarSequencia(cpf, ['-', '.'], 11, '0')
    },

    
    getCNPJ: cnpj => {
        cnpj = normalizarSequencia(cnpj, ['-', '.', '/'], 14, '0')
        return (cnpj.substr(0, 2) + '.' + cnpj.substr(2, 3) + '.' +cnpj.substr(5, 3) + '/' + cnpj.substr(8, 4) + '-' + cnpj.substr(-2))
    },

    setCNPJ: cnpj => {
        return normalizarSequencia(cnpj, ['-', '.', '/'], 14, '0')
    },


    setCnae: cnaeIn => {
        let cnaeOut = cnaeIn.split('-').join('');
        cnaeOut = cnaeOut.split('/').join('');
        return cnaeOut;
    },

    getCnae: cnaeIn => {
        let cnaeOut = cnaeIn.split('-').join('');
        cnaeOut = cnaeOut.split('/').join('');
        cnaeOut = (cnaeIn.substr(0, 4) + '-' + cnaeIn.substr(4, 1) + '/' + cnaeIn.substr(5, 2));
        return cnaeOut;
    },

    setLowerCase: x => {
        return x.toLowerCase();
    },

    setUpperCase: x => {
        return x.toUpperCase();
    },

    validarData: data => {
        if(data) return data instanceof Date;
        return true;
    },

    formatarData: data => {
        if (data !== undefined) {
            //let pattern = /(\d{2})[./](\d{2})[./](\d{4})/;
            //let retorno = new Date(data.replace(pattern, '$3-$2-$1'));
            // let retorno = new Date(data).toLocaleDateString("pt-BR");
            return new Date(data).toISOString();
        }
    },

    validarTipoParceiro: tipo => {
        let tp = /C|D|F/;
        return tp.test(tipo);
    },

    validarStatusCampanha: status => {
        let sts = /Nova|Andamento|Encerrada/;
        return sts.test(status);
    },

    validarPerfilUsuario: perfil => {
        let prf = /ADMIN|CLIE|ENFE|FORN|GEST/;
        return prf.test(perfil);
    }

};

module.exports = data_validation;