"use strict";

// @arquivo: login-crud.js
// @autor  : Clayton C. Carvalho
// @data   : 31.10.2016
// @projeto: YowAppartment

const c_e = "E",
    c_s = "S";

    
const usuarioSchema = require('../../seguranca/usuario/usuario-schema'),
    modelUsuario = require('../../seguranca/usuario/usuario-model')(usuarioSchema, 'usuarios'),
    enviarEmail = require('../../../utilitarios/email/email'),
    randomstring = require('randomstring');

function get_headerOrigem(req) {
    return req.headers["x-access-origem"];
}

function callback(req, res, msg, num, data) {
    //res.setHeader('x-access-token', data.token, 'Bearer token_type="JWT"');
    let codeStatus = num | 200;

    return res.status(codeStatus).json({
       // usuario: data,
        token: data.token,
        expires: data.expires,
        primeiro_acesso: data.primeiro_acesso
    });
}

function returnError(req, res, msg, err) {
    if (err) {
        return res.status(403).json({
            mensagem: msg,
            error: err
        });
    }

    return res.json({
        mensagem: msg
    });
}

const crudAuth = {

    login: (req, res) => {
        const body = req.body;

       // console.log(`LOGIN\n${req.body.email}: ${req.body.senha}`);

        // @ts-ignore
        modelUsuario.getAuthenticated(body.email, body.senha)
            .then(callback.bind(null, req, res, 'Login efetuado com sucesso', 202))
            .catch(returnError.bind(null, req, res, 'Ocorreu um erro de Login. Procure o suporte'));
    }, 

    changePassword: (req, res) => {
        const body = req.body;

        // @ts-ignore
        modelUsuario.getAuthenticated(body.email, body.senha)
            .then((usuario) => {
                // @ts-ignore
                modelUsuario.alteraSenha(usuario, body.novasenha)
                    .then(() => {
                        modelUsuario.getAuthenticated(body.email, body.novasenha)
                            .then(callback.bind(null, req, res, 'Alteração de Senha e Login efetuados com sucesso', 202))
                            .catch(returnError.bind(null, req, res, 'Ocorreu um erro de Login. Procure o suporte'));
                    })
                    .catch(returnError.bind(null, req, res, 'Erro ao alterar senha do usuário'));                
            })
            .catch(returnError.bind(null, req, res, 'Ocorreu um erro de Login. Procure o suporte'));
    },

    forgot: (req, res) => {
        const body = req.body;
        let novasenha = randomstring.generate(8);
        //console.log("SENHA TEMP: " + novasenha);
        modelUsuario.findOne({ 'email': body.email })
            .then( usuario => {
                if (usuario) {
                    modelUsuario
                        // @ts-ignore
                        .alteraSenha(usuario, novasenha, true)
                        .then((data) => {
                                enviarEmail(null, data, req, res, novasenha, true);
                                res.status(200).json({mensagem: `Senha alterada com sucesso e enviada por email ${usuario.email}`});
                        })
                        .catch(
                            returnError.bind(null, req, res, 'Erro ao processar nova senha. Procure o suporte'));
                } else {
                    return res.status(403).json({
                        mensagem: 'email não cadastrado'
                    });
                }
            })
            .catch(returnError.bind(null, req, res, 'email não cadastrado'));
    },

    logout: (req, res) => {

        modelUsuario.findOne({ 'email': res.usuario.email })
        .then( usuario => {
            // res.removeHeader("x-access-token");
            return res.status(200).json({
                mensagem: `Usuário ${usuario.email} desconectado. Sessão encerrada`
                })
        })
        .catch( err => {
            return res.status(400).json({
                mensagem: `email não cadastrado`
            })
        })
        
    }
};

module.exports = crudAuth;