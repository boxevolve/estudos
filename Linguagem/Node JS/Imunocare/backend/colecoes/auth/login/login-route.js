// @arquivo: login-route.js
// @autor  : Clayton C. Carvalho
// @data   : 27.01.2018
// @projeto: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();

//const deepArray =[ "pessoa", "idioma", "acesso.grupo", "acesso.grupo.perfis.perfil", "acesso.grupo.perfis.perfil.permissoes.permissao"];
    const config = require('../../../config/config'),
        path = require('path'),
        colecoes_routes = path.join(config.root, '/colecoes'),
        validarToken = require(path.join(colecoes_routes, '/auth/validarToken'));

const crudLogin = require('./login-crud');

router.route('/login').post((req, res, next) => {
    crudLogin.login(req, res);
});

router.route('/changePassword').post((req, res, next) => {
    //router.post('/changePassword', function(req, res, next) {
        crudLogin.changePassword(req, res);
});

router.route('/forgot').post((req, res, next) => {
    crudLogin.forgot(req, res);
});

router.route('/logout').post(validarToken, (req, res, next) => {
    crudLogin.logout(req, res);
});

module.exports = router;