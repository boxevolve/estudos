// @arquivo: validarToken.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const usuarioSchema = require('../../colecoes/seguranca/usuario/usuario-schema'),
    modelUsuario = require('../../colecoes/seguranca/usuario/usuario-model')(usuarioSchema, 'usuarios'),
    jwt = require('jwt-simple'),
    parametros = require('../../ambiente/parametros');

const c_e = 'E';
const segredo = parametros.segredo;

const returnError = function(req, res, next, err) {
    res.status(404).json({ message: "Erro ao identificar proprietário do token.", erro: err });
    //next();
};

const callback = function(req, res, next, data) {
    if (data.isLocked) {
        res.status(402).json({ message: 'Acesso bloqueado.' });
    }
    res.usuario = data;
    next();
};

const verificaIAC = function(perfil, url) {
    
    if(perfil!=='ENFE') {
        return false;
    } else {
        if(url === '/api/dados/campanhas/dadosCampanha' ||
            url === '/api/dados/elegiveis/dadosCampanha' ||
            url === '/api/dados/elegiveis/registraAplicacao' ||
            url === '/api/seguranca/usuarios/verificaAcesso' || 
            url === '/api/seguranca/usuarios/buscaDadosUsuario' ||
            url === '/api/seguranca/usuarios/getCampanhas' ||
            url === '/api/dados/elegiveis/sincronizaImunizacoes' || 
            url === '/api/dados/elegiveis/buscaElegiveisPorCPF' ||
            url === '/api/dados/elegiveis/create' ||
            url === '/api/acesso/logout') {
            return false;
        } else {
            return true;
        }
    }
    
};



module.exports = function(req, res, next) {

    let url = new String().concat(req.baseUrl, req.path);

    const token = req.headers["x-access-token"];

    if (!token) {
        res.setHeader('x-access-token', 'token_type="JWT"');
        res.status(401).json({message: 'Você precisa se autenticar.'});

    } else {
        try {
            const decoded = jwt.decode(token, segredo);

            if (decoded.exp <= Date.now() || !decoded.iac || verificaIAC(decoded.iac, url)) {
                return res.status(403).json({ message: 'Não autorizado.' });
            }          

            modelUsuario.findOne({ _id: decoded.iss })
                .exec()
                .then(callback.bind(null, req, res, next))
                .catch(returnError.bind(null, req, res, next));
                
        } catch (err) {
            res.status(403).json({message: 'Token inválido. Você precisa se autenticar.'});
        }
    }
};

