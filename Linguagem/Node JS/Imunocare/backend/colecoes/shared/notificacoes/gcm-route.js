// @arquivo: gcm-route.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const c_e = 'E',
    c_s = 'S';

const crudGcm = require('./gcm-crud');
//const validaToken = require("../../login/validarToken");

//router.route('/create').post(validaToken, (req, res, next) => {
router.route('/create').post((req, res, next) => {

    crudGcm.create(req, res);
});

//router.route('/retrieve').get(validaToken, (req, res, next) => {
router.route('/retrieve').get((req, res, next) => {
    //router.get('/retrieve', function validaToken(req, res, next) {
    crudGcm.retrieve(req, res);
});

//router.route('/findOne').get(validaToken, (req, res, next) => {
router.route('/findOne').get((req, res, next) => {
    //router.get('/findOne', function(req, res, next) {
    crudGcm.findOne(req, res);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/update').put((req, res, next) => {
    //router.put('/update', function(req, res, next) {
    crudGcm.update(req, res);
});

//router.route('/delete').delete(validaToken, (req, res, next) => {
router.route('/delete').delete((req, res, next) => {
    crudGcm.delete(req, res);
});

module.exports = router;