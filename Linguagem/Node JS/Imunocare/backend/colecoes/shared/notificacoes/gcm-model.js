'use strict';

// @arquivo: gcm-model.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const refPromises = require('mongoose-ref-promises');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const deepPopulate = require('mongoose-deep-populate')(mongoose);

module.exports = function(_schema, colecao) {

    const gcmSchema = _schema;

    const c_e = 'E',
        c_s = 'S';

    gcmSchema.retainKeyOrder = true;

    gcmSchema.index({
        sysop: 1,
        token_gcm: 1
    }, {
        unique: true
    });
    gcmSchema.index({
        push_ativo: 1,
        grupo_gcm: 1,
        token_gcm: 1
    }, {
        unique: false
    });

    gcmSchema.statics.altera_status_push = function(id, habilita_push, cb) {
        this.findOne({
            id: id
        }, function(err, gcm_token) {
            if (err) return cb(err);
            // make sure the user exists
            if (!gcm_token) {
                return cb(null, null, mensagens.lerMensagem(c_e, 2));
            }
            // reset attempts and lock info
            const updates = {
                $set: {
                    push_ativo: habilita_push
                }
            };

            return gcm_token.update(updates, function(err) {
                if (err) return cb(err);
                return cb(null, gcm_token, mensagens.lerMensagem(c_s, 3));
            });
        });
    };

    gcmSchema.statics.criar_grupo_tokens = function(ids, nome_grupo, cb) {
        return this.update({
            id: {
                '$in': ids
            }
        }, {
            grupo_gcm: nome_grupo
        }, {
            multi: 1
        }, cb);
    };

    gcmSchema.plugin(refPromises);
    gcmSchema.plugin(beautifyUnique);

    return mongoose.model(colecao, gcmSchema);
};