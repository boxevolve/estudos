'use strict';

// @arquivo: gcm-schema.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

// Constantes do Mongoose
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fields = require('../../fields/fields'),

    sysop = fields.cmpStringObrigatorio(),
    push_ativo = fields.cmpString(),
    grupo_gcm = fields.cmpString(),
    token_gcm = fields.cmpStringObrigatorio();

// Criando Schema
const jsonGCM = {
    token_gcm,
    push_ativo,
    grupo_gcm,
    sysop
};


//module.exports = schemaMedico;
module.exports = new Schema(jsonGCM);