'use strict';

// @arquivo: gcm-crud.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: Yow-botox

const c_e = 'E',
    c_s = 'S';

const gcmSchema = require('./gcm-schema'),
    modelGCM = require('./gcm-model')(gcmSchema, 'gcmtoken'),
    querystring = require('querystring'),
    url = require('url');

// enviarEmail = require('../email/email');

const callback = (err, data, res, _req) => res.json(data || err);

const montaParams = (_url, param) => {
    let url_parts = url.parse(_url);
    if (param === 'query') return querystring.parse(url_parts.query);
    if (param === 'options') return querystring.parse(url_parts.options);
};

const crudGCM = {
    create: (req, res) => {
        const body = req.body;
        let novogcm = new modelGCM(body);
        let query = {
            token_gcm: novogcm.token_gcm,
        };

        modelGCM.findOne(query, function(err, data) {
            if (err) {
                console.log(mensagens.lerMensagem(c_e, 19).mensagem);
                return res.json({
                    mensagem: mensagens.lerMensagem(c_e, 19),
                });
            }
            if (!data) {
                if (novogcm.sysop === 'IOS' || novogcm.sysop === 'Android') {
                    novogcm.save((err, data) => callback(err, data, res, req));
                } else {
                    return res.json({
                        mensagem: mensagens.lerMensagem(c_e, 15),
                    });
                }
            } else {
                console.log(mensagens.lerMensagem(c_e, 18).mensagem);
                return res.json({
                    mensagem: mensagens.lerMensagem(c_e, 18),
                });
            }
        });
    },

    retrieve: (req, res) => {
        let query = montaParams(req.url, 'query'),
            fields = {};

        // query += '{medico: 1}'

        modelGCM.find(query, fields, (err, data) => callback(err, data, res, req));
    },

    findOne: (req, res) => {
        let query = montaParams(req.url, 'query'),
            fields = {
                senha: 0,
            };

        modelGCM.findOne(query, fields, (err, data) =>
            callback(err, data, res, req)
        );
    },

    update: (req, res) => {
        let mod = req.body,
            query = montaParams(req.url, 'query'),
            options = {
                runValidators: 1,
            };

        console.log('QUERY: ', query, '\nOPTIONS: ', mod, '\nMOD: ', options);

        modelGCM.update(query, mod, options, (err, data) =>
            callback(err, data, res, req)
        );
    },

    delete: (req, res) => {
        let query = montaParams(req.url, 'query');
        let tam = Object.keys(query).length;
        if (tam === 0) {
            return res.render('error', {
                message: 'Nenhuma seleção informada para exclusão. Operação cancelada',
                error: {},
            });
        }
        modelGCM.remove(query, (err, data) => callback(err, data, res, req));
    },
};

module.exports = crudGCM;