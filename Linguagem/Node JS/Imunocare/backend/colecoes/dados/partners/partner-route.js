// @file: partner-route.js
// @author: Elcio Rodrigo
// @date: 22.11.2018
// @project: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const deepArray = ['endereco', 'criado_por'];

const crudEmpresa = require('./empresa-crud'),
    crudEmpresaCustom = require('./empresa-crud-custom');

router.route('/create').post((req, res, next) => {

    crudEmpresa.create(req, res);
});

router.route('/retrieve').get((req, res, next) => {
    const sort = {},
        fields = {};
    crudEmpresaCustom.retrieve(req, res, sort, fields, deepArray);
});

router.route('/listafornecedores').get((req, res, next) => {
    crudEmpresaCustom.listafornecedores(req, res, deepArray);
});

router.route('/listaClientes').get((req, res, next) => {
    crudEmpresaCustom.listaClientes(req, res, deepArray);
});

router.route('/findOne').get((req, res, next) => {
    const fields = {};
    crudEmpresa.findOne(req, res, fields, deepArray);
});

router.route('/update').patch((req, res, next) => {
    crudEmpresa.update(req, res);
});

router.route('/delete').delete((req, res, next) => {
    crudEmpresa.delete(req, res);
});

module.exports = router;