// @file: partner-crud.js
// @author: Elcio Rodrigo
// @date: 22.11.2018
// @project: ImunoApp

"use strict";

const empresaSchema = require("./empresa-schema"),
modelEmpresa = require("./empresa-model")(empresaSchema, "empresas"),
crudDinamico = require("../../../utilitarios/crud/crud_dinamico")(modelEmpresa, empresaSchema);

module.exports = crudDinamico;