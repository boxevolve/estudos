// @file: partner-schema.js
// @author: Elcio Rodrigo
// @date: 22.11.2018
// @project: ImunoApp

'use strict';

const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fields = require('../../fields/fields');

//TODO fild=type need to be enum
const jsonPartner = {
    company: fields.cmpString(),
    cnpj: fields.cmpCNPJ(),
    type: fields.cmpTipoEmpresa(),
    responsible: fields.cmpString(),
    email: fields.cmpString(),
    phone: fields.cmpString20(),
    cep: fields.cmpCep(),
    address: fields.cmpRefId('enderecos', true),
    number: fields.cmpNumber(),
    complement: fields.cmpString20(),
    created_by: fields.cmpRefId('usuarios', true),
    creation_date: fields.cmpData()
};

module.exports = new Schema(jsonPartner); 
