// @file: partner-model.js
// @author: Elcio Rodrigo
// @date: 22.11.2018
// @project: ImunoApp

'use strict';

const mongoose = require('mongoose');
const refPromises = require('mongoose-ref-promises');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

module.exports = function(_schema, colection) {

    const partnerSchema = _schema;

    partnerSchema.retainKeyOrder = true;

    partnerSchema.index({
        'company': 1,
        'cnpj': 1,
        'type': 1,
    }, {
        unique: true
    });

    partnerSchema.plugin(refPromises);
    partnerSchema.plugin(beautifyUnique);

    return mongoose.model(colection, partnerSchema);

};
