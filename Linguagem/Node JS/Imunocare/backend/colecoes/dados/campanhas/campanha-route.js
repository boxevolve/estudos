// @arquivo: campanha-route.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const c_e = 'E',
    c_s = 'S';

const deepArray = [
  'filiais.unidades.endereco',
  'cliente',
  'cliente.endereco',
  'distribuidor',
  'gestor',
  'fornecedores.fornecedor', 
  'fornecedores.fornecedor.endereco', 
  'agrupamento',
  'produtos.produto',
  'criado_por'
];

const crudCampanha = require('./campanha-crud');
const crudCampanhaCustom = require('./campanha-crud-custom');
//const validaToken = require("../../login/validarToken");

//router.route('/create').post(validaToken, (req, res, next) => {
router.route('/create').post((req, res, next) => {
  crudCampanha.create(req, res);
});

//router.route('/retrieve').get(validaToken, (req, res, next) => {
router.route('/retrieve').get((req, res, next) => {
  //router.get('/retrieve', function validaToken(req, res, next) {
  const sort = {},
  fields = {};
  crudCampanhaCustom.retrieve(req, res, sort, fields, deepArray);
});

router.route('/getNotCadAuto').get((req, res, next) => {
  const sort = {},
  fields = { 'cadastro_auto' : false };
  crudCampanhaCustom.retrieve(req, res, sort, fields, deepArray);
});

router.route('/dadosCampanha').get((req, res, next) => {
  //router.get('/retrieve', function validaToken(req, res, next) {
  const sort = {},
  fields = {};
  crudCampanhaCustom.dadosCampanha(req, res);
});

//router.route('/findOne').get(validaToken, (req, res, next) => {
router.route('/findOne').get((req, res, next) => {
  //router.get('/findOne', function(req, res, next) {
  const fields = {};      
  crudCampanha.findOne(req, res, fields, deepArray);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/update').patch((req, res, next) => {
  crudCampanha.update(req, res);
});

//router.route('/delete').delete(validaToken, (req, res, next) => {
router.route('/delete').delete((req, res, next) => {
  crudCampanha.delete(req, res);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/insereProdutos').put((req, res, next) => {
  crudCampanhaCustom.insereProdutos(req, res);
});

router.route('/removeProdutos').delete((req, res, next) => {
  crudCampanhaCustom.removeProdutos(req, res);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/insereLotes').put((req, res, next) => {
  crudCampanhaCustom.insereLotes(req, res);
});

router.route('/removeLotes').delete((req, res, next) => {
  crudCampanhaCustom.removeLotes(req, res);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/insereClinicas').put((req, res, next) => {
  crudCampanhaCustom.insereClinicas(req, res);
});

router.route('/removeClinicas').delete((req, res, next) => {
  crudCampanhaCustom.removeClinicas(req, res);
});
//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/insereRoteiros').put((req, res, next) => {
  crudCampanhaCustom.insereRoteiros(req, res);
});

router.route('/removeRoteiros').delete((req, res, next) => {
  crudCampanhaCustom.removeRoteiros(req, res);
});
//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/insereElegiveis').put((req, res, next) => {
  crudCampanhaCustom.insereElegiveis(req, res);
});

router.route('/removeElegiveis').delete((req, res, next) => {
  crudCampanhaCustom.removeElegiveis(req, res);
});

module.exports = router;