"use strict";

// @arquivo: campanha-crud.js
// @autor  : Clayton C. Carvalho
// @data   : 17.02.2018
// @projeto: ImunoApp

const campanhaSchema = require("./campanha-schema"),
modelcampanha = require("./campanha-model")(campanhaSchema, "campanhas"),
crudDinamico = require("../../../utilitarios/crud/crud_dinamico")(modelcampanha, campanhaSchema);

module.exports = crudDinamico;