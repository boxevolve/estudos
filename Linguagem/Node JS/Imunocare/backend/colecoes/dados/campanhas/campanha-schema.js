'use strict';

// @arquivo: campanha-schema.js
// @autor  : Clayton C. Carvalho
// @data   : 17.02.2018
// @projeto: ImunoApp

// Constantes do Mongoose
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fields = require('../../fields/fields');

    // Campos do Schema

const jsonCampanha = {
    campanha: fields.cmpString60(),
    status: fields.cmpStatusCampanha(), 
    geracao: {
        step: fields.cmpString(),
        status: fields.cmpStatusGeracaoCampanha() 
    },
    cliente: fields.cmpRefId('empresas'),
    distribuidor: fields.cmpRefId('empresas'),
    responsavel_cliente: fields.cmpString60(),
    email_cliente: fields.cmpString60(),
    telefone_cliente: fields.cmpString20(),
    gestor: fields.cmpRefId('empresas'),
    responsavel_gestor: fields.cmpString60(),
    email_gestor: fields.cmpString60(),
    telefone_gestor: fields.cmpString20(),
    foto_gestor: fields.cmpString(),
    fornecedores: [{ 
        fornecedor: fields.cmpRefId('empresas'),
    }],
        
    data_inicio: fields.cmpData(),
    data_fim: fields.cmpData(),
    horario_funcionamento_ini: fields.cmpHoraS(),
    horario_funcionamento_fim: fields.cmpHoraS(),
    nf: fields.cmpString20(),
    in_company: fields.cmpBoolean(),
    cadastro_auto: fields.cmpBoolean(),       // flag para inserir elegiveis sem autorização do gestor
    agrupamento: fields.cmpRefId('agrupamento', true),  // Permite agrupar campanhas para programação de roteiros e OS
    pesquisa_satisfacao: fields.cmpBoolean(), // Habilitar pesquisa de satisfação
    pesquisa_saude: fields.cmpBoolean(), // Habilitar pesquisa de dados de saúde do elegível a ser imunizado (atualizar registro do elegivel)
    total_qtde: fields.cmpNumber(),
    total_elegiveis: fields.cmpNumber(),
    total_aplicacoes: fields.cmpNumber(),
    valor_unitario_servico: fields.cmpNumberObrigatorio(),
    elegiveis_m: fields.cmpNumber(),
    elegiveis_f: fields.cmpNumber(),
    elegiveis_d: fields.cmpNumber(),
    elegiveis_c: fields.cmpNumber(),

    cnpj_total: [{
        cnpj: fields.cmpString(),
        cnpj_aplicacoes: fields.cmpNumber()
    }],

    produtos: [{
        produto: fields.cmpRefId('produtos', true),
        produto_qtde: fields.cmpNumber(),
        produto_aplicacoes: fields.cmpNumber(),
        elegiveis_m: fields.cmpNumber(),
        elegiveis_f: fields.cmpNumber(),
        elegiveis_d: fields.cmpNumber(),
        elegiveis_c: fields.cmpNumber(),
        duplicidade: fields.cmpNumber(), 
        novo: fields.cmpBoolean(),
        lotes: [{
            lote: fields.cmpString20(),
            lote_qtde: fields.cmpNumber(),
            lote_aplicacoes: fields.cmpNumber(),
            novo: fields.cmpBoolean()
        }]
    }],
    filiais: [{   // Quando in_company === true, cada clínica deverá definir o roteiro de aplicação
        filial: fields.cmpString60(),
        cnpj: fields.cmpString(),
        
        responsavel: fields.cmpString(),
        email: fields.cmpString(),
        fone: fields.cmpString20(),

        unidades: [ 
            {
                cod_unid: fields.cmpString(),
                nome_unid: fields.cmpString(),
                endereco: fields.cmpRefId('enderecos', true),
                numero: fields.cmpNumber(),
                complemento: fields.cmpString20(),
                previsao_doses: fields.cmpNumber()
            }
        ],
        previsao_doses: fields.cmpNumber(),
        usuario_atendimento: fields.cmpString(),
        atendimento_realizado: fields.cmpBoolean()
    }],
    senha_vacinacao: fields.cmpString(),
    previsao_doses: fields.cmpNumber(),
    criado_por: fields.cmpRefId('usuarios', true),
    data: fields.cmpData()
};

module.exports = new Schema(jsonCampanha);
