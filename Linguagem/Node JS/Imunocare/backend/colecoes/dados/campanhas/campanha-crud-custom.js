"use strict";

// @arquivo: campanha-crud-dinamico.js
// @autor  : Clayton C. Carvalho
// @data   : 28.11.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const c_e = "E",
    c_s = "S";

const campanhaSchema = require("./campanha-schema"),
    modelCampanha = require("./campanha-model")(campanhaSchema, "campanhas");

const elegivelSchema = require("../elegiveis/elegivel-schema"),
    modelElegivel = require("../elegiveis/elegivel-model")(elegivelSchema, "elegiveis");

const querystring = require("querystring"),
    url = require("url");
    
let promises = [],
    contador = 0,
    total = 0,
    dataAll = 0,
    errAll = 0;

const montaParams = (_url, param) => {
  let url_parts = url.parse(_url);
  if (param === "query") return querystring.parse(url_parts.query);
};

const newImu = function(){
  return {
    produto: ' ',
    desc_produto: ' ',
    lote: ' ',
    num_lote: ' ', 
    aplicacao: false
  }
}

function atualizarLotesElegiveis(campanha, novasImunizacoes) {

  return new Promise(function(executeNext, executeError) {

    modelElegivel.find({campanha: campanha._id}).exec()
      .then( elegiveis => {
        elegiveis.forEach(elegivel => {
          Array.prototype.push.apply(elegivel.imunizacoes, novasImunizacoes);
          promises.push(elegivel.save());
        })
        total = elegiveis.length;

        campanha.produtos.forEach(produto => {
          produto.lotes.forEach(lote => {
            if (lote.novo) {
              lote.novo = false;
            }
          });
        });

        promises.push(campanha.save());

        Promise.all(promises).then((data) => {
          contador = data.length - 1;
          dataAll = data;
          return executeNext(dataAll)
          /*
          return res.json({
            mensagem: `Lotes inseridos e novos elegiveis atualizados ${contador} de um total de ${total}`,
            contagem: `${contador}/${total}`,
            retorno: dataAll,
            erros: errAll                    
          });
          */
        })
        .catch((err) => {
          errAll = err;
          return executeError({data: dataAll, err: errAll})
          /*
          return res.status(206).json({
              mensagem: `Lotes inseridos e novos elegiveis atualizados ${contador} de um total de ${total}`,
              contagem: `${contador}/${total}`,
              retorno: dataAll,
              erros: errAll                    
          });
          */
        }); 
      })
    })
}

function get_headerOrigem(req) {
  return req.headers["x-access-origem"];
}

function callback(req, res, msg, data) {
  return res.json({
    mensagem: msg,
    contagem: data.length,
    retorno: data
  });
}

function callbackTable(req, res, length, data) {
  return res.json({
    contagem: length,
    retorno: data
  });
}

function returnError(req, res, msg, err) {
  return res.json({
    mensagem: msg,
    error: err.message
  });
}

let modelArray = [];

const crudCampanhas = {
  dadosCampanha: (req, res) => {
    let query = montaParams(req.url, "query"),
    options = {
      runValidators: true,
      new: true,
      upsert: true
    };

    modelCampanha
      .find(query, null, options)
      .exec()
      .then(callback.bind(null, req, res, 'Dados de Campanha'))
      .catch(returnError.bind(null, req, res, 'Campanha não encontrada'));
  },

  insereProdutos: (req, res) => {
    let query = montaParams(req.url, "query"),
    options = {
      runValidators: true,
      new: true,
      upsert: true
    };

    modelCampanha
      .findOne(query, null, options)
      .exec()
      .then( (campanha) => {
        campanha.insereProdutos(req.body)
          .then(callback.bind(null, req, res, 'Dados gravados'))
          .catch(returnError.bind(null, req, res, 'Erro ao atualizar Produtos'));
      })
      .catch(returnError.bind(null, req, res, 'campanha não encontrado'));
  },

  removeProdutos: (req, res) => {
    let query = montaParams(req.url, "query"),
      options = {
        runValidators: true,
        new: true,
        upsert: true
      };

    modelCampanha
      .findOne(query, null, options)
      .exec()
      .then( (campanha) => {
          campanha.removeProdutos(req.body)
            .then(callback.bind(null, req, res, 'Produtos eliminados'))
            .catch(returnError.bind(null, req, res, 'Erro ao eliminar Produtos'));
      })
      .catch(returnError.bind(null, req, res, 'campanha não encontrado'));
  },

  insereLotes: (req, res) => {
    let query = montaParams(req.url, "query"),
    options = {
        runValidators: true,
        new: true,
        upsert: true
    };
      
    let lotes = req.body.lotes;

    let novasImunizacoes = [];

    modelCampanha.findOne(query, null, options)
      .exec()
      .then( (campanha) => {
        campanha.insereLotes(req.body.produto, lotes)
          .then( campanha => {
            campanha.produtos.forEach(produto => {
              produto.lotes.forEach(lote => {
                if (lote.novo) {
                  let imunizacao = newImu();
                  imunizacao.produto = produto._id;
                  imunizacao.desc_produto = produto.produto;
                  imunizacao.lote = lote._id;
                  imunizacao.num_lote = lote.lote;
                  imunizacao.aplicacao = false;
                  novasImunizacoes.push(imunizacao);
                }
              });
            }); 
            atualizarLotesElegiveis(campanha, novasImunizacoes)
              .then(data => {
                return res.json({
                  mensagem: `Lotes inseridos e ${contador} novos elegiveis atualizados de um total de ${total}`,
                  contagem: `${contador}/${total}`
                  /*
                  retorno: data,
                  erros: '' 
                  */
                  });
              })
              .catch(err => {
                return res.status(206).json({
                  mensagem: `Lotes inseridos e ${contador} novos elegiveis atualizados de um total de ${total}`,
                  contagem: `${contador}/${total}`
                  /*
                  retorno: err.data,
                  erros: err.err                    
                  */
                  });
              })
        }).catch(returnError.bind(null, req, res, 'Erro ao inserir Lotes'));
      }).catch(returnError.bind(null, req, res, 'campanha não encontrado'));
  },

  removeLotes: (req, res) => {
    let query = montaParams(req.url, "query"),
    options = {
        runValidators: true,
        new: true,
        upsert: true
    };

    modelCampanha
      .findOne(query, null, options)
      .exec()
      .then( (campanha) => {
        campanha.removeLotes(req.body.produto, req.body.lotes)
          .then(callback.bind(null, req, res, 'Produtos eliminados'))
          .catch(returnError.bind(null, req, res, 'Erro ao eliminar Lote'));
      }).catch(returnError.bind(null, req, res, 'campanha não encontrado'));
  },

  insereRoteiros: (req, res) => {
    let query = montaParams(req.url, "query"),
    options = {
      runValidators: true,
      new: true,
      upsert: true
    };
    
    let lotes = req.body.lotes;

    modelCampanha
      .findOne(query, null, options)
      .exec()
      .then( (campanha) => {
        campanha.insereRoteiros(req.body)
          .then(callback.bind(null, req, res, 'Dados gravados'))
          .catch(returnError.bind(null, req, res, 'Erro ao atualizar Roteiros'));
      }).catch(returnError.bind(null, req, res, 'campanha não encontrado'));
  },

  removeRoteiros: (req, res) => {
    let query = montaParams(req.url, "query"),
    options = {
      runValidators: true,
      new: true,
      upsert: true
    };

    modelCampanha
      .findOne(query, null, options)
      .exec()
      .then( (campanha) => {
        campanha.removeRoteiros(req.body)
          .then(callback.bind(null, req, res, 'Produtos eliminados'))
          .catch(returnError.bind(null, req, res, 'Erro ao eliminar Roteiros'));
      }).catch(returnError.bind(null, req, res, 'campanha não encontrado'));
  },

  retrieve: (req, res, sort, fields, deepArray) => {
    let query = montaParams(req.url, "query");
    let page = 0, size = 0;

    if(query.page) page = query.page
    if(query.size) {
      size = Number.parseInt(query.size)
    } else size = 1

    modelCampanha.find(fields).count((err, count) => {
      if(query.all) {
        size = count;
      }
      if (deepArray === undefined || deepArray.length === 0) {
        modelCampanha.find(fields)
                     .skip(page * size)
                     .limit(size)
                     .exec()
                     .then( callbackTable.bind(null, req, res, count) )
                     .catch( returnError.bind(null, req, res) );
      } else {
        modelCampanha.find(fields)
                     .deepPopulate(deepArray)
                     .skip(page * size)
                     .limit(size)
                     .exec()
                     .then(callbackTable.bind(null, req, res, count))
                     .catch(returnError.bind(null, req, res));
      }
    });
  }
};

module.exports = crudCampanhas;