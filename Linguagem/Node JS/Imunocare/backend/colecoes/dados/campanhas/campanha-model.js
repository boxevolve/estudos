'use strict';

// @arquivo: campanha-model.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const refPromises = require('mongoose-ref-promises');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const deepPopulate = require('mongoose-deep-populate')(mongoose);

module.exports = function(_schema, colecao) {

  const campanhaSchema = _schema;

  campanhaSchema.retainKeyOrder = true;

  campanhaSchema.index({
    'campanha': 1,
    'cliente': 1,
    'data_inicio': 1,
    'data_fim': 1
  }, {
    unique: true
  });

  campanhaSchema.index({
    '_id': 1,
    'produtos.produto': 1,
    'produtos.lotes.lote': 1
  }, {
    unique: true
  });

  campanhaSchema.index({
    '_id': 1,
    'filiais.filial': 1
  }, {
    unique: true
  });

  campanhaSchema.plugin(refPromises);
  campanhaSchema.plugin(beautifyUnique);

  campanhaSchema.plugin(deepPopulate, {
    whitelist: ['filiais.unidades.endereco',
                'cliente',
                'cliente.endereco',
                'distribuidor',
                'gestor',
                'fornecedores.fornecedor', 
                'fornecedores.fornecedor.endereco', 
                'agrupamento',
                'produtos.produto',
                'criado_por'],
    populate: {
      "filiais.unidades.endereco": {
        select: ""
      },
      "cliente": {
        select: ""
      },
      'cliente.endereco': {
        select: ""
      },
      "distribuidor": {
        select: ""
      },
      "gestor": {
        select: ""
      },
      'fornecedores.fornecedor': {
        select: ""
      },
      'fornecedores.fornecedor.endereco': {
        select: ""
      },
      'agrupamento': {
        select: ""
      },
      'produtos.produto': {
        select: "produto"
      },
      "criado_por": {
        select: "usuario email"
      }
    }
  });

  function trim(str) {
    if (str) {
      return str.replace(/^\s+|\s+$/g,"");
    } else {
      return ''
    }
  }

  function pre(campanha) {
    campanha.cnpj = trim(campanha.cnpj)

    campanha.total_qtde = 0;
    if (!campanha.total_elegiveis)  campanha.total_elegiveis = 0;
    if (!campanha.total_aplicacoes)  campanha.total_aplicacoes = 0;
    if (!campanha.valor_unitario_servico)  campanha.valor_unitario_servico = 0;
    if (!campanha.elegiveis_m)  campanha.elegiveis_m = 0;
    if (!campanha.elegiveis_f)  campanha.elegiveis_f = 0;
    if (!campanha.elegiveis_d)  campanha.elegiveis_d = 0;
    if (!campanha.elegiveis_c)  campanha.elegiveis_c = 0;

    if(campanha.produtos && campanha.produtos.length > 0) {
      for(let p = 0; p < campanha.produtos.length; p++) {
        campanha.produtos[p]._id = campanha.produtos[p]._id ? campanha.produtos[p]._id : new mongoose.Types.ObjectId();
        campanha.produtos[p].produto_qtde = 0;

        if (!campanha.produtos[p].produto_aplicacoes) campanha.produtos[p].produto_aplicacoes = 0;
        if (!campanha.produtos[p].produto_qtde) campanha.produtos[p].produto_qtde = 0;
        if (!campanha.produtos[p].elegiveis_m) campanha.produtos[p].elegiveis_m = 0;
        if (!campanha.produtos[p].elegiveis_f) campanha.produtos[p].elegiveis_f = 0;
        if (!campanha.produtos[p].elegiveis_d) campanha.produtos[p].elegiveis_d = 0;
        if (!campanha.produtos[p].elegiveis_c) campanha.produtos[p].elegiveis_c = 0;

        for (let l = 0; l < campanha.produtos[p].lotes.length; l++) {
          campanha.produtos[p].lotes[l]._id = campanha.produtos[p].lotes[l]._id 
                                            ? campanha.produtos[p].lotes[l]._id 
                                            : new mongoose.Types.ObjectId();
          if (!campanha.produtos[p].lotes[l].lote_qtde) campanha.produtos[p].lotes[l].lote_qtde = 0;
          if (!campanha.produtos[p].lotes[l].lote_aplicacoes) campanha.produtos[p].lotes[l].lote_aplicacoes = 0;
          campanha.produtos[p].produto_qtde = campanha.produtos[p].produto_qtde + campanha.produtos[p].lotes[l].lote_qtde;
        }

        campanha.total_qtde = campanha.total_qtde + campanha.produtos[p].produto_qtde;
      }
    }
  }

  campanhaSchema.pre("save", function(next) {
    const campanha = this;
    pre(campanha);
    return next();
  });

  campanhaSchema.pre("update", function(next) {
    const campanha = this.getUpdate().$set;
    pre(campanha);
    return next();
  });  

  campanhaSchema.methods.insereProdutos = function(produtos) {

    let campanha = this;

    return new Promise(function(executeNext, executeError) {
      for (let i = 0; i < produtos.length; i++) {
        for(let j = 0; j < campanha.produtos.length; j++) {
          if (produtos[i]._id === campanha.produtos[j]._id) {
            // campanha.produtos[j] = produtos[i];
            // campanha.markModified('produtos');
            // return executeError('Produto já existe nesta campanha')
            break;
          }
        }
        if(!campanha.isModified('produtos')) {
          campanha.total_qtde = campanha.total_qtde + produtos[i].produto_qtde;
          produtos[i].novo = true;
          campanha.produtos.push(produtos[i]);
        }
      }
      
      return campanha.save(function(err, data){
        if (err) return executeError(err); 
        return executeNext(data);
      });
    });
  };

  campanhaSchema.methods.removeProdutos = function(produtos) {
    let campanha = this;
    return new Promise(function(executeNext, executeError) {
      for (let i = 0; i < produtos.length; i++) {
        let contato = campanha.produtos.id(produtos[i]._id).remove();
        campanha.total_qtde = campanha.total_qtde - produtos[i].produto_qtde;
        if (campanha.total_qtde < 0) campanha.total_qtde = 0;
      }
      
      return campanha.save(function(err, data){
        if (err) return executeError(err); 
        return executeNext(data);
      });
    });
  };

  campanhaSchema.methods.insereLotes = function(produto, lotes) {
    let campanha = this;
    return new Promise(function(executeNext, executeError) {
      for(let p = 0; p < campanha.produtos.length; p++) {
        if(campanha.produtos[p]._id.toString() === produto) {
          for (let i = 0; i < lotes.length; i++) {
            for(let j = 0; j < campanha.produtos[p].lotes.length; j++) {
              if (lotes[i].lote === campanha.produtos[p].lotes[j].lote) {
                // campanha.produtos[p].lotes[j] = lotes[i];
                // campanha.markModified('produtos.lotes');
                break;
              }
            }
            if(!campanha.isModified('produtos.lotes')) {
              campanha.produtos[p].produto_qtde = campanha.produtos[p].produto_qtde + lotes[i].lote_qtde;
              campanha.total_qtde = campanha.total_qtde + lotes[i].lote_qtde;
              lotes[i].novo = true;
              campanha.produtos[p].lotes.push(lotes[i]);
            }
          }
        }
      }
  
      return campanha.save(function(err, data){
        if (err) return executeError(err); 
        return executeNext(data);
      });
    });
  };

  campanhaSchema.methods.removeLotes = function(produto, lotes) {
    let campanha = this;
    return new Promise(function(executeNext, executeError) {
      for(let p = 0; p < campanha.produtos.length; p++) {
        if(campanha.produtos[p]._id.toString() === produto) {
          for (let i = 0; i < lotes.length; i++) {
            let prod = campanha.produtos[p].lotes.id(lotes[i]._id).remove();
            campanha.produtos[p].produto_qtde = campanha.produtos[p].produto_qtde - lotes[i].lote_qtde;
            campanha.total_qtde = campanha.total_qtde - lotes[i].lote_qtde;
            if (campanha.produtos[p].produto_qtde < 0) campanha.produtos[p].produto_qtde = 0;
          }
          break;
        }
      }

      if (campanha.total_qtde < 0) campanha.total_qtde = 0;
      
      return campanha.save(function(err, data){
        if (err) return executeError(err); 
        return executeNext(data);
      });
    });
  };

  campanhaSchema.statics.verificarQtdeProdutoELote = function(elegivel, imunizacao, res) {
    let campanha = this;
    let idProd = '';
    let idLote = '';

    for(let i = 0; i < elegivel.imunizacoes.length; i++) {
      if(elegivel.imunizacoes[i]._id.toString() === imunizacao._id) {
        idProd = elegivel.imunizacoes[i].produto;
        idLote = elegivel.imunizacoes[i].lote;
        break;
      }
    }

    let elegivel_m = 0,
        elegivel_f = 0,
        elegivel_d = 0,
        elegivel_c = 0;

    if (elegivel.sexo === 'M') {
      elegivel_m = 1;
    } else {
      elegivel_f = 1;
    }
    if (elegivel.dependente) {
      elegivel_d = 1;
    } else {
      elegivel_c = 1;
    }
      
    return new Promise(function(executeNext, executeError) {
      campanha.findOne( { "_id": elegivel.campanha, 
                          "produtos._id": idProd, 
                          "produtos.lotes._id": idLote })
      .then(data => {
        let achouCnpj = false
        let cnpjT = []

        if (data.cnpj_total) {
          for (let c = 0; c < data.cnpj_total.length; c++) {
            if(trim(data.cnpj_total[c].cnpj) === trim(elegivel.cnpj)) {
              data.cnpj_total[c].cnpj_aplicacoes = data.cnpj_total[c].cnpj_aplicacoes + 1;
              achouCnpj = true;
              break;
            }
          }
          cnpjT = data.cnpj_total;

          if(!achouCnpj) {
            cnpjT.push( { cnpj: trim(elegivel.cnpj), cnpj_aplicacoes: 1});
          }
        } else {
          cnpjT.push( { cnpj: trim(elegivel.cnpj), cnpj_aplicacoes: 1});
        }

        data.cnpj_total = cnpjT;

        for (let p = 0; p < data.produtos.length; p++) {
          if (data.produtos[p]._id.toString() === idProd) {
            if (data.produtos[p].produto_aplicacoes < data.produtos[p].produto_qtde) {
              for (let l = 0; l < data.produtos[p].lotes.length; l++) {
                if (data.produtos[p].lotes[l]._id.toString() === idLote) {
                  if (data.produtos[p].lotes[l].lote_aplicacoes >= data.produtos[p].lotes[l].lote_qtde) {
                    return res.status(405).json({mensagem: `Quantidade disponível do lote ${data.produtos[p].lotes[l].lote} atingida. Favor selecionar outro lote`});
                  } else {
                    data.total_aplicacoes = data.total_aplicacoes + 1;
                    data.produtos[p].produto_aplicacoes = data.produtos[p].produto_aplicacoes + 1;
                    data.produtos[p].lotes[l].lote_aplicacoes = data.produtos[p].lotes[l].lote_aplicacoes + 1;                                   

                    data.elegiveis_c = data.elegiveis_c + elegivel_c;
                    data.elegiveis_d = data.elegiveis_d + elegivel_d;
                    data.elegiveis_f = data.elegiveis_f + elegivel_f;
                    data.elegiveis_m = data.elegiveis_m + elegivel_m;
                    data.produtos[p].elegiveis_c = data.produtos[p].elegiveis_c + elegivel_c;
                    data.produtos[p].elegiveis_d = data.produtos[p].elegiveis_d + elegivel_d;
                    data.produtos[p].elegiveis_f = data.produtos[p].elegiveis_f + elegivel_f;
                    data.produtos[p].elegiveis_m = data.produtos[p].elegiveis_m + elegivel_m;
                    return executeNext(data);
                  }
                  break;
                }
              }
            } else {
              return res.status(405).json({mensagem: `Quantidade disponível do Produto ${data.produtos[p].produto} atingida. Favor entrar em contato com ${data.distribuidor}`});
            }
          }
        }
        return res.status(405).json({mensagem: `Produto não encontrado`});
      }).catch(err => { 
        return res.status(405).json({mensagem: `Campanha não encontrada`})
      }); 
    });
  };

  campanhaSchema.methods.atualizarCampanha = function() {
    const campanha = this;
    return new Promise(function(executeNext, executeError) {
      return campanha.save((err, data) => {
        if(err) return executeError(err);
          return executeNext();
      });
    });
  };

/*    
  campanhaSchema.methods.insereRoteiros = function(roteirizacao) {

      let campanha = this;

      return new Promise(function(executeNext, executeError) {

          for (let i = 0; i < roteirizacao.length; i++) {
              for(let j = 0; j < campanha.roteirizacao.length; j++) {
                  if (roteirizacao[i].unidade === campanha.roteirizacao[j].unidade) {
                      campanha.roteirizacao[j] = roteirizacao[i];
                      campanha.markModified('roteirizacao');
                      break;
                  }
              }
              if(!campanha.isModified('roteirizacao')) campanha.roteirizacao.push(roteirizacao[i]);
          }
          
          return campanha.save(function(err, data){
              if (err) return executeError(err); 
              return executeNext(data);
          });
      });
  };

  campanhaSchema.methods.removeRoteiros = function(roteirizacao) {

      let campanha = this;

      return new Promise(function(executeNext, executeError) {

          for (let i = 0; i < roteirizacao.length; i++) {
              let contato = campanha.roteirizacao.id(roteirizacao[i]._id).remove();
          }
          
          return campanha.save(function(err, data){
              if (err) return executeError(err); 
              return executeNext(data);
          });
      });
  };
*/
  return mongoose.model(colecao, campanhaSchema);

};
