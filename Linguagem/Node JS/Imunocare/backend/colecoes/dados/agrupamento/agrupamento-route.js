// @arquivo: agrupamento-route.js
// @autor  : Bruno C. Dourado
// @data   : 18.01.2019
// @projeto: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const c_e = 'E',
    c_s = 'S';

const deepArray = ['criado_por'];

const crudAgrupamento = require('./agrupamento-crud');
//const validaToken = require("../../login/validarToken");

//router.route('/create').post(validaToken, (req, res, next) => {
router.route('/create').post((req, res, next) => {
    crudAgrupamento.create(req, res);
});

//router.route('/retrieve').get(validaToken, (req, res, next) => {
router.route('/retrieve').get((req, res, next) => {
    //router.get('/retrieve', function validaToken(req, res, next) {
    const sort = {},
    fields = {};
    crudAgrupamento.retrieve(req, res, sort, fields, deepArray);
});

//router.route('/findOne').get(validaToken, (req, res, next) => {
router.route('/findOne').get((req, res, next) => {
    //router.get('/findOne', function(req, res, next) {
    const fields = {};      
    crudAgrupamento.findOne(req, res, fields, deepArray);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/update').patch((req, res, next) => {
    crudAgrupamento.update(req, res);
});


//router.route('/delete').delete(validaToken, (req, res, next) => {
router.route('/delete').delete((req, res, next) => {
    crudAgrupamento.delete(req, res);
});


module.exports = router;