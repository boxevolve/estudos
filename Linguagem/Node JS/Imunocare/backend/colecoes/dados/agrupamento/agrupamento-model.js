'use strict';

// @arquivo: agrupamento-model.js
// @autor  : Bruno C. Dourado
// @data   : 18.01.2019
// @projeto: ImunoApp

const mongoose = require('mongoose');
const refPromises = require('mongoose-ref-promises');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const deepPopulate = require('mongoose-deep-populate')(mongoose);


module.exports = function(_schema, colecao) {

    const agrupamentoSchema = _schema;

    agrupamentoSchema.retainKeyOrder = true;

    agrupamentoSchema.index({
        'descricao': 1
    }, {
        unique: true
    });

    agrupamentoSchema.plugin(refPromises);
    agrupamentoSchema.plugin(beautifyUnique);

    agrupamentoSchema.plugin(deepPopulate, {
        whitelist: ['criado_por'],
        populate: {
            "criado_por": {
                select: "usuario email"
            }
        }
    });

    return mongoose.model(colecao, agrupamentoSchema);

};
