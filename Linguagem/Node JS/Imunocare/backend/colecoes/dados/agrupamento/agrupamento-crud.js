"use strict";

// @arquivo: agrupamento-crud.js
// @autor  : Bruno C. Dourado
// @data   : 18.01.2019
// @projeto: ImunoApp

const agrupamentoSchema = require("./agrupamento-schema"),
modelAgrupamento = require("./agrupamento-model")(agrupamentoSchema, "agrupamento"),
crudDinamico = require("../../../utilitarios/crud/crud_dinamico")(modelAgrupamento, agrupamentoSchema);

module.exports = crudDinamico;