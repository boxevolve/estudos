'use strict';

// @arquivo: agrupamento-schema.js
// @autor  : Bruno C. Dourado
// @data   : 18.01.2019
// @projeto: ImunoApp

// Constantes do Mongoose
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fields = require('../../fields/fields');

// Campos do Schema

const jsonEndereco = {
    descricao: fields.cmpString(),
    criado_por: fields.cmpRefId('usuarios', true) || fields.cmpString(),
    data: fields.cmpData()
};

module.exports = new Schema(jsonEndereco); 
