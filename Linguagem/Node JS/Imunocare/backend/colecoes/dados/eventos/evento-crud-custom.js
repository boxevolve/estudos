"use strict";

// @arquivo: evento-crud-custom.js
// @autor  : Clayton C. Carvalho
// @data   : 28.11.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const c_e = "E", 
    c_s = "S";

const eventoSchema = require("./evento-schema"),
        modelEvento = require("./evento-model")(eventoSchema, "eventos")


const querystring = require("querystring"),
    url = require("url");

const montaParams = (_url, param) => {
    let url_parts = url.parse(_url);
    if (param === "query") return querystring.parse(url_parts.query);
};

function get_headerOrigem(req) {
    return req.headers["x-access-origem"];
}

function callback(req, res, data) {

    return res.json({
        contagem: data.length,
        retorno: data
    });
}

function returnError(req, res, err) {
    return res.json({
        mensagem: err.message,
        error: err
    });
}

function ajustarRetornoEventos(data, req, res) {
    
    let evs = []
    evs = data
    let sairUnid = false


// PAREI AQUI ******************************************************************************
    for(let i=0; i < evs.length; i++) {
        if(evs[i].filial && evs[i].campanha.filiais.length > 0 && evs[i].unidade) {
            for(let l=0; l < evs[i].campanha.filiais.length; l++) {
                sairUnid = false;
                // const local = evs[i].locais[l]
                const camp = {
                    _id: evs[i].campanha._id,
                    campanha: evs[i].campanha.campanha
                }
                const forn = {
                    _id: evs[i].fornecedor._id,
                    empresa: evs[i].fornecedor.empresa
                }
                for(let f=0; f< evs[i].campanha.filiais.length; f++) {
                    for(let u=0; u < evs[i].campanha.filiais[f].unidades.length; u++) {
                        if(evs[i].campanha.filiais[f].unidades[u].cod_unid === evs[i].unidade) {
                            evs[i].unidade = evs[i].campanha.filiais[f].unidades[u]
                            sairUnid = true;
                            break;
                        }
                    }
                    if(sairUnid) {
                        break;
                    }
                }
                evs[i].campanha = camp
                evs[i].fornecedor = forn
            }
        }
    }
    data = evs
    callback(req, res, data)
}

const crudevento = {

    getEventosCalendario: (req, res, sort, fields, deepArray) => {

        let query = montaParams(req.url, "query");

        if (deepArray === undefined || deepArray.length === 0) {
            modelEvento
                .find()
                .exec()
                .then( callback.bind(null, req, res) )
                .catch( returnError.bind(null, req, res) );

        } else {
            modelEvento 
                .find()
                .deepPopulate(deepArray)
                .exec()
                .then( data => {
                    ajustarRetornoEventos(data, req, res)
                })
                .catch(
                    returnError.bind(null, req, res));
        }
    }
};

module.exports = crudevento;