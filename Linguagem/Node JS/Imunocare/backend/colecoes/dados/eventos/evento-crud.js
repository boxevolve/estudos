"use strict";

// @arquivo: evento-crud.js
// @autor  : Clayton C. Carvalho
// @data   : 17.02.2018
// @projeto: ImunoApp

const eventoSchema = require("./evento-schema"),
modelEvento = require("./evento-model")(eventoSchema, "eventos"),
crudDinamico = require("../../../utilitarios/crud/crud_dinamico")(modelEvento, eventoSchema);

module.exports = crudDinamico;