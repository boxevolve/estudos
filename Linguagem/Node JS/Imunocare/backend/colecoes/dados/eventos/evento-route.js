// @arquivo: evento-route.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser'); 

const c_e = 'E',
    c_s = 'S';

const deepArray = ['campanha', 'campanha.filiais.unidades.endereco', 'fornecedor', 'criado_por'];

const crudEvento = require('./evento-crud');
const crudEventoCustom = require('./evento-crud-custom');
//const validaToken = require("../../login/validarToken");

//router.route('/create').post(validaToken, (req, res, next) => {
router.route('/create').post((req, res, next) => {
    crudEvento.create(req, res);
});

//router.route('/retrieve').get(validaToken, (req, res, next) => {
router.route('/retrieve').get((req, res, next) => {
    //router.get('/retrieve', function validaToken(req, res, next) {
    const sort = { 'inicio': 1 },
    fields = {};
    crudEvento.retrieve(req, res, sort, fields, deepArray);
});

//router.route('/retrieve').get(validaToken, (req, res, next) => {
router.route('/getEventosCalendario').get((req, res, next) => {
    //router.get('/retrieve', function validaToken(req, res, next) {
    const sort = { 'inicio': 1 },
    fields = {};
    crudEventoCustom.getEventosCalendario(req, res, sort, fields, deepArray);
});

//router.route('/findOne').get(validaToken, (req, res, next) => {
router.route('/findOne').get((req, res, next) => {
    //router.get('/findOne', function(req, res, next) {
    const fields = {};      
    crudEvento.findOne(req, res, fields, deepArray);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/update').patch((req, res, next) => {
    crudEvento.update(req, res);
});


//router.route('/delete').delete(validaToken, (req, res, next) => {
router.route('/delete').delete((req, res, next) => {
    crudEvento.delete(req, res);
});


module.exports = router;