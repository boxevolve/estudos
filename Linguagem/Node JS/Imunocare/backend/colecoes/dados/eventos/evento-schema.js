'use strict';

// @arquivo: evento-schema.js
// @autor  : Clayton C. Carvalho
// @data   : 17.02.2018
// @projeto: ImunoApp

// Constantes do Mongoose
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fields = require('../../fields/fields');

    // Campos do Schema

const jsonEvento = {
// Dados do Evento
    titulo: fields.cmpString(), 
    inicio: fields.cmpData(),
    fim: fields.cmpData(),
    diatodo: fields.cmpBoolean(),
    fornecedor: fields.cmpRefId('empresas', true),
    texto_evento: fields.cmpString(), 
// Dados da Campanha
    agrupamento: fields.cmpString(),
    campanha: fields.cmpRefId('campanhas', true),   // mudar para agrupamento (na seleção e combos)
    filial: fields.cmpString(),
    unidade: fields.cmpString(),
// Dados de usuário
    criado_por: fields.cmpRefId('usuarios', true),
    data: fields.cmpData() 
}

module.exports = new Schema(jsonEvento);
