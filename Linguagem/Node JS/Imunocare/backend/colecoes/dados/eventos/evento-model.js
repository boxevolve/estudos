'use strict';

// @arquivo: evento-model.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const refPromises = require('mongoose-ref-promises');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const deepPopulate = require('mongoose-deep-populate')(mongoose);

module.exports = function(_schema, colecao) {

    const eventoSchema = _schema;

    eventoSchema.retainKeyOrder = true;

    eventoSchema.index({ 
        'titulo': 1,
        'inicio': 1,
        'fim': 1,
        'campanha': 1,
        'filial': 1,
        'unidade': 1
    }, {
        unique: true
    });

    eventoSchema.plugin(refPromises);
    eventoSchema.plugin(beautifyUnique);

    eventoSchema.plugin(deepPopulate, {
        whitelist: ['campanha', 
                    'campanha.filiais.unidades.endereco',
                    'fornecedor', 
                    'locais', 
                    'criado_por'],
        populate: {
            "campanha": {
                select: "campanha cliente.empresa filiais"
            }, 
            "campanha.filiais.unidades.endereco": {
                select: ""
            },
            "fornecedor": {
                select: "empresa"
            }, 
            "criado_por": {
                select: "usuario email"
            }
        }
    });

    return mongoose.model(colecao, eventoSchema);

};
