'use strict';

// @arquivo: endereco-schema.js
// @autor  : Clayton C. Carvalho
// @data   : 17.02.2018
// @projeto: ImunoApp

// Constantes do Mongoose
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fields = require('../../fields/fields');

    // Campos do Schema

const jsonEndereco = {
    endereco: fields.cmpString(), 
    bairro: fields.cmpString(),
    cidade: fields.cmpString(),
    estado: fields.cmpUF(),
    cep: fields.cmpCep(),
    criado_por: fields.cmpRefId('usuarios', true) || fields.cmpString(),
    data: fields.cmpData()
};

module.exports = new Schema(jsonEndereco); 
