"use strict";

// @arquivo: endereco-crud.js
// @autor  : Clayton C. Carvalho
// @data   : 17.02.2018
// @projeto: ImunoApp

const enderecoSchema = require("./endereco-schema"),
modelEndereco = require("./endereco-model")(enderecoSchema, "enderecos"),
crudDinamico = require("../../../utilitarios/crud/crud_dinamico")(modelEndereco, enderecoSchema);

module.exports = crudDinamico;