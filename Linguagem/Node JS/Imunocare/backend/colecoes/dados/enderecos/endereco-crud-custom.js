"use strict";

// @arquivo: endereco-crud-custom.js
// @autor  : Clayton C. Carvalho
// @data   : 28.11.2017
// @projeto: ImunoApp

const enderecoSchema = require("../enderecos/endereco-schema"),
      modelEndereco = require("../enderecos/endereco-model")(enderecoSchema, "enderecos");

const querystring = require("querystring"),
    url = require("url");

var request = require("request");

const montaParams = (_url, param) => {
  let url_parts = url.parse(_url);
  if (param === "query") return querystring.parse(url_parts.query);
};

const crudEndereco = {
  cep: (req, res) => {
    let query = montaParams(req.url, "query");

    var options = {
      uri : 'https://viacep.com.br/ws/' + query.cep + '/json/',
      method : 'GET'
    };
  
    request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        res.json(JSON.parse(body));
      } else {
        res.json(JSON.parse(error));
      }
    });
  }
};

module.exports = crudEndereco;