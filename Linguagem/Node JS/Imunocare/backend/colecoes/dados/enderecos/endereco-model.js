'use strict';

// @arquivo: endereco-model.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const refPromises = require('mongoose-ref-promises');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const deepPopulate = require('mongoose-deep-populate')(mongoose);


module.exports = function(_schema, colecao) {

    const enderecoSchema = _schema;

    enderecoSchema.retainKeyOrder = true;

    enderecoSchema.plugin(refPromises);
    enderecoSchema.plugin(beautifyUnique);

    enderecoSchema.plugin(deepPopulate, {
        whitelist: ['criado_por'],
        populate: {
            "criado_por": {
                select: "usuario email"
            }
        }
    });

    return mongoose.model(colecao, enderecoSchema);

};
