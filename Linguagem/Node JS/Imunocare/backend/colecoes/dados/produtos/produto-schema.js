'use strict';

// @arquivo: produto-schema.js
// @autor  : Clayton C. Carvalho
// @data   : 17.02.2018
// @projeto: ImunoApp

// Constantes do Mongoose
const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      fields = require('../../fields/fields');

// Campos do Schema

const jsonProduto = {
  produto: fields.cmpString(),
  fornecedores: [{
    fornecedor: fields.cmpRefId('empresas', true),
    periodos: [{
      ordem: fields.cmpNumber(),
      periodo_meses: fields.cmpNumber()
    }]
  }],
  dosagem: fields.cmpString(),
  faixa_min: fields.cmpNumber(),
  faixa_max: fields.cmpNumber(),
  criado_por: fields.cmpRefId('usuarios', true),
  data: fields.cmpData()
};

module.exports = new Schema(jsonProduto);
