"use strict";

// @arquivo: produto-crud.js
// @autor  : Clayton C. Carvalho
// @data   : 17.02.2018
// @projeto: ImunoApp

const produtoSchema = require("./produto-schema"),
modelProduto = require("./produto-model")(produtoSchema, "produtos"),
crudDinamico = require("../../../utilitarios/crud/crud_dinamico")(modelProduto, produtoSchema);

module.exports = crudDinamico;
