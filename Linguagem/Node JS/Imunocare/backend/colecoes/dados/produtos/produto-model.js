'use strict';

// @arquivo: produto-model.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const refPromises = require('mongoose-ref-promises');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const deepPopulate = require('mongoose-deep-populate')(mongoose);


module.exports = function(_schema, colecao) {

  const produtoSchema = _schema;

  produtoSchema.retainKeyOrder = true;

  produtoSchema.index({
    'produto': 1
  }, {
    unique: true
  });

  produtoSchema.index({
    'produto': 1,
    'fornecedores.fornecedor': 1
  }, {
    unique: true
  });

  produtoSchema.plugin(refPromises);
  produtoSchema.plugin(beautifyUnique);

  produtoSchema.plugin(deepPopulate, {
    whitelist: ['fornecedores.fornecedor','criado_por'],
    populate: {
      'fornecedores.fornecedor': {
        select: " "
      }, "criado_por": {
        select: "usuario email"
      }
    }
  });

  return mongoose.model(colecao, produtoSchema);

};
