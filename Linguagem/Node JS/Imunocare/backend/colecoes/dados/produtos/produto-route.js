// @arquivo: produto-route.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const c_e = 'E',
      c_s = 'S';

const deepArray = ['criado_por', 'fornecedores.fornecedor'];

const crudproduto = require('./produto-crud'),
      crudProdutoCustom = require('./produto-crud-custom');

router.route('/create').post((req, res, next) => {
  crudproduto.create(req, res);
});

router.route('/retrieve').get((req, res, next) => {
  const sort = {},
  fields = {};
  crudProdutoCustom.retrieve(req, res, sort, fields, deepArray);
});

router.route('/byFornecedor').get((req, res, next) => {
  crudProdutoCustom.byFornecedor(req, res, deepArray);
});

router.route('/findOne').get((req, res, next) => {
  const fields = {};      
  crudproduto.findOne(req, res, fields, deepArray);
});

router.route('/update').patch((req, res, next) => {
  crudproduto.update(req, res);
});

router.route('/delete').delete((req, res, next) => {
  crudproduto.delete(req, res);
});

module.exports = router;
