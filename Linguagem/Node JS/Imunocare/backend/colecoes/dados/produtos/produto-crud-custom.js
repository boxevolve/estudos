"use strict";

// @arquivo: produto-crud-custom.js
// @autor  : Bruno C. Dourado
// @data   : 02.12.2018
// @projeto: ImunoApp

require('mongoose');

const produtoSchema = require("./produto-schema"),
      modelProduto = require("./produto-model")(produtoSchema, "produtos");

const empresaSchema = require("../empresas/empresa-schema");
require("../empresas/empresa-model")(empresaSchema, "empresas");

const querystring = require("querystring"),
      url = require("url");

const montaParams = (_url, param) => {
  let url_parts = url.parse(_url);
  if (param === "query") return querystring.parse(url_parts.query);
};

function callbackTable(req, res, length, data) {
  return res.json({
    contagem: length,
    retorno: data
  });
}

function callback(req, res, data) {
  return res.json({
      contagem: data.length,
      retorno: data
  });
}

function returnError(req, res, err) {
  return res.json({
    mensagem: err.message,
    error: err
  });
}

const crudProduto = {

  retrieve: (req, res, sort, fields, deepArray) => {
    let query = montaParams(req.url, "query");
    let page = 0, size = 0;

    if(query.page) page = query.page
    if(query.size) {
        size = Number.parseInt(query.size)
    } else size = 1

    modelProduto.count((err, count) => {
      if (deepArray === undefined || deepArray.length === 0) {
        modelProduto.find(fields)
                    .skip(page * size)
                    .limit(size)
                    .exec()
                    .then( callbackTable.bind(null, req, res, count) )
                    .catch( returnError.bind(null, req, res) );
      } else {
        modelProduto.find(fields)
                    .deepPopulate(deepArray)
                    .skip(page * size)
                    .limit(size)
                    .exec()
                    .then(callbackTable.bind(null, req, res, count))
                    .catch(returnError.bind(null, req, res));
      }
    });
  },

  byFornecedor: (req, res, deepArray) => {
    let query = montaParams(req.url, "query");
    if (deepArray === undefined || deepArray.length === 0) {
      modelProduto
            .find({ 'fornecedores.fornecedor': query.fornecedor })
            .exec()
            .then( callback.bind(null, req, res) )
            .catch( returnError.bind(null, req, res) );

    } else {
      modelProduto
            .find({ 'fornecedores.fornecedor': query.fornecedor })
            .deepPopulate(deepArray)
            .exec()
            .then( callback.bind(null, req, res) )
            .catch( returnError.bind(null, req, res) );
    }
},

};

module.exports = crudProduto;
