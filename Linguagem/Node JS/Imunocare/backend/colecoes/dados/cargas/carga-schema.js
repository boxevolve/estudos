'use strict';

// @arquivo: carga-schema.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

// Constantes do Mongoose
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fields = require('../../fields/fields'),

    // Campos do Schema

// Criando Schema
jsonCarga = {
    usuario: fields.cmpRefId('usuarios'),
    campanha: fields.cmpRefId('campanhas'),
    nomecampanha: fields.cmpString(),
    tipo: fields.cmpTipoCarga(),
    filename: fields.cmpStringObrigatorio(),
    originalname: fields.cmpString(),
    status: fields.cmpStatusFile(),
    data: fields.cmpData(),
    docs_total: fields.cmpNumber(),
    docs_erros: fields.cmpNumber()
};

module.exports = new Schema(jsonCarga);