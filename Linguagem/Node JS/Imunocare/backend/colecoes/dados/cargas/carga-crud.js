"use strict";

// @arquivo: carga-crud.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const cargaSchema = require("./carga-schema"),
    modelCarga = require("./carga-model")(cargaSchema, "cargas"),
    crudDinamico = require("../../../utilitarios/crud/crud_dinamico")(modelCarga, cargaSchema);

module.exports = crudDinamico;
