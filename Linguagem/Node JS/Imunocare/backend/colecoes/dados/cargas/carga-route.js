// @arquivo: carga-route.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();
const crudCarga = require('./carga-crud');
const crudCargaCustom = require('./carga-crud-custom');

router.route('/create').post((req, res, next) => {
    crudCarga.create(req, res);
});

router.route('/retrieve').get((req, res, next) => {
    const sort = {},
    fields = {};
    crudCarga.retrieve(req, res, sort, fields);
});

router.route('/findOne').get((req, res, next) => {
    const sort = {},
    fields = {};       
    crudCarga.findOne(req, res, fields);
});

router.route('/update').put((req, res, next) => {
    crudCarga.update(req, res);
});

router.route('/delete').delete((req, res, next) => {
    crudCarga.delete(req, res);
});

router.route('/carga').post((req, res, next) => {
    crudCargaCustom.carga(req, res, next);
});

router.route('/arquivos').get((req, res, next) => {
    crudCargaCustom.recuperarCargasDeArquivos(req, res);
});

module.exports = router;