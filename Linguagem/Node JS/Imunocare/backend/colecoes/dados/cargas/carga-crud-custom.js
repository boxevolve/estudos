'use strict';

// @arquivo: carga-crud-custom.js
// @autor  : Clayton C. Carvalho
// @data   : 13.08.2018
// @projeto: ImunoApp

const moment = require('moment-timezone');         
const mongoose = require('mongoose');
const c_e = "E",
  c_s = "S";

const cargaSchema = require("./carga-schema"),
  modelCarga = require("./carga-model")(cargaSchema, "cargas"),
  campanhaSchema = require("../../dados/campanhas/campanha-schema"),
  modelCampanha = require("../../dados/campanhas/campanha-model")(campanhaSchema, "campanhas"),
  produtoSchema = require("../../dados/produtos/produto-schema"),
  modelProduto = require("../../dados/produtos/produto-model")(produtoSchema, "produtos"),
  elegivelSchema = require("../../dados/elegiveis/elegivel-schema"),
  modelElegivel = require("../../dados/elegiveis/elegivel-model")(elegivelSchema, "elegiveis"),
  validatorElegivel = require("../../dados/elegiveis/elegivel-validator")(elegivelSchema, modelElegivel, modelCampanha),
  validacao = require('../../../validations/validations');

const fields = require('../../fields/fields'),
  querystring = require("querystring"),
  url = require("url"),
  enviarEmail = require('../../../utilitarios/email/email'),
  randomstring = require('randomstring');


const montaParams = (_url, param) => {
  let url_parts = url.parse(_url);
  if (param === "query") return querystring.parse(url_parts.query);
};

function get_headerOrigem(req) {
  return req.headers["x-access-origem"];
}

function callback(req, res, data) {
  const url_path = url.parse(req.url);

  if (url_path.pathname === '/create') {
    return res.status(200).json({
	  mensagem: `Dados de Carga de ${data.tipo} criados com sucesso.`,
	  objeto: {
        tipo: data.tipo,

	  },
    });
  }
}

function returnError(req, res, validacao, err) {
    console.log(err);

    const url_path = url.parse(req.url);

    if (validacao) {
        return res.status(412).json({
            mensagem: 'Ocorreram alguns erros de validação dos dados',
            error: err,
        });
    } else {
        return res.status(400).json('Erro ao salvar os dados');
    }
}

function returnErrorTable(req, res, err) {
  return res.json({
    mensagem: err.message,
    error: err
  });
}


function validarElegiveis(res, data, campanha, next) {
  let dataRet = []
  let itensComErros = [];
  let itensProcessados = 0,
      itensRecusados = 0;
  let produtoCampanha = campanha.produtos[0];
      
  modelProduto.findById(campanha.produtos[0].produto).then(produtoBanco => { 
    data.forEach((element) => {
      const model = new modelElegivel(element)

      const cpf = element.cpf
      const cpfr = element.cpfresp
      let dep = false
      if (campanha) {
        model.campanha = campanha._id
      }
      if (element.dependente !== '') dep = true

      model.cnpj = validacao.getCNPJ(`${model.cnpj}`)
      model.cpf = validacao.getCPF(`${cpf}`)
      model.cpfresp = cpfr ? validacao.getCPF(`${cpfr}`) : model.cpf
      model.matricula = element.matricula
      model.nome = element.nome
      model.dependente = dep
      model.criado_por = res.usuario._id.toString()
      model.data = moment.tz(new Date(), "America/Sao_Paulo").toDate()
      model.email = element.email
      if (!isNaN(element.dtnasc)) {
        let momento = moment('01/01/1900', 'DD/MM/YYYY').add(element.dtnasc, 'days')
        let mes = momento.date()-1;
        let dia = momento.month()+1;
        momento.date(dia)
        momento.month(mes)
        model.dtnasc = momento.toDate().toISOString()
      }
      else if(moment(element.dtnasc, 'DD-MM-YYYY').isValid()) 
        model.dtnasc = moment(element.dtnasc, 'DD/MM/YYYY').toDate().toISOString()
      
      model.imunizacoes = [];

      produtoCampanha.lotes.forEach(lote => {
        const imunizacao = {
          produto: produtoCampanha._id,
          desc_produto: produtoBanco.produto,
          lote: lote._id,
          num_lote: lote.lote,
          aplicacao: false
        }
        model.imunizacoes.push(imunizacao);
      });
        
      validatorElegivel(model, errorsValidatorElegivel => {
        itensProcessados++;
        if (errorsValidatorElegivel.length != 0) {
          dataRet.push({ data: element, errors: errorsValidatorElegivel })
          itensRecusados++
        } else {
          model.save(err => {
            if(err) itensComErros.push({erro: err, elegivel: model})
          })
        }
        if(itensProcessados == data.length) {
          next({erros: dataRet, total: data.length, itensRecusados: itensRecusados})
        }
      });
    })
  })
} // ValidarElegiveis

function validarRoteiros(data, campanha, next) {

  /*
  let dataRet = []

  data.forEach((element) => {
    const model = new modelElegivel(element)

    const cpf = element.cpf
    const cpfr = element.cpfresp
    const dep = false
    if (campanha) {
      model.campanha = campanha
    }
    if (element.dependente !== '') dep = true

    model.cnpj = model.cnpj.toString()
    model.cpf = cpf.toString()
    model.cpfresp = cpfr.toString()
    model.dependente = dep

    const error = model.validateSync()
    if (error) {
	  dataRet.push({ data: element.nome, errors: error })
    }
  })
  return next(dataRet)
  */
} // validarRoteiros

function validarCampanha(data, next) {
    const model = new modelCampanha(data)
    model.cnpj = model.cnpj.toString()

    const error = model.validateSync()
    if (error) {
        return next({ data: data.campanha, errors: error })
    }
    return next({ data: data.campanha, errors: '' })
} // validarCampanha

function callbackTable(req, res, length, data) {
  return res.json({
    contagem: length,
    retorno: data
  });
}

const crudCargas = {

  validacao: (req, res, campanha, tipo, data, next) => {
    let dataRet = []

    switch (tipo) {
	  case 'Campanha':
        validarCampanha(data, (ret) => {
          next(ret)
        })
        break;

	  case 'Elegiveis':
        validarElegiveis(res, data, campanha, (ret) => {
          next(ret)
        })

	  case 'Roteiros':
        validarRoteiros(data, campanha, (ret) => {
          next(ret)
        })
        break;

	  default:
        break;
    }
  },

  carga: (req, res, next) => {
    let modelo = new modelCarga(req.body);

    modelo
	  .save()
	  .then(callback.bind(null, req, res))
	  .catch(returnError.bind(null, req, res));
  },

  recuperarCargasDeArquivos: (req, res) => {
       
    let query = montaParams(req.url, "query");
    let page = 0, size = 0;

    if(query.page) page = Number.parseInt(query.page)
    if(query.size) size = Number.parseInt(query.size) 
    else size = 1
 
    let fields = {campanha: query.campanha, tipo: query.tipo, status: { $in: ['Em processamento', 'Processado com erros', 'Processado com sucesso']}};

    modelCarga.countDocuments(fields).exec().then(count => {
      modelCarga.find(fields)
        .skip(page * size)
        .limit(size)
        .exec()
        .then(callbackTable.bind(null, req, res, count))
        .catch(returnErrorTable.bind(null, req, res));
    });

  },

};

module.exports = crudCargas;
