'use strict';

// @arquivo: carga-model.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const mongoose = require("mongoose"),
  beautifyUnique = require("mongoose-beautiful-unique-validation")
//* ***************************************************************************
const moment = require("moment"),
  parametros = require('../../../ambiente/parametros')
//* ***************************************************************************


module.exports = function (_schema, colecao) {
  const cargaSchema = _schema;

  cargaSchema.retainKeyOrder = true;

  cargaSchema.index({
    usuario: 1,
    campanha: 1,
    tipo: 1,
    filename: 1
  }, {
    unique: false,
  });

  cargaSchema.plugin(beautifyUnique);

  cargaSchema.pre("save", function (next) {
    this.data = new Date().toLocaleDateString('pt-BR');
    return next()
  });


  return mongoose.model(colecao, cargaSchema);
};
