'use strict';

// @arquivo: empresa-model.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const refPromises = require('mongoose-ref-promises');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const deepPopulate = require('mongoose-deep-populate')(mongoose);


module.exports = function(_schema, colecao) {

    const empresaSchema = _schema;

    empresaSchema.retainKeyOrder = true;

    empresaSchema.index({
        'empresa': 1,
        'cnpj': 1,
        'tipo': 1,
    }, {
        unique: true
    });

    empresaSchema.plugin(refPromises);
    empresaSchema.plugin(beautifyUnique);

    empresaSchema.plugin(deepPopulate, {
        whitelist: ['endereco', 'criado_por'],
        populate: {
            "endereco": {
                select: ""
            },
            "criado_por": {
                select: "usuario email"
            }
        }
    });

    return mongoose.model(colecao, empresaSchema);

};
