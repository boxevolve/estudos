"use strict";

// @arquivo: empresa-crud-custom.js
// @autor  : Clayton C. Carvalho
// @data   : 28.11.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const c_e = "E",
    c_s = "S";

const empresaSchema = require("./empresa-schema"),
      modelEmpresa = require("./empresa-model")(empresaSchema, "empresas")

const enderecoSchema = require("../enderecos/endereco-schema"),
      modelEndereco = require("../enderecos/endereco-model")(enderecoSchema, "enderecos");

const querystring = require("querystring"),
      url = require("url");

const montaParams = (_url, param) => {
  let url_parts = url.parse(_url);
  if (param === "query") return querystring.parse(url_parts.query);
};

function get_headerOrigem(req) {
  return req.headers["x-access-origem"];
}

function callback(req, res, data) {
  return res.json({
    contagem: data.length,
    retorno: data
  });
}

function callbackTable(req, res, length, data) {
  return res.json({
  contagem: length,
  retorno: data
  });
}

function returnError(req, res, err) {
  return res.json({
    mensagem: err.message,
    error: err
  });
}

const crudEmpresa = {
  create: (req, res) => {
    let modelo = new modelEmpresa(req.body);

    modelo.data = new Date()
    modelo.criado_por = res.usuario.id

    if(!req.body.endereco._id) {
      let modeloEnd = new modelEndereco(req.body.endereco)
          modeloEnd.data = new Date()
          modeloEnd.criado_por = res.usuario.id
          modeloEnd.save()
                   .then(data => {
                      modelo.endereco = data._id
                      modelo
                      .save()
                      .then(callback.bind(null, req, res))
                      .catch(returnError.bind(null, req, res));
                    }).catch(returnError.bind(null, req, res));
    } else {
      modelo.save()
            .then(callback.bind(null, req, res))
            .catch(returnError.bind(null, req, res));
    }
  },

  retrieve: (req, res, sort, fields, deepArray) => {
    let query = montaParams(req.url, "query");
    let tipo = '', sort1 = '', order = '', page = 0, size = 0;

    if(query.tipo) tipo = query.tipo
    if(query.sort) sort1 = query.sort
    if(query.order) order = query.order
    if(query.page) page = query.page
    if(query.size) {
        size = Number.parseInt(query.size)
    } else size = 1

    if (deepArray === undefined || deepArray.length === 0) {
      modelEmpresa.find({ 'tipo': tipo }, fields)
                  .skip(page * size)
                  .limit(size)
                  //.sort({sort1: order})
                  .exec()
                  .then( callback.bind(null, req, res) )
                  .catch( returnError.bind(null, req, res) );
    } else {
      modelEmpresa.find({ 'tipo': tipo }, fields)
                  .deepPopulate(deepArray)
                  .skip(page * size)
                  .limit(size)
                  //.sort({sort1: order})
                  .exec()
                  .then(
                      callback.bind(null, req, res))
                  .catch(
                      returnError.bind(null, req, res));
    }
  },

  listafornecedores: (req, res, deepArray) => {
    if (deepArray === undefined || deepArray.length === 0) {
      modelEmpresa.find({ 'tipo': 'Fornecedores' })
                  .exec()
                  .then( callback.bind(null, req, res) )
                  .catch( returnError.bind(null, req, res) );
    } else {
        modelEmpresa.find({ 'tipo': 'Fornecedores' })
                    .deepPopulate(deepArray)
                    .exec()
                    .then( callback.bind(null, req, res) )
                    .catch( returnError.bind(null, req, res) );
    }
  },

  listaClientes: (req, res, deepArray) => {
    if (deepArray === undefined || deepArray.length === 0) {
      modelEmpresa.find({ 'tipo': 'Clientes' })
                  .exec()
                  .then( callback.bind(null, req, res) )
                  .catch( returnError.bind(null, req, res) );
    } else {
      modelEmpresa.find({ 'tipo': 'Clientes' })
                  .deepPopulate(deepArray)
                  .exec()
                  .then( callback.bind(null, req, res) )
                  .catch( returnError.bind(null, req, res) );
    }
  },

  update: (req, res, deepArray) => {
    let modelo = req.body,
        query = montaParams(req.url, "query"),
        options = {
            runValidators: 1,
            multi: true
        };

    let mod = { $set: modelo };

    modelEmpresa.update(query, mod, options)
                .exec()
                .then(callback.bind(null, req, res))
                .catch(returnError.bind(null, req, res));
  },

  retrieveAll: (req, res) => {
    let query = montaParams(req.url, "query");

    let page = query.page ? Number.parseInt(query.page) : 0;      
    let size = query.size ? Number.parseInt(query.size) : 0;
    
    modelEmpresa.count((err, count) => {
      modelEmpresa.find()
                  .skip(page * size)
                  .limit(size)
                  .exec()
                  .then(callbackTable.bind(null, req, res, count))
                  .catch(returnError.bind(null, req, res));
    });
  },

};

module.exports = crudEmpresa;