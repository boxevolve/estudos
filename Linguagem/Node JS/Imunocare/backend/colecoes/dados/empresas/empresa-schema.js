'use strict';

// @arquivo: empresa-schema.js
// @autor  : Clayton C. Carvalho
// @data   : 17.02.2018
// @projeto: ImunoApp

// Constantes do Mongoose
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fields = require('../../fields/fields');

    // Campos do Schema

const jsonEmpresa = {
    empresa: fields.cmpString(), 
    cnpj: fields.cmpCNPJ(),
    tipo: fields.cmpTipoEmpresa(),
    responsavel: fields.cmpString(),
    email: fields.cmpString(),
    fone: fields.cmpString20(),
    endereco: fields.cmpRefId('enderecos', true),
    numero: fields.cmpNumber(),
    complemento: fields.cmpString20(),
    criado_por: fields.cmpRefId('usuarios', true),
    data: fields.cmpData()
};

module.exports = new Schema(jsonEmpresa); 
