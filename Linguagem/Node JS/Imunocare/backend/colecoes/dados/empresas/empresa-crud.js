"use strict";

// @arquivo: empresa-crud.js
// @autor  : Clayton C. Carvalho
// @data   : 17.02.2018
// @projeto: ImunoApp

const empresaSchema = require("./empresa-schema"),
modelEmpresa = require("./empresa-model")(empresaSchema, "empresas"),
crudDinamico = require("../../../utilitarios/crud/crud_dinamico")(modelEmpresa, empresaSchema);

module.exports = crudDinamico;