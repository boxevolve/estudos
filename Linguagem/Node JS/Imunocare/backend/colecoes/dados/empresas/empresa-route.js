// @arquivo: empresa-route.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const c_e = 'E',
    c_s = 'S';

const deepArray = ['endereco', 'criado_por'];

const crudEmpresa = require('./empresa-crud'),
    crudEndereco = require('../enderecos/endereco-crud'),
    crudEmpresaCustom = require('./empresa-crud-custom');
//const validaToken = require("../../login/validarToken");

//router.route('/create').post(validaToken, (req, res, next) => {
router.route('/create').post((req, res, next) => {
    crudEmpresa.create(req, res);
});

router.route('/retrieveAll').get((req, res)=>{
    crudEmpresaCustom.retrieveAll(req, res);
});

//router.route('/retrieve').get(validaToken, (req, res, next) => {
router.route('/retrieve').get((req, res, next) => {
    //router.get('/retrieve', function validaToken(req, res, next) {
    const sort = {},
    fields = {};
    crudEmpresaCustom.retrieve(req, res, sort, fields, deepArray);
});

//router.route('/retrieve').get(validaToken, (req, res, next) => {
router.route('/listafornecedores').get((req, res, next) => {
    crudEmpresaCustom.listafornecedores(req, res, deepArray);
});

router.route('/listaClientes').get((req, res, next) => {
    crudEmpresaCustom.listaClientes(req, res, deepArray);
});

//router.route('/findOne').get(validaToken, (req, res, next) => {
router.route('/findOne').get((req, res, next) => {
    //router.get('/findOne', function(req, res, next) {
    const fields = {};      
    crudEmpresa.findOne(req, res, fields, deepArray);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/update').patch((req, res, next) => {
    crudEmpresa.update(req, res);
});


//router.route('/delete').delete(validaToken, (req, res, next) => {
router.route('/delete').delete((req, res, next) => {
    crudEmpresa.delete(req, res);
});

module.exports = router;