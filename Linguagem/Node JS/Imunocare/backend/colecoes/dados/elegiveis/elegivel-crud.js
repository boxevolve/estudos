"use strict";

// @arquivo: elegivel-crud.js
// @autor  : Clayton C. Carvalho
// @data   : 09.11.2017
// @projeto: ImunoApp


const elegivelSchema = require("./elegivel-schema"),
modelElegivel = require("./elegivel-model")(elegivelSchema, "elegiveis"),
crudDinamico = require("../../../utilitarios/crud/crud_dinamico")(modelElegivel, elegivelSchema);

module.exports = crudDinamico;