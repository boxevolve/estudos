'use strict';

// @arquivo: elegiveis-schema.js
// @autor  : Clayton C. Carvalho
// @data   : 02.11.2017
// @projeto: ImunoApp

// Constantes do Mongoose
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fields = require('../../fields/fields');

// Criando Schema
const jsonElegiveis = {
    campanha: fields.cmpRefId('campanhas'),
    matricula: fields.cmpString(),
    cpf: fields.cmpCPF(),
    nome: fields.cmpStringObrigatorio(),
    dependente: fields.cmpBoolean(),
    cpfresp: fields.cmpCPF(),
    dtnasc: fields.cmpData(),
    sexo: fields.cmpString(),
    classificacao: fields.cmpString(),
    email: fields.cmpString(),
    cnpj: fields.cmpString(),
    cliente: fields.cmpString(),
    imunizacoes: [{
        produto: fields.cmpString(),
        desc_produto: fields.cmpString(),
        lote: fields.cmpString(),
        num_lote: fields.cmpString(),
        aplicacao: fields.cmpBoolean(),
        duplicidade: fields.cmpNumber(),
        usuario: fields.cmpRefId('usuarios', true), //  || fields.cmpString(),
        data: fields.cmpData()
    }],
    pendente_aprovacao: fields.cmpBoolean()
};

module.exports = new Schema(jsonElegiveis);