// @arquivo: pessoa-route.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

    const config = require('../../../config/config'),
        path = require('path'),
        sync_routes = path.join(config.root, '/SYNC/sincronizacoes/');

const fs = require('fs');

const c_e = 'E',
    c_s = 'S';

let deepArray = ['campanha'];
//const deepArray = ['campanha', 'imunizacoes.produto', 'imunizacoes.lote', 'imunizacoes.usuario'];
const crudElegivel = require('./elegivel-crud');
const crudElegivelCustom = require('./elegivel-crud-custom');


//const validaToken = require("../../login/validarToken");

//router.route('/create').post(validaToken, (req, res, next) => {
router.route('/carga').post((req, res, next) => {
    crudElegivelCustom.carga(req, res);
});

router.route('/create').post((req, res, next) => {
    crudElegivel.create(req, res);
});

router.route('/buscaElegiveisPorCPF').get((req, res, next) => {
    crudElegivelCustom.buscaElegiveisPorCPF(req, res);
});
        
//router.route('/retrieve').get(validaToken, (req, res, next) => {
router.route('/retrieve').get((req, res, next) => {
    //router.get('/retrieve', function validaToken(req, res, next) {
    const sort = {'cpf': 1},
    fields = {};
    deepArray = ['imunizacoes.usuario'];
    //crudElegivelCustom.retrieve(req, res, sort, fields, deepArray);
    crudElegivel.retrieve(req, res, sort, fields, deepArray);
});

//router.route('/retrieve').get(validaToken, (req, res, next) => {
router.route('/elegiveisCampanha').get((req, res, next) => {
    //router.get('/retrieve', function validaToken(req, res, next) {
    const sort = {'cnpj': 1, 'cpf': 1},
    fields = {};
    deepArray = ['imunizacoes.usuario'];
    // crudElegivelCustom.retrieve(req, res, sort, fields, deepArray);
    crudElegivelCustom.elegiveisCampanha(req, res, sort, fields, deepArray);
});

router.route('/dadosCampanha').get((req, res, next) => {
    //router.get('/retrieve', function validaToken(req, res, next) {
    const sort = {},
    fields = {};
    crudElegivelCustom.dadosCampanha(req, res, sort, fields, deepArray);
});

//router.route('/findOne').get(validaToken, (req, res, next) => {
router.route('/findOne').get((req, res, next) => {
    //router.get('/findOne', function(req, res, next) {
    const fields = {};
    crudElegivel.findOne(req, res, fields, deepArray);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/update').patch((req, res, next) => {
    //router.put('/update', function(req, res, next) {
    crudElegivel.update(req, res, deepArray);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/correcaoCNPJ').put((req, res, next) => {
    //router.put('/update', function(req, res, next) {
    crudElegivelCustom.correcaoCNPJ(req, res);
});

//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/atualizaCNPJElegivel').put((req, res, next) => {
    //router.put('/update', function(req, res, next) {
    crudElegivelCustom.atualizaCNPJElegivel(req, res);
});

//router.route('/delete').delete(validaToken, (req, res, next) => {
router.route('/delete').delete((req, res, next) => {
    crudElegivel.delete(req, res);
});

router.route('/registraAplicacao').post((req, res, next) => {
    crudElegivelCustom.registraImunizacao(req, res);
});

router.route('/sincronizaImunizacoes').post((req, res, next) => {

        let hora = new Date(Date.now()).toLocaleString()  //toTimeString() //toLocaleDateString('pt-BR')
        let arquivo = sync_routes + res.usuario.usuario + '-' + hora + '.txt'
        let body = JSON.stringify(req.body)

        //console.log(arquivo);

        fs.writeFile(arquivo, body, {enconding:'utf-8', flag: 'w'}, function (err) {
            //  if (err) console.log('Erro ao gravar arquivo');
            // console.log('Arquivo salvo!');
        });

    crudElegivelCustom.sincronizaImunizacoes(req, res);
});

router.route('/ajustaKPIsCampanhas').post((req, res, next) => {
    crudElegivelCustom.ajustaKPIsCampanhas(req, res);
});

router.route('/getByCampanha').get((req, res, next) => {
    const sort = {'cpf': 1},
    fields = {};
    deepArray = ['imunizacoes.usuario'];
    crudElegivelCustom.getByCampanha(req, res, sort, fields, deepArray);
});

router.route('/aprovarElegivel').get((req, res, next) => {
    let fields = {};
    deepArray = ['imunizacoes.usuario'];
    crudElegivelCustom.aprovarElegivel(req, res, fields, deepArray);
});

module.exports = router;