"use strict";

// @arquivo: elegivel-crud-custom.js
// @autor  : Clayton C. Carvalho
// @data   : 21.02.2018
// @projeto: ImunoApp

const mongoose = require('mongoose');
const c_e = "E",
  c_s = "S";

const crudElegivel = require('./elegivel-crud');

const usuarioSchema = require("../../seguranca/usuario/usuario-schema"),
  modelUsuario = require("../../seguranca/usuario/usuario-model")(usuarioSchema, "usuarios");

const elegivelSchema = require("./elegivel-schema"),
  modelElegivel = require("./elegivel-model")(elegivelSchema, "elegiveis");

const campanhaSchema = require("../campanhas/campanha-schema"),
  modelCampanha = require("../campanhas/campanha-model")(campanhaSchema, "campanhas");

const fields = require('../../fields/fields'),
  querystring = require("querystring"),
  url = require("url");

const montaParams = (_url, param) => {
  let url_parts = url.parse(_url);
  if (param === "query") return querystring.parse(url_parts.query);
};

let campanhas = [];

let dataCamp = [],
  dataEleg = [],
  dataImu = [],
  errosCamp = [],
  errosEleg = [],
  errosImu = [],
  options = {
    // runValidators: false,
    new: true,
    upsert: true,
  };

function trim(str) {
  if (str) {
    return str.replace(/^\s+|\s+$/g, "");
  } else {
    return ''
  }
}

function desformatarCPF(cpf) {
  if (cpf) {
    return cpf.replace(/\D/g, '')
  } else {
    return ''
  }
}

function get_headerOrigem(req) {
  return req.headers["x-access-origem"];
}

function callbackSinc(c, data) {
  if (c === 'C') {
    dataCamp.push(data);
  } else if (c === 'E') {
    dataEleg.push(data);
  } else if (c === 'I') {
    dataImu.push(data)
  }
}

function returnErrorSinc(c, err) {
  if (c === 'C') {
    errosCamp.push(err);
  } else if (c === 'E') {
    errosEleg.push(err);
  } else if (c === 'I') {
    errosImu.push(err)
  }
}

function callback(req, res, msg, data) {
  return res.json({
    mensagem: msg,
    contagem: data.length,
    retorno: data,
  });
}


function callbackMultiData(req, res, msg, camp, data) {
  return res.json({
    mensagem: msg,
    contagem: data.length,
    retorno: {
      campanha: camp,
      elegiveis: data,
    },
  });
}

function callbackTable(req, res, length, data) {
  return res.json({
    contagem: length,
    retorno: data
  });
}

function returnError(req, res, msg, retHttp, err) {
//  console.log(err);
  return res.status(retHttp).json({
    mensagem: msg,
    error: err,
  });
}

// function atualizarCampanha(req, res, imunizacao, sinc, elegivel) {
function atualizarCampanha(data, elegivel, produto, lote, duplicidade) {
  let elegivel_m = 0,
    elegivel_f = 0,
    elegivel_d = 0,
    elegivel_c = 0;

  if (elegivel.sexo === 'M') {
    elegivel_m = 1;
  } else {
    elegivel_f = 1;
  }
  if (elegivel.dependente) {
    elegivel_d = 1;
  } else {
    elegivel_c = 1;
  }
  let achouCnpj = false
  let cnpjT = []

  if (elegivel.cnpj === undefined) {
    elegivel.cnpj = ""
  }

  if (data.cnpj_total) {
    for (let c = 0; c < data.cnpj_total.length; c++) {
      if (trim(data.cnpj_total[c].cnpj) === trim(elegivel.cnpj)) {
        data.cnpj_total[c].cnpj_aplicacoes = data.cnpj_total[c].cnpj_aplicacoes + 1;
        achouCnpj = true;
        break;
      }
    }
    cnpjT = data.cnpj_total;

    if (!achouCnpj) {
      cnpjT.push({ cnpj: trim(elegivel.cnpj), cnpj_aplicacoes: 1 });
    }
  } else {
    cnpjT.push({ cnpj: trim(elegivel.cnpj), cnpj_aplicacoes: 1 });
  }

  data.cnpj_total = cnpjT;

  for (let p = 0; p < data.produtos.length; p++) {
    if (data.produtos[p]._id.toString() === produto) {
      // data.produtos[p].duplicidade = 0;
      for (let l = 0; l < data.produtos[p].lotes.length; l++) {
        if (data.produtos[p].lotes[l]._id.toString() === lote) {
          if (data.produtos[p].lotes[l].lote_aplicacoes) {
            data.produtos[p].lotes[l].lote_aplicacoes = data.produtos[p].lotes[l].lote_aplicacoes + 1 + duplicidade;
          } else {
            data.produtos[p].lotes[l].lote_aplicacoes = 1;
          }
          if (data.produtos[p].produto_aplicacoes) {
            data.produtos[p].produto_aplicacoes = data.produtos[p].produto_aplicacoes + 1 + duplicidade;
          } else {
            data.produtos[p].produto_aplicacoes = 1;
          }
          if (data.produtos[p].duplicidade === undefined) {
            data.produtos[p].duplicidade = 0;
          }
          data.produtos[p].duplicidade = data.produtos[p].duplicidade + duplicidade;
          if (data.total_aplicacoes) {
            data.total_aplicacoes = data.total_aplicacoes + 1 + duplicidade;
          } else {
            data.total_aplicacoes = 1;
          }
          data.produtos[p].elegiveis_c = data.produtos[p].elegiveis_c + elegivel_c;
          data.produtos[p].elegiveis_d = data.produtos[p].elegiveis_d + elegivel_d;
          data.produtos[p].elegiveis_f = data.produtos[p].elegiveis_f + elegivel_f;
          data.produtos[p].elegiveis_m = data.produtos[p].elegiveis_m + elegivel_m;
          data.elegiveis_c = data.elegiveis_c + elegivel_c;
          data.elegiveis_d = data.elegiveis_d + elegivel_d;
          data.elegiveis_f = data.elegiveis_f + elegivel_f;
          data.elegiveis_m = data.elegiveis_m + elegivel_m;
        }
      }
    }
  }
}

function atualizaImunizacaoElegiveis(imunizacao, idUsuario) {
  let achouCamp = false;

  return new Promise(function (executeNext, executeError) {
    return modelElegivel.findOne({
      '_id': imunizacao.idElegivel,
    }) // , null, options)
      .exec()
      .then((elegivel) => {
        // console.log(`Elegivel ok: ${elegivel.nome}`)
        elegivel.atualizarImunizacao(imunizacao, idUsuario, true)
          .then((imuni) => {
            dataEleg.push(elegivel);
            dataImu.push(imunizacao);
            return executeNext(imuni);
          })
          .catch((err) => {
            errosImu.push(err)
            return executeError(err);
          });
      })
      .catch((err) => {
        // console.log(`Elegivel err: ${imunizacao.idElegivel}`)
        errosEleg.push(err)
        return executeError(err);
      });
  })
}

const crudElegiveis = {


  carga: (req, res) => {
    let // modelArray = [modelElegivel],
      objArray = [],
      promise = [],
      contador = 0,
      total = 0,
      dataAll = [],
      errAll = [],
      imunizacoes = [];

    const newImu = function () {
      return {
        produto: ' ',
        desc_produto: ' ',
        lote: ' ',
        num_lote: ' ',
        aplicacao: false,
        data: undefined,
      }
    }

    objArray = req.body;
    // modelArray = objArray;
    total = objArray.length;

    let query = montaParams(req.url, "query");

    modelCampanha.findOne({ '_id': query._id })
      .then((campanha) => {
        campanha.produtos.forEach((produto) => {
          produto.lotes.forEach((lote) => {
            let imunizacao = newImu();
            imunizacao.produto = produto._id;
            imunizacao.desc_produto = produto.produto;
            imunizacao.lote = lote._id;
            imunizacao.num_lote = lote.lote;
            if (query.imu) {
              imunizacao.aplicacao = true;
              imunizacao.data = '2018-05-18';
            }
            imunizacoes.push(imunizacao);
          });
        });

        objArray.forEach((item) => {
          let modelo = new modelElegivel(item);
          if (item.data !== undefined) {
            for (let i = 0; i < imunizacoes.length; i++) {
              imunizacoes[i].data = item.data
            }
          }
          modelo.imunizacoes = imunizacoes;
          promise.push(modelo.save());
        });

        const updates = {
          $inc: { total_elegiveis: total },
        };

        promise.push(campanha.update(updates))

        Promise.all(promise)
          .then((data) => {
            contador = data.length - 1;
            dataAll = data;
            return res.json({
              mensagem: `Registros criados ${contador} de um total de ${total}`,
              contagem: `${contador}/${total}`,
              retorno: dataAll,
              erros: errAll,
            });
          })
          .catch((err) => {
            errAll = err;
            return res.status(206).json({
              mensagem: `Registros criados ${contador} de um total de ${total}`,
              contagem: `${contador}/${total}`,
              retorno: dataAll,
              erros: errAll,
            });
          });
      })
      .catch(returnError.bind(null, req, res, 'Campanha não encontrada', 204));
  },

  registraImunizacao: (req, res) => {
    const imunizacao = req.body;
    let query = montaParams(req.url, "query");

    // Verifica se elegivel já foi imunizado com esta vacina e lote, nesta campanha
    modelElegivel
      .findOne(query, null, options)
      .exec()
      .then((elegivel) => {
        modelCampanha.verificarQtdeProdutoELote(elegivel, imunizacao, res)
          .then((data) => {
            elegivel.atualizarImunizacao(imunizacao, res.usuario._id, false)
              .then((eleg) => {
                data.atualizarCampanha() 
                  .then(camp => res.status(200).json({
                    mensagem: 'Imunizacao registrada com sucesso',
                  }))
                  .catch(returnError.bind(null, req, res, 'Erro ao atualizar', 405))
              })
              .catch(returnError.bind(null, req, res, 'Erro ao atualizar imunizacao', 405));
          })
          .catch(returnError.bind(null, req, res, 'Campanha não encontrada', 405))
      })
      .catch(returnError.bind(null, req, res, 'Elegivel não encontrado', 204));
  },

  sincronizaImunizacoes: (req, res) => {
    const imunizacoes = req.body;
    let query = montaParams(req.url, "query");
    let promises = [];

    if (imunizacoes.length > 0) {
      modelElegivel.findOne({ '_id': imunizacoes[0].idElegivel })
        .exec()
        .then((elegivel) => {
          modelCampanha.findOne({
            "_id": elegivel.campanha,
          }, null, options)
            .exec()
            .then((campanha) => {
              dataCamp = []
              dataEleg = []
              dataImu = []

              for (let imunizacao of imunizacoes) {
                if (imunizacao.usuario) {
                  promises.push(atualizaImunizacaoElegiveis(imunizacao, imunizacao.usuario));
                } else {
                  promises.push(atualizaImunizacaoElegiveis(imunizacao, res.usuario._id));
                }
              }

              Promise.all(promises)
                .then((data) => {
                  for (let imunizacao of dataImu) {
                    for (let e = 0; e < dataEleg.length; e++) {
                      for (let i = 0; i < dataEleg[e].imunizacoes.length; i++) {
                        if (dataEleg[e].imunizacoes[i]._id.toString() === imunizacao._id.toString()) {
                          let produto = dataEleg[e].imunizacoes[i].produto;
                          let lote = dataEleg[e].imunizacoes[i].lote;
                          atualizarCampanha(campanha, dataEleg[e], produto, lote, 0)
                          break;
                        }
                      }
                    }
                  }

                  campanha.save()
                    .then(datac => dataCamp.push(datac))
                    .catch(err => errosCamp.push(err))

                  return res.json({
                    mensagem: 'Sincronização finalizada',
                    contagem: {
                      'Dados atualizados': {
                        'Campanhas': dataCamp.length,
                        'Elegiveis': dataEleg.length,
                        'Imunizacoes': dataImu.length,
                      },
                      'Erros': {
                        'Campanhas': errosCamp,
                        'Elegiveis': errosEleg,
                        'Imunizacoes': errosImu,
                      },
                    },
                  })
                })
                .catch(err => res.status(400).json({
                  mensagem: 'Erro ao executar sincronização',
                  contagem: {
                    'Dados atualizados': {
                      'Campanhas': dataCamp.length,
                      'Elegiveis': dataEleg.length,
                      'Imunizacoes': dataImu.length,
                    },
                    'Erros': {
                      'Campanhas': errosCamp,
                      'Elegiveis': errosEleg,
                      'Imunizacoes': errosImu,
                    },
                  },
                }))
            })
            .catch(returnError.bind(null, req, res, 'Campanha não encontrada', 204))
        })
        .catch(returnError.bind(null, req, res, 'Erro ao validar Elegível', 204));
    } else {
      return res.status(400).json({
        mensagem: 'Erro ao executar sincronização',
        error: 'Nenhuma imunização encontrada',
        contagem: {
          'Dados atualizados': {
            'Campanhas': dataCamp.length,
            'Elegiveis': dataEleg.length,
            'Imunizacoes': dataImu.length,
          },
          'Erros': {
            'Campanhas': errosCamp,
            'Elegiveis': errosEleg,
            'Imunizacoes': errosImu,
          },
        },
      })
    }
  },

  ajustaKPIsCampanhas: (req, res) => {
    let query = montaParams(req.url, "query");

    modelCampanha.findOne(query)
      .exec()
      .then((campanha) => {
        campanha.total_elegiveis = 0
        campanha.total_aplicacoes = 0
        campanha.elegiveis_m = 0
        campanha.elegiveis_f = 0
        campanha.elegiveis_d = 0
        campanha.elegiveis_c = 0

        for (let p = 0; p < campanha.produtos.length; p++) {
          campanha.produtos[p].produto_aplicacoes = 0
          campanha.produtos[p].elegiveis_m = 0
          campanha.produtos[p].elegiveis_f = 0
          campanha.produtos[p].elegiveis_d = 0
          campanha.produtos[p].elegiveis_c = 0
          for (let l = 0; l < campanha.produtos[p].lotes.length; l++) {
            campanha.produtos[p].lotes[l].lote_aplicacoes = 0
          }
        }

        campanha.cnpj_total = undefined

        modelElegivel.find({ 'campanha': campanha._id })
          .exec()
          .then((elegiveis) => {
            elegiveis.forEach((elegivel) => {
              for (let i = 0; i < elegivel.imunizacoes.length; i++) {
                if (elegivel.imunizacoes[i].aplicacao === true) {
                  let produto = elegivel.imunizacoes[i].produto;
                  let lote = elegivel.imunizacoes[i].lote;
                  let duplicidade = 0
                  if (elegivel.imunizacoes[i].duplicidade) {
                    duplicidade = elegivel.imunizacoes[i].duplicidade
                  }
                  atualizarCampanha(campanha, elegivel, produto, lote, duplicidade)
                }
              }
            })
            campanha.total_elegiveis = elegiveis.length
            campanha.save()
              .then(data => res.json({
                mensagem: 'Campanha atualizada com sucesso',
                retorno: data,
              }))
              .catch(err => res.json({
                mensagem: 'Erro ao atualizar Campanha',
                error: err.message,
              }))
          })
          .catch((err) => {
            res.json({
              mensagem: 'Elegiveis não encontrados',
              error: err.message,
            })
          })
      })
      .catch(err => res.json({
        mensagem: 'Campanha não econtrada',
        error: err.message,
      }))
  },

  dadosCampanha: (req, res, fields, sort, deepArray) => {
    let query = montaParams(req.url, "query");

    if (!query.campanha && res.usuario.perfil === 'ENFE') {
      return res.status(200).json({
        mensagem: 'Favor informar os parâmetros de seleção',
        error: 'OPERAÇÃO CANCELADA!',
      });
    }

    let cpf = ''
    if (query.cpf) {
      cpf = query.cpf
    }

    modelUsuario.find({ _id: res.usuario._id }, { 'campanhas': 1 })
      .exec()
      .then((data) => {
        let campanhas = [];
        data.forEach((usuario) => {
          usuario.campanhas.forEach((campanha) => {
            campanhas.push(mongoose.Types.ObjectId(campanha.campanha));
          });
        });

        modelCampanha.find({
          '_id': { $in: campanhas },
        }, {
          'valor_unitario_servico': 0,
        })
          .exec()
          .then((camp) => {
            if (cpf) {
              modelElegivel
                .find({
                  campanha: { $in: campanhas },
                  cpf: cpf,
                }, {
                  valor_unitario_servico: 0,
                })
                .exec()
                .then(callbackMultiData.bind(null, req, res, 'Campanhas encontradas', camp))
                .catch(returnError.bind(null, req, res, 'Elegivel não encontrado', 204));
            } else {
              modelElegivel
                .find({ campanha: { $in: campanhas } })
                .exec()
                .then(callbackMultiData.bind(null, req, res, 'Campanhas encontradas', camp))
                .catch(returnError.bind(null, req, res, 'Elegivel não encontrado', 204));
            }
          })
          .catch(returnError.bind(null, req, res, 'Campanha não encontrada', 204));
      })
      .catch(returnError.bind(null, req, res, 'Usuário atual não está vinculado a nenhuma campanha no momento', 203))
  },

  buscaElegiveisPorCPF: (req, res) => {
    let query = montaParams(req.url, "query");

    modelElegivel
      .find(query).sort({ 'cpfresp': 1, 'dependente': 1 })
      .exec()
      .then((data) => {
        if (data.length > 0) {
          return res.json({
            contagem: data.length,
            retorno: data,
          })
        } else {
          return res.status(204).json({
            mensagem: 'CPF não encontrado',
            error: err,
          })
        }
      })
      .catch(err => res.status(204).json({
        mensagem: err.message,
        error: err,
      }));
  },

  elegiveisCampanha: (req, res, sort, fields, deepArray) => {
    let query = montaParams(req.url, "query");

    modelElegivel
      .find(query, fields, sort)
      .deepPopulate(deepArray)
      .exec()
      .then(data => res.json({
        contagem: data.length,
        retorno: data,
      }))
      .catch(err => res.json({
        mensagem: err.message,
        error: err,
      }));
  },

  retrieve: (req, res, sort, fields, deepArray) => {
    let query = montaParams(req.url, "query");

    let cpf = '',
      campanha = '';

    if (query.cpf) {
      cpf = query.cpf
    }

    if (query.campanha) {
      campanha = query.campanha
    } else {
      return res.status(200).json({
        mensagem: 'Parâmetro Campanha não informado',
        error: 'OPERAÇÃO CANCELADA!',
      });
    }

    let page = parseInt(req.query._page || 1)
    let pageSize = parseInt(req.query._pageSize)
    page = page > 0 ? page : 1
    const skip = (page - 1) * pageSize
    console.log(`Page: ${page} / PageSize:${pageSize} / Skip: ${skip}`)
    let retornoDados = []

    if (deepArray === undefined || deepArray.length === 0) {
      /*
        modelElegivel
        // .find({'cpf': { $gt: cpf}}, fields, sort)
          .find({ 'campanha': campanha, 'cpf': { $gt: cpf } })
          .skip(skip)
          .limit(pageSize)
          .sort({ 'cpf': 1 })
          .exec()
          .then(data => res.json({
            contagem: data.length,
            retorno: data,
          }))
          .catch(err => res.json({
            mensagem: err.message,
            error: err,
          }));
      */

      modelElegivel.find({ 'campanha': campanha })
        // .skip(skip)
        // .limit(pageSize)
        .sort({ 'cpf': 1 })
        .cursor()
        .on('data', function(doc) { 
            return retornoDados.push(doc)
        })
        .on('end', function() { 
            return res.json({ retorno: retornoDados }) 
        });
        
    } else {
      /*
      modelElegivel
        .find({ 'campanha': campanha, 'cpf': { $gt: cpf } })
        .skip(skip)
        .limit(pageSize)
        .sort({ 'cpf': 1 })
        .deepPopulate(deepArray)
        .exec()
        .then(data => res.json({
          contagem: data.length,
          retorno: data,
        }))
        .catch(err => res.json({
          mensagem: err.message,
          error: err,
        }));
      */
      modelElegivel
        .find({ 'campanha': campanha })
        // .skip(skip)
        // .limit(pageSize)
        .sort({ 'cpf': 1 })
        .deepPopulate(deepArray)
        .cursor()
        .on('data', function(doc) { 
            return retornoDados.push(doc)
        })
        .on('end', function() { 
            return res.json({ retorno: retornoDados }) 
        });
    }
  },

  // /////////////////////////////////////////////////////////////////////////////////////

  correcaoCNPJ: (req, res) => {
    let // modelArray = [modelElegivel],
      objArray = [],
      promise = [],
      contador = 0,
      total = 0,
      dataAll = [],
      errAll = [],
      achou = false,
      busca = 0;

    objArray = req.body;
    // modelArray = objArray;
    total = objArray.length;

    let query = montaParams(req.url, "query");

    if (query.campanha) {
      modelElegivel.find(query)
        .then((elegiveis) => {
          objArray.forEach((item) => {
            for (let e = 0; e < elegiveis.length; e++) {
              let elegivel = elegiveis[e];
              //              if (desformatarCPF(elegivel.cpf) === item.CPF) {
              if (elegivel._id.toString() === item._id.toString()) {
                const updates = { cnpj: item.cnpj };
                promise.push(elegivel.update(updates))
                break;
              }
            }
          })

          if (promise.length > 0) {
            Promise.all(promise)
              .then((data) => {
                contador = data.length;
                dataAll = data;
                return res.json({
                  mensagem: `Registros atualizados ${contador} de um total de ${total}`,
                  contagem: `${contador}/${total}`,
                  retorno: dataAll,
                  erros: errAll,
                });
              })
              .catch((err) => {
                errAll = err;
                return res.status(206).json({
                  mensagem: `Registros atualizados ${contador} de um total de ${total}`,
                  contagem: `${contador}/${total}`,
                  retorno: dataAll,
                  erros: errAll,
                });
              });
          } else {
            return res.status(206).json({
              erros: `Nenhum elegível alterado`,
            })
          }
        })
        .catch(err => res.status(206).json({
          erros: `Elegíveis não encontrados`,
        }))
    }
  },

  atualizaCNPJElegivel: (req, res) => {
    let // modelArray = [modelElegivel],
      objArray = [],
      promise = [],
      contador = 0,
      total = 0,
      dataAll = [],
      errAll = [],
      achou = false,
      busca = 0;

    objArray = req.body;
    // modelArray = objArray;
    total = objArray.length;

    let query = montaParams(req.url, "query");

    modelCampanha.findOne({ '_id': query._id })
      .then((campanha) => {
        campanha.produtos.forEach((produto) => {
          produto.lotes.forEach((lote) => {
            let imunizacao = newImu();
            imunizacao.produto = produto._id;
            imunizacao.desc_produto = produto.produto;
            imunizacao.lote = lote._id;
            imunizacao.num_lote = lote.lote;
            if (query.imu) {
              imunizacao.aplicacao = true;
            }
            imunizacoes.push(imunizacao);
          });
        });

        objArray.forEach((item) => {
          let modelo = new modelElegivel(item);
          if (item.data) {
            for (let i = 0; i < imunizacoes.length; i++) {
              imunizacoes[i].data = item.data
            }
          }
          modelo.imunizacoes = imunizacoes;
          promise.push(modelo.save());
        });

        const updates = {
          $inc: { total_elegiveis: total },
        };

        promise.push(campanha.update(updates))

        Promise.all(promise)
          .then((data) => {
            contador = data.length - 1;
            dataAll = data;
            return res.json({
              mensagem: `Registros criados ${contador} de um total de ${total}`,
              contagem: `${contador}/${total}`,
              retorno: dataAll,
              erros: errAll,
            });
          })
          .catch((err) => {
            errAll = err;
            return res.status(206).json({
              mensagem: `Registros criados ${contador} de um total de ${total}`,
              contagem: `${contador}/${total}`,
              retorno: dataAll,
              erros: errAll,
            });
          });
      })
      .catch(returnError.bind(null, req, res, 'Campanha não encontrada', 204));
  },

  getByCampanha: (req, res, sort, fields, deepArray) => {
    let query = montaParams(req.url, "query");

    let page = query.page ? query.page : 0;
    let size = query.size ? Number.parseInt(query.size) : 1;
    fields = { 'campanha' : query.campanha };
    if (query.pendenteAprovacao) {
      fields['pendente_aprovacao'] = query.pendenteAprovacao;
    }

    modelElegivel.find(fields).count((err, count) => {
      if (!query.campanha) {
        return callbackTable(req, res, 0, []);
      }
      size = query.all ? count : size;

      if (deepArray === undefined || deepArray.length === 0) {
        modelElegivel.find(fields)
                     .skip(page * size)
                     .limit(size)
                     .exec()
                     .then( callbackTable.bind(null, req, res, count) )
                     .catch( returnError.bind(null, req, res) );
      } else {
        modelElegivel.find(fields)
                     .deepPopulate(deepArray)
                     .skip(page * size)
                     .limit(size)
                     .exec()
                     .then( callbackTable.bind(null, req, res, count) )
                     .catch( returnError.bind(null, req, res) );
      }
    });
  },

  aprovarElegivel: (req, res, fields, deepArray) => {
    let query = montaParams(req.url, "query");

    if (deepArray.length === 0) {
      modelElegivel.findOne(query, fields)
                   .exec()
                   .then(elegivel => {
                      elegivel.pendente_aprovacao = false;
                      return crudElegivel.update(req, res, deepArray, elegivel);
                   })
                   .catch( returnError.bind(null, req, res) );
    } else {
      modelElegivel.findOne(query, fields)
                   .deepPopulate(deepArray)
                   .exec()
                   .then(elegivel => {
                      elegivel.pendente_aprovacao = false;
                      return crudElegivel.update(req, res, deepArray, elegivel);
                   })
                   .catch( returnError.bind(null, req, res) );
    }
  }

};

module.exports = crudElegiveis;
