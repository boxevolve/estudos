
'use strict';

// @arquivo: elegiveis-model.js
// @autor  : Clayton C. Carvalho
// @data   : 03.11.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const validacao = require('../../../validations/validations');

function campoObrigatorio(nomeCampo, valorCampo, arrayErros) {
    if(!valorCampo || valorCampo === undefined || valorCampo.toString().replace(/^\s+|\s+$/g,"").length === 0) arrayErros.push(`${nomeCampo} obrigatório`)
}

function validarCamposObrigatorios(_elegivel, errors) {
    campoObrigatorio('CNPJ', _elegivel.cnpj, errors)
    campoObrigatorio('Nome', _elegivel.nome, errors)
    campoObrigatorio('Matricula', _elegivel.matricula, errors)
    campoObrigatorio('Data de Nascimento', _elegivel.dtnasc, errors)
    // campoObrigatorio('Dependente', _elegivel.dependente, errors)
}

module.exports = function (schema, model, campanhaModel) {
    const _elegivelSchema = schema;
    const _elegivelModel = model;
    const _campanhaModel = campanhaModel;

    return (_elegivel, callback) => {
        var promises = [];
        promises.push(_elegivelModel.countDocuments({ cnpj: _elegivel.cnpj, campanha: _elegivel.campanha, cpf: _elegivel.cpf }).exec());
        promises.push(_campanhaModel.countDocuments({ _id: _elegivel.campanha._id, filiais: { $elemMatch: {cnpj: _elegivel.cnpj }}}).exec());


        Promise.all(promises).then(valores => {
            const errors = [];

            if (!_elegivel) {
                errors.push('Elegivel não preenchido');
            }

            validarCamposObrigatorios(_elegivel, errors)

            //CPF - Válido
            if (!validacao.validarCPF(_elegivel.cpf)) {
                errors.push('CPF inválido')
            }

            //CPF - Em um único CNPJ
            if (valores[0] > 0) errors.push('CPF em mais de um CNPJ')

            //CNPJ - Que existe na campanha
            if (valores[1] == 0) errors.push('CNPJ não existe em campanha')

            //Data de Nascimento válida.
            if (!validacao.validarData(_elegivel.dtnasc)) {
                errors.push('Data de nascimento invalida')
            }

            callback(errors);
        });
    };


};