"use strict";

// @arquivo: elegivel-crud-custom.js
// @autor  : Clayton C. Carvalho
// @data   : 21.02.2018
// @projeto: ImunoApp

const mongoose = require('mongoose');
const c_e = "E",
    c_s = "S";

const usuarioSchema = require("../../seguranca/usuario/usuario-schema"),
    modelUsuario = require("../../seguranca/usuario/usuario-model")(usuarioSchema, "usuarios");       

const elegivelSchema = require("./elegivel-schema"),
    modelElegivel = require("./elegivel-model")(elegivelSchema, "elegiveis");

const campanhaSchema = require("../campanhas/campanha-schema"),
    modelCampanha = require("../campanhas/campanha-model")(campanhaSchema, "campanhas");    

const fields = require('../../fields/fields'),
    querystring = require("querystring"),
    url = require("url");

const montaParams = (_url, param) => {
    let url_parts = url.parse(_url);
    if (param === "query") return querystring.parse(url_parts.query);
};

let campanhas = [];

let dataCamp = [],
    dataEleg = [],
    dataImu = [],
    errosCamp = [],
    errosEleg = [],
    errosImu = [],
    options = {
            // runValidators: false,
            new: true,
            upsert: true
    };

function get_headerOrigem(req) {
    return req.headers["x-access-origem"];
}

function callbackSinc(c, data) {
    if (c === 'C')
        dataCamp.push(data);
    else if ( c === 'E') {
        dataEleg.push(data);
    } else if ( c === 'I') {
        dataImu.push(data)
    }
}

function returnErrorSinc(c, err) {
    if (c === 'C')
        errosCamp.push(err);
    else if ( c === 'E') {
        errosEleg.push(err);
    } else if ( c === 'I') {
        errosImu.push(err)
    }
}

function callback(req, res, msg, data) {

    return res.json({
        mensagem: msg,
        contagem: data.length,
        retorno: data
    });
}


function callbackMultiData(req, res, msg, camp, data) {

    return res.json({
        mensagem: msg,
        contagem: data.length,
        retorno: {
            campanha: camp,
            elegiveis: data
        }
    });
}

function returnError(req, res, msg, retHttp, err) {
    console.log(err);
    return res.status(retHttp).json({
        mensagem: msg,
        error: err
    });
}

// function atualizarCampanha(req, res, imunizacao, sinc, elegivel) {
function atualizarCampanha(data, imunizacao, elegivel, produto, lote) {
/*
    let produto = '';
    let lote = '';

    for(let i = 0; i < elegivel.imunizacoes.length; i++) {
        if(elegivel.imunizacoes[i]._id.toString() === imunizacao._id) {
            produto = elegivel.imunizacoes[i].produto;
            lote = elegivel.imunizacoes[i].lote;
            break;
        }
    }
*/
    let elegivel_m = 0,
        elegivel_f = 0,
        elegivel_d = 0,
        elegivel_c = 0;

    if (elegivel.sexo === 'M') {
        elegivel_m = 1;
    } else {
        elegivel_f = 1;
    }
    if (elegivel.dependente) {
        elegivel_d = 1;
    } else {
        elegivel_c = 1;
    }
    /*
    modelCampanha.findOne( { "_id": elegivel.campanha, 
                            "produtos._id": produto, 
                            "produtos.lotes._id": lote }, null, options)
    .then(data => {
    */
    for (let p = 0; p < data.produtos.length; p++) {
        if (data.produtos[p]._id.toString() === produto) {
            for (let l = 0; l < data.produtos[p].lotes.length; l++) {
                if (data.produtos[p].lotes[l]._id.toString() === lote) {
                    if (data.total_aplicacoes) {
                        data.total_aplicacoes = data.total_aplicacoes + 1;
                    } else {
                        data.total_aplicacoes = 1;
                    }
                    data.elegiveis_c = data.elegiveis_c + elegivel_c;
                    data.elegiveis_d = data.elegiveis_d + elegivel_d;
                    data.elegiveis_f = data.elegiveis_f + elegivel_f;
                    data.elegiveis_m = data.elegiveis_m + elegivel_m;
                    if (data.produtos[p].produto_aplicacoes) {
                        data.produtos[p].produto_aplicacoes = data.produtos[p].produto_aplicacoes + 1;
                    } else {
                        data.produtos[p].produto_aplicacoes = 1;
                    }
                    data.produtos[p].elegiveis_c = data.produtos[p].elegiveis_c + elegivel_c;
                    data.produtos[p].elegiveis_d = data.produtos[p].elegiveis_d + elegivel_d;
                    data.produtos[p].elegiveis_f = data.produtos[p].elegiveis_f + elegivel_f;
                    data.produtos[p].elegiveis_m = data.produtos[p].elegiveis_m + elegivel_m;

                    if (data.produtos[p].lotes[l].lote_aplicacoes) {
                        data.produtos[p].lotes[l].lote_aplicacoes = data.produtos[p].lotes[l].lote_aplicacoes + 1;
                    } else {
                        data.produtos[p].lotes[l].lote_aplicacoes = 1;
                    }
                }
            }
        }
    }
    /*
                    data.save((err, data) => {
                        if(err) { 
                            // console.log(err);
                            return res.status(203).json({
                                mensagem: 'Imunizacao registrada com sucesso',
                                error: err
                            });
                        };
                        if (sinc === false) {
                            res.status(200).json({mensagem:'Imunizacao registrada com sucesso'})
                        } else {
                            callbackSinc('C', data)
                        }
                    });
    })
    .catch(err => { 
        if (sinc === false) {
            // console.log(err)
            returnError.bind(null, req, res, 'Erro ao atualizar KPIs da campanha', 304);  
        } else {
            // console.log(err)
            returnErrorSinc('C', err)
        }
    })  
    */
}

function atualizaImunizacaoElegiveis(imunizacao) {

    let achouCamp = false;

    return new Promise(function(executeNext, executeError) {

    // Verifica se elegivel já foi imunizado com esta vacina e lote, nesta campanha
    /*
    modelElegivel
        .findOne({'_id': imunizacao.idElegivel}, null, options)
        .exec()
        .then((elegivel) => {
            elegivel.atualizarImunizacao(imunizacao, imunizacao.usuario, true)
            .then(atualizarCampanha.bind(null, req, res, imunizacao, true))
            .catch(returnErrorSinc.bind(null, 'I'));
            dataEleg.push(elegivel);
        })
        .catch(returnErrorSinc.bind(null, 'E'));
    */
        return modelElegivel.findOne({'_id': imunizacao.idElegivel}, null, options)
            .exec()
            .then((elegivel) => {
                elegivel.atualizarImunizacao(imunizacao, imunizacao.usuario, true)
                    .then( imuni => {

                        dataEleg.push(elegivel);
                        dataImu.push(imunizacao);
                        achouCamp = false;

                        for(let i = 0; i < campanhas.length; i++) {
                            if (campanhas[i]._id.toString() === elegivel.campanha.toString()) {
                                let campanha = campanhas[i];
                                achouCamp = true;
                                break;
                            }
                        }
                        if (!achouCamp) {
                            modelCampanha.findOne( { "_id": elegivel.campanha }, null, options)
                                .exec()
                                .then(data => {
                                    campanhas.push(data)
                                    return executeNext();
                                })
                                .catch(err => {
                                    errosCamp.push(err);
                                    return executeError();
                                })
                        }
                    })
                    .catch(returnErrorSinc.bind(null, 'I'));
            })
            .catch(returnErrorSinc.bind(null, 'E'));
    })
}

const crudElegiveis = {


    carga: (req, res) => {

        let // modelArray = [modelElegivel],
            objArray = [],
            promise = [],
            contador = 0,
            total = 0,
            dataAll = [],
            errAll = [],
            imunizacoes = [];

        const newImu = function(){
            return {
                produto: ' ',
                desc_produto: ' ',
                lote: ' ',
                num_lote: ' ', 
                aplicacao: false
            }
        }

        objArray = req.body;
       // modelArray = objArray;
        total = objArray.length;

        let query = montaParams(req.url, "query");

        modelCampanha.findOne(query)
            .then( (campanha) => {
                campanha.produtos.forEach(produto => {
                    produto.lotes.forEach(lote => {
                        let imunizacao = newImu();
                        imunizacao.produto = produto._id;
                        imunizacao.desc_produto = produto.produto;
                        imunizacao.lote = lote._id;
                        imunizacao.num_lote = lote.lote;
                        imunizacao.aplicacao = false;
                        imunizacoes.push(imunizacao);
                    });
                }); 

                objArray.forEach(item => {
                    let modelo = new modelElegivel(item);
                    modelo.imunizacoes = imunizacoes;
                    promise.push(modelo.save());
                });

                const updates = {
                            $inc: { total_elegiveis: total }
                        };

                promise.push(campanha.update(updates))

                Promise.all(promise)
                .then((data) => {
                    contador = data.length - 1;
                    dataAll = data;
                    return res.json({
                        mensagem: `Registros criados ${contador} de um total de ${total}`,
                        contagem: `${contador}/${total}`,
                        retorno: dataAll,
                        erros: errAll                    
                    });
                })
                .catch((err) => {
                    errAll = err;
                    return res.status(206).json({
                        mensagem: `Registros criados ${contador} de um total de ${total}`,
                        contagem: `${contador}/${total}`,
                        retorno: dataAll,
                        erros: errAll                    
                    });
                }); 
            })
            .catch(returnError.bind(null, req, res, 'Campanha não encontrada', 204));     
    },

    registraImunizacao:(req, res) => {

        const imunizacao = req.body;
        let query = montaParams(req.url, "query");

        // Verifica se elegivel já foi imunizado com esta vacina e lote, nesta campanha
        modelElegivel
            .findOne(query, null, options)
            .exec()
            .then((elegivel) => {
                modelCampanha.verificarQtdeProdutoELote(elegivel, imunizacao, res)
                    .then(data => {
                        elegivel.atualizarImunizacao(imunizacao, res.usuario._id, false)
                            .then(eleg => {
                                data.atualizarCampanha()
                                .then(camp => { 
                                    return res.status(200).json({mensagem:'Imunizacao registrada com sucesso'})
                                })
                                .catch(returnError.bind(null, req, res, 'Erro ao atualizar', 405))
                            })
                            .catch(returnError.bind(null, req, res, 'Erro ao atualizar imunizacao', 405));                   
                    })
                    .catch(resp => {
                        console.log(resp)
                    })
            })
            .catch(returnError.bind(null, req, res, 'Elegivel não encontrado', 204));     

    },

    sincronizaImunizacoes:(req, res) => {

        const imunizacoes = req.body;
        let query = montaParams(req.url, "query");
        let promises = [];

        for (let imunizacao of imunizacoes) {
            promises.push(atualizaImunizacaoElegiveis(imunizacao));
        }

        Promise.all(promises)
        .then((data) => {

            let camps = [];
            let achou = false;
            for (let c = 0; c < campanhas.length; c++) {
                let camp = campanhas[c];
                if (camps) {
                    for (let i = 0; i < camps.length; i++) {
                        if (camp._id.toString() === camps[i]._id.toString()) {
                            achou = true;
                            break;
                        } 
                    }
                }
                if (!achou) {
                    camps.push(camp);
                }
            }
            campanhas = camps;

            for(let c = 0; c < campanhas.length; c++) {
                for (let e = 0; e < dataEleg.length; e++) {
                    if (dataEleg[e].campanha.toString() === campanhas[c]._id.toString()) {
                        for(let i = 0; i < dataImu.length; i++) {
                            let imunizacao = dataImu[i]
                            if(dataEleg[e].imunizacoes[i]._id.toString() === imunizacao._id.toString()) {
                                let produto = dataEleg[e].imunizacoes[i].produto;
                                let lote = dataEleg[e].imunizacoes[i].lote;
                                atualizarCampanha(campanhas[c], imunizacao, dataEleg[e], produto, lote)
                                break;
                            }
                        }
                    }
                }

                campanhas[c].save()
                    .then(callbackSinc.bind(null, c))
                    .catch(returnErrorSinc.bind(null, c))
            }

            return res.json({
                mensagem: 'Erro ao executar sincronização',
                contagem: {
                    'Dados atualizados': {
                        'Campanhas': dataCamp.length,
                        'Elegiveis': dataEleg.length,
                        'Imunizacoes': dataImu.length
                    },
                    'Erros': {
                        'Campanhas': errosCamp,
                        'Elegiveis': errosEleg,
                        'Imunizacoes': errosImu                    
                    }
                }
            })

        })
        .catch(err => {
            return res.json({
                mensagem: 'Erro ao executar sincronização',
                error: err.message,
                contagem: {
                    'Dados atualizados': {
                        'Campanhas': dataCamp.length,
                        'Elegiveis': dataEleg.length,
                        'Imunizacoes': dataImu.length
                    },
                    'Erros': {
                        'Campanhas': errosCamp,
                        'Elegiveis': errosEleg,
                        'Imunizacoes': errosImu                    
                    }
                }
            })
        })
    },

    dadosCampanha:(req, res, fields, sort, deepArray) => {

        let query = montaParams(req.url, "query");

        if(!query.campanha && res.usuario.perfil === 'ENFE') {
            return res.status(200).json({
                mensagem: 'Favor informar os parâmetros de seleção',
                error: 'OPERAÇÃO CANCELADA!'
            });
        }

        let cpf = ''
        if(query.cpf) {
            cpf = query.cpf
        }

        modelUsuario.find({_id: res.usuario._id}, {'campanhas': 1})
                    .exec()
                    .then(data => {
                        let campanhas = [];
                        data.forEach(usuario => {
                            usuario.campanhas.forEach(campanha => {
                                campanhas.push(mongoose.Types.ObjectId(campanha.campanha));
                            });
                        });

                        modelCampanha.find({'_id': { $in: campanhas}}, {'valor_unitario_servico': 0})
                            .exec()
                            .then( camp => {
                                if (cpf) {
                                    modelElegivel
                                        .find({campanha: { $in: campanhas}, cpf: cpf}, {valor_unitario_servico: 0})
                                        .exec()
                                        .then(callbackMultiData.bind(null, req, res, 'Campanhas encontradas', camp))
                                        .catch( returnError.bind(null, req, res, 'Elegivel não encontrado', 204));  
                                } else {
                                    modelElegivel
                                        .find({campanha: { $in: campanhas}})
                                        .exec()
                                        .then(callbackMultiData.bind(null, req, res, 'Campanhas encontradas', camp))
                                        .catch( returnError.bind(null, req, res, 'Elegivel não encontrado', 204));  
                                }
                            })
                            .catch( returnError.bind(null, req, res, 'Campanha não encontrada', 204) ); 
                    })
                    .catch(returnError.bind(null, req, res, 'Usuário atual não está vinculado a nenhuma campanha no momento', 203))
    },

    buscaElegiveisPorCPF: (req, res) => {
        let query = montaParams(req.url, "query");

        modelElegivel
            .find(query).sort({'cpfresp': 1, 'dependente': 1})
            .exec()
            .then(data => {
                if (data.length > 0) {
                    return res.json({
                        contagem: data.length,
                        retorno: data
                    })
                } else {
                    return res.status(204).json({
                        mensagem: 'CPF não encontrado',
                        error: err
                    })
                }
            })
            .catch(err => {
                return res.status(204).json({
                    mensagem: err.message,
                    error: err
                })
            });  
    },

    elegiveisCampanha: (req, res, sort, fields, deepArray) => {
        let query = montaParams(req.url, "query");

            modelElegivel
                .find(query, fields, sort)
                .deepPopulate(deepArray)
                .exec()
                .then(data => {
                        return res.json({
                            contagem: data.length,
                            retorno: data
                        });
                })
                .catch(err => {
                    return res.json({
                        mensagem: err.message,
                        error: err
                    });
                });

    },

    retrieve: (req, res, sort, fields, deepArray) => {
        let query = montaParams(req.url, "query");

        let cpf = '',
            campanha = '';

        if(query.cpf) {
            cpf = query.cpf
        }

        if(query.campanha) {
            campanha = query.campanha
        } else {
            return res.status(200).json({
                mensagem: 'Parâmetro Campanha não informado',
                error: 'OPERAÇÃO CANCELADA!'
            });
        }

        if (deepArray === undefined || deepArray.length === 0) {
            modelElegivel
                // .find({'cpf': { $gt: cpf}}, fields, sort)
                .find({'campanha': campanha, 'cpf': { $gt: cpf}})
                .limit(1000)
                .sort({'cpf': 1})
                .exec()
                .then( data => {
                    return res.json({
                        contagem: data.length,
                        retorno: data
                    });
                })
                .catch( err => {
                    return res.json({
                        mensagem: err.message,
                        error: err
                    });
                });

        } else {
            modelElegivel
                .find({'campanha': campanha, 'cpf': { $gt: cpf}})
                .limit(1000)
                .sort({'cpf': 1})
                .deepPopulate(deepArray)
                .exec()
                .then( data => {
                    return res.json({
                        contagem: data.length,
                        retorno: data
                    });
                })
                .catch( err => {
                    return res.json({
                        mensagem: err.message,
                        error: err
                    });
                });
        }
    },


    buscaAplicacoesPorCNPJ: (req, res) => {
        let query = montaParams(req.url, "query");

        function novoCnpjTotal() {
            return {
                cnpj: '',
                total_aplicacoes: ''
            }
        }

    },
    
};

module.exports = crudElegiveis;