'use strict';

// @arquivo: elegiveis-model.js
// @autor  : Clayton C. Carvalho
// @data   : 03.11.2017
// @projeto: ImunoApp

const mongoose = require('mongoose');
const refPromises = require('mongoose-ref-promises');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const deepPopulate = require('mongoose-deep-populate')(mongoose);

module.exports = function(_schema, colecao) {
    const elegivelSchema = _schema;

    elegivelSchema.retainKeyOrder = true;

    elegivelSchema.index({
        'campanha': 1,
        'cpf': 1,
        'nome': 1,
        'dependente': 1,
        'cpfresp': 1,
        'imunizacoes.produto': 1,
        'imunizacoes.lote': 1
    }, {
        unique: true
    });

    elegivelSchema.plugin(deepPopulate, {
        whitelist: ['campanha', 'imunizacoes.usuario'],
        populate: {
            "campanha": {
                select: " "
            },
            'imunizacoes.usuario': {
                select: "usuario nome"
            }
        }
    });

    elegivelSchema.plugin(refPromises);
    elegivelSchema.plugin(beautifyUnique);

    function trim(str) {
        if (str) {
            return str.replace(/^\s+|\s+$/g,"");
        } else {
            return ''
        }
    }

    elegivelSchema.methods.atualizarImunizacao = function(imunizacao, idUsuario, sincronizacao) {

        let me = this;
        
        return new Promise(function(executeNext, executeError) {

            me.cnpj = trim(me.cnpj)

            for(let i = 0; i < me.imunizacoes.length; i++) {

                if(me.imunizacoes[i]._id.toString() === imunizacao._id) {

                    if (!sincronizacao) {
                        for(let imu of me.imunizacoes) {
                            if(imu.desc_produto === me.imunizacoes[i].desc_produto && imu.aplicacao === true) {
                                return executeError(`Elegível já recebeu esta imunização em ${imu.data.toLocaleDateString('pt-BR')}`);
                            }
                        }
                    }

                    if(me.imunizacoes[i].aplicacao === true) {
                        if(sincronizacao) {
                            if (me.imunizacoes[i].duplicidade === undefined) {
                                me.imunizacoes[i].duplicidade = 0;
                            }
                            me.imunizacoes[i].duplicidade = me.imunizacoes[i].duplicidade + 1;
                        } else {
                            return executeError(`Elegível já recebeu esta imunização em ${me.imunizacoes[i].data.toLocaleDateString('pt-BR')}`);
                        }
                    } else {
                        me.imunizacoes[i].aplicacao = true;
                        me.imunizacoes[i].duplicidade = 0;
                        me.imunizacoes[i].data = new Date(Date.now()).toLocaleString();
                    }
                    me.imunizacoes[i].usuario = idUsuario;
                    me.markModified('imunizacoes');
                    break;
                }
            }
            if(me.isModified('imunizacoes')) {
                return me.save((err, data) => {
                    if(err) {
                        // console.log(err)
                        return executeError(err);
                    }
                    return executeNext(data);
                });
            } else {
                return executeError('Imunização não prevista para este elegível');
            }
        });
    }

    return mongoose.model(colecao, elegivelSchema);

};