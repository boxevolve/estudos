'use strict';

// @arquivo: fields.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp


const validacao = require('../../validations/validations');
const mensagens = require('../../validations/messages');
const mongoose = require('mongoose');

const data_dictionary = {


    // Campos String

    cmpTipoCarga: () => {
        return {
            type: String,
            required: true,
            enum: ['Campanha', 'Elegiveis', 'Roteiros']
        };
    },

    cmpStatusGeracaoCampanha: () => {
        return {
            type: String,
            required: true,
            enum: ['Pendente', 'Executando', 'Finalizado']
        };
    },

    cmpTipoEmpresa: () => {
        return {
            type: String,
            required: true,
            enum: ['Clientes', 'Distribuidores', 'Fornecedores']
        };
    },

    cmpStatusFile: () => {
        return {
            type: String,
            required: true,
            enum: ['Carregado', 'Covertido', 'Finalizado', 'Em processamento', 'Processado com erros', 'Processado com sucesso']
        };
    },

    cmpStringObrigatorio: () => {
        return {
            type: String,
            uppercase: true,
            required: mensagens.campo_obrigatorio
        };
    },

    cmpStringObrigatorioLower: () => {
        return {
            type: String,
            required: mensagens.campo_obrigatorio
        };
    },

    cmpStringObrigatorio1: () => {
        return {
            type: String,
            uppercase: true,
            maxlength: 1,
            required: mensagens.campo_obrigatorio
        };
    },

    cmpStringObrigatorio2: () => {
        return {
            type: String,
            uppercase: true,
            maxlength: 2,
            required: mensagens.campo_obrigatorio
        };
    },
    cmpStringObrigatorio4: () => {
        return {
            type: String,
            uppercase: true,
            maxlength: 4,
            required: mensagens.campo_obrigatorio
        };
    },

    cmpStringObrigatorio5: () => {
        return {
            type: String,
            uppercase: true,
            maxlength: 5,
            required: mensagens.campo_obrigatorio
        };
    },

    cmpStringObrigatorio10: () => {
        return {
            type: String,
            uppercase: true,
            maxlength: 10,
            required: mensagens.campo_obrigatorio
        };
    },

    cmpStringObrigatorio12: () => {
        return {
            type: String,
            uppercase: true,
            maxlength: 12,
            required: mensagens.campo_obrigatorio
        };
    },

    cmpStringObrigatorio20: () => {
        return {
            type: String,
            uppercase: true,
            maxlength: 20,
            required: mensagens.campo_obrigatorio
        };
    },

    cmpString: () => {
        return {
            type: String
        };
    },

    cmpStringUP: () => {
        return {
            type: String,
            uppercase: true
        };
    },

    cmpStringLow: () => {
        return {
            type: String,
            lowercase: true
        };
    },

    cmpString10: () => {
        return {
            type: String,
            maxlength: 10
        };
    },

    cmpString20: () => {
        return {
            type: String,
            maxlength: 20
        };
    },

    cmpString40: () => {
        return {
            type: String,
            maxlength: 40
        };
    },

    cmpString60: () => {
        return {
            type: String,
            maxlength: 60
        };
    },

    cmpSenha: () => {
        return {
            type: String,
            hideJSON: true
        };
    },


    cmpPerfil: () => {
        return {
            type: String,
            uppercase: true,
            maxlength: 5,
            // hideJSON: true,
            required: mensagens.campo_obrigatorio
        };
    },

    cmpNumber: () => {
        return {
            type: Number
        };
    },

    cmpNumberObrigatorio: () => {
        return {
            type: Number,
            required: mensagens.campo_obrigatorio
        };
    },

    cmpInt3: () => {
        return {
            type: Number,
            max: 999
        };
    },

    cmpInt5: () => {
        return {
            type: Number,
            max: 99999
        };
    },

    cmpFone: () => {
        return {
            type: String,
            maxlength: 19,
            validate: [validacao.validarFone, mensagens.campo_fone_invalido]
        };
    },

    cmpUF: () => {
        return {
            type: String,
            maxlength: 2,
            uppercase: true
        };
    },

    cmpBoolean: () => {
        return {
            type: Boolean,
            default: false
        };
    },

    // Tipos especiais

    cmpEmail: () => {
        return {
            type: String,
            unique: 1,
            lowercase: true,
            required: mensagens.campo_obrigatorio,
            validate: [validacao.validarEmail, mensagens.email_invalido]
        };
    },

    cmpEmailCliente: () => {
        return {
            type: String,
            lowercase: true,
            validate: [validacao.validarEmail, mensagens.email_invalido]
        };
    },

    cmpCnae: () => {
        return {
            type: String,
            maxlength: 9,
            unique: 1,
            required: mensagens.campo_obrigatorio,
            set: validacao.setCnae,
            get: validacao.getCnae
        };
    },

    cmpCep: () => {
        return {
            type: String,
            required: mensagens.campo_obrigatorio,
            set: validacao.setCep,
            get: validacao.getCep,
            validate: [validacao.validarCEP, mensagens.cep_invalido]
        };
    },

    cmpStatusCampanha: () => {
        return {
            type: String,
            required: mensagens.campo_obrigatorio,
            enum: ["Nova", "Andamento", "Encerrada"]
            // validate: [validacao.validarStatusCampanha]
        };
    },

    cmpPerfilUsuario: () => {
        return {
            type: String,
            uppercase: true,
            maxlength: 5,
            required: mensagens.campo_obrigatorio,
            enum: ["ADMIN", "CLIE", "ENFE", "FORN"]
            // validate: [validacao.validarPerfilUsuario]
        };
    },

    cmpCPF: () => {

        return {
            type: String,
            required: mensagens.campo_obrigatorio,
            get: validacao.getCPF,
            set: validacao.setCPF
            // validate: [validacao.validarCPF, mensagens.cpf_invalido]
        };
        /*
        if(dependente) {
            return {
                type: String
            };
        } else {
            return {
                type: String,
                required: mensagens.campo_obrigatorio,
                validate: [validacao.validarCPF, mensagens.cpf_invalido]
            };
        }
        */
    },

    cmpCNPJ: (sede) => {
        if(sede) {
            return {
                type: String
            };
        } else {
            return {
                type: String,
                required: mensagens.campo_obrigatorio,
                validate: [validacao.validarCNPJ, mensagens.cnpj_invalido]
            };
        }
    },

    cmpCNPJF: () => {
        return {
            type: String,
            validate: [validacao.validarCNPJ, mensagens.cnpj_invalido]
        };
    },

    cmpRefId: (colecao, notRequired) => {
        let req = true

    if (notRequired) req = false
        return {
            ref: colecao,
            type: mongoose.Schema.Types.ObjectId || String,
            required: req
        };
        /*
        if (notRequired) {
            return {
                type: String
            };
        } else {
            return {
                ref: colecao,
                type: mongoose.Schema.Types.ObjectId || String,
                required: false
            };
        }
        */
    },
 
    cmpData: () => {
        return {
            set: validacao.formatarData,
            type: Date,
            required: false
            //validate: [validacao.validarData, mensagens.data_invalida]
        };
    },
    
    cmpHoraS: () => {
        return {
            type: String,
            maxlength: 5,
            required: false
        };
    },
 
    cmpSubDoc: () => {
        return {
            type: Array
        };
    },

    cmpValor: () => {
        return {
            type: mongoose.Types.Decimal128
        };
    },

    cmpTipoParceiro: () => {
        return {
            type: String,
            uppercase: true,
            maxlength: 1,
            validate: [validacao.validarTipoParceiro, mensagens.tipo_parceiro_invalido]
        };
    }
};

module.exports = data_dictionary;