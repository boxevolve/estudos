'use strict';

// @arquivo: usuario-model.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const mongoose = require("mongoose"),
  mongooseHidden = require('mongoose-hidden')(),
  refPromises = require('mongoose-ref-promises'),
  beautifyUnique = require("mongoose-beautiful-unique-validation"),
  deepPopulate = require("mongoose-deep-populate")(mongoose);
//* ***************************************************************************
// Constantes para gerar token
const jwt = require("jwt-simple"),
  moment = require("moment"),
  parametros = require('../../../ambiente/parametros'),
  segredo = parametros.segredo;
//* ***************************************************************************
// Constantes para criptografia
const bcrypt = require("bcrypt");

module.exports = function (_schema, colecao) {
  const usuarioSchema = _schema;

  let token = "",
    expires = ""; // ,
  // novasenha = "";

  usuarioSchema.retainKeyOrder = true;

  usuarioSchema.index({
    nome: 1,
  }, {
    unique: false,
  });

  usuarioSchema.index({
    email: 1,
  }, {
    unique: true,
  });

  usuarioSchema.index({
    usuario: 1,
  }, {
    unique: true,
  });

  usuarioSchema.index({
    _id: 1,
    'campanhas.campanha': 1,
  }, {
    unique: true,
  });

  usuarioSchema.plugin(deepPopulate, {
    whitelist: ['campanhas.campanha'],
    populate: {
      "campanhas.campanha": {
        // select: "_id campanha cliente distribuidor"
        select: " ",
      },
    },
  });

  usuarioSchema.virtual('isLocked').get(function () {
    // check for a future lockUntil timestamp
    return !!(this.lockUntil && this.lockUntil > Date.now());
  });

  usuarioSchema.virtual("token").get(function () {
    return token;
  });

  usuarioSchema.virtual("expires").get(function () {
    return new Date(expires).toLocaleDateString('pt-BR');
  });

  // usuarioSchema.virtual("novasenha").get(function() {
  //    return novasenha;
  // });
  // usuarioSchema.set('toJSON', { getters: true, virtuals: true });
  usuarioSchema.plugin(refPromises);
  usuarioSchema.plugin(beautifyUnique);

  // By default in Mongoose virtuals will not be included. Turn on before enabling plugin.
  usuarioSchema.set('toJSON', { virtuals: false });
  usuarioSchema.set('toObject', { virtuals: true });

  // usuarioSchema.plugin(mongooseHidden, { virtuals: { isLocked: 'hide' }});
  // usuarioSchema.plugin(mongooseHidden, { hidden: { primeiro_acesso: true, email_enviado: true } } );
  usuarioSchema.plugin(mongooseHidden, {
    defaultHidden: { password: true, __v: true },
  });

  usuarioSchema.pre("save", function (next) {
    const usuario = this;

    // Validar perfil
    if (this.isModified("perfil")) {
      if (this.perfil !== 'ADMIN' && this.perfil !== 'CLIE' &&
                this.perfil !== 'FORN' && this.perfil !== 'ENFE') {
        return next("Perfil inválido");
      }
    }

    // only hash the senha if it has been modified (or is new)
    if (!this.isModified("senha")) return next();

    // generate a salt
    bcrypt.genSalt(parametros.SALT_WORK_FACTOR, function (err, salt) {
      if (err) return next(err);

      // hash the senha using our new salt
      bcrypt.hash(usuario.senha, salt, function (err, hash) {
        if (err) return next(err);

        // set the hashed senha back on our usuario document
        if (usuario.senha.charAt(0) !== "$") {
          // novasenha = usuario.senha;
          usuario.senha = hash;
        }
        return next();
      });
    });
  });

  usuarioSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.senha, function (err, isMatch) {
      if (err) return cb(err);
      cb(null, isMatch);
    });
  };

  usuarioSchema.methods.incLoginAttempts = function (cb) {
    // if we have a previous lock that has expired, restart at 1
    if (this.lockUntil && this.lockUntil < Date.now()) {
      return this.update({
        $set: { loginAttempts: 1 },
        $unset: { lockUntil: 1 },
      },
      cb
      );
    }
    // otherwise we're incrementing
    const updates = { $inc: { loginAttempts: 1 } };
    // lock the account if we've reached max attempts and it's not locked already
    if (this.loginAttempts + 1 >= parametros.MAX_LOGIN_ATTEMPTS && !this.isLocked) {
      updates.$set = { lockUntil: Date.now() + parametros.LOCK_TIME };
    }
    return this.update(updates, cb);
  };

  usuarioSchema.statics.setReset = function (user) {
    return this.findOne({ email: user.email }).exec();
  };

  usuarioSchema.statics.getAuthenticated = function (email, senha) {
    let usr = this;

    return new Promise(function (executeNext, executeError) {
      // let query = usr.findOne({ email: email }).select('usuario');

      usr.findOne({ email: email }, function (err, usuario) {
        if (err) return executeError(err);

        // make sure the user exists
        if (!usuario) {
          return executeError('Usuário não encontrado');
        }
        // check if the account is currently locked
        if (usuario.isLocked) {
          // just increment login attempts if account is already locked
          usuario.incLoginAttempts(function (err) {
            if (err) return executeError(err);
            return executeError('Acesso do usuário bloqueado');
          });
        }

        // test for a matching password
        usuario.comparePassword(senha, function (err, isMatch) {
          if (err) return executeError(err);

          // check if the password was a match
          if (isMatch) {
            expires = moment().add(7, "days").valueOf();
            token = jwt.encode({
              iss: usuario.id,
              iac: usuario.perfil,
              exp: expires,
            },
            segredo
            );

            // if there's no lock or failed attempts, just return the user
            if (!usuario.loginAttempts && !usuario.lockUntil) {
              return executeNext(usuario);
            }

            // reset attempts and lock info
            const updates = {

              $set: { loginAttempts: 0 },
              $unset: { lockUntil: 1 },
            };

            return usuario.update(updates, function (err) {
              if (err) return executeError(err);
              usuario.loginAttempts = 0;
              return executeNext(usuario);
            });
          }

          // password is incorrect, so increment login attempts before responding
          usuario.incLoginAttempts(function (err) {
            if (err) return executeError(err);
            return executeError('Senha incorreta');
          });
        });
      });
    });
  };

  usuarioSchema.statics.alteraSenha = function (usuario, novasenha, forgot) {
    let usr = this;

    return new Promise(function (executeNext, executeError) {
      // const alterarSenhaUsuario = function(usuario) {
      // console.log("ALTERA SENHA: ", usuario.senha);
      // generate a salt
      bcrypt.genSalt(parametros.SALT_WORK_FACTOR, function (err, salt) {
        // console.log("ALTERA SENHA SALT: ", salt);
        if (err) return executeError(err);

        // hash the senha using our new salt
        bcrypt.hash(novasenha, salt, function (err, hash) {
          if (err) return executeError(err);

          this.novasenha = novasenha;
          // console.log("NOVA SENHA: ", hash);

          // set the hashed senha back on our usuario document
          let updates = "";

          if (usuario.email === null) {
            updates = {
              $set: { senha: hash, loginAttempts: 0 },
              $unset: { lockUntil: 1 },
            };
          } else {
            if (forgot === undefined) forgot = false;
            if (!forgot && !usuario.primeiro_acesso) forgot = true;
            if (forgot) {
                updates = {
                    $set: {
                        senha: hash,
                        loginAttempts: 0,
                        primeiro_acesso: forgot,
                        lockUntil: Date.now() + parametros.LOCK_TIME,
                    },
                };
            } else {
                updates = {
                    $set: {
                        senha: hash,
                        loginAttempts: 0,
                        primeiro_acesso: forgot,
                    },
                };
            }
          }

          return usuario.update(updates, function (err, data) {
            if (err) return executeError(err);
            usuario.loginAttempts = 0;
            return executeNext(usuario);
          });
        });
      });

      /*
            if (email) {
                usr.findOne({ 'email': email })
                    .then(alterarSenhaUsuario)
                    .catch(err => {
                        return executeError(err);
                    });
            }
            */
    });
  };

  return mongoose.model(colecao, usuarioSchema);
};
