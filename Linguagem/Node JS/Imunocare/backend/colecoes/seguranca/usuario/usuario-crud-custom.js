'use strict';

// @arquivo: elegivel-crud-custom.js
// @autor  : Clayton C. Carvalho
// @data   : 21.02.2018
// @projeto: ImunoApp

const mongoose = require('mongoose');
const c_e = "E",
      c_s = "S";

const usuarioSchema = require("./usuario-schema"),
  modelUsuario = require("./usuario-model")(usuarioSchema, "usuarios");   

const campanhaSchema = require("../../dados/campanhas/campanha-schema"),
  modelCampanha = require("../../dados/campanhas/campanha-model")(campanhaSchema, "campanhas");

const elegivelSchema = require("../../dados/elegiveis/elegivel-schema"),
  modelElegivel = require("../../dados/elegiveis/elegivel-model")(elegivelSchema, "elegiveis");

const fields = require('../../fields/fields'),
  querystring = require("querystring"),
  url = require("url"),
  enviarEmail = require('../../../utilitarios/email/email'),
  randomstring = require('randomstring');

const crudCampanhaCustom = require('../../dados/campanhas/campanha-crud-custom');

const montaParams = (_url, param) => {
  let url_parts = url.parse(_url);
  if (param === "query") return querystring.parse(url_parts.query);
};

function get_headerOrigem(req) {
  return req.headers["x-access-origem"];
}

function callback(req, res, data) {
  const url_path = url.parse(req.url);

  if (url_path.pathname === '/create') {
    /*
    enviarEmail(null, data, req, res);
    if (data.perfil === 'ENFE') {
        return res.status(200).json({
            mensagem: `Usuário ${data.nome} criado com sucesso. Senha foi enviada por email para o responsável pela campanha`
        });
    } else {
        return res.status(200).json({
            mensagem: `Usuário ${data.nome} criado com sucesso. Senha foi enviada por email`
        });
    }
    */
    return res.status(200).json({
        mensagem: `Usuário ${data.nome} criado com sucesso.`
    });
  } else {
    return res.status(200);
  }
}

function callbackTable(req, res, length, data) {
  return res.json({
    contagem: length,
    retorno: data
  });
}

function returnError(req, res, msg, err) {
  console.log(err);
  const url_path = url.parse(req.url);
  if (url_path.pathname === '/create') {
    return res.status(404).json({
      mensagem: 'Erro ao criar novo usuário',
      error: err
    });
  } else {
    return res.status(404).json('Usuário não faz parte de nenhuma campanha no momento');
  }
}

const crudUsuarios = {
  create: (req, res) => {
    let modelo = new modelUsuario(req.body);
    /*
    if(modelo.perfil !== 'ENFE') {
        modelo.senha = modelo.novasenha = randomstring.generate(8);
        modelo.primeiro_acesso = true;
    }
    */
    console.log('SENHA: ', modelo.senha);

    modelo.save()
      .then(callback.bind(null, req, res))
      .catch(returnError.bind(null, req, res));
  },

  addCampanha: (req, res, fields, deepArray) => {
    let modelo = req.body;
    let query = montaParams(req.url, "query");
    
    modelUsuario.findOne(query, fields)
                .deepPopulate(deepArray)
                .exec()
                .then(function(result, err) {
                  result.campanhas = modelo.campanhas;
                  let mod = { $set: result };
                  modelUsuario.update(query, mod)
                              .exec()
                              .then(function() {
                                return res.status(200).json({retorno: 'salvo'});
                              })
                              .catch(function() {
                                return res.status(400).json({retorno: 'erro'});
                              });
                })
                .catch(function() {
                  return res.status(400);
                });
  },

  getCampanhas:(req, res, fields, deepArray) => {
    //let query = montaParams(req.url, "query"),
    let options = {
      //runValidators: true,
      new: true
      //upsert: true
    };

    // Verifica se elegivel já foi imunizado com esta vacina e lote, nesta campanha
    modelUsuario.findOne({'_id': res.usuario._id}, fields, options).deepPopulate(deepArray)
      .exec()
      .then(data => {
          let campanhas = [];
          if (res.usuario.perfil === 'ADMIN') {
              modelCampanha.find().deepPopulate(['cliente', 'cliente.endereco', 'distribuidor', 'fornecedores.fornecedor', 'produtos.produto', 
                                                 'filiais.unidades.endereco', 'gestor', 'agrupamento', 'criado_por'])
              .then(camps => {
                  campanhas = camps;
                  return res.json({campanhas: campanhas});
              })
              .catch(err => {
                  return res.status(204).json(err)
              })

          } else {
            for (let i = 0; i < data.campanhas.length; i++) {
              campanhas.push(data.campanhas[i].campanha);
            }
            return res.json({campanhas: campanhas});
          }

        // callback.bind(null, req, res, 'Dados de Campanhas recuperados com sucesso')
      })
      .catch(returnError.bind(null, req, res, 'Usuário atual não faz parte de nenhuma campanha'));     
  },

  getCampanhasByUsuario: (req, res, sort, fields, deepArray, deepArrayCampanha) => {
    let query = montaParams(req.url, "query");
    let page = 0, size = 0;

    if(query.page) page = query.page
    if(query.size) {
      size = Number.parseInt(query.size)
    } else size = 1

    let options = {
      new: true
    };
    modelUsuario.findOne({'_id': res.usuario._id}, fields, options).deepPopulate(deepArray).exec()
      .then(data => {
        let campanhas = [];
        if (res.usuario.perfil === 'ADMIN' || res.usuario.perfil === 'GEST') {
          crudCampanhaCustom.retrieve(req, res, sort, {}, deepArrayCampanha);
        } else {
          let skip = page * size;
          let max = skip + size;
          let length = data.campanhas.length;
          max = max > length ? length : max;
          let amount = 0;
          
          for (let i = skip; i < max; i++) {
            modelCampanha.find({'_id' : data.campanhas[i].campanha}).deepPopulate(deepArrayCampanha)
              .then(campanha => {
                amount++;
                if (campanha && campanha.length) {
                  campanhas.push.apply(campanhas, campanha);
                }
                if (amount === (max - skip)) {
                  return callbackTable(req, res, length, campanhas);
                }
              }).catch(err => {
                return res.status(204).json(err)
              })
          }
        }
      })
      .catch(returnError.bind(null, req, res, 'Usuário atual não faz parte de nenhuma campanha'));
  },
  
  verificaAcesso:(req, res) => {
    let query = montaParams(req.url, "query");
    console.log(`Verifica acesso: ${res.usuario.perfil}`);
    console.log(query);
    if (res.usuario.perfil === 'ENFE') {
      return res.status(200).json({ acesso: false });
    } 
    return res.status(200).json({ acesso: true });
  }
};

module.exports = crudUsuarios;