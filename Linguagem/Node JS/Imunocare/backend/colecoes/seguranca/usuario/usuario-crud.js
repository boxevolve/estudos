"use strict";

// @arquivo: usuario-crud.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

const usuarioSchema = require("./usuario-schema"),
    modelUsuario = require("./usuario-model")(usuarioSchema, "usuarios"),
    crudDinamico = require("../../../utilitarios/crud/crud_dinamico")(modelUsuario, usuarioSchema);

module.exports = crudDinamico;
