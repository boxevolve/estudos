// @arquivo: usuario-route.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const express = require('express');
const router = express.Router();

//const deepArray =[ "pessoa", "idioma", "acesso.grupo", "acesso.grupo.perfis.perfil", "acesso.grupo.perfis.perfil.permissoes.permissao"];
const crudUsuario = require('./usuario-crud');
const crudUsuarioCustom = require('./usuario-crud-custom');
//router.route('/create').post(validaToken, (req, res, next) => {
router.route('/create').post((req, res, next) => {
    crudUsuarioCustom.create(req, res);
});

router.route('/addCampanha').post((req, res, next) => {
    const fields = {};
    let deepArray = ['campanhas.campanha'];
    crudUsuarioCustom.addCampanha(req, res, fields, deepArray);
});

router.route('/retrieve').get((req, res, next) => {
    const sort = { 'usuario': 1, 'nome': 1 },
    fields = { 'senha': 0 };
    const deepArray = ['campanhas.campanha'];
    crudUsuario.retrieve(req, res, sort, fields, deepArray);
});

router.route('/buscaDadosUsuario').get((req, res, next) => {
    const usuario = res.usuario;
    return res.status(200).json(usuario);
});

router.route('/findOne').get((req, res, next) => {
    //router.route('/findOne').get((req, res, next) => {
    //router.get('/findOne', function(req, res, next) {
    const fields = { 'senha': 0 };  
    let deepArray = [];          
    crudUsuario.findOne(req, res, fields, deepArray);
});

router.route('/getCampanhas').get((req, res, next) => {
    //router.route('/findOne').get((req, res, next) => {
    //router.get('/findOne', function(req, res, next) {
    const fields = { 'campanhas': 1 };  
    let deepArray = ['campanhas.campanha'];          
    crudUsuarioCustom.getCampanhas(req, res, fields, deepArray);
});

router.route('/getCampanhasByUsuario').get((req, res, next) => {
    const fields = { 'campanhas': 1 };  
    let deepArray = [
        'campanhas.campanha',
    ];
    let deepArrayCampanha = [
        'filiais.unidades.endereco',
        'cliente',
        'cliente.endereco',
        'distribuidor',
        'gestor',
        'fornecedores.fornecedor', 
        'fornecedores.fornecedor.endereco', 
        'agrupamento',
        'produtos.produto',
        'criado_por'
      ];        
    const sort = {};
    crudUsuarioCustom.getCampanhasByUsuario(req, res, sort, fields, deepArray, deepArrayCampanha);
});

router.route('/verificaAcesso').get((req, res, next) => {
    crudUsuarioCustom.verificaAcesso(req, res);
});


//router.route('/update').put(validaToken, (req, res, next) => {
router.route('/update').put((req, res, next) => {
    //router.put('/update', function(req, res, next) {
    crudUsuario.update(req, res);
});

//router.route('/delete').delete(validaToken, (req, res, next) => {
router.route('/delete').delete((req, res, next) => {
    crudUsuario.delete(req, res);
});

module.exports = router;