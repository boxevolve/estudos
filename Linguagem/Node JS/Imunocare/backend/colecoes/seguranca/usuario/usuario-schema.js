'use strict';

// @arquivo: usuario-schema.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

// Constantes do Mongoose
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fields = require('../../fields/fields'),

    // Campos do Schema

// Criando Schema
jsonUsuario = {
    usuario: fields.cmpStringObrigatorio12(),
    nome: fields.cmpStringObrigatorioLower(),
    email: fields.cmpEmail(),
    email_cliente: fields.cmpEmailCliente(),
    senha: fields.cmpSenha(),
    perfil: fields.cmpPerfilUsuario(),
    coren: fields.cmpString10(),
    tipo: fields.cmpTipoEmpresa(),
    empresa: fields.cmpString(), 
    campanhas: [
        {
            campanha: fields.cmpRefId('campanhas') || fields.cmpString()
        }
    ],
    primeiro_acesso: fields.cmpBoolean(),
    email_enviado: fields.cmpBoolean(),
    loginAttempts: fields.cmpNumber(),
    lockUntil: fields.cmpData()
};

module.exports = new Schema(jsonUsuario);