'use strict';

module.exports = function(app) {
      if (app.get('env') === 'development') {
          return 'localhost'
      }else{
          return 'icmongoproxy'
      }
}
