'use strict';

const env = {
    production: () => (true),
    mongoserver: () => ('icmongoproxy')
};

module.exports = env;