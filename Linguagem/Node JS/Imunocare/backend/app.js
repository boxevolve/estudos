// @arquivo: app.js
// @autor  : Clayton C. Carvalho
// @data   : 29.10.2017
// @projeto: ImunoApp

'use strict';

const express = require('express');
let app = express();

//const favicon = require('serve-favicon');
const logger = require('morgan');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('./config/config');
const path = require('path');
// const multer = require('multer');

// const path = require('path');

// const expressValidator = require('express-validator');
// const session = require('express-session');
// const passport = require('passport');
// const mensagens = require('./db/mensagens');

const fs = require('fs');
const https = require('https');
//const http = require('http');

//const const_a = 'A';


app.use(cors());

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit:50000}));
// setup the logger
app.use(logger('common', {
    stream: fs.createWriteStream(path.join(__dirname, 'acesso.log'), {flags: 'a'})
  }));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cookieParser());

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'POST')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, X-Requested-With,content-type')
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.setHeader('Accept-Ranges', 'bytes')
  next();
});



require('./config/routes')(app); // Include para todas as nossas rotas...
require('./config/express')(app, config); // Configuração do express...


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    // @ts-ignore
err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

require('./db/config')(app);


// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(500).json(err);
});

const options = {
    cert: fs.readFileSync(path.join(__dirname, '/config/keys/imunocare.care.pem')),
    key: fs.readFileSync(path.join(__dirname, './config/keys/imunocare.care.key'))
  };

 https.createServer(options, app).listen(config.port, () => {
/*
  http.createServer(app).listen(config.port, () => {
 */
    console.log(`ImunoCare server listening on https://localhost:${config.port}`);
  });


module.exports = app;