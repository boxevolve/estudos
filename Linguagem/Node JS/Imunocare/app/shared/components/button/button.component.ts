import { Input, Output, EventEmitter } from '@angular/core';
import { Component } from '@angular/core';
import { BaseComponent } from '../../utils/base.component';
import { Util } from '../../utils/util';

@Component({
  selector: 'ic-button',
  templateUrl: './button.component.html'
})
export class ButtonComponent extends BaseComponent {

  @Input() id: string;
  @Input() text: string;
  @Input() clazz: string;
  @Input() style: string;
  @Input() clazzIcon: string;
  @Input() disabled: boolean;
  @Input() type = 'button';
  @Input() title = '';
  @Input() multiple = 'false';
  @Input() accept: string;

  @Output() eventClick = new EventEmitter();

  constructor() {
    super();
  }

  public emitOnClick(event: any): void {
    this.eventClick.emit(event);
  }

  public showIcon(): boolean {
    return !Util.isEmpty(this.clazzIcon);
  }

  public showText(): boolean {
    return !Util.isEmpty(this.text);
  }

}
