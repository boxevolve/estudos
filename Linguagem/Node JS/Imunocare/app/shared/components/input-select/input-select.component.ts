import { Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../utils/base.component';
import { Util } from '../../utils/util';

@Component({
  selector: 'ic-input-select',
  templateUrl: './input-select.component.html'
})
export class InputSelectComponent extends BaseComponent implements OnInit, OnChanges {

  @Input() id: string;
  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() selected: any = null;
  @Input() compareWith: any = this.compareWithDefault;
  @Input() options: any[] = [];
  @Input() value: string;
  @Input() text: any;
  @Input() concat: string;
  @Input() showNull = true;
  @Input() nullText = 'Selecione';
  @Input() required = false;
  @Input() placeholder = '';
  @Input() clazz: any[] = [];
  @Input() config: any = {};
  @Input() okText = 'Selecionar';
  @Input() cancelText = 'Cancelar';

  @Output() eventChange = new EventEmitter();

  nameDisabled = 'disabled';
  nameSelected = 'selected';
  namePlaceholder = 'placeholder';

  private clazzFullWidth = 'full-width-select';
  showError = false;

  constructor() {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameDisabled, (currentValue?: any) => {
      this.nameDisabled ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    });
    this.onChangeInput(changes, this.nameSelected, (currentValue?: any) => {
      this.setValueForm(this.form, this.name, currentValue);
    });
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.selected || null, disabled: this.disabled}, Validators.required)
      : new FormControl({value: this.selected || null, disabled: this.disabled});
  }

  public getValue(value: any): any {
    if (Util.isEmpty(value)) {
      return '';
    }
    return !this.value ? value : value[this.value];
  }

  public getText(value: any): any {
    let text = '';
    let values = [];
    if (this.typeArray(this.text)) {
      this.text.forEach(item => {
        values.push(value[item]);
      });
      text = this.concatStr(values, this.concat);
    }
    return text ? text : (!this.text ? value : value[this.text]);
  }

  public compareWithDefault(objectA: any, objectB: any): boolean {
    return (objectA && objectB) ? objectA === objectB : false;
  }

  public emitOnChange(event: any): void {
    this.clazz = this.removeAll(this.clazz, null, this.clazzFullWidth);
    if (!Util.isEmpty(event)) {
      this.clazz.push(this.clazzFullWidth);
    }
    this.eventChange.emit(event);
  }

  public showLabel(): boolean {
    return Util.isEmpty(this.getValueForm(this.form, this.name));
  }

  public validateRequired(event?: any): boolean {
    this.showError = this.updateStyle(this.name, () => {
      return this.validateRequiredField(this.required, this.form, this.name, this.getFormControl());
    });
    return this.showError;
  }

  public getIdComponent(item: string): string {
    return this.getFieldId(this.name, item);
  }

}
