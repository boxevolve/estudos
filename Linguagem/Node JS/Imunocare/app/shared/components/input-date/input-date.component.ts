import { Input, OnChanges, SimpleChanges, ChangeDetectorRef, AfterContentChecked, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../utils/base.component';
import $ from "jquery";
import { InputType } from '../../const/input-type.const';

@Component({
  selector: 'ic-input-date',
  templateUrl: './input-date.component.html'
})
export class InputDateComponent extends BaseComponent implements OnInit, OnChanges, AfterContentChecked, AfterViewInit {

  @Input() id: string;
  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() value: any = null;
  @Input() required = false;
  @Input() placeholder = '';
  @Input() maxlength = 14;
  @Input() minlength = 0;
  @Input() clazz: string;
  @Input() style: string;
  @Input() minDate: Date;
  @Input() minDateControl: string;
  @Input() maxDate: Date;
  @Input() maxDateControl: string;
  @Input() displayFormat = 'MM/DD/YYYY';
  @Input() doneText = 'Selecionar';
  @Input() cancelText = 'Cancelar';

  nameDisabled = 'disabled';
  nameValue = 'value';

  public dateOptions: any = {
    buttons: [{
       text: 'Limpar',
       handler: () => {
          this.setValueForm(this.form, this.name, null);
          this.validateRequired();
       }
    }]
  };

  constructor(
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngAfterViewInit() {
    this.initStyle();
  }

  ngAfterContentChecked() {
    if (this.minDateControl) {
      this.form.get(this.minDateControl).valueChanges.subscribe(value => {
        this.minDate = value;
      });
    }
    if (this.maxDateControl) {
      this.form.get(this.maxDateControl).valueChanges.subscribe(value => {
        this.maxDate = value;
      });
    }
    this.cdr.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    const keyChanges = Object.keys(changes);
    if (this.contains(keyChanges, this.nameDisabled)) {
      changes[this.nameDisabled].currentValue ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    }
    if (this.contains(keyChanges, this.nameValue)) {
      this.setValueForm(this.form, this.name, changes[this.nameValue].currentValue);
    }
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
    if (this.required) {
      this.minlength = 1;
    }
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.value || null, disabled: this.disabled}, Validators.required)
      : new FormControl({value: this.value || null, disabled: this.disabled});
  }

  public initStyle(): void {
    this.updateStyle(this.name, null, true);
  }

  public validateRequired(event?: any): boolean {
    return this.updateStyle(this.name, () => {
      return this.validateRequiredField(this.required, this.form, this.name, this.getFormControl());
    });
  }

  public getIdComponent(item: string): string {
    return this.getFieldId(this.name, item);
  }

}
