import { Input, SimpleChanges, OnChanges, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../utils/base.component';

@Component({
  selector: 'ic-input-email',
  templateUrl: './input-email.component.html'
})
export class InputEmailComponent extends BaseComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() value: any = null;
  @Input() required = false;
  @Input() placeholder = '';
  @Input() maxlength = 60;
  @Input() minlength = 0;
  @Input() clazz: string;

  nameDisabled = 'disabled';
  nameValue = 'value';

  messageError = '';

  constructor() {
    super();
  }

  ngAfterViewInit() {
    this.initStyle();
  }

  ngOnChanges(changes: SimpleChanges) {
    const keyChanges = Object.keys(changes);
    if (this.contains(keyChanges, this.nameDisabled)) {
      this.nameDisabled ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    }
    if (this.contains(keyChanges, this.nameValue)) {
      this.setValueForm(this.form, this.name, changes[this.nameValue].currentValue);
    }
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
    if (this.required) {
      this.minlength = 1;
    }
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.value || null, disabled: this.disabled}, [Validators.required, Validators.email])
      : new FormControl({value: this.value || null, disabled: this.disabled}, Validators.email);
  }

  public initStyle(): void {
    this.updateStyle(this.name, null, true);
  }

  public validateRequired(event?: any): boolean {
    return this.updateStyle(this.name, () => {
      let isInvalid = false;
      let emailInvalid = this.validateEmail();
      if (emailInvalid || this.validateRequiredField(this.required, this.form, this.name, this.getFormControl())) {
        this.messageError = emailInvalid ? this.msgValidEmail() : this.msgRequiredField();
        isInvalid = true;
      }
      this.setText(this.getIdComponent('message'), this.messageError);
      return isInvalid;
    });
  }

  public validateEmail(): boolean {
    const control = this.getControl(this.form, this.name);
    if (control) {
      return control.hasError('email') && !this.validateRequiredField(this.required, this.form, this.name, this.getFormControl());
    }
    return this.getFormControl().hasError('email') && !this.getFormControl().hasError('required');
  }

  public getIdComponent(item: string): string {
    return this.getFieldId(this.name, item);
  }

}
