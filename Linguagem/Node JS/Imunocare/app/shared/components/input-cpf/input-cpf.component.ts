import { Input, SimpleChanges, OnChanges, AfterContentChecked, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../utils/base.component';

@Component({
  selector: 'ic-input-cpf',
  templateUrl: './input-cpf.component.html'
})
export class InputCpfComponent extends BaseComponent implements OnInit, OnChanges, AfterViewInit, AfterContentChecked {

  @Input() id: string;
  @Input() form: FormGroup;
  @Input() name: string;
  @Input() disabled = false;
  @Input() value: any = null;
  @Input() required = false;
  @Input() placeholder = '';
  @Input() maxlength = 14;
  @Input() minlength = 0;
  @Input() clazz: string;

  nameDisabled = 'disabled';
  nameValue = 'value';
  nameRequired = 'required';

  messageError = '';

  constructor(
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngAfterViewInit() {
    this.initStyle();
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeInput(changes, this.nameDisabled, (currentValue?: any) => {
      this.nameDisabled ? this.disableField(this.form, this.name) : this.enableField(this.form, this.name);
    });
    this.onChangeInput(changes, this.nameValue, (currentValue?: any) => {
      this.setValueForm(this.form, this.name, currentValue);
    });
    this.onChangeInput(changes, this.nameRequired, (currentValue?: any) => {
      this.setRequired(currentValue);
    });
  }

  ngOnInit(): void {
    this.form.addControl(
      this.name,
      this.getFormControl()
    );
    if (this.required) {
      this.minlength = 11;
    }
  }

  public getFormControl(): FormControl {
    return this.required
      ? new FormControl({value: this.value || null, disabled: this.disabled}, Validators.required)
      : new FormControl({value: this.value || null, disabled: this.disabled});
  }

  public setRequired(required: boolean): void {
    const validation = required ? [Validators.required, Validators.minLength(this.minlength)] : null;
    this.setValidationForm(this.form, this.name, validation);
  }

  public initStyle(): void {
    this.updateStyle(this.name, null, true);
  }

  public validateRequired(event?: any): boolean {
    this.cpfMask(event);
    return this.updateStyle(this.name, () => {
      let isInvalid = false;
      let cpfInvalid = this.validateFieldCpf();
      if (cpfInvalid || this.validateRequiredField(this.required, this.form, this.name, this.getFormControl())) {
        this.messageError = cpfInvalid ? this.msgValidCpf() : this.msgRequiredField();
        isInvalid = true;
      }
      this.setText(this.getIdComponent('message'), this.messageError);
      return isInvalid;
    });
  }

  public validateFieldCpf(): boolean {
    const validate = this.validateCpf(this.getValueForm(this.form, this.name));
    this.minlength = validate ? 11 : 12;
    return !validate && !this.validateRequiredField(this.required, this.form, this.name, this.getFormControl());
  }

  public getIdComponent(item: string): string {
    return this.getFieldId(this.name, item);
  }

  cpfMask(value) {
    if (value) {
      value = value.target.value;
      let v = value.replace(/\D/g, "");
      let cpf = {
        n1: v.substring(0, 3),
        n2: v.substring(3, 6),
        n3: v.substring(6, 9),
        n4: v.substring(9, 11)
      }

      let res = cpf.n1 
      if (cpf.n2) { res += '.' + cpf.n2 }
      if (cpf.n3) { res += '.' + cpf.n3 }
      if (cpf.n4) { res += '-' + cpf.n4 }

      this.setValueForm(this.form, this.name, res);
    }
  }

}
