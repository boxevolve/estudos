import { Util } from '../utils/util';

export class Sexo {

  static MASCULINO = new Sexo('MASCULINO', 'Masculino', 'M');
  static FEMININO = new Sexo('FEMININO', 'Feminino', 'F');
  static OUTRO = new Sexo('OUTRO', 'Outro', 'X');

  private constructor(
    private _key?: string,
    private _text?: string,
    private _value?: any,
  ) {}

  public static getByValue(value: string): Sexo {
    value = Util.removeAccentuation(value);
    let returnValue = null;
    switch (value) {
      case Sexo.MASCULINO.value:
        returnValue = Sexo.MASCULINO;
        break;
      case Sexo.FEMININO.value:
        returnValue = Sexo.FEMININO;
        break;
      case Sexo.OUTRO.value:
        returnValue = Sexo.OUTRO;
        break;
    }
    return returnValue;
  }

  public static getByText(value: string): Sexo {
    let returnValue = null;
    switch (value) {
      case Sexo.MASCULINO.text:
        returnValue = Sexo.MASCULINO;
        break;
      case Sexo.FEMININO.text:
        returnValue = Sexo.FEMININO;
        break;
      case Sexo.OUTRO.text:
        returnValue = Sexo.OUTRO;
        break;
    }
    return returnValue;
  }

  public static getTextByValue(value: string): string {
    const sexo = this.getByValue(value);
    return sexo ? sexo.text : '';
  }

  public static getValueByText(value: string): any {
    const sexo = this.getByText(value);
    return sexo ? sexo.value : null;
  }

  public static list(): Sexo[] {
    return [Sexo.MASCULINO, Sexo.FEMININO, Sexo.OUTRO];
  }

  get text(): string {
    return this._text;
  }

  get value(): string {
    return this._value;
  }

  toString(): string {
    return this._key;
  }

}
