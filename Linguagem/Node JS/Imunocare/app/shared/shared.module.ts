import { InputTextComponent } from "./components/input-text/input-text.component";
import { InputSelectComponent } from "./components/input-select/input-select.component";
import { CommonModule, DatePipe } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ModuleWithProviders, NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";
import { InputCpfComponent } from "./components/input-cpf/input-cpf.component";
import { InputRadioComponent } from "./components/input-radio/input-radio.component";
import { InputDateComponent } from "./components/input-date/input-date.component";
import { InputEmailComponent } from "./components/input-email/input-email.component";
import { ButtonComponent } from "./components/button/button.component";

@NgModule({
  declarations: [
    InputTextComponent,
    InputSelectComponent,
    InputCpfComponent,
    InputRadioComponent,
    InputDateComponent,
    InputEmailComponent,
    ButtonComponent
  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule
  ],

  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    InputTextComponent,
    InputSelectComponent,
    InputCpfComponent,
    InputRadioComponent,
    InputDateComponent,
    InputEmailComponent,
    ButtonComponent
  ]

})

export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        DatePipe
      ]
    }
  }
}
