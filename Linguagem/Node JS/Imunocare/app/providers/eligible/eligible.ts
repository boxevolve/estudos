import { Injectable } from '@angular/core';
import { HttpServiceProvider } from '../http-service/http-service';

@Injectable()
export class EligibleProvider {

  constructor(
    private httpProvider: HttpServiceProvider
  ) {}

  save(eligible: any) {
    return this.httpProvider.postJson('dados/elegiveis/create', eligible);  
  }

}
