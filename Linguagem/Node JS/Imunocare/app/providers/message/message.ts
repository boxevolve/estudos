import { Injectable } from '@angular/core';
import { AlertController, Platform } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';

@Injectable()
export class MessageProvider {
  constructor(
    private alertCtrl: AlertController, 
    private toastPlugin: Toast, 
    private platform: Platform) {
  }

  alert(message: string);
  alert(message: string, title: string);
  alert(message: string, title: string, cb: any);
  alert(message: string, title: string, cb: any, buttonYesNo: boolean);
  alert(message: string, title: string = '', cb: any = null, buttonYesNo: boolean = false) {
    let buttons;
    if (!buttonYesNo) {
      buttons = [{
        text: 'OK',
        handler: res => {
          if (cb) {
            return cb(true);
          }
        }
      }];
    } else {
      buttons = [{
        text: 'Sim',
        handler: res => {
          if (cb) {
            return cb(true);
          }
        }
      }, {
        text: 'Não'
      }];
    }
    let confirm = this.alertCtrl.create({
      title: title ? title : 'Aviso',
      message: message,
      buttons: buttons
    });
    
    confirm.present();
  };

  error(message: string = '') {
    let confirm = this.alertCtrl.create({
      title: 'Erro',
      message: message || 'Não foi possível concluir a operação. Tente novamente.',
      buttons: [
        {
          text: 'OK'
        }
      ]
    });
    
    confirm.present();
  }  

  toast(message) {
    if (this.platform.is('cordova')) {
      this.toastPlugin.showShortBottom(message).subscribe(res => {
        console.log('show toast: ' + message);
      })
    } else {
      console.log('show toast: ' + message);
    }
  }
}
