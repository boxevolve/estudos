import { Injectable } from '@angular/core';
import { HttpServiceProvider } from '../http-service/http-service';
import { DatabaseProvider } from '../database/database';
import { Observable } from 'rxjs/Observable';
import { MessageProvider } from '../message/message';
import { Platform } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';

@Injectable()
export class UserProvider {
  private token: string;
  private user: any;   

  constructor(
    private httpProvider: HttpServiceProvider, 
    private database: DatabaseProvider, 
    private message: MessageProvider,
    private platform: Platform,
    private storage: NativeStorage
  ) {}

  login(user, password) {
    let params = 'email=' + user + '&senha=' + password;
    return this.httpProvider.post('acesso/login', params);
  }

    getData() {
      return this.httpProvider.get('seguranca/usuarios/buscaDadosUsuario');
    }

    setToken(token) {
      this.token = token;
      this.httpProvider.addToken(token);        
    }

    getToken() {
      return this.token;
    }

    setUserData(data) {
      this.user = data;
    }

    getUserData() {
      return this.user;
    }

    saveCampaignInBatches(data, loading, callback?: any) {
      // console.log(data.length);

      const intervalo = 20000
      let steps = 0, ranges: any[] = [], elegiveis: any[] = [], total = data.length

      if(total > 0) {

        steps = total / intervalo

        for(let i = 0; i < steps; i++) {
          const range = {
            skip: 0,
            limit: 0
          }
          range.skip = intervalo * i
          range.limit = intervalo + range.skip 
          if(range.limit > total ) range.limit = total
          ranges.push(range)
        }
        for(let i = 0; i < steps; i++) {
          for (let d = ranges[i].skip; d < ranges[i].limit; d++) {
            elegiveis.push(data[d])
          }
          this.saveData(elegiveis, loading, true, callback);
          elegiveis = []
        }
        loading.dismiss();
        this.message.alert('Os dados da campanha foram atualizados.', 'Dados Atualizados');
      }
    }

    clearCampaign() {
      this.database.executeSql('delete from campaign').subscribe(res => {
        console.log('base de elegiveis da campanha foram eliminados')
      })      
    }

    saveCampaign(data, loading, onlyOne?: boolean, callback?: any) {
      console.log(data);
      if (onlyOne) {
        this.saveData(data, loading, callback);
      } else {
        this.database.executeSql('delete from campaign').subscribe(res => {
          this.saveData(data, loading, callback);
        })
      }
    }



    saveData(data, loading, init, callback?: any) {
      let count = 0;
      // console.log(`Num regs: ${data.length}...`)
      loading.setContent('Atualizando... ');

      if (this.platform.is('cordova')) {
        let sqls = [];
        data.forEach(item => {
          // let cpf = item.cpf.replace(/\D/g, "");
          // let cpfresp = item.cpfresp.replace(/\D/g, "");

          sqls.push(['insert into campaign(cpf, cpfresp, json) values(?, ?, ?)', [item.cpf, item.cpfresp, JSON.stringify(item)]]);
        });

        // console.log(`SQL count: ${sqls.length}`)

        this.database.executeBatch(sqls).subscribe(() => {
          loading.dismiss();
          if(!init) this.message.alert('Os dados da campanha foram atualizados.', 'Dados Atualizados');
          if (callback) {
            callback();
          }
        }, err => {
          loading.dismiss();
          console.error(err);
        })
      } else {
        data.forEach(item => {
          // let cpf = item.cpf.replace(/\D/g, "");
          // let cpfresp = item.cpfresp.replace(/\D/g, "");

          this.database.executeSql('insert into campaign(cpf, cpfresp, json) values(?, ?, ?)', [item.cpf, item.cpfresp, JSON.stringify(item)]).subscribe(res => {
            count++;

            loading.setContent('Atualizando... ' + count + '/' + data.length);

            if (count == data.length) {
              if (loading) {
                loading.dismiss();
                this.message.alert('Os dados da campanha foram atualizados.', 'Dados Atualizados');
              } else {
                this.message.toast('Banco de dados interno atualizado');
              }
              if (callback) {
                callback();
              }
            }
          })
        });
      }
    }

    findCampaign(cpf) {
      return Observable.create(observer => {
        let result = [];
        cpf = cpf.replace(/\D/g, "");

        this.database.executeSql('select * from campaign where cpf = ? or cpfresp = ?', [cpf, cpf]).subscribe(res => {
          let data = res.rows;
          let first_campaign;

          for (let i = 0; i < data.length; i++) {
            let item = JSON.parse(data.item(i).json)

            if (!first_campaign || first_campaign == item.campanha) {
              first_campaign = item.campanha;
              result.push(item);
            }
          }

          observer.next(result);
          observer.complete(); 
        }, err => {
          observer.error(err);
        })               
      })
    }

    findEligible(cpf) {
      return Observable.create(observer => {
        let result = [];
        cpf = cpf.replace(/\D/g, "");
        
        this.database.executeSql('select * from campaign where cpf = ?', [cpf]).subscribe(res => {
          let data = res.rows;
          for (let i = 0; i < data.length; i++) {
            let item = JSON.parse(data.item(i).json)
            result.push(item);
          }
          observer.next(result);
          observer.complete(); 
        }, err => {
          observer.error(err);
        })               
      })
    }

    registerImmunization(user, item) {
      return Observable.create(observer => {
        this.database.executeSql('insert into immunization(campaign, cpf, eligible, product, lote, immunization, sync) values(?, ?, ?, ?, ?, ?, ?)', [user.campanha, user.cpf, user._id, item.produto, item.lote, item._id, false]).subscribe(res => {
          observer.next(res.insertId);
          observer.complete(); 
        }, err => {
          observer.error(err);
        })               
      })
    }

    syncImmunization(eligible, immunization) {
      let params = 'usuario=' + this.user._id + '&_id=' + immunization;
      return this.httpProvider.post('dados/elegiveis/registraAplicacao?_id=' + eligible, params);
    }

    syncAllImmunization(immunizations) {
      let params = [];
      immunizations.forEach(i => {
        params.push({
          eligible: i.eligibleObject,
          data: {
            idElegivel: i.eligible,
            _id: i.immunization,
            usuario: this.user._id
          }
        })
      });

      return this.httpProvider.postJson('dados/elegiveis/sincronizaImunizacoes', params);
    }

    deleteImmunizations() {
      return Observable.create(observer => {
        this.database.executeSql('delete from immunization').subscribe(res => {
          observer.next();
          observer.complete(); 
        }, err => {
          observer.error(err);
        })               
      })
    }

    updateImmunizationAsSynced(id) {
      return Observable.create(observer => {
        this.database.executeSql('update immunization set sync = ? where id = ?', [true, id]).subscribe(res => {
          observer.next();
          observer.complete(); 
        }, err => {
          observer.error(err);
        })               
      })
    }

    loadImmunizations(campaign) {
      return Observable.create(observer => {
        let result = [];
        this.database.executeSql('select * from immunization where campaign = ?', [campaign]).subscribe(res => {
          let data = res.rows;

          for (let i = 0; i < data.length; i++) {
            result.push({
              id: data.item(i).id,
              cpf: data.item(i).cpf,
              eligible: data.item(i).eligible,
              product: data.item(i).product,
              lote: data.item(i).lote
            });
          }

          observer.next(result);
          observer.complete(); 
        }, err => {
          observer.error(err);
        })               
      })
    }

    loadPendingImmunizations() {
      return Observable.create(observer => {
        let result = [];
        this.database.executeSql('select * from immunization where sync = ?', [false]).subscribe(res => {
          let data = res.rows;

          for (let i = 0; i < data.length; i++) {
            result.push({
              id: data.item(i).id,
              cpf: data.item(i).cpf,
              eligible: data.item(i).eligible,
              product: data.item(i).product,
              immunization: data.item(i).immunization
            });
          }

          observer.next(result);
          observer.complete(); 
        }, err => {
          observer.error(err);
        })               
      })
    }

    getLastUser(cb) {
      if (this.platform.is('cordova')) {
        this.storage.getItem('imunocare-last-user').then(res => {
          return cb(res);
        }, err => {
          return cb('');
        })
      } else {
        return cb(window.localStorage.getItem('imunocare-last-user'));
      }
    }

    setLastUser(user) {
      if (this.platform.is('cordova')) {
        this.storage.setItem('imunocare-last-user', user).then(res => {
          // console.log('last user saved: ' + user);
        }, err => {
          console.error(err);
        })
      } else {
        return window.localStorage.setItem('imunocare-last-user', user);
      }
    }
}