import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpServiceProvider {
    // protected URL: string  = 'https://54.233.151.18:3001/api/';
    protected URL: string = 'https://imunocare.inf.br:3001/api/';
    // protected URL: string = 'http://172.16.1.7:3001/api/';  
    protected header: HttpHeaders;
    protected headerJson: HttpHeaders;

    constructor(public http: HttpClient) {
        this.header = new HttpHeaders();
        this.header = this.header.set('Content-Type', 'application/x-www-form-urlencoded');

        this.headerJson = new HttpHeaders();
        this.headerJson = this.headerJson.set('Content-Type', 'application/json');
    }

    get(resource: string): Observable<any> {
        return this.http.get(this.URL + resource, { headers: this.header });
    }

    post(resource, data): Observable<any> {
        return this.http.post(this.URL + resource, data, { headers: this.header });
    }

    postJson(resource, data): Observable<any> {
        return this.http.post(this.URL + resource, data, { headers: this.headerJson });
    }

    addToken(token) {
        this.header = this.header.set('x-access-token', token);
        this.headerJson = this.headerJson.set('x-access-token', token);
    }
}
