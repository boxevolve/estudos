import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { Platform } from 'ionic-angular';

declare let window: any;

@Injectable()
export class DatabaseProvider {
    private db: any;

    constructor(public http: HttpClient, private sqlite: SQLite, private platform: Platform) {
    }

    createDatabase() {
        if (this.platform.is('cordova')) {
            this.sqlite.create({ name: 'imunocare.db', location: 'default' }).then(database => { 
                this.db = database;
                this.createTables();
            }).catch(err => {
                console.error(err);
            });
        } else {
            this.db = window.openDatabase('imunocare.db', '1.0', 'ImunoCare', -1);
            this.createTables();
        }        
    }

    private createTables() {
        Observable.forkJoin(
            this.executeSql('CREATE TABLE IF NOT EXISTS campaign(id integer primary key, cpf text, cpfresp text, json text)'),
            this.executeSql('CREATE TABLE IF NOT EXISTS immunization(id integer primary key, campaign string, eligible text, cpf text, product text, lote text, immunization text, sync boolean)'),
            this.executeSql('CREATE INDEX IF NOT EXISTS IDX_CPF ON CAMPAIGN(CPF)'),
            this.executeSql('CREATE INDEX IF NOT EXISTS IDX_CPFRESP ON CAMPAIGN(CPFRESP)')
        ).subscribe(res => {
            console.log('Tabelas criadas');
        }, err => {
            console.error('Erro ao criar as tabelas', err);
        })
    }

    executeSql(sql, values: any = null) {
        if (this.platform.is('cordova')) {
            if (!values) {
                values = []
            }

            return Observable.create(observer => {
                this.db.executeSql(sql, values).then(res => {
                    observer.next(res);
                    observer.complete();
                }).catch(err => {       
                    console.error(err);
                    observer.error(err);
                })
            })
        } else {
            if (!values) {
                values = [];
            }

            return Observable.create(observer => {
                this.db.transaction(tx => {
                    tx.executeSql(sql, values, (tx, res) => {
                        observer.next(res);
                        observer.complete();
                    }, (tx, err) => {
                        console.error(err);
                        observer.error(err);
                    });
                })
            })
        }
    }

    executeBatch(sql) {
        if (this.platform.is('cordova')) {
            return Observable.create(observer => {
                this.db.sqlBatch(sql).then(res => {
                    observer.next(res);
                    observer.complete();
                }).catch(err => {       
                    console.error(err);
                    observer.error(err);
                })
            })
        } else {
            return Observable.create(observer => {
                this.db.sqlBatch(sql, (tx, res) => {
                    observer.next(res);
                    observer.complete();
                }, (tx, err) => {
                    console.error(err);
                    observer.error(err);
                })
            })
        }
    }
}