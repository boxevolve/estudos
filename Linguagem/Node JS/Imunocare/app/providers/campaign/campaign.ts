
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpServiceProvider } from '../http-service/http-service';
import { UserProvider } from '../user/user';
import { Observable } from 'rxjs/Observable';
import { from } from 'rxjs/observable/from';
import { mergeMap, delay, concatAll, tap } from 'rxjs/operators';

@Injectable()
export class CampaignProvider {
    private campaigns: any[] = [];

    constructor( private httpProvider: HttpServiceProvider, private platform: Platform,
                 private storage: NativeStorage, private userProvider: UserProvider) {

    }

    loadCampaignOnInit(ranges: any[]): Observable<any[]> {
        let user = this.userProvider.getUserData();

        // const last = ranges.length
        return from(ranges).pipe(
            mergeMap((range, index) => {
                console.log(`SELECAO (${index} - skip:${range.skip} / limit:${range.limit}`)
                if (index === 0) {
                    return <Observable<any[]>> this.httpProvider.get(`dados/elegiveis/dadosCampanha?skip=${range.skip}&limit=${range.limit}` + ((user.campanhas && user.campanhas.length) ? ('&campanha=' +  user.campanhas[0].campanha) : ''))
                        // .pipe(delay(0000))
                        .map(res => res.retorno.elegiveis)
                        .pipe(concatAll());
                } else {
                    return <Observable<any[]>> this.httpProvider.get(`dados/elegiveis/dadosCampanha?skip=${range.skip}&limit=${range.limit}` + ((user.campanhas && user.campanhas.length) ? ('&campanha=' +  user.campanhas[0].campanha) : ''))
                        .pipe(delay(30000))
                        .map(res => res.retorno.elegiveis)
                        .pipe(concatAll());                    
                }
            })
        );
    }
    
    
    loadCampaign(skip?: any, limit?: any) {
        let user = this.userProvider.getUserData();
        return this.httpProvider.get(`dados/elegiveis/dadosCampanha?skip=${skip}&limit=${limit}` + ((user.campanhas && user.campanhas.length) ? ('&campanha=' +  user.campanhas[0].campanha) : ''))  
            }

    getNumElegiveis(user?: any) {
        if (!user)
        user = this.userProvider.getUserData();
        return this.httpProvider.get('dados/elegiveis/numElegiveis' + ((user.campanhas && user.campanhas.length) ? ('?campanha=' +  user.campanhas[0].campanha) : ''));  
    }

    loadCampaignToSpecificUser(campaign_id, cpf) {
        return this.httpProvider.get('dados/elegiveis/dadosCampanha?campanha=' + campaign_id + '&cpf=' + cpf);
    }

    getCampaignData() {
        return this.httpProvider.get('seguranca/usuarios/getCampanhas');
    }

    setCampaigData(data: any) {
        this.campaigns = data;     

        if (this.platform.is('cordova')) {
            this.storage.setItem('imunocare-campaigns', data).then(() => {
                console.log('campaigns saved');
            })
        } else {
            window.localStorage.setItem('imunocare-campaigns', JSON.stringify(data));
        }
    }

    getById(id) {
        let result = {};

        this.campaigns.forEach(c => {
            if (c._id == id) {
                result = c;
            }
        });

        return result;
    }

    getData(): any[] {
        return this.campaigns;
    }

    getCampaignDataOffline() {
        if (this.platform.is('cordova')) {
            this.storage.getItem('imunocare-campaigns').then(res => {
                this.setCampaigData(res);
            }, err => {
                console.log('não achou as campanhas');
            })
        } else {
            if (window.localStorage.getItem('imunocare-campaigns')) {
                this.setCampaigData(JSON.parse(window.localStorage.getItem('imunocare-campaigns')));
            }
        }
    }

    checkProduct(campaign_id, product_id, lote_id, campaigns: any[]) {
        let result = {
            available: true,
            message: ''
        };

        if (!campaigns) {
            campaigns = this.campaigns;
        }

        campaigns.forEach(c => {
            if (c._id == campaign_id) {
                c.produtos.forEach(p => {
                    if (p._id == product_id) {
                        if (p.produto_aplicacoes >= p.produto_qtde) {
                            result.available = false;
                            result.message = 'Quantidade disponível do produto <b>' + p.produto + '</b> atingida. Favor entrar em contato com <b>' + c.distribuidor + '</b>.';
                        } else {
                            p.lotes.forEach(l => {
                                if (l._id == lote_id) {
                                    if (l.lote_aplicacoes >= l.lote_qtde) {
                                        result.available = false;
                                        result.message = 'Quantidade disponível do lote <b>' + l.lote + '</b> atingida. Favor selecionar outro lote';
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });

        return result;
    }

    updateCampaign(campaign_id, product_id, lote_id) {
        this.campaigns.forEach(c => {
            if (c._id == campaign_id) {
                c.produtos.forEach(p => {
                    if (p._id == product_id) {
                        p.produto_aplicacoes++;

                        p.lotes.forEach(l => {
                            if (l._id == lote_id) {
                                l.lote_aplicacoes++;
                            }
                        });
                    }
                });
            }
        });

        this.setCampaigData(this.campaigns);
    }
}
