import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { SQLite } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
import { Toast } from '@ionic-native/toast';
import { Keyboard } from '@ionic-native/keyboard';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CalendarModule } from 'angular-calendar';

import { MyApp } from './app.component';
import { MessageProvider } from '../providers/message/message';
import { HttpServiceProvider } from '../providers/http-service/http-service';
import { UserProvider } from '../providers/user/user';
import { DatabaseProvider } from '../providers/database/database';
import { CampaignProvider } from '../providers/campaign/campaign';
import { EligibleProvider } from '../providers/eligible/eligible';

@NgModule({
    declarations: [
        MyApp
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp, {
            mode: 'md',
            // popoverEnter: 'popover-pop-in',
            // popoverLeave: 'popover-pop-out'
        }),
        HttpClientModule,
        PdfViewerModule,
        CalendarModule.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp
    ],
    providers: [
        StatusBar,
        SplashScreen,
        NativeStorage,
        SQLite,
        Network,
        Toast,
        Keyboard,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        MessageProvider,
        HttpServiceProvider,
        UserProvider,
        DatabaseProvider,
        CampaignProvider,
        EligibleProvider
    ]
})
export class AppModule {}
