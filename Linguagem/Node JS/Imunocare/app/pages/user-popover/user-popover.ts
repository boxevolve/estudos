import { Component } from '@angular/core';
import { IonicPage, ViewController, AlertController, App } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
    selector: 'page-user-popover',
    templateUrl: 'user-popover.html',
})
export class UserPopoverPage {
    constructor(private viewCtrl: ViewController, private app: App, private alertCtrl: AlertController, private userProvider: UserProvider) {
    }

    logoff() {        
        let select = this.alertCtrl.create({
            title: 'Deseja efetuar o logoff?',
            subTitle: 'Você será direcionado para a tela inicial',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'cancel-button'
                },
                {
                    text: 'OK',
                    cssClass: 'ok-button',
                    handler: data => {
                        this.userProvider.setLastUser('');
                        this.app.getRootNav().setRoot('LoginPage');     
                    }
                }
            ]
        });
        
        select.present();
        this.viewCtrl.dismiss();
    }
}
