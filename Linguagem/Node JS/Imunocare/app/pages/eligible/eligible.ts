import { Component, AfterViewInit, AfterContentChecked, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { MessageProvider } from '../../providers/message/message';
import { Network } from '@ionic-native/network';
import { CampaignProvider } from '../../providers/campaign/campaign';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BaseComponent } from '../../shared/utils/base.component';
import { SimNao } from '../../shared/const/sim-nao.const';
import { Constants } from '../../shared/utils/constants';
import { InputType } from '../../shared/const/input-type.const';
import { EligibleProvider } from '../../providers/eligible/eligible';
import { Util } from '../../shared/utils/util';
import * as ObjectID from 'bson-objectid';

@IonicPage()
@Component({
  selector: 'page-eligible',
  templateUrl: 'eligible.html',
})
export class EligiblePage extends BaseComponent implements AfterViewInit, AfterContentChecked {
  nameCampanha = 'campanha';
  nameMatricula = 'matricula';
  nameCpf = 'cpf';
  nameNome = 'nome';
  nameDependente = 'dependente';
  nameCpfresp = 'cpfresp';
  nameDtnasc = 'dtnasc';
  nameSexo = 'sexo';
  nameClassificacao = 'classificacao';
  nameEmail = 'email';
  nameCnpj = 'cnpj';
  nameCliente = 'cliente';
  nameImunizacoes = 'imunizacoes';

  nameFormPendenteAprovacao = 'pendente_aprovacao';

  idDivCpfresp = 'div_cpfresp';
  idDivForm = 'div_form';
  
  private user: any;
  private form: FormGroup;
  private pendenteAprovacao: boolean;
  private campaigns: any[];
  private filiais: any[];
  public showForm: boolean;
  public cpf: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private userProvider: UserProvider, 
    private message: MessageProvider,
    private network: Network,
    private loadingCtrl: LoadingController,
    private campaignProvider: CampaignProvider,
    private eligibleProvider: EligibleProvider,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private cdr: ChangeDetectorRef
  ) {
    super();
    this.cpf = this.navParams.data.data;
    this.user = this.userProvider.getUserData();
    this.createFormGroup();
    this.init();
  }

  ngAfterViewInit() {
    this.init();
    this.showHideForm(false);
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

  public createFormGroup(cpf?: string): void {
    this.form = this.formBuilder.group({
      _id: [null],
      pendente_aprovacao: [this.pendenteAprovacao !== undefined ? this.pendenteAprovacao : null, Validators.required],
      imunizacoes: [null, Validators.required]
    });
  }

  ionViewDidLoad() {
    this.loadCampaign();
  }

  public loadCampaign(callback?: any) {
    if (Util.isOnline(this.network)) {
      this.userProvider.getLastUser(last_user => {
        if (last_user != this.user.email || callback) {
          let loading;
          loading = this.loadingCtrl.create({
            showBackdrop: true,
            cssClass: 'sync-loading',
            content: 'Obtendo dados...'
          });
          loading.present();
          
          this.campaignProvider.loadCampaign().subscribe(res => {
            if (callback) {
              callback(res);
            } else {
              this.campaignProvider.setCampaigData(res.retorno.campanha);
              this.userProvider.saveCampaign(res.retorno.elegiveis, loading);
              this.userProvider.setLastUser(this.user.email);
            }
          }, err => {
            console.error(err);
            loading.dismiss();

            if (err.error && err.error.mensagem) {
              return this.message.alert(err.error.mensagem);
            }

            return this.message.error();
          })
        } else {
          this.campaignProvider.getCampaignDataOffline();
        }
      })
    } else {
      this.campaignProvider.getCampaignDataOffline();
    }
  }

  public save(event?: any): void {
    this.setValueForm(this.form, this.nameFormPendenteAprovacao, this.pendenteAprovacao);
    this.setValueForm(this.form, this.nameImunizacoes, this.createImunizacoes(this.getCampaignByList()));

    if (!this.getValueForm(this.form, this.nameImunizacoes).length) {
      this.message.alert(Constants.MSG_CAMPAIGN_WITHOUT_PRODUCT, Constants.MSG_SPE_TITLE_ALERT);
      return;
    }

    this.validateForm(this.form, () => {
      const valueForm = this.getCloneForm(this.form);
      if (!valueForm[this.nameDependente]) {
        valueForm[this.nameCpfresp] = valueForm[this.nameCpf];
      }
      valueForm[this.nameCpf] = this.clearCPF(valueForm[this.nameCpf]);
      valueForm[this.nameCpfresp] = this.clearCPF(valueForm[this.nameCpfresp]);
      this.saveEligible(valueForm);
    }, () => {
      this.message.alert(Constants.MSG_ALL_REQUIRED_FIELD, Constants.MSG_SPE_TITLE_ALERT);
    });
  }

  public createImunizacoes(campanha: any): any[] {
    const imunizacoes = [];
    if (campanha) {
      campanha.produtos.forEach(produto => {
        produto.lotes.forEach(lote => {
          const imunizacao = {
            _id: ObjectID.default.generate(),
            produto: produto._id,
            desc_produto: produto.produto.produto,
            lote: lote._id,
            num_lote: lote.lote,
            aplicacao: false
          }
          imunizacoes.push(imunizacao);
        });
      });
    }
    return imunizacoes;
  }

  /**
   * Carga muito pesada.
   * 
   * @param eligible
   */
  public saveEligible(eligible: any): void {
    let loading;
    loading = this.loadingCtrl.create({
      showBackdrop: true,
      cssClass: 'sync-loading',
      content: 'Salvando elegível...'
    });
    loading.present();

    if (Util.isOnline(this.network)) {
      this.eligibleProvider.save(eligible).subscribe(res => {
        this.finishSave(res.retorno, loading);
      }, err => {
        console.error(err);
        loading.dismiss();
        if (err.error && err.error.mensagem) {
          return this.message.alert(err.error.mensagem);
        }
        return this.message.error();
      });
    } else {
      eligible._id = ObjectID.default.generate();
      this.finishSave(eligible, loading);
    }
  }

  public finishSave(eligible: any, loading: any): void {
    this.userProvider.saveCampaign([eligible], loading, true, () => {
      this.navCtrl.setRoot('CampaignPage', { data: [eligible] });
    });
  }

  public getCampaigns(): any[] {
    if (!this.campaigns) {
      this.campaigns = this.campaignProvider.getData();
    }
    return this.campaigns;
  }

  public getFiliais(): any[] {
    if (!this.filiais || !this.filiais.length) {
      const campaign = this.getCampaignByList();
      this.filiais = campaign ? campaign.filiais : [];
    }
    return this.filiais;
  }

  public changeDependente(event?: any): void {
    this.showHideElement(this.idDivCpfresp, event);
    if (!event) {
      this.removeValidationForm(this.form, this.nameCpfresp);
    }
    this.setValueForm(this.form, this.nameCpfresp, null);
  }

  public changeCampaign(event?: any): void {
    this.showHideForm(false);
    this.clear();

    if (!event) {
      return;
    }
    const campaign = this.getCampaignByList();
    if (campaign && !campaign.cadastro_auto) {
      this.pendenteAprovacao = true;
      this.showPassword(campaign);
    } else {
      this.pendenteAprovacao = false;
      this.showHideForm(true);
    }
  }

  public showPassword(campaign: any): void {
    let select = this.alertCtrl.create({
      title: Constants.MSG_SPE_TITLE_PASSWORD_MANAGER,
      subTitle: Constants.MSG_SPE_PASSWORD_MANAGER,
      inputs: [{
        type: InputType.PASSWORD.value,
        name: 'password'
      }],
      buttons: [{
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'cancel-button'
        }, {
          text: 'Enviar',
          cssClass: 'ok-button',
          handler: data => {
            if (this.equals(data.password, campaign.senha_vacinacao)) {
              this.showHideForm(true);
            } else {
              this.message.alert(Constants.MSG_PASSWORD_INVALID, Constants.MSG_SPE_TITLE_ALERT, (result?: any) => {
                this.showPassword(campaign);
              });
            }
          }
      }]
    });
    select.present();
  }

  public showHideForm(show: boolean): void {
    this.showHideElement(this.idDivForm, show);
  }

  public clear(): void {
    this.clearForm(this.form,
      [
        this.nameMatricula, 
        this.nameCpf, 
        this.nameNome, 
        this.nameDependente, 
        this.nameCpfresp, 
        this.nameDtnasc, 
        this.nameSexo,
        this.nameClassificacao,
        this.nameEmail,
        this.nameCnpj,
        this.nameCliente,
        this.nameImunizacoes
    ]);
    this.setValueForm(this.form, this.nameDependente, false);
    this.setValueForm(this.form, this.nameCpf, this.cpf);
    this.init();
  }

  public getCampaignByList(): any {
    return this.findObject(this.campaigns, null, (x?: any) => {
      return this.equals(x._id, this.getValueForm(this.form, this.nameCampanha));
    });
  }

  public isDependente(): boolean {
    return this.getValueForm(this.form, this.nameDependente);
  }

  public getNao(): boolean {
    return SimNao.NAO.bool;
  }

  public init(): void {
    this.hideElement(this.idDivCpfresp);
  }

}
