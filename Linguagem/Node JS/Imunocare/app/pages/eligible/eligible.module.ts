import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EligiblePage } from './eligible';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    EligiblePage
  ],
  imports: [
    IonicPageModule.forChild(EligiblePage),
    SharedModule
  ],
})
export class EligiblePageModule {}
