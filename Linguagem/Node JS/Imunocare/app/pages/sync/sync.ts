import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, PopoverController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { MessageProvider } from '../../providers/message/message';
import { CampaignProvider } from '../../providers/campaign/campaign';
import { Network } from '@ionic-native/network';
import { BaseComponent } from '../../shared/utils/base.component';
import { Util } from '../../shared/utils/util';

@IonicPage()
@Component({
    selector: 'page-sync',
    templateUrl: 'sync.html',
})
export class SyncPage extends BaseComponent {
  private user: any;
    
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private userProvider: UserProvider, 
    private campaignProvider: CampaignProvider,
    private alertCtrl: AlertController, 
    private loadingCtrl: LoadingController,
    private message: MessageProvider,
    private popoverCtrl: PopoverController,
    private network: Network
  ) {
    super();
    this.user = this.userProvider.getUserData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SyncPage');
  }

  ionViewDidEnter() {
    this.startSync();
  }

  private startSync() {
    let select = this.alertCtrl.create({
      title: 'Confirma Sincronização?',
      subTitle: 'Esta função irá sincronizar toda a base do ImunoCare',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'cancel-button'
        },
        {
          text: 'Confirmar',
          cssClass: 'ok-button',
          handler: data => {
            this.sync();                
          }
        }
      ]
    });
    
    select.present();
  }

  private sync() {
    if (Util.isOnline(this.network)) {
      let loading;
      loading = this.loadingCtrl.create({
        showBackdrop: true,
        cssClass: 'sync-loading',
        content: 'Procurando dados pendentes...'
      });

      loading.present();

      this.userProvider.loadPendingImmunizations().subscribe(res => {
        console.log(res);
        
        if (res.length > 0) {
          let count = 0;
          for (let index = 0; index < res.length; index++) {
            const element = res[index];
            this.userProvider.findEligible(element.cpf).subscribe(result => {
              element.eligibleObject = result.length ? result[0] : null;
              count++;
              if (count === res.length) {
                this.finishSync(loading, res);
              }
            }, err => {
              loading.dismiss();            
              console.error(err);
              return this.message.error();
            })
          }
        } else {
          loading.setContent('Atualizando dados...');
          
          this.userProvider.getData().subscribe(user => {
            this.userProvider.setUserData(user)
            this.campaignProvider.getNumElegiveis(user).subscribe(res => {

              const intervalo = 20000
              let steps = 0, ranges: any[] = [], total = res.contagem

              this.campaignProvider.setCampaigData(res.retorno.campanha);

              if(total > 0) {
                steps = total / intervalo
                // if((res.contagem % intervalo) !== 0) steps++
    /******************************************************************************* */              
                for(let i = 0; i < steps; i++) {
                  const range = {
                    skip: 0,
                    limit: 0
                  }
                  range.skip = intervalo * i
                  range.limit = intervalo
                  if( (range.skip + range.limit) > total ) range.limit = total - range.skip
                  ranges.push(range)
                }
              }

              let elegiveis: any[] = []
              this.campaignProvider.loadCampaignOnInit(ranges)
                .subscribe( res => {
                  elegiveis.push(res)
                  loading.setContent(`Obtendo registro ${elegiveis.length} de ${total}`)
                  if(elegiveis.length === 1) this.userProvider.clearCampaign()
                  if(elegiveis.length === total) {
                    // console.log(`total de elegiveis: ${elegiveis.length}`)
                    this.userProvider.saveCampaignInBatches(elegiveis, loading)
                    // console.log(`total de elegiveis gravados: ${elegiveis.length}`)
                    this.userProvider.setLastUser(this.user.email);              
                  }
                }, err => {
                  console.error(err);
                  loading.dismiss();
      
                  if (err.error && err.error.mensagem) {
                    return this.message.alert(err.error.mensagem);
                  }
      
                  return this.message.error();
                }
              )
              console.log(`total: ${elegiveis.length}`)
            // })
    /******************************************************************************* */    
            }, err => {
              loading.dismiss();            
              console.error(err);
              this.message.error();
            })
          })
        } 
      }, err => {
        console.error(err);
        loading.dismiss();
        this.message.error();
      })
    } else {
      return this.message.alert('Sem conexão para sincronizar os dados.');
    }
  }

  finishSync(loading, pendingImmunizations) {
    loading.setContent('Enviando dados...');
    this.userProvider.syncAllImmunization(pendingImmunizations).subscribe(res => {
      console.log(res);

      this.userProvider.deleteImmunizations().subscribe(res => {
        console.log(res);

        loading.setContent('Atualizando dados...');

        // setTimeout(() => {
        //   this.campaignProvider.loadCampaign().subscribe(c => {     
        //     this.campaignProvider.setCampaigData(c.retorno.campanha);
        //     this.userProvider.saveCampaign(c.retorno.elegiveis, loading);
        //     this.userProvider.setLastUser(this.user.email);
        //   }, err => {
        //     loading.dismiss();            
        //     console.error(err);
        //     this.message.error();
        //   })
        // }, 1000);
      this.userProvider.getData().subscribe(user => {
        this.userProvider.setUserData(user)
        setTimeout(() => {
          this.campaignProvider.getNumElegiveis(user).subscribe(res => {

            const intervalo = 20000
            let steps = 0, ranges: any[] = [], total = res.contagem

            this.campaignProvider.setCampaigData(res.retorno.campanha);

            if(total > 0) {
              steps = total / intervalo
              // if((res.contagem % intervalo) !== 0) steps++
  /******************************************************************************* */              
              for(let i = 0; i < steps; i++) {
                const range = {
                  skip: 0,
                  limit: 0
                }
                range.skip = intervalo * i
                range.limit = intervalo
                if( (range.skip + range.limit) > total ) range.limit = total - range.skip
                ranges.push(range)
              }
            }

            let elegiveis: any[] = []
            this.campaignProvider.loadCampaignOnInit(ranges)
              .subscribe( res => {
                elegiveis.push(res)
                loading.setContent(`Obtendo registro ${elegiveis.length} de ${total}`)
                if(elegiveis.length === 1) this.userProvider.clearCampaign()
                if(elegiveis.length === total) {
                  // console.log(`total de elegiveis: ${elegiveis.length}`)
                  this.userProvider.saveCampaignInBatches(elegiveis, loading)
                  // console.log(`total de elegiveis gravados: ${elegiveis.length}`)
                  this.userProvider.setLastUser(this.user.email);              
                }
              }, err => {
                console.error(err);
                loading.dismiss();
    
                if (err.error && err.error.mensagem) {
                  return this.message.alert(err.error.mensagem);
                }
    
                return this.message.error();
              }
            )
            console.log(`total: ${elegiveis.length}`)
          })
        }, 1000)
      })
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      }, err => {
        console.error();
        loading.dismiss();
        this.message.error();
      })
    }, err => {
      console.error(err);
      loading.dismiss();
      this.message.alert('Não foi possível enviar os dados para o servidor. Verifique sua conexão e tente novamente.', 'Erro');
    })
  }

  showPopover(event) {
    let popover = this.popoverCtrl.create('UserPopoverPage', {}, { cssClass: 'logout-popover' });
    popover.present({ ev: event });
  }
}
