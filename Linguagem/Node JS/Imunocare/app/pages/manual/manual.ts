import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, PopoverController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
    selector: 'page-manual',
    templateUrl: 'manual.html',
})
export class ManualPage {
    private user: any;
    private loading: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, private loadingCtrl: LoadingController, private popoverCtrl: PopoverController) {
        this.user = this.userProvider.getUserData();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ManualPage');

        this.loading = this.loadingCtrl.create({
            showBackdrop: true
        });
        this.loading.present();
    }

    callBackFn(pdf) {
        this.loading.dismiss();
    }

    onError(error) {
        console.error(error);
        this.loading.dismiss();
    }

    showPopover(event) {
        let popover = this.popoverCtrl.create('UserPopoverPage', {}, { cssClass: 'logout-popover' });
        popover.present({ ev: event });
    }
}
