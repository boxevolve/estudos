import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManualPage } from './manual';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [
    ManualPage,
  ],
  imports: [
    IonicPageModule.forChild(ManualPage),
    PdfViewerModule
  ],
})
export class ManualPageModule {}
