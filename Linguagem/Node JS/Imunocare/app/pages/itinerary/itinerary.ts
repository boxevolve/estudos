import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
    selector: 'page-itinerary',
    templateUrl: 'itinerary.html',
})
export class ItineraryPage {
    private user: any;
    private today: any;
    private days_label: Array<string> = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'];
    
    constructor(public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, private popoverCtrl: PopoverController) {
        this.today = new Date();
        this.user = this.userProvider.getUserData();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ItineraryPage');
    }

    showPopover(event) {
        let popover = this.popoverCtrl.create('UserPopoverPage', {}, { cssClass: 'logout-popover' });
        popover.present({ ev: event });
    }
}
