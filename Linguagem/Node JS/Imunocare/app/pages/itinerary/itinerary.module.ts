import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ItineraryPage } from './itinerary';
import { CalendarModule } from 'angular-calendar';

@NgModule({
    declarations: [
        ItineraryPage,
    ],
    imports: [
        IonicPageModule.forChild(ItineraryPage),
        CalendarModule.forRoot()
    ],
})
export class ItineraryPageModule {}
