import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events, LoadingController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { MessageProvider } from '../../providers/message/message';
import { CampaignProvider } from '../../providers/campaign/campaign';
import { Network } from '@ionic-native/network';
import { Observable } from 'rxjs/Observable';
import { Util } from '../../shared/utils/util';

@IonicPage()
@Component({
    selector: 'page-immunization',
    templateUrl: 'immunization.html',
})
export class ImmunizationPage {
    private user: any;
    private immunizations: any;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams, 
        private alertCtrl: AlertController, 
        private userProvider: UserProvider,
        private campaignProvider: CampaignProvider,
        private message: MessageProvider,
        private events: Events,
        private network: Network,
        private loadingCtrl: LoadingController
    ) {
        this.user = this.navParams.data.user;
        this.immunizations = this.navParams.data.immunizations;
    }

    selectImmunization(item) {
        this.verifyImmunizationOnline(item).subscribe(ok => {
            if (!ok.status) {
                return this.message.alert('Produto já aplicado em outra origem. Efetue a sincronização para atualizar os dados locais.');
            } else {
                if (this.verifyImmunization(item)) {
                    return this.message.alert('Produto já aplicado.');
                }

                let check = this.campaignProvider.checkProduct(this.user.campanha, item.produto, item.lote, (ok.campaigns ? ok.campaigns : null));
                if (!check.available) {
                    return this.message.alert(check.message);
                }

                let select = this.alertCtrl.create({
                    title: 'Confirma a aplicação da vacina?',
                    subTitle: this.user.nome + ', ' + 
                            (this.user.sexo == 'M' ? 'masculino' : 'feminino') + ', ' + 
                            this.calculateAge(this.user.dtnasc) + ' anos, ' +
                            item.desc_produto + ', ' +
                            'lote ' + item.num_lote,
                    buttons: [
                        {
                            text: 'Cancelar',
                            role: 'cancel',
                            cssClass: 'cancel-button'
                        },
                        {
                            text: 'Aplicar',
                            cssClass: 'ok-button',
                            handler: data => {
                                //save in local database
                                this.userProvider.registerImmunization(this.user, item).subscribe(res => {
                                    console.log(res);

                                    this.campaignProvider.updateCampaign(this.user.campanha, item.produto, item.lote);

                                    let new_id = res;
                                    //sync
                                    if (Util.isOnline(this.network)) {
                                        this.userProvider.syncImmunization(this.user._id, item._id).subscribe(res => {                                
                                            //update as synced
                                            console.log(res);
                                            this.userProvider.updateImmunizationAsSynced(new_id).subscribe(res => {
                                                console.log(res);
                                            })
                                        }, err => {
                                            console.error(err);
                                        })
                                    }

                                    this.events.publish('confirm:immunization');

                                    this.navCtrl.pop();
                                }, err => {
                                    console.error(err);
                                    this.message.error();
                                });                        
                            }
                        }
                    ]
                });
                
                select.present();
            }
        })        
    }

    private calculateAge(birthday) {
        birthday = new Date(birthday);
        let ageDifMs = Date.now() - birthday.getTime();
        let ageDate = new Date(ageDifMs);

        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    showInjection(item) {
        if (item && this.immunizations) {
            return this.verifyImmunization(item);
        } 
    }

    private verifyImmunization(item) {
        let find = false;

        if (item.aplicacao) {
            find = true;
        }

        if (!find) {
            this.immunizations.forEach(i => {
                if (i.product == item.produto) {
                    find = true;
                }
            });
        }

        return find;
    }

    private verifyImmunizationOnline(item) {
        return Observable.create(observer => {
            var result = {
                status: true,
                campaigns: null
            }

            if (Util.isOnline(this.network)) {
                let loading = this.loadingCtrl.create({
                    showBackdrop: true
                });
                loading.present();

                this.campaignProvider.loadCampaignToSpecificUser(this.user.campanha, this.user.cpf).subscribe(res => {
                    loading.dismiss();

                    let find = false;

                    res.retorno.elegiveis.forEach(user => {
                        if (user.cpf == this.user.cpf && user.campanha == this.user.campanha) {
                            user.imunizacoes.forEach(i => {
                                if (i.produto == item.produto && i.aplicacao) {                                
                                    find = true;
                                }
                            });
                        }
                    });

                    result.status = !find;
                    result.campaigns = res.retorno.campanha;

                    observer.next(result);
                    observer.complete();
                }, err => {
                    loading.dismiss();

                    observer.next(result);
                    observer.complete();
                });
            } else {
                observer.next(result);
                observer.complete();
            }
        })
    }
}
