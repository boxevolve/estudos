import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { MessageProvider } from '../../providers/message/message';
import { Observable } from 'rxjs/Observable';
import { Network } from '@ionic-native/network';
import { CampaignProvider } from '../../providers/campaign/campaign';
import { Util } from '../../shared/utils/util';

const NO_IMMUNIZED = 0;
const PARTIAL_IMMUNIZED = 1;
const TOTAL_IMMUNIZED = 2;

@IonicPage()
@Component({
    selector: 'page-campaign',
    templateUrl: 'campaign.html',
})
export class CampaignPage {    
    private users;
    private immunizations;
    private campaign: any;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams, 
        private userProvider: UserProvider, 
        private events: Events,
        private message: MessageProvider,
        private network: Network,
        private loadingCtrl: LoadingController,
        private campaignProvider: CampaignProvider
    ) {
        this.users = this.navParams.data;
        this.loadImmunizations();
        this.campaign = this.campaignProvider.getById(this.users.data[0].campanha);

        this.events.unsubscribe('confirm:immunization');
        this.events.subscribe('confirm:immunization', res => {
            this.loadImmunizations();
        })
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CampaignPage');
    }

    selectUser(user) {        
        this.verifyImunnizationOnline(user).subscribe(ok => {
            if (!ok) {
                return this.message.alert('Paciente já imunizado em outra origem. Efetue a sincronização para atualizar os dados locais.');
            } else {
                if (this.verifyImunnization(user) == TOTAL_IMMUNIZED) {
                    return this.message.alert('Paciente já imunizado.');
                }
                let immunizations = [];
                this.immunizations.forEach(i => {
                    if (user._id != null && i.eligible == user._id) {
                        immunizations.push(i);
                    }
                });

                this.navCtrl.push('ImmunizationPage', { user: user, immunizations: immunizations });    
            }
        })
    }    

    loadImmunizations() {
        this.userProvider.loadImmunizations(this.users.data[0].campanha).subscribe(res => {
            this.immunizations = res;

            this.users.data.forEach(item => {
                let total = item.imunizacoes.length;
                let count = 0;

                item.imunizacoes.forEach(obj => {
                    if (obj.aplicacao) {
                        count++;
                    } else {
                        this.immunizations.forEach(local => {
                            if (local.eligible == item._id && local.product == obj.produto) {
                                count++;
                            }
                        });
                    }
                });

                item.total_aplicacoes = total;
                item.contador_aplicacoes = count;
                item.imagens_aplicacoes = [];
                for (let i = 0; i < count; i++) {
                    item.imagens_aplicacoes.push('assets/imgs/injection.png');
                }
            });
        }, err => {
            console.error(err);
        })
    }

    showImmunization(user) {
        if (user && this.immunizations) {
            return this.verifyImunnization(user) != NO_IMMUNIZED;
        }
    }

    private verifyImunnization(user) {
        let products = [];
        let applications = 0;

        user.imunizacoes.forEach(obj => {
            let has_product = false;
            products.forEach(p => {
                if (p == obj.produto) {
                    has_product = true;
                }
            });

            if (!has_product) {
                products.push(obj.produto);
            }

            if (obj.aplicacao) {
                applications++;
            } else {
                this.immunizations.forEach(local => {
                    if (user._id !== null && local.eligible == user._id && local.product == obj.produto) {
                        applications++;
                    }
                });
            }
        });

        if (applications == 0) {
            return NO_IMMUNIZED;
        } else if (applications < products.length) {
            return PARTIAL_IMMUNIZED;
        } else if (applications >= products.length) {
            return TOTAL_IMMUNIZED;
        }
    }
    
    private calculateAge(birthday) {
        birthday = new Date(birthday);
        let ageDifMs = Date.now() - birthday.getTime();
        let ageDate = new Date(ageDifMs);

        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    private verifyImunnizationOnline(user) {
        return Observable.create(observer => {
            if (Util.isOnline(this.network)) {
                let loading = this.loadingCtrl.create({
                    showBackdrop: true
                });
                loading.present();

                this.campaignProvider.loadCampaignToSpecificUser(user.campanha, user.cpf).subscribe(res => {
                    loading.dismiss();

                    let applications = 0;
                    let products = [];

                    res.retorno.elegiveis.forEach(u => {
                        if (user.cpf == u.cpf && user.campanha == u.campanha) {
                            u.imunizacoes.forEach(obj => {
                                let has_product = false;
                                products.forEach(p => {
                                    if (p == obj.produto) {
                                        has_product = true;
                                    }
                                });

                                if (!has_product) {
                                    products.push(obj.produto);
                                }

                                if (obj.aplicacao) {
                                    applications++;
                                }
                            });
                        }
                    });

                    if (applications > 0 && applications >= products.length) {
                        observer.next(false);
                        observer.complete();
                    } else {
                        observer.next(true);
                        observer.complete();
                    }
                }, err => {
                    loading.dismiss();

                    observer.next(true);
                    observer.complete();
                });
            } else {
                observer.next(true);
                observer.complete();
            }
        })
    }
}
