import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-main',
    templateUrl: 'main.html'
})
export class MainPage {
    vaccinationRoot = 'VaccinationPage'
    itineraryRoot = 'ItineraryPage'
    manualRoot = 'ManualPage'
    syncRoot = 'SyncPage'


    constructor(public navCtrl: NavController) {}
}
