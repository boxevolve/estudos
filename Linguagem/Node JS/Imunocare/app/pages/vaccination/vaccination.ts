import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, PopoverController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { MessageProvider } from '../../providers/message/message';
import { Network } from '@ionic-native/network';
import { CampaignProvider } from '../../providers/campaign/campaign';
import { Util } from '../../shared/utils/util';

@IonicPage()
@Component({
  selector: 'page-vaccination',
  templateUrl: 'vaccination.html',
})
export class VaccinationPage {
  @ViewChild('barsearch') barsearch: any;
  user: any;
  cpf_campaign: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private userProvider: UserProvider, 
    private campaignProvider: CampaignProvider,
    private loadingCtrl: LoadingController,
    private message: MessageProvider,
    private popoverCtrl: PopoverController,
    private network: Network
  ) {
    this.user = this.userProvider.getUserData();
  }

  ionViewDidLoad() {
    this.loadCampaign();
  }

  ionViewDidEnter() {
    this.barsearch.setFocus();
  }

  // loadCampaign() {
  //   if (Util.isOnline(this.network)) {
  //     this.userProvider.getLastUser(last_user => {
  //       if (last_user != this.user.email) {
  //         let loading;
  //         loading = this.loadingCtrl.create({
  //           showBackdrop: true,
  //           cssClass: 'sync-loading',
  //           content: 'Obtendo dados...'
  //         });
  //         loading.present();
          
  //         this.campaignProvider.loadCampaign().subscribe(res => {
  //           this.campaignProvider.setCampaigData(res.retorno.campanha);
            
  //           this.userProvider.saveCampaign(res.retorno.elegiveis, loading);
  //           this.userProvider.setLastUser(this.user.email);
  //         }, err => {
  //           console.error(err);
  //           loading.dismiss();

  //           if (err.error && err.error.mensagem) {
  //             return this.message.alert(err.error.mensagem);
  //           }

  //           return this.message.error();
  //         })
  //       } else {
  //         this.campaignProvider.getCampaignDataOffline();
  //       }
  //     })
  //   } else {
  //     this.campaignProvider.getCampaignDataOffline();
  //   }
  // }

  loadCampaign() {
    if (Util.isOnline(this.network)) {
      this.userProvider.getLastUser(last_user => {
        if (last_user != this.user.email) {
          let loading;
          loading = this.loadingCtrl.create({
            showBackdrop: true,
            cssClass: 'sync-loading',
            content: 'Obtendo dados...'
          });
          loading.present();

          this.campaignProvider.getNumElegiveis().subscribe(res => {

            const intervalo = 20000
            let steps = 0, ranges: any[] = [], total = res.contagem

            this.campaignProvider.setCampaigData(res.retorno.campanha);

            if(total > 0) {

              steps = total / intervalo
  /******************************************************************************* */              
              for(let i = 0; i < steps; i++) {
                const range = {
                  skip: 0,
                  limit: 0
                }
                range.skip = intervalo * i
                range.limit = intervalo
                if( (range.skip + range.limit) > total ) range.limit = total - range.skip
                ranges.push(range)
              }

            }

            let elegiveis: any[] = []
            this.campaignProvider.loadCampaignOnInit(ranges)
              .subscribe( res => {
                elegiveis.push(res)
                loading.setContent(`Obtendo registro ${elegiveis.length} de ${total}`)
                if(elegiveis.length === 1) this.userProvider.clearCampaign()
                if(elegiveis.length === total) {
                  // console.log(`total de elegiveis: ${elegiveis.length}`)
                  this.userProvider.saveCampaignInBatches(elegiveis, loading)
                  // console.log(`total de elegiveis gravados: ${elegiveis.length}`)
                  this.userProvider.setLastUser(this.user.email);              
                }
              }, err => {
                console.error(err);
                loading.dismiss();
    
                if (err.error && err.error.mensagem) {
                  return this.message.alert(err.error.mensagem);
                }
    
                return this.message.error();
              }
            )
            // console.log(`total: ${elegiveis.length}`)
            /******************************************************************************* */
          })
        } else {
          this.campaignProvider.getCampaignDataOffline();
        }
      })
    } else {
      this.campaignProvider.getCampaignDataOffline();
    }
  }


  findCampaign(cpf) {
    let loading;
    loading = this.loadingCtrl.create({
      showBackdrop: true
    });
    loading.present();

    this.userProvider.findCampaign(cpf).subscribe(res => {
      loading.dismiss();
      this.cpf_campaign = '';

      if (res.length == 0) {
        this.barsearch.setFocus();
        return this.message.alert('Deseja cadastrar novo elegível?',
                                  'Nenhuma campanha encontrada para o CPF informado',
                                  (result: any) => {
                                    this.navCtrl.push('EligiblePage', { data: cpf });
                                  },
                                  true);
      } else {
        this.navCtrl.push('CampaignPage', { data: res });
      }
    }, err => {
      loading.dismiss();            
      console.error(err);

      return this.message.error();
    })
  }

  cpfMask(value) {
    if (value) {
      let v = value.replace(/\D/g, "");
      let cpf = {
        n1: v.substring(0, 3),
        n2: v.substring(3, 6),
        n3: v.substring(6, 9),
        n4: v.substring(9, 11)
      }

      let res = cpf.n1 
      if (cpf.n2) { res += '.' + cpf.n2 }
      if (cpf.n3) { res += '.' + cpf.n3 }
      if (cpf.n4) { res += '-' + cpf.n4 }

      this.cpf_campaign = res;

      if (this.cpf_campaign.length == 14) {
        this.findCampaign(this.cpf_campaign);
      }
    }
  }

  showPopover(event) {
    let popover = this.popoverCtrl.create('UserPopoverPage', {}, { cssClass: 'logout-popover' });
    popover.present({ ev: event });
  }
}
