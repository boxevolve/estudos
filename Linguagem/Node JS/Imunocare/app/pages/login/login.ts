import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Platform } from 'ionic-angular';
import { MessageProvider } from '../../providers/message/message';
import { UserProvider } from '../../providers/user/user';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    public login_data: {
        user: string,
        password: string,
        save: boolean,
        token: string,
        expires_date: Date,
        last_user: string,
        name: string,
        profile: string
    }

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams, 
        private message: MessageProvider,
        private loadingCtrl: LoadingController,
        private userProvider: UserProvider,
        private alertCtrl: AlertController,
        private platform: Platform,
        private storage: NativeStorage,
        private network: Network
    ) {
        this.login_data = {
            user: '',
            password: '',
            save: false,
            token: '',
            expires_date: null,
            last_user: '',
            name: '',
            profile: ''
        }

        if (this.platform.is('cordova')) {
            this.storage.getItem('imunocare-user').then(res => {
                if (res) {
                    this.message.toast('Dados do usuário recuperado');
                }
                
                if (res.save) {
                    this.login_data.save = true;
                    this.login_data.user = res.user;
                }

                if (res.token) {
                    this.login_data.token = res.token;
                }

                if (res.expires_date) {
                    this.login_data.expires_date = res.expires_date;
                }

                if (res.last_user) {
                    this.login_data.last_user = res.last_user;
                }

                if (res.name) {
                    this.login_data.name = res.name;
                }

                if (res.profile) {
                    this.login_data.profile = res.profile;
                }
            }, err => {
                console.log('não achou o usuário');
            })
        } else {
            if (window.localStorage.getItem('imunocare-user')) {
                let data = JSON.parse(window.localStorage.getItem('imunocare-user'));
                if (data.save) {
                    this.login_data.save = true;
                    this.login_data.user = data.user;
                }
            }
        }
    }

    doLogin() {        
        if (!this.login_data.user) {
            return this.message.alert('O e-mail não foi informado ou está inválido');
        }

        if (!this.login_data.password) {
            return this.message.alert('A senha não foi informada');
        }

        let loading;
        loading = this.loadingCtrl.create({
            showBackdrop: true
        });
        loading.present();

        if (this.network.type === 'none') {
            loading.dismiss();

            let today = new Date();
            let expires;
            if (this.login_data.expires_date) {
                expires = new Date(this.login_data.expires_date);
            }

            if (this.login_data.last_user == this.login_data.user && this.login_data.token && expires && expires >= today) {
                this.userProvider.setToken(this.login_data.token);
                this.userProvider.setUserData({ nome: this.login_data.name, perfil: this.login_data.profile });
                this.navCtrl.setRoot('MainPage');
            } else {
                return this.message.error('Não foi possível se autenticar. Verifique a sua conexão para efetuar a primeira autenticação do usuário.');
            }
        } else {
            this.userProvider.login(this.login_data.user, this.login_data.password).subscribe(res => {    
                let login_result = res;

                this.userProvider.setToken(res.token)

                this.userProvider.getData().subscribe(res => {
                    loading.dismiss();
                    this.userProvider.setUserData(res);

                    this.saveLocalUser(this.login_data.user, login_result.token, login_result.expires, res.nome, res.perfil, res.campanhas, this.login_data.save);

                    this.navCtrl.setRoot('MainPage');
                }, err => {
                    loading.dismiss();
                    console.error(err);
                    return this.message.error();
                })
            }, err => {
                loading.dismiss();            
                console.error(err);
                
                if (err.error && err.error.mensagem) {
                    return this.message.alert(err.error.mensagem);
                }

                return this.message.error();
            })
        }
    }

    recoverPassword() {
        if (!this.login_data.user) {
            return this.message.alert('Para recuperar a senha, informe o seu e-mail');
        }

        let select = this.alertCtrl.create({
            title: 'Recuperação de Senha',
            subTitle: 'Informe o código enviado para o e-mail ****',
            inputs: [
                {
                    type: 'tel',
                    name: 'code'
                }
            ],
            buttons: [
                {
                    text: 'Voltar',
                    role: 'cancel',
                    cssClass: 'cancel-button'
                },
                {
                    text: 'OK',
                    cssClass: 'ok-button',
                    handler: data => {
                        this.navCtrl.push('ChangePasswordPage');
                    }
                }
            ]
        });
        
        select.present();
    }

    private saveLocalUser(user, token, expires_date, name, profile, campaigns, save) {
        let data = {
            user: user,
            token: token,
            expires_date: new Date(expires_date),
            last_user: user,
            name: name,
            profile: profile,
            campaigns: campaigns,
            save: save
        }

        if (this.platform.is('cordova')) {
            this.storage.setItem('imunocare-user', data).then(() => {
                console.log('user saved');
            })
        } else {
            window.localStorage.setItem('imunocare-user', JSON.stringify(data));
        }
    }
}
