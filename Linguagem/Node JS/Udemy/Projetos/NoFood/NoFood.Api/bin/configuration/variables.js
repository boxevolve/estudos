const variables = {
    Api: {
        port: process.env.port || 3000
    },
    Database: {
        connection: process.env.connection || 'mongodb://admin:admin123456@ds123454.mlab.com:23454/nofood'
    }
}
module.exports = variables;
