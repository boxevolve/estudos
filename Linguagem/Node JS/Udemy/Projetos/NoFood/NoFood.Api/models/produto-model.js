'use strict'

const mongoose = require('mongoose');
const schema = mongoose.Schema;

const produtoModel = new schema({
    nome: { trim: true, index: true, required: true, type: String },
    descricao: { type: String, required: true },
    preco: { type: Number, required: true },
    foto: { type: String, required: true },
    ativo: { type: Boolean, required: true },
    dataCriacao: { type: Date, default: Date.now }
}, { versionKey: false });

produtoModel.pre('save', next => {
    let now = new Date();
    if (!this.dataCriacao) {
        this.dataCriacao = now;
    } 
    next();
});

module.exports = mongoose.model('Produto', produtoModel);
